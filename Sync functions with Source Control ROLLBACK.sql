/*
Run this script on:

        CHI-SQ-DP-01\LINKED_DB.functions2    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\LINKED_DB.functions

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/13/2020 10:17:02 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[preCreditMemoIntegration]'
GO

ALTER PROCEDURE [dbo].[preCreditMemoIntegration] 
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,@startdate)) --the end of the start month

--get the hotel production totals for invoice header information
DECLARE @hotelPreferences as table
	(	hotelCode nvarchar(10), currencyCode char(3), DECPLCUR int --, PYMTRMID char(21), DSCPCTAM int
	)

INSERT	INTO @hotelPreferences
SELECT	HS.hotelCode,
		customerMaster.CURNCYID,
		currencyMaster.DECPLCUR

FROM	ReservationBilling.loyalty.Credit_Memos CM

JOIN	Core.dbo.hotels_Synxis HS
	ON	CM.hotel_code = HS.hotelcode

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	HS.hotelCode = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC40200 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID

WHERE	CM.Create_Date >= @startDate
AND		CM.Redemption_Date <= @enddate
AND		ISNULL(CM.Payable_Value, 0) <> 0.00
		
GROUP BY HS.hotelCode, customerMaster.CURNCYID, currencyMaster.DECPLCUR --,customerMaster.PYMTRMID --,paymentTerms.DSCPCTAM
ORDER BY HS.hotelCode


--wipe out the single use table for integration
truncate table dbo.CreditMemoIntegration

--fill the single use reservation integration table with the details of this particular integration
INSERT INTO dbo.CreditMemoIntegration
	(	[CustomerCode] ,[BrandCode] ,[InvoiceDate] ,[ItemCode] ,[CurrencyCode] ,[gpSiteId] ,
		[AccountNumber] , [EntryType], [Source] , [Quantity] , TotalValue, BatchID , [poNum]
	)
select	CM.hotel_code , -- [customerCode]
		H.mainbrandcode , --[brandCode]
		@invoiceDate , --[invoiceDate]
		'IPREFER_CM' , --[itemCode]  ** hard coded for the time being, but could change in the future
		HP.currencyCode , --[currencyCode]
		B.[gpSiteId] , -- from Brands table, based on hotel main brand code ??
		'GL.00100 AcctNumbers' ,
		'EntryType ',
		'Source' , 
		COUNT(CM.Voucher_Number) , -- [quantity]
		CAST(SUM(ROUND(CM.Payable_Value * CASE HP.currencyCode WHEN 'USD' THEN 1.00 ELSE CurrencyMaster.XCHGRATE END, HP.DECPLCUR-1)) as DECIMAL(12,2)) , --as CreditAmount_HotelCurrency ,
		'IPREFERCM' + CAST(YEAR(@invoiceDate) as nvarchar(4)) + RIGHT(('0' + CAST(MONTH(@invoiceDate) as nvarchar(2))), 2),
		'IPREFERREDEMPTION'  -- ,[poNum]

from	@hotelPreferences HP
join	ReservationBilling.dbo.[Credit_Memos] CM 
	on	HP.hotelcode = CM.Hotel_Code

join	Core.dbo.HotelsReporting H with (nolock)
	on	CM.hotel_code = H.code

join	Core.dbo.Brands B with (nolock)
	on	H.mainbrandcode = B.code

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	H.Code = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC00100 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID
	AND	CM.Currency_Conversion_Date between currencyMaster.EXCHDATE AND currencyMaster.EXPNDATE
	AND	EXGTBLID like '%AVG%'

WHERE	CM.Create_Date >= @startDate
AND		CM.Redemption_Date <= @enddate
AND		CM.sopNumber is null

GROUP BY CM.hotel_code, H.mainbrandcode , B.[gpSiteId], HP.DECPLCUR, HP.currencyCode

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[preResIntegration]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-03-19
-- Description:	Load table functions.dbo.reservationIntegration from job RES for reservationbilling
-- History:
--			2019-11-12	Brian Davey	Performance Tuning
-- =============================================

ALTER PROCEDURE [dbo].[preResIntegration]
	@startDate date,
	@endDate date,
	@invoiceDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--these declare/sets are just a short cut for simulating parameter values for troubleshooting the statements.
	--DECLARE @invoiceDate AS Date,@startDate AS Date,@endDate AS Date
	--SET @invoiceDate = CAST(CONVERT(char(2),MONTH(GETDATE())) + '/10/' + CONVERT(char(4),YEAR(GETDATE())) AS datetime)
	--SET @startDate = DATEADD(MONTH,-1,CAST(CONVERT(char(2),MONTH(GETDATE())) + '/01/' + CONVERT(char(4),YEAR(GETDATE())) AS datetime))
	--SET @endDate = DATEADD(DAY,-1,CAST(CONVERT(char(2),MONTH(GETDATE())) + '/01/' + CONVERT(char(4),YEAR(GETDATE())) AS datetime))


	-- CREATE & POPULATE #CD ------------------------------
	IF OBJECT_ID('tempdb..#CD') IS NOT NULL
		DROP TABLE #CD
	CREATE TABLE #CD
	(
		billableDate date NOT NULL,

		classificationID int NOT NULL,


		ChargeValueInUSD decimal(38,2) NOT NULL,
		hotelCode nvarchar(10),
		confirmationNumber nvarchar(255) NOT NULL,
		roomRevenueInHotelCurrency decimal(38,2),
		roomNights int,
		itemCode nvarchar(50) NOT NULL,
		sopNumber char(21),
		collectionCode nvarchar(6),
		gpSiteID char(2),
		[hotelCurrencyCode] char(3),
		chargeValueInHotelCurrency decimal(38,2)
	)

	INSERT INTO #CD(billableDate,classificationID,ChargeValueInUSD,hotelCode,confirmationNumber,roomRevenueInHotelCurrency,roomNights,itemCode,sopNumber,collectionCode,gpSiteID,[hotelCurrencyCode],chargeValueInHotelCurrency)
	SELECT billableDate,classificationID,ChargeValueInUSD,hotelCode,confirmationNumber,roomRevenueInHotelCurrency,roomNights,itemCode,sopNumber,collectionCode,gpSiteID,[hotelCurrencyCode],chargeValueInHotelCurrency
	FROM ReservationBilling.dbo.Charges
	WHERE billableDate BETWEEN @startDate AND @endDate
		AND
		(
			classificationID NOT IN (4,5) --remove non billable charge because of NULL itemcode
			OR
			(classificationID = 5 AND ChargeValueInUSD <> 0.00)
		)
	-------------------------------------------------------


	--get the hotel production totals for invoice header information
	DECLARE	@hotelTotals AS table(hotelCode nvarchar(10),bookings int,roomNights int,revenue decimal(18,2),currencyCode char(3),
									DECPLCUR int,PYMTRMID char(21),DSCPCTAM int,mainBrand nvarchar(10))

	INSERT	INTO @hotelTotals
	SELECT	hr.code,
			COUNT(*) AS bookings,
			SUM(billy.roomNights) AS roomNights,
			CAST(ROUND(SUM(billy.roomRevenueInHotelCurrency),MAX(DECPLCUR)-1) AS Decimal(10,2)) AS revenue,
			customerMaster.CURNCYID,
			currencyMaster.DECPLCUR,
			customerMaster.PYMTRMID,
			paymentTerms.DSCPCTAM,
			hr.mainBrandCode
	FROM
	(
		SELECT DISTINCT hotelCode,confirmationNumber,billableDate,roomRevenueInHotelCurrency,roomNights
		FROM #CD

		WHERE itemCode <> 'PMS_I'
			AND itemcode <> 'IPREFERMANUAL_I'

			AND itemcode <> 'PMS_B'
			AND itemcode <> 'IPREFERMANUAL_B' 

			AND classificationid <> 5
	) AS billy 
		INNER JOIN Core.dbo.hotelsReporting hr ON billy.hotelCode = hr.code
		INNER JOIN IC.dbo.RM00101 AS customerMaster ON hr.code = customerMaster.CUSTNMBR
		INNER JOIN DYNAMICS.dbo.MC40200 AS currencyMaster ON customerMaster.CURNCYID = currencyMaster.CURNCYID

		LEFT OUTER JOIN IC.dbo.SY03300 AS paymentTerms ON customerMaster.PYMTRMID = paymentTerms.PYMTRMID



	GROUP BY hr.code,customerMaster.CURNCYID,currencyMaster.DECPLCUR,customerMaster.PYMTRMID,paymentTerms.DSCPCTAM,hr.mainBrandCode
	ORDER BY hr.code

	--wipe out the single use table for reservation integration
	DELETE FROM functions.dbo.reservationIntegration

	--fill the single use reservation integration table with the details of this particular integration
	INSERT INTO functions.dbo.reservationIntegration([customerCode],[brandCode],[invoiceDate],[itemCode],[currencyCode],[gpSiteId],[totalRoomNights],
													[totalRevenue],[totalBookings],[quantity],[pricePer],[totalPrice],[autoCreditAmount],[batchID],[poNum])
	SELECT	CD.[hotelCode]
			,CD.collectionCode
			,@invoiceDate AS invoiceDate
			,CD.[itemCode]
			,[hotelCurrencyCode]
			,CD.gpSiteID        
			,MAX(totals.[roomNights]) AS totalRoomNights
			,MAX(totals.[revenue]) AS totalRevenue
			,MAX(totals.bookings) AS totalBookings
			,COUNT(DISTINCT CD.confirmationNumber) AS quantity
			,CASE (totals.DECPLCUR-1)
					WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) AS DECIMAL(12,2))) AS DECIMAL(12,2))
					ELSE CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) AS DECIMAL(12,2)) 
			END AS pricePer
			,CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2)) AS totalPrice
			,ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2)) * 
				((CASE itemCodeMaster.USCATVLS_1
					WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM
					ELSE 0
					END AS decimal(8,3))
				ELSE 0
			END)/10000),totals.DECPLCUR-1) AS autoCreditAmount
			,('RES ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + ' STD') AS batchID
			,('ARRIVALS ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2)) AS poNum

	FROM #CD CD

		LEFT OUTER JOIN IC.dbo.IV00101 AS itemCodeMaster ON	CD.itemCode = itemCodeMaster.ITEMNMBR
		LEFT OUTER JOIN @hotelTotals AS totals ON CD.hotelCode = totals.hotelCode

	WHERE totals.mainBrand NOT IN ('HE','HW')

		AND	CD.sopNumber is null -- new charges only
		AND CD.classificationID <> 5 --remove iprefer charge
	GROUP BY CD.[hotelCode],CD.collectionCode,CD.gpSiteID,CD.[itemCode],[hotelCurrencyCode],itemCodeMaster.USCATVLS_1,totals.PYMTRMID,totals.DSCPCTAM,totals.DECPLCUR
	ORDER BY CD.[hotelCode],itemCode


	--fill the single use reservation integration table with the details of this particular integration
	INSERT INTO functions.dbo.reservationIntegration([customerCode],[brandCode],[invoiceDate],[itemCode],[currencyCode],[gpSiteId],[totalRoomNights],
													[totalRevenue],[totalBookings],[quantity],[pricePer],[totalPrice],[autoCreditAmount],[batchID],[poNum])
	SELECT	CD.[hotelCode]
			,CD.collectionCode
			,@invoiceDate AS invoiceDate
			,CD.[itemCode]
			,[hotelCurrencyCode]
			,CD.gpSiteID        
			,MAX(totals.[roomNights]) AS totalRoomNights
			,MAX(totals.[revenue]) AS totalRevenue
			,MAX(totals.bookings) AS totalBookings
			,COUNT(DISTINCT CD.confirmationNumber) AS quantity
			,CASE (totals.DECPLCUR-1)
					WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) AS DECIMAL(12,2))) AS DECIMAL(12,2))
					ELSE CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) AS DECIMAL(12,2)) 
			END AS pricePer
			,CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2)) AS totalPrice
			,ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) AS DECIMAL(12,2)) * 
				((CASE itemCodeMaster.USCATVLS_1
					WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM
					ELSE 0
					END AS decimal(8,3))
				ELSE 0
			END)/10000),totals.DECPLCUR-1) AS autoCreditAmount
			,('RES' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + 'HESTD') AS batchID
			,('ARRIVALS ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2)) AS poNum

	FROM	#CD CD

		LEFT OUTER JOIN IC.dbo.IV00101 AS itemCodeMaster ON	CD.itemCode = itemCodeMaster.ITEMNMBR
		LEFT OUTER JOIN @hotelTotals AS totals ON CD.hotelCode = totals.hotelCode

	WHERE totals.mainBrand IN ('HE','HW')

		AND CD.sopNumber is null -- new charges only
		AND CD.classificationID <> 5 --remove iprefer charge
	GROUP BY CD.[hotelCode],CD.collectionCode,CD.gpSiteID,CD.[itemCode],[hotelCurrencyCode],itemCodeMaster.USCATVLS_1,totals.PYMTRMID,totals.DSCPCTAM,totals.DECPLCUR
	ORDER BY CD.[hotelCode],itemCode


	-- get iprefer charge data

	select	hotel_code,mainbrandcode,Hotel_Currency,gpSiteId,Qty,Billable_Amount_Hotel_Currency,billing_Date,CAST('IPREFER' AS varchar(50)) AS itemcode
	into	#IPC_TEMP
	from	Superset.BSI.PreReservationIntegration_IPreferCharges
	WHERE Billing_Date BETWEEN @startDate AND @endDate

	INSERT INTO #IPC_TEMP
	SELECT [hotelCode],collectionCode,[hotelCurrencyCode],gpSiteID,COUNT(confirmationNumber) AS Qty,SUM([chargeValueInHotelCurrency]) AS Billable_Amount_Hotel_Currency,billableDate AS billing_Date,itemcode 
	FROM #CD
	WHERE classificationID = 5 AND ChargeValueInUSD <> 0.00 AND sopNumber is null
	GROUP BY [hotelCode],collectionCode,[hotelCurrencyCode],gpSiteID,itemcode,billableDate

	SELECT hotel_code,mainbrandcode,Hotel_Currency,gpSiteId,SUM(Qty) AS Qty,SUM(Billable_Amount_Hotel_Currency) AS Billable_Amount_Hotel_Currency,MAX(billing_Date) AS billing_Date,itemcode
	INTO #IPC
	FROM #IPC_TEMP
	GROUP BY hotel_code,mainbrandcode,Hotel_Currency,gpSiteId,itemcode


	--  now new insert the new iPrefer line item 
	INSERT INTO functions.dbo.reservationIntegration([customerCode],[brandCode],[invoiceDate],[itemCode],[currencyCode],[gpSiteId],[totalRoomNights],
													[totalRevenue],[totalBookings],[quantity],[pricePer],[totalPrice],[autoCreditAmount],[batchID],[poNum])
	SELECT	IPC.hotel_code,-- [customerCode]
			IPC.mainbrandcode,--[brandCode]
			@invoiceDate,--[invoiceDate]
			IPC.itemcode,
			IPC.Hotel_Currency,--[currencyCode]
			IPC.gpSiteId,
			0 AS totalRoomNights,
			0 AS totalRevenue,
			0 AS totalBookings,
			IPC.Qty,
			CASE (totals.DECPLCUR-1)
					WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),IsNull(totals.DECPLCUR-1,2) ) AS DECIMAL(12,2))/ [IPC].[Qty],IsNull(totals.DECPLCUR-1,2)) AS DECIMAL(12,2))) AS DECIMAL(12,2))
					ELSE CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),IsNull(totals.DECPLCUR-1,2)) AS DECIMAL(12,2))/ [IPC].[Qty],IsNull(totals.DECPLCUR-1,2) ) AS DECIMAL(12,2)) 
			END AS pricePer,--  [pricePer]
			CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),Isnull(totals.DECPLCUR-1,2) ) AS DECIMAL(12,2)) AS totalPrice
			--,ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),totals.DECPLCUR-1) AS DECIMAL(12,2)) * 
			--((CASE itemCodeMaster.USCATVLS_1
			--	WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM ELSE 0 END AS decimal(8,3))
			--	ELSE 0
			--END)/10000),totals.DECPLCUR-1) AS autoCreditAmount -- autoCreditAmount
			,0.00 AS autoCreditAmount
			,('RES ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + ' IPR') AS batchID
			,('ARRIVALS ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2)) AS poNum
	FROM #IPC IPC

		LEFT OUTER JOIN @hotelTotals AS totals ON IPC.hotel_Code = totals.hotelCode
	WHERE IPC.billing_Date BETWEEN @startDate AND @endDate

		AND (totals.mainBrand NOT IN ('HE','HW') OR totals.mainBrand IS NULL)
		--AND IPC.sopNumber is null

	GROUP BY IPC.hotel_code,IPC.mainbrandcode,IPC.Hotel_Currency,IPC.Qty,totals.PYMTRMID,totals.DSCPCTAM,totals.DECPLCUR,IPC.gpSiteId,IPC.itemcode
	ORDER BY IPC.hotel_code 


	--  now new insert the new iPrefer line item 
	INSERT INTO functions.dbo.reservationIntegration([customerCode],[brandCode],[invoiceDate],[itemCode],[currencyCode],[gpSiteId],[totalRoomNights],
													[totalRevenue],[totalBookings],[quantity],[pricePer],[totalPrice],[autoCreditAmount],[batchID],[poNum])
	SELECT	IPC.hotel_code,-- [customerCode]
			IPC.mainbrandcode,--[brandCode]
			@invoiceDate,--[invoiceDate]
			IPC.itemcode,
			IPC.Hotel_Currency,--[currencyCode]
			IPC.gpSiteId,
			0 AS totalRoomNights,
			0 AS totalRevenue,
			0 AS totalBookings,
			IPC.Qty,
			CASE (totals.DECPLCUR-1)
					WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),IsNull(totals.DECPLCUR-1,2) ) AS DECIMAL(12,2))/ [IPC].[Qty],IsNull(totals.DECPLCUR-1,2)) AS DECIMAL(12,2))) AS DECIMAL(12,2))
					ELSE CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),IsNull(totals.DECPLCUR-1,2)) AS DECIMAL(12,2))/ [IPC].[Qty],IsNull(totals.DECPLCUR-1,2) ) AS DECIMAL(12,2)) 
			END AS pricePer,--  [pricePer]
			CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),Isnull(totals.DECPLCUR-1,2) ) AS DECIMAL(12,2)) AS totalPrice
			--,ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),totals.DECPLCUR-1) AS DECIMAL(12,2)) * 
			--((CASE itemCodeMaster.USCATVLS_1
			--	WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM ELSE 0 END AS decimal(8,3))
			--	ELSE 0
			--END)/10000),totals.DECPLCUR-1) AS autoCreditAmount -- autoCreditAmount
			,0.00 AS autoCreditAmount
			,('RES' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + 'HEIPR') AS batchID
			,('ARRIVALS ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2)) AS poNum

	FROM #IPC IPC

		LEFT OUTER JOIN @hotelTotals AS totals ON IPC.hotel_Code = totals.hotelCode
	WHERE IPC.billing_Date BETWEEN @startDate AND @endDate

		AND totals.mainBrand IN ('HE','HW')
		--AND IPC.sopNumber is null

	GROUP BY IPC.hotel_code,IPC.mainbrandcode,IPC.Hotel_Currency,IPC.Qty,totals.PYMTRMID,totals.DSCPCTAM,totals.DECPLCUR,IPC.gpSiteId,IPC.itemcode
	ORDER BY IPC.hotel_code 

	-- only iprefer charges
	UPDATE ri
		SET	batchID = 'RES ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + ' IPO' 
	FROM functions.dbo.reservationIntegration ri
	where customercode in 
		(	select	distinct customercode 
			from	functions.dbo.reservationIntegration
			where	itemCode = 'IPREFER'
					OR itemcode LIKE '%[_]I'
		)
		AND customercode NOT IN  
		(	select	CustomerCode 
			from	functions.dbo.reservationIntegration
			group by customerCode
			having COUNT(distinct batchID) > 1
		)	

update	ri
set		batchID = 'RES ' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + ' IPR' 
from functions.dbo.reservationIntegration AS ri
inner join @hotelTotals AS totals
on ri.customerCode = totals.hotelCode
and totals.mainBrand NOT IN ('HE','HW')
where	customercode in 
	(	select	distinct customercode 
		from	functions.dbo.reservationIntegration
		where	itemCode = 'IPREFER'
				OR itemcode LIKE '%[_]I'
	)

update	ri
set		batchID = 'RES' + CAST(YEAR(@startDate) AS nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) AS nvarchar(2))),2) + 'HEIPR' 
from functions.dbo.reservationIntegration AS ri
inner join @hotelTotals AS totals
on ri.customerCode = totals.hotelCode
and totals.mainBrand IN ('HE','HW')
where	customercode in 
	(	select	distinct customercode 
		from	functions.dbo.reservationIntegration
		where	itemCode = 'IPREFER'
				OR itemcode LIKE '%[_]I'
	)	

--delete any invoices that would have no charges
DELETE FROM [functions].[dbo].[reservationIntegration]
WHERE customerCode IN (
SELECT customerCode
  FROM [functions].[dbo].[reservationIntegration]
  GROUP BY customerCode
  HAVING SUM(totalPrice) = 0)

END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SSRS_IPCM_Report_FormsPrinter]'
GO





-- =============================================
-- Author:		Kris Scott
-- Create date: Jun 2 2015
-- Description:	IPCM Report query to use when Forms Printer is running the report
-- =============================================
ALTER PROCEDURE [dbo].[SSRS_IPCM_Report_FormsPrinter] 
	@SOPNumber char(21), --provided by forms printer
	@SOPType int, --provided by forms printer
	@DocumentType int, --provided by forms printer
	@UserID char(15),  --provided by forms printer
	@SequenceNumber int	--provided by forms printer
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	/*use a UNION of both History and Unposted documents that may be in the forms printer batch*/ 

	/* The select statement is the same for both unions, the SOP Header and Amounts tables are all that differ */
	(
	SELECT 
	LTRIM(RTRIM(customerMaster.CUSTNAME)) as Hotel_Name,
	LTRIM(RTRIM(customerMaster.CUSTNMBR)) as Hotel_Code,
			CASE customerMaster.CUSTCLAS 
			WHEN 'PHGCONSULT' THEN 'PHG Consulting'
			WHEN 'HISTORIC' THEN 'Historic Hotels of America' 
			WHEN 'HISTORIC WWIDE' THEN 'Historic Hotels Worldwide' 
			ELSE 'Preferred Hotels & Resorts'
		END AS brandName,
  SopHeader.LOCNCODE,	
  Voucher_Number,
  Redemption_Date,
  Voucher_Currency,      
  Voucher_Value as Voucher_Value_Original_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', Voucher_Currency, [Currency_Conversion_Date]) as Voucher_Payable_Value_Original_Currency
  ,sopheader.CURNCYID as Billed_Currency
  ,dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Value_Payable_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Payable_Value_Payable_Currency
  ,Voucher_Value / dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Conversion_Rate
  ,SOPNumber
FROM IC.dbo.SOP50200 AS tempSopHeader
LEFT OUTER JOIN	IC.dbo.SOP10100 AS SopHeader
	ON tempSopHeader.SOPTYPE = SopHeader.SOPTYPE 
	AND tempSopHeader.SOPNUMBE = SopHeader.SOPNUMBE
LEFT OUTER JOIN [Superset].[BSI].[Credit_Memos] cm
	ON sopheader.SOPNUMBE = cm.SOPNumber
LEFT OUTER JOIN IC.dbo.RM00101 customerMaster
	ON sopheader.CUSTNMBR = customerMaster.CUSTNMBR		

WHERE	(@SOPNumber = '') AND (@SOPType = 0) 
AND (tempSopHeader.DOCTYPE = @DocumentType) 
AND (tempSopHeader.USERID = @UserID) 
AND (tempSopHeader.SEQNUMBR = @SequenceNumber) 

OR (tempSopHeader.DOCTYPE = @DocumentType) 
AND  (tempSopHeader.USERID = @UserID) 
AND (tempSopHeader.SEQNUMBR = @SequenceNumber) 
AND (tempSopHeader.SOPNUMBE = @SOPNumber) 
AND (tempSopHeader.SOPTYPE = @SOPType)
	)

UNION ALL

	(
	SELECT 
	LTRIM(RTRIM(customerMaster.CUSTNAME)) as Hotel_Name,
	LTRIM(RTRIM(customerMaster.CUSTNMBR)) as Hotel_Code,
			CASE customerMaster.CUSTCLAS 
			WHEN 'PHGCONSULT' THEN 'PHG Consulting'
			WHEN 'HISTORIC' THEN 'Historic Hotels of America' 
			WHEN 'HISTORIC WWIDE' THEN 'Historic Hotels Worldwide' 
			ELSE 'Preferred Hotels & Resorts'
		END AS brandName,
  SopHeader.LOCNCODE,	
  Voucher_Number,
  Redemption_Date,
  Voucher_Currency,      
  Voucher_Value as Voucher_Value_Original_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', Voucher_Currency, [Currency_Conversion_Date]) as Voucher_Payable_Value_Original_Currency
  ,sopheader.CURNCYID as Billed_Currency
  ,dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Value_Payable_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Payable_Value_Payable_Currency
  ,Voucher_Value / dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Conversion_Rate
  ,SOPNumber
FROM IC.dbo.SOP50200 AS tempSopHeader
LEFT OUTER JOIN	IC.dbo.SOP30200 AS SopHeader
	ON tempSopHeader.SOPTYPE = SopHeader.SOPTYPE 
	AND tempSopHeader.SOPNUMBE = SopHeader.SOPNUMBE
LEFT OUTER JOIN [Superset].[BSI].[Credit_Memos] cm
	ON sopheader.SOPNUMBE = cm.SOPNumber
LEFT OUTER JOIN IC.dbo.RM00101 customerMaster
	ON sopheader.CUSTNMBR = customerMaster.CUSTNMBR			

WHERE	(@SOPNumber = '') AND (@SOPType = 0) 
AND (tempSopHeader.DOCTYPE = @DocumentType) 
AND (tempSopHeader.USERID = @UserID) 
AND (tempSopHeader.SEQNUMBR = @SequenceNumber) 

OR (tempSopHeader.DOCTYPE = @DocumentType) 
AND  (tempSopHeader.USERID = @UserID) 
AND (tempSopHeader.SEQNUMBR = @SequenceNumber) 
AND (tempSopHeader.SOPNUMBE = @SOPNumber) 
AND (tempSopHeader.SOPTYPE = @SOPType)

)
					
END






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SSRS_IPCM_Report_History]'
GO





-- =============================================
-- Author:		Kris Scott
-- Create date: Jun 2 2015
-- Description:	IPCM query to use for a single history document
-- =============================================
ALTER PROCEDURE [dbo].[SSRS_IPCM_Report_History] 
	@SOPNumber char(21)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	LTRIM(RTRIM(customerMaster.CUSTNAME)) as Hotel_Name,
	LTRIM(RTRIM(customerMaster.CUSTNMBR)) as Hotel_Code,
			CASE customerMaster.CUSTCLAS 
			WHEN 'PHGCONSULT' THEN 'PHG Consulting'
			WHEN 'HISTORIC' THEN 'Historic Hotels of America' 
			WHEN 'HISTORIC WWIDE' THEN 'Historic Hotels Worldwide' 
			ELSE 'Preferred Hotels & Resorts'
		END AS brandName,
  SopHeader.LOCNCODE,	
  Voucher_Number,
  Redemption_Date,
  Voucher_Currency,      
  Voucher_Value as Voucher_Value_Original_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', Voucher_Currency, [Currency_Conversion_Date]) as Voucher_Payable_Value_Original_Currency
  ,sopheader.CURNCYID as Billed_Currency
  ,dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Value_Payable_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Payable_Value_Payable_Currency
  ,Voucher_Value / dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Conversion_Rate
  ,SOPNumber
FROM [Superset].[BSI].[Credit_Memos] cm
INNER JOIN IC.dbo.SOP30200 sopheader
	ON cm.SOPNumber = sopheader.SOPNUMBE
INNER JOIN IC.dbo.RM00101 customerMaster
	ON sopheader.CUSTNMBR = customerMaster.CUSTNMBR		
WHERE cm.SOPNumber = @SOPNumber
END






GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SSRS_IPCM_Report_Unposted]'
GO



-- =============================================
-- Author:		Kris Scott
-- Create date: Jun 2 2015
-- Description:	IPCM Report query to use for a single unposted document
-- =============================================
ALTER PROCEDURE [dbo].[SSRS_IPCM_Report_Unposted] 
	@SOPNumber char(21)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	LTRIM(RTRIM(customerMaster.CUSTNAME)) as Hotel_Name,
	LTRIM(RTRIM(customerMaster.CUSTNMBR)) as Hotel_Code,
			CASE customerMaster.CUSTCLAS 
			WHEN 'PHGCONSULT' THEN 'PHG Consulting'
			WHEN 'HISTORIC' THEN 'Historic Hotels of America' 
			WHEN 'HISTORIC WWIDE' THEN 'Historic Hotels Worldwide' 
			ELSE 'Preferred Hotels & Resorts'
		END AS brandName,
  SopHeader.LOCNCODE,
  Voucher_Number,
  Redemption_Date,
  Voucher_Currency,      
  Voucher_Value as Voucher_Value_Original_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', Voucher_Currency, [Currency_Conversion_Date]) as Voucher_Payable_Value_Original_Currency
  ,sopheader.CURNCYID as Billed_Currency
  ,dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Value_Payable_Currency
  ,dbo.convertCurrency(Payable_Value,'USD', sopheader.CURNCYID, [Currency_Conversion_Date]) as Voucher_Payable_Value_Payable_Currency
  ,Voucher_Value / dbo.convertCurrency(Voucher_Value,Voucher_Currency, sopheader.CURNCYID, [Currency_Conversion_Date]) as Conversion_Rate
  ,SOPNumber
FROM [Superset].[BSI].[Credit_Memos] cm
INNER JOIN IC.dbo.SOP10100 sopheader
	ON cm.SOPNumber = sopheader.SOPNUMBE
INNER JOIN IC.dbo.RM00101 customerMaster
	ON sopheader.CUSTNMBR = customerMaster.CUSTNMBR	
WHERE cm.SOPNumber = @SOPNumber

END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[checkMissingDays_Reservations]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-08-07
-- Description:	Checks for missing superset days
-- =============================================
ALTER PROCEDURE [dbo].[checkMissingDays_Reservations]
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#DATE_RANGE') IS NOT NULL
		DROP TABLE #DATE_RANGE;
	CREATE TABLE #DATE_RANGE(MissingDate date NOT NULL, PRIMARY KEY CLUSTERED(MissingDate))

	;WITH DateRange(Date) AS
	(
		SELECT @StartDate Date
			UNION ALL
		SELECT DATEADD(day, 1, Date) Date FROM DateRange WHERE Date < @EndDate
	)
	INSERT INTO #DATE_RANGE(MissingDate)
	SELECT Date as MissingDate 
	FROM DateRange
	OPTION (MaxRecursion 10000);


	SELECT MissingDate FROM #DATE_RANGE
	EXCEPT 
	SELECT DISTINCT CAST(t.transactiontimestamp as DATE)
	FROM [CHI-SQ-PR-01\WAREHOUSE].Reservations.dbo.transactions t
	WHERE t.DataSourceID IN(SELECT DataSourceID FROM [CHI-SQ-PR-01\WAREHOUSE].Reservations.authority.DataSource WHERE SourceName = 'SynXis')
		AND t.sourceKey IS NOT NULL
		AND CAST(t.transactiontimestamp as DATE) BETWEEN @StartDate AND @EndDate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[currencyRateInsert]'
GO


/* =================================================================================================
-- Author:		Kris Scott
-- Create date: 5-27-2011
-- Description:	Update currency rates in GP
Changes:

Name				Date		Description
-------------------------------------------------
Laura Culley		11/30/2011	Changed start and end date to accomdate daily currency rate loads
Laura Culley		12/29/2011  Changed end date to be the same date as the start date to accurately calculate 
								the currency Rate.
Brian Davey			26-JUN-2017	Modified SP to RAISERROR when no rows are inserted.
Ti Yao				2020-04-09 Modified SP to import into run status report
-- ================================================================================================*/
ALTER PROCEDURE [dbo].[currencyRateInsert] 
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @suffixes AS TABLE (suffix nvarchar(10))

	DECLARE @Err nvarchar(4000)

	DECLARE @QueueID int

	BEGIN TRY

		INSERT INTO ETL.dbo.Queue( [Application], [FilePath], [CreateDate], [QueueStatus], [ImportStarted])
		SELECT 'CurrencyImporttoGP', 'CHISQP01', GETDATE(), 1, GETDATE()

		SET @QueueID = SCOPE_IDENTITY()

		INSERT INTO @suffixes(suffix)
		VALUES('-AVG'),('-BUY'),('-SELL')

		DECLARE @startDate datetime, @endDate datetime

		SET @startDate = (DATEADD(DAY, 0, DATEDIFF(DAY, 0, GETDATE())))
		SET @endDate = @startDate


		INSERT INTO DYNAMICS.dbo.MC00100([EXGTBLID],[CURNCYID],[EXCHDATE],TIME1,[XCHGRATE],[EXPNDATE])
		SELECT [EXGTBLID],[CURNCYID],[EXCHDATE],TIME1,[XCHGRATE],[EXPNDATE]
		FROM
			(
				SELECT DISTINCT [code] + suffix AS EXGTBLID,code AS CURNCYID,@startDate AS EXCHDATE,'1900-01-01 00:00:00.000' AS TIME1,CONVERT(numeric(19,7),fromUSD) AS XCHGRATE,@endDate AS EXPNDATE
				FROM [CurrencyRates].[dbo].[dailyRates]
					INNER JOIN DYNAMICS.dbo.MC40200 ON dailyRates.code = MC40200.CURNCYID
					INNER JOIN @suffixes ON 1=1
				WHERE dailyRates.rateDate = @startDate
	
				EXCEPT

				SELECT [EXGTBLID],[CURNCYID],[EXCHDATE],TIME1,[XCHGRATE],[EXPNDATE]
				FROM DYNAMICS.dbo.MC00100
				WHERE [EXCHDATE] = @startDate
			) xchg
		ORDER BY CURNCYID

		IF @@ROWCOUNT = 0
			RAISERROR(N'WARNING! No Records Inserted Into Great Plains.',16,1)

		EXEC ETL.[dbo].[ftp_UpdateQueueStatus] @QueueID,2,'FINISHED',0

	END TRY
	BEGIN CATCH
		SELECT @Err = ERROR_MESSAGE()

		EXEC ETL.dbo.ftp_UpdateQueueStatus @QueueID,3,@Err,0

		RAISERROR(@Err,16,1)
	END CATCH


END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[checkMissingDays_Epsilon]'
GO


-- =============================================
-- Author:		Jake Smith
-- Create date: 2020-03-04
-- Description:	Checks for missing Epsilon days
-- =============================================
CREATE PROCEDURE [dbo].[checkMissingDays_Epsilon]
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  WITH DateRange(Date) AS
     (
         SELECT
             @StartDate Date
         UNION ALL
         SELECT
             DATEADD(day, 1, Date) Date
         FROM
             DateRange
         WHERE
             Date < @EndDate
     )

     SELECT Date as MissingDate 
     FROM DateRange
     EXCEPT 
     SELECT DISTINCT CAST(activityDate as DATE)
     FROM [CHI-SQ-PR-01\WAREHOUSE].loyalty.dbo.PointActivity
     WHERE 
	 CAST(activitydate as DATE) BETWEEN @StartDate AND @EndDate
     --You could remove Maximum Recursion level constraint by specifying a MaxRecusion of zero
     OPTION (MaxRecursion 10000);
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
