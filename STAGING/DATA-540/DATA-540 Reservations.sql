USE Reservations
GO

/*
Run this script on:

        CHI-SQ-ST-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/10/2019 12:41:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[TokyuReservations]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [rpt].[TokyuReservations]
	-- Add the parameters for the stored procedure here
	 @dateType bit
	,@startDate date
	,@endDate date
	,@Gross	bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select 

g.FirstName,
g.LastName,
ge.emailAddress,
ln.loyaltyNumber,
lp.loyaltyProgram,
t.confirmationNumber,
ts.confirmationDate,
td.arrivalDate,
hh.HotelCode as Hotel_Code,
hh.HotelName as Hotel_Name,
r.RegionName as Hotel_Region,
td.reservationRevenue * xe.toUSD as reservationRevenueUSD,
td.nights,
ts.status,
ibe.ibeSourceName, 
cro.CRO_Code

from Reservations.dbo.transactions t
join Reservations.dbo.TransactionDetail td
	ON t.TransactionDetailID = td.TransactionDetailID
join Reservations.dbo.TransactionStatus ts
	ON t.TransactionStatusID = ts.TransactionStatusID
join Reservations.dbo.Guest g
	ON t.GuestID = g.GuestID
join Reservations.dbo.Guest_EmailAddress ge
	ON g.Guest_EmailAddressID = ge.Guest_EmailAddressID
join Reservations.dbo.LoyaltyNumber ln
	ON t.LoyaltyNumberID = ln.LoyaltyNumberID
join Reservations.dbo.CRS_BookingSource cbs
	ON t.CRS_BookingSourceID = cbs.BookingSourceID
join Reservations.dbo.CROCode cx
	ON cbs.CROCodeID = cx.CROCodeID
join Reservations.authority.CRO_Code cro
	ON cx.auth_CRO_CodeID = cro.CRO_CodeID
join Reservations.dbo.ibeSource ix
	ON cbs.ibeSourceNameID = ix.ibeSourceID
join Reservations.authority.ibeSource ibe
	ON ix.auth_ibeSourceID = ibe.ibeSourceID
join Reservations.dbo.hotel rh
	ON t.HotelID = rh.HotelID
join Hotels.dbo.hotel hh
	ON rh.Hotel_hotelID = hh.HotelID
join Hotels.dbo.Location l
	ON hh.LocationID = l.LocationID
join Hotels.dbo.Region r
	ON l.RegionID = r.RegionID
join Reservations.dbo.LoyaltyProgram lp
	ON t.LoyaltyProgramID = lp.LoyaltyProgramID
join CurrencyRates.dbo.dailyRates xe
	ON ts.confirmationDate = xe.rateDate
	AND td.currency = xe.code
WHERE 
(CRO_Code IN ('SYN_Tokyu','CRO_Tokyu','SYN_TokyuAlliance ')
OR ibe.ibeSourceName IN ('Partner - Tokyu Private Offer'))
AND (
  (@dateType = 0 AND ts.confirmationDate BETWEEN @startDate AND @endDate)
OR
  (@dateType = 1 AND td.arrivalDate BETWEEN @startDate AND @endDate)
)
AND (@gross = 1 OR ts.status = 'Confirmed')
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
