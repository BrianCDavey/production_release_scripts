USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-ST-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/9/2019 9:57:14 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [work].[Calculate_Charges]'
GO
DROP PROCEDURE [work].[Calculate_Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Calculate_StandardCharges]'
GO







ALTER PROCEDURE [work].[Calculate_StandardCharges]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE TEMP TABLE ---------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES;

	CREATE TABLE #CHARGES
	(
		[clauseID] [int] NOT NULL,
		[billingRuleID] [int] NOT NULL,
		[classificationID] [int] NOT NULL,
		[transactionSourceID] [int] NOT NULL,
		[transactionKey] [nvarchar](20) NOT NULL,
		[confirmationNumber] [nvarchar](255) NOT NULL,
		[hotelCode] [nvarchar](10) NOT NULL,
		[collectionCode] [nvarchar](6) NULL,
		[clauseName] [nvarchar](250) NULL,
		[billableDate] [date] NULL,
		[arrivalDate] [date] NULL,
		[roomNights] [int] NULL,
		[hotelCurrencyCode] [char](3) NULL,
		[exchangeDate] [date] NULL,
		[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
		[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
		[chargeValueInUSD] [decimal](38, 2) NULL,
		[roomRevenueInUSD] [decimal](38, 2) NULL,
		[itemCode] [nvarchar](50) NULL,
		[gpSiteID] [char](2) NULL,
		[dateCalculated] [datetime] NULL,
		[sopNumber] [char](21) NULL,
		[invoiceDate] [date] NULL,
		[loyaltyNumber] [nvarchar](50) NULL,
		[LoyaltyNumberValidated] [bit] NULL,
		[LoyaltyNumberTagged] [bit] NULL		
	)
	------------------------------------------------------------------------------------

	;WITH cte_clauses AS (
		SELECT DISTINCT runID, transactionSourceID, transactionKey, clauseID, clauseName, criteriaID, RateCategoryCode, RateCode, TravelAgentGroupID
		FROM work.MrtForCalc_Clauses
		WHERE runID = @RunID
	)
	INSERT INTO #CHARGES(clauseID,billingRuleID,classificationID,transactionSourceID,transactionKey,confirmationNumber,hotelCode,collectionCode,
							clauseName,billableDate,arrivalDate,roomNights,hotelCurrencyCode,exchangeDate,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate, loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged)
	SELECT clauses.clauseID,br.billingRuleID,br.classificationID,mrtC.transactionSourceID,mrtC.transactionKey,mrtC.confirmationNumber,mrtC.phgHotelCode,mrtC.mainBrandCode AS collectionCode,
			clauses.ClauseName,mrtC.billableDate,mrtC.arrivalDate,mrtC.roomNights,mrtC.hotelCurrencyCode,mrtc.exchangeDate,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) *  mrtC.hotelCurrencyExchangeRate, mrtC.hotelCurrencyDecimalPlaces-1
			) AS chargeValueInHotelCurrency,
			ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * mrtC.hotelCurrencyExchangeRate,mrtC.hotelCurrencyDecimalPlaces-1) AS roomRevenueInHotelCurrency,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * 	((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) * 1.00, 2
		) AS chargeValueInUSD,
		ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * 1.00, 2) AS roomRevenueInUSD,
		COALESCE
		(c.itemCodeOverride,CASE cl.classificationName
								WHEN 'Booking' THEN
								--if any of the non-source based criteria fields are anything other than the wildcard, this charge is a 'special booking'
									CASE 
										WHEN (clauses.RateCategoryCode <> '*' OR clauses.RateCode <> '*' OR clauses.TravelAgentGroupID <> 0)
										THEN mrtC.ItemCode + '_SB'
        								ELSE mrtC.ItemCode + '_B'
									END
								WHEN 'Commission' THEN mrtC.ItemCode + '_C'
								WHEN 'Surcharge' THEN mrtC.ItemCode + '_S'
								WHEN 'Non-Billable' THEN NULL
								WHEN 'I Prefer' THEN mrtC.ItemCode + '_I'
								ELSE NULL
							END
		) AS ItemCode,
		COALESCE(c.gpSiteOverride,mrtC.gpSiteID) AS gpSiteID,
		GETDATE(),NULL,NULL,
		loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged
	FROM BillingRules br
		INNER JOIN cte_clauses as clauses ON clauses.ClauseID = br.clauseID
		INNER JOIN Classifications cl ON cl.classificationID = br.classificationID
		INNER JOIN work.MrtForCalculation mrtC ON mrtC.transactionSourceID = clauses.transactionSourceID
			AND mrtc.transactionKey = clauses.transactionKey
			AND mrtc.runID = clauses.runID
		INNER JOIN work.[local_exchange_rates] brCE 
		ON br.currencyCode = brCE.CURNCYID 
			AND mrtC.exchangeDate = brCE.EXCHDATE
		INNER JOIN dbo.Criteria c ON c.criteriaID = clauses.CriteriaID
		LEFT JOIN dbo.ThresholdRules tr ON tr.clauseID = clauses.ClauseID
	WHERE clauses.runID = @RunID
		AND (
				mrtC.bookingStatus != 'Cancelled'
				OR
				(mrtC.bookingStatus = 'Cancelled' AND br.refundable = 0)
			)
		AND (
				(br.afterConfirmation = 1 AND mrtC.confirmationDate >= br.confirmationDate)
				OR
				(br.afterConfirmation = 0 AND mrtC.confirmationDate <= br.confirmationDate)
			)
		AND mrtC.billableDate BETWEEN br.startDate AND br.endDate
	------------------------------------------------------------------------------------


		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE c
		FROM dbo.Charges c
			INNER JOIN work.MrtForCalculation m
				ON c.transactionSourceID = m.transactionSourceID
				AND c.transactionKey = m.transactionKey
		WHERE m.runID = @RunID
		AND c.sopNumber IS NULL
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO dbo.Charges(runID,clauseID,billingRuleID,classificationID,transactionSourceID,transactionKey,confirmationNumber,hotelCode,collectionCode,
							clauseName,billableDate,arrivalDate,roomNights,hotelCurrencyCode,exchangeDate,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate,loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged)
		SELECT @RunID,clauseID,billingRuleID,classificationID,transactionSourceID,transactionKey,confirmationNumber,hotelCode,collectionCode,
							clauseName,billableDate,arrivalDate,roomNights,hotelCurrencyCode,exchangeDate,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate,loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged
		FROM #CHARGES
		------------------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Calculate_ThresholdCharges]'
GO




ALTER PROCEDURE [work].[Calculate_ThresholdCharges]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @startDate date,@endDate date
	
	SELECT	@startDate = startDate,
			@endDate = endDate
	FROM work.Run
	WHERE RunID = @RunID
	
	-- CREATE #THRESHOLD TEMP TABLE --------------------------------------------
	IF OBJECT_ID('tempdb..#THRESHOLD') IS NOT NULL
		DROP TABLE #THRESHOLD
	CREATE TABLE #THRESHOLD
	(
		billableDate date,
		transactionSourceID int,
		transactionKey nvarchar(20),
		hotelCode nvarchar(10),
		billingRuleID int,
		thresholdMinimum decimal(18,2),
		thresholdTypeID int,
		clauseID int,
		sumRoomNights int,
		countConfNum int,
		sumRoomRevenue decimal(18,2)
	)
	----------------------------------------------------------------------------

	-- CREATE #CHARGES TEMP TABLE --------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES
	CREATE TABLE #CHARGES
	(
		clauseID int,
		billableDate date,
		transactionSourceID int,
		transactionKey int,
		hotelCode nvarchar(50),
		roomNights int,
		confCounter int,
		[roomRevenueInHotelCurrency] decimal(38,2)
	)
	----------------------------------------------------------------------------

	DECLARE @billingRuleID int,
			@thresholdMinimum decimal(18,2),
			@LastPeriod date,
			@EndOfPeriod date,
			@IsGlobal bit,
			@hotelCode nvarchar(10),
			@thresholdTypeID int,
			@clauseID int
	
	-- LOOP THROUGH EACH BILLING PERIOD WITHIN DATE RANGE ----------------------
	DECLARE curPeriod CURSOR FAST_FORWARD LOCAL FOR
		SELECT lp.LastPeriod,lp.EndOfPeriod,tr.[global],cl.hotelCode,tr.thresholdTypeID,tr.clauseID
		FROM dbo.fnc_Threshold_LastPeriod(@startDate,@endDate,@RunID) lp
			INNER JOIN dbo.ThresholdRules tr ON tr.clauseID = lp.clauseID
			INNER JOIN dbo.Clauses cl ON cl.clauseID = tr.clauseID
			INNER JOIN (SELECT DISTINCT ClauseID FROM work.MrtForCalc_Clauses WHERE runID = @RunID) mrtc ON mrtc.ClauseID = cl.clauseID

		OPEN curPeriod
		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE #CHARGES

		IF @IsGlobal = 0
		BEGIN
			INSERT INTO #CHARGES(clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,confCounter,[roomRevenueInHotelCurrency])
			SELECT @clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,
				MAX(roomNights),1,MAX([roomRevenueInHotelCurrency])
			FROM dbo.Charges
			WHERE billableDate BETWEEN @LastPeriod AND @EndOfPeriod
				AND clauseID = @clauseID
				--AND hotelCode = CASE @IsGlobal WHEN 0 THEN hotelCode ELSE @hotelCode END
				AND hotelCode = @hotelCode
				--AND (@IsGlobal = 0 OR (@IsGlobal = 1 AND transactionSourceID != 3)) --iPrefer manual transactions don't count in global counts, only if the threshold is just for manual points
			GROUP BY billableDate,transactionSourceID,transactionKey,hotelCode
		END
		ELSE IF @IsGlobal = 1
		BEGIN
			INSERT INTO #CHARGES(clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,confCounter,[roomRevenueInHotelCurrency])
			SELECT @clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,
				MAX(roomNights),1,MAX([roomRevenueInHotelCurrency])
			FROM dbo.Charges
			WHERE billableDate BETWEEN @LastPeriod AND @EndOfPeriod
				AND clauseID IS NOT NULL --= CASE @IsGlobal WHEN 0 THEN @clauseID ELSE clauseID END
				--AND hotelCode = CASE @IsGlobal WHEN 0 THEN hotelCode ELSE @hotelCode END
				AND hotelCode = @hotelCode
				AND transactionSourceID != 3 --iPrefer manual transactions don't count in global counts, only if the threshold is just for manual points
			GROUP BY billableDate,transactionSourceID,transactionKey,hotelCode
		END

		;WITH cte_Threshold
		AS
		(
			SELECT billableDate,transactionSourceID,transactionKey,hotelCode,clauseID,
					SUM(roomNights) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomNights,
					SUM(confCounter) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS countConfNum,
					SUM([roomRevenueInHotelCurrency]) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomRevenue
			FROM #CHARGES
		)
		INSERT INTO #THRESHOLD(billableDate,transactionSourceID,transactionKey,hotelCode,billingRuleID,thresholdMinimum,thresholdTypeID,clauseID,
								sumRoomNights,countConfNum,sumRoomRevenue)
		SELECT DISTINCT billableDate,transactionSourceID,transactionKey,hotelCode,br.billingRuleID,br.thresholdMinimum,@thresholdTypeID,@clauseID,
						sumRoomNights,countConfNum,sumRoomRevenue
		FROM cte_Threshold tr
			INNER JOIN dbo.BillingRules br ON br.[clauseID] = tr.[clauseID] AND tr.billableDate BETWEEN br.startDate AND br.endDate
		WHERE sumRoomNights >= CASE @thresholdTypeID WHEN 3 THEN br.thresholdMinimum ELSE sumRoomNights END
			AND countConfNum >= CASE @thresholdTypeID WHEN 2 THEN br.thresholdMinimum ELSE countConfNum END
			AND sumRoomRevenue >= CASE @thresholdTypeID WHEN 1 THEN br.thresholdMinimum ELSE sumRoomRevenue END
			AND br.startDate <= @endDate AND br.endDate >= @startDate

		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID
	END
	DEALLOCATE curPeriod
	----------------------------------------------------------------------------


		-- DELETE ALL CHARGES THAT HAVE THE SAME ClauseID BUT != BillingRuleID -----
		;WITH cte_charges
		AS
		(
			SELECT DISTINCT t.transactionSourceID,t.transactionKey,t.billingRuleID,t.clauseID
			FROM dbo.Charges c
				INNER JOIN #THRESHOLD t ON t.transactionSourceID = c.transactionSourceID
					AND t.transactionKey = c.transactionKey
					AND t.clauseID = c.clauseID
				INNER JOIN (SELECT transactionSourceID,transactionKey,clauseID,MAX(thresholdMinimum) AS thresholdMinimum
							FROM #THRESHOLD
							GROUP BY transactionSourceID,transactionKey,clauseID
						   ) x ON x.transactionSourceID = t.transactionSourceID
					AND x.transactionKey = t.transactionKey 
					AND x.clauseID = t.clauseID 
					AND x.thresholdMinimum = t.thresholdMinimum
			WHERE c.billableDate BETWEEN @startDate AND @endDate
		)
		DELETE c
		FROM dbo.Charges c
			INNER JOIN cte_charges cte ON cte.transactionSourceID = c.transactionSourceID
				AND cte.transactionKey = c.transactionKey
				AND cte.clauseID = c.clauseID
		WHERE c.billingRuleID != cte.billingRuleID
			AND c.sopNumber IS NULL
			AND c.runID = @RunID
		----------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[runCalculator]'
GO












ALTER PROCEDURE [work].[runCalculator]
	@runType bit = 1, --0=test, 1=production
	@startDate date = NULL,
	@endDate date = NULL,
	@hotelCode nvarchar(20) = NULL,
	@confirmationNumber nvarchar(255) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': start'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT


	DECLARE @RunID int;
	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunType,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),@runType,0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': work.Run insert'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-2,MIN(sub.billableDate)),
				@endDate = DATEADD(DAY,2,MAX(sub.billableDate)),
				@hotelCode = MAX(sub.phgHotelCode)
		
		FROM (		
			SELECT mrt.phgHotelCode, CONVERT(date,CASE WHEN mrt.arrivalDate >= GETDATE() THEN mrt.confirmationDate ELSE mrt.arrivaldate END) as billableDate
			FROM work.mrtJoined mrt
			WHERE mrt.confirmationNumber = @confirmationNumber
				UNION 
			SELECT tdr.phgHotelCode, CASE WHEN tdr.arrivalDate >= GETDATE() THEN tdr.transactionTimeStamp ELSE tdr.arrivalDate END as billableDate
			FROM work.tdrJoined tdr
			WHERE tdr.confirmationNumber = @confirmationNumber
			) as sub
	END

	IF(@startDate IS NULL OR @endDate IS NULL)
	BEGIN
		IF @startDate IS NULL
			BEGIN
			IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
			BEGIN
				SET @startDate = DATEADD (mm,-1,GETDATE())
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
			ELSE
			BEGIN
				SET @startDate = GETDATE()
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
		END

		IF @endDate IS NULL OR @endDate < @startDate
		BEGIN
			SET @endDate = DATEADD(YEAR,2,@startDate)
		END
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': set up parameter'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- UPDATE RUN PARAMETERS ----------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 1,
	startDate = @startDate,
	endDate = @endDate
	WHERE runID = @RunID;
	-----------------------------------------------------------------------------
	
	-- POPULATE [MrtForCalculation] ---------------------------------------------
	SET @print = '	@startDate=' + CONVERT(varchar(100),@startDate,120) + ', @endDate=' + CONVERT(varchar(100),@endDate,120) + ', @RunID=' + CONVERT(varchar(20), @RunID) + ', @hotelCode=' + @hotelCode + ', @confirmationNumber=' + @confirmationNumber
	RAISERROR(@print,10,1) WITH NOWAIT

	IF @runType = 0
	BEGIN
		EXEC [work].[Populate_ExchangeRates] @runType, @startDate, @endDate
		EXEC [test].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber
	END
	ELSE
		EXEC [work].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE [MrtForCalculation] complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- POPULATE MrtForCalc_Clauses ----------------------------------------------
	IF @runType = 0
		EXEC test.Populate_MrtForCalc_Clauses @RunID
	ELSE
		EXEC work.Populate_MrtForCalc_Clauses @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE work.Populate_MrtForCalc_Clauses complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- Eliminate excluded clauses------------------------------------------------
	IF @runType = 0
		EXEC test.handleExclusions @RunID
	ELSE
		EXEC work.handleExclusions @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': handleExclusions complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- RUN standard calculation -------------------------------------------------
	IF @runType = 0
		EXEC test.Calculate_StandardCharges @RunID
	ELSE
	BEGIN

		DELETE c
		FROM dbo.Charges c
		WHERE billableDate BETWEEN @startDate AND @endDate
		AND hotelCode = ISNULL(@hotelCode, hotelCode)
		AND confirmationNumber = ISNULL(@confirmationNumber, confirmationNumber)
		AND c.sopNumber IS NULL
		AND transactionSourceID = 3 --delete iprefer charge for recalculating summary manual point credit
									--This is temparory solution to avoid big change on Calculate_StandardCharges proc. Will fix this bug later.

		EXEC work.Calculate_StandardCharges @RunID
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Calculate_StandardCharges complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- Calculate thresholds -----------------------------------------------------
	IF @runType = 0
		EXEC test.Calculate_ThresholdCharges @RunID
	ELSE
		EXEC work.Calculate_ThresholdCharges @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Calculate_ThresholdCharges complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- Subtract Booking fees from Commission fees--------------------------------
	EXEC work.handleCommissions @RunID, @RunType

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': handleCommissions complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------
	
	IF @runType = 1
	BEGIN
		EXEC work.LoadBillyErrorTable @RunID
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': LoadBillyErrorTable complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT

	-- SET RUN TO COMPLETED -------------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 2 
	WHERE runID = @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': execution complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT

	RETURN @runID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
