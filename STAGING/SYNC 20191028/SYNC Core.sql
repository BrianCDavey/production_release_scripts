/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Core    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Core

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/28/2019 2:57:04 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[loadSynxisAreaInfo]'
GO
DROP PROCEDURE [dbo].[loadSynxisAreaInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[loadPegasusAreaInfo]'
GO
DROP PROCEDURE [dbo].[loadPegasusAreaInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[loadHistoricPegasusAreaInfo]'
GO
DROP PROCEDURE [dbo].[loadHistoricPegasusAreaInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[IPrefer_Memeber_Card_Request_Report]'
GO
DROP PROCEDURE [dbo].[IPrefer_Memeber_Card_Request_Report]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[FCM_APR2018]'
GO
DROP TABLE [dbo].[FCM_APR2018]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[ffGuestNameTest]'
GO
DROP PROCEDURE [dbo].[ffGuestNameTest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[selBookingParameters]'
GO
DROP PROCEDURE [dbo].[selBookingParameters]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[JayGroupHotelReport]'
GO
DROP PROCEDURE [dbo].[JayGroupHotelReport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[hotelMCMStoCore]'
GO
DROP PROCEDURE [dbo].[hotelMCMStoCore]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[hotelsReporting]'
GO




ALTER VIEW [dbo].[hotelsReporting]
AS
SELECT	dbo.hotels.crmGuid, 		dbo.hotels.code, 
		dbo.hotels_Synxis.synxisID, 
		dbo.hotels.hotelName, 
		dbo.hotels.physicalAddress1, 
		dbo.hotels.physicalAddress2, 
		dbo.hotels.physicalCity, 
		dbo.hotels.state, 
		account.phg_stateprovinceidname AS subAreaName,
		dbo.hotels.country, 
		account.phg_countryidname AS shortName,
		dbo.hotels.postalCode, 
		CAST(dbo.hotels.geoLocation AS nvarchar(MAX)) AS geoLocationWKT, 
		dbo.hotels.yearBuilt, 
		dbo.hotels.yearLastRenovated, 
		dbo.hotels.floors, 
		dbo.hotels.primaryPhone, 
		dbo.hotels.primaryEmail, 
		dbo.hotels.primaryWebsite, 
		dbo.hotels.openingDate, 
		dbo.hotels.totalRooms, 
		dbo.hotels.currencyCode, 
		dbo.hotels.competitiveSet, 
		dbo.hotels.geographicRegionCode, 
		dbo.hotels.geographicRegionCode AS geographicRegionName, 
		COALESCE (activeBrands.code, inactiveBrands.code) AS mainBrandCode, 
		COALESCE (account.phg_regionalmanageridName, N'None') AS phg_regionalmanageridName, 
		COALESCE (account.phg_areamanageridName, N'None') AS phg_areamanageridName, 
		account.statuscodename, 
		COALESCE (account.phg_revenueaccountmanageridName, N'None') AS phg_revenueaccountmanageridName, 
		COALESCE (account.phg_regionaladministrationidName, N'None') AS phg_regionaladministrationidName, 
		account.PHG_InvoicePastDueStatus, 
		COALESCE (account.phg_accountdirectoridName, N'None') AS phg_AccountManagerName,
		COALESCE (account.phg_marketingcontactidName, N'None') AS phg_marketingcontactidName, 
		COALESCE (account.phg_accountsreceivablecontactidName, N'None') AS phg_accountsreceivablecontactidName,
		COALESCE (account.phg_ipreferexecutiveadminidName, N'None') as phg_ipreferexecutiveadminidName,
		COALESCE (account.PHG_FinanceRegion, N'None') as PHG_FinanceRegion,
		dbo.hotels_OpenHospitality.openHospitalityCode
FROM	dbo.hotels 
LEFT OUTER JOIN  (	SELECT	dbo.hotels_brands.hotelCode, MIN(subBrands.heirarchy) AS mainHeirarchy
					FROM	dbo.hotels_brands INNER JOIN
                                                   dbo.brands AS subBrands ON dbo.hotels_brands.brandCode = subBrands.code
                            WHERE      (dbo.hotels_brands.endDatetime IS NULL) AND (dbo.hotels_brands.startDatetime <= GETDATE()) OR
                                                   (GETDATE() BETWEEN dbo.hotels_brands.startDatetime AND dbo.hotels_brands.endDatetime)
                            GROUP BY dbo.hotels_brands.hotelCode) AS hotelActiveBrands ON dbo.hotels.code = hotelActiveBrands.hotelCode LEFT OUTER JOIN
                      dbo.brands AS activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy LEFT OUTER JOIN
                          (SELECT     hotelCode, MIN(heirarchy) AS mainHeirarchy
                            FROM          (SELECT     hotels_brands_2.hotelCode, hotels_brands_2.brandCode, dbo.brands.heirarchy
                                                    FROM          dbo.hotels_brands AS hotels_brands_2 INNER JOIN
                                                                               (SELECT     hotelCode, MAX(endDatetime) AS maxEnd
                                                                                 FROM          dbo.hotels_brands AS hotels_brands_1
                                                                                 GROUP BY hotelCode) AS maxDate ON hotels_brands_2.endDatetime = maxDate.maxEnd INNER JOIN
                                                                           dbo.brands ON hotels_brands_2.brandCode = dbo.brands.code
                                                    GROUP BY hotels_brands_2.hotelCode, hotels_brands_2.brandCode, dbo.brands.heirarchy) AS maxBrands
                            GROUP BY hotelCode) AS hotelInactiveBrands ON dbo.hotels.code = hotelInactiveBrands.hotelCode LEFT OUTER JOIN
                      dbo.brands AS inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy LEFT OUTER JOIN
                      LocalCRM.dbo.Account AS account ON dbo.hotels.crmGuid = account.AccountId LEFT OUTER JOIN
                      dbo.hotels_OpenHospitality ON dbo.hotels.code = dbo.hotels_OpenHospitality.hotelCode LEFT OUTER JOIN
                      dbo.hotels_Synxis ON dbo.hotels.code = dbo.hotels_Synxis.hotelCode 
                      LEFT OUTER JOIN
                      dbo.geographicRegions ON dbo.hotels.geographicRegionCode = dbo.geographicRegions.code











GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[FindHotelByRes]'
GO



CREATE procedure [Reporting].[FindHotelByRes]
	@ResidencesOnly bit =0
as
-- added IF clause for Residences only radio button

IF @ResidencesOnly = 1
BEGIN
	SELECT DISTINCT Core.dbo.hotels_Synxis.synxisID, Core.dbo.hotelsWKT.hotelName
	FROM            Core.dbo.hotelsWKT INNER JOIN
				        Core.dbo.hotels_Synxis ON Core.dbo.hotelsWKT.code = Core.dbo.hotels_Synxis.hotelCode
					JOIN Core.dbo.residencesRoomTypes ON Core.dbo.residencesRoomTypes.synxisId = Core.dbo.hotels_Synxis.synxisID
	ORDER BY Core.dbo.hotelsWKT.hotelName
END
ELSE
BEGIN
	SELECT        Core.dbo.hotels_Synxis.synxisID, Core.dbo.hotelsWKT.hotelName
	FROM            Core.dbo.hotelsWKT INNER JOIN
				        Core.dbo.hotels_Synxis ON Core.dbo.hotelsWKT.code = Core.dbo.hotels_Synxis.hotelCode
	ORDER BY Core.dbo.hotelsWKT.hotelName
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[HotelLookup]'
GO





CREATE procedure [Reporting].[HotelLookup]
	@ResidencesOnly bit,
	@HotelID int
as

-- Residences only added and logo

SELECT        TOP (1) HS.synxisID, HW.mainBrandCode, HW.hotelName, 
                         CASE HW.mainBrandCode WHEN 'PH' THEN '#000000' 
						 WHEN 'XL' THEN '#660066' 
						 WHEN 'WR' THEN '#669999' 
						 WHEN 'BC' THEN '#660000' 
						 WHEN 'HE' THEN '#69172D' 
						 ELSE '#000000' END AS primaryColor, 
                         CASE WHEN (@ResidencesOnly = 1 OR
                         HW.mainBrandCode IN ('PRR', 'PSR')) 
                         THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/residenceLogo.jpg' 
						 WHEN HW.mainBrandCode = 'HE' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/Historic_cmyk_SM.jpg' 
						 ELSE 'http://chi-wb-pr-01:8080/WorkbookLogos/SRSphotels.png'
                          END AS logoURL
FROM            Core.dbo.hotelsWKT AS HW INNER JOIN
                         Core.dbo.hotels_Synxis AS HS ON HW.code = HS.hotelCode
WHERE        (HS.synxisID = @HotelID)



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[HotelLookupByCode]'
GO




CREATE procedure [Reporting].[HotelLookupByCode]
	@HotelCode varchar(20)
as

SELECT	HS.synxisID
,		HW.mainBrandCode
,		HW.hotelName
,		(	CASE	HW.mainBrandCode
				WHEN 'PH' THEN '#000000'
				WHEN 'XL' THEN '#660066'
				WHEN 'WR' THEN '#669999'
				WHEN 'BC' THEN '#660000'
				WHEN 'HE' THEN '#000066'
				ELSE '#000000'
			END
		) AS primaryColor
,		(	CASE	HW.mainBrandCode
			WHEN 'PH' THEN '#000000'
				WHEN 'XL' THEN '#000000'
				WHEN 'WR' THEN '#000000'
				WHEN 'BC' THEN '#000000'
				WHEN 'HE' THEN '#000000'
				ELSE '#000000'
			END
		) AS secondaryColor
,		(	CASE	HW.mainBrandCode
				WHEN 'PH' THEN '#999966'
				WHEN 'XL' THEN '#CCCCCC'
				WHEN 'WR' THEN '#CCCCCC'
				WHEN 'BC' THEN '#CCCCCC'
				WHEN 'HE' THEN '#FFFFCC'
				ELSE '#999966'
			END
		) AS tertiaryColor
,		(	CASE	HW.mainBrandCode
				WHEN 'PH' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/SRSphotels.png'
				WHEN 'XL' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/SRSxl.jpg'
				WHEN 'WR' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/SRSwr.jpg'
				WHEN 'BC' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/SRSbc.jpg'
				WHEN 'HE' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/SRShe.jpg'
				--WHEN 'RES' THEN 'http://chi-wb-pr-01:8080/WorkbookLogos/' waiting to see link name then will be complete, just need to change to proc
				ELSE 'http://chi-wb-pr-01:8080/WorkbookLogos/SRSphotels.png'
			END
		) AS logoURL

FROM		Core.dbo.hotelsWKT HW
		JOIN
		Core.dbo.hotels_Synxis HS
		ON
		HW.code = HS.hotelCode
			
WHERE		HW.code = @hotelCode

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iata_prepIataGroup]'
GO

CREATE PROCEDURE [dbo].[iata_prepIataGroup]
	
	@travelAgentGroupId int
,	@endDate date

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE	Core.dbo.travelAgentIds_travelAgentGroups
	SET		endDate = @endDate
	WHERE	travelAgentGroupID = @travelAgentGroupID
			AND
			endDate > @endDate
END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[MonthlyAggrigateByHotel]'
GO





CREATE procedure [Reporting].[MonthlyAggrigateByHotel]
	@ResidencesOnly bit,
	@HotelID varchar(20),
	@Month int,
	@Year int
as
-- added IF clause to cover the Residences only radio button

IF @ResidencesOnly = 1
BEGIN


SELECT hotelsWKT.code, hotelsWKT.hotelName, 
substring((SELECT ', ' + brandCode
		   FROM Core.dbo.brands JOIN Core.dbo.hotels_brands ON brands.code = hotels_brands.brandCode
		   WHERE hotels_brands.hotelCode = hotelsWKT.code 
		     AND GETDATE() BETWEEN ISNULL(hotels_brands.startDatetime, '1900-1-1') AND ISNULL(hotels_brands.endDatetime, '2100-12-31')
		   ORDER BY brands.heirarchy  
		FOR XML Path('')),3,8000) AS brandCodes, hotelsWKT.physicalCity, subAreas.subAreaName AS state, countries.shortName AS country,
COALESCE (Account.phg_regionalmanageridName, N'None') AS 'RD',
COALESCE (Account.phg_revenueaccountmanageridName, N'None') AS 'RAM', hotelsWKT.totalRooms, 
account.phg_pmsprovidername AS 'PMS',
substring((SELECT ', ' + iTools.phg_iinterfacetypename
		   FROM Core.dbo.hotelswkt 
				JOIN COre.dbo.hotels_Synxis ON hotelswkt.code = hotels_synxis.hotelcode
				JOIN LocalCRM.dbo.iTools ON hotelsWKT.crmGuid = itools.phg_accountid
														AND itools.phg_itooltypename = 'Interface'
														AND GETDATE() BETWEEN COALESCE(itools.phg_startdate, '1900-1-1') and COALESCE(itools.phg_enddate, '2100-12-31')
				WHERE hotels_Synxis.synxisID = @HotelID
		   ORDER BY iTools.phg_iinterfacetypename
		FOR XML Path('')),3,8000) AS 'IInterface',
account.phg_ibecompanyname AS 'IBE',
SUM(ThisYearMonth.TotalRoomNights) AS TotalRoomNightsMonth, SUM(ThisYearMonth.TotalReservations) AS TotalReservationsMonth, 
             SUM(ThisYearMonth.TotalRevenueUSD) AS TotalRevenueUSDMonth, Superset.dbo.Average(SUM(ThisYearMonth.TotalRevenueUSD), 
             SUM(ThisYearMonth.TotalRoomNights)) AS ADRMonth, Superset.dbo.Average(SUM(ThisYearMonth.TotalRoomNights), 
             SUM(ThisYearMonth.TotalReservations)) AS ALOSMonth, Superset.dbo.Average(SUM(ThisYearMonth.TotalLeadTime), SUM(ThisYearMonth.TotalReservations)) AS AverageLeadTimeMonth, 
             SUM(LastYearMonth.TotalRoomNights) AS LastYearTotalRoomNightsMonth, SUM(LastYearMonth.TotalReservations) AS LastYearTotalReservationsMonth, 
             SUM(LastYearMonth.TotalRevenueUSD) AS LastYearTotalRevenueUSDMonth, Superset.dbo.Average(SUM(LastYearMonth.TotalRevenueUSD), 
             SUM(LastYearMonth.TotalRoomNights)) AS LastYearADRMonth, Superset.dbo.Average(SUM(LastYearMonth.TotalRoomNights), SUM(LastYearMonth.TotalReservations)) AS LastYearALOSMonth, 
             Superset.dbo.Average(SUM(LastYearMonth.TotalLeadTime), SUM(LastYearMonth.TotalReservations)) AS LastYearAverageLeadTimeMonth, 
             Superset.dbo.PercentChange(SUM(ThisYearMonth.TotalRoomNights), SUM(LastYearMonth.TotalRoomNights)) AS PctChgTotalRoomNightsMonth, 
             Superset.dbo.PercentChange(SUM(ThisYearMonth.TotalReservations), SUM(LastYearMonth.TotalReservations)) AS PctChgTotalReservationsMonth, 
             Superset.dbo.PercentChange(SUM(ThisYearMonth.TotalRevenueUSD), SUM(LastYearMonth.TotalRevenueUSD)) AS PctChgTotalRevenueUSDMonth, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearMonth.TotalRevenueUSD), SUM(ThisYearMonth.TotalRoomNights)), 
             Superset.dbo.Average(SUM(LastYearMonth.TotalRevenueUSD), SUM(LastYearMonth.TotalRoomNights))) AS PctChgADRMonth, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearMonth.TotalRoomNights), SUM(ThisYearMonth.TotalReservations)), 
             Superset.dbo.Average(SUM(LastYearMonth.TotalRoomNights), SUM(LastYearMonth.TotalReservations))) AS PctChgALOSMonth, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearMonth.TotalLeadTime), SUM(ThisYearMonth.TotalReservations)), Superset.dbo.Average(SUM(LastYearMonth.TotalLeadTime), 
             SUM(LastYearMonth.TotalReservations))) AS PctChgLeadTimeMonth, SUM(ThisYearOverDate.TotalRoomNights) AS TotalRoomNightsYearOverDate, 
             SUM(ThisYearOverDate.TotalReservations) AS TotalReservationsYearOverDate, SUM(ThisYearOverDate.TotalRevenueUSD) AS TotalRevenueUSDYearOverDate, 
             Superset.dbo.Average(SUM(ThisYearOverDate.TotalRevenueUSD), SUM(ThisYearOverDate.TotalRoomNights)) AS ADRYearOverDate, 
             Superset.dbo.Average(SUM(ThisYearOverDate.TotalRoomNights), SUM(ThisYearOverDate.TotalReservations)) AS ALOSYearOverDate, 
             Superset.dbo.Average(SUM(ThisYearOverDate.TotalLeadTime), SUM(ThisYearOverDate.TotalReservations)) AS AverageLeadTimeYearOverDate, 
             SUM(LastYearOverDate.TotalRoomNights) AS LastYearTotalRoomNightsYearOverDate, SUM(LastYearOverDate.TotalReservations) 
             AS LastYearTotalReservationsYearOverDate, SUM(LastYearOverDate.TotalRevenueUSD) AS LastYearTotalRevenueUSDYearOverDate, 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRevenueUSD), SUM(LastYearOverDate.TotalRoomNights)) AS LastYearADRYearOverDate, 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRoomNights), SUM(LastYearOverDate.TotalReservations)) AS LastYearALOSYearOverDate, 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalLeadTime), SUM(LastYearOverDate.TotalReservations)) AS LastYearAverageLeadTimeYearOverDate, 
             Superset.dbo.PercentChange(SUM(ThisYearOverDate.TotalRoomNights), SUM(LastYearOverDate.TotalRoomNights)) AS PctChgTotalRoomNightsYearOverDate, 
             Superset.dbo.PercentChange(SUM(ThisYearOverDate.TotalReservations), SUM(LastYearOverDate.TotalReservations)) AS PctChgTotalReservationsYearOverDate, 
             Superset.dbo.PercentChange(SUM(ThisYearOverDate.TotalRevenueUSD), SUM(LastYearOverDate.TotalRevenueUSD)) AS PctChgTotalRevenueUSDYearOverDate, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearOverDate.TotalRevenueUSD), SUM(ThisYearOverDate.TotalRoomNights)), 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRevenueUSD), SUM(LastYearOverDate.TotalRoomNights))) AS PctChgADRYearOverDate, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearOverDate.TotalRoomNights), SUM(ThisYearOverDate.TotalReservations)), 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRoomNights), SUM(LastYearOverDate.TotalReservations))) AS PctChgALOSYearOverDate, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearOverDate.TotalLeadTime), SUM(ThisYearOverDate.TotalReservations)), 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalLeadTime), SUM(LastYearOverDate.TotalReservations))) AS PctChgLeadTimeYearOverDate
             
FROM			Core.dbo.hotels_Synxis
				LEFT JOIN
				Core.dbo.hotelsWKT ON hotels_synxis.hotelcode = hotelsWKT.code
				LEFT JOIN
				LocalCRM.dbo.Account ON hotelsWKT.crmGuid = Account.accountId
				LEFT JOIN
				ISO.dbo.subAreas ON hotelsWKT.country = subAreas.countryCode2 AND hotelsWKT.state = subAreas.subAreaCode
				LEFT JOIN
				ISO.dbo.countries ON hotelsWKT.country = countries.code2
				LEFT JOIN
				(
					SELECT  MONTH(arrivalDate) AS ArrivalMonth
					,		hotelID
					FROM	Superset.dbo.mostrecenttransactionsreporting
					WHERE   (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND 
							(MONTH(arrivalDate) BETWEEN 1 AND @Month) AND (YEAR(arrivalDate) BETWEEN @Year - 1 AND @Year)
					GROUP BY MONTH(arrivalDate), hotelID
                ) AS AllList
					ON hotels_synxis.synxisid = AllList.hotelID
                LEFT OUTER JOIN
				(
					SELECT      YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM        Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_2
					JOIN Core.dbo.residencesRoomTypes as res ON mostrecenttransactions_2.hotelID = res.synxisId
					and mostrecenttransactions_2.roomTypeCode = res.roomTypeCode
					WHERE       (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) = @Month) AND (YEAR(arrivalDate) = @Year)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS ThisYearMonth
					ON ThisYearMonth.ArrivalMonth = @Month AND 
						AllList.ArrivalMonth = ThisYearMonth.ArrivalMonth
				LEFT OUTER JOIN
                (
					SELECT		YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM		Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_1
					JOIN Core.dbo.residencesRoomTypes as res ON mostrecenttransactions_1.hotelID = res.synxisId
					and mostrecenttransactions_1.roomTypeCode = res.roomTypeCode
					WHERE       (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) = @Month) AND (YEAR(arrivalDate) = @Year - 1)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS LastYearMonth 
					ON LastYearMonth.ArrivalMonth = @Month AND 
                         AllList.ArrivalMonth = LastYearMonth.ArrivalMonth LEFT OUTER JOIN
				(
					SELECT		YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM		Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_2
					JOIN Core.dbo.residencesRoomTypes as res ON mostrecenttransactions_2.hotelID = res.synxisId
					and mostrecenttransactions_2.roomTypeCode = res.roomTypeCode
					WHERE       (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) BETWEEN 1 AND @Month) AND (YEAR(arrivalDate) = @Year)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS ThisYearOverDate 
					ON AllList.ArrivalMonth = ThisYearOverDate.ArrivalMonth 
				LEFT OUTER JOIN
				(
					SELECT		YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM		Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_1
					JOIN Core.dbo.residencesRoomTypes as res ON mostrecenttransactions_1.hotelID = res.synxisId
					and mostrecenttransactions_1.roomTypeCode = res.roomTypeCode
					WHERE		(status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) BETWEEN 1 AND @Month) AND (YEAR(arrivalDate) = @Year - 1)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS LastYearOverDate 
					ON AllList.ArrivalMonth = LastYearOverDate.ArrivalMonth

WHERE hotels_Synxis.synxisID = @HotelID						

GROUP BY	hotelsWKT.code
,			hotelsWKT.hotelName
,			hotelsWKT.physicalCity
,			subAreas.subAreaName
,			countries.shortName
,			COALESCE (Account.phg_regionalmanageridName, N'None')
,			COALESCE (Account.phg_revenueaccountmanageridName, N'None')
,			hotelsWKT.totalRooms
,			Account.phg_pmsprovidername
,			Account.phg_ibecompanyname

END
ELSE
BEGIN


  SELECT hotelsWKT.code, hotelsWKT.hotelName, 
substring((SELECT ', ' + brandCode
		   FROM Core.dbo.brands JOIN Core.dbo.hotels_brands ON brands.code = hotels_brands.brandCode
		   WHERE hotels_brands.hotelCode = hotelsWKT.code 
		     AND GETDATE() BETWEEN ISNULL(hotels_brands.startDatetime, '1900-1-1') AND ISNULL(hotels_brands.endDatetime, '2100-12-31')
		   ORDER BY brands.heirarchy  
		FOR XML Path('')),3,8000) AS brandCodes, hotelsWKT.physicalCity, subAreas.subAreaName AS state, countries.shortName AS country,
COALESCE (Account.phg_regionalmanageridName, N'None') AS 'RD',
COALESCE (Account.phg_revenueaccountmanageridName, N'None') AS 'RAM', hotelsWKT.totalRooms, 
account.phg_pmsprovidername AS 'PMS',
substring((SELECT ', ' + iTools.phg_iinterfacetypename
		   FROM Core.dbo.hotelswkt 
				JOIN COre.dbo.hotels_Synxis ON hotelswkt.code = hotels_synxis.hotelcode
				JOIN LocalCRM.dbo.iTools ON hotelsWKT.crmGuid = itools.phg_accountid
														AND itools.phg_itooltypename = 'Interface'
														AND GETDATE() BETWEEN COALESCE(itools.phg_startdate, '1900-1-1') and COALESCE(itools.phg_enddate, '2100-12-31')
				WHERE hotels_Synxis.synxisID = @HotelID
		   ORDER BY iTools.phg_iinterfacetypename
		FOR XML Path('')),3,8000) AS 'IInterface',
account.phg_ibecompanyname AS 'IBE',
SUM(ThisYearMonth.TotalRoomNights) AS TotalRoomNightsMonth, SUM(ThisYearMonth.TotalReservations) AS TotalReservationsMonth, 
             SUM(ThisYearMonth.TotalRevenueUSD) AS TotalRevenueUSDMonth, Superset.dbo.Average(SUM(ThisYearMonth.TotalRevenueUSD), 
             SUM(ThisYearMonth.TotalRoomNights)) AS ADRMonth, Superset.dbo.Average(SUM(ThisYearMonth.TotalRoomNights), 
             SUM(ThisYearMonth.TotalReservations)) AS ALOSMonth, Superset.dbo.Average(SUM(ThisYearMonth.TotalLeadTime), SUM(ThisYearMonth.TotalReservations)) AS AverageLeadTimeMonth, 
             SUM(LastYearMonth.TotalRoomNights) AS LastYearTotalRoomNightsMonth, SUM(LastYearMonth.TotalReservations) AS LastYearTotalReservationsMonth, 
             SUM(LastYearMonth.TotalRevenueUSD) AS LastYearTotalRevenueUSDMonth, Superset.dbo.Average(SUM(LastYearMonth.TotalRevenueUSD), 
             SUM(LastYearMonth.TotalRoomNights)) AS LastYearADRMonth, Superset.dbo.Average(SUM(LastYearMonth.TotalRoomNights), SUM(LastYearMonth.TotalReservations)) AS LastYearALOSMonth, 
             Superset.dbo.Average(SUM(LastYearMonth.TotalLeadTime), SUM(LastYearMonth.TotalReservations)) AS LastYearAverageLeadTimeMonth, 
             Superset.dbo.PercentChange(SUM(ThisYearMonth.TotalRoomNights), SUM(LastYearMonth.TotalRoomNights)) AS PctChgTotalRoomNightsMonth, 
             Superset.dbo.PercentChange(SUM(ThisYearMonth.TotalReservations), SUM(LastYearMonth.TotalReservations)) AS PctChgTotalReservationsMonth, 
             Superset.dbo.PercentChange(SUM(ThisYearMonth.TotalRevenueUSD), SUM(LastYearMonth.TotalRevenueUSD)) AS PctChgTotalRevenueUSDMonth, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearMonth.TotalRevenueUSD), SUM(ThisYearMonth.TotalRoomNights)), 
             Superset.dbo.Average(SUM(LastYearMonth.TotalRevenueUSD), SUM(LastYearMonth.TotalRoomNights))) AS PctChgADRMonth, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearMonth.TotalRoomNights), SUM(ThisYearMonth.TotalReservations)), 
             Superset.dbo.Average(SUM(LastYearMonth.TotalRoomNights), SUM(LastYearMonth.TotalReservations))) AS PctChgALOSMonth, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearMonth.TotalLeadTime), SUM(ThisYearMonth.TotalReservations)), Superset.dbo.Average(SUM(LastYearMonth.TotalLeadTime), 
             SUM(LastYearMonth.TotalReservations))) AS PctChgLeadTimeMonth, SUM(ThisYearOverDate.TotalRoomNights) AS TotalRoomNightsYearOverDate, 
             SUM(ThisYearOverDate.TotalReservations) AS TotalReservationsYearOverDate, SUM(ThisYearOverDate.TotalRevenueUSD) AS TotalRevenueUSDYearOverDate, 
             Superset.dbo.Average(SUM(ThisYearOverDate.TotalRevenueUSD), SUM(ThisYearOverDate.TotalRoomNights)) AS ADRYearOverDate, 
             Superset.dbo.Average(SUM(ThisYearOverDate.TotalRoomNights), SUM(ThisYearOverDate.TotalReservations)) AS ALOSYearOverDate, 
             Superset.dbo.Average(SUM(ThisYearOverDate.TotalLeadTime), SUM(ThisYearOverDate.TotalReservations)) AS AverageLeadTimeYearOverDate, 
             SUM(LastYearOverDate.TotalRoomNights) AS LastYearTotalRoomNightsYearOverDate, SUM(LastYearOverDate.TotalReservations) 
             AS LastYearTotalReservationsYearOverDate, SUM(LastYearOverDate.TotalRevenueUSD) AS LastYearTotalRevenueUSDYearOverDate, 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRevenueUSD), SUM(LastYearOverDate.TotalRoomNights)) AS LastYearADRYearOverDate, 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRoomNights), SUM(LastYearOverDate.TotalReservations)) AS LastYearALOSYearOverDate, 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalLeadTime), SUM(LastYearOverDate.TotalReservations)) AS LastYearAverageLeadTimeYearOverDate, 
             Superset.dbo.PercentChange(SUM(ThisYearOverDate.TotalRoomNights), SUM(LastYearOverDate.TotalRoomNights)) AS PctChgTotalRoomNightsYearOverDate, 
             Superset.dbo.PercentChange(SUM(ThisYearOverDate.TotalReservations), SUM(LastYearOverDate.TotalReservations)) AS PctChgTotalReservationsYearOverDate, 
             Superset.dbo.PercentChange(SUM(ThisYearOverDate.TotalRevenueUSD), SUM(LastYearOverDate.TotalRevenueUSD)) AS PctChgTotalRevenueUSDYearOverDate, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearOverDate.TotalRevenueUSD), SUM(ThisYearOverDate.TotalRoomNights)), 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRevenueUSD), SUM(LastYearOverDate.TotalRoomNights))) AS PctChgADRYearOverDate, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearOverDate.TotalRoomNights), SUM(ThisYearOverDate.TotalReservations)), 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalRoomNights), SUM(LastYearOverDate.TotalReservations))) AS PctChgALOSYearOverDate, 
             Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYearOverDate.TotalLeadTime), SUM(ThisYearOverDate.TotalReservations)), 
             Superset.dbo.Average(SUM(LastYearOverDate.TotalLeadTime), SUM(LastYearOverDate.TotalReservations))) AS PctChgLeadTimeYearOverDate
             
FROM			Core.dbo.hotels_Synxis
				LEFT JOIN
				Core.dbo.hotelsWKT ON hotels_synxis.hotelcode = hotelsWKT.code
				LEFT JOIN
				LocalCRM.dbo.Account ON hotelsWKT.crmGuid = Account.accountId
				LEFT JOIN
				ISO.dbo.subAreas ON hotelsWKT.country = subAreas.countryCode2 AND hotelsWKT.state = subAreas.subAreaCode
				LEFT JOIN
				ISO.dbo.countries ON hotelsWKT.country = countries.code2
				LEFT JOIN
				(
					SELECT  MONTH(arrivalDate) AS ArrivalMonth
					,		hotelID
					FROM	Superset.dbo.mostrecenttransactionsreporting
					WHERE   (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND 
							(MONTH(arrivalDate) BETWEEN 1 AND @Month) AND (YEAR(arrivalDate) BETWEEN @Year - 1 AND @Year)
					GROUP BY MONTH(arrivalDate), hotelID
                ) AS AllList
					ON hotels_synxis.synxisid = AllList.hotelID
                LEFT OUTER JOIN
				(
					SELECT      YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM        Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_2
					WHERE       (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) = @Month) AND (YEAR(arrivalDate) = @Year)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS ThisYearMonth
					ON ThisYearMonth.ArrivalMonth = @Month AND 
						AllList.ArrivalMonth = ThisYearMonth.ArrivalMonth
				LEFT OUTER JOIN
                (
					SELECT		YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM		Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_1
					WHERE       (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) = @Month) AND (YEAR(arrivalDate) = @Year - 1)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS LastYearMonth 
					ON LastYearMonth.ArrivalMonth = @Month AND 
                         AllList.ArrivalMonth = LastYearMonth.ArrivalMonth LEFT OUTER JOIN
				(
					SELECT		YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM		Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_2
					WHERE       (status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) BETWEEN 1 AND @Month) AND (YEAR(arrivalDate) = @Year)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS ThisYearOverDate 
					ON AllList.ArrivalMonth = ThisYearOverDate.ArrivalMonth 
				LEFT OUTER JOIN
				(
					SELECT		YEAR(arrivalDate) AS ArrivalYear, MONTH(arrivalDate) AS ArrivalMonth,
								SUM(roomNights) AS TotalRoomNights, COUNT(DISTINCT confirmationNumber) AS TotalReservations, 
								SUM(reservationRevenueUSD) AS TotalRevenueUSD, SUM(bookingLeadTime) AS TotalLeadTime
					FROM		Superset.dbo.mostrecenttransactionsreporting AS mostrecenttransactions_1
					WHERE		(status <> 'Cancelled') AND (channel <> 'PMS Rez Synch') AND (CROCode NOT LIKE 'HTL_%') AND hotelID = @HotelID AND
								(MONTH(arrivalDate) BETWEEN 1 AND @Month) AND (YEAR(arrivalDate) = @Year - 1)
					GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate)
				) AS LastYearOverDate 
					ON AllList.ArrivalMonth = LastYearOverDate.ArrivalMonth

WHERE hotels_Synxis.synxisID = @HotelID						

GROUP BY	hotelsWKT.code
,			hotelsWKT.hotelName
,			hotelsWKT.physicalCity
,			subAreas.subAreaName
,			countries.shortName
,			COALESCE (Account.phg_regionalmanageridName, N'None')
,			COALESCE (Account.phg_revenueaccountmanageridName, N'None')
,			hotelsWKT.totalRooms
,			Account.phg_pmsprovidername
,			Account.phg_ibecompanyname

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iata_processIataNumber]'
GO

CREATE PROCEDURE [dbo].[iata_processIataNumber]
	
	@travelAgentGroupId int
,	@travelAgentId nvarchar(250)
,	@startDate date
,	@endDate date

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Add travel agent id if it doesn't exist
	IF NOT EXISTS (SELECT 1 FROM Core.dbo.travelAgentIds WHERE id = @travelAgentId)
		BEGIN
			INSERT INTO Core.dbo.travelAgentIds (id, travelAgentIdTypeId) VALUES (@travelAgentId, 2);
		END


	-- Set end date for all groups that use this IATA number
	UPDATE	Core.dbo.travelAgentIds_travelAgentGroups
	SET		endDate = @endDate
	WHERE	travelAgentId = @travelAgentId
			AND
			endDate > @endDate


	-- Set end date for IATA number to 2100-12-31 for the group or add the IATA number for the group if it doesn't already exist
	IF EXISTS (SELECT 1 FROM Core.dbo.travelAgentIds_travelAgentGroups WHERE travelAgentGroupId = @travelAgentGroupId AND travelAgentId = @travelAgentId)
		BEGIN
			UPDATE	Core.dbo.travelAgentIds_travelAgentGroups
			SET		endDate = '2100-12-31'
			WHERE	travelAgentGroupId = @travelAgentGroupId
					AND
					travelAgentId = @travelAgentId
		END
	ELSE
		BEGIN
			INSERT INTO Core.dbo.travelAgentIds_travelAgentGroups (travelAgentGroupId, travelAgentId, startDate, endDate)
				VALUES (@travelAgentGroupId, @travelAgentId, @startDate, '2100-12-31')
		END
END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sysdiagrams]'
GO
CREATE TABLE [dbo].[sysdiagrams]
(
[name] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[principal_id] [int] NOT NULL,
[diagram_id] [int] NOT NULL,
[version] [int] NULL,
[definition] [varbinary] (max) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
