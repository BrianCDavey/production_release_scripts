USE Reservations
GO

/*
Run this script on:

        CHI-SQ-ST-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/24/2019 9:24:07 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[ReservationInvoiceDetail]'
GO


-- =============================================
-- Author:		Jake Smith
-- Create date: 10/23/2019
-- Description:	Report to breakdown Billy charges
-- =============================================
CREATE PROCEDURE [rpt].[ReservationInvoiceDetail] 
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date,
	@hotelCode nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
YEAR(cd.arrivalDate) as ArrivalYear,
MONTH(cd.arrivalDate) as ArrivalMonth,
mrt.hotelID as SynxisID,
hr.statuscodename as HotelStatus,
cd.hotelCode as HotelCode,
hr.hotelName as HotelName,
cd.sopNumber as SopNumber,
cc.name as ChargeClass,
cd.chargeDescription as BillyChargeDescription,
cd.itemCode as BillyItemCode,
SUM(cd.chargeValueInUSD) as BillyChargeUSD,
COUNT(distinct cd.confirmationNumber) as Bookings,
SUM(cd.roomRevenueInUSD) as RoomRevenueUSD,
SUM(cd.roomNights) as RoomNights


  FROM [Superset].[dbo].[calculatedDebits] cd
  INNER JOIN Reservations.dbo.mostrecenttransactions mrt 
	on cd.confirmationNumber = mrt.confirmationNumber
INNER JOIN Hotels.dbo.hotelsReporting hr 
	on cd.hotelCode = hr.code
INNER JOIN BillingProduction.dbo.chargeClassifications cc
	on cd.classificationID = cc.id

WHERE cd.arrivalDate BETWEEN @startDate and @endDate
AND (@hotelCode is null OR cd.hotelCode in ( @hotelCode) )

GROUP BY YEAR(cd.arrivalDate),
MONTH(cd.arrivalDate),
mrt.hotelID,
hr.statuscodename,
cd.hotelCode,
hr.hotelName,
cd.sopNumber,
cc.name,
cd.chargeDescription,
cd.itemCode
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
