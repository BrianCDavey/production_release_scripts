USE Dimensional_Warehouse
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/29/2019 12:12:28 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [fact].[Group Sales Actual]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [fact].[Group Sales Actual] ADD
[ConsumedRevenue_USD] [numeric] (38, 4) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Group_Sales_Actual]'
GO


ALTER PROCEDURE [dbo].[Populate_Group_Sales_Actual]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_StatOrder
	AS
	(
		SELECT StatusCodeID,StatusCode_Desc,0 AS StatOrder FROM [GroupSales].[dbo].[RFP_StatusCode] WHERE StatusCode_Desc = 'Won'
			UNION ALL
		SELECT StatusCodeID,StatusCode_Desc,1 AS StatOrder FROM [GroupSales].[dbo].[RFP_StatusCode] WHERE StatusCode_Desc = 'Lost'
			UNION ALL
		SELECT StatusCodeID,StatusCode_Desc,ROW_NUMBER() OVER(ORDER BY StatusCodeID) + 1 AS StatOrder FROM [GroupSales].[dbo].[RFP_StatusCode] WHERE StatusCode_Desc NOT IN('Won','Lost')
	),
	cte_RFP
	AS
	(
		SELECT act.ActualID,rfp.[RFP_ID],rfp.[CRM_OpportunityID],rfp.[CloseDate],ROW_NUMBER() OVER(PARTITION BY rfp.[RFP_ID] ORDER BY sta.StatOrder) AS rowNum
		FROM GroupSales.[dbo].[RFP] rfp
			INNER JOIN [GroupSales].[dbo].[Actual] act ON act.RFP_ID = rfp.RFP_ID
			INNER JOIN cte_StatOrder sta ON sta.StatusCodeID = rfp.StatusCodeID
		WHERE act.ActualIsActive = 1
	)
	MERGE INTO [fact].[Group Sales Actual] AS tgt
	USING
	(
		SELECT DISTINCT a.[CRM_GroupSalesActualsID],ISNULL(a.[Actual_Name],'') AS [Actual_Name],
				ISNULL(ddCreate.dateKey,0) AS [CreateDate],ISNULL(ddStart.dateKey,0) AS [StartDate],ISNULL(ddEnd.dateKey,0) AS [EndDate],
				ISNULL(a.[ExchangeRate],0) AS [ExchangeRate],ISNULL(a.[ActualRevenue],0) AS [ActualRevenue],ISNULL(a.[ActualRoomNights],0) AS [ActualRoomNights],ISNULL(a.[FoodAndBeverageSpend],0) AS [FoodAndBeverageSpend],
				CASE a.[ActualIsActive] WHEN 0 THEN 'False' WHEN 1 THEN 'True' ELSE '' END AS [ActualIsActive],
				ISNULL(asr.[StatusReason_Desc],'') AS [StatusReason_Desc],ISNULL(h.[hotelKey],0) AS [hotelKey],ISNULL(dcur.[currencyKey],0) AS [currencyKey],ISNULL(drfp.[groupSalesRfpKey],0) AS [groupSalesRfpKey],
				ISNULL(ddClose.dateKey,0) AS CloseDate,
				ISNULL(cxClose.[USD Exchange Rate] * a.[ActualRevenue],0.0) AS ActualRevenue_USD,
				ISNULL(cxEnd.[USD Exchange Rate] * a.[ActualRevenue],0.0) AS ConsumedRevenue_USD
		FROM [GroupSales].[dbo].[Actual] a
			--INNER JOIN (SELECT [RFP_ID],MAX(VersionNumber) AS VersionNumber FROM [GroupSales].[dbo].[Actual] GROUP BY [RFP_ID]) u ON u.[RFP_ID] = a.[RFP_ID] AND u.VersionNumber = a.VersionNumber
			LEFT JOIN [GroupSales].[dbo].[Actual_StatusReason] asr ON asr.[StatusReasonID] = a.[StatusReasonID]
			LEFT JOIN [dim].[Hotel] h ON h.[sourceKey] = a.[Winning_Hotel_hotelID]
			LEFT JOIN GroupSales.[dbo].[Currency] cur ON cur.[CurrencyID] = a.[CurrencyID]
			LEFT JOIN [dim].[Currency] dcur ON dcur.[Currency Code] = cur.[Code]
			INNER JOIN cte_RFP rfp ON rfp.[RFP_ID] = a.[RFP_ID] AND rfp.ActualID = a.ActualID
			INNER JOIN [fact].[Group Sales RFP] drfp ON drfp.[CRM_OpportunityID] = rfp.[CRM_OpportunityID]
			LEFT JOIN [dim].[DimDate] ddCreate ON ddCreate.[Full Date] = a.[CreateDate]
			LEFT JOIN [dim].[DimDate] ddStart ON ddStart.[Full Date] = a.[StartDate]
			LEFT JOIN [dim].[DimDate] ddEnd ON ddEnd.[Full Date] = a.[EndDate]
			LEFT JOIN [dim].[DimDate] ddClose ON ddClose.[Full Date] = rfp.[CloseDate]
			LEFT JOIN fact.CurrencyExchange cxClose ON cxClose.DateKey = ddClose.dateKey AND cxClose.[currencyKey] = dcur.[currencyKey]
			LEFT JOIN fact.CurrencyExchange cxEnd ON cxEnd.DateKey = ddEnd.dateKey AND cxEnd.[currencyKey] = dcur.[currencyKey]
		WHERE asr.[StatusReason_Desc] != 'Invalid  DO NOT USE'
			AND rfp.rowNum = 1
	) AS src ON  src.[CRM_GroupSalesActualsID] = tgt.[CRM_GroupSalesActualsID] AND src.[hotelKey] = tgt.[Winning Hotel] AND src.[groupSalesRfpKey] = tgt.[groupSalesRfpKey]
	WHEN MATCHED THEN
		UPDATE
			SET [Actual Name] = src.[Actual_Name],
				[Create Date] = src.[CreateDate],
				[Start Date] = src.[StartDate],
				[End Date] = src.[EndDate],
				[Exchange Rate] = src.[ExchangeRate],
				[Actual Revenue] = src.[ActualRevenue],
				[Actual Room Nights] = src.[ActualRoomNights],
				[Food And Beverage Spend] = src.[FoodAndBeverageSpend],
				[Actual Is Active] = src.[ActualIsActive],
				[Actual Status Reason] = src.[StatusReason_Desc],
				[currencykey] = src.[currencyKey],
				[Close Date] = src.[CloseDate],
				[Actual Revenue USD] = src.ActualRevenue_USD,
				ConsumedRevenue_USD = src.ConsumedRevenue_USD
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([CRM_GroupSalesActualsID],[Actual Name],[Create Date],[Start Date],[End Date],[Exchange Rate],[Actual Revenue],[Actual Room Nights],[Food And Beverage Spend],
				[Actual Is Active],
				[Actual Status Reason],[Winning Hotel],[currencykey],[groupSalesRfpKey],[Close Date],[Actual Revenue USD],ConsumedRevenue_USD)
		VALUES(src.[CRM_GroupSalesActualsID],src.[Actual_Name],src.[CreateDate],src.[StartDate],src.[EndDate],src.[ExchangeRate],src.[ActualRevenue],src.[ActualRoomNights],src.[FoodAndBeverageSpend],
				src.[ActualIsActive],src.[StatusReason_Desc],src.[hotelKey],src.[currencyKey],src.[groupSalesRfpKey],src.[CloseDate],src.ActualRevenue_USD,ConsumedRevenue_USD)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
