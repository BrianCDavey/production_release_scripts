USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/3/2020 9:15:32 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CPIincrease]'
GO


-- =============================================
-- Author:		Ti Yao
-- Create date: 01/29/2020
-- Description:	Perform CPI percentage increases on all Booking and Commission fees for a hotel
-- =============================================
CREATE PROCEDURE [dbo].[CPIincrease] 
	-- Add the parameters for the stored procedure here
	@hotelCode nvarchar(10), 
	@percentage decimal(5,4),
	@startDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT INTO ReservationBilling.[dbo].BillingRules(
 [clauseID]
, [classificationID]
, [startDate]
, [endDate]
, [confirmationDate]
, [afterConfirmation]
, [currencyCode]
, [thresholdMinimum]
, [perReservationFlatFee]
, [perRoomNightFlatFee]
, [perReservationPercentage]
, [percentageMinimum]
, [percentageMaximum]
, [refundable]
, [lastBilled]
)
  SELECT  BR.[clauseID]
, [classificationID]
, @startDate
, [endDate]
, [confirmationDate]
, [afterConfirmation]
, [currencyCode]
, [thresholdMinimum]
, [perReservationFlatFee] + ([perReservationFlatFee] * @percentage)
, [perRoomNightFlatFee] 
, [perReservationPercentage]
, [percentageMinimum]
, [percentageMaximum]
, [refundable]
, BR.[lastBilled]
  FROM ReservationBilling.[dbo].[BillingRules] BR
  LEFT JOIN ReservationBilling.dbo.Clauses cl
  ON br.clauseID = cl.clauseID
  WHERE 
  hotelCode = @hotelCode
  AND endDate >= @startDate
  AND startDate < @startDate
  AND [perReservationFlatFee] > 0 
  
  UPDATE BR
  SET endDate = DATEADD(DAY,-1,@startDate)
  FROM ReservationBilling.[dbo].[BillingRules] BR
  LEFT JOIN ReservationBilling.dbo.Clauses cl
  ON br.clauseID = cl.clauseID
  WHERE 
  hotelCode = @hotelCode
  AND startDate < @startDate
  AND endDate >= @startDate
  AND perReservationFlatFee > 0   	
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
