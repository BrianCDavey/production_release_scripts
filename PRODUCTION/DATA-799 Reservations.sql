USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/11/2020 7:31:09 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[iPreferAutoEnrollReservations]'
GO



ALTER   PROCEDURE [rpt].[iPreferAutoEnrollReservations] 
	@byArrival bit = 0,
	@startDate date,
	@endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE & POPULATE #MRT ---------------------------------------------
	IF OBJECT_ID('tempdb..#MRT') IS NOT NULL
		DROP TABLE #MRT;

	CREATE TABLE #MRT
	(
		[confirmationNumber] [nvarchar](20) NOT NULL,
		[confirmationDate] [date] NULL,
		[arrivalDate] [date] NOT NULL,
		[HotelCode] [varchar](20) NULL,
		[HotelName] [nvarchar](250) NULL,
		[ratetypecode] [nvarchar](200) NULL,
		[salutation] [nvarchar](160) NULL,
		[guestFirstName] [nvarchar](255) NULL,
		[guestLastName] [nvarchar](255) NULL,
		[customerAddress1] [nvarchar](255) NULL,
		[customerAddress2] [nvarchar](255) NULL,
		[customerCity] [nvarchar](255) NULL,
		[customerState] [nvarchar](255) NULL,
		[customerPostalCode] [nvarchar](255) NULL,
		[customerPhone] [varchar](200) NULL,
		[customerCountry] [nvarchar](255) NULL,
		[customerEmailBlanked] [nvarchar](255) NULL,
		[existingIPreferMemberNumber] [nvarchar](50) NULL,
		[ChannelReportLabel] [nvarchar](50) NULL,
		[subSourceReportLabel] [nvarchar](50) NULL,
		[optIn] [bit] NULL
	)

	IF @byArrival = 1
	BEGIN
		INSERT INTO #MRT([confirmationNumber],[confirmationDate],[arrivalDate],[HotelCode],[HotelName],[ratetypecode],
						[salutation],[guestFirstName],[guestLastName],[customerAddress1],
						[customerAddress2],[customerCity],[customerState],
						[customerPostalCode],[customerPhone],[customerCountry],
						[customerEmailBlanked],[existingIPreferMemberNumber],
						[ChannelReportLabel],[subSourceReportLabel],[optIn])
		SELECT t.confirmationNumber,ts.confirmationDate,td.arrivalDate,hh.HotelCode,hh.HotelName,rc.RateCode AS ratetypecode,
			g.salutation,g.FirstName AS guestFirstName,g.LastName AS guestLastName,g.Address1 AS customerAddress1,
			g.Address2 AS customerAddress2,loc.City_Text AS customerCity,loc.State_Text AS customerState,
			loc.PostalCode_Text AS customerPostalCode,g.phone AS customerPhone,loc.Country_Text AS customerCountry,
			ge.emailAddress AS customerEmailBlanked,ISNULL(cp.iPrefer_Number,ln2.LoyaltyNumberName) AS existingIPreferMemberNumber,
			bs.ChannelReportLabel,bs.subSourceReportLabel,td.optIn
		FROM dbo.Transactions t
			INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			LEFT JOIN dbo.hotel h ON h.HotelID = t.HotelID
				LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
			LEFT JOIN dbo.RateCode rc ON rc.RateCodeID = t.RateCodeID
			LEFT JOIN dbo.Guest g ON g.GuestID = t.GuestID
				LEFT JOIN dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
				LEFT JOIN dbo.vw_Location loc ON loc.LocationID = g.LocationID
			LEFT JOIN dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
			LEFT JOIN Loyalty.dbo.Customer_Profile_Import cp ON ge.emailAddress = cp.Email
													AND LTRIM(RTRIM(ISNULL(cp.Email,''))) != ''
													AND LTRIM(RTRIM(ISNULL(cp.iPrefer_Number,''))) != ''
			LEFT JOIN dbo.vw_CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID

			LEFT JOIN Guests.dbo.Guest_EmailAddress ge2  with (nolock) on ge.emailAddress = ge2.emailAddress
			LEFT JOIN guests.dbo.Guest g2  with (nolock) on ge2.Guest_EmailAddressID = g2.Guest_EmailAddressID
			left JOIN Loyalty.dbo.LoyaltyNumber ln2 with (nolock) on ln2.LoyaltyNumberID = g2.LoyaltyNumberID
		WHERE td.arrivalDate BETWEEN @startDate AND @endDate
			AND ts.[status] <> 'Cancelled'
			AND NULLIF(ge.emailAddress,'') IS NOT NULL
			AND NULLIF(ln.loyaltyNumber,'') IS NULL
	END
	ELSE
	BEGIN
		INSERT INTO #MRT([confirmationNumber],[confirmationDate],[arrivalDate],[HotelCode],[HotelName],[ratetypecode],
						[salutation],[guestFirstName],[guestLastName],[customerAddress1],
						[customerAddress2],[customerCity],[customerState],
						[customerPostalCode],[customerPhone],[customerCountry],
						[customerEmailBlanked],[existingIPreferMemberNumber],
						[ChannelReportLabel],[subSourceReportLabel],[optIn])
		SELECT t.confirmationNumber,ts.confirmationDate,td.arrivalDate,hh.HotelCode,hh.HotelName,rc.RateCode AS ratetypecode,
			g.salutation,g.FirstName AS guestFirstName,g.LastName AS guestLastName,g.Address1 AS customerAddress1,
			g.Address2 AS customerAddress2,loc.City_Text AS customerCity,loc.State_Text AS customerState,
			loc.PostalCode_Text AS customerPostalCode,g.phone AS customerPhone,loc.Country_Text AS customerCountry,
			ge.emailAddress AS customerEmailBlanked,ISNULL(cp.iPrefer_Number,ln2.LoyaltyNumberName) AS existingIPreferMemberNumber,
			bs.ChannelReportLabel,bs.subSourceReportLabel,td.optIn
		FROM dbo.Transactions t
			INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			LEFT JOIN dbo.hotel h ON h.HotelID = t.HotelID
				LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
			LEFT JOIN dbo.RateCode rc ON rc.RateCodeID = t.RateCodeID
			LEFT JOIN dbo.Guest g ON g.GuestID = t.GuestID
				LEFT JOIN dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
				LEFT JOIN dbo.vw_Location loc ON loc.LocationID = g.LocationID
			LEFT JOIN dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
			LEFT JOIN Loyalty.dbo.Customer_Profile_Import cp ON ge.emailAddress = cp.Email
													AND LTRIM(RTRIM(ISNULL(cp.Email,''))) != ''
													AND LTRIM(RTRIM(ISNULL(cp.iPrefer_Number,''))) != ''
			LEFT JOIN dbo.vw_CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID

			LEFT JOIN Guests.dbo.Guest_EmailAddress ge2  with (nolock) on ge.emailAddress = ge2.emailAddress
			LEFT JOIN guests.dbo.Guest g2  with (nolock) on ge2.Guest_EmailAddressID = g2.Guest_EmailAddressID
			left JOIN Loyalty.dbo.LoyaltyNumber ln2 with (nolock) on ln2.LoyaltyNumberID = g2.LoyaltyNumberID
		WHERE ts.confirmationdate BETWEEN @startDate AND @endDate
			AND ts.[status] <> 'Cancelled'
			AND NULLIF(ge.emailAddress,'') IS NOT NULL
			AND NULLIF(ln.loyaltyNumber,'') IS NULL
	END
	-----------------------------------------------------------------------

	;WITH cte_MRT
	AS
	(
		SELECT mrt.confirmationdate,mrt.arrivalDate,mrt.confirmationNumber,mrt.ChannelReportLabel,mrt.HotelCode,mrt.HotelName,mrt.ratetypecode,
				mrt.salutation,mrt.guestFirstName,mrt.guestLastName,mrt.customerAddress1,
				mrt.customerAddress2,mrt.customerCity,mrt.customerState,
				mrt.customerPostalCode,mrt.customerPhone,mrt.customerCountry,mrt.customerEmailBlanked,
				mrt.existingIPreferMemberNumber
		FROM #MRT mrt
			LEFT JOIN Hotels.authority.IPrefer_Checkbox ip on mrt.hotelCode = ip.hotelCode
		WHERE 
			(
				--opted in Brand site bookings
				mrt.optIn = 1
				AND
				mrt.ChannelReportLabel = 'IBE - PHG'
				AND
				mrt.subSourceReportLabel NOT IN(
												'HHA Call Center',
												'HHA Call Center - iPrefer Partners on HHA.org',
												'www.historichotels.org',
												'www.historichotels.org-cro',
												'www.historichotels.org-flex',
												'www.historichotels.org-gcomi',
												'www.historichotels.org-partner',
												'www.historichotelsworldwide.com',
												'www.historichotelsworldwide.com-flex',
												'Active International',
												'LuxLink',
												'Sky Auction',
												'www.phgoffers.com-choice',
												'www.phgoffers.com-lion',
												'www.phgoffers.com-svc',
												'www.preferredhotelgroup.com-luxlink',
												'www.preferredhotelgroup.com-perx',
												'www.preferredhotelgroup.com-skyauction',
												'www.preferredhotelgroup.com-amex'
												)
			)
			OR (mrt.ChannelReportLabel = 'IBE - Hotel' AND mrt.optIn = 1 AND ip.hotelCode IS NOT NULL) --opted in Hotel site bookings
			OR (mrt.rateTypeCode = 'MKTIPM') --I Prefer member rate from any channel
	)
	SELECT confirmationdate,arrivalDate,confirmationNumber,ChannelReportLabel,HotelCode,HotelName,ratetypecode,
			salutation,guestFirstName,guestLastName,customerAddress1,
			customerAddress2,customerCity,customerState,
			customerPostalCode,customerPhone,customerCountry,customerEmailBlanked AS customerEmail,
			existingIPreferMemberNumber
	FROM cte_MRT
	ORDER BY arrivaldate asc

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
