USE ETL
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ETL    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 9/17/2024 5:09:30 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ETL_ApplicationImportStatus]'
GO


ALTER PROCEDURE [dbo].[ETL_ApplicationImportStatus]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_Q
	AS
	(
		SELECT [Application],COALESCE(ImportFinished,ImportStarted,CreateDate) AS RunDate,QueueStatus_Desc,ISNULL([Message],'Message is empty') AS [Message],
			DATEDIFF(hour,ImportStarted,ImportFinished) AS Q_Hours,DATEDIFF(minute,ImportStarted,ImportFinished) AS Q_Minutes,
			ROW_NUMBER() OVER(PARTITION BY Application ORDER BY COALESCE(ImportFinished,ImportStarted,CreateDate) DESC) AS rowNum
		FROM dbo.Queue
		
	),
	cte_Q2
	AS
	(
		SELECT [Application],RunDate,QueueStatus_Desc,
			[Message] + ISNULL(' (Time to Complete: ' + RIGHT('00' + CONVERT(varchar(20),Q_Hours),2) + ':' + RIGHT('00' + CONVERT(varchar(20),Q_Minutes - (60 * Q_Hours)),2) + ')','') AS [Message],
			rowNum
		FROM cte_Q
	),
	cte_Freq
	AS
	(
		SELECT 
    JobName,
    [Application],
    CONVERT(datetime, CONVERT(date, GETDATE())) + CONVERT(datetime, CONVERT(time, ETL_StartDate)) AS Daily_Freq,
    CONVERT(datetime,
            CONVERT(varchar(4), YEAR(GETDATE())) + '-' + 
            RIGHT('00' + CONVERT(varchar(2), MONTH(GETDATE())), 2) + '-' + 
            RIGHT('00' + CONVERT(varchar(2), DAY(ETL_StartDate)), 2)
    ) + CONVERT(datetime, CONVERT(time, ETL_StartDate)) AS Monthly_Freq,
    DATEADD(day, ((DATEDIFF(day, ETL_StartDate, GETDATE()) / 7) + 1) * 7, CAST(ETL_StartDate AS datetime)) AS Weekly_Freq,
    ETL_FrequencyType,
    ETL_FrequencyType_Desc,
    ETL_FrequencyInterval

FROM 
    dbo.ETL_Applications
WHERE 
    IsActive = 1

	),
	cte_Utility_Max_JH
	AS
	(
		SELECT instance_id,job_id,msdb.dbo.agent_datetime(run_date,run_time) AS RunTimeDate,
			RIGHT('00' + CONVERT(varchar(20),run_duration/10000),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration/100%100),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration%100),2) AS Run_Duration,
			ROW_NUMBER() OVER(PARTITION BY job_id ORDER BY msdb.dbo.agent_datetime(run_date,run_time) DESC) AS rowNum
		FROM [WUS2-DATA-UT-VM].msdb.dbo.sysjobhistory
		WHERE step_id = 0
	),
	cte_Utility
	AS
	(
		SELECT m1.RunTimeDate AS [Last Import Date],
			'WUS2_DATA_UT_VM: ' + j.name AS [Job Name],
			'UNKNOWN' AS [File Upload Status],
			NULL AS [Last Import Status],
			LEFT(jh.message,CHARINDEX('The last step to run',jh.message,1) -1) + ' (Time to Complete: ' + m1.Run_Duration + ')' AS [Last Import Message],
			NULL AS [Last Queue Count],
			NULL AS [Last Import Count],
			NULL AS [Last FinalCount Count],
			NULL AS [FilePath],
			'Every ' + CONVERT(varchar(20),DATEDIFF(hour,m2.RunTimeDate,m1.RunTimeDate)) + ' Hour(s)' AS [File Upload Interval],
			NULL AS [Next Import Date],
			NULL AS [DocumentationPath],j.job_id
		FROM [WUS2-DATA-UT-VM].msdb.dbo.sysjobs j
			INNER JOIN [WUS2-DATA-UT-VM].msdb.dbo.sysjobhistory jh ON jh.job_id = j.job_id
			INNER JOIN cte_Utility_Max_JH m1 ON m1.instance_id = jh.instance_id AND m1.rowNum = 1
			LEFt JOIN cte_Utility_Max_JH m2 ON m2.job_id = jh.job_id AND m2.rowNum = 2
		WHERE j.name IN('Key Club Reservations','CVENT - CVENT Data Pull / CRM Data Push','Swoogo CRM Push','Daily Rate Cache Import','CRM Synch')
			AND jh.step_id = 0
	),
	cte_AZSQZ01_Max_JH
	AS
	(
		SELECT instance_id,job_id,msdb.dbo.agent_datetime(run_date,run_time) AS RunTimeDate,
			RIGHT('00' + CONVERT(varchar(20),run_duration/10000),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration/100%100),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration%100),2) AS Run_Duration,
			ROW_NUMBER() OVER(PARTITION BY job_id ORDER BY msdb.dbo.agent_datetime(run_date,run_time) DESC) AS rowNum
		FROM [AZSQZ01].msdb.dbo.sysjobhistory
		WHERE step_id = 0
	),
	cte_AZSQZ01
	AS
	(
		SELECT m1.RunTimeDate AS [Last Import Date],
			'AZSQZ01: ' + j.name AS [Job Name],
			'UNKNOWN' AS [File Upload Status],
			NULL AS [Last Import Status],
			LEFT(jh.message,CHARINDEX('The last step to run',jh.message,1) -1) + ' (Time to Complete: ' + m1.Run_Duration + ')' AS [Last Import Message],
			NULL AS [Last Queue Count],
			NULL AS [Last Import Count],
			NULL AS [Last FinalCount Count],
			NULL AS [FilePath],
			'Every ' + CONVERT(varchar(20),DATEDIFF(hour,m2.RunTimeDate,m1.RunTimeDate)) + ' Hour(s)' AS [File Upload Interval],
			NULL AS [Next Import Date],
			NULL AS [DocumentationPath],j.job_id
		FROM [AZSQZ01].msdb.dbo.sysjobs j
			INNER JOIN [AZSQZ01].msdb.dbo.sysjobhistory jh ON jh.job_id = j.job_id
			INNER JOIN cte_AZSQZ01_Max_JH m1 ON m1.instance_id = jh.instance_id AND m1.rowNum = 1
			LEFt JOIN cte_AZSQZ01_Max_JH m2 ON m2.job_id = jh.job_id AND m2.rowNum = 2
		WHERE j.name IN('Member Portal Import Data','Corporate Survey Recurring Update','STAR: Email Global Sales Directors','CRM Sync LOCAL')
			AND jh.step_id = 0
	),
	cte_WUS2ASVM_Max_JH
	AS
	(
		SELECT instance_id,job_id,msdb.dbo.agent_datetime(run_date,run_time) AS RunTimeDate,
			RIGHT('00' + CONVERT(varchar(20),run_duration/10000),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration/100%100),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration%100),2) AS Run_Duration,
			ROW_NUMBER() OVER(PARTITION BY job_id ORDER BY msdb.dbo.agent_datetime(run_date,run_time) DESC) AS rowNum
		FROM [10.1.2.18].msdb.dbo.sysjobhistory
		WHERE step_id = 0
	),
	cte_WUS2ASVM
	AS
	(
		SELECT m1.RunTimeDate AS [Last Import Date],
			'WUS2_DATA_AS_VM: ' + j.name AS [Job Name],
			'UNKNOWN' AS [File Upload Status],
			NULL AS [Last Import Status],
			LEFT(jh.message,CHARINDEX('The last step to run',jh.message,1) -1) + ' (Time to Complete: ' + m1.Run_Duration + ')' AS [Last Import Message],
			NULL AS [Last Queue Count],
			NULL AS [Last Import Count],
			NULL AS [Last FinalCount Count],
			NULL AS [FilePath],
			'Every ' + CONVERT(varchar(20),DATEDIFF(hour,m2.RunTimeDate,m1.RunTimeDate)) + ' Hour(s)' AS [File Upload Interval],
			NULL AS [Next Import Date],
			NULL AS [DocumentationPath],j.job_id
		FROM [10.1.2.18].msdb.dbo.sysjobs j
			INNER JOIN [10.1.2.18].msdb.dbo.sysjobhistory jh ON jh.job_id = j.job_id
			INNER JOIN cte_WUS2ASVM_Max_JH m1 ON m1.instance_id = jh.instance_id AND m1.rowNum = 1
			LEFt JOIN cte_WUS2ASVM_Max_JH m2 ON m2.job_id = jh.job_id AND m2.rowNum = 2
		WHERE j.name IN('Rebuild Data Marts')
			AND jh.step_id = 0
	),
	cte_MI_Max_JH
	AS
	(
		SELECT instance_id,job_id,msdb.dbo.agent_datetime(run_date,run_time) AS RunTimeDate,
			RIGHT('00' + CONVERT(varchar(20),run_duration/10000),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration/100%100),2) + ':'
			+ RIGHT('00' + CONVERT(varchar(20),run_duration%100),2) AS Run_Duration,
			ROW_NUMBER() OVER(PARTITION BY job_id ORDER BY msdb.dbo.agent_datetime(run_date,run_time) DESC) AS rowNum
		FROM ITOPS.dbo.sysjobhistory
		WHERE step_id = 0
	),
	cte_MI
	AS
	(
		SELECT m1.RunTimeDate AS [Last Import Date],
			'WUS2_DATA_MI: ' + j.name AS [Job Name],
			'UNKNOWN' AS [File Upload Status],
			NULL AS [Last Import Status],
			LEFT(jh.message,CHARINDEX('The last step to run',jh.message,1) -1) + ' (Time to Complete: ' + m1.Run_Duration + ')' AS [Last Import Message],
			NULL AS [Last Queue Count],
			NULL AS [Last Import Count],
			NULL AS [Last FinalCount Count],
			NULL AS [FilePath],
			'Every ' + CONVERT(varchar(20),DATEDIFF(hour,m2.RunTimeDate,m1.RunTimeDate)) + ' Hour(s)' AS [File Upload Interval],
			NULL AS [Next Import Date],
			NULL AS [DocumentationPath],j.job_id
		FROM ITOPS.dbo.sysjobs j
			INNER JOIN ITOPS.dbo.sysjobhistory jh ON jh.job_id = j.job_id
			INNER JOIN cte_MI_Max_JH m1 ON m1.instance_id = jh.instance_id AND m1.rowNum = 1
			LEFt JOIN cte_MI_Max_JH m2 ON m2.job_id = jh.job_id AND m2.rowNum = 2
		WHERE j.name IN('Profitability Report Results','Populate Reservations Reporting')
			AND jh.step_id = 0
	),
	cte_Final
	AS
	(
		SELECT f.JobName + ' [' + f.Application + ']' AS JobName,
				CONVERT(date,
						CASE
						WHEN f.ETL_FrequencyType = 1 THEN DATEADD(day,-f.ETL_FrequencyInterval + 1,f.Daily_Freq)
						WHEN f.ETL_FrequencyType = 2 THEN DATEADD(day,-f.ETL_FrequencyInterval + 1,f.Weekly_Freq)
						WHEN f.ETL_FrequencyType = 3 THEN DATEADD(day,-f.ETL_FrequencyInterval + 1,f.Monthly_Freq)
					END) AS ExpectedRunDate,q.RunDate,q.QueueStatus_Desc,q.[Message],
				'Every ' + CONVERT(varchar(20),ETL_FrequencyInterval) + ' ' + CASE ETL_FrequencyType WHEN 1 THEN 'Day(s)' WHEN 2 THEN 'Week(s)' WHEN 3 THEN 'Month(s)' END AS Freq,
				ETL_FrequencyType,
				f.Application
		FROM cte_Freq f
			INNER JOIN cte_Q2 q on q.[Application] = f.[Application]
		WHERE q.rowNum = 1
	)
	SELECT RunDate AS [Last Import Date],JobName AS [Job Name],
		CASE WHEN ETL_FrequencyType = 1 AND DATEDIFF(DAY,RunDate,ExpectedRunDate) > 1 THEN 'LATE'
			 WHEN ETL_FrequencyType = 2 AND DATEDIFF(DAY,RunDate,ExpectedRunDate) > 7 THEN 'LATE'
			 WHEN ETL_FrequencyType = 3 AND DATEDIFF(DAY,RunDate,ExpectedRunDate) > 30 THEN 'LATE'	ELSE 'ON TIME' END AS [File Upload Status],
		NULL AS [Last Import Status],
		CASE
			WHEN ETL_FrequencyType = 1 AND DATEDIFF(DAY,RunDate,ExpectedRunDate) > 1 THEN 'LATE: Check Job Status:'+' [' + Application + ']'
			WHEN ETL_FrequencyType = 2 AND DATEDIFF(DAY,RunDate,ExpectedRunDate) > 7 THEN 'LATE: Check Job Status:'+'[' + Application + ']'
			WHEN ETL_FrequencyType = 3 AND DATEDIFF(DAY,RunDate,ExpectedRunDate) > 30 THEN 'LATE: Check Job Status:'+' [' + Application + ']'
			ELSE QueueStatus_Desc + ': ' + [Message]	
		END AS [Last Import Message],
		NULL AS [Last Queue Count],
		NULL AS [Last Import Count],
		NULL AS [Last FinalCount Count],
		NULL AS [FilePath],
		Freq AS [File Upload Interval],
		NULL AS [Next Import Date],
		NULL AS [DocumentationPath]
	FROM cte_Final
	
	UNION ALL
	
	SELECT [Last Import Date],[Job Name],
		[File Upload Status],
		[Last Import Status],
		[Last Import Message],
		[Last Queue Count],
		[Last Import Count],
		[Last FinalCount Count],
		[FilePath],
		[File Upload Interval],
		[Next Import Date],
		[DocumentationPath]
	FROM cte_Utility
	
	UNION ALL
	
	SELECT [Last Import Date],[Job Name],
		[File Upload Status],
		[Last Import Status],
		[Last Import Message],
		[Last Queue Count],
		[Last Import Count],
		[Last FinalCount Count],
		[FilePath],
		[File Upload Interval],
		[Next Import Date],
		[DocumentationPath]
	FROM cte_AZSQZ01

	UNION ALL
	
	SELECT [Last Import Date],[Job Name],
		[File Upload Status],
		[Last Import Status],
		[Last Import Message],
		[Last Queue Count],
		[Last Import Count],
		[Last FinalCount Count],
		[FilePath],
		[File Upload Interval],
		[Next Import Date],
		[DocumentationPath]
	FROM cte_WUS2ASVM

	UNION ALL 
	
	SELECT [Last Import Date],[Job Name],
		[File Upload Status],
		[Last Import Status],
		[Last Import Message],
		[Last Queue Count],
		[Last Import Count],
		[Last FinalCount Count],
		[FilePath],
		[File Upload Interval],
		[Next Import Date],
		[DocumentationPath]
	FROM cte_MI
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
