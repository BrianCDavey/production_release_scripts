USE Dimensional_Warehouse
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        CHI-SQ-ST-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/10/2020 9:04:15 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProductionWorkbookPaceChannel]'
GO



ALTER PROCEDURE [rpt].[ProductionWorkbookPaceChannel] 
	@HotelID int,
	@Year int,
	@Month int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select distinct
	h.hotelKey
	, cal.Year
	, cal.Month
	, CAST(DATENAME(month, convert(varchar(4), cal.Year)+'-'+Convert(varchar(2),cal.Month)+'-01') as varchar(3)) as 'Month Name'
	, chan.[PH Channel]
	, coalesce( CYconf.[Room Nights], 0) + coalesce (CYcan.[Room Nights],0) as 'CY Pace RN'
	, coalesce(LYconf.[Room Nights], 0) + coalesce (LYcan.[Room Nights],0) as 'LY Pace RN'
	, coalesce(LY.[Room Nights],0) as 'LY Month End'

	from Dimensional_Warehouse.dim.BookingSource chan

	 cross join
		(select hotelKey from Dimensional_Warehouse.dim.Hotel
		where hotelKey = @HotelID) h

	cross join 
	(select 
	YEAR(dat.[Full Date]) as Year
	,MONTH(dat.[Full Date]) as Month
	from Dimensional_Warehouse.dim.DimDate dat
	where 
	dat.[Full Date] between convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'))
	) cal

	left join
	-- Begin CY confirmed o1
	(select
		Res.hotelKey
		,YEAR(da.[Full Date]) as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		,ch.[PH Channel] as 'Channel'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month + 1)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'))
		and stat.[Booking Status Name] <> 'Cancelled'
		and con.[Full Date] <= convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
			Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])
		,ch.[PH Channel]

	) as CYconf
	on chan.[PH Channel] = CYconf.Channel and (cal.Year=CYconf.Year AND cal.Month = CYconf.Month)
	-- end CY confirmed o1

	left join
	-- Begin CY cancelled o2
	(select
		Res.hotelKey
		,YEAR(da.[Full Date]) as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		,ch.[PH Channel] as 'Channel'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey
		join
		Dimensional_Warehouse.dim.DimDate can
		on res.[Cancellation Date] = can.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month + 1)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'))
		and con.[Full Date] <= convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'
		and can.[Full Date] > convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by
			Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date]) 
		,ch.[PH Channel]

	) as CYcan
	on  chan.[PH Channel] = CYcan.Channel and (CYcan.Year = cal.Year AND CYcan.Month = cal.Month)-- and CYcan.Channel = CYconf.Channel
	-- end CY cancelled o2

	left join
	-- Begin LY confirmed o3
	(select
		Res.hotelKey
		,YEAR(da.[Full Date])+1 as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		,ch.[PH Channel] as 'Channel'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month + 1)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'))
		and stat.[Booking Status Name] <> 'Cancelled'
		and con.[Full Date] <= convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
		Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])
		,ch.[PH Channel]

	) as LYconf
	on  chan.[PH Channel] = LYconf.Channel and (LYconf.Year = cal.Year AND LYconf.Month = cal.Month) --and LYconf.Channel = CYconf.Channel
	-- end LY confirmed o3

	left join
	-- Begin LY cancelled o4
	(select
		Res.hotelKey
		,YEAR(da.[Full Date])+1 as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		,ch.[PH Channel] as 'Channel'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey
		join 
		Dimensional_Warehouse.dim.DimDate can
		on res.[Cancellation Date] = can.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month + 1)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'))
		and con.[Full Date] <= convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'
		and can.[Full Date] > convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
		Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])
		,ch.[PH Channel]

	) as LYcan
	on  chan.[PH Channel] =LYcan.Channel  and (LYcan.Year = cal.Year AND LYcan.Month = cal.Month) --and LYcan.Channel = CYconf.Channel
	-- end LY cancelled o4

	left join
	-- Begin LY END o5
	(select
		Res.hotelKey
		,YEAR(da.[Full Date])+1 as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		,ch.[PH Channel] as 'Channel'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month + 1)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'))
		and stat.[Booking Status Name] <> 'Cancelled'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
		Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])
		,ch.[PH Channel]

	) as LY
	on chan.[PH Channel] =LY.Channel  and (LY.Year = cal.Year AND LY.Month = cal.Month) --and LY.Channel = CYconf.Channel
	-- end LY end o5

	WHERE [PH Channel] <>'Unknown'
	and not
	(COALESCE(CYconf.[Room Nights],0) + COALESCE(CYcan.[Room Nights],0) = 0
	and COALESCE(LYconf.[Room Nights], 0) + COALESCE(LYcan.[Room Nights], 0) = 0
	and COALESCE(LY.[Room Nights], 0) =0)



	order by 2,3,5
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProductionWorkbookPaceLine]'
GO

ALTER PROCEDURE [rpt].[ProductionWorkbookPaceLine]
	@HotelID int,
	@Year int,
	@Month int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select distinct
	h.hotelKey
	, cal.Year
	, cal.Month
	, CAST(DATENAME(month, convert(varchar(4), cal.Year)+'-'+Convert(varchar(2),cal.Month)+'-01') as varchar(3)) as 'Month Name'
	, coalesce( CYconf.[Room Nights],0) + coalesce (CYcan.[Room Nights],0) as 'CY Pace RN'
	, coalesce(LYconf.[Room Nights], 0) + coalesce (LYcan.[Room Nights],0) as 'LY Pace RN'
	, coalesce(LY.[Room Nights],0) as 'LY Month End'


	-- select pct change for months
	, CASE WHEN LYconf.[Room Nights]+LYcan.[Room Nights] = 0
		THEN
		coalesce(LYconf.[Room Nights]+LYcan.[Room Nights] , 0)
		ELSE
		coalesce((CAST(CYconf.[Room Nights]+CYcan.[Room Nights] as float) - cast(LYconf.[Room Nights]+LYcan.[Room Nights] as float)) / cast(LYconf.[Room Nights]+LYcan.[Room Nights] as float) , 0)
		END
		as 'RN P Chg'

	from Dimensional_Warehouse.dim.Hotel h

	cross join 
	(select 
	YEAR(dat.[Full Date]) as Year
	,MONTH(dat.[Full Date]) as Month
	from Dimensional_Warehouse.dim.DimDate dat
	where 
	dat.[Full Date] between convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'))
	) cal

	left join
	-- Begin CY confirmed o1
	(select
		Res.hotelKey
		,YEAR(da.[Full Date]) as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'))
		and stat.[Booking Status Name] <> 'Cancelled'
		and con.[Full Date] <= convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
			Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])

	) as CYconf
	on cal.Year=CYconf.Year AND cal.Month = CYconf.Month
	-- end CY confirmed o1

	left join
	-- Begin CY cancelled o2
	(select
		Res.hotelKey
		,YEAR(da.[Full Date]) as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey
		join
		Dimensional_Warehouse.dim.DimDate can
		on res.[Cancellation Date] = can.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'))
		and con.[Full Date] <= convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'
		and can.[Full Date] > convert(varchar(4), @Year)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by
			Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date]) 

	) as CYcan
	on  CYcan.Year = cal.Year AND CYcan.Month = cal.Month
	-- end CY cancelled o2

	left join
	-- Begin LY confirmed o3
	(select
		Res.hotelKey
		,YEAR(da.[Full Date])+1 as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'))
		and stat.[Booking Status Name] <> 'Cancelled'
		and con.[Full Date] <= convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
		Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])

	) as LYconf
	on  LYconf.Year = cal.Year AND LYconf.Month = cal.Month
	-- end LY confirmed o3

	left join
	-- Begin LY cancelled o4
	(select
		Res.hotelKey
		,YEAR(da.[Full Date])+1 as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey
		join 
		Dimensional_Warehouse.dim.DimDate can
		on res.[Cancellation Date] = can.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'))
		and con.[Full Date] <= convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'
		and can.[Full Date] > convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
		Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])

	) as LYcan
	on  LYcan.Year = cal.Year AND LYcan.Month = cal.Month
	-- end LY cancelled o4

	left join
	-- Begin LY END o5
	(select
		Res.hotelKey
		,YEAR(da.[Full Date])+1 as 'Year'
		,MONTH(da.[Full Date]) as 'Month'
		, coalesce(SUM(Res.[Room Nights]),0) as 'Room Nights'

		from
		Dimensional_Warehouse.fact.Reservation Res
		join
		Dimensional_Warehouse.dim.DimDate da
		on res.[Arrival Date] = da.dateKey
		join
		Dimensional_Warehouse.dim.BookingStatus stat
		on stat.bookingStatusKey = res.bookingStatusKey
		join
		Dimensional_Warehouse.dim.BookingSource ch
		on ch.bookingSourceKey = res.bookingSourceKey
		join 
		Dimensional_Warehouse.dim.DimDate con
		on res.[Confirmation Date] = con.dateKey

	Where
		Res.hotelKey = @HotelID
		and da.[Full Date] between convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01' and EOMONTH(DATEADD(MONTH, 11, convert(varchar(4), @Year-1)+'-'+Convert(varchar(2),@Month)+'-01'))
		and stat.[Booking Status Name] <> 'Cancelled'
		-- BO Sept 2019: we used to filter out these channels and CRO codes in the old production dashboard but no longer want to
		--and ch.[CRO Code]  NOT LIKE 'HTL_%'
		--and ch.[CRS Channel] <> 'PMS Rez Synch'

		group by 
		Res.hotelKey
		,YEAR(da.[Full Date])
		,MONTH(da.[Full Date])

	) as LY
	on LY.Year = cal.Year AND LY.Month = cal.Month
	-- end LY end o5

	WHERE 
	h.hotelKey = @HotelID

	order by 2,3
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
