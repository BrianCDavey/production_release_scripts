USE IC
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.IC    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.IC

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 12:26:37 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppWorkOrderNumber]'
GO
DROP FUNCTION [dbo].[dgppWorkOrderNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppVendorID]'
GO
DROP FUNCTION [dbo].[dgppVendorID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppUPRTimecard]'
GO
DROP FUNCTION [dbo].[dgppUPRTimecard]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppUPRESSW4]'
GO
DROP FUNCTION [dbo].[dgppUPRESSW4]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppUPRESSSkillsTraining]'
GO
DROP FUNCTION [dbo].[dgppUPRESSSkillsTraining]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppUPRESSEmpProfile]'
GO
DROP FUNCTION [dbo].[dgppUPRESSEmpProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppUPRESSDirectDeposit]'
GO
DROP FUNCTION [dbo].[dgppUPRESSDirectDeposit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppSmartListView]'
GO
DROP FUNCTION [dbo].[dgppSmartListView]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppServiceRecordType]'
GO
DROP FUNCTION [dbo].[dgppServiceRecordType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppReceivablesTransactionNumber]'
GO
DROP FUNCTION [dbo].[dgppReceivablesTransactionNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppRTVNumber]'
GO
DROP FUNCTION [dbo].[dgppRTVNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppRMBatchEntry]'
GO
DROP FUNCTION [dbo].[dgppRMBatchEntry]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppRMANumber]'
GO
DROP FUNCTION [dbo].[dgppRMANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPurchaseRequisitionNumber]'
GO
DROP FUNCTION [dbo].[dgppPurchaseRequisitionNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPurchaseOrder]'
GO
DROP FUNCTION [dbo].[dgppPurchaseOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPurchaseOrderReturn]'
GO
DROP FUNCTION [dbo].[dgppPurchaseOrderReturn]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPurchaseOrderReceipt]'
GO
DROP FUNCTION [dbo].[dgppPurchaseOrderReceipt]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPurchaseOrderPOTRX]'
GO
DROP FUNCTION [dbo].[dgppPurchaseOrderPOTRX]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPurchaseOrderPM]'
GO
DROP FUNCTION [dbo].[dgppPurchaseOrderPM]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppProjectNumber]'
GO
DROP FUNCTION [dbo].[dgppProjectNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPayablesTransactionNumber]'
GO
DROP FUNCTION [dbo].[dgppPayablesTransactionNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPTETimesheet]'
GO
DROP FUNCTION [dbo].[dgppPTETimesheet]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPTEExpenseDocument]'
GO
DROP FUNCTION [dbo].[dgppPTEExpenseDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppPMBatchEntry]'
GO
DROP FUNCTION [dbo].[dgppPMBatchEntry]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppOpenPMTrxNmbr]'
GO
DROP FUNCTION [dbo].[dgppOpenPMTrxNmbr]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppItemTransactionNumber]'
GO
DROP FUNCTION [dbo].[dgppItemTransactionNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppItemID]'
GO
DROP FUNCTION [dbo].[dgppItemID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppGLBatchEntry]'
GO
DROP FUNCTION [dbo].[dgppGLBatchEntry]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppEquipmentID]'
GO
DROP FUNCTION [dbo].[dgppEquipmentID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppEmployeeID]'
GO
DROP FUNCTION [dbo].[dgppEmployeeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppContractNumber]'
GO
DROP FUNCTION [dbo].[dgppContractNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppCMTransactionZoom]'
GO
DROP FUNCTION [dbo].[dgppCMTransactionZoom]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppBudgetTrxSummary]'
GO
DROP FUNCTION [dbo].[dgppBudgetTrxSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppAssetIndex]'
GO
DROP FUNCTION [dbo].[dgppAssetIndex]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppAcctNumberDetail]'
GO
DROP FUNCTION [dbo].[dgppAcctNumberDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Writeoff]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Writeoff]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Workflow_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Workflow_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Which_Cash_Account_For_Pay]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Which_Cash_Account_For_Pay]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Voided]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Voided]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Vendor_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Vendor_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Valuation_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Valuation_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Vacation_Accrual_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Vacation_Accrual_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Type_of_Employment]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Type_of_Employment]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Transfer_To_QTY_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Transfer_To_QTY_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Transfer_From_QTY_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Transfer_From_QTY_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Tip_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Tip_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Time_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Time_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Tax_Schedule_Source_from_Sales_Line_Item]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Tax_Schedule_Source_from_Sales_Line_Item]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Tax_Options]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Tax_Options]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Tax_Detail_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Tax_Detail_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Summary_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Summary_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Status_Group]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Status_Group]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Source_Indicator]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Source_Indicator]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Sick_Time_Accrual_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Sick_Time_Accrual_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Series_Tax_Detail_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Series_Tax_Detail_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Salary_Change]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Salary_Change]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_SOP_HDR_Flags]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_SOP_HDR_Flags]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_SOP_Distribution_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_SOP_Distribution_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Requisition_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Requisition_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Requisition_Line_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Requisition_Line_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Requisition_Document_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Requisition_Document_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Replenishment_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Replenishment_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Replenishment_Level]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Replenishment_Level]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Record_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Record_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Record_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Record_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Receiving_Rate_Calc_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Receiving_Rate_Calc_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Receiving_MC_Transaction_State]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Receiving_MC_Transaction_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Receipt_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Receipt_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Receipts]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Receipts]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Line_Items]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Line_Items]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Rate_Calc_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Rate_Calc_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_QTY_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_QTY_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Purchasing_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Purchasing_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Purchase_Tax_Options]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Purchase_Tax_Options]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Purchase_Receipt_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Purchase_Receipt_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Purchase_Misc_Taxable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Purchase_Misc_Taxable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Purchase_IV_Item_Taxable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Purchase_IV_Item_Taxable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Purchase_Freight_Taxable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Purchase_Freight_Taxable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Price_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Price_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Posting_Status_POP_Receipts]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Posting_Status_POP_Receipts]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Posting_Status_POP_Receipt_Lines]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Posting_Status_POP_Receipt_Lines]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Posting_Status_MDA]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Posting_Status_MDA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_To_Cash_Account_From]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_To_Cash_Account_From]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_Results_To_Vendor]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_Results_To_Vendor]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Payroll_Record_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Payroll_Record_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Payment_Entry_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Payment_Entry_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_PO_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_PO_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_PO_Status_Orig]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_PO_Status_Orig]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_PO_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_PO_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_PO_Line_Status_Orig]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_PO_Line_Status_Orig]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_PO_Line_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_PO_Line_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_POP_Vendor_Selection]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_POP_Vendor_Selection]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_POP_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_POP_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_POP_Tax_Schedule_Source]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_POP_Tax_Schedule_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_POP_Order_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_POP_Order_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Order_Policy]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Order_Policy]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Maximum_Invoice_Amount_For_Vendors]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Maximum_Invoice_Amount_For_Vendors]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Markdown_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Markdown_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_MaritalStatus]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_MaritalStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Line_Origin]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Line_Origin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Kit_COGS_Account_Source]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Kit_COGS_Account_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Item_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Item_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Item_Tracking_Option]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Item_Tracking_Option]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Include_In_Catalog]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Include_In_Catalog]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_IV_Item_Taxable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_IV_Item_Taxable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_History_Year]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_History_Year]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_HR_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_HR_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Gender]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Gender]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_GL_Line_Valid]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_GL_Line_Valid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_GL_Header_Valid]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_GL_Header_Valid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Functional_Currency_Decimals]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Functional_Currency_Decimals]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Free_On_Board]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Free_On_Board]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Forecast_Consumption_Period]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Forecast_Consumption_Period]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Federal_Filing_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Federal_Filing_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Ethnic_Origin]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Ethnic_Origin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_EIC_Filing_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_EIC_Filing_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Type_Tax_Detail_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Type_Tax_Detail_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Type_IV_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Type_IV_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_SOP_Line_Items]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_SOP_Line_Items]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_RM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_RM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_Purch_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_Purch_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_POP_Line_Items]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_POP_Line_Items]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_PM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_PM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_IV_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_IV_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_GL_Sum]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_GL_Sum]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_CM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_CM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Display_In_Lookups]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Display_In_Lookups]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Deposit_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Deposit_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Decimal_Places_Currency]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Decimal_Places_Currency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Decimal_Places]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Decimal_Places]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_DTA_Series]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_DTA_Series]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Currency_Decimals]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Currency_Decimals]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Credit_Limit]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Credit_Limit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Cost_Selection]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Cost_Selection]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Cost_Calculation_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Cost_Calculation_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Control_Type_PM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Control_Type_PM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Control_Type_CM]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Control_Type_CM]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Control_Blanket_By]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Control_Blanket_By]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Computer_TRX_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Computer_TRX_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Cash_Receipt_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Cash_Receipt_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_CM_Trx_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_CM_Trx_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Batch_Frequency]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Batch_Frequency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Attendance_TRX_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Attendance_TRX_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Address_Source]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Address_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_ABC_Code]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_ABC_Code]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_1099_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_1099_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_RM_Document_TypeAll]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_RM_Document_TypeAll]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Type_RM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Type_RM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Type_PM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Type_PM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Prefix_RM_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Prefix_RM_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Aging_Bucket]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Aging_Bucket]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_1099_Box_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_1099_Box_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[SalesTransactions]'
GO
DROP VIEW [dbo].[SalesTransactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppCheckbookID]'
GO
DROP FUNCTION [dbo].[dgppCheckbookID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppCustomerID]'
GO
DROP FUNCTION [dbo].[dgppCustomerID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppSalesOrder]'
GO
DROP FUNCTION [dbo].[dgppSalesOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppSalesOrderIVC]'
GO
DROP FUNCTION [dbo].[dgppSalesOrderIVC]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppSalesOrderRM]'
GO
DROP FUNCTION [dbo].[dgppSalesOrderRM]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppSalesOrderSOP]'
GO
DROP FUNCTION [dbo].[dgppSalesOrderSOP]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppSalespersonID]'
GO
DROP FUNCTION [dbo].[dgppSalespersonID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Allocate_By]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Allocate_By]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Balance_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Balance_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Commission_Applied_To]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Commission_Applied_To]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Credit_Limit_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Credit_Limit_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Default_Cash_Account_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Default_Cash_Account_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_Sls_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_Sls_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Finance_Charge_Amt_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Finance_Charge_Amt_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Freight_Taxable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Freight_Taxable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Maximum_Writeoff_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Maximum_Writeoff_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Minimum_Payment_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Minimum_Payment_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Misc_Taxable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Misc_Taxable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Order_Fulfillment_Shortage_Default]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Order_Fulfillment_Shortage_Default]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Original_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Original_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_Results_To_Customer]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_Results_To_Customer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Posting_Status_SOP_Line_Items]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Posting_Status_SOP_Line_Items]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Priority]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Priority]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_SOP_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_SOP_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_SOP_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_SOP_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Statement_Cycle]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Statement_Cycle]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Tax_Schedule_Source]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Tax_Schedule_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_TRX_Frequency]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_TRX_Frequency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Void_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Void_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[AccountTransactions]'
GO
DROP VIEW [dbo].[AccountTransactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppAccountIndex]'
GO
DROP FUNCTION [dbo].[dgppAccountIndex]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppJournalEntry]'
GO
DROP FUNCTION [dbo].[dgppJournalEntry]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[dgppJournalInquiry]'
GO
DROP FUNCTION [dbo].[dgppJournalInquiry]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Account_Category_Number]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Account_Category_Number]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Account_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Account_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Balance_For_Calculation]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Balance_For_Calculation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Boolean_All]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Boolean_All]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Closed_Year]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Closed_Year]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Conversion_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Conversion_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Decimal_Places_QTYS]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Decimal_Places_QTYS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Document_Status_GL_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Document_Status_GL_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Fixed_Or_Variable]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Fixed_Or_Variable]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_GL_Ledger_Description]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_GL_Ledger_Description]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_GL_Ledger_Name]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_GL_Ledger_Name]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Line_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Line_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_MC_Transaction_State]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_MC_Transaction_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Open_Year]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Open_Year]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Originating_DTA_Series]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Originating_DTA_Series]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Originating_TRX_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Originating_TRX_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Originating_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Originating_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_Inventory_In]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_Inventory_In]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_Payroll_In]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_Payroll_In]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_Purchasing_In]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_Purchasing_In]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Post_Sales_In]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Post_Sales_In]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Posting_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Posting_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Printing_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Printing_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Rate_Calculation_Method]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Rate_Calculation_Method]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Reversing_Closed_Year]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Reversing_Closed_Year]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Reversing_Year]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Reversing_Year]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Series_GL_Trx]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Series_GL_Trx]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Transaction_Type]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Transaction_Type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Typical_Balance]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Typical_Balance]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Workflow_Approval_Status]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Workflow_Approval_Status]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DYN_FUNC_Workflow_Priority]'
GO
DROP FUNCTION [dbo].[DYN_FUNC_Workflow_Priority]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
