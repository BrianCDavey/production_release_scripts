USE Superset
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 1:13:22 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[RollupGetiPrefer]'
GO
DROP PROCEDURE [Reporting].[RollupGetiPrefer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReportKES]'
GO

CREATE PROCEDURE [Reporting].[ProfitabilityReportKES] 
-- Add the parameters for the stored procedure here
@arrivalStart DATE,
@arrivalEnd   DATE,
@hotelcode    VARCHAR(10) = NULL,
@Extended     BIT         = 0
AS
     BEGIN
         SET NOCOUNT ON; -- prevents extra result sets from interfering with SELECT statements
         --get the hotel information
         SELECT H.synxisID,
                H.openHospitalityCode,
                H.code AS hotelCode,
                H.hotelName AS hotelName,
                H.mainBrandCode AS primaryBrand,
                LTRIM(RTRIM(RM00101.SALSTERR)) AS gpSalesTerritory,
                H.phg_areamanageridName AS AMD,
                H.phg_regionalmanageridName AS RD,
                H.phg_revenueaccountmanageridName AS RAM,
                H.phg_AccountManagerName AS AccountManager,
				CRM.accountclassificationcodename AS CRMClassification,
                H.geographicRegionName AS geographicRegion,
                H.shortName AS country,
                H.physicalCity AS city,
                H.subAreaName AS state,
                H.totalRooms hotelRooms,
                H.statuscodename hotelStatus,
                LTRIM(RTRIM(RM00101.CURNCYID)) AS hotelCurrency,
                B.gpSiteID
         INTO #Hotels-- select * 
         FROM Core.dbo.hotelsReporting H
              LEFT OUTER JOIN IC.dbo.RM00101 ON H.code = RM00101.CUSTNMBR
              LEFT OUTER JOIN LocalCRM.dbo.account CRM ON H.crmGuid = CRM.accountid
              LEFT OUTER JOIN Core.dbo.Brands B ON H.mainBrandCode = B.code
         WHERE(synxisID > 0
               OR openHospitalityCode > 0)
              AND (@hotelcode IS NULL
                   OR H.code = @hotelcode);
         CREATE CLUSTERED INDEX ix_Hotels ON #Hotels(synxisID, openHospitalityCode);



         -- get iata account manager from CRM, store in separate table
         SELECT accountnumber,
                MIN(phg_iataglobalaccountmanageridName) iatamanager
         INTO #rpt_IATAManager
         FROM LocalCRM.dbo.account
         WHERE phg_iataglobalaccountmanageridName IS NOT NULL
               AND accountnumber IS NOT NULL
         GROUP BY accountnumber;
         CREATE CLUSTERED INDEX ix_iatamanager ON #rpt_IATAManager(accountnumber);


         --	build master MRT table - get all the data ONCE (not multiple reads of MRT)
         --  cost, sums, counts, channels, sources etc, 
         --  a field that indicates whether the record is billable 
         SELECT mrt.crsSourceID,
                CASE mrt.crsSourceID
                    WHEN 2
                    THEN mrt.openhospitalityID
                    ELSE mrt.hotelID
                END AS crsHotelID,
                mrt.arrivalDate,
                mrt.confirmationDate,
                mrt.confirmationNumber,
                1 AS SynXisCount,
                mrt.status,
                mrt.channel,
                mrt.CROcode,
                mrt.billingDescription,
                mrt.SecondarySource,
                mrt.subSourceReportLabel,
                CASE mrt.billingDescription
                    WHEN ''
                    THEN pegsBookingCost.cost
                    ELSE synxisBookingCost.cost
                END+CASE mrt.channel
                        WHEN 'GDS'
                        THEN gdsBookingCost.cost
                        ELSE 0
                    END AS cost,
                IM.iatamanager AS IataGam,
                Core.dbo.GetIATAGroups(mrt.IATANumber, mrt.confirmationdate) IATAGroups,
                mrt.IATANumber,
                mrt.travelAgencyName,
                mrt.travelAgencyCity,
                mrt.travelAgencyState,
                mrt.travelAgencyCountry,
                mrt.rateCategoryCode,
                mrt.rateTypeCode,
                mrt.rateTypeName AS ratename,
                CASE
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'GOL'
                    THEN 'GOLF'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'PKG'
                    THEN 'PKG'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'PRO'
                    THEN 'PRO'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'MKT'
                    THEN 'MKT'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'CON'
                    THEN 'CON'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'NEG'
                    THEN 'NEG'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'BAR'
                    THEN 'BAR'
                    WHEN LEFT(mrt.rateTypeCode, 3) = 'DIS'
                    THEN 'DIS'
                    ELSE CASE
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'GOL'
                             THEN 'GOLF'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'PKG'
                             THEN 'PKG'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'PRO'
                             THEN 'PRO'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'MKT'
                             THEN 'MKT'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'CON'
                             THEN 'CON'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'NEG'
                             THEN 'NEG'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'BAR'
                             THEN 'BAR'
                             WHEN LEFT(mrt.rateCategoryCode, 3) = 'DIS'
                             THEN 'DIS'
                             ELSE 'Other'
                         END
                END AS rateCategory_PHG,
                mrt.rooms,
                mrt.nights,
                mrt.rooms * mrt.nights AS RoomNights,
                mrt.currency,
                mrt.reservationRevenue,
                mrt.reservationRevenueUSD,
                0 AS IPreferCount,
                CAST(0.00 AS DECIMAL(18, 2)) AS IPreferChargesUSD,
				CAST(0.00 AS DECIMAL(18, 2)) AS IPreferChargesHotelCurrency,
                CAST(0.00 AS DECIMAL(18, 2)) AS IPreferCost
         INTO #rpt_Main
         FROM superset.dbo.mostrecenttransactionsreporting mrt WITH (nolock)
              LEFT OUTER JOIN Superset.dbo.gdsBookingCost ON mrt.secondarySource = gdsBookingCost.gdsType
                                                             AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
              LEFT OUTER JOIN Superset.dbo.synxisBookingCost ON mrt.billingDescription = synxisBookingCost.billingDescription
                                                                AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
              LEFT OUTER JOIN Superset.dbo.pegsBookingCost ON mrt.channel = pegsBookingCost.channel
                                                              AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')
              LEFT OUTER JOIN #rpt_IATAManager IM WITH (nolock) ON mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS
         WHERE MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
               AND (@hotelcode IS NULL
                    OR mrt.hotelCode = @hotelcode)
               AND MRT.channel <> 'PMS Rez Synch'
               AND MRT.CROcode <> 'HTL_Carlton'
               AND MRT.Status <> 'Cancelled'

         -- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
         UNION ALL
         SELECT NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
				NULL,
                NULL;
         CREATE CLUSTERED INDEX ix_rptMain_arrivaldate_synxisID ON #rpt_Main(arrivaldate, crsHotelID);
         CREATE INDEX ix_rptMain_ConfirmNum ON #rpt_Main(confirmationnumber);
         DELETE FROM #rpt_Main
         WHERE crsHotelID IS NULL;


         -- get all billy charges from CalculatedDebits for the date parameters
         SELECT cd.confirmationNumber,
                cd.hotelCode,
				H.synxisID,
				H.openHospitalityCode,
                cd.gpSiteID,
                cd.arrivalDate,
		
/* CODE CHANGE: TMG 2015-09-15
				new list of criteriaIDs that are surcharges - per hiren
		
		--1=Booking Charges
		SUM(CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END)	AS bookingCharges , 		
		--2=Commissions
		SUM(CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END)
		AS commissions , 
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
			ELSE 0 END) AS surcharges 
		*/

                SUM(CASE
                        WHEN(ESC.CriteriaId = 365
                             AND ESC.Surcharge = 1
                             AND CD.classificationID = 1)
                        THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                        ELSE CASE
                                 WHEN(ESC.CriteriaId IS NOT NULL
                                      AND ESC.Surcharge = 1)
                                 THEN 0
                                 ELSE CASE classificationID
                                          WHEN 1
                                          THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                                          WHEN 3
                                          THEN CASE
                                                   WHEN C.itemCode IS NULL
                                                   THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                                                   ELSE 0
                                               END 
         --3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
                                          ELSE 0
                                      END
                             END
                    END) AS bookingChargesUSD, 		
		
         --2=Commissions
                SUM(CASE
                        WHEN(ESC.CriteriaId = 365
                             AND ESC.Surcharge = 1
                             AND CD.classificationID = 2)
                        THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                        ELSE CASE
                                 WHEN(ESC.CriteriaId IS NOT NULL
                                      AND ESC.Surcharge = 1)
                                 THEN 0
                                 ELSE CASE classificationID
                                          WHEN 2
                                          THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                                          ELSE 0
                                      END
                             END
                    END) AS commissionsUSD, 
		
         --3=Surcharges, only if the item code was overridden do we count it separately per Hiren
                SUM(CASE
                        WHEN(ESC.CriteriaId = 365
                             AND ESC.Surcharge = 1
                             AND CD.classificationID IN(1, 2))
                        THEN 0
                        ELSE CASE
                                 WHEN(ESC.CriteriaId = 365
                                      AND ESC.Surcharge = 1
                                      AND CD.classificationID = 3)
                                 THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                                 ELSE CASE
                                          WHEN(ESC.CriteriaId IS NOT NULL
                                               AND ESC.Surcharge = 1)
                                          THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                                          ELSE CASE classificationID
                                                   WHEN 3
                                                   THEN CASE
                                                            WHEN C.itemCode IS NOT NULL
                                                            THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency, cd.hotelCurrencyCode, cd.arrivalDate)
                                                            ELSE 0
                                                        END
                                                   ELSE 0
                                               END
                                      END
                             END
                    END) AS surchargesUSD,

				SUM(CASE
                        WHEN(ESC.CriteriaId = 365
                             AND ESC.Surcharge = 1
                             AND CD.classificationID = 1)
                        THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                        ELSE CASE
                                 WHEN(ESC.CriteriaId IS NOT NULL
                                      AND ESC.Surcharge = 1)
                                 THEN 0
                                 ELSE CASE classificationID
                                          WHEN 1
                                          THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                                          WHEN 3
                                          THEN CASE
                                                   WHEN C.itemCode IS NULL
                                                   THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                                                   ELSE 0
                                               END 
         --3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
                                          ELSE 0
                                      END
                             END
                    END) AS bookingChargesHotelCurrency, 		
		
         --2=Commissions
                SUM(CASE
                        WHEN(ESC.CriteriaId = 365
                             AND ESC.Surcharge = 1
                             AND CD.classificationID = 2)
                        THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                        ELSE CASE
                                 WHEN(ESC.CriteriaId IS NOT NULL
                                      AND ESC.Surcharge = 1)
                                 THEN 0
                                 ELSE CASE classificationID
                                          WHEN 2
                                          THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                                          ELSE 0
                                      END
                             END
                    END) AS commissionsHotelCurrency, 
		
         --3=Surcharges, only if the item code was overridden do we count it separately per Hiren
                SUM(CASE
                        WHEN(ESC.CriteriaId = 365
                             AND ESC.Surcharge = 1
                             AND CD.classificationID IN(1, 2))
                        THEN 0
                        ELSE CASE
                                 WHEN(ESC.CriteriaId = 365
                                      AND ESC.Surcharge = 1
                                      AND CD.classificationID = 3)
                                 THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                                 ELSE CASE
                                          WHEN(ESC.CriteriaId IS NOT NULL
                                               AND ESC.Surcharge = 1)
                                          THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                                          ELSE CASE classificationID
                                                   WHEN 3
                                                   THEN CASE
                                                            WHEN C.itemCode IS NOT NULL
                                                            THEN CASE WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate) END
                                                            ELSE 0
                                                        END
                                                   ELSE 0
                                               END
                                      END
                             END
                    END) AS surchargesHotelCurrency
     INTO #rpt_BillyCharges
         FROM superset.dbo.calculatedDebits CD WITH (nolock)
              JOIN BillingProduction.dbo.criteria C WITH (nolock) ON cd.criteriaID = C.id
              LEFT OUTER JOIN BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC ON Cd.criteriaID = ESC.CriteriaId
			  LEFT OUTER JOIN #Hotels H ON CD.hotelCode = H.hotelCode
         WHERE cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
               AND (@hotelcode IS NULL
                    OR CD.hotelCode = @hotelcode)
         --AND		cd.chargeValueInHotelCurrency <> 0  -- but without this it greatly increases the billybookingcount
         -- this is needed for the new bsi functionality, credit memos can now uncharge for cancelled reserveatins
         GROUP BY CD.confirmationNumber,
                  cd.hotelCode,
				  H.synxisID,
				  H.openHospitalityCode,
				  H.hotelCurrency,
                  cd.arrivaldate,
                  cd.gpSiteID
         ORDER BY CD.confirmationNumber,
                  cd.hotelCode,
                  cd.arrivaldate,
                  cd.gpSiteID;
         CREATE CLUSTERED INDEX ix_RptCharges ON #rpt_BillyCharges(confirmationnumber);



/*	2015-03-09 TMG

	this fix will keep billy count the same for each month, even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count, because the arrival month 

*/

         INSERT INTO #rpt_Main
         (crsHotelID,
          arrivaldate,
          confirmationdate,
          confirmationnumber,
          SynXisCount,
          billingDescription,
          Channel,
          status,
          SecondarySource,
          subSourceReportLabel,
          CROcode,
          cost,
          IataGam,
          IATAGroups,
          IATANumber,
          travelAgencyName,
          travelAgencyCity,
          travelAgencyState,
          travelAgencyCountry,
          rateCategoryCode,
          rateTypeCode,
          ratename,
          rateCategory_PHG,
          rooms,
          nights,
          RoomNights,
          currency,
          reservationRevenue,
          reservationRevenueUSD,
          IPreferCount,
          IPreferChargesUSD,
		  IPreferChargesHotelCurrency,
          IPreferCost
         )
                SELECT ISNULL(billy.synxisID,billy.openHospitalityCode), -- synxisid
                       billy.arrivalDate,
                       missingmrt.confirmationDate,
                       billy.confirmationNumber, -- confirmationnumber -- do not inflate synxis booking count because the billy charge has a different month
                       0, -- do not inflate synxis booking count because the billy charge has a different month
                       missingMRT.billingDescription, -- billing description
                       missingMRT.channel, -- as channel ,
                       missingMRT.Status, -- as status
                       missingMRT.SecondarySource, -- as SecondarySource ,
                       missingMRT.subSourceReportLabel, -- as subSourceReportLabel ,
                       missingMRT.CROcode, -- as CROcode ,
                       NULL, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
                       IM.iatamanager, --as IataGam ,
                       Core.dbo.GetIATAGroups(missingMRT.IATANumber, missingMRT.confirmationdate) AS IATAGroups,
                       missingMRT.IATANumber, -- as IATANumber,
                       missingMRT.travelAgencyName, --as travelAgencyName,
                       missingMRT.travelAgencyCity, -- as travelAgencyCity,
                       missingMRT.travelAgencyState, -- as travelAgencyState,
                       missingMRT.travelAgencyCountry, --as travelAgencyCountry,
                       missingMRT.rateCategoryCode, --as rateCategoryCode,
                       missingMRT.rateTypeCode, -- as rateTypeCode,
                       missingMRT.rateTypeName, --as ratename ,
                       CASE
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'GOL'
                           THEN 'GOLF'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'PKG'
                           THEN 'PKG'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'PRO'
                           THEN 'PRO'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'MKT'
                           THEN 'MKT'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'CON'
                           THEN 'CON'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'NEG'
                           THEN 'NEG'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'BAR'
                           THEN 'BAR'
                           WHEN LEFT(missingMRT.rateTypeCode, 3) = 'DIS'
                           THEN 'DIS'
                           ELSE CASE
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'GOL'
                                    THEN 'GOLF'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'PKG'
                                    THEN 'PKG'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'PRO'
                                    THEN 'PRO'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'MKT'
                                    THEN 'MKT'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'CON'
                                    THEN 'CON'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'NEG'
                                    THEN 'NEG'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'BAR'
                                    THEN 'BAR'
                                    WHEN LEFT(missingMRT.rateCategoryCode, 3) = 'DIS'
                                    THEN 'DIS'
                                    ELSE 'Other'
                                END
                       END, -- as rateCategory_PHG,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD,
                       0, -- IPreferCount
                       CAST(0.00 AS DECIMAL(18, 2)), -- IPreferChargesUSD
					   CAST(0.00 AS DECIMAL(18, 2)), -- IPreferChargesHotelCurrency
                       CAST(0.00 AS DECIMAL(18, 2))   -- as IPreferCost
                FROM #rpt_BillyCharges billy
                     JOIN superset.dbo.mostRecentTransactionsReporting missingMRT ON billy.confirmationNumber = missingMRT.confirmationNumber
                     LEFT OUTER JOIN #rpt_IATAManager IM WITH (nolock) ON missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS -- get only confirmation numbers that have not been added yet
                     LEFT OUTER JOIN #rpt_Main mrt ON billy.confirmationnumber = mrt.confirmationnumber
                WHERE mrt.confirmationNumber IS NULL
                      AND (@hotelcode IS NULL
                           OR billy.hotelcode = @hotelcode)
                GROUP BY billy.HotelCode,
						 ISNULL(billy.synxisID,billy.openHospitalityCode),
                         billy.arrivalDate,
                         missingMRT.confirmationDate,
                         billy.confirmationNumber,
                         missingMRT.billingDescription,
                         missingMRT.channel,
                         missingMRT.Status,
                         missingMRT.SecondarySource,
                         missingMRT.subSourceReportLabel,
                         missingMRT.CROcode,
                         IM.iatamanager,
                         IATAGroups,
                         missingMRT.IATANumber,
                         missingMRT.confirmationdate,
                         missingMRT.travelAgencyName,
                         missingMRT.travelAgencyCity,
                         missingMRT.travelAgencyState,
                         missingMRT.travelAgencyCountry,
                         missingMRT.rateCategoryCode,
                         missingMRT.rateTypeCode,
                         missingMRT.rateTypeName;



         -- get IPrefer Counts and Sums by current billing date, using the following logic:
         --		if matching a current MRT record - use the MRT data for channel, source, etc
         --		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
         --		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
         INSERT INTO #rpt_Main
         (crsHotelID,
          arrivaldate,
          confirmationdate,
          confirmationnumber,
          SynXisCount,
          billingDescription,
          Channel,
          status,
          SecondarySource,
          subSourceReportLabel,
          CROcode,
          cost,
          IataGam,
          IATAGroups,
          IATANumber,
          travelAgencyName,
          travelAgencyCity,
          travelAgencyState,
          travelAgencyCountry,
          rateCategoryCode,
          rateTypeCode,
          ratename,
          rateCategory_PHG,
          rooms,
          nights,
          RoomNights,
          currency,
          reservationRevenue,
          reservationRevenueUSD,
          IPreferCount,
          IPreferChargesUSD,
		  IPreferChargesHotelCurrency,
          IPreferCost
         )
                SELECT ISNULL(H.synxisID,H.openHospitalityCode), -- synxisid
                       IP.Billing_Date, -- the IPrefer still need to be categorized as Month(MRT.ArrivalDate)
                       IP.Booking_Date, -- for currency conversions
                       NULL, -- confirmationnumber -- do not inflate synxis booking count because of an IPrefer match
                       0, -- do not inflate synxis booking count because the billy charge has a different month
                       COALESCE(MRT.billingDescription, 'Non-Matching IPrefer Reservation'), -- billing description
                       COALESCE(MRT.channel,
                                    CASE
                                        WHEN IP.arrival_date BETWEEN @arrivalStart AND @arrivalEnd
                                        THEN 'IPrefer Manual Entry'
                                        ELSE 'IPrefer Prior Period'
                                    END), -- as channel ,
                       COALESCE(MRT.Status, ''), -- as status
                       COALESCE(MRT.SecondarySource, ''), -- as SecondarySource ,
                       COALESCE(MRT.subSourceReportLabel, ''), -- as subSourceReportLabel ,
                       COALESCE(MRT.CROcode, ''), -- as CROcode ,
                       NULL, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
                       COALESCE(MRT.IataGam, ''), -- as IataGam ,
                       COALESCE(MRT.IATAGroups, ''), -- as IATAGroups ,
                       COALESCE(MRT.IATANumber, ''), -- as IATANumber,
                       COALESCE(MRT.travelAgencyName, ''), --as travelAgencyName,
                       COALESCE(MRT.travelAgencyCity, ''), -- as travelAgencyCity,
                       COALESCE(MRT.travelAgencyState, ''), -- as travelAgencyState,
                       COALESCE(MRT.travelAgencyCountry, ''), --as travelAgencyCountry,
                       COALESCE(MRT.rateCategoryCode, ''), --as rateCategoryCode,
                       COALESCE(MRT.rateTypeCode, ''), -- as rateTypeCode,
                       COALESCE(MRT.ratename, ''), -- as ratename,
                       COALESCE(MRT.rateCategory_PHG, ''), -- as rateCategory_PHG,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL,
                       NULL, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD
                       COUNT(DISTINCT IP.Booking_ID), -- IPreferCount
					   SUM(IP.Billable_Amount_USD), --Sum(dbo.convertCurrencyToUSD(IP.Billable_Amount_Hotel_Currency, IP.Hotel_Currency, IP.Arrival_Date)), -- IPreferChargesUSD
					   SUM(CASE WHEN H.hotelCurrency = IP.Hotel_Currency THEN IP.Billable_Amount_Hotel_Currency ELSE dbo.convertCurrencyXE(IP.Billable_Amount_USD,'USD',H.hotelCurrency,IP.Arrival_Date) END), --IPreferChargesHotelCurrency
                       SUM(IP.Reservation_Amount_USD * 0.02) -- as IPreferCost
                FROM BSI.IPrefer_Charges IP
					 LEFT OUTER JOIN #Hotels H ON IP.Hotel_Code = H.hotelCode
                     LEFT OUTER JOIN #rpt_Main mrt ON IP.booking_id = mrt.confirmationnumber
                WHERE IP.billing_date BETWEEN @arrivalStart AND @arrivalEnd
                      AND (@hotelcode IS NULL
                           OR IP.Hotel_Code = @hotelcode)
                GROUP BY IP.Hotel_Code,
						ISNULL(H.synxisID,H.openHospitalityCode),
						H.hotelCurrency,
						IP.Hotel_Currency,
                         IP.Billing_Date,
                         IP.Booking_Date,
                         MRT.arrivalDate,
                         MRT.billingDescription,
                         MRT.channel,
                         MRT.Status,
                         CASE
                             WHEN IP.arrival_date BETWEEN @arrivalStart AND @arrivalEnd
                             THEN 'IPrefer Manual Entry'
                             ELSE 'IPrefer Prior Period'
                         END,
                         MRT.SecondarySource,
                         MRT.subSourceReportLabel,
                         MRT.CROcode,
                         MRT.cost,
                         MRT.IataGam,
                         MRT.IATAGroups,
                         MRT.IATANumber,
                         MRT.travelAgencyName,
                         MRT.travelAgencyCity,
                         MRT.travelAgencyState,
                         MRT.travelAgencyCountry,
                         MRT.rateCategoryCode,
                         MRT.rateTypeCode,
                         MRT.ratename,
                         MRT.rateCategory_PHG;
         IF @Extended = 0
             SELECT main.crsHotelID,
                    hotels.hotelCode,
                    hotels.hotelName,
                    hotels.primaryBrand,
                    hotels.gpSalesTerritory,
                    hotels.AMD,
                    hotels.RD,
                    hotels.RAM,
                    hotels.AccountManager,
                    hotels.geographicRegion,
                    hotels.country,
                    hotels.city,
                    hotels.state,
                    hotels.hotelRooms,
                    hotels.hotelStatus,
                    hotels.CRMClassification,
                    hotels.hotelCurrency,
                    main.billingDescription,
                    main.channel,
                    main.secondarySource,
                    main.subSourceReportLabel,
                    main.CROcode,
                    YEAR(main.arrivaldate) ArrivalYear,
                    MONTH(main.arrivaldate) ArrivalMonth,

				
                    /*Hiren asked that "Confirmed Cancelled" bookings be removed*/

                    SUM(main.SynXisCount) AS ConfirmedSynxisBookingCount,
                    --SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
                    --SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount ,
                    SUM(main.reservationrevenueUSD) ConfirmedRevenueUSD, 
                    --SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
                    --SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
                    SUM(main.roomNights) AS ConfirmedRoomNights,
                    --SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
                    --SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
                    SUM(main.cost) AS ConfirmedTotalCost, 
                    --SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
                    --SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
                    SUM(main.reservationRevenueUSD) / SUM(main.roomNights) AS avgNightlyRateUSD,
                    SUM(main.nights) / CASE
                                           WHEN COUNT(main.confirmationnumber) = 0
                                           THEN 1
                                           ELSE COUNT(main.confirmationnumber)
                                       END AS avgLengthOfStay, 

                    -- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
                    COUNT(DISTINCT billy.confirmationnumber) AS BillyBookingCount,
                    COUNT(CASE
                              WHEN billy.gpSiteID IN(1)
                              THEN billy.confirmationnumber
                              ELSE NULL
                          END) AS PHGSiteBillyBookingCount,
                    COUNT(CASE
                              WHEN billy.gpSiteID IN(6, 8)
                              THEN billy.confirmationnumber
                              ELSE NULL
                          END) AS HHASiteBillyBookingCount,
                    SUM(billy.bookingChargesUSD) AS BillyBookingCharges,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.bookingChargesUSD
                            ELSE 0
                        END) AS PHGSiteBillyBookingCharges,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.bookingChargesUSD
                            ELSE 0
                        END) AS HHASiteBillyBookingCharges,
                    SUM(billy.commissionsUSD) AS commissions,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.commissionsUSD
                            ELSE 0
                        END) AS PHGSiteCommissions,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.commissionsUSD
                            ELSE 0
                        END) AS HHASiteCommissions,
                    SUM(billy.surchargesUSD) AS surcharges,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.surchargesUSD
                            ELSE 0
                        END) AS PHGSiteSurcharges,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.surchargesUSD
                            ELSE 0
                        END) AS HHASiteSurcharges,
                    SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD) BookAndComm,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.bookingChargesUSD + billy.commissionsUSD
                            ELSE 0
                        END) AS PHGSiteBookAndComm,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.bookingChargesUSD + billy.commissionsUSD
                            ELSE 0
                        END) AS HHASiteBookAndComm,
                    SUM(main.cost) / CASE
                                         WHEN COUNT(main.confirmationnumber) = 0
                                         THEN 1
                                         ELSE COUNT(main.confirmationnumber)
                                     END AS costperbooking,
                    SUM(main.IPreferCount) AS IPreferBookingCount,
                    SUM(main.IPreferChargesUSD) AS IPreferCharges,
                    SUM(main.IPreferCost) AS IPreferCost,
                    CASE
                        WHEN COALESCE(SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD), 0) = 0
                        THEN COUNT(billy.confirmationnumber)
                        ELSE 0
                    END AS BillyBookingWithZeroBookAndComm,

-- adding all sum fields in hotel billing currency from USD
                    SUM(Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate)) AS ConfirmedRevenueHotelCurrency,
--SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end)
-- ConfirmedRevenueHotelCurrency ,
--SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) 
--CancelledRevenueHotelCurrency ,					
                    SUM(billy.bookingChargesHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
						AS BillyBookingChargesHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.bookingChargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS PHGSiteBillyBookingChargesHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.bookingChargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS HHASiteBillyBookingChargesHotelCurrency,
                    SUM(billy.commissionsHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
						AS CommissionsHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.commissionsHotelCurrency --Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS PHGSiteCommissionsHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.commissionsHotelCurrency --Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS HHASiteCommissionsHotelCurrency,
                    SUM(billy.surchargesHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
						AS SurchargesHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN billy.surchargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS PHGSiteSurchargesHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN billy.surchargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS HHASiteSurchargesHotelCurrency,
                    SUM(billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
						AS BookAndCommHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(1)
                            THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) --Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS PHGSiteBookAndCommHotelCurrency,
                    SUM(CASE
                            WHEN billy.gpSiteID IN(6, 8)
                            THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) --Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                            ELSE 0
                        END) AS HHASiteBookAndCommHotelCurrency,
                    SUM(main.IPreferChargesHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate)) 
						AS IPreferChargesHotelCurrency 
			 INTO #Results
             FROM #Hotels AS hotels
                  LEFT OUTER JOIN #rpt_Main AS main ON main.crsHotelID = CASE main.crsSourceID
                                                                             WHEN 2
                                                                             THEN hotels.openHospitalityCode
                                                                             ELSE hotels.synxisID
                                                                         END
                  LEFT OUTER JOIN #rpt_BillyCharges AS billy ON main.confirmationnumber = billy.confirmationNumber
             GROUP BY YEAR(main.arrivaldate),
                      MONTH(main.arrivaldate),
                      main.crsHotelID,
                      hotels.hotelCode,
                      hotels.hotelName,
                      hotels.primaryBrand,
                      hotels.gpSalesTerritory,
                      hotels.AMD,
                      hotels.RD,
                      hotels.RAM,
                      hotels.AccountManager,
                      hotels.geographicRegion,
                      hotels.country,
                      hotels.city,
                      hotels.state,
                      hotels.hotelRooms,
                      hotels.hotelStatus,
                      hotels.CRMClassification,
                      hotels.hotelCurrency,
                      main.Channel,
                      main.billingDescription,
                      main.SecondarySource,
                      main.subSourceReportLabel,
                      main.CROcode --,billy.gpSiteID -- removed 2015-01-06 TMG -- put MIN(billy.gpsiteid) in above select
             ORDER BY main.crsHotelID,
                      YEAR(main.arrivaldate),
                      MONTH(main.arrivaldate),
                      main.channel,
                      main.secondarysource,
                      main.subsourcereportlabel;
         ELSE -- @extended = 1, returns different resultset
         SELECT main.crsHotelID,
                hotels.hotelCode,
                hotels.hotelName,
                hotels.primaryBrand,
                hotels.gpSalesTerritory,
                hotels.AMD,
                hotels.RD,
                hotels.RAM,
                hotels.AccountManager,
                hotels.geographicRegion,
                hotels.country,
                hotels.city,
                hotels.state,
                hotels.hotelRooms,
                hotels.hotelStatus,
                hotels.CRMClassification,
                hotels.hotelcurrency,
                main.billingDescription,
                main.channel,
                main.secondarySource,
                main.subSourceReportLabel,
                main.CROcode,
                main.IataGam,
                REPLACE(main.IATAGroups, ',', '') AS IATAGroups,
                main.IATANumber,
                REPLACE(main.travelAgencyName, ',', '') AS travelAgencyName,
                main.travelAgencyCity,
                main.travelAgencyState,
                main.travelAgencyCountry,
                main.rateCategoryCode,
                main.rateTypeCode,
                main.ratename,
                main.rateCategory_PHG,
                YEAR(main.arrivaldate) ArrivalYear,
                MONTH(main.arrivaldate) ArrivalMonth,
                SUM(main.SynXisCount) AS ConfirmedSynxisBookingCount,
                --SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
                --SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount ,
                SUM(main.reservationrevenueUSD) ConfirmedRevenueUSD, 
                --SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
                --SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
                SUM(main.roomNights) AS ConfirmedRoomNights,
                --SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
                --SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
                SUM(main.cost) AS ConfirmedTotalCost, 
                --SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
                --SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
                SUM(main.reservationRevenueUSD) / SUM(main.roomNights) AS avgNightlyRateUSD,
                SUM(main.nights) / CASE
                                       WHEN COUNT(main.confirmationnumber) = 0
                                       THEN 1
                                       ELSE COUNT(main.confirmationnumber)
                                   END AS avgLengthOfStay, 

                -- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
                COUNT(DISTINCT billy.confirmationnumber) AS BillyBookingCount,
                COUNT(CASE
                          WHEN billy.gpSiteID IN(1)
                          THEN billy.confirmationnumber
                          ELSE NULL
                      END) AS PHGSiteBillyBookingCount,
                COUNT(CASE
                          WHEN billy.gpSiteID IN(6, 8)
                          THEN billy.confirmationnumber
                          ELSE NULL
                      END) AS HHASiteBillyBookingCount,
                SUM(billy.bookingChargesUSD) AS BillyBookingCharges,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.bookingChargesUSD
                        ELSE 0
                    END) AS PHGSiteBillyBookingCharges,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.bookingChargesUSD
                        ELSE 0
                    END) AS HHASiteBillyBookingCharges,
                SUM(billy.commissionsUSD) AS commissions,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.commissionsUSD
                        ELSE 0
                    END) AS PHGSiteCommissions,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.commissionsUSD
                        ELSE 0
                    END) AS HHASiteCommissions,
                SUM(billy.surchargesUSD) AS surcharges,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.surchargesUSD
                        ELSE 0
                    END) AS PHGSiteSurcharges,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.surchargesUSD
                        ELSE 0
                    END) AS HHASiteSurcharges,
                SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD) BookAndComm,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.bookingChargesUSD + billy.commissionsUSD
                        ELSE 0
                    END) AS PHGSiteBookAndComm,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.bookingChargesUSD + billy.commissionsUSD
                        ELSE 0
                    END) AS HHASiteBookAndComm,
                SUM(main.cost) / CASE
                                     WHEN COUNT(main.confirmationnumber) = 0
                                     THEN 1
                                     ELSE COUNT(main.confirmationnumber)
                                 END AS costperbooking,
                SUM(main.IPreferCount) AS IPreferBookingCount,
                SUM(main.IPreferChargesUSD) AS IPreferCharges,
                SUM(main.IPreferCost) AS IPreferCost,
                CASE
                    WHEN COALESCE(SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD), 0) = 0
                    THEN COUNT(billy.confirmationnumber)
                    ELSE 0
                END AS BillyBookingWithZeroBookAndComm,
				
-- adding all sum fields in hotel billing currency from USD
                SUM(Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate)) AS ConfirmedRevenueHotelCurrency,
--SUM(CASE WHEN main.status <> 'Cancelled' THEN Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) ELSE 0 END) 
--ConfirmedRevenueHotelCurrency ,
--SUM(CASE WHEN main.status = 'Cancelled' THEN Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) ELSE 0 END) 
--CancelledRevenueHotelCurrency ,					
                SUM(billy.bookingChargesHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
					AS BillyBookingChargesHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.bookingChargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS PHGSiteBillyBookingChargesHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.bookingChargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS HHASiteBillyBookingChargesHotelCurrency,
                SUM(billy.commissionsHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
					AS CommissionsHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.commissionsHotelCurrency --Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS PHGSiteCommissionsHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.commissionsHotelCurrency --Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS HHASiteCommissionsHotelCurrency,
                SUM(billy.surchargesHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
					AS SurchargesHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN billy.surchargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS PHGSiteSurchargesHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN billy.surchargesHotelCurrency --Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS HHASiteSurchargesHotelCurrency,
                SUM(billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)) 
					AS BookAndCommHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(1)
                        THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) --Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS PHGSiteBookAndCommHotelCurrency,
                SUM(CASE
                        WHEN billy.gpSiteID IN(6, 8)
                        THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) --Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate)
                        ELSE 0
                    END) AS HHASiteBookAndCommHotelCurrency,
                SUM(main.IPreferChargesHotelCurrency) --SUM(Superset.dbo.convertCurrencyXE(main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate)) 
					AS IPreferChargesHotelCurrency 
		 INTO #Results_Extended
         FROM #Hotels AS hotels
              LEFT OUTER JOIN #rpt_Main AS main ON main.crsHotelID = CASE main.crsSourceID
                                                                         WHEN 2
                                                                         THEN hotels.openHospitalityCode
                                                                         ELSE hotels.synxisID
                                                                     END
              LEFT OUTER JOIN #rpt_BillyCharges AS billy ON main.confirmationnumber = billy.confirmationNumber
         GROUP BY YEAR(main.arrivaldate),
                  MONTH(main.arrivaldate),
                  main.crsHotelID,
                  hotels.hotelCode,
                  hotels.hotelName,
                  hotels.primaryBrand,
                  hotels.gpSalesTerritory,
                  hotels.AMD,
                  hotels.RD,
                  hotels.RAM,
                  hotels.AccountManager,
                  hotels.geographicRegion,
                  hotels.country,
                  hotels.city,
                  hotels.state,
                  hotels.hotelRooms,
                  hotels.hotelStatus,
                  hotels.CRMClassification,
                  hotels.hotelCurrency,
                  main.crsHotelID,
                  main.Channel,
                  main.billingDescription,
                  main.SecondarySource,
                  main.subSourceReportLabel,
                  main.CROcode,
                  hotels.hotelCurrency,
                  main.IataGam,
                  IATAGroups,
                  main.IATANumber,
                  travelAgencyName,
                  main.travelAgencyCity,
                  main.travelAgencyState,
                  main.travelAgencyCountry,
                  main.rateCategoryCode,
                  main.rateTypeCode,
                  main.ratename,
                  main.rateCategory_PHG --, billy.gpSiteID -- TMG removed and put MIN(billy.gpSiteID) in select -- 2015-01-06
         ORDER BY main.crsHotelID,
                  YEAR(main.arrivaldate),
                  MONTH(main.arrivaldate),
                  main.channel,
                  main.secondarysource,
                  main.subsourcereportlabel;
         IF @extended = 0
             SELECT *
             FROM #Results
             WHERE NOT(BillyBookingCount = 0
                       AND ConfirmedSynxisBookingCount = 0
                       --AND		CancelledSynxisBookingCount = 0
                       AND IPreferBookingCount = 0)
             ORDER BY crsHotelID,
                      ArrivalYear,
                      ArrivalMonth,
                      channel,
                      secondarysource,
                      subsourcereportlabel;
         ELSE -- @extended = 1, returns different resultset
         SELECT *
         FROM #Results_Extended
         WHERE NOT(BillyBookingCount = 0
                   AND ConfirmedSynxisBookingCount = 0
                   --AND		CancelledSynxisBookingCount = 0
                   AND IPreferBookingCount = 0)
         ORDER BY crsHotelID,
                  ArrivalYear,
                  ArrivalMonth,
                  channel,
                  secondarysource,
                  subsourcereportlabel;
         IF OBJECT_ID('#usdExchange ') IS NOT NULL
             DROP TABLE #usdExchange;
         IF OBJECT_ID('#Hotels') IS NOT NULL
             DROP TABLE #Hotels;
         IF OBJECT_ID('#rpt_Main') IS NOT NULL
             DROP TABLE #rpt_Main;
         IF OBJECT_ID('#rpt_BillyCharges') IS NOT NULL
             DROP TABLE #rpt_BillyCharges;
         IF OBJECT_ID('#rpt_IPreferCharges') IS NOT NULL
             DROP TABLE #rpt_IPreferCharges;
         IF OBJECT_ID('#rpt_IATAManager') IS NOT NULL
             DROP TABLE #rpt_IATAManager;
         IF OBJECT_ID('#profitabilityreport_tmg') IS NOT NULL
             DROP TABLE #profitabilityreport_tmg;
         IF OBJECT_ID('#profitabilityreport_tmg_ext') IS NOT NULL
             DROP TABLE #profitabilityreport_tmg_ext;
         IF OBJECT_ID('#profitabilityreport_tmg_ext') IS NOT NULL
             DROP TABLE #Results;
         IF OBJECT_ID('#profitabilityreport_tmg_ext') IS NOT NULL
             DROP TABLE #Results_Extended;
     END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReportResultsExtended]'
GO
CREATE TABLE [Reporting].[ProfitabilityReportResultsExtended]
(
[crsHotelID] [int] NULL,
[hotelCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[primaryBrand] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[gpSalesTerritory] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMD] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RD] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RAM] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountManager] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[geographicRegion] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[country] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[city] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelRooms] [int] NULL,
[hotelStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRMClassification] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelcurrency] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[billingDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[channel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondarySource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSourceReportLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROcode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IataGam] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IATAGroups] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IATANumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[travelAgencyName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[travelAgencyCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[travelAgencyState] [nchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[travelAgencyCountry] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateCategoryCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateTypeCode] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ratename] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateCategory_PHG] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArrivalYear] [int] NULL,
[ArrivalMonth] [int] NULL,
[ConfirmedSynxisBookingCount] [int] NULL,
[ConfirmedRevenueUSD] [decimal] (38, 2) NULL,
[ConfirmedRoomNights] [int] NULL,
[ConfirmedTotalCost] [decimal] (38, 2) NULL,
[avgNightlyRateUSD] [decimal] (38, 6) NULL,
[avgLengthOfStay] [int] NULL,
[BillyBookingCount] [int] NULL,
[PHGSiteBillyBookingCount] [int] NULL,
[HHASiteBillyBookingCount] [int] NULL,
[BillyBookingCharges] [decimal] (38, 2) NULL,
[PHGSiteBillyBookingCharges] [decimal] (38, 2) NULL,
[HHASiteBillyBookingCharges] [decimal] (38, 2) NULL,
[commissions] [decimal] (38, 2) NULL,
[PHGSiteCommissions] [decimal] (38, 2) NULL,
[HHASiteCommissions] [decimal] (38, 2) NULL,
[surcharges] [decimal] (38, 2) NULL,
[PHGSiteSurcharges] [decimal] (38, 2) NULL,
[HHASiteSurcharges] [decimal] (38, 2) NULL,
[BookAndComm] [decimal] (38, 2) NULL,
[PHGSiteBookAndComm] [decimal] (38, 2) NULL,
[HHASiteBookAndComm] [decimal] (38, 2) NULL,
[costperbooking] [decimal] (38, 6) NULL,
[IPreferBookingCount] [int] NULL,
[IPreferCharges] [decimal] (38, 2) NULL,
[IPreferCost] [decimal] (38, 2) NULL,
[BillyBookingWithZeroBookAndComm] [int] NULL,
[ConfirmedRevenueHotelCurrency] [decimal] (38, 2) NULL,
[BillyBookingChargesHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteBillyBookingChargesHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteBillyBookingChargesHotelCurrency] [decimal] (38, 2) NULL,
[CommissionsHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteCommissionsHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteCommissionsHotelCurrency] [decimal] (38, 2) NULL,
[SurchargesHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteSurchargesHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteSurchargesHotelCurrency] [decimal] (38, 2) NULL,
[BookAndCommHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteBookAndCommHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteBookAndCommHotelCurrency] [decimal] (38, 2) NULL,
[IPreferChargesHotelCurrency] [decimal] (38, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReportResults]'
GO
CREATE TABLE [Reporting].[ProfitabilityReportResults]
(
[crsHotelID] [int] NULL,
[hotelCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[primaryBrand] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[gpSalesTerritory] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMD] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RD] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RAM] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountManager] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[geographicRegion] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[country] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[city] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelRooms] [int] NULL,
[hotelStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRMClassification] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelCurrency] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[billingDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[channel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondarySource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSourceReportLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROcode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArrivalYear] [int] NULL,
[ArrivalMonth] [int] NULL,
[ConfirmedSynxisBookingCount] [int] NULL,
[ConfirmedRevenueUSD] [decimal] (38, 2) NULL,
[ConfirmedRoomNights] [int] NULL,
[ConfirmedTotalCost] [decimal] (38, 2) NULL,
[avgNightlyRateUSD] [decimal] (38, 6) NULL,
[avgLengthOfStay] [int] NULL,
[BillyBookingCount] [int] NULL,
[PHGSiteBillyBookingCount] [int] NULL,
[HHASiteBillyBookingCount] [int] NULL,
[BillyBookingCharges] [decimal] (38, 2) NULL,
[PHGSiteBillyBookingCharges] [decimal] (38, 2) NULL,
[HHASiteBillyBookingCharges] [decimal] (38, 2) NULL,
[commissions] [decimal] (38, 2) NULL,
[PHGSiteCommissions] [decimal] (38, 2) NULL,
[HHASiteCommissions] [decimal] (38, 2) NULL,
[surcharges] [decimal] (38, 2) NULL,
[PHGSiteSurcharges] [decimal] (38, 2) NULL,
[HHASiteSurcharges] [decimal] (38, 2) NULL,
[BookAndComm] [decimal] (38, 2) NULL,
[PHGSiteBookAndComm] [decimal] (38, 2) NULL,
[HHASiteBookAndComm] [decimal] (38, 2) NULL,
[costperbooking] [decimal] (38, 6) NULL,
[IPreferBookingCount] [int] NULL,
[IPreferCharges] [decimal] (38, 2) NULL,
[IPreferCost] [decimal] (38, 2) NULL,
[BillyBookingWithZeroBookAndComm] [int] NULL,
[ConfirmedRevenueHotelCurrency] [decimal] (38, 2) NULL,
[BillyBookingChargesHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteBillyBookingChargesHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteBillyBookingChargesHotelCurrency] [decimal] (38, 2) NULL,
[CommissionsHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteCommissionsHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteCommissionsHotelCurrency] [decimal] (38, 2) NULL,
[SurchargesHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteSurchargesHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteSurchargesHotelCurrency] [decimal] (38, 2) NULL,
[BookAndCommHotelCurrency] [decimal] (38, 2) NULL,
[PHGSiteBookAndCommHotelCurrency] [decimal] (38, 2) NULL,
[HHASiteBookAndCommHotelCurrency] [decimal] (38, 2) NULL,
[IPreferChargesHotelCurrency] [decimal] (38, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReportBCD]'
GO

CREATE PROCEDURE [Reporting].[ProfitabilityReportBCD]
	@arrivalStart DATE,
	@arrivalEnd   DATE,
	@hotelcode    VARCHAR(10) = NULL,
	@Extended     BIT         = 0
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- DELETE EXISTING DATES -------------------------------
	IF @Extended = 0
		DELETE [Reporting].[ProfitabilityReportResults]
		WHERE CONVERT(date, CONVERT(varchar(20),ArrivalYear) + '-' + CONVERT(varchar(20),ArrivalMonth) + '-01') >= @arrivalStart
			AND CONVERT(date, CONVERT(varchar(20),ArrivalYear) + '-' + CONVERT(varchar(20),ArrivalMonth) + '-01') < @arrivalEnd
	ELSE
		DELETE [Reporting].[ProfitabilityReportResultsExtended]
		WHERE CONVERT(date, CONVERT(varchar(20),ArrivalYear) + '-' + CONVERT(varchar(20),ArrivalMonth) + '-01') >= @arrivalStart
			AND CONVERT(date, CONVERT(varchar(20),ArrivalYear) + '-' + CONVERT(varchar(20),ArrivalMonth) + '-01') < @arrivalEnd
	--------------------------------------------------------

	-- CREATE TEMP TABLES ----------------------------------
	IF OBJECT_ID('tempdb..#HOTELS') IS NOT NULL
		DROP TABLE #HOTELS;
	CREATE TABLE #HOTELS
	(
		synxisID int,
		openHospitalityCode nvarchar(10),
		hotelCode nvarchar(20),
		hotelName nvarchar(250),
		primaryBrand nvarchar(6),
		gpSalesTerritory varchar(15),
		AMD nvarchar(200),
		RD nvarchar(200),
		RAM nvarchar(200),
		AccountManager nvarchar(200),
		CRMClassification nvarchar(255),
		geographicRegion nvarchar(255),
		country nvarchar(100),
		city nvarchar(250),
		state nvarchar(100),
		hotelRooms int,
		hotelStatus nvarchar(255),
		hotelCurrency varchar(15),
		gpSiteID char(2)
	)


	IF OBJECT_ID('tempdb..#RPT_IATAManager') IS NOT NULL
		DROP TABLE #RPT_IATAManager;
	CREATE TABLE #RPT_IATAManager(accountnumber nvarchar(20),iatamanager nvarchar(200))


	IF OBJECT_ID('tempdb..#RPT_MAIN') IS NOT NULL
		DROP TABLE #RPT_MAIN;
	CREATE TABLE #RPT_MAIN
	(
		crsSourceID smallint,
		crsHotelID int,
		arrivalDate date,
		confirmationDate date,
		confirmationNumber nvarchar(20),
		SynXisCount int,
		status nvarchar(10),
		channel nvarchar(50),
		CROcode nvarchar(50),
		billingDescription nvarchar(255),
		SecondarySource nvarchar(50),
		subSourceReportLabel nvarchar(100),
		cost decimal(6,2),
		IataGam nvarchar(200),
		IATAGroups varchar(MAX),
		IATANumber nvarchar(50),
		travelAgencyName nvarchar(450),
		travelAgencyCity nvarchar(50),
		travelAgencyState nchar(4),
		travelAgencyCountry nvarchar(80),
		rateCategoryCode nvarchar(20),
		rateTypeCode nvarchar(450),
		ratename nvarchar(500),
		rateCategory_PHG varchar(5),
		rooms int,
		nights int,
		RoomNights int,
		currency nvarchar(12),
		reservationRevenue decimal(38,2),
		reservationRevenueUSD decimal(18,2),
		IPreferCount int,
		IPreferChargesUSD decimal(18,2),
		IPreferChargesHotelCurrency decimal(18,2),
		IPreferCost decimal(18,2)
	)

	IF OBJECT_ID('tempdb..#RPT_BillyCharges') IS NOT NULL
		DROP TABLE #RPT_BillyCharges;
	CREATE TABLE #RPT_BillyCharges
	(
		confirmationNumber nvarchar(20),
		hotelCode nvarchar(10),
		synxisID int,
		openHospitalityCode nvarchar(10),
		gpSiteID char(2),
		arrivalDate date,
		bookingChargesUSD decimal(38,2),
		commissionsUSD decimal(38,2),
		surchargesUSD decimal(38,2),
		bookingChargesHotelCurrency decimal(38,2),
		commissionsHotelCurrency decimal(38,2),
		surchargesHotelCurrency decimal(38,2)
	)
	--------------------------------------------------------

	-- POPULATE #HOTELS ------------------------------------
	INSERT INTO #HOTELS(synxisID,openHospitalityCode,hotelCode,hotelName,primaryBrand,
						gpSalesTerritory,AMD,RD,
						RAM,AccountManager,CRMClassification,
						geographicRegion,country,city,state,hotelRooms,
						hotelStatus,
						hotelCurrency,gpSiteID)
	SELECT H.synxisID,H.openHospitalityCode,H.code AS hotelCode,H.hotelName AS hotelName,H.mainBrandCode AS primaryBrand,
			LTRIM(RTRIM(RM00101.SALSTERR)) AS gpSalesTerritory,H.phg_areamanageridName AS AMD,H.phg_regionalmanageridName AS RD,
			H.phg_revenueaccountmanageridName AS RAM,H.phg_AccountManagerName AS AccountManager,CRM.accountclassificationcodename AS CRMClassification,
			H.geographicRegionName AS geographicRegion,H.shortName AS country,H.physicalCity AS city,H.subAreaName AS state,H.totalRooms AS hotelRooms,
			H.statuscodename hotelStatus,
			LTRIM(RTRIM(RM00101.CURNCYID)) AS hotelCurrency,B.gpSiteID
	FROM Core.dbo.hotelsReporting H
		LEFT OUTER JOIN IC.dbo.RM00101 ON H.code = RM00101.CUSTNMBR
		LEFT OUTER JOIN LocalCRM.dbo.account CRM ON H.crmGuid = CRM.accountid
		LEFT OUTER JOIN Core.dbo.Brands B ON H.mainBrandCode = B.code
	WHERE(synxisID > 0
		OR openHospitalityCode > 0)
		AND (@hotelcode IS NULL
		OR H.code = @hotelcode);
	
	CREATE CLUSTERED INDEX ix_Hotels ON #HOTELS(synxisID,openHospitalityCode);
	--------------------------------------------------------

	-- POPULATE #RPT_IATAManager ---------------------------
	INSERT INTO #RPT_IATAManager(accountnumber,iataManager)
	SELECT accountnumber,MIN(phg_iataglobalaccountmanageridName) iatamanager
	FROM LocalCRM.dbo.account
	WHERE phg_iataglobalaccountmanageridName IS NOT NULL
		AND accountnumber IS NOT NULL
	GROUP BY accountnumber;
	
	CREATE CLUSTERED INDEX ix_iatamanager ON #RPT_IATAManager(accountnumber);
	--------------------------------------------------------

	-- POPULATE #RPT_MAIN ----------------------------------
         --	build master MRT table - get all the data ONCE (not multiple reads of MRT)
         --  cost,sums,counts,channels,sources etc,
         --  a field that indicates whether the record is billable 
	INSERT INTO #RPT_MAIN(CRSSourceID,CRSHotelID,arrivalDate,confirmationDate,confirmationNumber,SynxisCount,status,channel,CROcode,billingDescription,
							secondarySource,subSourceReportLabel,cost,IataGam,IATAGroups,IATANumber,travelAgencyName,travelAgencyCity,travelAgencyState,
							travelAgencyCountry,rateCategoryCode,rateTypeCode,ratename,rateCategory_PHG,rooms,nights,RoomNights,currency,
							reservationRevenue,reservationRevenueUSD,IPreferCount,IPreferChargesUSD,IPreferChargesHotelCurrency,IPreferCost)
							
	SELECT mrt.crsSourceID,CASE mrt.crsSourceID WHEN 2 THEN mrt.openhospitalityID ELSE mrt.hotelID END AS crsHotelID,
			mrt.arrivalDate,mrt.confirmationDate,mrt.confirmationNumber,1 AS SynXisCount,mrt.status,mrt.channel,mrt.CROcode,mrt.billingDescription,
			mrt.SecondarySource,mrt.subSourceReportLabel,
			CASE mrt.billingDescription WHEN '' THEN pegsBookingCost.cost ELSE synxisBookingCost.cost END
				+ CASE mrt.channel WHEN 'GDS' THEN gdsBookingCost.cost ELSE 0 END AS cost,
			IM.iatamanager AS IataGam,Core.dbo.GetIATAGroups(mrt.IATANumber,mrt.confirmationdate) IATAGroups,
			mrt.IATANumber,mrt.travelAgencyName,mrt.travelAgencyCity,mrt.travelAgencyState,mrt.travelAgencyCountry,mrt.rateCategoryCode,
			mrt.rateTypeCode,mrt.rateTypeName AS ratename,
			CASE
				WHEN LEFT(mrt.rateTypeCode,3) = 'GOL' THEN 'GOLF'
				WHEN LEFT(mrt.rateTypeCode,3) = 'PKG' THEN 'PKG'
				WHEN LEFT(mrt.rateTypeCode,3) = 'PRO' THEN 'PRO'
				WHEN LEFT(mrt.rateTypeCode,3) = 'MKT' THEN 'MKT'
				WHEN LEFT(mrt.rateTypeCode,3) = 'CON' THEN 'CON'
				WHEN LEFT(mrt.rateTypeCode,3) = 'NEG' THEN 'NEG'
				WHEN LEFT(mrt.rateTypeCode,3) = 'BAR' THEN 'BAR'
				WHEN LEFT(mrt.rateTypeCode,3) = 'DIS' THEN 'DIS'
				ELSE CASE
							WHEN LEFT(mrt.rateCategoryCode,3) = 'GOL' THEN 'GOLF'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'PKG' THEN 'PKG'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'PRO' THEN 'PRO'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'MKT' THEN 'MKT'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'CON' THEN 'CON'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'NEG' THEN 'NEG'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'BAR' THEN 'BAR'
							WHEN LEFT(mrt.rateCategoryCode,3) = 'DIS' THEN 'DIS'
							ELSE 'Other'
						END
			END AS rateCategory_PHG,
			mrt.rooms,mrt.nights,
			mrt.rooms * mrt.nights AS RoomNights,mrt.currency,mrt.reservationRevenue,mrt.reservationRevenueUSD,0 AS IPreferCount,
			CAST(0.00 AS DECIMAL(18,2)) AS IPreferChargesUSD,CAST(0.00 AS DECIMAL(18,2)) AS IPreferChargesHotelCurrency,
			CAST(0.00 AS DECIMAL(18,2)) AS IPreferCost
	FROM superset.dbo.mostrecenttransactionsreporting mrt
		LEFT OUTER JOIN Superset.dbo.gdsBookingCost ON mrt.secondarySource = gdsBookingCost.gdsType AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate,'12/31/2999')
		LEFT OUTER JOIN Superset.dbo.synxisBookingCost ON mrt.billingDescription = synxisBookingCost.billingDescription AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate,'12/31/2999')
		LEFT OUTER JOIN Superset.dbo.pegsBookingCost ON mrt.channel = pegsBookingCost.channel AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate,'12/31/2999')
		LEFT OUTER JOIN #RPT_IATAManager IM ON mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS
	WHERE MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
		AND (@hotelcode IS NULL OR mrt.hotelCode = @hotelcode)
		AND MRT.channel <> 'PMS Rez Synch'
		AND MRT.CROcode <> 'HTL_Carlton'
		AND MRT.Status <> 'Cancelled'
	UNION ALL -- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
	SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL;
         
	CREATE CLUSTERED INDEX ix_rptMain_arrivaldate_synxisID ON #RPT_MAIN(arrivaldate,crsHotelID);
	CREATE INDEX ix_rptMain_ConfirmNum ON #RPT_MAIN(confirmationnumber);
         
	DELETE FROM #RPT_MAIN WHERE crsHotelID IS NULL;
	--------------------------------------------------------

	-- POPULATE #RPT_BillyCharges --------------------------
         -- get all billy charges from CalculatedDebits for the date parameters

	INSERT INTO #RPT_BillyCharges(confirmationNumber,hotelCode,synxisID,openHospitalityCode,gpSiteID,arrivalDate,bookingChargesUSD,commissionsUSD,
									surchargesUSD,bookingChargesHotelCurrency,commissionsHotelCurrency,surchargesHotelCurrency)
	SELECT cd.confirmationNumber,cd.hotelCode,H.synxisID,H.openHospitalityCode,cd.gpSiteID,cd.arrivalDate,
		SUM(CASE
				WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID = 1) THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
				ELSE
					CASE WHEN(ESC.CriteriaId IS NOT NULL AND ESC.Surcharge = 1) THEN 0
							ELSE
							CASE classificationID
								WHEN 1 THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
								WHEN 3 THEN
											CASE
												WHEN C.itemCode IS NULL THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
												ELSE 0
											END --3=Surcharge,but if the item code was not overridden in Billy we count it as booking revenue per Hiren
								ELSE 0
							END
					END
				END) AS bookingChargesUSD,		
		--2=Commissions
		SUM(CASE
				WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID = 2) THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
				ELSE
					CASE
						WHEN(ESC.CriteriaId IS NOT NULL AND ESC.Surcharge = 1) THEN 0
						ELSE
							CASE classificationID
								WHEN 2 THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
								ELSE 0
							END
					END
				END) AS commissionsUSD,
		--3=Surcharges,only if the item code was overridden do we count it separately per Hiren
		SUM(CASE
				WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID IN(1,2)) THEN 0
				ELSE
					CASE
						WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID = 3) THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
						ELSE
							CASE
								WHEN(ESC.CriteriaId IS NOT NULL AND ESC.Surcharge = 1) THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
								ELSE
									CASE classificationID
										WHEN 3 THEN
													CASE
														WHEN C.itemCode IS NOT NULL THEN CD.chargeValueInUSD --dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency,cd.hotelCurrencyCode,cd.arrivalDate)
														ELSE 0
													END
										ELSE 0
									END
							END
					END
				END) AS surchargesUSD,

		SUM(CASE
				WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID = 1) THEN
														CASE
															WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
															ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
														END
				ELSE
					CASE
						WHEN(ESC.CriteriaId IS NOT NULL AND ESC.Surcharge = 1)
						THEN 0
						ELSE
							CASE classificationID
								WHEN 1 THEN
											CASE
												WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
												ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
											END
								WHEN 3 THEN
											CASE
												WHEN C.itemCode IS NULL THEN
																			CASE
																				WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
																				ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
																			END
												ELSE 0
											END --3=Surcharge,but if the item code was not overridden in Billy we count it as booking revenue per Hiren
								ELSE 0
							END
					END
			END) AS bookingChargesHotelCurrency,		
		--2=Commissions
		SUM(CASE
				WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID = 2) THEN
														CASE
															WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
															ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
														END
				ELSE
					CASE
						WHEN(ESC.CriteriaId IS NOT NULL AND ESC.Surcharge = 1) THEN 0
						ELSE
							CASE classificationID
								WHEN 2 THEN
									CASE
										WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
										ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
									END
								ELSE 0
							END
					END
				END) AS commissionsHotelCurrency,--3=Surcharges,only if the item code was overridden do we count it separately per Hiren
		SUM(CASE
				WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID IN(1,2)) THEN 0
				ELSE
					CASE
						WHEN(ESC.CriteriaId = 365 AND ESC.Surcharge = 1 AND CD.classificationID = 3) THEN
																CASE
																	WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
																	ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
																END
						ELSE
							CASE
								WHEN(ESC.CriteriaId IS NOT NULL AND ESC.Surcharge = 1) THEN
																							CASE
																								WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
																								ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
																							END
								ELSE
									CASE classificationID
										WHEN 3 THEN
													CASE
														WHEN C.itemCode IS NOT NULL THEN
																						CASE
																							WHEN H.hotelCurrency = CD.hotelCurrencyCode THEN CD.chargeValueInHotelCurrency
																							ELSE dbo.convertCurrencyXE(CD.chargeValueInUSD,'USD',H.hotelCurrency,CD.arrivalDate)
																						END
														ELSE 0
													END
										ELSE 0
									END
							END
					END
			END) AS surchargesHotelCurrency
	FROM superset.dbo.calculatedDebits CD
		INNER JOIN BillingProduction.dbo.criteria C ON cd.criteriaID = C.id
		LEFT OUTER JOIN BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC ON Cd.criteriaID = ESC.CriteriaId
		LEFT OUTER JOIN #HOTELS H ON CD.hotelCode = H.hotelCode
	WHERE cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
		AND (@hotelcode IS NULL OR CD.hotelCode = @hotelcode)
		-- this is needed for the new bsi functionality,credit memos can now uncharge for cancelled reserveatins
	GROUP BY CD.confirmationNumber,cd.hotelCode,H.synxisID,H.openHospitalityCode,H.hotelCurrency,cd.arrivaldate,cd.gpSiteID
	

	CREATE CLUSTERED INDEX ix_RptCharges ON #RPT_BillyCharges(confirmationnumber);
	--------------------------------------------------------


/*	2015-03-09 TMG

	this fix will keep billy count the same for each month,even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count,because the arrival month 

*/
	
	-- POPULATE #RPT_MAIN ----------------------------------
	INSERT INTO #RPT_MAIN(crsHotelID,arrivaldate,confirmationdate,confirmationnumber,SynXisCount,billingDescription,Channel,status,SecondarySource,
							subSourceReportLabel,CROcode,cost,IataGam,IATAGroups,IATANumber,travelAgencyName,travelAgencyCity,travelAgencyState,
							travelAgencyCountry,rateCategoryCode,rateTypeCode,ratename,rateCategory_PHG,rooms,nights,RoomNights,currency,
							reservationRevenue,reservationRevenueUSD,IPreferCount,IPreferChargesUSD,IPreferChargesHotelCurrency,IPreferCost)
	SELECT ISNULL(billy.synxisID,billy.openHospitalityCode),billy.arrivalDate,missingmrt.confirmationDate,billy.confirmationNumber,0,
			missingMRT.billingDescription,missingMRT.channel,missingMRT.Status,missingMRT.SecondarySource,missingMRT.subSourceReportLabel,
			missingMRT.CROcode,NULL,IM.iatamanager,Core.dbo.GetIATAGroups(missingMRT.IATANumber,missingMRT.confirmationdate) AS IATAGroups,
			missingMRT.IATANumber,missingMRT.travelAgencyName,missingMRT.travelAgencyCity,missingMRT.travelAgencyState,missingMRT.travelAgencyCountry,
			missingMRT.rateCategoryCode,missingMRT.rateTypeCode,missingMRT.rateTypeName,
			CASE
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'GOL' THEN 'GOLF'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'PKG' THEN 'PKG'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'PRO' THEN 'PRO'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'MKT' THEN 'MKT'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'CON' THEN 'CON'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'NEG' THEN 'NEG'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'BAR' THEN 'BAR'
				WHEN LEFT(missingMRT.rateTypeCode,3) = 'DIS' THEN 'DIS'
				ELSE
					CASE
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'GOL' THEN 'GOLF'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'PKG' THEN 'PKG'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'PRO' THEN 'PRO'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'MKT' THEN 'MKT'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'CON' THEN 'CON'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'NEG' THEN 'NEG'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'BAR' THEN 'BAR'
						WHEN LEFT(missingMRT.rateCategoryCode,3) = 'DIS' THEN 'DIS'
						ELSE 'Other'
					END
			END,
			NULL,NULL,NULL,NULL,NULL,NULL,0,
			CAST(0.00 AS DECIMAL(18,2)),-- IPreferChargesUSD
			CAST(0.00 AS DECIMAL(18,2)),-- IPreferChargesHotelCurrency
			CAST(0.00 AS DECIMAL(18,2))   -- as IPreferCost
	FROM #RPT_BillyCharges billy
		JOIN superset.dbo.mostRecentTransactionsReporting missingMRT ON billy.confirmationNumber = missingMRT.confirmationNumber
		LEFT OUTER JOIN #RPT_IATAManager IM ON missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS -- get only confirmation numbers that have not been added yet
		LEFT OUTER JOIN #RPT_MAIN mrt ON billy.confirmationnumber = mrt.confirmationnumber
	WHERE mrt.confirmationNumber IS NULL
		AND (@hotelcode IS NULL OR billy.hotelcode = @hotelcode)
	GROUP BY billy.HotelCode,ISNULL(billy.synxisID,billy.openHospitalityCode),billy.arrivalDate,missingMRT.confirmationDate,billy.confirmationNumber,
			missingMRT.billingDescription,missingMRT.channel,missingMRT.Status,missingMRT.SecondarySource,missingMRT.subSourceReportLabel,
			missingMRT.CROcode,IM.iatamanager,IATAGroups,missingMRT.IATANumber,missingMRT.confirmationdate,missingMRT.travelAgencyName,
			missingMRT.travelAgencyCity,missingMRT.travelAgencyState,missingMRT.travelAgencyCountry,missingMRT.rateCategoryCode,missingMRT.rateTypeCode,
			missingMRT.rateTypeName;
	--------------------------------------------------------

	-- POPULATE #RPT_MAIN ----------------------------------
         -- get IPrefer Counts and Sums by current billing date,using the following logic:
         --		if matching a current MRT record - use the MRT data for channel,source,etc
         --		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
         --		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
	INSERT INTO #RPT_MAIN(crsHotelID,arrivaldate,confirmationdate,confirmationnumber,SynXisCount,billingDescription,Channel,status,SecondarySource,
							subSourceReportLabel,CROcode,cost,IataGam,IATAGroups,IATANumber,travelAgencyName,travelAgencyCity,travelAgencyState,
							travelAgencyCountry,rateCategoryCode,rateTypeCode,ratename,rateCategory_PHG,rooms,nights,RoomNights,currency,
							reservationRevenue,reservationRevenueUSD,IPreferCount,IPreferChargesUSD,IPreferChargesHotelCurrency,IPreferCost)
	SELECT ISNULL(H.synxisID,H.openHospitalityCode),IP.Billing_Date,IP.Booking_Date,NULL,0,
			COALESCE(MRT.billingDescription,'Non-Matching IPrefer Reservation'),
			COALESCE(MRT.channel,	CASE
										WHEN IP.arrival_date BETWEEN @arrivalStart AND @arrivalEnd THEN 'IPrefer Manual Entry'
										ELSE 'IPrefer Prior Period'
									END),
			COALESCE(MRT.Status,''),COALESCE(MRT.SecondarySource,''),COALESCE(MRT.subSourceReportLabel,''),COALESCE(MRT.CROcode,''),NULL,
			COALESCE(MRT.IataGam,''),COALESCE(MRT.IATAGroups,''),COALESCE(MRT.IATANumber,''),COALESCE(MRT.travelAgencyName,''),
			COALESCE(MRT.travelAgencyCity,''),COALESCE(MRT.travelAgencyState,''),COALESCE(MRT.travelAgencyCountry,''),COALESCE(MRT.rateCategoryCode,''),
			COALESCE(MRT.rateTypeCode,''),COALESCE(MRT.ratename,''),COALESCE(MRT.rateCategory_PHG,''),NULL,NULL,NULL,NULL,NULL,NULL,
			COUNT(DISTINCT IP.Booking_ID),SUM(IP.Billable_Amount_USD),
			SUM(CASE
					WHEN H.hotelCurrency = IP.Hotel_Currency THEN IP.Billable_Amount_Hotel_Currency
					ELSE dbo.convertCurrencyXE(IP.Billable_Amount_USD,'USD',H.hotelCurrency,IP.Arrival_Date)
				END),
			SUM(IP.Reservation_Amount_USD * 0.02)
	FROM BSI.IPrefer_Charges IP
		LEFT OUTER JOIN #HOTELS H ON IP.Hotel_Code = H.hotelCode
		LEFT OUTER JOIN #RPT_MAIN mrt ON IP.booking_id = mrt.confirmationnumber
	WHERE IP.billing_date BETWEEN @arrivalStart AND @arrivalEnd
		AND (@hotelcode IS NULL OR IP.Hotel_Code = @hotelcode)
	GROUP BY IP.Hotel_Code,ISNULL(H.synxisID,H.openHospitalityCode),H.hotelCurrency,IP.Hotel_Currency,IP.Billing_Date,IP.Booking_Date,MRT.arrivalDate,
			MRT.billingDescription,MRT.channel,MRT.Status,
			CASE
				WHEN IP.arrival_date BETWEEN @arrivalStart AND @arrivalEnd THEN 'IPrefer Manual Entry'
				ELSE 'IPrefer Prior Period'
			END,
			MRT.SecondarySource,MRT.subSourceReportLabel,MRT.CROcode,MRT.cost,MRT.IataGam,MRT.IATAGroups,MRT.IATANumber,MRT.travelAgencyName,
			MRT.travelAgencyCity,MRT.travelAgencyState,MRT.travelAgencyCountry,MRT.rateCategoryCode,MRT.rateTypeCode,MRT.ratename,MRT.rateCategory_PHG;
	--------------------------------------------------------

	IF @Extended = 0
		INSERT INTO [Reporting].[ProfitabilityReportResults](crsHotelID,hotelCode,hotelName,primaryBrand,gpSalesTerritory,AMD,RD,RAM,AccountManager,
																geographicRegion,country,city,state,hotelRooms,hotelStatus,CRMClassification,hotelCurrency,
																billingDescription,channel,secondarySource,subSourceReportLabel,CROcode,ArrivalYear,
																ArrivalMonth,ConfirmedSynxisBookingCount,ConfirmedRevenueUSD,ConfirmedRoomNights,
																ConfirmedTotalCost,avgNightlyRateUSD,avgLengthOfStay,BillyBookingCount,
																PHGSiteBillyBookingCount,HHASiteBillyBookingCount,BillyBookingCharges,
																PHGSiteBillyBookingCharges,HHASiteBillyBookingCharges,commissions,PHGSiteCommissions,
																HHASiteCommissions,surcharges,PHGSiteSurcharges,HHASiteSurcharges,BookAndComm,
																PHGSiteBookAndComm,HHASiteBookAndComm,costperbooking,IPreferBookingCount,IPreferCharges,
																IPreferCost,BillyBookingWithZeroBookAndComm,ConfirmedRevenueHotelCurrency,
																BillyBookingChargesHotelCurrency,PHGSiteBillyBookingChargesHotelCurrency,
																HHASiteBillyBookingChargesHotelCurrency,CommissionsHotelCurrency,
																PHGSiteCommissionsHotelCurrency,HHASiteCommissionsHotelCurrency,SurchargesHotelCurrency,
																PHGSiteSurchargesHotelCurrency,HHASiteSurchargesHotelCurrency,BookAndCommHotelCurrency,
																PHGSiteBookAndCommHotelCurrency,HHASiteBookAndCommHotelCurrency,IPreferChargesHotelCurrency)
		SELECT main.crsHotelID,hotels.hotelCode,hotels.hotelName,hotels.primaryBrand,hotels.gpSalesTerritory,hotels.AMD,hotels.RD,hotels.RAM,
				hotels.AccountManager,hotels.geographicRegion,hotels.country,hotels.city,hotels.state,hotels.hotelRooms,hotels.hotelStatus,
				hotels.CRMClassification,hotels.hotelCurrency,main.billingDescription,main.channel,main.secondarySource,main.subSourceReportLabel,
				main.CROcode,YEAR(main.arrivaldate) ArrivalYear,MONTH(main.arrivaldate) ArrivalMonth,
				/*Hiren asked that "Confirmed Cancelled" bookings be removed*/
				SUM(main.SynXisCount) AS ConfirmedSynxisBookingCount,SUM(main.reservationrevenueUSD) ConfirmedRevenueUSD,
				SUM(main.roomNights) AS ConfirmedRoomNights,SUM(main.cost) AS ConfirmedTotalCost,
				SUM(main.reservationRevenueUSD) / SUM(main.roomNights) AS avgNightlyRateUSD,
				SUM(main.nights) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS avgLengthOfStay,
				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(DISTINCT billy.confirmationnumber) AS BillyBookingCount,
				COUNT(CASE WHEN billy.gpSiteID IN(1) THEN billy.confirmationnumber ELSE NULL END) AS PHGSiteBillyBookingCount,
				COUNT(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.confirmationnumber ELSE NULL END) AS HHASiteBillyBookingCount,
				SUM(billy.bookingChargesUSD) AS BillyBookingCharges,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.bookingChargesUSD ELSE 0 END) AS PHGSiteBillyBookingCharges,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.bookingChargesUSD ELSE 0 END) AS HHASiteBillyBookingCharges,
				SUM(billy.commissionsUSD) AS commissions,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.commissionsUSD ELSE 0 END) AS PHGSiteCommissions,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.commissionsUSD ELSE 0 END) AS HHASiteCommissions,
				SUM(billy.surchargesUSD) AS surcharges,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.surchargesUSD ELSE 0 END) AS PHGSiteSurcharges,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.surchargesUSD ELSE 0 END) AS HHASiteSurcharges,
				SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD) BookAndComm,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.bookingChargesUSD + billy.commissionsUSD ELSE 0 END) AS PHGSiteBookAndComm,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.bookingChargesUSD + billy.commissionsUSD ELSE 0 END) AS HHASiteBookAndComm,
				SUM(main.cost) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS costperbooking,
				SUM(main.IPreferCount) AS IPreferBookingCount,
				SUM(main.IPreferChargesUSD) AS IPreferCharges,
				SUM(main.IPreferCost) AS IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END AS BillyBookingWithZeroBookAndComm,
					-- adding all sum fields in hotel billing currency from USD
				SUM(Superset.dbo.convertCurrencyXE(main.reservationrevenue,main.currency,hotels.hotelcurrency,main.confirmationdate)) AS ConfirmedRevenueHotelCurrency,
				SUM(billy.bookingChargesHotelCurrency) AS BillyBookingChargesHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.bookingChargesHotelCurrency ELSE 0 END) AS PHGSiteBillyBookingChargesHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.bookingChargesHotelCurrency ELSE 0 END) AS HHASiteBillyBookingChargesHotelCurrency,
				SUM(billy.commissionsHotelCurrency) AS CommissionsHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.commissionsHotelCurrency ELSE 0 END) AS PHGSiteCommissionsHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.commissionsHotelCurrency ELSE 0 END) AS HHASiteCommissionsHotelCurrency,
				SUM(billy.surchargesHotelCurrency) AS SurchargesHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.surchargesHotelCurrency ELSE 0 END) AS PHGSiteSurchargesHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.surchargesHotelCurrency ELSE 0 END) AS HHASiteSurchargesHotelCurrency,
				SUM(billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) AS BookAndCommHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(1) THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) ELSE 0 END) AS PHGSiteBookAndCommHotelCurrency,
				SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) ELSE 0 END) AS HHASiteBookAndCommHotelCurrency,
				SUM(main.IPreferChargesHotelCurrency) AS IPreferChargesHotelCurrency 
		FROM #HOTELS AS hotels
			LEFT OUTER JOIN #RPT_MAIN AS main ON main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
			LEFT OUTER JOIN #RPT_BillyCharges AS billy ON main.confirmationnumber = billy.confirmationNumber
		GROUP BY YEAR(main.arrivaldate),MONTH(main.arrivaldate),main.crsHotelID,hotels.hotelCode,hotels.hotelName,hotels.primaryBrand,hotels.gpSalesTerritory,
				hotels.AMD,hotels.RD,hotels.RAM,hotels.AccountManager,hotels.geographicRegion,hotels.country,hotels.city,hotels.state,hotels.hotelRooms,
				hotels.hotelStatus,hotels.CRMClassification,hotels.hotelCurrency,main.Channel,main.billingDescription,main.SecondarySource,
				main.subSourceReportLabel,main.CROcode

	ELSE -- @extended = 1,returns different resultset
		INSERT INTO [Reporting].[ProfitabilityReportResultsExtended](crsHotelID,hotelCode,hotelName,primaryBrand,gpSalesTerritory,AMD,RD,RAM,
																		AccountManager,geographicRegion,country,city,state,hotelRooms,hotelStatus,
																		CRMClassification,hotelcurrency,billingDescription,channel,secondarySource,
																		subSourceReportLabel,CROcode,IataGam,IATAGroups,IATANumber,travelAgencyName,
																		travelAgencyCity,travelAgencyState,travelAgencyCountry,rateCategoryCode,
																		rateTypeCode,ratename,rateCategory_PHG,ArrivalYear,ArrivalMonth,
																		ConfirmedSynxisBookingCount,ConfirmedRevenueUSD,ConfirmedRoomNights,
																		ConfirmedTotalCost,avgNightlyRateUSD,avgLengthOfStay,BillyBookingCount,
																		PHGSiteBillyBookingCount,HHASiteBillyBookingCount,BillyBookingCharges,
																		PHGSiteBillyBookingCharges,HHASiteBillyBookingCharges,commissions,
																		PHGSiteCommissions,HHASiteCommissions,surcharges,PHGSiteSurcharges,
																		HHASiteSurcharges,BookAndComm,PHGSiteBookAndComm,HHASiteBookAndComm,costperbooking,
																		IPreferBookingCount,IPreferCharges,IPreferCost,BillyBookingWithZeroBookAndComm,
																		ConfirmedRevenueHotelCurrency,BillyBookingChargesHotelCurrency,
																		PHGSiteBillyBookingChargesHotelCurrency,HHASiteBillyBookingChargesHotelCurrency,
																		CommissionsHotelCurrency,PHGSiteCommissionsHotelCurrency,
																		HHASiteCommissionsHotelCurrency,SurchargesHotelCurrency,
																		PHGSiteSurchargesHotelCurrency,HHASiteSurchargesHotelCurrency,
																		BookAndCommHotelCurrency,PHGSiteBookAndCommHotelCurrency,
																		HHASiteBookAndCommHotelCurrency,IPreferChargesHotelCurrency)
		SELECT main.crsHotelID,hotels.hotelCode,hotels.hotelName,hotels.primaryBrand,hotels.gpSalesTerritory,hotels.AMD,hotels.RD,hotels.RAM,
				hotels.AccountManager,hotels.geographicRegion,hotels.country,hotels.city,hotels.state,hotels.hotelRooms,hotels.hotelStatus,
				hotels.CRMClassification,hotels.hotelcurrency,main.billingDescription,main.channel,main.secondarySource,main.subSourceReportLabel,
				main.CROcode,main.IataGam,REPLACE(main.IATAGroups,',','') AS IATAGroups,main.IATANumber,
				REPLACE(main.travelAgencyName,',','') AS travelAgencyName,main.travelAgencyCity,main.travelAgencyState,main.travelAgencyCountry,
				main.rateCategoryCode,main.rateTypeCode,main.ratename,main.rateCategory_PHG,YEAR(main.arrivaldate) ArrivalYear,MONTH(main.arrivaldate) ArrivalMonth,
				SUM(main.SynXisCount) AS ConfirmedSynxisBookingCount,
				SUM(main.reservationrevenueUSD) ConfirmedRevenueUSD,
                SUM(main.roomNights) AS ConfirmedRoomNights,
                SUM(main.cost) AS ConfirmedTotalCost,
                SUM(main.reservationRevenueUSD) / SUM(main.roomNights) AS avgNightlyRateUSD,
                SUM(main.nights) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS avgLengthOfStay,
                -- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
                COUNT(DISTINCT billy.confirmationnumber) AS BillyBookingCount,
                COUNT(CASE WHEN billy.gpSiteID IN(1) THEN billy.confirmationnumber ELSE NULL END) AS PHGSiteBillyBookingCount,
                COUNT(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.confirmationnumber ELSE NULL END) AS HHASiteBillyBookingCount,
                SUM(billy.bookingChargesUSD) AS BillyBookingCharges,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.bookingChargesUSD ELSE 0 END) AS PHGSiteBillyBookingCharges,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.bookingChargesUSD ELSE 0 END) AS HHASiteBillyBookingCharges,
                SUM(billy.commissionsUSD) AS commissions,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.commissionsUSD ELSE 0 END) AS PHGSiteCommissions,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.commissionsUSD ELSE 0 END) AS HHASiteCommissions,
                SUM(billy.surchargesUSD) AS surcharges,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.surchargesUSD ELSE 0 END) AS PHGSiteSurcharges,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.surchargesUSD ELSE 0 END) AS HHASiteSurcharges,
                SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD) BookAndComm,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.bookingChargesUSD + billy.commissionsUSD ELSE 0 END) AS PHGSiteBookAndComm,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.bookingChargesUSD + billy.commissionsUSD ELSE 0 END) AS HHASiteBookAndComm,
                SUM(main.cost) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS costperbooking,
                SUM(main.IPreferCount) AS IPreferBookingCount,
                SUM(main.IPreferChargesUSD) AS IPreferCharges,
                SUM(main.IPreferCost) AS IPreferCost,
                CASE WHEN COALESCE(SUM(billy.bookingChargesUSD) + SUM(billy.commissionsUSD),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END AS BillyBookingWithZeroBookAndComm,
					-- adding all sum fields in hotel billing currency from USD
                SUM(Superset.dbo.convertCurrencyXE(main.reservationrevenue,main.currency,hotels.hotelcurrency,main.confirmationdate)) AS ConfirmedRevenueHotelCurrency,
                SUM(billy.bookingChargesHotelCurrency) AS BillyBookingChargesHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.bookingChargesHotelCurrency ELSE 0 END) AS PHGSiteBillyBookingChargesHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.bookingChargesHotelCurrency ELSE 0 END) AS HHASiteBillyBookingChargesHotelCurrency,
                SUM(billy.commissionsHotelCurrency) AS CommissionsHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.commissionsHotelCurrency ELSE 0 END) AS PHGSiteCommissionsHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.commissionsHotelCurrency ELSE 0 END) AS HHASiteCommissionsHotelCurrency,
                SUM(billy.surchargesHotelCurrency) AS SurchargesHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN billy.surchargesHotelCurrency ELSE 0 END) AS PHGSiteSurchargesHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN billy.surchargesHotelCurrency ELSE 0 END) AS HHASiteSurchargesHotelCurrency,
                SUM(billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) AS BookAndCommHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(1) THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) ELSE 0 END) AS PHGSiteBookAndCommHotelCurrency,
                SUM(CASE WHEN billy.gpSiteID IN(6,8) THEN (billy.bookingChargesHotelCurrency + billy.commissionsHotelCurrency) ELSE 0 END) AS HHASiteBookAndCommHotelCurrency,
                SUM(main.IPreferChargesHotelCurrency) AS IPreferChargesHotelCurrency 
		FROM #HOTELS AS hotels
			LEFT OUTER JOIN #RPT_MAIN AS main ON main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
			LEFT OUTER JOIN #RPT_BillyCharges AS billy ON main.confirmationnumber = billy.confirmationNumber
         GROUP BY YEAR(main.arrivaldate),MONTH(main.arrivaldate),main.crsHotelID,hotels.hotelCode,hotels.hotelName,hotels.primaryBrand,
				hotels.gpSalesTerritory,hotels.AMD,hotels.RD,hotels.RAM,hotels.AccountManager,hotels.geographicRegion,hotels.country,hotels.city,
				hotels.state,hotels.hotelRooms,hotels.hotelStatus,hotels.CRMClassification,hotels.hotelCurrency,main.crsHotelID,main.Channel,
				main.billingDescription,main.SecondarySource,main.subSourceReportLabel,main.CROcode,hotels.hotelCurrency,main.IataGam,IATAGroups,
				main.IATANumber,travelAgencyName,main.travelAgencyCity,main.travelAgencyState,main.travelAgencyCountry,main.rateCategoryCode,
				main.rateTypeCode,main.ratename,main.rateCategory_PHG
END;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[profitabilityExtendedReport]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 03/15/2011
-- Description:	underlying query for all profitability reports
-- UPDATE 5-25-2011: changed data source info for new GP and Calculated Debits schemas
-- =============================================
CREATE procedure [dbo].[profitabilityExtendedReport] 
	-- Add the parameters for the stored procedure here
	@arrivalStart date, 
	@arrivalEnd date,
	@rateCodes nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @usdExchange table(invDate datetime, currCode char(3), exchRate decimal(20,8))

	INSERT INTO @usdExchange 
	SELECT distinct invoiceDate, hotelCurrencyCode, CASE hotelCurrencyCode WHEN 'USD' THEN 1 ELSE gpCurr.XCHGRATE END as xchRate
	FROM Superset.dbo.calculatedDebits 
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 as gpCurr 
		ON calculatedDebits.hotelCurrencyCode = gpCurr.CURNCYID
		AND invoiceDate BETWEEN gpCurr.EXCHDATE AND gpCurr.EXPNDATE 
		AND (gpCurr.EXGTBLID LIKE '%AVG%' OR gpCurr.EXGTBLID LIKE '%AVERAGE%')
	WHERE arrivalDate BETWEEN @arrivalStart AND @arrivalEnd


    -- Insert statements for procedure here
	DECLARE @costs table(arrivalYear int, arrivalMonth int, billingDescription nvarchar(50), channel nvarchar(50), secondarySource nvarchar(50), costPerBooking decimal(5,2));

	INSERT INTO @costs
	SELECT distinct
	YEAR(arrivalDate) as arrivalYear,
	MONTH(arrivalDate) as arrivalMonth,
	mostRecentTransactions.billingDescription,
	mostRecentTransactions.channel,
	mostRecentTransactions.secondarySource,
	((CASE mostRecentTransactions.billingDescription 
		WHEN '' THEN pegsBookingCost.cost 
		WHEN 'SynXis Call Center' THEN (Superset.dbo.avgVoiceMinuteCost() + synxisBookingCost.cost)
		ELSE synxisBookingCost.cost
	END) 
	+ 
	(CASE mostRecentTransactions.channel 
		WHEN 'GDS' THEN gdsBookingCost.cost 
		ELSE 0 
	END)) as totalCost
	FROM Superset.dbo.mostRecentTransactions
LEFT JOIN Superset.dbo.gdsBookingCost ON mostrecenttransactions.secondarySource = gdsBookingCost.gdsType AND 
mostrecenttransactions.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
LEFT JOIN Superset.dbo.synxisBookingCost ON mostrecenttransactions.billingDescription = synxisBookingCost.billingDescription AND 
mostrecenttransactions.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
LEFT JOIN Superset.dbo.pegsBookingCost ON mostrecenttransactions.channel = pegsBookingCost.channel AND 
mostrecenttransactions.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')
	WHERE status <> 'Cancelled' AND
	arrivalDate BETWEEN @arrivalStart AND @arrivalEnd AND
	mostRecentTransactions.channel <> 'PMS Rez Synch' AND
	CROcode <> 'HTL_Carlton';


	
	DECLARE @hotels table(synxisID int, 
		hotelCode nvarchar(10), 
		hotelName nvarchar(250), 
		primaryBrand nvarchar(7), 
		gpSalesTerritory nvarchar(15), 
		AMD nvarchar(160), 
		RD nvarchar(160), 
		RAM nvarchar(160), 
		geographicRegion nvarchar(50), 
		country nvarchar(250), 
		state nvarchar(250), 
		city nvarchar(250),
		hotelRooms int,
		hotelStatus nvarchar(25)
		)
	INSERT INTO @hotels 
	SELECT distinct
COALESCE(synxisID, 0) as synxisID,
	COALESCE(hotelsReporting.code, 'UNKNOWN') as hotelCode,
	COALESCE(hotelsReporting.hotelName,'UNKNOWN') as hotelName,
	COALESCE(hotelsReporting.mainBrandCode, 'UNKNOWN') as primaryBrand,
	COALESCE(RM00101.SALSTERR, 'UNKNOWN') as gpSalesTerritory,
	COALESCE(hotelsReporting.phg_areamanageridName, 'UNKNOWN') as AMD,
	COALESCE(hotelsReporting.phg_regionalmanageridName, 'UNKNOWN') as RD,
	COALESCE(hotelsReporting.phg_revenueaccountmanageridName, 'UNKNOWN') as RAM,
	COALESCE(hotelsReporting.geographicRegionName, 'UNKNOWN') as geographicRegion,
	COALESCE(hotelsReporting.shortName, 'UNKNOWN') as country,
	COALESCE(hotelsReporting.subAreaName, 'UNKNOWN') as state,
	COALESCE(hotelsReporting.physicalCity, 'UNKNOWN') as city,
	COALESCE(hotelsReporting.totalRooms, 0) as hotelRooms,
	COALESCE(hotelsReporting.statuscodename, 'UNKNOWN') as hotelStatus
	FROM Core.dbo.hotelsReporting LEFT OUTER JOIN
	IC.dbo.RM00101 ON hotelsReporting.code = RM00101.CUSTNMBR
	WHERE synxisID > 0
	
	--runtime for above subqueries on July 10, 2012 = 00:00:46
	
	DECLARE @charges table(confirmationNumber nvarchar(50) PRIMARY KEY, hotelCurrencyCode char(3), invoiceDate date, bookingCharges decimal(12,2), commissions decimal(12,2), surcharges decimal(12,2))
	
	INSERT INTO @charges SELECT
		confirmationNumber,
		hotelCurrencyCode,
		invoiceDate,
		SUM(CASE classificationID WHEN 1 THEN chargeValueInHotelCurrency ELSE 0 END) AS bookCharges --1=Booking
		,SUM(CASE classificationID WHEN 2 THEN chargeValueInHotelCurrency ELSE 0 END) AS commCharges --2=Commission
		,SUM(CASE classificationID WHEN 3 THEN chargeValueInHotelCurrency ELSE 0 END) AS surcharges --3=Surcharge
		FROM [Superset].[dbo].[calculatedDebits] 
		WHERE arrivalDate between @arrivalStart and @arrivalEnd
		GROUP BY confirmationNumber,
		hotelCurrencyCode,
		invoiceDate	
	
	--runtime for above subqueries on July 10, 2012 = 00:01:00
	
	select 
	YEAR(arrivalDate) as arrivalYear,
	MONTH(arrivalDate) as arrivalMonth,
	mrt.hotelID as synxisID,
	MIN(hotels.hotelCode) as hotelCode,
	MIN(hotels.hotelName) as hotelName,
	MIN(hotels.primaryBrand) as primaryBrand,
	MIN(hotels.gpSalesTerritory) as gpSalesTerritory,
	MIN(hotels.AMD) as AMD,
	MIN(hotels.RD) as RD,
	MIN(hotels.RAM) as RAM,
	MIN(hotels.geographicRegion) as geographicRegion,
	MIN(hotels.country) as country,
	MIN(hotels.state) as state,
	MIN(hotels.city) as city,
	MIN(hotels.hotelRooms) as hotelRooms,
	MIN(hotels.hotelStatus) as hotelStatus,
	mrt.Channel,
	mrt.SecondarySource,
	mrt.subSourceReportLabel,
	mrt.IATANumber,
	mrt.travelAgencyName,
	mrt.travelAgencyCity,
	mrt.travelAgencyState,
	mrt.travelAgencyCountry,
	mrt.rateCategoryCode,
	mrt.rateTypeCode,
	COUNT(mrt.confirmationNumber) as synxisBookings,
	COUNT(charges.confirmationNumber) as billyBookings,
	MAX(COALESCE(costs.costPerBooking, 99999)) as costPerBooking,
	(COUNT(mrt.confirmationNumber) * MAX(COALESCE(costs.costPerBooking, 99999))) AS totalCost,
	(COALESCE(SUM(bookingCharges),0) / COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)),99999)) as bookingCharges
	,(COALESCE(SUM(commissions),0) / COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)), 99999)) as commissions
	,(COALESCE(SUM(surcharges),0) / COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)), 99999)) as surcharges
	,COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)), 99999) as exchRate
	,hotelCurrencyCode
	,SUM(mrt.reservationRevenueUSD) as hotelRevenueUSD
	,SUM(mrt.roomNights) as roomNights
	,SUM(mrt.reservationRevenueUSD)/SUM(mrt.roomNights) as avgNightlyRateUSD
	,SUM(mrt.nights)/COUNT(mrt.confirmationNumber) as avgLengthOfStay

	FROM superset.dbo.mostRecentTransactionsReporting as MRT
	LEFT OUTER JOIN @costs as costs ON mrt.billingDescription = costs.billingDescription AND mrt.channel = costs.channel AND mrt.secondarySource = costs.secondarySource AND YEAR(mrt.arrivalDate) = costs.arrivalYear AND MONTH(mrt.arrivalDate) = costs.arrivalMonth
	LEFT OUTER JOIN @hotels as hotels ON mrt.hotelID = hotels.synxisID
	LEFT OUTER JOIN @charges as charges on mrt.confirmationNumber = charges.confirmationNumber
	LEFT OUTER JOIN @usdExchange as usdExchange ON charges.invoiceDate = usdExchange.invDate AND charges.hotelCurrencyCode = usdExchange.currCode
	LEFT OUTER JOIN CurrencyRates.dbo.dailyRates on mrt.confirmationDate = dailyRates.rateDate AND charges.hotelCurrencyCode = dailyRates.code

	WHERE status <> 'Cancelled' AND
	mrt.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd AND
	mrt.channel <> 'PMS Rez Synch' AND
	mrt.CROcode <> 'HTL_Carlton' AND
	(mrt.rateTypeCode IN (@rateCodes) OR '' IN (@rateCodes))

	GROUP BY
	YEAR(arrivalDate),
	MONTH(arrivalDate),
	mrt.hotelID,
	mrt.Channel,
	mrt.SecondarySource,
	subSourceReportLabel,
	mrt.billingDescription,
	charges.invoiceDate,
	hotelCurrencyCode,
	mrt.IATANumber,
	mrt.travelAgencyName,
	mrt.travelAgencyCity,
	mrt.travelAgencyState,
	mrt.travelAgencyCountry,
	mrt.rateCategoryCode,
	mrt.rateTypeCode
    
    
	END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iPreferYearOverYearRevenue]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[iPreferYearOverYearRevenue]
	-- Add the parameters for the stored procedure here
	@startDate Date,
	@endDate Date,
	@sameStore bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @previousYearSD DATE =  DATEADD(year, -1, @startDate);
DECLARE @previousYearED DATE =  DATEADD(year, -1, @endDate);


;WITH cte_hotelId(hotelID,arrivalDate,[confirmationNumber],[nights],[reservationRevenueUSD],[loyaltyNumber])
AS
(
SELECT hotelID,arrivalDate,[confirmationNumber],[nights],[reservationRevenueUSD],[loyaltyNumber]
FROM Superset.dbo.mostrecenttransactions
WHERE channel = 'GDS'
  AND [status] <> 'Canceled'
  AND arrivalDate BETWEEN @previousYearSD AND @endDate
),
cte_PrevYear(hotelID)
AS
(
SELECT DISTINCT hotelID
FROM cte_hotelId
WHERE arrivalDate BETWEEN @previousYearSD AND @previousYearED
),
cte_CurYear(hotelID)
AS
(
SELECT DISTINCT hotelID
FROM cte_hotelId
WHERE arrivalDate BETWEEN @startDate AND @endDate
  AND hotelID IN(SELECT hotelID FROM cte_PrevYear)
)
SELECT @startDate as [Start Date], @endDate as [End Date],COUNT([confirmationNumber]) as [Number Of Bookings],SUM([nights])as [Number Of Nights],SUM([reservationRevenueUSD]) as [Reservation Revenue in USD]
FROM cte_hotelId h
WHERE arrivalDate BETWEEN @startDate AND @endDate
       AND ((@sameStore = 1 AND h.hotelId IN(SELECT hotelId FROM cte_CurYear)) OR @sameStore = 0)
       AND [loyaltyNumber] IN(SELECT iPrefer_Number FROM [Superset].[BSI].[Customer_Profile_Import])
UNION ALL
SELECT  @previousYearSD as [Start Date], @previousYearED as [End Date],COUNT([confirmationNumber]) as [Number Of Bookings],SUM([nights])as [Number Of Nights],SUM([reservationRevenueUSD]) as [Reservation Revenue in USD]
FROM cte_hotelId h
WHERE arrivalDate BETWEEN @previousYearSD AND @previousYearED
       AND ((@sameStore = 1 AND h.hotelId IN(SELECT hotelId FROM cte_CurYear)) OR @sameStore = 0)
       AND [loyaltyNumber] IN(SELECT iPrefer_Number FROM [Superset].[BSI].[Customer_Profile_Import])


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReport_GDSwithIATAs_AdHocDELETEME]'
GO

/*
-- Author:		Tory Greco
-- Create date: 02/10/2014
-- Description:	underlying query for all profitability reports
	new methodology:	get all linked server data into local tables
						get all needed MRT records into 1 master table
						put more case logic into sums for group by
						
						changed the underlying views - mrtreporting, 

	-- hiren wants to add this	
	-- @ratecodes varchar(max) -- contains comma separated values - list of rate codes


add column to rpt main - synxiscount

exec [Reporting].[ProfitabilityReport] 
	@arrivalStart = '2015-02-01', 
	@arrivalEnd = '2015-02-28',
	@Extended = 0

-- for true timing test, clear the cache and rerun
DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS



from email:
•	They expect the total ‘Hotel Revenue USD’ and ‘Synxis Booking Count’ to match the exact total amounts from this report for the same period http://miasrp02/Reports/Pages/Report.aspx?ItemPath=%2fPHGRDL%2f9_d125g13i0e0s0p0c0 or else know exactly why they are off, even by 1 booking or $1.
•	All numbers from the standard profit report should match all numbers on the extended report
•	They would like the sum of all Billy and Iprefer charges for a month to match exactly the sum of all reservation SOP documents in GP
•	They would like to be able to run both reports adhoc on their own from SSRS
•	Add new ‘Account Manager’ role from CRM to both reports alongside other PHG roles currently there
•	Add IPrefer revenue to the report using the following logic/process:
o	Add two new columns labeled ‘IPrefer Charges’ that has the sum of all IPrefer charges for each reservation and ‘IPrefer Booking Count’ that has the distinct count of every Booking_ID that received an IPrefer charge
o	For charges where the BSI Booking_ID exactly matches our Superset Confirmation Number, this number will appear alongside all the other charges
o	For charges where the BSI Booking_ID does not match, for absolutely any reason, this should not be joined to any existing booking, it should appear on the report as its own separate booking line where the Channel = ‘IPrefer Manual Entry’, any hotel info is still joined to by IPrefer Charge Hotel Code. Synxis booking count, billy booking count, other charge fields other than the new IPrefer fields should have a zero
o	None of the calculated fields (profitability, cost, sums, etc) should take the IPrefer charge into account

from the report link:
SELECT	year(arrivaldate) yr, month(arrivaldate) mon, count(confirmationnumber) cnt, channel
FROM Superset.dbo.MostRecentTransactionsReporting 
WHERE (status <> 'Cancelled')
AND (channel <> 'PMS Rez Synch') 
AND (Superset.dbo.MostRecentTransactionsReporting.arrivalDate 
BETWEEN '2013-01-01' AND '2013-12-31') 
GROUP by year(arrivaldate), month(arrivaldate), channel
ORDER BY year(arrivaldate), month(arrivaldate), channel


*/

CREATE procedure [Reporting].[ProfitabilityReport_GDSwithIATAs_AdHocDELETEME] 
	-- Add the parameters for the stored procedure here
	@arrivalStart date, 
	@arrivalEnd date,
	@hotelcode varchar(10) = NULL,
	@Extended bit = 1
AS
BEGIN
	SET NOCOUNT ON; -- prevents extra result sets from interfering with SELECT statements
	

--get the hotel information
SELECT	H.synxisID,
		H.openHospitalityCode,
		H.code as hotelCode,
		H.hotelName as hotelName,
		H.mainBrandCode as primaryBrand,
		LTRIM(RTRIM(RM00101.SALSTERR)) as gpSalesTerritory,
		H.phg_areamanageridName as AMD,
		H.phg_regionalmanageridName as RD,
		H.phg_revenueaccountmanageridName as RAM,
		H.phg_AccountManagerName as AccountManager,
		case	when CRM.accountclassificationcode = 200000 then 'City Hotel'
				when CRM.accountclassificationcode = 200001 then 'Resort Hotel'
				else NULL
		end as CRMClassification,   
		H.geographicRegionName  as geographicRegion,
		H.shortName  as country,
		H.physicalCity as city,
		H.subAreaName as state,
		H.totalRooms hotelRooms,
		H.statuscodename hotelStatus,
		LTRIM(RTRIM(RM00101.CURNCYID)) as hotelCurrency,
		B.gpSiteID
into	#Hotels-- select * 
FROM	Core.dbo.hotelsReporting H
LEFT OUTER JOIN	IC.dbo.RM00101 
	ON H.code = RM00101.CUSTNMBR
LEFT OUTER JOIN LocalCRM.dbo.account CRM
	on	H.crmGuid = CRM.accountid
LEFT OUTER JOIN	Core.dbo.Brands B
	on	H.mainBrandCode = B.code

WHERE (	synxisID > 0 OR openHospitalityCode > 0 )
and	  ( @hotelcode is null OR H.code = @hotelcode )

create clustered index ix_Hotels ON #Hotels (synxisID, openHospitalityCode)



-- get iata account manager from CRM, store in separate table
select	accountnumber, min(phg_iataglobalaccountmanageridName) iatamanager
into	#rpt_IATAManager 
from	LocalCRM.dbo.account
where	phg_iataglobalaccountmanageridName is not null
and		accountnumber is not null
group by accountnumber

create clustered index ix_iatamanager ON #rpt_IATAManager (accountnumber)


--	build master MRT table - get all the data ONCE (not multiple reads of MRT)
--  cost, sums, counts, channels, sources etc, 
--  a field that indicates whether the record is billable 
select	mrt.crsSourceID,
		CASE mrt.crsSourceID WHEN 2 THEN mrt.openhospitalityID ELSE mrt.hotelID END as crsHotelID,
		mrt.arrivalDate ,
		mrt.confirmationDate ,
		mrt.confirmationNumber,
		1 as SynXisCount,
		mrt.status ,
		mrt.channel ,
		mrt.CROcode ,
		mrt.billingDescription ,
		mrt.SecondarySource,
		mrt.subSourceReportLabel,
		CASE	mrt.billingDescription WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost	END
		  + 
		CASE mrt.channel WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 END as cost ,
		IM.iatamanager as IataGam ,
		Core.dbo.GetIATAGroups(mrt.IATANumber, mrt.confirmationdate ) IATAGroups,
		mrt.IATANumber , 
		mrt.travelAgencyName , 
		mrt.travelAgencyCity , 
		mrt.travelAgencyState , 
		mrt.travelAgencyCountry , 
		mrt.rateCategoryCode , 
		mrt.rateTypeCode ,	
		mrt.rateTypeName as ratename ,
		case	when LEFT(mrt.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(mrt.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(mrt.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(mrt.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(mrt.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(mrt.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(mrt.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(mrt.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(mrt.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(mrt.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(mrt.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(mrt.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(mrt.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(mrt.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(mrt.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(mrt.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end as rateCategory_PHG  ,
		mrt.rooms ,
		mrt.nights ,
		mrt.rooms *	mrt.nights as RoomNights,
		mrt.currency ,
		mrt.reservationRevenue ,
		mrt.reservationRevenueUSD 	,
		0 as IPreferCount ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCharges ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCost 
into	#rpt_Main
from	superset.dbo.mostrecenttransactionsreporting mrt with (nolock)
LEFT OUTER JOIN Superset.dbo.gdsBookingCost 
	ON	mrt.secondarySource = gdsBookingCost.gdsType 
	AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.synxisBookingCost 
	ON	mrt.billingDescription = synxisBookingCost.billingDescription 
	AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.pegsBookingCost 
	ON	mrt.channel = pegsBookingCost.channel 
	AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')

left outer join #rpt_IATAManager IM with (nolock)
	on	mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

where	MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
and	 (	@hotelcode is null OR mrt.hotelCode = @hotelcode )
and	  ( MRT.Channel = 'GDS' OR  (	mrt.IATANumber is not null AND mrt.IATANumber <> '' ) )
AND		MRT.channel <> 'PMS Rez Synch' 
AND		MRT.CROcode <> 'HTL_Carlton'

-- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
Union All
Select	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		
create clustered index ix_rptMain_arrivaldate_synxisID on #rpt_Main (arrivaldate, crsHotelID)
create index ix_rptMain_ConfirmNum on #rpt_Main (confirmationnumber)

delete from #rpt_Main
where crsHotelID is null


-- get all billy charges from CalculatedDebits for the date parameters
SELECT	cd.confirmationNumber ,
		cd.hotelCode ,
		cd.gpSiteID ,
		cd.arrivalDate ,
		/* CODE CHANGE: TMG 2015-09-15
				new list of criteriaIDs that are surcharges - per hiren
		
		--1=Booking Charges
		SUM(CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END)	AS bookingCharges , 		
		--2=Commissions
		SUM(CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END)
		AS commissions , 
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
			ELSE 0 END) AS surcharges 
		*/
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
			CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END END END ) AS bookingCharges , 		
		
		--2=Commissions
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 2) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
				CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END END END )
			AS commissions , 
		
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID in (1, 2) ) then 0 ELSE
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 3) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
				CASE classificationID WHEN 3 THEN 
				CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				ELSE 0 END END END END ) AS surcharges 
		
into	#rpt_BillyCharges	
from	superset.dbo.calculatedDebits CD with (nolock)

-- this join is needed to only include billing for the desired rate code reservations
join	#rpt_Main MRT 
	on	CD.confirmationNumber = MRT.confirmationNumber

join	BillingProduction.dbo.criteria C with (nolock)
	on	cd.criteriaID = C.id 
left outer join BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC
	on	Cd.criteriaID = ESC.CriteriaId
WHERE	cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
AND		( @hotelcode is null OR CD.hotelCode = @hotelcode )
--AND		cd.chargeValueInHotelCurrency <> 0  -- but without this it greatly increases the billybookingcount
			-- this is needed for the new bsi functionality, credit memos can now uncharge for cancelled reserveatins
GROUP BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID
ORDER BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID

create	clustered index ix_RptCharges ON #rpt_BillyCharges (confirmationnumber)



/*	2015-03-09 TMG

	this fix will keep billy count the same for each month, even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count, because the arrival month 

*/
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate , confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue , reservationRevenueUSD, IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (billy.HotelCode) , -- synxisid
		billy.arrivalDate, 
		missingmrt.confirmationDate,
		billy.confirmationNumber , -- confirmationnumber -- do not inflate synxis booking count because the billy charge has a different month
		0, -- do not inflate synxis booking count because the billy charge has a different month
		missingMRT.billingDescription, -- billing description
		missingMRT.channel, -- as channel ,
		missingMRT.Status , -- as status
		missingMRT.SecondarySource , -- as SecondarySource ,
		missingMRT.subSourceReportLabel , -- as subSourceReportLabel ,
		missingMRT.CROcode , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		IM.iatamanager , --as IataGam ,
		Core.dbo.GetIATAGroups(missingMRT.IATANumber, missingMRT.confirmationdate ) , --IATAGroups ,
		missingMRT.IATANumber, -- as IATANumber,
		missingMRT.travelAgencyName, -- as travelAgencyName,
		missingMRT.travelAgencyCity, -- as travelAgencyCity,
		missingMRT.travelAgencyState, -- as travelAgencyState,
		missingMRT.travelAgencyCountry, --as travelAgencyCountry,
		missingMRT.rateCategoryCode, --as rateCategoryCode,
		missingMRT.rateTypeCode, -- as rateTypeCode,
		missingMRT.rateTypeName , --as ratename ,
		case	when LEFT(missingMRT.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(missingMRT.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(missingMRT.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(missingMRT.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(missingMRT.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(missingMRT.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(missingMRT.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(missingMRT.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(missingMRT.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(missingMRT.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(missingMRT.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(missingMRT.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(missingMRT.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end , -- as rateCategory_PHG,
		null, null, null, null, null, null,  -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD,
		0 ,  -- IPreferCount
		CAST(0.00 AS  DECIMAL(18,2)),-- as IPreferCharges ,
		CAST(0.00 AS  DECIMAL(18,2)) --as IPreferCost 
from	#rpt_BillyCharges billy
join	superset.dbo.mostRecentTransactionsReporting missingMRT
	on	billy.confirmationNumber = missingMRT.confirmationNumber

left outer join #rpt_IATAManager IM with (nolock)
	on	missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

-- get only confirmation numbers that have not been added yet
left outer join #rpt_Main mrt
	on	billy.confirmationnumber = mrt.confirmationnumber
where	mrt.confirmationNumber is null
AND ( @hotelcode is null OR missingMRT.hotelcode = @hotelcode )
AND (	missingMRT.Channel = 'GDS' OR ( missingMRT.IATANumber is not null AND missingMRT.IATANumber <> '' ) )

GROUP BY billy.HotelCode, 
		billy.arrivalDate, 
		missingMRT.confirmationDate ,
		billy.confirmationNumber,
		missingMRT.billingDescription ,
		missingMRT.channel, 
		missingMRT.Status,
		missingMRT.SecondarySource, 
		missingMRT.subSourceReportLabel, 
		missingMRT.CROcode, 
		IM.iatamanager, 
		--missingMRT.IATAGroups, 
		missingMRT.IATANumber, missingMRT.confirmationdate, 
		missingMRT.travelAgencyName, 
		missingMRT.travelAgencyCity, 
		missingMRT.travelAgencyState, 
		missingMRT.travelAgencyCountry, 
		missingMRT.rateCategoryCode, 
		missingMRT.rateTypeCode, 
		missingMRT.rateTypeName



-- get IPrefer Counts and Sums by current billing date, using the following logic:
--		if matching a current MRT record - use the MRT data for channel, source, etc
--		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
--		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate, confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue, reservationRevenueUSD , IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (IP.Hotel_Code) , -- synxisid
		IP.Billing_Date, -- the IPrefer still need to be categorized as Month(MRT.ArrivalDate)
		IP.Booking_Date , -- for currency conversions
		null, -- confirmationnumber -- do not inflate synxis booking count because of an IPrefer match
		0 , -- do not inflate synxis booking count because the billy charge has a different month
		COALESCE(MRT.billingDescription, 'Non-Matching IPrefer Reservation') , -- billing description
		COALESCE(MRT.channel, 
				case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
					else	'IPrefer Prior Period'
				end ) , -- as channel ,
		COALESCE(MRT.Status, '') , -- as status
		COALESCE(MRT.SecondarySource, '') , -- as SecondarySource ,
		COALESCE(MRT.subSourceReportLabel, '') , -- as subSourceReportLabel ,
		COALESCE(MRT.CROcode, '') , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		COALESCE(MRT.IataGam, '') , -- as IataGam ,
		COALESCE(MRT.IATAGroups, '') , -- as IATAGroups ,
		COALESCE(MRT.IATANumber, '') , -- as IATANumber,
		COALESCE(MRT.travelAgencyName, '') , -- as travelAgencyName,
		COALESCE(MRT.travelAgencyCity, '') , -- as travelAgencyCity,
		COALESCE(MRT.travelAgencyState, '') , -- as travelAgencyState,
		COALESCE(MRT.travelAgencyCountry, '') , --as travelAgencyCountry,
		COALESCE(MRT.rateCategoryCode, '') , --as rateCategoryCode,
		COALESCE(MRT.rateTypeCode, '') , -- as rateTypeCode,
		COALESCE(MRT.ratename, '') , -- as ratename,
		COALESCE(MRT.rateCategory_PHG, '') , -- as rateCategory_PHG,
		null, null, null, null, null, null, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD
		COUNT(distinct IP.Booking_ID),  -- IPreferCount
		Sum(dbo.convertCurrencyToUSD( IP.Billable_Amount_Hotel_Currency ,-- IPreferCharges
			IP.Hotel_Currency, IP.Arrival_Date ) )	,
		SUM(IP.Reservation_Amount_USD * 0.02) -- as IPreferCost
from	BSI.IPrefer_Charges IP

--left outer 
-- removed left outer join - question for hiren, do we want all iprefer charges - or just those associated with the MER MRT reservations??

join #rpt_Main mrt
	on	IP.booking_id = mrt.confirmationnumber

where	IP.billing_date between @arrivalStart and @arrivalEnd
AND		( @hotelcode is null OR IP.Hotel_Code = @hotelcode )

GROUP BY IP.Hotel_Code, IP.Billing_Date , IP.Booking_Date ,
		MRT.arrivalDate, MRT.billingDescription , MRT.channel, 
		MRT.Status,
		case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
				else	'IPrefer Prior Period'
			end  , 
		MRT.SecondarySource, 
		MRT.subSourceReportLabel, 
		MRT.CROcode, 
		MRT.cost, 
		MRT.IataGam, 
		MRT.IATAGroups, 
		MRT.IATANumber, 
		MRT.travelAgencyName, 
		MRT.travelAgencyCity, 
		MRT.travelAgencyState, 
		MRT.travelAgencyCountry, 
		MRT.rateCategoryCode, 
		MRT.rateTypeCode, 
		MRT.ratename,
		MRT.rateCategory_PHG


if 	@Extended = 0 

		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.billingDescription ,
				main.channel , 
				main.secondarySource , 
				main.subSourceReportLabel , 
				main.CROcode ,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
					
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,
								
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,

				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency
				
		INTO	#Results
		
		FROM	#Hotels  as hotels 
		LEFT OUTER JOIN	#rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.Channel ,	
				main.billingDescription, 
				main.SecondarySource, 
				main.subSourceReportLabel, 
				main.CROcode --,billy.gpSiteID -- removed 2015-01-06 TMG -- put MIN(billy.gpsiteid) in above select
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelcurrency, 
				main.billingDescription ,
				main.channel ,
				main.secondarySource ,
				main.subSourceReportLabel ,
				main.CROcode ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,			
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,
				
				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency
		
		INTO	#Results_Extended

		FROM	#Hotels 	 as hotels 
		left outer JOIN #rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
		
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.crsHotelID ,
				main.Channel ,
				main.billingDescription,
				main.SecondarySource,
				main.subSourceReportLabel,
				main.CROcode ,
				hotels.hotelCurrency ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG --, billy.gpSiteID -- TMG removed and put MIN(billy.gpSiteID) in select -- 2015-01-06
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


if @extended = 0
		SELECT	*
		FROM	#Results
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	*
		FROM	#Results_Extended
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel




if	OBJECT_ID ('#usdExchange ') IS NOT NULL
	drop table #usdExchange 

if	OBJECT_ID ('#Hotels') IS NOT NULL
	drop table #Hotels

if	OBJECT_ID ('#rpt_Main') IS NOT NULL
	drop table #rpt_Main

if	OBJECT_ID ('#rpt_BillyCharges') IS NOT NULL
	drop table #rpt_BillyCharges

if	OBJECT_ID ('#rpt_IPreferCharges') IS NOT NULL
	drop table #rpt_IPreferCharges

if	OBJECT_ID ('#rpt_IATAManager') IS NOT NULL
	drop table #rpt_IATAManager

if	OBJECT_ID ('#profitabilityreport_tmg') IS NOT NULL
	drop table #profitabilityreport_tmg

if	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	drop table #profitabilityreport_tmg_ext

if	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	drop table #Results

if	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	drop table #Results_Extended




END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PW_MostRecentTransactions]'
GO
CREATE TABLE [dbo].[PW_MostRecentTransactions]
(
[hotelID] [int] NULL,
[channel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondarySource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSourceCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[status] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[confirmationDate] [date] NULL,
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[arrivalDate] [date] NULL,
[bookingLeadTime] [int] NULL,
[rateCategoryCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateTypeCode] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roomTypeCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[nights] [int] NULL,
[reservationRevenue] [decimal] (38, 2) NULL,
[currency] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[xbeTemplateName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[reservationRevenueUSD] [decimal] (18, 2) NULL,
[LocalCurrency] [nvarchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[arrivalYear] [int] NULL,
[arrivalMonth] [int] NULL,
[LocalReservationRevenue] [decimal] (38, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[rpt_TravelAgencyLocalCurrency]'
GO


CREATE procedure [Reporting].[rpt_TravelAgencyLocalCurrency]

	@HotelSynxisID int, 
	@Month int,
	@Year int

as

set nocount on;



WITH Hotel_IATA_Data
AS
(
SELECT	MRT.IATANumber 
		, YEAR(arrivalDate) AS ArrivalYear
		, MONTH(arrivalDate) AS ArrivalMonth
		, hotelID
		, SUM(nights) AS TotalRoomNights
		, COUNT(DISTINCT confirmationNumber) AS TotalReservations
		, dbo.Average(SUM(reservationRevenue), SUM(nights)) AS AVGNightlyRate
		, SUM(reservationRevenue) AS TotalRevenue
		, CAST(AVG(CAST(nights AS DECIMAL(5, 2))) AS DECIMAL(5, 2)) AS AVGLOS
		, AVG(bookingLeadTime) AS AVGLeadTime
		, SUM(bookingLeadTime) AS SUMLeadTime
		, CAST(CAST(YEAR(arrivalDate) AS char(4)) + '-' + CAST(MONTH(arrivalDate) AS char(2)) + '-01' AS datetime) AS concatDate

FROM	Superset.dbo.mostRecentTransactions MRT with (nolock)
WHERE	hotelid = @HotelSynxisID
AND		Month(ArrivalDate) between 1 and @Month 
AND		Year(ArrivalDate) >= @Year - 1
and	 (	MRT.IATANumber <> '' AND MRT.IATANumber IS NOT NULL )
AND		MRT.[status] <> 'Cancelled'
AND		MRT.[channel] <> 'PMS Rez Synch'
AND		MRT.[CROCode] NOT LIKE 'HTL_%'
GROUP BY	YEAR(arrivalDate), MONTH(arrivalDate) , hotelID , MRT.IATANumber
)

SELECT	TOP 25 @HotelSynxisID, 

		(	SELECT	top 1 MRT.IATANumber + ' ' + MRT.travelAgencyName + '; ' + MRT.travelAgencyCity + ', ' + MRT.travelAgencyCountry as TravelAgency 
			FROM	dbo.mostRecentTransactions mrt with (nolock)
			where	hotelid = @HotelSynxisID
			AND		IATANumber = AllList.IATANumber
			AND		Month(ArrivalDate) between 1 and @Month
			AND		Year(ArrivalDate) >= @Year - 1
			and	 (	MRT.IATANumber <> '' AND MRT.IATANumber IS NOT NULL )
			AND		MRT.[status] <> 'Cancelled'
			AND		MRT.[channel] <> 'PMS Rez Synch'
			AND		MRT.[CROCode] NOT LIKE 'HTL_%'
		) TravelAgency
,		SUM(ThisYear.TotalRoomNights) AS TotalRoomNights
,		SUM(ThisYear.TotalReservations) AS TotalReservations
,		SUM(ThisYear.TotalRevenue) AS TotalRevenue
,		SUM(ThisYear.SUMLeadTime) AS SUMLeadTime
,		Superset.dbo.Average(SUM(ThisYear.TotalRevenue),SUM(ThisYear.TotalRoomNights)) AS AVGNightlyRate
,		Superset.dbo.Average(SUM(ThisYear.TotalRoomNights),SUM(ThisYear.TotalReservations)) AS AVGLOS
,		Superset.dbo.Average(SUM(ThisYear.SUMLeadTime),SUM(ThisYear.TotalReservations)) AS AVGLeadTime
,		SUM(LastYear.TotalRoomNights) AS LYTotalRoomNights
,		SUM(LastYear.TotalReservations) AS LYTotalReservations
,		SUM(LastYear.TotalRevenue) AS LYTotalRevenue
,		SUM(LastYear.SUMLeadTime) AS LYSUMLeadTime
,		Superset.dbo.Average(SUM(LastYear.TotalRevenue),SUM(LastYear.TotalRoomNights)) AS LYAVGNightlyRate
,		Superset.dbo.Average(SUM(LastYear.TotalRoomNights),SUM(LastYear.TotalReservations)) AS LYAVGLOS
,		Superset.dbo.Average(SUM(LastYear.SUMLeadTime),SUM(LastYear.TotalReservations)) AS LYAVGLeadTime
,		Superset.dbo.PercentChange(SUM(ThisYear.TotalRoomNights),SUM(LastYear.TotalRoomNights)) AS PctChgRoomNights
,		Superset.dbo.PercentChange(SUM(ThisYear.TotalReservations),SUM(LastYear.TotalReservations)) AS PctChgReservations
,		Superset.dbo.PercentChange(SUM(ThisYear.TotalRevenue),SUM(LastYear.TotalRevenue)) AS PctChgRevenue
,		Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYear.TotalRevenue),SUM(ThisYear.TotalRoomNights)),
		Superset.dbo.Average(SUM(LastYear.TotalRevenue),SUM(LastYear.TotalRoomNights))) AS PctChgAVGNightlyRate
,		Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYear.TotalRoomNights),SUM(ThisYear.TotalReservations)),
		Superset.dbo.Average(SUM(LastYear.TotalRoomNights),SUM(LastYear.TotalReservations))) AS PctChgAVGLOS
,		Superset.dbo.PercentChange(Superset.dbo.Average(SUM(ThisYear.SUMLeadTime),SUM(ThisYear.TotalReservations)),
		Superset.dbo.Average(SUM(LastYear.SUMLeadTime),SUM(LastYear.TotalReservations))) AS PctChgAVGLeadTime

FROM (	select  BRT.IATANumber, BRT.arrivalMonth
		from	Hotel_IATA_Data BRT 
		WHERE	BRT.hotelID = @HotelSynxisID
		AND		MONTH(BRT.concatDate) BETWEEN 1 AND @Month
		AND		(	YEAR(BRT.concatDate) = @Year
				OR
					YEAR(BRT.concatDate) = @Year - 1
				)
		GROUP BY	BRT.IATANumber, BRT.arrivalMonth
	)	 AllList



LEFT OUTER JOIN
	(	SELECT		BRT1.IATANumber
			,		BRT1.ArrivalYear
			,		BRT1.ArrivalMonth
			,		BRT1.hotelID
			,		BRT1.TotalRoomNights
			,		BRT1.TotalReservations
			,		BRT1.AVGNightlyRate
			,		BRT1.TotalRevenue
			,		BRT1.AVGLOS
			,		BRT1.AVGLeadTime
			,		BRT1.SUMLeadTime
			,		BRT1.concatDate
		FROM		Hotel_IATA_Data BRT1
		WHERE		BRT1.hotelID = @HotelSynxisID
		AND		(BRT1.ArrivalMonth BETWEEN 1 AND @Month)
		AND		BRT1.ArrivalYear = @Year
	) AS ThisYear
	ON	AllList.IATANumber = ThisYear.IATANumber
	AND	AllList.ArrivalMonth = ThisYear.ArrivalMonth

LEFT OUTER JOIN
	(	SELECT		BRT2.IATANumber
			,		BRT2.ArrivalYear
			,		BRT2.ArrivalMonth
			,		BRT2.hotelID
			,		BRT2.TotalRoomNights
			,		BRT2.TotalReservations
			,		BRT2.AVGNightlyRate
			,		BRT2.TotalRevenue
			,		BRT2.AVGLOS
			,		BRT2.AVGLeadTime
			,		BRT2.SUMLeadTime
			,		BRT2.concatDate
		FROM		Hotel_IATA_Data BRT2
		WHERE		BRT2.hotelID = @HotelSynxisID
		AND		(BRT2.ArrivalMonth BETWEEN 1 AND @Month)
		AND		BRT2.ArrivalYear = @Year - 1
		) AS LastYear
		ON	ThisYear.IATANumber = LastYear.IATANumber
		AND	ThisYear.ArrivalMonth = LastYear.ArrivalMonth
			
WHERE	ThisYear.IATANumber is not null

GROUP BY	AllList.IATANumber
ORDER BY	SUM(ThisYear.TotalRoomNights) DESC , SUM(ThisYear.TotalReservations) DESC


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ipr_iPreferBillingReconciliation]'
GO



CREATE PROCEDURE [dbo].[ipr_iPreferBillingReconciliation]
	@StartDate DATE,
	@EndDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @StartDate DATE = '2017-1-1'
--DECLARE @EndDate DATE = '2017-12-31'



SELECT	CASE WHEN C.classificationID = 5 AND SUM(C.Points_Earned) = 0 THEN 'Billed but no points'
			WHEN ISNULL(C.classificationID, 0) <> 5 AND SUM(C.Points_Earned) <> 0 THEN 'Points but not billed'
			ELSE ''
		END AS TransStatus,
		CASE WHEN ISNULL(MRT.confirmationNumber, '') <> '' THEN 'Superset'
			ELSE 'Manual'
		END AS TransactionSource,
		C.confirmationNumber,
		ISNULL(IBC.Channel, 'Manual') AS BookingSource,
		SUM(C.chargeValueInUSD) AS ChargeInUSD,
		SUM(CASE WHEN C.classificationID = 5 THEN C.chargeValueInUSD END) AS iPreferClassifiedCharges,
		C.roomRevenueInUSD AS ReservationRevenueUSD,
		SUM(C.Points_Earned) AS PointsAwarded,
		MRT.loyaltyNumber AS iPreferNumber,
		CASE 
			WHEN MRT.LoyaltyNumberTagged = 1 THEN 'Tagged' 
			WHEN ISNULL(MRT.loyaltyNumber, '') = '' THEN 'None' 
			WHEN MRT.LoyaltyNumberValidated = 0 THEN 'Invalid' 
			WHEN MRT.LoyaltyNumberValidated = 1 THEN 'Valid'
			ELSE 'None' 
		END AS LoyaltyNumberStatus

FROM	(
		SELECT	C1.confirmationNumber,
				C1.hotelCode,
				C1.classificationID,
				SUM(C1.chargeValueInUSD) AS chargeValueInUSD,
				SUM(C1.roomRevenueInUSD) AS roomRevenueInUSD,
				SUM(TD1.Points_Earned) AS Points_Earned

		FROM	ReservationBilling.dbo.Charges C1
				LEFT JOIN
				Superset.BSI.TransactionDetailedReport TD1
				ON C1.confirmationNumber = TD1.Booking_ID

		WHERE	COALESCE(C1.classificationID, 0) = 5
				AND
				C1.arrivalDate BETWEEN @StartDate AND @EndDate

		GROUP BY	C1.confirmationNumber,
					C1.hotelCode,
					C1.classificationID

		HAVING	SUM(TD1.Points_Earned) = 0
		UNION
		SELECT	TD2.[Booking_ID] AS confirmationNumber,
				TD2.Hotel_Code AS hotelCode,
				C2.classificationID,
				SUM(C2.chargeValueInUSD) AS chargeValueInUSD,
				SUM(C2.roomRevenueInUSD) AS roomRevenueInUSD,
				SUM(TD2.Points_Earned) AS Points_Earned

		FROM	Superset.BSI.TransactionDetailedReport TD2
				LEFT JOIN
				ReservationBilling.dbo.Charges C2
				ON TD2.Booking_ID = C2.confirmationNumber

		WHERE	COALESCE(C2.classificationID, 0) <> 5
				AND
				TD2.Arrival_Date BETWEEN @StartDate AND @EndDate

		GROUP BY	TD2.Booking_ID,
					TD2.Hotel_Code,
					C2.classificationID

		HAVING	SUM(TD2.Points_Earned) <> 0
		) AS C
		LEFT JOIN
		Superset.dbo.mostRecentTransactions MRT
		ON
		C.confirmationNumber = MRT.confirmationNumber
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON C.confirmationNumber = ASI.Booking_ID
			AND C.hotelCode = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept

GROUP BY	C.confirmationNumber,
			CASE WHEN ISNULL(MRT.confirmationNumber, '') <> '' THEN 'Superset'
				ELSE 'Manual'
			END,
			C.classificationID,
			C.roomRevenueInUSD,
			IBC.Channel,
			MRT.loyaltyNumber,
			CASE 
			WHEN MRT.LoyaltyNumberTagged = 1 THEN 'Tagged' 
			WHEN ISNULL(MRT.loyaltyNumber, '') = '' THEN 'None' 
			WHEN MRT.LoyaltyNumberValidated = 0 THEN 'Invalid'
			WHEN MRT.LoyaltyNumberValidated = 1 THEN 'Valid'
			ELSE 'None' 
			END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[profitabilityReport_Old2]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 03/15/2011
-- Description:	underlying query for all profitability reports
-- UPDATE 5-25-2011: changed data source info for new GP and Calculated Debits schemas
-- =============================================
CREATE procedure [dbo].[profitabilityReport_Old2] 
	-- Add the parameters for the stored procedure here
	@arrivalStart date, 
	@arrivalEnd date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


--get all the exchange rates we'll need for this period
DECLARE @usdExchange table(invDate datetime, currCode char(3), exchRate decimal(20,8))
INSERT INTO @usdExchange 
SELECT distinct invoiceDate, hotelCurrencyCode, CASE hotelCurrencyCode WHEN 'USD' THEN 1 ELSE gpCurr.XCHGRATE END as xchRate
FROM Superset.dbo.calculatedDebits 
LEFT OUTER JOIN DYNAMICS.dbo.MC00100 as gpCurr 
	ON calculatedDebits.hotelCurrencyCode = gpCurr.CURNCYID
	AND invoiceDate BETWEEN gpCurr.EXCHDATE AND gpCurr.EXPNDATE 
	AND (gpCurr.EXGTBLID LIKE '%AVG%' OR gpCurr.EXGTBLID LIKE '%AVERAGE%')
WHERE arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
AND invoiceDate is not null


--get the hotel information
DECLARE @hotels table(synxisID int PRIMARY KEY, 
	hotelCode nvarchar(10), 
	hotelName nvarchar(250), 
	primaryBrand nvarchar(7), 
	gpSalesTerritory nvarchar(15), 
	AMD nvarchar(160), 
	RD nvarchar(160), 
	RAM nvarchar(160), 
	geographicRegion nvarchar(50), 
	country nvarchar(250), 
	state nvarchar(250), 
	hotelRooms int,
	hotelStatus nvarchar(25),
	hotelCurrency nvarchar(10)
	)
INSERT INTO @hotels 
SELECT distinct
COALESCE(synxisID, 0) as synxisID,
COALESCE(hotelsReporting.code, 'UNKNOWN') as hotelCode,
COALESCE(hotelsReporting.hotelName,'UNKNOWN') as hotelName,
COALESCE(hotelsReporting.mainBrandCode, 'UNKNOWN') as primaryBrand,
LTRIM(RTRIM(COALESCE(RM00101.SALSTERR, 'UNKNOWN'))) as gpSalesTerritory,
COALESCE(hotelsReporting.phg_areamanageridName, 'UNKNOWN') as AMD,
COALESCE(hotelsReporting.phg_regionalmanageridName, 'UNKNOWN') as RD,
COALESCE(hotelsReporting.phg_revenueaccountmanageridName, 'UNKNOWN') as RAM,
COALESCE(hotelsReporting.geographicRegionName, 'UNKNOWN') as geographicRegion,
COALESCE(hotelsReporting.shortName, 'UNKNOWN') as country,
COALESCE(hotelsReporting.subAreaName, 'UNKNOWN') as state,
COALESCE(hotelsReporting.totalRooms, 0) as hotelRooms,
COALESCE(hotelsReporting.statuscodename, 'UNKNOWN') as hotelStatus,
LTRIM(RTRIM(COALESCE(RM00101.CURNCYID, 'UNKNOWN'))) as hotelCurrency
FROM Core.dbo.hotelsReporting LEFT OUTER JOIN
IC.dbo.RM00101 ON hotelsReporting.code = RM00101.CUSTNMBR
WHERE synxisID > 0


--get all the combinations of sources that produced in the period, along with cost
Declare @main as table (synxisID int, arrivalYear int, arrivalMonth int, billingDescription nvarchar(250), channel nvarchar(250), secondarySource nvarchar(250),
subSourceReportLabel nvarchar(250), CROcode nvarchar(25), cost decimal(10,2))
INSERT INTO @main 
select distinct
mrt.hotelID as synxisID,
YEAR(mrt.arrivalDate) as arrivalYear,
MONTH(mrt.arrivalDate) as arrivalMonth,
mrt.billingDescription,
mrt.Channel,
mrt.SecondarySource,
mrt.subSourceReportLabel,
mrt.CROcode,
((CASE mrt.billingDescription 
		WHEN '' THEN pegsBookingCost.cost 
		WHEN 'SynXis Call Center' THEN (Superset.dbo.avgVoiceMinuteCost() + synxisBookingCost.cost)
		ELSE synxisBookingCost.cost
	END) 
	+ 
	(CASE mrt.channel 
		WHEN 'GDS' THEN gdsBookingCost.cost 
		ELSE 0 
	END)) as totalCost
from superset.dbo.mostrecenttransactionsreporting as mrt
LEFT JOIN Superset.dbo.gdsBookingCost ON mrt.secondarySource = gdsBookingCost.gdsType AND 
mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
LEFT JOIN Superset.dbo.synxisBookingCost ON mrt.billingDescription = synxisBookingCost.billingDescription AND 
mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
LEFT JOIN Superset.dbo.pegsBookingCost ON mrt.channel = pegsBookingCost.channel AND 
mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')
where arrivalDate BETWEEN @arrivalStart AND @arrivalEnd


--get all the billable transactions from MRT
Declare @billableMRT as table (synxisID int, arrivalYear int, arrivalMonth int, billingDescription nvarchar(250), channel nvarchar(250), secondarySource nvarchar(250),
subSourceReportLabel nvarchar(250), CROcode nvarchar(25), synxisBookingCount int, hotelRevenueUSD decimal(18,2), roomNights int, avgNightlyRateUSD decimal(10,2), avgLengthOfStay decimal(10,2))
INSERT INTO @billableMRT
	select 
	mrt.hotelID as synxisID,	
	YEAR(mrt.arrivalDate) as arrivalYear,
	MONTH(mrt.arrivalDate) as arrivalMonth,
	mrt.billingDescription,
	mrt.Channel,
	mrt.SecondarySource,
	mrt.subSourceReportLabel,
	mrt.CROcode,
	COUNT(confirmationNumber) as synxisBookings
	,SUM(mrt.reservationRevenueUSD) as hotelRevenueUSD
	,SUM(mrt.roomNights) as roomNights
	,SUM(mrt.reservationRevenueUSD)/SUM(mrt.roomNights) as avgNightlyRateUSD
	,SUM(mrt.nights)/COUNT(confirmationNumber) as avgLengthOfStay
from Superset.dbo.mostrecenttransactionsreporting as mrt
where 	mrt.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd AND
	mrt.status <> 'Cancelled' AND
	mrt.channel <> 'PMS Rez Synch' AND
	mrt.CROcode <> 'HTL_Carlton'
GROUP BY 	mrt.hotelID,	
	YEAR(mrt.arrivalDate),
	MONTH(mrt.arrivalDate),
	mrt.billingDescription,
	mrt.Channel,
	mrt.SecondarySource,
	mrt.subSourceReportLabel,
	mrt.CROcode


--get all the charges from Billy
DECLARE @charges table(synxisID int, arrivalYear int, arrivalMonth int, billingDescription nvarchar(250), channel nvarchar(250), secondarySource nvarchar(250),
subSourceReportLabel nvarchar(250), CROcode nvarchar(25), billyBookingCount int, bookingCharges decimal(12,2), commissions decimal(12,2), surcharges decimal(12,2), 
hotelCurrency char(3), exchangeRate decimal(10,7))
INSERT INTO @charges
SELECT
	mrt.hotelID as synxisID,	
	YEAR(mrt.arrivalDate) as arrivalYear,
	MONTH(mrt.arrivalDate) as arrivalMonth,
	mrt.billingDescription,
	mrt.Channel,
	mrt.SecondarySource,
	mrt.subSourceReportLabel,
	mrt.CROcode,
	COUNT(distinct cd.confirmationNumber) as billyBookings
	,SUM(CASE classificationID 
			WHEN 1 THEN chargeValueInHotelCurrency 
			WHEN 3 THEN CASE WHEN criteria.itemCode IS NULL THEN chargeValueInHotelCurrency ELSE 0 END --3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END) / COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD), 99999) AS bookingCharges --1=Booking
	,SUM(CASE classificationID WHEN 2 THEN chargeValueInHotelCurrency ELSE 0 END) / COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD), 99999) AS commissions --2=Commission
	,SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN criteria.itemCode IS NOT NULL THEN chargeValueInHotelCurrency ELSE 0 END 
			ELSE 0 END) / COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD), 99999) AS surcharges --3=Surcharge, only if the item code was overridden do we count it separately per Hiren
	,cd.hotelCurrencyCode
	,COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD), 99999) as exchangeRate
from superset.dbo.calculatedDebits as cd
INNER JOIN BillingProduction.dbo.criteria on cd.criteriaID = criteria.id
left outer join superset.dbo.mostrecenttransactionsreporting as mrt on cd.confirmationNumber = mrt.confirmationNumber
LEFT OUTER JOIN @usdExchange as usdExchange ON cd.invoiceDate = usdExchange.invDate AND cd.hotelCurrencyCode = usdExchange.currCode
LEFT OUTER JOIN CurrencyRates.dbo.dailyRates on mrt.confirmationDate = dailyRates.rateDate AND cd.hotelCurrencyCode = dailyRates.code
WHERE cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
GROUP BY 	mrt.hotelID,	
	YEAR(mrt.arrivalDate),
	MONTH(mrt.arrivalDate),
	mrt.billingDescription,
	mrt.Channel,
	mrt.SecondarySource,
	mrt.subSourceReportLabel,
	mrt.CROcode,
	cd.hotelCurrencyCode




--put it all together
SELECT
main.synxisID
,COALESCE(hotels.hotelCode, 'UNKNOWN') as hotelCode
,COALESCE(hotels.hotelName, 'UNKNOWN') as hotelName
,COALESCE(hotels.primaryBrand, 'UNKNOWN') as primaryBrand
,COALESCE(hotels.gpSalesTerritory, 'UNKNOWN') as gpSalesTerritory
,COALESCE(hotels.AMD, 'UNKNOWN') as AMD
,COALESCE(hotels.RD, 'UNKNOWN') as RD
,COALESCE(hotels.RAM, 'UNKNOWN') as RAM
,COALESCE(hotels.geographicRegion, 'UNKNOWN') as geographicRegion
,COALESCE(hotels.country, 'UNKNOWN') as country
,COALESCE(hotels.state, 'UNKNOWN') as state
,COALESCE(hotels.hotelRooms, 0) as hotelRooms
,COALESCE(hotels.hotelStatus, 'UNKNOWN') as hotelStatus
,main.arrivalYear
,main.arrivalMonth
,main.billingDescription
,main.channel
,main.secondarySource
,main.subSourceReportLabel
,main.CROcode
,COALESCE(main.cost, 0) as costPerBooking
,COALESCE(billy.hotelCurrency, hotels.hotelCurrency, 'UNKNOWN') as hotelCurrency
,COALESCE(billy.exchangeRate, 0) as exchangeRate
,COALESCE(mrt.synxisBookingCount,0) as synxisBookingCount
,COALESCE(main.cost, 0) * COALESCE(mrt.synxisBookingCount,0) as totalCost
,COALESCE(mrt.hotelRevenueUSD,0) as hotelRevenueUSD
,COALESCE(mrt.roomNights,0) as roomNights
,COALESCE(mrt.avgNightlyRateUSD,0) as avgNightlyRateUSD
,COALESCE(mrt.avgLengthOfStay,0) as avgLengthOfStay
,COALESCE(billy.billyBookingCount,0) as billyBookingCount
,COALESCE(billy.bookingCharges,0) as bookingCharges
,COALESCE(billy.commissions,0) as commissions
,COALESCE(billy.surcharges,0) as surcharges

FROM @main as main

LEFT OUTER JOIN @hotels as hotels on main.synxisID = hotels.synxisID

LEFT OUTER JOIN @billableMRT as mrt on main.synxisID = mrt.synxisID and
main.arrivalYear = mrt.arrivalYear and
main.arrivalMonth = mrt.arrivalMonth and
main.billingDescription = mrt.billingDescription and
main.Channel = mrt.Channel and
main.SecondarySource = mrt.SecondarySource and
main.subSourceReportLabel = mrt.subSourceReportLabel and
main.CROcode = mrt.CROcode

LEFT OUTER JOIN @charges as billy on main.synxisID = billy.synxisID and
main.arrivalYear = billy.arrivalYear and
main.arrivalMonth = billy.arrivalMonth and
main.billingDescription = billy.billingDescription and
main.Channel = billy.Channel and
main.SecondarySource = billy.SecondarySource and
main.subSourceReportLabel = billy.subSourceReportLabel and
main.CROcode = billy.CROcode

WHERE billy.billyBookingCount IS NOT NULL 
OR mrt.synxisBookingCount IS NOT NULL

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[iPreferGDSYearOverYearRevenue]'
GO

CREATE PROCEDURE [dbo].[iPreferGDSYearOverYearRevenue]
	-- Add the parameters for the stored procedure here
	@startDate Date,
	@endDate Date,
	@sameStore bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @previousYearSD DATE =  DATEADD(year, -1, @startDate);
DECLARE @previousYearED DATE =  DATEADD(year, -1, @endDate);


;WITH cte_hotelId(hotelID,arrivalDate,[confirmationNumber],[roomnights],[reservationRevenueUSD],[loyaltyNumber],previousYearArrivalDate)
AS
(
SELECT hotelID,arrivalDate,[confirmationNumber],[nights] * [rooms],[reservationRevenueUSD],[loyaltyNumber],DATEADD(year, -1, arrivalDate)
FROM Superset.dbo.mostrecenttransactions
WHERE channel = 'GDS'
  AND [status] <> 'Canceled'
  AND arrivalDate BETWEEN @previousYearSD AND @endDate
),
cte_SameStore(hotelID, firstArrival)
AS
(
SELECT DISTINCT hotelID, MIN(arrivalDate) as firstArrival
FROM cte_hotelId
WHERE arrivalDate BETWEEN @previousYearSD AND @previousYearED
GROUP BY hotelID
)

SELECT @startDate as [Start Date], @endDate as [End Date],COUNT([confirmationNumber]) as [Number Of Bookings],SUM([roomNights])as [Number Of Nights],SUM([reservationRevenueUSD]) as [Reservation Revenue in USD]
FROM cte_hotelId h
INNER JOIN Superset.BSI.Customer_Profile_Import cpi
	ON h.loyaltyNumber = cpi.iPrefer_Number
LEFT OUTER JOIN cte_SameStore s
	ON h.hotelID = s.hotelID
	AND h.previousYearArrivalDate >= s.firstArrival
WHERE arrivalDate BETWEEN @startDate AND @endDate
       AND (@sameStore = 0
			OR (@sameStore = 1 AND s.hotelID IS NOT NULL)
		)
UNION ALL
SELECT  @previousYearSD as [Start Date], @previousYearED as [End Date],COUNT([confirmationNumber]) as [Number Of Bookings],SUM([roomNights])as [Number Of Nights],SUM([reservationRevenueUSD]) as [Reservation Revenue in USD]
FROM cte_hotelId h
INNER JOIN Superset.BSI.Customer_Profile_Import cpi
	ON h.loyaltyNumber = cpi.iPrefer_Number
LEFT OUTER JOIN cte_SameStore s
	ON h.hotelID = s.hotelID
	AND h.arrivalDate >= s.firstArrival
WHERE arrivalDate BETWEEN @previousYearSD AND @previousYearED
       AND (@sameStore = 0
			OR (@sameStore = 1 AND s.hotelID IS NOT NULL)
		)

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Hotel_Export_BCD]'
GO







CREATE view [BSI].[Hotel_Export_BCD] 
as

select	cast(h.crmGuid as varchar(50)) [crmGuid], h.code , cast(synxisID as varchar(10)) [synxisID], hotelName , 
		REPLACE(REPLACE(physicalAddress1 , CHAR(13), ' '), CHAR(10), ' ') as physicalAddress1 , 
		REPLACE(REPLACE(physicalAddress2 , CHAR(13), ' '), CHAR(10), ' ') as physicalAddress2 , 
		physicalCity , state , subAreaName , 
		country , shortName , postalCode , cast(geoLocationWKT as varchar(max)) geoLocationWKT,
		cast(yearBuilt as varchar(5)) [yearBuilt], cast(yearLastRenovated as varchar(5)) [yearLastRenovated], cast(floors as varchar(5)) [floors], primaryPhone , 
		primaryEmail , primaryWebsite , cast(openingDate as varchar(11)) [openingDate], cast(totalRooms as varchar(5)) [totalRooms], currencyCode ,
		REPLACE(REPLACE(competitiveSet, CHAR(13), ' '), CHAR(10), ' ') [competitiveSet], 
		geographicRegionCode , geographicRegionName , phg_brand , phg_regionalmanageridName , phg_areamanageridName , 
		statuscodename , phg_revenueaccountmanageridName , phg_regionaladministrationidName , 
		cast(PHG_InvoicePastDueStatus as varchar(2)) [PHG_InvoicePastDueStatus], cast(iPreferParticipant as varchar(2)) [iPreferParticipant], 
		H.Amenity, '10-Jan-2018' AS lastupdatedate
from	BSI.Hotel_Populate H
join	BSI.Hotel_LastUpdated LU on	LU.code = H.code
WHERE synxisID IN(58277,63707,60520,29114,66827,55982,60169,26931,-2222322,-2222323,59017,-222258955,69684,70996,58210,26980,60094,63191,63196,27021,27025,1,67452,28903,63736,27051,63451,56360,62410,59179,27110,60829,28065,60830,26973,60831,27514,58080,28064,64069,55062)

UNION ALL

select	cast(h.crmGuid as varchar(50)) [crmGuid], h.code , cast(synxisID as varchar(10)) [synxisID], hotelName , 
		REPLACE(REPLACE(physicalAddress1 , CHAR(13), ' '), CHAR(10), ' ') as physicalAddress1 , 
		REPLACE(REPLACE(physicalAddress2 , CHAR(13), ' '), CHAR(10), ' ') as physicalAddress2 , 
		physicalCity , state , subAreaName , 
		country , shortName , postalCode , cast(geoLocationWKT as varchar(max)) geoLocationWKT,
		cast(yearBuilt as varchar(5)) [yearBuilt], cast(yearLastRenovated as varchar(5)) [yearLastRenovated], cast(floors as varchar(5)) [floors], primaryPhone , 
		primaryEmail , primaryWebsite , cast(openingDate as varchar(11)) [openingDate], cast(totalRooms as varchar(5)) [totalRooms], currencyCode ,
		REPLACE(REPLACE(competitiveSet, CHAR(13), ' '), CHAR(10), ' ') [competitiveSet], 
		geographicRegionCode , geographicRegionName , phg_brand , phg_regionalmanageridName , phg_areamanageridName , 
		statuscodename , phg_revenueaccountmanageridName , phg_regionaladministrationidName , 
		cast(PHG_InvoicePastDueStatus as varchar(2)) [PHG_InvoicePastDueStatus], cast(iPreferParticipant as varchar(2)) [iPreferParticipant], 
		H.Amenity, LU.lastupdatedate 
from	BSI.Hotel_Populate H
join	BSI.Hotel_LastUpdated LU on	LU.code = H.code
WHERE synxisID NOT IN(58277,63707,60520,29114,66827,55982,60169,26931,-2222322,-2222323,59017,-222258955,69684,70996,58210,26980,60094,63191,63196,27021,27025,1,67452,28903,63736,27051,63451,56360,62410,59179,27110,60829,28065,60830,26973,60831,27514,58080,28064,64069,55062)





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[profitabilityReportOld]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 03/15/2011
-- Description:	underlying query for all profitability reports
-- UPDATE 5-25-2011: changed data source info for new GP and Calculated Debits schemas
-- =============================================
CREATE procedure [dbo].[profitabilityReportOld] 
	-- Add the parameters for the stored procedure here
	@arrivalStart date, 
	@arrivalEnd date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @usdExchange table(invDate datetime, currCode char(3), exchRate decimal(20,8))

	INSERT INTO @usdExchange 
	SELECT distinct invoiceDate, hotelCurrencyCode, CASE hotelCurrencyCode WHEN 'USD' THEN 1 ELSE gpCurr.XCHGRATE END as xchRate
	FROM Superset.dbo.calculatedDebits 
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 as gpCurr 
		ON calculatedDebits.hotelCurrencyCode = gpCurr.CURNCYID
		AND invoiceDate BETWEEN gpCurr.EXCHDATE AND gpCurr.EXPNDATE 
		AND (gpCurr.EXGTBLID LIKE '%AVG%' OR gpCurr.EXGTBLID LIKE '%AVERAGE%')
	WHERE arrivalDate BETWEEN @arrivalStart AND @arrivalEnd


    -- Insert statements for procedure here
	DECLARE @costs table(arrivalYear int, arrivalMonth int, billingDescription nvarchar(50), channel nvarchar(50), secondarySource nvarchar(50), costPerBooking decimal(5,2));

	INSERT INTO @costs
	SELECT distinct
	YEAR(arrivalDate) as arrivalYear,
	MONTH(arrivalDate) as arrivalMonth,
	mostRecentTransactions.billingDescription,
	mostRecentTransactions.channel,
	mostRecentTransactions.secondarySource,
	((CASE mostRecentTransactions.billingDescription 
		WHEN '' THEN pegsBookingCost.cost 
		WHEN 'SynXis Call Center' THEN (Superset.dbo.avgVoiceMinuteCost() + synxisBookingCost.cost)
		ELSE synxisBookingCost.cost
	END) 
	+ 
	(CASE mostRecentTransactions.channel 
		WHEN 'GDS' THEN gdsBookingCost.cost 
		ELSE 0 
	END)) as totalCost
	FROM Superset.dbo.mostRecentTransactions
LEFT JOIN Superset.dbo.gdsBookingCost ON mostrecenttransactions.secondarySource = gdsBookingCost.gdsType AND 
mostrecenttransactions.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
LEFT JOIN Superset.dbo.synxisBookingCost ON mostrecenttransactions.billingDescription = synxisBookingCost.billingDescription AND 
mostrecenttransactions.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
LEFT JOIN Superset.dbo.pegsBookingCost ON mostrecenttransactions.channel = pegsBookingCost.channel AND 
mostrecenttransactions.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')
	WHERE status <> 'Cancelled' AND
	arrivalDate BETWEEN @arrivalStart AND @arrivalEnd AND
	mostRecentTransactions.channel <> 'PMS Rez Synch' AND
	CROcode <> 'HTL_Carlton';


	
	DECLARE @hotels table(synxisID int PRIMARY KEY, 
		hotelCode nvarchar(10), 
		hotelName nvarchar(250), 
		primaryBrand nvarchar(7), 
		gpSalesTerritory nvarchar(15), 
		AMD nvarchar(160), 
		RD nvarchar(160), 
		RAM nvarchar(160), 
		geographicRegion nvarchar(50), 
		country nvarchar(250), 
		state nvarchar(250), 
		hotelRooms int,
		hotelStatus nvarchar(25)
		)
	INSERT INTO @hotels 
	SELECT distinct
	COALESCE(synxisID, 0) as synxisID,
	COALESCE(hotelsReporting.code, 'UNKNOWN') as hotelCode,
	COALESCE(hotelsReporting.hotelName,'UNKNOWN') as hotelName,
	COALESCE(hotelsReporting.mainBrandCode, 'UNKNOWN') as primaryBrand,
	COALESCE(RM00101.SALSTERR, 'UNKNOWN') as gpSalesTerritory,
	COALESCE(hotelsReporting.phg_areamanageridName, 'UNKNOWN') as AMD,
	COALESCE(hotelsReporting.phg_regionalmanageridName, 'UNKNOWN') as RD,
	COALESCE(hotelsReporting.phg_revenueaccountmanageridName, 'UNKNOWN') as RAM,
	COALESCE(hotelsReporting.geographicRegionName, 'UNKNOWN') as geographicRegion,
	COALESCE(hotelsReporting.shortName, 'UNKNOWN') as country,
	COALESCE(hotelsReporting.subAreaName, 'UNKNOWN') as state,
	COALESCE(hotelsReporting.totalRooms, 0) as hotelRooms,
	COALESCE(hotelsReporting.statuscodename, 'UNKNOWN') as hotelStatus
	FROM Core.dbo.hotelsReporting LEFT OUTER JOIN
	IC.dbo.RM00101 ON hotelsReporting.code = RM00101.CUSTNMBR
	WHERE synxisID > 0
	
	--runtime for above subqueries on July 10, 2012 = 00:00:46
	
	DECLARE @charges table(confirmationNumber nvarchar(50) PRIMARY KEY, hotelCurrencyCode char(3), invoiceDate date, bookingCharges decimal(12,2), commissions decimal(12,2), surcharges decimal(12,2))
	
	INSERT INTO @charges SELECT
		confirmationNumber,
		hotelCurrencyCode,
		invoiceDate,
		SUM(CASE classificationID WHEN 1 THEN chargeValueInHotelCurrency ELSE 0 END) AS bookCharges --1=Booking
		,SUM(CASE classificationID WHEN 2 THEN chargeValueInHotelCurrency ELSE 0 END) AS commCharges --2=Commission
		,SUM(CASE classificationID WHEN 3 THEN chargeValueInHotelCurrency ELSE 0 END) AS surcharges --3=Surcharge
		FROM [Superset].[dbo].[calculatedDebits] 
		WHERE arrivalDate between @arrivalStart and @arrivalEnd
		GROUP BY confirmationNumber,
		hotelCurrencyCode,
		invoiceDate	
	
	--runtime for above subqueries on July 10, 2012 = 00:01:00
	
	select 
	YEAR(mrt.arrivalDate) as arrivalYear,
	MONTH(mrt.arrivalDate) as arrivalMonth,
	mrt.hotelID as synxisID,
	MIN(hotels.hotelCode) as hotelCode,
	MIN(hotels.hotelName) as hotelName,
	MIN(hotels.primaryBrand) as primaryBrand,
	MIN(hotels.gpSalesTerritory) as gpSalesTerritory,
	MIN(hotels.AMD) as AMD,
	MIN(hotels.RD) as RD,
	MIN(hotels.RAM) as RAM,
	MIN(hotels.geographicRegion) as geographicRegion,
	MIN(hotels.country) as country,
	MIN(hotels.state) as state,
	MIN(hotels.hotelRooms) as hotelRooms,
	MIN(hotels.hotelStatus) as hotelStatus,
	mrt.Channel,
	mrt.SecondarySource,
	mrt.subSourceReportLabel,
	COUNT(mrt.confirmationNumber) as synxisBookings,
	COUNT(charges.confirmationNumber) as billyBookings,
	MAX(COALESCE(costs.costPerBooking, 99999)) as costPerBooking,
	(COUNT(mrt.confirmationNumber) * MAX(COALESCE(costs.costPerBooking, 99999))) AS totalCost,
	(COALESCE(SUM(bookingCharges),0) / COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)),99999)) as bookingCharges
	,(COALESCE(SUM(commissions),0) / COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)), 99999)) as commissions
	,(COALESCE(SUM(surcharges),0) / COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)), 99999)) as surcharges
	,COALESCE(COALESCE(MAX(usdExchange.exchRate), MAX(dailyRates.toUSD)), 99999) as exchRate
	,hotelCurrencyCode
	,SUM(mrt.reservationRevenueUSD) as hotelRevenueUSD
	,SUM(mrt.roomNights) as roomNights
	,SUM(mrt.reservationRevenueUSD)/SUM(mrt.roomNights) as avgNightlyRateUSD
	,SUM(mrt.nights)/COUNT(mrt.confirmationNumber) as avgLengthOfStay

	FROM superset.dbo.mostRecentTransactionsReporting as MRT
	LEFT OUTER JOIN @costs as costs ON mrt.billingDescription = costs.billingDescription AND mrt.channel = costs.channel AND mrt.secondarySource = costs.secondarySource AND YEAR(mrt.arrivalDate) = costs.arrivalYear AND MONTH(mrt.arrivalDate) = costs.arrivalMonth
	LEFT OUTER JOIN @hotels as hotels ON mrt.hotelID = hotels.synxisID
	LEFT OUTER JOIN @charges as charges on mrt.confirmationNumber = charges.confirmationNumber
	LEFT OUTER JOIN @usdExchange as usdExchange ON charges.invoiceDate = usdExchange.invDate AND charges.hotelCurrencyCode = usdExchange.currCode
	LEFT OUTER JOIN CurrencyRates.dbo.dailyRates on mrt.confirmationDate = dailyRates.rateDate AND charges.hotelCurrencyCode = dailyRates.code

	WHERE 
	mrt.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd AND
	mrt.channel <> 'PMS Rez Synch' AND
	mrt.CROcode <> 'HTL_Carlton'

	GROUP BY
	YEAR(mrt.arrivalDate),
	MONTH(mrt.arrivalDate),
	mrt.hotelID,
	mrt.Channel,
	mrt.SecondarySource,
	subSourceReportLabel,
	mrt.billingDescription,
	invoiceDate,
	hotelCurrencyCode    
    
    
	END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReport_HotelCode_Extended]'
GO



--EXEC [Reporting].[ProfitabilityReport] @arrivalStart = '3/1/2016', @arrivalEnd = '3/31/2016'

/*
-- Author:		Tory Greco
-- Create date: 02/10/2014
-- Description:	underlying query for all profitability reports
	new methodology:	get all linked server data into local tables
						get all needed MRT records into 1 master table
						put more case logic into sums for group by
						
						changed the underlying views - mrtreporting, 

	-- hiren wants to add this	
	-- @ratecodes varchar(max) -- contains comma separated values - list of rate codes


add column to rpt main - synxiscount

exec [Reporting].[ProfitabilityReport] 
	@arrivalStart = '2016-03-01', 
	@arrivalEnd = '2016-03-31',
	@Extended = 1

-- for true timing test, clear the cache and rerun
DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS


*/

CREATE PROCEDURE [Reporting].[ProfitabilityReport_HotelCode_Extended] 
	-- Add the parameters for the stored procedure here
	@arrivalStart DATE, 
	@arrivalEnd DATE,
	@hotelcode VARCHAR(10),
	@Extended BIT
AS
BEGIN
	SET NOCOUNT ON; -- prevents extra result sets from interfering with SELECT statements
	

--get the hotel information
SELECT	H.synxisID,
		H.openHospitalityCode,
		H.code as hotelCode,
		H.hotelName as hotelName,
		H.mainBrandCode as primaryBrand,
		LTRIM(RTRIM(RM00101.SALSTERR)) as gpSalesTerritory,
		H.phg_areamanageridName as AMD,
		H.phg_regionalmanageridName as RD,
		H.phg_revenueaccountmanageridName as RAM,
		H.phg_AccountManagerName as AccountManager,
		case	when CRM.accountclassificationcode = 200000 then 'City Hotel'
				when CRM.accountclassificationcode = 200001 then 'Resort Hotel'
				else NULL
		end as CRMClassification,   
		H.geographicRegionName  as geographicRegion,
		H.shortName  as country,
		H.physicalCity as city,
		H.subAreaName as state,
		H.totalRooms hotelRooms,
		H.statuscodename hotelStatus,
		LTRIM(RTRIM(RM00101.CURNCYID)) as hotelCurrency,
		B.gpSiteID
into	#Hotels-- select * 
FROM	Core.dbo.hotelsReporting H
LEFT OUTER JOIN	IC.dbo.RM00101 
	ON H.code = RM00101.CUSTNMBR
LEFT OUTER JOIN LocalCRM.dbo.account CRM
	on	H.crmGuid = CRM.accountid
LEFT OUTER JOIN	Core.dbo.Brands B
	on	H.mainBrandCode = B.code

WHERE (	synxisID > 0 OR openHospitalityCode > 0 )
and	  ( @hotelcode is null OR H.code = @hotelcode )

create clustered index ix_Hotels ON #Hotels (synxisID, openHospitalityCode)



-- get iata account manager from CRM, store in separate table
select	accountnumber, min(phg_iataglobalaccountmanageridName) iatamanager
into	#rpt_IATAManager 
from	LocalCRM.dbo.account
where	phg_iataglobalaccountmanageridName is not null
and		accountnumber is not null
group by accountnumber

create clustered index ix_iatamanager ON #rpt_IATAManager (accountnumber)


--	build master MRT table - get all the data ONCE (not multiple reads of MRT)
--  cost, sums, counts, channels, sources etc, 
--  a field that indicates whether the record is billable 
select	mrt.crsSourceID,
		CASE mrt.crsSourceID WHEN 2 THEN mrt.openhospitalityID ELSE mrt.hotelID END as crsHotelID,
		mrt.arrivalDate ,
		mrt.confirmationDate ,
		mrt.confirmationNumber,
		1 as SynXisCount,
		mrt.status ,
		mrt.channel ,
		mrt.CROcode ,
		mrt.billingDescription ,
		mrt.SecondarySource,
		mrt.subSourceReportLabel,
		CASE	mrt.billingDescription WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost	END
		  + 
		CASE mrt.channel WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 END as cost ,
		IM.iatamanager as IataGam ,
		Core.dbo.GetIATAGroups(mrt.IATANumber, mrt.confirmationdate ) IATAGroups,
		mrt.IATANumber , 
		mrt.travelAgencyName , 
		mrt.travelAgencyCity , 
		mrt.travelAgencyState , 
		mrt.travelAgencyCountry , 
		mrt.rateCategoryCode , 
		mrt.rateTypeCode ,	
		mrt.rateTypeName as ratename ,
		case	when LEFT(mrt.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(mrt.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(mrt.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(mrt.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(mrt.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(mrt.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(mrt.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(mrt.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(mrt.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(mrt.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(mrt.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(mrt.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(mrt.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(mrt.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(mrt.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(mrt.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end as rateCategory_PHG  ,
		mrt.rooms ,
		mrt.nights ,
		mrt.rooms *	mrt.nights as RoomNights,
		mrt.currency ,
		mrt.reservationRevenue ,
		mrt.reservationRevenueUSD 	,
		0 as IPreferCount ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCharges ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCost 
into	#rpt_Main
from	superset.dbo.mostrecenttransactionsreporting mrt with (nolock)
LEFT OUTER JOIN Superset.dbo.gdsBookingCost 
	ON	mrt.secondarySource = gdsBookingCost.gdsType 
	AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.synxisBookingCost 
	ON	mrt.billingDescription = synxisBookingCost.billingDescription 
	AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.pegsBookingCost 
	ON	mrt.channel = pegsBookingCost.channel 
	AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')

left outer join #rpt_IATAManager IM with (nolock)
	on	mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

where	MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
and	  ( @hotelcode is null OR mrt.hotelCode = @hotelcode )
AND		MRT.channel <> 'PMS Rez Synch' 
AND		MRT.CROcode <> 'HTL_Carlton'

-- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
Union All
Select	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		
create clustered index ix_rptMain_arrivaldate_synxisID on #rpt_Main (arrivaldate, crsHotelID)
create index ix_rptMain_ConfirmNum on #rpt_Main (confirmationnumber)

delete from #rpt_Main
where crsHotelID is null


-- get all billy charges from CalculatedDebits for the date parameters
SELECT	cd.confirmationNumber ,
		cd.hotelCode ,
		cd.gpSiteID ,
		cd.arrivalDate ,
		/* CODE CHANGE: TMG 2015-09-15
				new list of criteriaIDs that are surcharges - per hiren
		
		--1=Booking Charges
		SUM(CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END)	AS bookingCharges , 		
		--2=Commissions
		SUM(CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END)
		AS commissions , 
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
			ELSE 0 END) AS surcharges 
		*/
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , 
			cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
			CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END END END ) AS bookingCharges , 		
		
		--2=Commissions
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 2) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , 
			cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
				CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END END END )
			AS commissions , 
		
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID in (1, 2) ) then 0 ELSE
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 3) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , 
			cd.arrivalDate ) ELSE
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
				CASE classificationID WHEN 3 THEN 
				CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				ELSE 0 END END END END ) AS surcharges 
		
into	#rpt_BillyCharges	
from	superset.dbo.calculatedDebits CD with (nolock)
join	BillingProduction.dbo.criteria C with (nolock)
	on	cd.criteriaID = C.id 
left outer join BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC
	on	Cd.criteriaID = ESC.CriteriaId
WHERE	cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
AND		( @hotelcode is null OR CD.hotelCode = @hotelcode )
--AND		cd.chargeValueInHotelCurrency <> 0  -- but without this it greatly increases the billybookingcount
			-- this is needed for the new bsi functionality, credit memos can now uncharge for cancelled reserveatins
GROUP BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID
ORDER BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID

create	clustered index ix_RptCharges ON #rpt_BillyCharges (confirmationnumber)



/*	2015-03-09 TMG

	this fix will keep billy count the same for each month, even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count, because the arrival month 

*/
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate , confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue , reservationRevenueUSD, IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (billy.HotelCode) , -- synxisid
		billy.arrivalDate, 
		missingmrt.confirmationDate,
		billy.confirmationNumber , -- confirmationnumber -- do not inflate synxis booking count because the billy charge has a different month
		0, -- do not inflate synxis booking count because the billy charge has a different month
		missingMRT.billingDescription, -- billing description
		missingMRT.channel, -- as channel ,
		missingMRT.Status , -- as status
		missingMRT.SecondarySource , -- as SecondarySource ,
		missingMRT.subSourceReportLabel , -- as subSourceReportLabel ,
		missingMRT.CROcode , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		IM.iatamanager , --as IataGam ,
		Core.dbo.GetIATAGroups(missingMRT.IATANumber, missingMRT.confirmationdate ) , --IATAGroups ,
		missingMRT.IATANumber, -- as IATANumber,
		missingMRT.travelAgencyName, -- as travelAgencyName,
		missingMRT.travelAgencyCity, -- as travelAgencyCity,
		missingMRT.travelAgencyState, -- as travelAgencyState,
		missingMRT.travelAgencyCountry, --as travelAgencyCountry,
		missingMRT.rateCategoryCode, --as rateCategoryCode,
		missingMRT.rateTypeCode, -- as rateTypeCode,
		missingMRT.rateTypeName , --as ratename ,
		case	when LEFT(missingMRT.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(missingMRT.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(missingMRT.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(missingMRT.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(missingMRT.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(missingMRT.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(missingMRT.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(missingMRT.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(missingMRT.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(missingMRT.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(missingMRT.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(missingMRT.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(missingMRT.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end , -- as rateCategory_PHG,
		null, null, null, null, null, null,  -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD,
		0 ,  -- IPreferCount
		CAST(0.00 AS  DECIMAL(18,2)) , -- IPreferCharges
		CAST(0.00 AS  DECIMAL(18,2))   -- as IPreferCost
from	#rpt_BillyCharges billy
join	superset.dbo.mostRecentTransactionsReporting missingMRT
	on	billy.confirmationNumber = missingMRT.confirmationNumber

left outer join #rpt_IATAManager IM with (nolock)
	on	missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

-- get only confirmation numbers that have not been added yet
left outer join #rpt_Main mrt
	on	billy.confirmationnumber = mrt.confirmationnumber
where	mrt.confirmationNumber is null
AND ( @hotelcode is null OR missingMRT.hotelcode = @hotelcode )

GROUP BY billy.HotelCode, 
		billy.arrivalDate, 
		missingMRT.confirmationDate ,
		billy.confirmationNumber,
		missingMRT.billingDescription ,
		missingMRT.channel, 
		missingMRT.Status,
		missingMRT.SecondarySource, 
		missingMRT.subSourceReportLabel, 
		missingMRT.CROcode, 
		IM.iatamanager, 
		--missingMRT.IATAGroups, 
		missingMRT.IATANumber, missingMRT.confirmationdate, 
		missingMRT.travelAgencyName, 
		missingMRT.travelAgencyCity, 
		missingMRT.travelAgencyState, 
		missingMRT.travelAgencyCountry, 
		missingMRT.rateCategoryCode, 
		missingMRT.rateTypeCode, 
		missingMRT.rateTypeName



-- get IPrefer Counts and Sums by current billing date, using the following logic:
--		if matching a current MRT record - use the MRT data for channel, source, etc
--		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
--		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate, confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue, reservationRevenueUSD , IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (IP.Hotel_Code) , -- synxisid
		IP.Billing_Date, -- the IPrefer still need to be categorized as Month(MRT.ArrivalDate)
		IP.Booking_Date , -- for currency conversions
		null, -- confirmationnumber -- do not inflate synxis booking count because of an IPrefer match
		0 , -- do not inflate synxis booking count because the billy charge has a different month
		COALESCE(MRT.billingDescription, 'Non-Matching IPrefer Reservation') , -- billing description
		COALESCE(MRT.channel, 
				case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
					else	'IPrefer Prior Period'
				end ) , -- as channel ,
		COALESCE(MRT.Status, '') , -- as status
		COALESCE(MRT.SecondarySource, '') , -- as SecondarySource ,
		COALESCE(MRT.subSourceReportLabel, '') , -- as subSourceReportLabel ,
		COALESCE(MRT.CROcode, '') , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		COALESCE(MRT.IataGam, '') , -- as IataGam ,
		COALESCE(MRT.IATAGroups, '') , -- as IATAGroups ,
		COALESCE(MRT.IATANumber, '') , -- as IATANumber,
		COALESCE(MRT.travelAgencyName, '') , -- as travelAgencyName,
		COALESCE(MRT.travelAgencyCity, '') , -- as travelAgencyCity,
		COALESCE(MRT.travelAgencyState, '') , -- as travelAgencyState,
		COALESCE(MRT.travelAgencyCountry, '') , --as travelAgencyCountry,
		COALESCE(MRT.rateCategoryCode, '') , --as rateCategoryCode,
		COALESCE(MRT.rateTypeCode, '') , -- as rateTypeCode,
		COALESCE(MRT.ratename, '') , -- as ratename,
		COALESCE(MRT.rateCategory_PHG, '') , -- as rateCategory_PHG,
		null, null, null, null, null, null, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD
		COUNT(distinct IP.Booking_ID),  -- IPreferCount
		Sum(dbo.convertCurrencyToUSD( IP.Billable_Amount_Hotel_Currency ,-- IPreferCharges
			IP.Hotel_Currency, IP.Arrival_Date ) )	,
		SUM(IP.Reservation_Amount_USD * 0.02) -- as IPreferCost
from	BSI.IPrefer_Charges IP
left outer join #rpt_Main mrt
	on	IP.booking_id = mrt.confirmationnumber
where	IP.billing_date between @arrivalStart and @arrivalEnd
AND		( @hotelcode is null OR IP.Hotel_Code = @hotelcode )
GROUP BY IP.Hotel_Code, IP.Billing_Date , IP.Booking_Date ,
		MRT.arrivalDate, MRT.billingDescription , MRT.channel, 
		MRT.Status,
		case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
				else	'IPrefer Prior Period'
			end  , 
		MRT.SecondarySource, 
		MRT.subSourceReportLabel, 
		MRT.CROcode, 
		MRT.cost, 
		MRT.IataGam, 
		MRT.IATAGroups, 
		MRT.IATANumber, 
		MRT.travelAgencyName, 
		MRT.travelAgencyCity, 
		MRT.travelAgencyState, 
		MRT.travelAgencyCountry, 
		MRT.rateCategoryCode, 
		MRT.rateTypeCode, 
		MRT.ratename,
		MRT.rateCategory_PHG


if 	@Extended = 0 

		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.billingDescription ,
				main.channel , 
				main.secondarySource , 
				main.subSourceReportLabel , 
				main.CROcode ,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
					
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,
								
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,

				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end)
				 ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) 
				CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end)
				 as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency
				
		INTO	#Results
		
		FROM	#Hotels  as hotels 
		LEFT OUTER JOIN	#rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.Channel ,	
				main.billingDescription, 
				main.SecondarySource, 
				main.subSourceReportLabel, 
				main.CROcode --,billy.gpSiteID -- removed 2015-01-06 TMG -- put MIN(billy.gpsiteid) in above select
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelcurrency, 
				main.billingDescription ,
				main.channel ,
				main.secondarySource ,
				main.subSourceReportLabel ,
				main.CROcode ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.SynXisCount ELSE NULL END ) AS ConfirmedSynxisBookingCount ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.SynXisCount ELSE NULL END ) AS CancelledSynxisBookingCount , 
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.reservationrevenueUSD ELSE 0 END) ConfirmedRevenueUSD ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.reservationrevenueUSD ELSE 0 END) CancelledRevenueUSD ,
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.roomNights ELSE 0 END) AS ConfirmedRoomNights ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.roomNights ELSE 0 END) AS CancelledRoomNights ,
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.cost ELSE 0 END) AS ConfirmedTotalCost , 
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.cost ELSE 0 END) AS CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) AS avgNightlyRateUSD ,
				SUM(main.nights) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) AS BillyBookingCount, 
				COUNT(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.confirmationnumber ELSE NULL END) AS PHGSiteBillyBookingCount ,
				COUNT(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.confirmationnumber ELSE NULL END) AS HHASiteBillyBookingCount ,
				SUM(billy.bookingCharges) AS BillyBookingCharges,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.bookingCharges ELSE 0 END) AS PHGSiteBillyBookingCharges ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.bookingCharges ELSE 0 END) AS HHASiteBillyBookingCharges ,
				SUM(billy.commissions) AS commissions ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.commissions ELSE 0 END) AS PHGSiteCommissions ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.commissions ELSE 0 END) AS HHASiteCommissions ,
				SUM(billy.surcharges) AS surcharges ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.surcharges ELSE 0 END) AS PHGSiteSurcharges ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.surcharges ELSE 0 END) AS HHASiteSurcharges,			
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.bookingCharges + billy.commissions ELSE 0 END) AS PHGSiteBookAndComm ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.bookingCharges + billy.commissions ELSE 0 END) AS HHASiteBookAndComm,
				SUM(main.cost) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS costperbooking ,
				SUM(main.IPreferCount) AS IPreferBookingCount,
				SUM(main.IPrefercharges) AS IPreferCharges,
				SUM(main.IPreferCost) AS IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END AS BillyBookingWithZeroBookAndComm ,
				
				-- adding all sum fields in hotel billing currency from USD
				SUM(CASE WHEN main.status <> 'Cancelled' THEN Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) ELSE 0 END) 
				ConfirmedRevenueHotelCurrency ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) ELSE 0 END) 
				CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS BillyBookingChargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS CommissionsHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteCommissionsHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS SurchargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteSurchargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS BookAndCommHotelCurrency,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteBookAndCommHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) AS IPreferChargesHotelCurrency
		
		INTO	#Results_Extended

		FROM	#Hotels 	 AS hotels 
		LEFT OUTER JOIN #rpt_Main AS main
			ON	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges AS billy
			ON	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
		
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.crsHotelID ,
				main.Channel ,
				main.billingDescription,
				main.SecondarySource,
				main.subSourceReportLabel,
				main.CROcode ,
				hotels.hotelCurrency ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG --, billy.gpSiteID -- TMG removed and put MIN(billy.gpSiteID) in select -- 2015-01-06
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


IF @extended = 0
		SELECT	*
		FROM	#Results
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	*
		FROM	#Results_Extended
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel




IF	OBJECT_ID ('#usdExchange ') IS NOT NULL
	DROP TABLE #usdExchange 

IF	OBJECT_ID ('#Hotels') IS NOT NULL
	DROP TABLE #Hotels

IF	OBJECT_ID ('#rpt_Main') IS NOT NULL
	DROP TABLE #rpt_Main

IF	OBJECT_ID ('#rpt_BillyCharges') IS NOT NULL
	DROP TABLE #rpt_BillyCharges

IF	OBJECT_ID ('#rpt_IPreferCharges') IS NOT NULL
	DROP TABLE #rpt_IPreferCharges

IF	OBJECT_ID ('#rpt_IATAManager') IS NOT NULL
	DROP TABLE #rpt_IATAManager

IF	OBJECT_ID ('#profitabilityreport_tmg') IS NOT NULL
	DROP TABLE #profitabilityreport_tmg

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #profitabilityreport_tmg_ext

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #Results

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #Results_Extended




END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReport_HotelCode]'
GO



--EXEC [Reporting].[ProfitabilityReport] @arrivalStart = '3/1/2016', @arrivalEnd = '3/31/2016'

/*
-- Author:		Tory Greco
-- Create date: 02/10/2014
-- Description:	underlying query for all profitability reports
	new methodology:	get all linked server data into local tables
						get all needed MRT records into 1 master table
						put more case logic into sums for group by
						
						changed the underlying views - mrtreporting, 

	-- hiren wants to add this	
	-- @ratecodes varchar(max) -- contains comma separated values - list of rate codes


add column to rpt main - synxiscount

exec [Reporting].[ProfitabilityReport] 
	@arrivalStart = '2016-03-01', 
	@arrivalEnd = '2016-03-31',
	@Extended = 1

-- for true timing test, clear the cache and rerun
DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS

*/

CREATE PROCEDURE [Reporting].[ProfitabilityReport_HotelCode] 
	-- Add the parameters for the stored procedure here
	@arrivalStart DATE, 
	@arrivalEnd DATE,
	@hotelcode VARCHAR(10),
	@Extended BIT = 0 
AS
BEGIN
	SET NOCOUNT ON; -- prevents extra result sets from interfering with SELECT statements
	

--get the hotel information
SELECT	H.synxisID,
		H.openHospitalityCode,
		H.code as hotelCode,
		H.hotelName as hotelName,
		H.mainBrandCode as primaryBrand,
		LTRIM(RTRIM(RM00101.SALSTERR)) as gpSalesTerritory,
		H.phg_areamanageridName as AMD,
		H.phg_regionalmanageridName as RD,
		H.phg_revenueaccountmanageridName as RAM,
		H.phg_AccountManagerName as AccountManager,
		case	when CRM.accountclassificationcode = 200000 then 'City Hotel'
				when CRM.accountclassificationcode = 200001 then 'Resort Hotel'
				else NULL
		end as CRMClassification,   
		H.geographicRegionName  as geographicRegion,
		H.shortName  as country,
		H.physicalCity as city,
		H.subAreaName as state,
		H.totalRooms hotelRooms,
		H.statuscodename hotelStatus,
		LTRIM(RTRIM(RM00101.CURNCYID)) as hotelCurrency,
		B.gpSiteID
into	#Hotels-- select * 
FROM	Core.dbo.hotelsReporting H
LEFT OUTER JOIN	IC.dbo.RM00101 
	ON H.code = RM00101.CUSTNMBR
LEFT OUTER JOIN LocalCRM.dbo.account CRM
	on	H.crmGuid = CRM.accountid
LEFT OUTER JOIN	Core.dbo.Brands B
	on	H.mainBrandCode = B.code

WHERE (	synxisID > 0 OR openHospitalityCode > 0 )
and	  ( @hotelcode is null OR H.code = @hotelcode )

create clustered index ix_Hotels ON #Hotels (synxisID, openHospitalityCode)



-- get iata account manager from CRM, store in separate table
select	accountnumber, min(phg_iataglobalaccountmanageridName) iatamanager
into	#rpt_IATAManager 
from	LocalCRM.dbo.account
where	phg_iataglobalaccountmanageridName is not null
and		accountnumber is not null
group by accountnumber

create clustered index ix_iatamanager ON #rpt_IATAManager (accountnumber)


--	build master MRT table - get all the data ONCE (not multiple reads of MRT)
--  cost, sums, counts, channels, sources etc, 
--  a field that indicates whether the record is billable 
select	mrt.crsSourceID,
		CASE mrt.crsSourceID WHEN 2 THEN mrt.openhospitalityID ELSE mrt.hotelID END as crsHotelID,
		mrt.arrivalDate ,
		mrt.confirmationDate ,
		mrt.confirmationNumber,
		1 as SynXisCount,
		mrt.status ,
		mrt.channel ,
		mrt.CROcode ,
		mrt.billingDescription ,
		mrt.SecondarySource,
		mrt.subSourceReportLabel,
		CASE	mrt.billingDescription WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost	END
		  + 
		CASE mrt.channel WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 END as cost ,
		IM.iatamanager as IataGam ,
		Core.dbo.GetIATAGroups(mrt.IATANumber, mrt.confirmationdate ) IATAGroups,
		mrt.IATANumber , 
		mrt.travelAgencyName , 
		mrt.travelAgencyCity , 
		mrt.travelAgencyState , 
		mrt.travelAgencyCountry , 
		mrt.rateCategoryCode , 
		mrt.rateTypeCode ,	
		mrt.rateTypeName as ratename ,
		case	when LEFT(mrt.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(mrt.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(mrt.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(mrt.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(mrt.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(mrt.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(mrt.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(mrt.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(mrt.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(mrt.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(mrt.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(mrt.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(mrt.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(mrt.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(mrt.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(mrt.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end as rateCategory_PHG  ,
		mrt.rooms ,
		mrt.nights ,
		mrt.rooms *	mrt.nights as RoomNights,
		mrt.currency ,
		mrt.reservationRevenue ,
		mrt.reservationRevenueUSD 	,
		0 as IPreferCount ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCharges ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCost 
into	#rpt_Main
from	superset.dbo.mostrecenttransactionsreporting mrt with (nolock)
LEFT OUTER JOIN Superset.dbo.gdsBookingCost 
	ON	mrt.secondarySource = gdsBookingCost.gdsType 
	AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.synxisBookingCost 
	ON	mrt.billingDescription = synxisBookingCost.billingDescription 
	AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.pegsBookingCost 
	ON	mrt.channel = pegsBookingCost.channel 
	AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')

left outer join #rpt_IATAManager IM with (nolock)
	on	mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

where	MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
and	  ( @hotelcode is null OR mrt.hotelCode = @hotelcode )
AND		MRT.channel <> 'PMS Rez Synch' 
AND		MRT.CROcode <> 'HTL_Carlton'

-- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
Union All
Select	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		
create clustered index ix_rptMain_arrivaldate_synxisID on #rpt_Main (arrivaldate, crsHotelID)
create index ix_rptMain_ConfirmNum on #rpt_Main (confirmationnumber)

delete from #rpt_Main
where crsHotelID is null


-- get all billy charges from CalculatedDebits for the date parameters
SELECT	cd.confirmationNumber ,
		cd.hotelCode ,
		cd.gpSiteID ,
		cd.arrivalDate ,
		/* CODE CHANGE: TMG 2015-09-15
				new list of criteriaIDs that are surcharges - per hiren
		
		--1=Booking Charges
		SUM(CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END)	AS bookingCharges , 		
		--2=Commissions
		SUM(CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END)
		AS commissions , 
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
			ELSE 0 END) AS surcharges 
		*/
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , 
			cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
			CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END END END ) AS bookingCharges , 		
		
		--2=Commissions
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 2) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , 
			cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
				CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END END END )
			AS commissions , 
		
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID in (1, 2) ) then 0 ELSE
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 3) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , 
			cd.arrivalDate ) ELSE
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
				CASE classificationID WHEN 3 THEN 
				CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				ELSE 0 END END END END ) AS surcharges 
		
into	#rpt_BillyCharges	
from	superset.dbo.calculatedDebits CD with (nolock)
join	BillingProduction.dbo.criteria C with (nolock)
	on	cd.criteriaID = C.id 
left outer join BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC
	on	Cd.criteriaID = ESC.CriteriaId
WHERE	cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
AND		( @hotelcode is null OR CD.hotelCode = @hotelcode )
--AND		cd.chargeValueInHotelCurrency <> 0  -- but without this it greatly increases the billybookingcount
			-- this is needed for the new bsi functionality, credit memos can now uncharge for cancelled reserveatins
GROUP BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID
ORDER BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID

create	clustered index ix_RptCharges ON #rpt_BillyCharges (confirmationnumber)



/*	2015-03-09 TMG

	this fix will keep billy count the same for each month, even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count, because the arrival month 

*/
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate , confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue , reservationRevenueUSD, IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (billy.HotelCode) , -- synxisid
		billy.arrivalDate, 
		missingmrt.confirmationDate,
		billy.confirmationNumber , -- confirmationnumber -- do not inflate synxis booking count because the billy charge has a different month
		0, -- do not inflate synxis booking count because the billy charge has a different month
		missingMRT.billingDescription, -- billing description
		missingMRT.channel, -- as channel ,
		missingMRT.Status , -- as status
		missingMRT.SecondarySource , -- as SecondarySource ,
		missingMRT.subSourceReportLabel , -- as subSourceReportLabel ,
		missingMRT.CROcode , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		IM.iatamanager , --as IataGam ,
		Core.dbo.GetIATAGroups(missingMRT.IATANumber, missingMRT.confirmationdate ) , --IATAGroups ,
		missingMRT.IATANumber, -- as IATANumber,
		missingMRT.travelAgencyName, -- as travelAgencyName,
		missingMRT.travelAgencyCity, -- as travelAgencyCity,
		missingMRT.travelAgencyState, -- as travelAgencyState,
		missingMRT.travelAgencyCountry, --as travelAgencyCountry,
		missingMRT.rateCategoryCode, --as rateCategoryCode,
		missingMRT.rateTypeCode, -- as rateTypeCode,
		missingMRT.rateTypeName , --as ratename ,
		case	when LEFT(missingMRT.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(missingMRT.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(missingMRT.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(missingMRT.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(missingMRT.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(missingMRT.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(missingMRT.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(missingMRT.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(missingMRT.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(missingMRT.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(missingMRT.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(missingMRT.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(missingMRT.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end , -- as rateCategory_PHG,
		null, null, null, null, null, null,  -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD,
		0 ,  -- IPreferCount
		CAST(0.00 AS  DECIMAL(18,2)) , -- IPreferCharges
		CAST(0.00 AS  DECIMAL(18,2))   -- as IPreferCost
from	#rpt_BillyCharges billy
join	superset.dbo.mostRecentTransactionsReporting missingMRT
	on	billy.confirmationNumber = missingMRT.confirmationNumber

left outer join #rpt_IATAManager IM with (nolock)
	on	missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

-- get only confirmation numbers that have not been added yet
left outer join #rpt_Main mrt
	on	billy.confirmationnumber = mrt.confirmationnumber
where	mrt.confirmationNumber is null
AND ( @hotelcode is null OR missingMRT.hotelcode = @hotelcode )

GROUP BY billy.HotelCode, 
		billy.arrivalDate, 
		missingMRT.confirmationDate ,
		billy.confirmationNumber,
		missingMRT.billingDescription ,
		missingMRT.channel, 
		missingMRT.Status,
		missingMRT.SecondarySource, 
		missingMRT.subSourceReportLabel, 
		missingMRT.CROcode, 
		IM.iatamanager, 
		--missingMRT.IATAGroups, 
		missingMRT.IATANumber, missingMRT.confirmationdate, 
		missingMRT.travelAgencyName, 
		missingMRT.travelAgencyCity, 
		missingMRT.travelAgencyState, 
		missingMRT.travelAgencyCountry, 
		missingMRT.rateCategoryCode, 
		missingMRT.rateTypeCode, 
		missingMRT.rateTypeName



-- get IPrefer Counts and Sums by current billing date, using the following logic:
--		if matching a current MRT record - use the MRT data for channel, source, etc
--		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
--		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate, confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue, reservationRevenueUSD , IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (IP.Hotel_Code) , -- synxisid
		IP.Billing_Date, -- the IPrefer still need to be categorized as Month(MRT.ArrivalDate)
		IP.Booking_Date , -- for currency conversions
		null, -- confirmationnumber -- do not inflate synxis booking count because of an IPrefer match
		0 , -- do not inflate synxis booking count because the billy charge has a different month
		COALESCE(MRT.billingDescription, 'Non-Matching IPrefer Reservation') , -- billing description
		COALESCE(MRT.channel, 
				case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
					else	'IPrefer Prior Period'
				end ) , -- as channel ,
		COALESCE(MRT.Status, '') , -- as status
		COALESCE(MRT.SecondarySource, '') , -- as SecondarySource ,
		COALESCE(MRT.subSourceReportLabel, '') , -- as subSourceReportLabel ,
		COALESCE(MRT.CROcode, '') , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		COALESCE(MRT.IataGam, '') , -- as IataGam ,
		COALESCE(MRT.IATAGroups, '') , -- as IATAGroups ,
		COALESCE(MRT.IATANumber, '') , -- as IATANumber,
		COALESCE(MRT.travelAgencyName, '') , -- as travelAgencyName,
		COALESCE(MRT.travelAgencyCity, '') , -- as travelAgencyCity,
		COALESCE(MRT.travelAgencyState, '') , -- as travelAgencyState,
		COALESCE(MRT.travelAgencyCountry, '') , --as travelAgencyCountry,
		COALESCE(MRT.rateCategoryCode, '') , --as rateCategoryCode,
		COALESCE(MRT.rateTypeCode, '') , -- as rateTypeCode,
		COALESCE(MRT.ratename, '') , -- as ratename,
		COALESCE(MRT.rateCategory_PHG, '') , -- as rateCategory_PHG,
		null, null, null, null, null, null, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD
		COUNT(distinct IP.Booking_ID),  -- IPreferCount
		Sum(dbo.convertCurrencyToUSD( IP.Billable_Amount_Hotel_Currency ,-- IPreferCharges
			IP.Hotel_Currency, IP.Arrival_Date ) )	,
		SUM(IP.Reservation_Amount_USD * 0.02) -- as IPreferCost
from	BSI.IPrefer_Charges IP
left outer join #rpt_Main mrt
	on	IP.booking_id = mrt.confirmationnumber
where	IP.billing_date between @arrivalStart and @arrivalEnd
AND		( @hotelcode is null OR IP.Hotel_Code = @hotelcode )
GROUP BY IP.Hotel_Code, IP.Billing_Date , IP.Booking_Date ,
		MRT.arrivalDate, MRT.billingDescription , MRT.channel, 
		MRT.Status,
		case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
				else	'IPrefer Prior Period'
			end  , 
		MRT.SecondarySource, 
		MRT.subSourceReportLabel, 
		MRT.CROcode, 
		MRT.cost, 
		MRT.IataGam, 
		MRT.IATAGroups, 
		MRT.IATANumber, 
		MRT.travelAgencyName, 
		MRT.travelAgencyCity, 
		MRT.travelAgencyState, 
		MRT.travelAgencyCountry, 
		MRT.rateCategoryCode, 
		MRT.rateTypeCode, 
		MRT.ratename,
		MRT.rateCategory_PHG


if 	@Extended = 0 

		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.billingDescription ,
				main.channel , 
				main.secondarySource , 
				main.subSourceReportLabel , 
				main.CROcode ,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
					
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,
								
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,

				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end)
				 ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) 
				CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end)
				 as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) 
				AS HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency
				
		INTO	#Results
		
		FROM	#Hotels  as hotels 
		LEFT OUTER JOIN	#rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.Channel ,	
				main.billingDescription, 
				main.SecondarySource, 
				main.subSourceReportLabel, 
				main.CROcode --,billy.gpSiteID -- removed 2015-01-06 TMG -- put MIN(billy.gpsiteid) in above select
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelcurrency, 
				main.billingDescription ,
				main.channel ,
				main.secondarySource ,
				main.subSourceReportLabel ,
				main.CROcode ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.SynXisCount ELSE NULL END ) AS ConfirmedSynxisBookingCount ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.SynXisCount ELSE NULL END ) AS CancelledSynxisBookingCount , 
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.reservationrevenueUSD ELSE 0 END) ConfirmedRevenueUSD ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.reservationrevenueUSD ELSE 0 END) CancelledRevenueUSD ,
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.roomNights ELSE 0 END) AS ConfirmedRoomNights ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.roomNights ELSE 0 END) AS CancelledRoomNights ,
				SUM(CASE WHEN main.status <> 'Cancelled' THEN main.cost ELSE 0 END) AS ConfirmedTotalCost , 
				SUM(CASE WHEN main.status = 'Cancelled' THEN main.cost ELSE 0 END) AS CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) AS avgNightlyRateUSD ,
				SUM(main.nights) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) AS BillyBookingCount, 
				COUNT(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.confirmationnumber ELSE NULL END) AS PHGSiteBillyBookingCount ,
				COUNT(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.confirmationnumber ELSE NULL END) AS HHASiteBillyBookingCount ,
				SUM(billy.bookingCharges) AS BillyBookingCharges,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.bookingCharges ELSE 0 END) AS PHGSiteBillyBookingCharges ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.bookingCharges ELSE 0 END) AS HHASiteBillyBookingCharges ,
				SUM(billy.commissions) AS commissions ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.commissions ELSE 0 END) AS PHGSiteCommissions ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.commissions ELSE 0 END) AS HHASiteCommissions ,
				SUM(billy.surcharges) AS surcharges ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.surcharges ELSE 0 END) AS PHGSiteSurcharges ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.surcharges ELSE 0 END) AS HHASiteSurcharges,			
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN billy.bookingCharges + billy.commissions ELSE 0 END) AS PHGSiteBookAndComm ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN billy.bookingCharges + billy.commissions ELSE 0 END) AS HHASiteBookAndComm,
				SUM(main.cost) / CASE WHEN COUNT(main.confirmationnumber) = 0 THEN 1 ELSE COUNT(main.confirmationnumber) END AS costperbooking ,
				SUM(main.IPreferCount) AS IPreferBookingCount,
				SUM(main.IPrefercharges) AS IPreferCharges,
				SUM(main.IPreferCost) AS IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END AS BillyBookingWithZeroBookAndComm ,
				
				-- adding all sum fields in hotel billing currency from USD
				SUM(CASE WHEN main.status <> 'Cancelled' THEN Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) ELSE 0 END) 
				ConfirmedRevenueHotelCurrency ,
				SUM(CASE WHEN main.status = 'Cancelled' THEN Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) ELSE 0 END) 
				CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS BillyBookingChargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS CommissionsHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteCommissionsHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS SurchargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteSurchargesHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) AS BookAndCommHotelCurrency,
				SUM(CASE WHEN  billy.gpSiteID IN ( 1 ) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS PHGSiteBookAndCommHotelCurrency ,
				SUM(CASE WHEN  billy.gpSiteID IN (6, 8) THEN Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ELSE 0 END) 
				AS HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) AS IPreferChargesHotelCurrency
		
		INTO	#Results_Extended

		FROM	#Hotels 	 AS hotels 
		LEFT OUTER JOIN #rpt_Main AS main
			ON	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges AS billy
			ON	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
		
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.crsHotelID ,
				main.Channel ,
				main.billingDescription,
				main.SecondarySource,
				main.subSourceReportLabel,
				main.CROcode ,
				hotels.hotelCurrency ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG --, billy.gpSiteID -- TMG removed and put MIN(billy.gpSiteID) in select -- 2015-01-06
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


IF @extended = 0
		SELECT	*
		FROM	#Results
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	*
		FROM	#Results_Extended
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel




IF	OBJECT_ID ('#usdExchange ') IS NOT NULL
	DROP TABLE #usdExchange 

IF	OBJECT_ID ('#Hotels') IS NOT NULL
	DROP TABLE #Hotels

IF	OBJECT_ID ('#rpt_Main') IS NOT NULL
	DROP TABLE #rpt_Main

IF	OBJECT_ID ('#rpt_BillyCharges') IS NOT NULL
	DROP TABLE #rpt_BillyCharges

IF	OBJECT_ID ('#rpt_IPreferCharges') IS NOT NULL
	DROP TABLE #rpt_IPreferCharges

IF	OBJECT_ID ('#rpt_IATAManager') IS NOT NULL
	DROP TABLE #rpt_IATAManager

IF	OBJECT_ID ('#profitabilityreport_tmg') IS NOT NULL
	DROP TABLE #profitabilityreport_tmg

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #profitabilityreport_tmg_ext

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #Results

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #Results_Extended




END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReport_ConfirmationNumber]'
GO





CREATE PROCEDURE [Reporting].[ProfitabilityReport_ConfirmationNumber] 
	-- Add the parameters for the stored procedure here
	@arrivalStart DATE, 
	@arrivalEnd DATE,
	@hotelcode VARCHAR(10) = NULL,
	@Extended BIT = 0
AS
BEGIN
	SET NOCOUNT ON; -- prevents extra result sets from interfering with SELECT statements
	

--get the hotel information
SELECT	H.synxisID,
		H.openHospitalityCode,
		H.code as hotelCode,
		H.hotelName as hotelName,
		H.mainBrandCode as primaryBrand,
		LTRIM(RTRIM(RM00101.SALSTERR)) as gpSalesTerritory,
		H.phg_areamanageridName as AMD,
		H.phg_regionalmanageridName as RD,
		H.phg_revenueaccountmanageridName as RAM,
		H.phg_AccountManagerName as AccountManager,
		case	when CRM.accountclassificationcode = 200000 then 'City Hotel'
				when CRM.accountclassificationcode = 200001 then 'Resort Hotel'
				else NULL
		end as CRMClassification,   
		H.geographicRegionName  as geographicRegion,
		H.shortName  as country,
		H.physicalCity as city,
		H.subAreaName as state,
		H.totalRooms hotelRooms,
		H.statuscodename hotelStatus,
		LTRIM(RTRIM(RM00101.CURNCYID)) as hotelCurrency,
		B.gpSiteID
into	#Hotels-- select * 
FROM	Core.dbo.hotelsReporting H
LEFT OUTER JOIN	IC.dbo.RM00101 
	ON H.code = RM00101.CUSTNMBR
LEFT OUTER JOIN LocalCRM.dbo.account CRM
	on	H.crmGuid = CRM.accountid
LEFT OUTER JOIN	Core.dbo.Brands B
	on	H.mainBrandCode = B.code

WHERE (	synxisID > 0 OR openHospitalityCode > 0 )
and	  ( @hotelcode is null OR H.code = @hotelcode )

create clustered index ix_Hotels ON #Hotels (synxisID, openHospitalityCode)



-- get iata account manager from CRM, store in separate table
select	accountnumber, min(phg_iataglobalaccountmanageridName) iatamanager
into	#rpt_IATAManager 
from	LocalCRM.dbo.account
where	phg_iataglobalaccountmanageridName is not null
and		accountnumber is not null
group by accountnumber

create clustered index ix_iatamanager ON #rpt_IATAManager (accountnumber)


--	build master MRT table - get all the data ONCE (not multiple reads of MRT)
--  cost, sums, counts, channels, sources etc, 
--  a field that indicates whether the record is billable 


select	mrt.crsSourceID,
		CASE mrt.crsSourceID WHEN 2 THEN mrt.openhospitalityID ELSE mrt.hotelID END as crsHotelID,
		mrt.arrivalDate ,
		mrt.confirmationDate ,
		mrt.confirmationNumber,
		1 as SynXisCount,
		mrt.status ,
		mrt.channel ,
		mrt.CROcode ,
		mrt.billingDescription ,
		mrt.SecondarySource,
		mrt.subSourceReportLabel,
		CASE	mrt.billingDescription WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost	END
		  + 
		CASE mrt.channel WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 END as cost ,
		IM.iatamanager as IataGam ,
		Core.dbo.GetIATAGroups(mrt.IATANumber, mrt.confirmationdate ) IATAGroups,
		mrt.IATANumber , 
		mrt.travelAgencyName , 
		mrt.travelAgencyCity , 
		mrt.travelAgencyState , 
		mrt.travelAgencyCountry , 
		mrt.rateCategoryCode , 
		mrt.rateTypeCode ,	
		mrt.rateTypeName as ratename ,
		case	when LEFT(mrt.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(mrt.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(mrt.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(mrt.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(mrt.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(mrt.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(mrt.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(mrt.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(mrt.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(mrt.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(mrt.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(mrt.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(mrt.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(mrt.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(mrt.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(mrt.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end as rateCategory_PHG  ,
		mrt.rooms ,
		mrt.nights ,
		mrt.rooms *	mrt.nights as RoomNights,
		mrt.currency ,
		mrt.reservationRevenue ,
		mrt.reservationRevenueUSD 	,
		0 as IPreferCount ,
		0 as IPreferCharges ,
		0 as IPreferCost,
		CAST('' AS VARCHAR(100))  AS iPreferBookingID,
		CAST('' AS VARCHAR(100))  AS Billing_Concept
into	#rpt_Main
from	superset.dbo.mostrecenttransactionsreporting mrt with (nolock)
LEFT OUTER JOIN Superset.dbo.gdsBookingCost 
	ON	mrt.secondarySource = gdsBookingCost.gdsType 
	AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.synxisBookingCost 
	ON	mrt.billingDescription = synxisBookingCost.billingDescription 
	AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.pegsBookingCost 
	ON	mrt.channel = pegsBookingCost.channel 
	AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')

left outer join #rpt_IATAManager IM with (nolock)
	on	mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

where	MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
and	  ( @hotelcode is null OR mrt.hotelCode = @hotelcode )
AND		MRT.channel <> 'PMS Rez Synch' 
AND		MRT.CROcode <> 'HTL_Carlton'
--AND mrt.confirmationNumber IN (SELECT Booking_ID FROM BSI.IPrefer_Charges)

-- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
Union All
Select	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		
create clustered index ix_rptMain_arrivaldate_synxisID on #rpt_Main (arrivaldate, crsHotelID)
create index ix_rptMain_ConfirmNum on #rpt_Main (confirmationnumber)

delete from #rpt_Main
where crsHotelID is null


-- get all billy charges from CalculatedDebits for the date parameters
SELECT	cd.confirmationNumber ,
		cd.hotelCode ,
		cd.gpSiteID ,
		cd.arrivalDate ,
		/* CODE CHANGE: TMG 2015-09-15
				new list of criteriaIDs that are surcharges - per hiren
		
		--1=Booking Charges
		SUM(CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END)	AS bookingCharges , 		
		--2=Commissions
		SUM(CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END)
		AS commissions , 
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
			ELSE 0 END) AS surcharges 
		*/
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
			CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END END END ) AS bookingCharges , 		
		
		--2=Commissions
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 2) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
				CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END END END )
			AS commissions , 
		
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID in (1, 2) ) then 0 ELSE
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 3) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
				CASE classificationID WHEN 3 THEN 
				CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				ELSE 0 END END END END ) AS surcharges 
		
into	##rpt_BillyCharges	
from	superset.dbo.calculatedDebits CD with (nolock)
join	BillingProduction.dbo.criteria C with (nolock)
	on	cd.criteriaID = C.id 
left outer join BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC
	on	Cd.criteriaID = ESC.CriteriaId
WHERE	cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
AND		( @hotelcode is null OR CD.hotelCode = @hotelcode )
--AND		cd.chargeValueInHotelCurrency <> 0  -- but without this it greatly increases the billybookingcount
			-- this is needed for the new bsi functionality, credit memos can now uncharge for cancelled reserveatins
GROUP BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID
ORDER BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID

create	clustered index ix_RptCharges ON ##rpt_BillyCharges (confirmationnumber)



/*	2015-03-09 TMG

	this fix will keep billy count the same for each month, even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count, because the arrival month 

*/
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate , confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue , reservationRevenueUSD, IPreferCount, IPreferCharges, IPreferCost, iPreferBookingID, Billing_Concept  )
select	dbo.GetSynXisIDwithHotelCode (billy.HotelCode) , -- synxisid
		billy.arrivalDate, 
		missingmrt.confirmationDate,
		billy.confirmationNumber , -- confirmationnumber -- do not inflate synxis booking count because the billy charge has a different month
		0, -- do not inflate synxis booking count because the billy charge has a different month
		missingMRT.billingDescription, -- billing description
		missingMRT.channel, -- as channel ,
		missingMRT.Status , -- as status
		missingMRT.SecondarySource , -- as SecondarySource ,
		missingMRT.subSourceReportLabel , -- as subSourceReportLabel ,
		missingMRT.CROcode , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		IM.iatamanager , --as IataGam ,
		Core.dbo.GetIATAGroups(missingMRT.IATANumber, missingMRT.confirmationdate ) , --IATAGroups ,
		missingMRT.IATANumber, -- as IATANumber,
		missingMRT.travelAgencyName, -- as travelAgencyName,
		missingMRT.travelAgencyCity, -- as travelAgencyCity,
		missingMRT.travelAgencyState, -- as travelAgencyState,
		missingMRT.travelAgencyCountry, --as travelAgencyCountry,
		missingMRT.rateCategoryCode, --as rateCategoryCode,
		missingMRT.rateTypeCode, -- as rateTypeCode,
		missingMRT.rateTypeName , --as ratename ,
		case	when LEFT(missingMRT.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(missingMRT.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(missingMRT.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(missingMRT.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(missingMRT.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(missingMRT.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(missingMRT.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(missingMRT.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(missingMRT.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(missingMRT.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(missingMRT.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(missingMRT.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(missingMRT.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end , -- as rateCategory_PHG,
		null, null, null, null, null, null,  -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD,
		0 ,  -- IPreferCount
		0 , -- IPreferCharges
		0,   -- as IPreferCost
		CAST('' AS VARCHAR(100)),--   iPreferBookingID,
		CAST('' AS VARCHAR(100))  --AS Billing_Concept
from	##rpt_BillyCharges billy
join	superset.dbo.mostRecentTransactionsReporting missingMRT
	on	billy.confirmationNumber = missingMRT.confirmationNumber

left outer join #rpt_IATAManager IM with (nolock)
	on	missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

-- get only confirmation numbers that have not been added yet
left outer join #rpt_Main mrt
	on	billy.confirmationnumber = mrt.confirmationnumber
where	mrt.confirmationNumber is null
AND ( @hotelcode is null OR missingMRT.hotelcode = @hotelcode )

GROUP BY billy.HotelCode, 
		billy.arrivalDate, 
		missingMRT.confirmationDate ,
		billy.confirmationNumber,
		missingMRT.billingDescription ,
		missingMRT.channel, 
		missingMRT.Status,
		missingMRT.SecondarySource, 
		missingMRT.subSourceReportLabel, 
		missingMRT.CROcode, 
		IM.iatamanager, 
		--missingMRT.IATAGroups, 
		missingMRT.IATANumber, missingMRT.confirmationdate, 
		missingMRT.travelAgencyName, 
		missingMRT.travelAgencyCity, 
		missingMRT.travelAgencyState, 
		missingMRT.travelAgencyCountry, 
		missingMRT.rateCategoryCode, 
		missingMRT.rateTypeCode, 
		missingMRT.rateTypeName



---- get IPrefer Counts and Sums by current billing date, using the following logic:
----		if matching a current MRT record - use the MRT data for channel, source, etc
----		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
----		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate, confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue, reservationRevenueUSD , IPreferCount, IPreferCharges, IPreferCost, iPreferBookingID, Billing_Concept  )
select	dbo.GetSynXisIDwithHotelCode (IP.Hotel_Code) , -- synxisid
		IP.Billing_Date, -- the IPrefer still need to be categorized as Month(MRT.ArrivalDate)
		IP.Booking_Date , -- for currency conversions
		null, -- confirmationnumber -- do not inflate synxis booking count because of an IPrefer match
		0 , -- do not inflate synxis booking count because the billy charge has a different month
		COALESCE(MRT.billingDescription, 'Non-Matching IPrefer Reservation') , -- billing description
		COALESCE(MRT.channel, 
				case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
					else	'IPrefer Prior Period'
				end ) , -- as channel ,
		COALESCE(MRT.Status, '') , -- as status
		COALESCE(MRT.SecondarySource, '') , -- as SecondarySource ,
		COALESCE(MRT.subSourceReportLabel, '') , -- as subSourceReportLabel ,
		COALESCE(MRT.CROcode, '') , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		COALESCE(MRT.IataGam, '') , -- as IataGam ,
		COALESCE(MRT.IATAGroups, '') , -- as IATAGroups ,
		COALESCE(MRT.IATANumber, '') , -- as IATANumber,
		COALESCE(MRT.travelAgencyName, '') , -- as travelAgencyName,
		COALESCE(MRT.travelAgencyCity, '') , -- as travelAgencyCity,
		COALESCE(MRT.travelAgencyState, '') , -- as travelAgencyState,
		COALESCE(MRT.travelAgencyCountry, '') , --as travelAgencyCountry,
		COALESCE(MRT.rateCategoryCode, '') , --as rateCategoryCode,
		COALESCE(MRT.rateTypeCode, '') , -- as rateTypeCode,
		COALESCE(MRT.ratename, '') , -- as ratename,
		COALESCE(MRT.rateCategory_PHG, '') , -- as rateCategory_PHG,
		null, null, null, null, null, null, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD
		COUNT(distinct IP.Booking_ID),  -- IPreferCount
		Sum(dbo.convertCurrencyToUSD( IP.Billable_Amount_Hotel_Currency ,-- IPreferCharges
			IP.Hotel_Currency, IP.Arrival_Date ) )	,
		SUM(IP.Reservation_Amount_USD * 0.02), -- as IPreferCost
		IP.Booking_ID, --AS iPreferBookingID 
		IP.Billing_Concept
from	BSI.IPrefer_Charges IP
left outer join #rpt_Main mrt
	on	IP.booking_id = mrt.confirmationnumber
where	IP.billing_date between @arrivalStart and @arrivalEnd
AND		( @hotelcode is null OR IP.Hotel_Code = @hotelcode )
GROUP BY IP.Hotel_Code, IP.Billing_Date , IP.Booking_Date ,
		MRT.arrivalDate, MRT.billingDescription , MRT.channel, 
		MRT.Status,
		case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
				else	'IPrefer Prior Period'
			end  , 
		MRT.SecondarySource, 
		MRT.subSourceReportLabel, 
		MRT.CROcode, 
		MRT.cost, 
		MRT.IataGam, 
		MRT.IATAGroups, 
		MRT.IATANumber, 
		MRT.travelAgencyName, 
		MRT.travelAgencyCity, 
		MRT.travelAgencyState, 
		MRT.travelAgencyCountry, 
		MRT.rateCategoryCode, 
		MRT.rateTypeCode, 
		MRT.ratename,
		MRT.rateCategory_PHG,
		IP.Booking_ID,
		IP.Billing_Concept


if 	@Extended = 0 

		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.billingDescription ,
				main.channel , 
				main.secondarySource , 
				main.subSourceReportLabel , 
				main.CROcode ,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else 0 end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else 0 end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
					
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,
								
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,

				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency,
				main.iPreferBookingID,
				main.Billing_Concept,
				main.confirmationNumber
				
		INTO	##Results
		
		FROM	#Hotels  as hotels 
		LEFT OUTER JOIN	#rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN ##rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		--WHERE main.IPreferCount <> 0
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.Channel ,	
				main.billingDescription, 
				main.SecondarySource, 
				main.subSourceReportLabel, 
				main.CROcode, --,billy.gpSiteID -- removed 2015-01-06 TMG -- put MIN(billy.gpsiteid) in above select
				main.confirmationNumber,
				main.iPreferBookingID,
				main.Billing_Concept
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel



IF @extended = 0
		SELECT	*
		FROM	##Results
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel





IF	OBJECT_ID ('#usdExchange ') IS NOT NULL
	DROP TABLE #usdExchange 

IF	OBJECT_ID ('#Hotels') IS NOT NULL
	DROP TABLE #Hotels

IF	OBJECT_ID ('#rpt_Main') IS NOT NULL
	DROP TABLE #rpt_Main

IF	OBJECT_ID ('##rpt_BillyCharges') IS NOT NULL
	DROP TABLE ##rpt_BillyCharges

IF	OBJECT_ID ('#rpt_IPreferCharges') IS NOT NULL
	DROP TABLE #rpt_IPreferCharges

IF	OBJECT_ID ('#rpt_IATAManager') IS NOT NULL
	DROP TABLE #rpt_IATAManager

IF	OBJECT_ID ('#profitabilityreport_tmg') IS NOT NULL
	DROP TABLE #profitabilityreport_tmg

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #profitabilityreport_tmg_ext

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE ##Results

IF	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	DROP TABLE #Results_Extended




END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReport_RateCode]'
GO




/*
-- Author:		Tory Greco
-- Create date: 02/10/2014
-- Description:	underlying query for all profitability reports
	new methodology:	get all linked server data into local tables
						get all needed MRT records into 1 master table
						put more case logic into sums for group by
						
						changed the underlying views - mrtreporting, 

	-- hiren wants to add this	
	-- @ratecodes varchar(max) -- contains comma separated values - list of rate codes


add column to rpt main - synxiscount

exec [Reporting].[ProfitabilityReport] 
	@arrivalStart = '2015-02-01', 
	@arrivalEnd = '2015-02-28',
	@Extended = 0

-- for true timing test, clear the cache and rerun
DBCC FREEPROCCACHE
DBCC DROPCLEANBUFFERS



*/

CREATE procedure [Reporting].[ProfitabilityReport_RateCode] 
	-- Add the parameters for the stored procedure here
	@arrivalStart date, 
	@arrivalEnd date,
	@hotelcode varchar(10) = NULL,
	@Extended bit = 1,
	@RateCode varchar(20) = NULL
AS
BEGIN
	SET NOCOUNT ON; -- prevents extra result sets from interfering with SELECT statements
	

--get the hotel information
SELECT	H.synxisID,
		H.openHospitalityCode,
		H.code as hotelCode,
		H.hotelName as hotelName,
		H.mainBrandCode as primaryBrand,
		LTRIM(RTRIM(RM00101.SALSTERR)) as gpSalesTerritory,
		H.phg_areamanageridName as AMD,
		H.phg_regionalmanageridName as RD,
		H.phg_revenueaccountmanageridName as RAM,
		H.phg_AccountManagerName as AccountManager,
		case	when CRM.accountclassificationcode = 200000 then 'City Hotel'
				when CRM.accountclassificationcode = 200001 then 'Resort Hotel'
				else NULL
		end as CRMClassification,   
		H.geographicRegionName  as geographicRegion,
		H.shortName  as country,
		H.physicalCity as city,
		H.subAreaName as state,
		H.totalRooms hotelRooms,
		H.statuscodename hotelStatus,
		LTRIM(RTRIM(RM00101.CURNCYID)) as hotelCurrency,
		B.gpSiteID
into	#Hotels-- select * 
FROM	Core.dbo.hotelsReporting H
LEFT OUTER JOIN	IC.dbo.RM00101 
	ON H.code = RM00101.CUSTNMBR
LEFT OUTER JOIN LocalCRM.dbo.account CRM
	on	H.crmGuid = CRM.accountid
LEFT OUTER JOIN	Core.dbo.Brands B
	on	H.mainBrandCode = B.code

WHERE (	synxisID > 0 OR openHospitalityCode > 0 )
and	  ( @hotelcode is null OR H.code = @hotelcode )

create clustered index ix_Hotels ON #Hotels (synxisID, openHospitalityCode)



-- get iata account manager from CRM, store in separate table
select	accountnumber, min(phg_iataglobalaccountmanageridName) iatamanager
into	#rpt_IATAManager 
from	LocalCRM.dbo.account
where	phg_iataglobalaccountmanageridName is not null
and		accountnumber is not null
group by accountnumber

create clustered index ix_iatamanager ON #rpt_IATAManager (accountnumber)


--	build master MRT table - get all the data ONCE (not multiple reads of MRT)
--  cost, sums, counts, channels, sources etc, 
--  a field that indicates whether the record is billable 
select	mrt.crsSourceID,
		CASE mrt.crsSourceID WHEN 2 THEN mrt.openhospitalityID ELSE mrt.hotelID END as crsHotelID,
		mrt.arrivalDate ,
		mrt.confirmationDate ,
		mrt.confirmationNumber,
		1 as SynXisCount,
		mrt.status ,
		mrt.channel ,
		mrt.CROcode ,
		mrt.billingDescription ,
		mrt.SecondarySource,
		mrt.subSourceReportLabel,
		CASE	mrt.billingDescription WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost	END
		  + 
		CASE mrt.channel WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 END as cost ,
		IM.iatamanager as IataGam ,
		Core.dbo.GetIATAGroups(mrt.IATANumber, mrt.confirmationdate ) IATAGroups,
		mrt.IATANumber , 
		mrt.travelAgencyName , 
		mrt.travelAgencyCity , 
		mrt.travelAgencyState , 
		mrt.travelAgencyCountry , 
		mrt.rateCategoryCode , 
		mrt.rateTypeCode ,	
		mrt.rateTypeName as ratename ,
		case	when LEFT(mrt.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(mrt.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(mrt.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(mrt.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(mrt.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(mrt.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(mrt.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(mrt.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(mrt.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(mrt.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(mrt.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(mrt.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(mrt.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(mrt.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(mrt.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(mrt.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end as rateCategory_PHG  ,
		mrt.rooms ,
		mrt.nights ,
		mrt.rooms *	mrt.nights as RoomNights,
		mrt.currency ,
		mrt.reservationRevenue ,
		mrt.reservationRevenueUSD 	,
		0 as IPreferCount ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCharges ,
		CAST(0.00 AS  DECIMAL(18,2)) as IPreferCost 
into	#rpt_Main
from	superset.dbo.mostrecenttransactionsreporting mrt with (nolock)
LEFT OUTER JOIN Superset.dbo.gdsBookingCost 
	ON	mrt.secondarySource = gdsBookingCost.gdsType 
	AND mrt.arrivalDate BETWEEN gdsBookingCost.startDate AND ISNULL(gdsBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.synxisBookingCost 
	ON	mrt.billingDescription = synxisBookingCost.billingDescription 
	AND mrt.arrivalDate BETWEEN synxisBookingCost.startDate AND ISNULL(synxisBookingCost.endDate, '12/31/2999')
	
LEFT OUTER JOIN Superset.dbo.pegsBookingCost 
	ON	mrt.channel = pegsBookingCost.channel 
	AND mrt.arrivalDate BETWEEN pegsBookingCost.startDate AND ISNULL(pegsBookingCost.endDate, '12/31/2999')

left outer join #rpt_IATAManager IM with (nolock)
	on	mrt.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

where	MRT.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
and	  ( @hotelcode is null OR mrt.hotelCode = @hotelcode )
and	  ( @RateCode is null OR mrt.rateTypeCode = @RateCode ) 
AND		MRT.channel <> 'PMS Rez Synch' 
AND		MRT.CROcode <> 'HTL_Carlton'

-- this was to allow nulls to be inserted into #main for iprefer charges that do not have this data
Union All
Select	NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
		NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
		
create clustered index ix_rptMain_arrivaldate_synxisID on #rpt_Main (arrivaldate, crsHotelID)
create index ix_rptMain_ConfirmNum on #rpt_Main (confirmationnumber)

delete from #rpt_Main
where crsHotelID is null


-- get all billy charges from CalculatedDebits for the date parameters
SELECT	cd.confirmationNumber ,
		cd.hotelCode ,
		cd.gpSiteID ,
		cd.arrivalDate ,
		/* CODE CHANGE: TMG 2015-09-15
				new list of criteriaIDs that are surcharges - per hiren
		
		--1=Booking Charges
		SUM(CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END)	AS bookingCharges , 		
		--2=Commissions
		SUM(CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END)
		AS commissions , 
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(CASE classificationID WHEN 3 THEN 
			CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
			ELSE 0 END) AS surcharges 
		*/
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
			CASE classificationID 
			WHEN 1 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate )
			WHEN 3 THEN CASE WHEN C.itemCode IS NULL THEN dbo.convertcurrencytousd(chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				--3=Surcharge, but if the item code was not overridden in Billy we count it as booking revenue per Hiren
			ELSE 0 END END END ) AS bookingCharges , 		
		
		--2=Commissions
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 2) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then 0 ELSE 
				CASE classificationID WHEN 2 THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END END END )
			AS commissions , 
		
		--3=Surcharges, only if the item code was overridden do we count it separately per Hiren
		SUM(
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID in (1, 2) ) then 0 ELSE
			CASE WHEN (ESC.CriteriaId = 365 and ESC.Surcharge = 1 AND CD.classificationID = 3) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE
			CASE WHEN (ESC.CriteriaId IS NOT NULL and ESC.Surcharge = 1) then dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 
				CASE classificationID WHEN 3 THEN 
				CASE WHEN C.itemCode IS NOT NULL THEN dbo.convertcurrencytousd(cd.chargeValueInHotelCurrency , cd.hotelCurrencyCode , cd.arrivalDate ) ELSE 0 END 
				ELSE 0 END END END END ) AS surcharges 
		
into	#rpt_BillyCharges	
from	superset.dbo.calculatedDebits CD with (nolock)

-- this join is needed to only include billing for the desired rate code reservations
join	#rpt_Main MRT 
	on	CD.confirmationNumber = MRT.confirmationNumber

join	BillingProduction.dbo.criteria C with (nolock)
	on	cd.criteriaID = C.id 
left outer join BillingProduction.dbo.ExplicitSurchargeCriteriaId ESC
	on	Cd.criteriaID = ESC.CriteriaId
WHERE	cd.arrivalDate BETWEEN @arrivalStart AND @arrivalEnd
AND		( @hotelcode is null OR CD.hotelCode = @hotelcode )
--AND		cd.chargeValueInHotelCurrency <> 0  -- but without this it greatly increases the billybookingcount
			-- this is needed for the new bsi functionality, credit memos can now uncharge for cancelled reserveatins
GROUP BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID
ORDER BY CD.confirmationNumber , cd.hotelCode , cd.arrivaldate, cd.gpSiteID

create	clustered index ix_RptCharges ON #rpt_BillyCharges (confirmationnumber)



/*	2015-03-09 TMG

	this fix will keep billy count the same for each month, even when the MRT.arrival date has been changed 
	to a different month; so also NOT include the MRT record count, because the arrival month 

*/
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate , confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue , reservationRevenueUSD, IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (billy.HotelCode) , -- synxisid
		billy.arrivalDate, 
		missingmrt.confirmationDate,
		billy.confirmationNumber , -- confirmationnumber -- do not inflate synxis booking count because the billy charge has a different month
		0, -- do not inflate synxis booking count because the billy charge has a different month
		missingMRT.billingDescription, -- billing description
		missingMRT.channel, -- as channel ,
		missingMRT.Status , -- as status
		missingMRT.SecondarySource , -- as SecondarySource ,
		missingMRT.subSourceReportLabel , -- as subSourceReportLabel ,
		missingMRT.CROcode , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		IM.iatamanager , --as IataGam ,
		Core.dbo.GetIATAGroups(missingMRT.IATANumber, missingMRT.confirmationdate ) , --IATAGroups ,
		missingMRT.IATANumber, -- as IATANumber,
		missingMRT.travelAgencyName, -- as travelAgencyName,
		missingMRT.travelAgencyCity, -- as travelAgencyCity,
		missingMRT.travelAgencyState, -- as travelAgencyState,
		missingMRT.travelAgencyCountry, --as travelAgencyCountry,
		missingMRT.rateCategoryCode, --as rateCategoryCode,
		missingMRT.rateTypeCode, -- as rateTypeCode,
		missingMRT.rateTypeName , --as ratename ,
		case	when LEFT(missingMRT.rateTypeCode,3) = 'GOL' then 'GOLF'
				when LEFT(missingMRT.rateTypeCode,3) = 'PKG' then 'PKG'
				when LEFT(missingMRT.rateTypeCode,3) = 'PRO' then 'PRO'
				when LEFT(missingMRT.rateTypeCode,3) = 'MKT' then 'MKT'
				when LEFT(missingMRT.rateTypeCode,3) = 'CON' then 'CON'
				when LEFT(missingMRT.rateTypeCode,3) = 'NEG' then 'NEG'
				when LEFT(missingMRT.rateTypeCode,3) = 'BAR' then 'BAR'
				when LEFT(missingMRT.rateTypeCode,3) = 'DIS' then 'DIS'
				else 
					case	when LEFT(missingMRT.rateCategoryCode,3) = 'GOL' then 'GOLF'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PKG' then 'PKG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'PRO' then 'PRO'
							when LEFT(missingMRT.rateCategoryCode,3) = 'MKT' then 'MKT'
							when LEFT(missingMRT.rateCategoryCode,3) = 'CON' then 'CON'
							when LEFT(missingMRT.rateCategoryCode,3) = 'NEG' then 'NEG'
							when LEFT(missingMRT.rateCategoryCode,3) = 'BAR' then 'BAR'
							when LEFT(missingMRT.rateCategoryCode,3) = 'DIS' then 'DIS'
							else 'Other'
					end 
			end , -- as rateCategory_PHG,
		null, null, null, null, null, null,  -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD,
		0 ,  -- IPreferCount
		CAST(0.00 AS  DECIMAL(18,2)),-- as IPreferCharges ,
		CAST(0.00 AS  DECIMAL(18,2)) --as IPreferCost 
from	#rpt_BillyCharges billy
join	superset.dbo.mostRecentTransactionsReporting missingMRT
	on	billy.confirmationNumber = missingMRT.confirmationNumber

left outer join #rpt_IATAManager IM with (nolock)
	on	missingMRT.IATANumber = IM.accountnumber COLLATE SQL_Latin1_General_CP1_CI_AS

-- get only confirmation numbers that have not been added yet
left outer join #rpt_Main mrt
	on	billy.confirmationnumber = mrt.confirmationnumber
where	mrt.confirmationNumber is null
AND ( @hotelcode is null OR missingMRT.hotelcode = @hotelcode )
AND ( @RateCode is null OR missingMRT.rateTypeCode = @RateCode )


GROUP BY billy.HotelCode, 
		billy.arrivalDate, 
		missingMRT.confirmationDate ,
		billy.confirmationNumber,
		missingMRT.billingDescription ,
		missingMRT.channel, 
		missingMRT.Status,
		missingMRT.SecondarySource, 
		missingMRT.subSourceReportLabel, 
		missingMRT.CROcode, 
		IM.iatamanager, 
		--missingMRT.IATAGroups, 
		missingMRT.IATANumber, missingMRT.confirmationdate, 
		missingMRT.travelAgencyName, 
		missingMRT.travelAgencyCity, 
		missingMRT.travelAgencyState, 
		missingMRT.travelAgencyCountry, 
		missingMRT.rateCategoryCode, 
		missingMRT.rateTypeCode, 
		missingMRT.rateTypeName



-- get IPrefer Counts and Sums by current billing date, using the following logic:
--		if matching a current MRT record - use the MRT data for channel, source, etc
--		will have been added manually with a current period arrival date - use channel = 'IPrefer Manual Entry'
--		will not match a current MRT record and will have a different arrival date - use channel = 'IPrefer Prior Period'
insert into #rpt_Main
	(	crsHotelID, arrivaldate, confirmationdate, confirmationnumber, SynXisCount, billingDescription, Channel , 
		status, SecondarySource, subSourceReportLabel, CROcode,	cost, IataGam, IATAGroups, IATANumber,
		travelAgencyName, travelAgencyCity, travelAgencyState, travelAgencyCountry,
		rateCategoryCode, rateTypeCode, ratename, rateCategory_PHG, rooms, nights, RoomNights, currency ,
		reservationRevenue, reservationRevenueUSD , IPreferCount, IPreferCharges, IPreferCost )
select	dbo.GetSynXisIDwithHotelCode (IP.Hotel_Code) , -- synxisid
		IP.Billing_Date, -- the IPrefer still need to be categorized as Month(MRT.ArrivalDate)
		IP.Booking_Date , -- for currency conversions
		null, -- confirmationnumber -- do not inflate synxis booking count because of an IPrefer match
		0 , -- do not inflate synxis booking count because the billy charge has a different month
		COALESCE(MRT.billingDescription, 'Non-Matching IPrefer Reservation') , -- billing description
		COALESCE(MRT.channel, 
				case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
					else	'IPrefer Prior Period'
				end ) , -- as channel ,
		COALESCE(MRT.Status, '') , -- as status
		COALESCE(MRT.SecondarySource, '') , -- as SecondarySource ,
		COALESCE(MRT.subSourceReportLabel, '') , -- as subSourceReportLabel ,
		COALESCE(MRT.CROcode, '') , -- as CROcode ,
		null, -- COALESCE(MRT.cost, 0) , -- as cost , TMG, 20150223: caused iPrefer records . cost to double -- already in #rpt_Main
		COALESCE(MRT.IataGam, '') , -- as IataGam ,
		COALESCE(MRT.IATAGroups, '') , -- as IATAGroups ,
		COALESCE(MRT.IATANumber, '') , -- as IATANumber,
		COALESCE(MRT.travelAgencyName, '') , -- as travelAgencyName,
		COALESCE(MRT.travelAgencyCity, '') , -- as travelAgencyCity,
		COALESCE(MRT.travelAgencyState, '') , -- as travelAgencyState,
		COALESCE(MRT.travelAgencyCountry, '') , --as travelAgencyCountry,
		COALESCE(MRT.rateCategoryCode, '') , --as rateCategoryCode,
		COALESCE(MRT.rateTypeCode, '') , -- as rateTypeCode,
		COALESCE(MRT.ratename, '') , -- as ratename,
		COALESCE(MRT.rateCategory_PHG, '') , -- as rateCategory_PHG,
		null, null, null, null, null, null, -- rooms, nights, roomnights, currency, reservationRevenue, reservationrevenueUSD
		COUNT(distinct IP.Booking_ID),  -- IPreferCount
		Sum(dbo.convertCurrencyToUSD( IP.Billable_Amount_Hotel_Currency ,-- IPreferCharges
			IP.Hotel_Currency, IP.Arrival_Date ) )	,
		SUM(IP.Reservation_Amount_USD * 0.02) -- as IPreferCost
from	BSI.IPrefer_Charges IP

--left outer 
-- removed left outer join - question for hiren, do we want all iprefer charges - or just those associated with the MER MRT reservations??

join #rpt_Main mrt
	on	IP.booking_id = mrt.confirmationnumber

where	IP.billing_date between @arrivalStart and @arrivalEnd
AND		( @hotelcode is null OR IP.Hotel_Code = @hotelcode )

GROUP BY IP.Hotel_Code, IP.Billing_Date , IP.Booking_Date ,
		MRT.arrivalDate, MRT.billingDescription , MRT.channel, 
		MRT.Status,
		case when IP.arrival_date between @arrivalStart and @arrivalEnd then 'IPrefer Manual Entry'
				else	'IPrefer Prior Period'
			end  , 
		MRT.SecondarySource, 
		MRT.subSourceReportLabel, 
		MRT.CROcode, 
		MRT.cost, 
		MRT.IataGam, 
		MRT.IATAGroups, 
		MRT.IATANumber, 
		MRT.travelAgencyName, 
		MRT.travelAgencyCity, 
		MRT.travelAgencyState, 
		MRT.travelAgencyCountry, 
		MRT.rateCategoryCode, 
		MRT.rateTypeCode, 
		MRT.ratename,
		MRT.rateCategory_PHG


if 	@Extended = 0 

		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.billingDescription ,
				main.channel , 
				main.secondarySource , 
				main.subSourceReportLabel , 
				main.CROcode ,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
					
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,
								
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,

				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency
				
		INTO	#Results
		
		FROM	#Hotels  as hotels 
		LEFT OUTER JOIN	#rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.Channel ,	
				main.billingDescription, 
				main.SecondarySource, 
				main.subSourceReportLabel, 
				main.CROcode --,billy.gpSiteID -- removed 2015-01-06 TMG -- put MIN(billy.gpsiteid) in above select
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager ,
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelcurrency, 
				main.billingDescription ,
				main.channel ,
				main.secondarySource ,
				main.subSourceReportLabel ,
				main.CROcode ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG,
				YEAR(main.arrivaldate) ArrivalYear , 
				MONTH(main.arrivaldate) ArrivalMonth ,
				
				SUM(Case when main.status <> 'Cancelled' then main.SynXisCount else null end ) as ConfirmedSynxisBookingCount ,
				SUM(Case when main.status = 'Cancelled' then main.SynXisCount else null end ) as CancelledSynxisBookingCount , 
				SUM(Case when main.status <> 'Cancelled' then main.reservationrevenueUSD else 0 end) ConfirmedRevenueUSD ,
				SUM(Case when main.status = 'Cancelled' then main.reservationrevenueUSD else 0 end) CancelledRevenueUSD ,
				SUM(Case when main.status <> 'Cancelled' then main.roomNights else 0 end) as ConfirmedRoomNights ,
				SUM(Case when main.status = 'Cancelled' then main.roomNights else 0 end) as CancelledRoomNights ,
				SUM(Case when main.status <> 'Cancelled' then main.cost else 0 end) as ConfirmedTotalCost , 
				SUM(Case when main.status = 'Cancelled' then main.cost else 0 end) as CancelledTotalCost , 
				SUM(main.reservationRevenueUSD)/ SUM(main.roomNights) as avgNightlyRateUSD ,
				SUM(main.nights) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as avgLengthOfStay , 

				-- TMG 2015-02-09 - new columns to remove GPSiteID from the Group By
				COUNT(billy.confirmationnumber) as BillyBookingCount, 
				COUNT(Case when  billy.gpSiteID in ( 1 ) then billy.confirmationnumber else null end) as PHGSiteBillyBookingCount ,
				COUNT(Case when  billy.gpSiteID in (6, 8) then billy.confirmationnumber else null end) as HHASiteBillyBookingCount ,
				SUM(billy.bookingCharges) as BillyBookingCharges,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges else 0 end) as PHGSiteBillyBookingCharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges else 0 end) as HHASiteBillyBookingCharges ,
				SUM(billy.commissions) as commissions ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.commissions else 0 end) as PHGSiteCommissions ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.commissions else 0 end) as HHASiteCommissions ,
				SUM(billy.surcharges) as surcharges ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.surcharges else 0 end) as PHGSiteSurcharges ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.surcharges else 0 end) as HHASiteSurcharges,			
				SUM(billy.bookingCharges) + SUM(billy.commissions) BookAndComm ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then billy.bookingCharges + billy.commissions else 0 end) as PHGSiteBookAndComm ,
				SUM(Case when  billy.gpSiteID in (6, 8) then billy.bookingCharges + billy.commissions else 0 end) as HHASiteBookAndComm,
				SUM(main.cost) / Case when COUNT(main.confirmationnumber) = 0 then 1 else COUNT(main.confirmationnumber) end as costperbooking ,
				SUM(main.IPreferCount) as IPreferBookingCount,
				SUM(main.IPrefercharges) as IPreferCharges,
				SUM(main.IPreferCost) as IPreferCost,
				CASE WHEN COALESCE(SUM(billy.bookingCharges) + SUM(billy.commissions),0) = 0 THEN COUNT(billy.confirmationnumber) ELSE 0 END as BillyBookingWithZeroBookAndComm ,
				
				-- adding all sum fields in hotel billing currency from USD
				SUM(Case when main.status <> 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) ConfirmedRevenueHotelCurrency ,
				SUM(Case when main.status = 'Cancelled' then Superset.dbo.convertCurrencyXE(main.reservationrevenue, main.currency, hotels.hotelcurrency, main.confirmationdate) else 0 end) CancelledRevenueHotelCurrency ,					
				SUM( Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBillyBookingChargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBillyBookingChargesHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as CommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteCommissionsHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteCommissionsHotelCurrency ,				
				SUM( Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as SurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteSurchargesHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.surcharges, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteSurchargesHotelCurrency,
				SUM( Superset.dbo.convertCurrencyXE( billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) ) as BookAndCommHotelCurrency,
				SUM(Case when  billy.gpSiteID in ( 1 ) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as PHGSiteBookAndCommHotelCurrency ,
				SUM(Case when  billy.gpSiteID in (6, 8) then Superset.dbo.convertCurrencyXE(billy.bookingCharges + billy.commissions, 'USD', hotels.hotelcurrency, billy.arrivaldate) else 0 end) as HHASiteBookAndCommHotelCurrency ,
				SUM( Superset.dbo.convertCurrencyXE( main.IPrefercharges, 'USD', hotels.hotelcurrency, main.arrivaldate) ) as IPreferChargesHotelCurrency
		
		INTO	#Results_Extended

		FROM	#Hotels 	 as hotels 
		left outer JOIN #rpt_Main as main
			on	main.crsHotelID = CASE main.crsSourceID WHEN 2 THEN hotels.openHospitalityCode ELSE hotels.synxisID END
		LEFT OUTER JOIN #rpt_BillyCharges as billy
			on	main.confirmationnumber = billy.confirmationNumber
		
		GROUP BY YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , 
		
				main.crsHotelID ,
				hotels.hotelCode, 
				hotels.hotelName, 
				hotels.primaryBrand, 
				hotels.gpSalesTerritory, 
				hotels.AMD,
				hotels.RD,
				hotels.RAM,
				hotels.AccountManager, 
				hotels.geographicRegion,
				hotels.country,
				hotels.city,
				hotels.state,
				hotels.hotelRooms,
				hotels.hotelStatus,
				hotels.CRMClassification ,
				hotels.hotelCurrency ,
				main.crsHotelID ,
				main.Channel ,
				main.billingDescription,
				main.SecondarySource,
				main.subSourceReportLabel,
				main.CROcode ,
				hotels.hotelCurrency ,
				main.IataGam, 
				main.IATAGroups, 
				main.IATANumber,
				main.travelAgencyName, 
				main.travelAgencyCity, 
				main.travelAgencyState, 
				main.travelAgencyCountry,
				main.rateCategoryCode, 
				main.rateTypeCode, 
				main.ratename, 
				main.rateCategory_PHG --, billy.gpSiteID -- TMG removed and put MIN(billy.gpSiteID) in select -- 2015-01-06
		ORDER BY main.crsHotelID, YEAR(main.arrivaldate) , MONTH(main.arrivaldate) , main.channel, main.secondarysource, main.subsourcereportlabel


if @extended = 0
		SELECT	*
		FROM	#Results
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel


ELSE -- @extended = 1, returns different resultset
		SELECT	*
		FROM	#Results_Extended
		WHERE	NOT ( BillyBookingCount = 0 
			AND		ConfirmedSynxisBookingCount = 0
			AND		CancelledSynxisBookingCount = 0
			AND		IPreferBookingCount = 0
				)
		
		ORDER BY crsHotelID, ArrivalYear , ArrivalMonth , channel, secondarysource, subsourcereportlabel




if	OBJECT_ID ('#usdExchange ') IS NOT NULL
	drop table #usdExchange 

if	OBJECT_ID ('#Hotels') IS NOT NULL
	drop table #Hotels

if	OBJECT_ID ('#rpt_Main') IS NOT NULL
	drop table #rpt_Main

if	OBJECT_ID ('#rpt_BillyCharges') IS NOT NULL
	drop table #rpt_BillyCharges

if	OBJECT_ID ('#rpt_IPreferCharges') IS NOT NULL
	drop table #rpt_IPreferCharges

if	OBJECT_ID ('#rpt_IATAManager') IS NOT NULL
	drop table #rpt_IATAManager

if	OBJECT_ID ('#profitabilityreport_tmg') IS NOT NULL
	drop table #profitabilityreport_tmg

if	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	drop table #profitabilityreport_tmg_ext

if	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	drop table #Results

if	OBJECT_ID ('#profitabilityreport_tmg_ext') IS NOT NULL
	drop table #Results_Extended




END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[ProfitabilityReport_old]'
GO
CREATE TABLE [Reporting].[ProfitabilityReport_old]
(
[synxisID] [int] NULL,
[hotelCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[primaryBrand] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[gpSalesTerritory] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMD] [nvarchar] (160) COLLATE Latin1_General_CI_AI NULL,
[RD] [nvarchar] (160) COLLATE Latin1_General_CI_AI NULL,
[RAM] [nvarchar] (160) COLLATE Latin1_General_CI_AI NULL,
[geographicRegion] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[country] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelRooms] [int] NULL,
[hotelStatus] [nvarchar] (255) COLLATE Latin1_General_CI_AI NULL,
[arrivalYear] [int] NULL,
[arrivalMonth] [int] NULL,
[billingDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[channel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondarySource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSourceReportLabel] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROcode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[costPerBooking] [decimal] (7, 2) NULL,
[hotelCurrency] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[exchangeRate] [decimal] (21, 9) NULL,
[synxisBookingCount] [int] NULL,
[totalCost] [decimal] (18, 2) NULL,
[hotelRevenueUSD] [decimal] (38, 2) NULL,
[roomNights] [int] NULL,
[avgNightlyRateUSD] [decimal] (38, 6) NULL,
[avgLengthOfStay] [int] NULL,
[billyBookingCount] [int] NULL,
[bookingCharges] [decimal] (38, 6) NULL,
[commissions] [decimal] (38, 6) NULL,
[surcharges] [decimal] (38, 6) NULL,
[BookAndComm] [decimal] (38, 6) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PW_HotelCurrencyCode]'
GO
CREATE TABLE [dbo].[PW_HotelCurrencyCode]
(
[synxisID] [int] NOT NULL,
[CurrencyCode] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__PW_Hotel__625B59F73FB9C309] on [dbo].[PW_HotelCurrencyCode]'
GO
ALTER TABLE [dbo].[PW_HotelCurrencyCode] ADD CONSTRAINT [PK__PW_Hotel__625B59F73FB9C309] PRIMARY KEY CLUSTERED  ([synxisID], [CurrencyCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[profitabilitylastmonth]'
GO
CREATE TABLE [dbo].[profitabilitylastmonth]
(
[arrival Year] [float] NULL,
[arrival Month] [float] NULL,
[synxis ID] [float] NULL,
[hotel status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotel Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotel Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotel rooms] [float] NULL,
[primary Brand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP Sales Territory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RAM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[geographic Region] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[country] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Channel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Secondary Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sub Source Report Label] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROcode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Synxis Bookings Count] [float] NULL,
[Billy Bookings Count] [float] NULL,
[Avg Cost Per Booking] [float] NULL,
[Total Cost] [float] NULL,
[Avg Charge Per Booking] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Booking Charges] [float] NULL,
[Commissions] [float] NULL,
[Surcharges] [float] NULL,
[Book and Comm] [float] NULL,
[Avg Per Booking Profit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Total Profit] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Room Nights] [float] NULL,
[Avg Length of Stay] [float] NULL,
[Avg Nightly Rate] [float] NULL,
[Hotel Revenue] [float] NULL,
[R#Report] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel Status1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AREA] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Key] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[New Book & Comm] [float] NULL,
[Old Book & Comm] [float] NULL,
[New Hotel Revenue] [float] NULL,
[F42] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[profitlastmonth]'
GO
CREATE TABLE [dbo].[profitlastmonth]
(
[arrival Year] [float] NULL,
[arrival Month] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[synxis ID] [float] NULL,
[hotel status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotel Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotel Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotel rooms] [float] NULL,
[primary Brand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GP Sales Territory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AMD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RAM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[geographic Region] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[country] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[state] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Channel] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Secondary Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sub Source Report Label] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROcode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Synxis Bookings Count] [float] NULL,
[Billy Bookings Count] [float] NULL,
[Avg Cost Per Booking] [float] NULL,
[Total Cost] [float] NULL,
[Avg Charge Per Booking] [float] NULL,
[Booking Charges] [float] NULL,
[Commissions] [float] NULL,
[Surcharges] [float] NULL,
[Book and Comm] [float] NULL,
[Avg Per Booking Profit] [float] NULL,
[Total Profit] [float] NULL,
[Room Nights] [float] NULL,
[Avg Length of Stay] [float] NULL,
[Avg Nightly Rate] [float] NULL,
[Hotel Revenue] [float] NULL,
[R#Report] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel Status1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AREA] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
