USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 1:12:49 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[PW_HotelCurrencyCode]'
GO
ALTER TABLE [dbo].[PW_HotelCurrencyCode] DROP CONSTRAINT [PK__PW_Hotel__625B59F73FB9C309]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[profitlastmonth]'
GO
DROP TABLE [dbo].[profitlastmonth]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[profitabilitylastmonth]'
GO
DROP TABLE [dbo].[profitabilitylastmonth]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[PW_HotelCurrencyCode]'
GO
DROP TABLE [dbo].[PW_HotelCurrencyCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReport_old]'
GO
DROP TABLE [Reporting].[ProfitabilityReport_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReport_RateCode]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReport_RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReport_ConfirmationNumber]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReport_ConfirmationNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReport_HotelCode]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReport_HotelCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReport_HotelCode_Extended]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReport_HotelCode_Extended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[profitabilityReportOld]'
GO
DROP PROCEDURE [dbo].[profitabilityReportOld]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Hotel_Export_BCD]'
GO
DROP VIEW [BSI].[Hotel_Export_BCD]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[iPreferGDSYearOverYearRevenue]'
GO
DROP PROCEDURE [dbo].[iPreferGDSYearOverYearRevenue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[profitabilityReport_Old2]'
GO
DROP PROCEDURE [dbo].[profitabilityReport_Old2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[ipr_iPreferBillingReconciliation]'
GO
DROP PROCEDURE [dbo].[ipr_iPreferBillingReconciliation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[rpt_TravelAgencyLocalCurrency]'
GO
DROP PROCEDURE [Reporting].[rpt_TravelAgencyLocalCurrency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[PW_MostRecentTransactions]'
GO
DROP TABLE [dbo].[PW_MostRecentTransactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReport_GDSwithIATAs_AdHocDELETEME]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReport_GDSwithIATAs_AdHocDELETEME]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[iPreferYearOverYearRevenue]'
GO
DROP PROCEDURE [dbo].[iPreferYearOverYearRevenue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[profitabilityExtendedReport]'
GO
DROP PROCEDURE [dbo].[profitabilityExtendedReport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReportBCD]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReportBCD]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReportResults]'
GO
DROP TABLE [Reporting].[ProfitabilityReportResults]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReportResultsExtended]'
GO
DROP TABLE [Reporting].[ProfitabilityReportResultsExtended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[ProfitabilityReportKES]'
GO
DROP PROCEDURE [Reporting].[ProfitabilityReportKES]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [Reporting].[RollupGetiPrefer]'
GO
-- =============================================
-- Author:		Jake Smith
-- Create date: August 28, 2019
-- Description:	Get iPrefer data for Rollup Mid-Year GM 
-- =============================================
CREATE PROCEDURE [Reporting].[RollupGetiPrefer]
	-- Add the parameters for the stored procedure here
 @startDate date
,@endDate  date
,@hotelCode varchar(250)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	
SELECT DISTINCT MRT.hotelName as hotelName,MRT.hotelCode,MRT.confirmationNumber,MRT.roomNights,MRT.arrivalDate,ASI.Membership_Number,MRT.reservationRevenueUSD,IBC.Channel
	FROM dbo.mostRecentTransactionsReporting AS MRT
		JOIN BSI.Account_Statement_Import AS ASI ON ASI.Booking_ID = MRT.confirmationNumber AND ASI.Hotel_Code = mrt.hotelCode
		LEFT OUTER JOIN dbo.IPreferChannelMapping AS IBC ON ASI.Booking_Source = IBC.Concept 
	WHERE (Mrt.hotelCode = @hotelCode OR @hotelCode IS NULL)
		AND MRT.arrivalDate BETWEEN @startDate AND @endDate
	
	UNION ALL
	
	SELECT ASI.Hotel_Name,ASI.Hotel_Code,ASI.Booking_ID as confirmationNumber,
			ISNULL(SUM(DATEDIFF(D, ASI.Arrival_Date, ASI.Departure_Date)), 0) AS roomNights,ASI.Arrival_Date as arrivalDate,ASI.Membership_Number,
			MIN(ASI.Reservation_Amount_USD) as reservationRevenueUSD,IBC.Channel
	FROM dbo.mostRecentTransactionsReporting AS MRT
		RIGHT JOIN BSI.Account_Statement_Import AS ASI ON ASI.Booking_ID = MRT.confirmationNumber AND ASI.Hotel_Code = mrt.hotelCode
		LEFT OUTER JOIN dbo.IPreferChannelMapping AS IBC ON ASI.Booking_Source = IBC.Concept
	WHERE (ASI.Hotel_Code =  @hotelCode OR @hotelCode IS NULL)
		AND asi.Arrival_Date  BETWEEN @startDate AND @endDate
		AND MRT.confirmationNumber is null
	GROUP BY ASI.Arrival_Date, ASI.Booking_ID, ASI.Membership_Number, ASI.Hotel_Name, ASI.Hotel_Code, ASI.Departure_Date, IBC.Channel
	HAVING MIN(ASI.Reservation_Amount_USD) >= 0
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
