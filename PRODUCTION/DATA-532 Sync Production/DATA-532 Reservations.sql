USE Reservations
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 1:07:45 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[BillyReservationsCharges_Results]'
GO
DROP PROCEDURE [rpt].[BillyReservationsCharges_Results]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[IPreferGuestNameReport_2015]'
GO
DROP PROCEDURE [rpt].[IPreferGuestNameReport_2015]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[HistoricHotelsReservations_Weekly]'
GO
DROP PROCEDURE [rpt].[HistoricHotelsReservations_Weekly]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[HistoricHotelsReservations_Monthly]'
GO
DROP PROCEDURE [rpt].[HistoricHotelsReservations_Monthly]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[Cancellations]'
GO
DROP PROCEDURE [rpt].[Cancellations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[BillyReservationsCharges_ReservationDetails]'
GO
DROP PROCEDURE [rpt].[BillyReservationsCharges_ReservationDetails]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Populate_MostRecentTransaction]'
GO
DROP PROCEDURE [dbo].[Populate_MostRecentTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ReservationInvoiceDetail]'
GO
DROP PROCEDURE [rpt].[ReservationInvoiceDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ProfitabilityExtendedByConfirmation]'
GO
DROP PROCEDURE [rpt].[ProfitabilityExtendedByConfirmation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [synxis].[RawDataForLoading]'
GO


ALTER FUNCTION [synxis].[RawDataForLoading]
(
	@QueueID int = NULL,
	@startDate date = NULL,
	@endDate date = NULL
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 0 AS QueueID,[chainName],[chainID],[hotelName],[hotelID],[SAPID],[hotelCode],[billingDescription],[transactionTimeStamp],[faxNotificationCount],[channel],[secondarySource],[subSource],[subSourceCode],[PMSRateTypeCode],[PMSRoomTypeCode],[marketSourceCode],[marketSegmentCode],[userName],[status],rd.[confirmationNumber],[confirmationDate],[cancellationNumber],[cancellationDate],[salutation],[guestFirstName],[guestLastName],[customerID],[customerAddress1],[customerAddress2],[customerCity],[customerState],[customerPostalCode],[customerPhone],[customerCountry],[customerArea],[customerRegion],[customerCompanyName],[arrivalDate],[departureDate],[bookingLeadTime],[rateCategoryName],[rateCategoryCode],[rateTypeName],[rateTypeCode],[roomTypeName],[roomTypeCode],[nights],[averageDailyRate],[rooms],[reservationRevenue],[currency],[IATANumber],[travelAgencyName],[travelAgencyAddress1],[travelAgencyAddress2],[travelAgencyCity],[travelAgencyState],[travelAgencyPostalCode],[travelAgencyPhone],[travelAgencyFax],[travelAgencyCountry],[travelAgencyArea],[travelAgencyRegion],[travelAgencyEmail],[consortiaCount],[consortiaName],[totalPackageRevenue],[optIn],[customerEmail],[totalGuestCount],[adultCount],[childrenCount],[creditCardType],[actionType],[shareWith],[arrivalDOW],[departureDOW],[itineraryNumber],[secondaryCurrency],[secondaryCurrencyExchangeRate],[secondaryCurrencyAverageDailyRate],[secondaryCurrencyReservationRevenue],[secondaryCurrencyPackageRevenue],[commisionPercent],[membershipNumber],[corporationCode],[promotionalCode],[CROCode],[channelConnectConfirmationNumber],[primaryGuest],[loyaltyProgram],rd.[loyaltyNumber],[vipLevel],[xbeTemplateName],[xbeShellName],[profileTypeSelection],[averageDailyRateUSD],[reservationRevenueUSD],[totalPackageRevenueUSD],[timeLoaded]
			,LoyaltyNumberTagged,LoyaltyNumberValidated
	FROM Superset.[dbo].[rawData] rd
		INNER JOIN
			(
				SELECT confirmationNumber,loyaltyNumber,LoyaltyNumberTagged,LoyaltyNumberValidated
				FROM Superset.dbo.mostrecenttransactions 
				WHERE arrivalDate >= @startDate
					AND arrivalDate <= CASE WHEN @endDate IS NULL THEN '9999-12-31' ELSE @endDate END
			) mrt ON mrt.confirmationNumber = rd.confirmationNumber
	WHERE @QueueID IS NULL
		AND RIGHT(rd.confirmationNumber,3) != '_OH'
--------------------------------------
		--AND rd.confirmationNumber NOT IN(SELECT confirmationNumber FROM Reservations.synxis.Transactions)
--------------------------------------
	UNION ALL

	SELECT DISTINCT @QueueID AS QueueID,
		ISNULL(chainName,'') AS chainName,CONVERT(int,NULLIF(chainID,'')) AS chainID,ISNULL(hotelName,'') AS hotelName,NULLIF(hotelID,'') AS hotelID,ISNULL(SAPID,'') AS SAPID,
		ISNULL(hotelCode,'') AS hotelCode,ISNULL(billingDescription,'') AS billingDescription,CONVERT(datetime,NULLIF(transactionTimeStamp,'')) AS transactionTimeStamp,
		CONVERT(int,NULLIF(faxNotificationCount,'')) AS faxNotificationCount,
		CASE WHEN channel = 'PMS' THEN 'PMS Rez Synch' ELSE ISNULL(channel,'') END AS channel,
		ISNULL(secondarySource,'') AS secondarySource,
		ISNULL(subSource,'') AS subSource,ISNULL(subSourceCode,'') AS subSourceCode,ISNULL(PMSRateTypeCode,'') AS PMSRateTypeCode,ISNULL(PMSRoomTypeCode,'') AS PMSRoomTypeCode,
		ISNULL(marketSourceCode,'') AS marketSourceCode,ISNULL(marketSegmentCode,'') AS marketSegmentCode,ISNULL(userName,'') AS userName,ISNULL(status,'') AS status,
		ISNULL(confirmationNumber,'') AS confirmationNumber,CONVERT(datetime,NULLIF(confirmationDate,'')) AS confirmationDate,ISNULL(cancellationNumber,'') AS cancellationNumber,
		NULLIF(CONVERT(datetime,NULLIF(cancellationDate,'')),'1900-01-01') AS cancellationDate,ISNULL(salutation,'') AS salutation,ISNULL(guestFirstName,'') AS guestFirstName,
		ISNULL(guestLastName,'') AS guestLastName,SUBSTRING(ISNULL(customerID,''),1,12) AS customerID,ISNULL(customerAddress1,'') AS customerAddress1,
		ISNULL(customerAddress2,'') AS customerAddress2,ISNULL(customerCity,'') AS customerCity,ISNULL(customerState,'') AS customerState,ISNULL(customerPostalCode,'') AS customerPostalCode,
		ISNULL(customerPhone,'') AS customerPhone,ISNULL(customerCountry,'') AS customerCountry,ISNULL(customerArea,'') AS customerArea,ISNULL(customerRegion,'') AS customerRegion,
		ISNULL(customerCompanyName,'') AS customerCompanyName,CONVERT(datetime,NULLIF(arrivalDate,'')) AS arrivalDate,CONVERT(datetime,NULLIF(departureDate,'')) AS departureDate,
		CONVERT(int,NULLIF(bookingLeadTime,'')) AS bookingLeadTime,ISNULL(rateCategoryName,'') AS rateCategoryName,ISNULL(rateCategoryCode,'') AS rateCategoryCode,
		ISNULL(rateTypeName,'') AS rateTypeName,ISNULL(rateTypeCode,'') AS rateTypeCode,ISNULL(roomTypeName,'') AS roomTypeName,ISNULL(roomTypeCode,'') AS roomTypeCode,
		CONVERT(int,NULLIF(nights,'')) AS nights,CONVERT(decimal(12,2),NULLIF(averageDailyRate,'')) AS averageDailyRate,CONVERT(int,NULLIF(rooms,'')) AS rooms,
		CONVErt(DECIMAL(18,2),NULLIF(reservationRevenue,'')) AS reservationRevenue,ISNULL(currency,'') AS currency,ISNULL(IATANumber,'') AS IATANumber,
		ISNULL(travelAgencyName,'') AS travelAgencyName,ISNULL(travelAgencyAddress1,'') AS travelAgencyAddress1,ISNULL(travelAgencyAddress2,'') AS travelAgencyAddress2,
		ISNULL(travelAgencyCity,'') AS travelAgencyCity,ISNULL(travelAgencyState,'') AS travelAgencyState,ISNULL(travelAgencyPostalCode,'') AS travelAgencyPostalCode,
		ISNULL(travelAgencyPhone,'') AS travelAgencyPhone,ISNULL(travelAgencyFax,'') AS travelAgencyFax,ISNULL(travelAgencyCountry,'') AS travelAgencyCountry,
		ISNULL(travelAgencyArea,'') AS travelAgencyArea,ISNULL(travelAgencyRegion,'') AS travelAgencyRegion,ISNULL(travelAgencyEmail,'') AS travelAgencyEmail,
		CONVERT(int,NULLIF(consortiaCount,'')) AS consortiaCount,ISNULL(consortiaName,'') AS consortiaName,CONVERT(decimal(18,2),NULLIF(totalPackageRevenue,'')) AS totalPackageRevenue,
		ISNULL(optIn,'') AS optIn,ISNULL(customerEmail,'') AS customerEmail,CONVERT(int,NULLIF(totalGuestCount,'')) AS totalGuestCount,CONVERT(int,NULLIF(adultCount,'')) AS adultCount,
		CONVERT(int,NULLIF(childrenCount,'')) AS childrenCount,ISNULL(creditCardType,'') AS creditCardType,ISNULL(actionType,'') AS actionType,ISNULL(shareWith,'') AS shareWith,
		ISNULL(arrivalDOW,'') AS arrivalDOW,ISNULL(departureDOW,'') AS departureDOW,ISNULL(itineraryNumber,'') AS itineraryNumber,ISNULL(secondaryCurrency,'') AS secondaryCurrency,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyExchangeRate,'')) AS secondaryCurrencyExchangeRate,CONVERT(decimal(18,2),NULLIF(secondaryCurrencyAverageDailyRate,'')) AS secondaryCurrencyAverageDailyRate,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyReservationRevenue,'')) AS secondaryCurrencyReservationRevenue,CONVERT(decimal(18,2),NULLIF(secondaryCurrencyPackageRevenue,'')) AS secondaryCurrencyPackageRevenue,
		CONVERT(decimal(18,2),NULLIF(TRIM(commisionPercent),'')) AS commisionPercent,ISNULL(membershipNumber,'') AS membershipNumber,ISNULL(corporationCode,'') AS corporationCode,
		ISNULL(promotionalCode,'') AS promotionalCode,ISNULL(CROCode,'') AS CROCode,ISNULL(channelConnectConfirmationNumber,'') AS channelConnectConfirmationNumber,
		ISNULL(primaryGuest,'') AS primaryGuest,ISNULL(loyaltyProgram,'') AS loyaltyProgram,ISNULL(loyaltyNumber,'') AS loyaltyNumber,ISNULL(vipLevel,'') AS vipLevel,
		ISNULL(s.xbeTemplateName,'') AS xbeTemplateName,ISNULL(xbeShellName,'') AS xbeShellName,ISNULL(profileTypeSelection,'') AS profileTypeSelection,
		Superset.dbo.convertCurrencyToUSD(averageDailyRate,currency,confirmationDate) AS averageDailyRateUSD,
		Superset.dbo.convertCurrencyToUSD(reservationRevenue,currency,confirmationDate) AS reservationRevenueUSD,
		Superset.dbo.convertCurrencyToUSD(totalPackageRevenue,currency,confirmationDate) AS totalPackageRevenueUSD,
		(SELECT ISNULL([ImportFinished],[ImportStarted]) FROM ETL.dbo.[Queue] WHERE QueueID = @QueueID) AS timeLoaded
		,CASE
			WHEN ISNULL(tag1.[iPrefer Number],'') > '' THEN 1
			WHEN ISNULL(tag2.[iPrefer Number],'') > '' THEN 1
			ELSE 0
		END AS LoyaltyNumberTagged
		,ISNULL(cpi.IsMemberEnabled,0) AS LoyaltyNumberValidated

	FROM ETL.dbo.Import_Sabre s
		LEFT JOIN
			(
				SELECT [iPrefer_Number],Membership_Date,IsMemberEnabled
				FROM [Superset].[BSI].[Customer_Profile_Import]
			) cpi ON cpi.iPrefer_Number = s.loyaltyNumber
				AND s.arrivalDate >= cpi.Membership_Date
				AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		LEFT JOIN Superset.[BSI].[TaggedCustomers] tag1 ON tag1.[iPrefer Number] = s.loyaltyNumber
														AND tag1.Hotel_Code = s.hotelCode
														AND s.arrivalDate >= tag1.DateTagged
														AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		LEFT JOIN [ReservationBilling].[dbo].[Templates] temp ON temp.xbeTemplateName = s.xbeTemplateName
		LEFT JOIN [Superset].[BSI].[Customer_Profile_Import] cpi2 ON cpi2.Email = s.customerEmail
		LEFT JOIN Superset.[BSI].[TaggedCustomers] tag2 ON tag2.[iPrefer Number] = cpi2.iPrefer_Number
														AND tag2.Hotel_Code = s.hotelCode
														AND s.arrivalDate >= tag2.DateTagged
														AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
														AND temp.templateGroupID = 3

	WHERE QueueID = @QueueID
		AND primaryGuest = 'Y'
)

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [synxis].[LoadTransactions]'
GO



ALTER PROCEDURE [synxis].[LoadTransactions]
	@QueueID int = NULL,
	@startDate date = NULL,
	@endDate date = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);


	-- ADD ACTION TYPE ----------------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.synXis.ActionType'
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO Reservations.synXis.ActionType(actionType,actionTypeOrder)
		SELECT DISTINCT ActionType,CASE ActionType WHEN 'N' THEN 1 WHEN 'M' THEN 2 WHEN 'X' THEN 4 WHEN 'NX' THEN 3 ELSE 0 END
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
			EXCEPT
		SELECT actionType,actionTypeOrder
		FROM Reservations.synXis.ActionType
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[IATANumber] ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[IATANumber]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[IATANumber](IATANumber)
		SELECT DISTINCT IATANumber
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE IATANumber IS NOT NULL
			EXCEPT
		SELECT IATANumber FROM Reservations.[synxis].[IATANumber]
	-----------------------------------------------------------------------------------

	-- POPULATE LOCATION TABLES -------------------------------------------------------
		-- #RAW_LOCATION --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': #RAW_LOCATION'
		RAISERROR(@print,10,1) WITH NOWAIT

		IF OBJECT_ID('tempdb..#RAW_LOCATION') IS NOT NULL
			DROP TABLE #RAW_LOCATION;
		CREATE TABLE #RAW_LOCATION
		(
			City_Text nvarchar(255) NULL,
			State_Text nvarchar(255) NULL,
			Country_Text nvarchar(255) NULL,
			PostalCode_Text nvarchar(255) NULL,
			Area_Text nvarchar(255) NULL,
			Region_Text nvarchar(255) NULL
		)
		INSERT INTO #RAW_LOCATION(City_Text,State_Text,Country_Text,PostalCode_Text,Area_Text,Region_Text)
		SELECT DISTINCT [customerCity],[customerState],[customerCountry],[customerPostalCode],[customerArea],[customerRegion]
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
			UNION
		SELECT DISTINCT travelAgencyCity,travelAgencyState,travelAgencyCountry,travelAgencyPostalCode,travelAgencyArea,travelAgencyRegion
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)

		-- Reservations.[synxis].[City] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[City]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[City](City_Text)
		SELECT DISTINCT City_Text FROM #RAW_LOCATION
		WHERE City_Text IS NOT NULL
			EXCEPT
		SELECT City_Text FROM Reservations.[synxis].[City]

		-- Reservations.[synxis].[State] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[State]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[State](State_Text)
		SELECT DISTINCT State_Text FROM #RAW_LOCATION
		WHERE State_Text IS NOT NULL
			EXCEPT
		SELECT State_Text FROM Reservations.[synxis].[State]


		-- Reservations.[synxis].[Country] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Country]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[Country](Country_Text)
		SELECT DISTINCT Country_Text FROM #RAW_LOCATION
		WHERE Country_Text IS NOT NULL
			EXCEPT
		SELECT Country_Text FROM Reservations.[synxis].[Country]


		-- Reservations.[synxis].[PostalCode] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[PostalCode]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[PostalCode](PostalCode_Text)
		SELECT DISTINCT PostalCode_Text FROM #RAW_LOCATION
		WHERE PostalCode_Text IS NOT NULL
			EXCEPT
		SELECT PostalCode_Text FROM Reservations.[synxis].[PostalCode]


		-- Reservations.[synxis].[Area] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Area]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[Area](Area_Text)
		SELECT DISTINCT Area_Text FROM #RAW_LOCATION
		WHERE Area_Text IS NOT NULL
			EXCEPT
		SELECT Area_Text FROM Reservations.[synxis].[Area]


		-- Reservations.[synxis].[Region] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Region]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[Region](Region_Text)
		SELECT DISTINCT Region_Text FROM #RAW_LOCATION
		WHERE Region_Text IS NOT NULL
			EXCEPT
		SELECT Region_Text FROM Reservations.[synxis].[Region]


		-- Reservations.[synxis].[Location] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Location]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[Location](CityID,StateID,CountryID,PostalCodeID,AreaID,RegionID)
		SELECT DISTINCT ci.CityID,st.StateID,co.CountryID,p.PostalCodeID,a.AreaID,r.RegionID
		FROM #RAW_LOCATION s
			LEFT JOIN Reservations.[synxis].[City] ci ON ci.City_Text = s.City_Text
			LEFT JOIN Reservations.[synxis].[State] st ON st.State_Text = s.State_Text
			LEFT JOIN Reservations.[synxis].[Country] co ON co.Country_Text = s.Country_Text
			LEFT JOIN Reservations.[synxis].[PostalCode] p ON p.PostalCode_Text = s.PostalCode_Text
			LEFT JOIN Reservations.[synxis].[Area] a ON a.Area_Text = s.Area_Text
			LEFT JOIN Reservations.[synxis].[Region] r ON r.Region_Text = s.Region_Text
			EXCEPT
		SELECT CityID,StateID,CountryID,PostalCodeID,AreaID,RegionID
		FROM Reservations.[synxis].[Location]


	CREATE TABLE #LOCATION
	(
		LocationID int NOT NULL,
		City_Text nvarchar(255) NOT NULL,
		State_Text nvarchar(255) NOT NULL,
		Country_Text nvarchar(255) NOT NULL,
		PostalCode_Text nvarchar(255) NOT NULL,
		Area_Text nvarchar(255) NOT NULL,
		Region_Text nvarchar(255) NOT NULL
	)
	INSERT INTO #LOCATION(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text,Area_Text,Region_Text)
	SELECT DISTINCT l.LocationID,ISNULL(ci.City_Text,'NULL'),ISNULL(st.State_Text,'NULL'),ISNULL(co.Country_Text,'NULL'),ISNULL(p.PostalCode_Text,'NULL'),ISNULL(a.Area_Text,'NULL'),ISNULL(r.Region_Text,'NULL')
	FROM  Reservations.[synxis].[Location] l
		LEFT JOIN Reservations.[synxis].[City] ci ON ci.CityID = l.CityID
		LEFT JOIN Reservations.[synxis].[State] st ON st.StateID = l.StateID
		LEFT JOIN Reservations.[synxis].[Country] co ON co.CountryID = l.CountryID
		LEFT JOIN Reservations.[synxis].[PostalCode] p ON p.PostalCodeID = l.PostalCodeID
		LEFT JOIN Reservations.[synxis].[Area] a ON a.AreaID = l.AreaID
		LEFT JOIN Reservations.[synxis].[Region] r ON r.RegionID = l.RegionID

	-----------------------------------------------------------------------------------

	-- [synxis].[Guest_CompanyName] ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Guest_CompanyName]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[Guest_CompanyName](CompanyName)
		SELECT DISTINCT customerCompanyName
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE customerCompanyName IS NOT NULL
			EXCEPT
		SELECT CompanyName FROM Reservations.[synxis].[Guest_CompanyName]
	-----------------------------------------------------------------------------------

	-- [synxis].[Guest_EmailAddress] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Guest_EmailAddress]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[synxis].[Guest_EmailAddress](emailAddress)
		SELECT DISTINCT customerEmail
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE customerEmail IS NOT NULL
			EXCEPT
		SELECT emailAddress FROM Reservations.[synxis].[Guest_EmailAddress]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[TravelAgent] --------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[TravelAgent]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[TravelAgent](IATANumberID,Name,Address1,Address2,LocationID,Phone,Fax,Email)
		SELECT DISTINCT i.IATANumberID,rd.travelAgencyName,rd.travelAgencyAddress1,rd.travelAgencyAddress2,gloc.LocationID,rd.travelAgencyPhone,rd.travelAgencyFax,rd.travelAgencyEmail
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) rd
			LEFT JOIN #LOCATION gLoc ON gLoc.City_Text = ISNULL(rd.travelAgencyCity,'NULL')
									AND gLoc.State_Text = ISNULL(rd.travelAgencyState,'NULL')
									AND gLoc.Country_Text = ISNULL(rd.travelAgencyCountry,'NULL')
									AND gLoc.PostalCode_Text = ISNULL(rd.travelAgencyPostalCode,'NULL')
									AND gLoc.Area_Text = ISNULL(rd.travelAgencyArea,'NULL')
									AND gLoc.Region_Text = ISNULL(rd.travelAgencyRegion,'NULL')
			LEFT JOIN Reservations.[synxis].[IATANumber] i ON i.IATANumber = rd.IATANumber
			EXCEPT
		SELECT IATANumberID,Name,Address1,Address2,LocationID,Phone,Fax,Email
		FROM Reservations.[synxis].[TravelAgent]
	-----------------------------------------------------------------------------------

	-- CREATE TEMP TABLE & POPULATE ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': CREATE TEMP TABLE & POPULATE'
		RAISERROR(@print,10,1) WITH NOWAIT
		

	IF OBJECT_ID('tempdb..#SYNXIS') IS NOT NULL
		DROP TABLE #SYNXIS;

	CREATE TABLE #SYNXIS
	(
		QueueID int,
		[itineraryNumber] [nvarchar](18) NULL,
		[transactionTimeStamp] [datetime] NULL,
		[channelConnectConfirmationNumber] [nvarchar](40) NULL,
		[timeLoaded] [datetime] NULL,

		[IATANumber] varchar(50),
		[billingDescription] nvarchar(255),
		[channel] nvarchar(50),
		[secondarySource] nvarchar(50),
		[subSource] nvarchar(100),
		[subSourceCode] nvarchar(100),
		[CROCode] nvarchar(20),
		[xbeTemplateName] nvarchar(255),
		[xbeShellName] nvarchar(255),
		[ChainID] varchar(120),
		[consortiaName] nvarchar(60),
		[corporationCode] nvarchar(20),
		[HotelId] varchar(255),
		[HotelName] varchar(255),
		[HotelCode] varchar(255),
		[loyaltyNumber] nvarchar(50),
		[loyaltyProgram] nvarchar(50),
		[promotionalCode] nvarchar(20),
		[rateCategoryName] nvarchar(255),
		[rateCategoryCode] nvarchar(20),
		[PMSRateTypeCode] nvarchar(200),
		[RateTypeName] varchar(450),
		[RateTypeCode] varchar(450),
		[roomTypeName] nvarchar(80),
		[roomTypeCode] nvarchar(50),
		[PMSRoomTypeCode] nvarchar(20),
		[userName] nvarchar(142),
		[vipLevel] nvarchar(50),
		[customerID] [nvarchar](40) NULL,
		[salutation] [nvarchar](160) NULL,
		[FirstName] [nvarchar](255) NULL,
		[LastName] [nvarchar](255) NULL,
		[Address1] [nvarchar](255) NULL,
		[Address2] [nvarchar](255) NULL,
		Guest_LocationID int,
		[phone] [varchar](200) NULL,
		[Guest_EmailAddressID] int,
		[Guest_CompanyNameID] int,
		[Guest_hashKey] AS (hashbytes('MD5',(((((((((((((((upper(ltrim(rtrim(isnull([customerID],''))))+'|')+upper(ltrim(rtrim(isnull([salutation],'')))))+'|')+upper(ltrim(rtrim(isnull([FirstName],'')))))+'|')+upper(ltrim(rtrim(isnull([LastName],'')))))+'|')+upper(ltrim(rtrim(isnull([Address1],'')))))+'|')+upper(ltrim(rtrim(isnull([Address2],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),Guest_LocationID,(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([phone],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),[Guest_EmailAddressID],(0)),'')))))) PERSISTED,

		[confirmationNumber] [nvarchar](20) NOT NULL,
		[nights] [int] NULL,
		[averageDailyRate] [decimal](38,2) NULL,
		[rooms] [int] NULL,
		[reservationRevenue] [decimal](38,2) NULL,
		[currency] [nvarchar](12) NULL,
		[totalPackageRevenue] [decimal](18,2) NULL,
		[totalGuestCount] [int] NULL,
		[adultCount] [int] NULL,
		[childrenCount] [int] NULL,
		[secondaryCurrency] [nvarchar](12) NULL,
		[secondaryCurrencyExchangeRate] [decimal](10,5) NULL,
		[secondaryCurrencyAverageDailyRate] [decimal](18,2) NULL,
		[secondaryCurrencyReservationRevenue] [decimal](18,2) NULL,
		[secondaryCurrencyPackageRevenue] [decimal](18,2) NULL,
		[commisionPercent] [decimal](8,5) NULL,
		[arrivalDate] [date] NOT NULL,
		[departureDate] [date] NULL,
		[bookingLeadTime] [int] NULL,
		[arrivalDOW] tinyint NULL,
		[departureDOW] tinyint NULL,
		[TransactionDetail_hashKey] AS (hashbytes('MD5',(((((((((((((((((((((((((((((((((((((upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[nights],(0)),''))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[averageDailyRate],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[rooms],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[reservationRevenue],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([currency],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[totalPackageRevenue],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[totalGuestCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[adultCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[childrenCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([secondaryCurrency],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[secondaryCurrencyExchangeRate],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[secondaryCurrencyAverageDailyRate],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[secondaryCurrencyReservationRevenue],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[secondaryCurrencyPackageRevenue],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[commisionPercent],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[arrivalDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[departureDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[bookingLeadTime],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[arrivalDOW],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[departureDOW],(0)),'')))))) PERSISTED,

		--[confirmationNumber] [nvarchar](20) NOT NULL,
		[SAPID] [nvarchar](10) NULL,
		[faxNotificationCount] [nvarchar](60) NULL,
		[marketSourceCode] [nvarchar](20) NULL,
		[marketSegmentCode] [nvarchar](12) NULL,
		[shareWith] [nchar](1) NULL,
		[membershipNumber] [nvarchar](20) NULL,
		[consortiaCount] [int] NULL,
		[optIn] [nvarchar](5) NULL,
		[profileTypeSelection] [nvarchar](50) NULL,
		[IsPrimaryGuest] [bit] NULL,
		[TransactionsExtended_hashKey]  AS (hashbytes('MD5',(((((((((((((((((upper(ltrim(rtrim(isnull([SAPID],''))))+'|')+upper(ltrim(rtrim(isnull([faxNotificationCount],'')))))+'|')+upper(ltrim(rtrim(isnull([marketSourceCode],'')))))+'|')+upper(ltrim(rtrim(isnull([marketSegmentCode],'')))))+'|')+upper(ltrim(rtrim(isnull([shareWith],'')))))+'|')+upper(ltrim(rtrim(isnull([membershipNumber],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),[consortiaCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([optIn],'')))))+'|')+upper(ltrim(rtrim(isnull([profileTypeSelection],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),[IsPrimaryGuest],(0)),'')))))) PERSISTED,

		--[confirmationNumber] [nvarchar](20) NOT NULL,
		[status] [nvarchar](10) NOT NULL,
		[confirmationDate] [date] NULL,
		[cancellationNumber] [nvarchar](20) NULL,
		[cancellationDate] [date] NULL,
		[creditCardType] [nvarchar](40) NULL,
		[ActionTypeID] [int] NOT NULL,
		[TransactionStatus_hashKey]  AS (hashbytes('MD5',(((((((((upper(ltrim(rtrim(isnull([status],''))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[confirmationDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull([cancellationNumber],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[cancellationDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull([creditCardType],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[ActionTypeID],(0)),'')))))) PERSISTED,

		[IATANumberID] [int] NULL,
		[travelAgencyName] [nvarchar](450) NULL,
		[travelAgencyAddress1] [nvarchar](120) NULL,
		[travelAgencyAddress2] [nvarchar](120) NULL,
		TravelAgent_LocationID int,
		[travelAgencyPhone] [nvarchar](25) NULL,
		[travelAgencyFax] [nvarchar](25) NULL,
		[travelAgencyEmail] [nvarchar](255) NULL,
		[TravelAgent_hashKey]  AS (hashbytes('MD5',(((((((((((((upper(ltrim(rtrim(isnull(CONVERT([varchar](20),[IATANumberID],(0)),''))))+'|')+upper(ltrim(rtrim(isnull([travelAgencyName],'')))))+'|')+upper(ltrim(rtrim(isnull([travelAgencyAddress1],'')))))+'|')+upper(ltrim(rtrim(isnull([travelAgencyAddress2],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),TravelAgent_LocationID,(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([travelAgencyPhone],'')))))+'|')+upper(ltrim(rtrim(isnull([travelAgencyFax],'')))))+'|')+upper(ltrim(rtrim(isnull([travelAgencyEmail],'')))))) PERSISTED
	)

	INSERT INTO #SYNXIS(QueueID,timeLoaded,itineraryNumber,transactionTimeStamp,channelConnectConfirmationNumber,
						[IATANumber],[billingDescription],[channel],[secondarySource],[subSource],[subSourceCode],[CROCode],[xbeTemplateName],[xbeShellName],[ChainID],[consortiaName],
						[corporationCode],[HotelId],[HotelName],[HotelCode],[loyaltyNumber],[loyaltyProgram],[promotionalCode],[rateCategoryName],
						[rateCategoryCode],[PMSRateTypeCode],[RateTypeName],[RateTypeCode],[roomTypeName],[roomTypeCode],[PMSRoomTypeCode],[userName],[vipLevel],
						customerID,salutation,FirstName,LastName,Address1,Address2,Guest_LocationID,
						phone,
						[Guest_CompanyNameID],[Guest_EmailAddressID],
						IsPrimaryGuest,
						confirmationNumber,nights,averageDailyRate,rooms,reservationRevenue,currency,totalPackageRevenue,
						totalGuestCount,adultCount,childrenCount,secondaryCurrency,secondaryCurrencyExchangeRate,
						secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,secondaryCurrencyPackageRevenue,commisionPercent,
						SAPID,faxNotificationCount,marketSourceCode,marketSegmentCode,shareWith,membershipNumber,consortiaCount,optIn,profileTypeSelection,
						[status],confirmationDate,cancellationNumber,cancellationDate,arrivalDate,departureDate,bookingLeadTime,
						creditCardType,
						arrivalDOW,
						departureDOW,
						ActionTypeID,
						IATANumberID,travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,TravelAgent_LocationID,
						travelAgencyPhone,travelAgencyFax,
						travelAgencyEmail
						)
	SELECT DISTINCT s.QueueID,s.timeLoaded,
			s.itineraryNumber,s.transactionTimeStamp,s.channelConnectConfirmationNumber,
			s.[IATANumber],s.billingDescription,s.channel,s.secondarySource,s.subSource,s.subSourceCode,s.CROCode,s.xbeTemplateName,s.xbeShellName,s.chainID,s.consortiaName,
			s.corporationCode,s.hotelID,s.hotelName,s.hotelCode,s.loyaltyNumber,s.loyaltyProgram,s.promotionalCode,s.rateCategoryName,
			s.rateCategoryCode,s.PMSRateTypeCode,s.rateTypeName,s.rateTypeCode,s.roomTypeName,s.roomTypeCode,s.PMSRoomTypeCode,s.userName,s.vipLevel,
			s.[customerID],s.[salutation],s.[guestFirstName],s.[guestLastName],s.[customerAddress1],s.[customerAddress2],gloc.LocationID,
			s.[customerPhone],
			gcn.Guest_CompanyNameID,gea.Guest_EmailAddressID,
			CASE s.[primaryGuest] WHEN 'y' THEN 1 WHEN 'n' THEN 0 ELSE CONVERT(bit,s.[primaryGuest]) END AS IsPrimaryGuest,
			s.[confirmationNumber],CONVERT(int,s.[nights]),CONVERT(decimal(38,2),s.[averageDailyRate]),CONVERT(int,s.[rooms]),CONVERT(decimal(38,2),s.[reservationRevenue]),s.[currency],s.[totalPackageRevenue],
			s.[totalGuestCount],s.[adultCount],s.[childrenCount],s.[secondaryCurrency],s.[secondaryCurrencyExchangeRate],
			s.[secondaryCurrencyAverageDailyRate],s.[secondaryCurrencyReservationRevenue],s.[secondaryCurrencyPackageRevenue],s.[commisionPercent],
			s.[SAPID],s.[faxNotificationCount],s.[marketSourceCode],s.[marketSegmentCode],s.[shareWith],s.[membershipNumber],s.consortiaCount,s.optIn,s.profileTypeSelection,
			s.[status],s.[confirmationDate],s.[cancellationNumber],s.[cancellationDate],s.[arrivalDate],s.[departureDate],s.[bookingLeadTime],
			s.[creditCardType],
			CASE s.[arrivalDOW] WHEN 'Mon' THEN 2 WHEN 'Tue' THEN 3 WHEN 'Wed' THEN 4 WHEN 'Thu' THEN 5 WHEN 'Fri' THEN 6 WHEN 'Sat' THEN 7 WHEN 'Sun' THEN 1 END,
			CASE s.[departureDOW] WHEN 'Mon' THEN 2 WHEN 'Tue' THEN 3 WHEN 'Wed' THEN 4 WHEN 'Thu' THEN 5 WHEN 'Fri' THEN 6 WHEN 'Sat' THEN 7 WHEN 'Sun' THEN 1 END,
			at.ActionTypeID,
			i.IATANumberID,s.travelAgencyName,s.travelAgencyAddress1,s.travelAgencyAddress2,tLoc.LocationID,
			s.travelAgencyPhone,s.travelAgencyFax,
			s.travelAgencyEmail
	FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		LEFT JOIN Reservations.synXis.[ActionType] at ON ISNULL(at.actionType,'NULL') = ISNULL(s.ActionType,'NULL')
		LEFT JOIN Reservations.[synxis].[IATANumber] i ON i.IATANumber = s.IATANumber
		LEFT JOIN #LOCATION gLoc ON gLoc.City_Text = ISNULL(s.customerCity,'NULL')
									AND gLoc.State_Text = ISNULL(s.customerState,'NULL')
									AND gLoc.Country_Text = ISNULL(s.customerCountry,'NULL')
									AND gLoc.PostalCode_Text = ISNULL(s.customerPostalCode,'NULL')
									AND gLoc.Area_Text = ISNULL(s.customerArea,'NULL')
									AND gLoc.Region_Text = ISNULL(s.customerRegion,'NULL')

		LEFT JOIN #LOCATION tLoc ON tLoc.City_Text = ISNULL(s.travelAgencyCity,'NULL')
									AND tLoc.State_Text = ISNULL(s.travelAgencyState,'NULL')
									AND tLoc.Country_Text = ISNULL(s.travelAgencyCountry,'NULL')
									AND tLoc.PostalCode_Text = ISNULL(s.travelAgencyPostalCode,'NULL')
									AND tLoc.Area_Text = ISNULL(s.travelAgencyArea,'NULL')
									AND tLoc.Region_Text = ISNULL(s.travelAgencyRegion,'NULL')
		LEFT JOIN Reservations.[synxis].[Guest_CompanyName] gcn ON gcn.CompanyName = s.customerCompanyName
		LEFT JOIN Reservations.[synxis].[Guest_EmailAddress] gea ON gea.emailAddress = s.customerEmail
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[BillingDescription] -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[BillingDescription]'
		RAISERROR(@print,10,1) WITH NOWAIT

		
		INSERT INTO Reservations.[synxis].[BillingDescription](billingDescription)
		SELECT DISTINCT billingDescription
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE billingDescription IS NOT NULL
			EXCEPT
		SELECT billingDescription FROM Reservations.[synxis].[BillingDescription]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[Channel] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Channel]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[Channel](channel)
		SELECT DISTINCT channel
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE channel IS NOT NULL
			EXCEPT
		SELECT channel FROM Reservations.[synxis].[Channel]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[SecondarySource] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[SecondarySource]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[SecondarySource](secondarySource)
		SELECT DISTINCT secondarySource
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE secondarySource IS NOT NULL
			EXCEPT
		SELECT secondarySource FROM Reservations.[synxis].[SecondarySource]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[SubSource] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[SubSource]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[SubSource](subSource,subSourceCode)
		SELECT subSource,subSourceCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE subSource IS NOT NULL
		GROUP BY subSource,subSourceCode
			EXCEPT
		SELECT subSource,subSourceCode FROM Reservations.[synxis].[SubSource]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[CROCode] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[CROCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[CROCode](CROCode)
		SELECT DISTINCT CROCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE CROCode IS NOT NULL
			EXCEPT
		SELECT CROCode FROM Reservations.[synxis].[CROCode]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[xbeTemplate] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[xbeTemplate]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[xbeTemplate](xbeTemplateName,xbeShellName)
		SELECT xbeTemplateName,xbeShellName
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE xbeTemplateName IS NOT NULL
		GROUP BY xbeTemplateName,xbeShellName
			EXCEPT
		SELECT xbeTemplateName,xbeShellName FROM Reservations.[synxis].[xbeTemplate]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[BookingSource] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[BookingSource]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[BookingSource](ChannelID,SecondarySourceID,SubSourceID,CROCodeID,xbeTemplateID)
		SELECT DISTINCT c.ChannelID,sec.SecondarySourceID,sub.SubSourceID,cro.CROCodeID,x.xbeTemplateID
		FROM  [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
			LEFT JOIN Reservations.[synxis].Channel c ON c.channel = s.channel
			LEFT JOIN Reservations.[synxis].SecondarySource sec ON sec.secondarySource = s.secondarySource
			LEFT JOIN Reservations.[synxis].SubSource sub ON sub.subSource = s.subSource AND sub.subSourceCode = s.subSourceCode
			LEFT JOIN Reservations.[synxis].CROCode cro ON cro.CROCode = s.CROCode
			LEFT JOIN Reservations.[synxis].xbeTemplate x ON x.xbeTemplateName = s.xbeTemplateName AND x.xbeTemplateName = s.xbeTemplateName
			EXCEPT
		SELECT ChannelID,SecondarySourceID,SubSourceID,CROCodeID,xbeTemplateID
		FROM Reservations.[synxis].[BookingSource]
	-----------------------------------------------------------------------------------
	
	-- Reservations.[synxis].[Chain] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Chain]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[Chain](ChainName,ChainID)
		SELECT MAX(ChainName),ChainID
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE ChainID IS NOT NULL
		GROUP BY ChainID
			EXCEPT
		SELECT ChainName,ChainID FROM Reservations.[synxis].[Chain]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[Consortia] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Consortia]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[Consortia]([consortiaName])
		SELECT DISTINCT consortiaName
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE consortiaName IS NOT NULL
			EXCEPT
		SELECT [consortiaName] FROM Reservations.[synxis].[Consortia]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[CoroporateCode] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[CorporateCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[CorporateCode](corporationCode)
		SELECT DISTINCT corporationCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE corporationCode IS NOT NULL
			EXCEPT
		SELECT corporationCode FROM Reservations.[synxis].[CorporateCode]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[Guest] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Guest]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[Guest](customerID,salutation,FirstName,LastName,Address1,Address2,LocationID,phone,Guest_EmailAddressID)
		SELECT DISTINCT s.customerID,s.salutation,s.FirstName,s.LastName,s.Address1,s.Address2,s.Guest_LocationID,s.phone,s.Guest_EmailAddressID
		FROM #SYNXIS s
		WHERE [Guest_hashKey] NOT IN(SELECT hashKey FROM Reservations.[synxis].[Guest])
		--	EXCEPT
		--SELECT customerID,salutation,FirstName,LastName,Address1,Address2,LocationID,phone,Guest_EmailAddressID
		--FROM Reservations.[synxis].[Guest]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[hotel] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[hotel]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[hotel](HotelId,HotelName,HotelCode)
		SELECT s.HotelId,MAX(s.HotelName),s.HotelCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		GROUP BY s.HotelId,s.HotelCode
			EXCEPT
		SELECT HotelId,HotelName,HotelCode FROM Reservations.[synxis].[hotel]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[LoyaltyNumber] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[LoyaltyNumber]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[LoyaltyNumber](loyaltyNumber)
		SELECT DISTINCT s.loyaltyNumber
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		WHERE s.loyaltyNumber IS NOT NULL
			EXCEPT
		SELECT loyaltyNumber FROM Reservations.[synxis].[LoyaltyNumber]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[LoyaltyProgram] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[LoyaltyProgram]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[LoyaltyProgram](loyaltyProgram)
		SELECT DISTINCT s.loyaltyProgram
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		WHERE s.loyaltyProgram IS NOT NULL
			EXCEPT
		SELECT loyaltyProgram FROM Reservations.[synxis].[LoyaltyProgram]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[PromoCode] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[PromoCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[PromoCode]([promotionalCode])
		SELECT DISTINCT [promotionalCode]
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE [promotionalCode] IS NOT NULL
			EXCEPT
		SELECT [promotionalCode] FROM Reservations.[synxis].[PromoCode]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[RateCategory] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[RateCategory]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[RateCategory](rateCategoryName,rateCategoryCode)
		SELECT s.rateCategoryName,s.rateCategoryCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		WHERE s.rateCategoryCode IS NOT NULL
		GROUP BY s.rateCategoryName,s.rateCategoryCode
			EXCEPT
		SELECT rateCategoryName,rateCategoryCode FROM Reservations.[synxis].[RateCategory]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[PMSRateTypeCode] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[PMSRateTypeCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[PMSRateTypeCode](PMSRateTypeCode)
		SELECT DISTINCT PMSRateTypeCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE PMSRateTypeCode IS NOT NULL
			EXCEPT
		SELECT PMSRateTypeCode FROM Reservations.[synxis].[PMSRateTypeCode]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[RateTypeCode] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[RateTypeCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[RateTypeCode](RateTypeName,RateTypeCode)
		SELECT s.RateTypeName,s.RateTypeCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		WHERE s.RateTypeCode IS NOT NULL
		GROUP BY s.RateTypeName,s.RateTypeCode
			EXCEPT
		SELECT RateTypeName,RateTypeCode FROM Reservations.[synxis].[RateTypeCode]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[RoomType] -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[RoomType]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[RoomType](roomTypeName,roomTypeCode,PMSRoomTypeCode)
		SELECT s.roomTypeName,s.roomTypeCode,s.PMSRoomTypeCode
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		GROUP BY s.roomTypeName,s.roomTypeCode,s.PMSRoomTypeCode
			EXCEPT
		SELECT roomTypeName,roomTypeCode,PMSRoomTypeCode FROM Reservations.[synxis].[RoomType]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[UserName] -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[UserName]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[UserName](userName)
		SELECT DISTINCT userName
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE userName IS NOT NULL
			EXCEPT
		SELECT userName FROM Reservations.[synxis].[UserName]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[VIP_Level] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[VIP_Level]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[VIP_Level](vipLevel)
		SELECT DISTINCT s.vipLevel
		FROM [synxis].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		WHERE s.vipLevel IS NOT NULL
			EXCEPT
		SELECT vipLevel FROM Reservations.[synxis].[VIP_Level]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[TransactionDetail] -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[TransactionDetail]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[TransactionDetail](confirmationNumber,nights,averageDailyRate,rooms,reservationRevenue,currency,totalPackageRevenue,
																totalGuestCount,adultCount,childrenCount,secondaryCurrency,secondaryCurrencyExchangeRate,
																secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,
																secondaryCurrencyPackageRevenue,commisionPercent,
																[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],[departureDOW])
		SELECT DISTINCT s.confirmationNumber,s.nights,s.averageDailyRate,s.rooms,s.reservationRevenue,s.currency,s.totalPackageRevenue,
						s.totalGuestCount,s.adultCount,s.childrenCount,s.secondaryCurrency,s.secondaryCurrencyExchangeRate,
						s.secondaryCurrencyAverageDailyRate,s.secondaryCurrencyReservationRevenue,
						s.secondaryCurrencyPackageRevenue,s.commisionPercent,
						s.arrivalDate,s.departureDate,s.bookingLeadTime,s.arrivalDOW,s.departureDOW
		FROM #SYNXIS s
			EXCEPT
		SELECT confirmationNumber,nights,averageDailyRate,rooms,reservationRevenue,currency,totalPackageRevenue,
				totalGuestCount,adultCount,childrenCount,secondaryCurrency,secondaryCurrencyExchangeRate,
				secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,
				secondaryCurrencyPackageRevenue,commisionPercent,
				[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],[departureDOW]
		FROM Reservations.[synxis].[TransactionDetail]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[TransactionsExtended] -----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[TransactionsExtended]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[TransactionsExtended](confirmationNumber,SAPID,faxNotificationCount,marketSourceCode,marketSegmentCode,shareWith,membershipNumber,consortiaCount,optIn,profileTypeSelection,IsPrimaryGuest)
		SELECT DISTINCT s.confirmationNumber,s.SAPID,s.faxNotificationCount,s.marketSourceCode,s.marketSegmentCode,s.shareWith,s.membershipNumber,s.consortiaCount,s.optIn,s.profileTypeSelection,s.IsPrimaryGuest
		FROM #SYNXIS s
			EXCEPT
		SELECT confirmationNumber,SAPID,faxNotificationCount,marketSourceCode,marketSegmentCode,shareWith,membershipNumber,consortiaCount,optIn,profileTypeSelection,IsPrimaryGuest
		FROM Reservations.[synxis].[TransactionsExtended]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[TransactionStatus] --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[TransactionStatus]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[synxis].[TransactionStatus](confirmationNumber,[status],confirmationDate,cancellationNumber,cancellationDate,creditCardType,ActionTypeID)
		SELECT DISTINCT s.confirmationNumber,s.[status],s.confirmationDate,s.cancellationNumber,s.cancellationDate,s.creditCardType,s.ActionTypeID
		FROM #SYNXIS s
			EXCEPT
		SELECT confirmationNumber,[status],confirmationDate,cancellationNumber,cancellationDate,creditCardType,ActionTypeID
		FROM Reservations.[synxis].[TransactionStatus]
	-----------------------------------------------------------------------------------

	-- Reservations.[synxis].[Transactions] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[synxis].[Transactions]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		;WITH cte_bookingSource([BookingSourceID],[channel],[secondarySource],[subSource],[subSourceCode],[CROCode],[xbeTemplateName],[xbeShellName])
		AS
		(
			SELECT bs.BookingSourceID,ch.channel,ss.secondarySource,sub.subSource,sub.subSourceCode,cro.CROCode,x.xbeTemplateName,x.xbeShellName
			FROM Reservations.[synxis].[BookingSource] bs
				LEFT JOIN Reservations.[synxis].[Channel] ch ON ch.ChannelID = bs.ChannelID
				LEFT JOIN Reservations.[synxis].[SecondarySource] ss ON ss.SecondarySourceID = bs.SecondarySourceID
				LEFT JOIN Reservations.[synxis].[SubSource] sub ON sub.SubSourceID = bs.SubSourceID
				LEFT JOIN Reservations.[synxis].[CROCode] cro ON cro.CROCodeID = bs.CROCodeID
				LEFT JOIN Reservations.[synxis].[xbeTemplate] x ON x.xbeTemplateID = bs.xbeTemplateID
		),cte_Transactions
		AS
		(
			SELECT DISTINCT x.QueueID,x.timeLoaded,
							x.itineraryNumber,x.confirmationNumber,x.transactionTimeStamp,x.channelConnectConfirmationNumber,
							ts.TransactionStatusID,tr.TransactionDetailID,te.TransactionsExtendedID,
							vip.VIP_LevelID,cc.CorporateCodeID,con.ConsortiaID,rom.RoomTypeID,ratc.RateCategoryID,usr.UserNameID,bil.BillingDescriptionID,
							bok.BookingSourceID,loy.LoyaltyProgramID,loyn.LoyaltyNumberID,gst.GuestID,rtc.RateTypeCodeID,pro.PromoCodeID,tra.TravelAgentID,iat.IATANumberID,
							hot.intHotelID,chn.intChainID,pms.PMSRateTypeCodeID,x.[Guest_CompanyNameID]
			FROM #SYNXIS x
				LEFT JOIN Reservations.[synxis].[TransactionStatus] ts ON ts.confirmationNumber = x.confirmationNumber AND ts.hashKey = x.TransactionStatus_hashKey
				LEFT JOIN Reservations.[synxis].[TransactionDetail] tr ON tr.confirmationNumber = x.confirmationNumber AND tr.hashKey = x.TransactionDetail_hashKey
				LEFT JOIN Reservations.[synxis].[TransactionsExtended] te ON te.confirmationNumber = x.confirmationNumber AND te.hashKey = x.TransactionsExtended_hashKey
				LEFT JOIN Reservations.[synxis].[VIP_Level] vip ON vip.vipLevel = x.vipLevel
				LEFT JOIN Reservations.[synxis].[CorporateCode] cc ON cc.corporationCode = x.corporationCode
				LEFT JOIN Reservations.[synxis].[Consortia] con ON con.consortiaName = x.consortiaName
				LEFT JOIN Reservations.[synxis].[RoomType] rom ON rom.roomTypeCode = x.roomTypeCode AND rom.PMSRoomTypeCode = x.PMSRoomTypeCode AND rom.roomTypeName = x.roomTypeName
				LEFT JOIN Reservations.[synxis].[RateCategory] ratc ON ratc.rateCategoryCode = x.rateCategoryCode AND ratc.rateCategoryName = x.rateCategoryName
				LEFT JOIN Reservations.[synxis].[UserName] usr ON usr.userName = x.userName
				LEFT JOIN Reservations.[synxis].[BillingDescription] bil ON bil.billingDescription = x.billingDescription
				LEFT JOIN cte_BookingSource bok ON bok.channel = x.channel AND bok.CROCode = x.CROCode AND bok.secondarySource = x.secondarySource AND bok.subSource = x.subSource AND bok.subSourceCode = x.subSourceCode AND bok.xbeTemplateName = x.xbeTemplateName AND bok.xbeShellName = x.xbeShellName
				LEFT JOIN Reservations.[synxis].[LoyaltyProgram] loy ON loy.loyaltyProgram = x.loyaltyProgram
				LEFT JOIN Reservations.[synxis].[LoyaltyNumber] loyn ON loyn.loyaltyNumber = x.loyaltyNumber
				LEFT JOIN Reservations.[synxis].[Guest] gst ON gst.Guest_EmailAddressID = x.[Guest_EmailAddressID] AND gst.hashKey = x.[Guest_hashKey]
				LEFT JOIN Reservations.[synxis].[RateTypeCode] rtc ON rtc.RateTypeCode = x.RateTypeCode AND rtc.RateTypeName = x.RateTypeName
				LEFT JOIN Reservations.[synxis].[PromoCode] pro ON pro.promotionalCode = x.promotionalCode
				LEFT JOIN Reservations.[synxis].[TravelAgent] tra ON tra.hashKey = x.TravelAgent_hashKey
				LEFT JOIN Reservations.[synxis].[IATANumber] iat ON iat.IATANumber = x.IATANumber
				LEFT JOIN Reservations.[synxis].[hotel] hot ON hot.HotelCode = x.HotelCode AND hot.HotelId = x.HotelId
				LEFT JOIN Reservations.[synxis].[Chain] chn ON chn.ChainID = x.ChainID
				LEFT JOIN Reservations.[synxis].[PMSRateTypeCode] pms ON pms.PMSRateTypeCode = x.PMSRateTypeCode
		)
		INSERT INTO Reservations.[synxis].[Transactions](subQueueID,QueueID,itineraryNumber,confirmationNumber,transactionTimeStamp,channelConnectConfirmationNumber,timeLoaded,
														TransactionStatusID,TransactionDetailID,TransactionsExtendedID,
														VIP_LevelID,CorporateCodeID,ConsortiaID,RoomTypeID,RateCategoryID,UserNameID,BillingDescriptionID,
														BookingSourceID,LoyaltyProgramID,LoyaltyNumberID,GuestID,RateTypeCodeID,PromoCodeID,TravelAgentID,IATANumberID,
														intHotelID,intChainID,PMSRateTypeCodeID,[Guest_CompanyNameID])
		SELECT 0,QueueID,itineraryNumber,confirmationNumber,transactionTimeStamp,channelConnectConfirmationNumber,timeLoaded,
				TransactionStatusID,TransactionDetailID,TransactionsExtendedID,
				VIP_LevelID,CorporateCodeID,ConsortiaID,RoomTypeID,RateCategoryID,UserNameID,BillingDescriptionID,
				BookingSourceID,LoyaltyProgramID,LoyaltyNumberID,GuestID,RateTypeCodeID,PromoCodeID,TravelAgentID,IATANumberID,
				intHotelID,intChainID,PMSRateTypeCodeID,[Guest_CompanyNameID]
		FROM cte_Transactions
			EXCEPT
		SELECT subQueueID,QueueID,itineraryNumber,confirmationNumber,transactionTimeStamp,channelConnectConfirmationNumber,timeLoaded,
				TransactionStatusID,TransactionDetailID,TransactionsExtendedID,
				VIP_LevelID,CorporateCodeID,ConsortiaID,RoomTypeID,RateCategoryID,UserNameID,BillingDescriptionID,
				BookingSourceID,LoyaltyProgramID,LoyaltyNumberID,GuestID,RateTypeCodeID,PromoCodeID,TravelAgentID,IATANumberID,
				intHotelID,intChainID,PMSRateTypeCodeID,[Guest_CompanyNameID]
		FROM Reservations.[synxis].[Transactions]
	-----------------------------------------------------------------------------------

	-- POPULATE MRT -------------------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Reservations.[synxis].[MostRecentTransactions]'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC [synxis].[PopulateMostRecentTransactions] @QueueID,@startDate,@endDate
	-----------------------------------------------------------------------------------

	-- PRINT STATUS --
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [openHosp].[LoadTransactions]'
GO




ALTER PROCEDURE [openHosp].[LoadTransactions]
	@QueueID int = NULL,
	@startDate date = NULL,
	@endDate date = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	-- ADD ACTION TYPE ----------------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.openHosp.ActionType'
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO Reservations.openHosp.ActionType(actionType,actionTypeOrder)
		SELECT DISTINCT ActionType,CASE ActionType WHEN 'N' THEN 1 WHEN 'M' THEN 2 WHEN 'X' THEN 4 WHEN 'NX' THEN 3 ELSE 0 END
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			EXCEPT
		SELECT actionType,actionTypeOrder FROM Reservations.openHosp.ActionType
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[IATANumber] ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[IATANumber]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[IATANumber](IATANumber)
		SELECT DISTINCT IATANumber
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE IATANumber IS NOT NULL
			EXCEPT
		SELECT IATANumber FROM Reservations.[openHosp].[IATANumber]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[BookingSource] ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[BookingSource]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[BookingSource](Bkg_Src_Cd,Booking_Source_Name)
		SELECT DISTINCT Bkg_Src_Cd,Booking_Source_Name
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			EXCEPT
		SELECT Bkg_Src_Cd,Booking_Source_Name FROM Reservations.[openHosp].[BookingSource]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[CorpInfoCode] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[CorpInfoCode]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[CorpInfoCode](CorpInfoCode)
		SELECT DISTINCT CorpInfoCode
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			EXCEPT
		SELECT CorpInfoCode FROM Reservations.[openHosp].[CorpInfoCode]
	-----------------------------------------------------------------------------------

	-- POPULATE LOCATION TABLES -------------------------------------------------------
	
		-- Reservations.[openHosp].[City] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[City]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[City](City_Text)
		SELECT City_Text
		FROM
			(
				SELECT DISTINCT [customerCity] AS City_Text FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			) x
		WHERE City_Text IS NOT NULL
			EXCEPT
		SELECT City_Text FROM Reservations.[openHosp].[City]


		-- Reservations.[openHosp].[State] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[State]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[State](State_Text)
		SELECT State_Text
		FROM
			(
				SELECT DISTINCT [customerState] AS State_Text FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			) x
		WHERE State_Text IS NOT NULL
			EXCEPT
		SELECT State_Text FROM Reservations.[openHosp].[State]


		-- Reservations.[openHosp].[Country] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[Country]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[Country](Country_Text)
		SELECT Country_Text
		FROM
			(
				SELECT DISTINCT [customerCountry] AS Country_Text FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			) x
		WHERE Country_Text IS NOT NULL
			EXCEPT
		SELECT Country_Text FROM Reservations.[openHosp].[Country]


		-- Reservations.[openHosp].[PostalCode] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[PostalCode]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[PostalCode](PostalCode_Text)
		SELECT PostalCode_Text
		FROM
			(
				SELECT DISTINCT [customerPostalCode] AS PostalCode_Text FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
			) x
		WHERE PostalCode_Text IS NOT NULL
			EXCEPT
		SELECT PostalCode_Text FROM Reservations.[openHosp].[PostalCode]


		-- Reservations.[openHosp].[Location] --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[Location]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[Location](CityID,StateID,CountryID,PostalCodeID)
		SELECT DISTINCT ci.CityID,st.StateID,co.CountryID,p.PostalCodeID
		FROM  [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate) s
			LEFT JOIN Reservations.[openHosp].[City] ci ON ci.City_Text = s.[customerCity]
			LEFT JOIN Reservations.[openHosp].[State] st ON st.State_Text = s.[customerState]
			LEFT JOIN Reservations.[openHosp].[Country] co ON co.Country_Text = s.[customerCountry]
			LEFT JOIN Reservations.[openHosp].[PostalCode] p ON p.PostalCode_Text = s.[customerPostalCode]
			EXCEPT
		SELECT CityID,StateID,CountryID,PostalCodeID FROM Reservations.[openHosp].[Location]

	CREATE TABLE #LOCATION
	(
		LocationID int NOT NULL,
		City_Text nvarchar(255) NOT NULL,
		State_Text nvarchar(255) NOT NULL,
		Country_Text nvarchar(255) NOT NULL,
		PostalCode_Text nvarchar(255) NOT NULL,
	)
	INSERT INTO #LOCATION(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text)
	SELECT DISTINCT l.LocationID,ISNULL(ci.City_Text,'NULL'),ISNULL(st.State_Text,'NULL'),ISNULL(co.Country_Text,'NULL'),ISNULL(p.PostalCode_Text,'NULL')
	FROM  Reservations.[openHosp].[Location] l
		LEFT JOIN Reservations.[openHosp].[City] ci ON ci.CityID = l.CityID
		LEFT JOIN Reservations.[openHosp].[State] st ON st.StateID = l.StateID
		LEFT JOIN Reservations.[openHosp].[Country] co ON co.CountryID = l.CountryID
		LEFT JOIN Reservations.[openHosp].[PostalCode] p ON p.PostalCodeID = l.PostalCodeID
	-----------------------------------------------------------------------------------

	-- [openHosp].[Guest_EmailAddress] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[Guest_EmailAddress]'
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO Reservations.[openHosp].[Guest_EmailAddress](emailAddress)
		SELECT DISTINCT customerEmail
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE customerEmail IS NOT NULL
			EXCEPT
		SELECT emailAddress FROM Reservations.[openHosp].[Guest_EmailAddress]
	-----------------------------------------------------------------------------------

	-- CREATE TEMP TABLE & POPULATE ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': CREATE TEMP TABLE & POPULATE'
		RAISERROR(@print,10,1) WITH NOWAIT
		

	IF OBJECT_ID('tempdb..#openHosp') IS NOT NULL
		DROP TABLE #openHosp;

	CREATE TABLE #openHosp
	(
		QueueID int,
		[transactionTimeStamp] [datetime] NULL,
		[timeLoaded] [datetime] NULL,

		[IATANumber] varchar(50),
		[ChainID] varchar(120),
		[HotelId] varchar(255),
		[HotelName] varchar(255),
		[HotelCode] varchar(255),
		[promotionalCode] nvarchar(20),
		[RateTypeName] varchar(450),
		[RateTypeCode] varchar(450),

		[FirstName] [nvarchar](255) NULL,
		[LastName] [nvarchar](255) NULL,
		[Address1] [nvarchar](255) NULL,
		Guest_LocationID int,
		[phone] [varchar](200) NULL,
		[Guest_EmailAddressID] int,
		[Guest_hashKey] AS (hashbytes('MD5',((((((((((upper(ltrim(rtrim(isnull([FirstName],'')))))+'|')+upper(ltrim(rtrim(isnull([LastName],'')))))+'|')+upper(ltrim(rtrim(isnull([Address1],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),Guest_LocationID,(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([phone],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](20),[Guest_EmailAddressID],(0)),'')))))) PERSISTED,


		[confirmationNumber] [nvarchar](20) NOT NULL,
		[nights] [int] NULL,
		[rooms] [int] NULL,
		[reservationRevenue] [decimal](38,2) NULL,
		[currency] [nvarchar](12) NULL,
		[totalGuestCount] [int] NULL,
		[adultCount] [int] NULL,
		[childrenCount] [int] NULL,
		[arrivalDate] [date] NOT NULL,
		[departureDate] [date] NULL,
		[TransactionDetail_hashKey] AS (hashbytes('MD5',(((((((((((((((upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[nights],(0)),''))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[rooms],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[reservationRevenue],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull([currency],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[totalGuestCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[adultCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[childrenCount],(0)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[arrivalDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[departureDate],(112)),'')))))) PERSISTED,

		--[confirmationNumber] [nvarchar](20) NOT NULL,
		[status] [nvarchar](10) NOT NULL,
		[confirmationDate] [date] NULL,
		[cancellationNumber] [nvarchar](20) NULL,
		[cancellationDate] [date] NULL,
		[creditCardType] [nvarchar](40) NULL,
		[ActionTypeID] [int] NOT NULL,
		[TransactionStatus_hashKey]  AS (hashbytes('MD5',(((((((((upper(ltrim(rtrim(isnull([status],''))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[confirmationDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull([cancellationNumber],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[cancellationDate],(112)),'')))))+'|')+upper(ltrim(rtrim(isnull([creditCardType],'')))))+'|')+upper(ltrim(rtrim(isnull(CONVERT([varchar](50),[ActionTypeID],(0)),'')))))) PERSISTED,

		[IATANumberID] [int] NULL,
		CorpInfoCode [nvarchar](20),
		[Bkg_Src_Cd] [varchar](500),
		[Booking_Source_Name] [varchar](500)
	)

	INSERT INTO #openHosp(QueueID,transactionTimeStamp,timeLoaded,[IATANumber],[ChainID],[HotelId],[HotelName],[HotelCode],[promotionalCode],
							[RateTypeName],[RateTypeCode],FirstName,LastName,Address1,Guest_LocationID,phone,[Guest_EmailAddressID],
							confirmationNumber,nights,rooms,reservationRevenue,currency,totalGuestCount,
							adultCount,childrenCount,arrivalDate,departureDate,[status],confirmationDate,cancellationNumber,cancellationDate,
							creditCardType,ActionTypeID,IATANumberID,CorpInfoCode,Bkg_Src_Cd,Booking_Source_Name
						
					)
	SELECT DISTINCT s.QueueID,s.transactionTimeStamp,s.TimeLoaded,s.[IATANumber],s.chainID,s.hotelID,s.hotelName,s.hotelCode,s.PromoCode,
			s.rateTypeName,s.rateTypeCode,s.[guestFirstName],s.[guestLastName],s.[customerAddress1],gloc.LocationID,s.[customerPhone],gea.Guest_EmailAddressID,
			s.[confirmationNumber],CONVERT(int,s.[nights]),CONVERT(int,s.[rooms]),CONVERT(decimal(38,2),s.[reservationRevenue]),s.[currency],s.[totalGuestCount],
			s.[adultCount],s.[childrenCount],s.[arrivalDate],s.[departureDate],s.[status],s.[confirmationDate],s.[cancellationNumber],s.[cancellationDate],
			s.[creditCardType],at.ActionTypeID,i.IATANumberID,s.CorpInfoCode,s.Bkg_Src_Cd,s.Booking_Source_Name
	FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		LEFT JOIN Reservations.openHosp.[ActionType] at ON ISNULL(at.actionType,'NULL') = ISNULL(s.ActionType,'NULL')
		LEFT JOIN Reservations.[openHosp].[IATANumber] i ON i.IATANumber = s.IATANumber
		LEFT JOIN #LOCATION gLoc ON gLoc.City_Text = ISNULL(s.customerCity,'NULL')
									AND gLoc.State_Text = ISNULL(s.customerState,'NULL')
									AND gLoc.Country_Text = ISNULL(s.customerCountry,'NULL')
									AND gLoc.PostalCode_Text = ISNULL(s.customerPostalCode,'NULL')
		LEFT JOIN Reservations.[openHosp].[Guest_EmailAddress] gea ON gea.emailAddress = s.customerEmail
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[Chain] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[Chain]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[Chain](ChainName,ChainID)
		SELECT MAX(ChainName),ChainID
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE ChainID IS NOT NULL
		GROUP BY ChainID
			EXCEPT
		SELECT ChainName,ChainID FROM Reservations.[openHosp].[Chain]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[Guest] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[Guest]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[Guest](FirstName,LastName,Address1,LocationID,phone,Guest_EmailAddressID)
		SELECT DISTINCT s.FirstName,s.LastName,s.Address1,s.Guest_LocationID,s.phone,s.Guest_EmailAddressID
		FROM #openHosp s
			EXCEPT
		SELECT FirstName,LastName,Address1,LocationID,phone,Guest_EmailAddressID
		FROM Reservations.[openHosp].[Guest]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[hotel] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[hotel]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
				
		INSERT INTO Reservations.[openHosp].[hotel](HotelId,HotelName,HotelCode)
		SELECT s.HotelId,MAX(s.HotelName),s.HotelCode
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		GROUP BY s.HotelId,s.HotelCode
			EXCEPT
		SELECT HotelId,HotelName,HotelCode FROM Reservations.[openHosp].[hotel]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[PromoCode] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[PromoCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[PromoCode]([promotionalCode])
		SELECT DISTINCT [PromoCode]
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate)
		WHERE [PromoCode] IS NOT NULL
			EXCEPT
		SELECT [promotionalCode] FROM Reservations.[openHosp].[PromoCode]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[RateTypeCode] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[RateTypeCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[RateTypeCode](RateTypeName,RateTypeCode)
		SELECT s.RateTypeName,s.RateTypeCode
		FROM [openHosp].[RawDataForLoading](@QueueID,@startDate,@endDate) s
		WHERE s.RateTypeCode IS NOT NULL
		GROUP BY s.RateTypeName,s.RateTypeCode
			EXCEPT
		SELECT RateTypeName,RateTypeCode FROM Reservations.[openHosp].[RateTypeCode]
	-----------------------------------------------------------------------------------


	-- Reservations.[openHosp].[TransactionDetail] -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[TransactionDetail]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[TransactionDetail](confirmationNumber,nights,rooms,reservationRevenue,currency,
																totalGuestCount,adultCount,childrenCount,
																[arrivalDate],[departureDate])
		SELECT DISTINCT s.confirmationNumber,s.nights,s.rooms,s.reservationRevenue,s.currency,
						s.totalGuestCount,s.adultCount,s.childrenCount,
						s.arrivalDate,s.departureDate
		FROM #openHosp s
			EXCEPT
		SELECT confirmationNumber,nights,rooms,reservationRevenue,currency,totalGuestCount,adultCount,childrenCount,[arrivalDate],[departureDate]
		FROM Reservations.[openHosp].[TransactionDetail]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[TransactionStatus] --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[TransactionStatus]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		
		INSERT INTO Reservations.[openHosp].[TransactionStatus](confirmationNumber,[status],confirmationDate,cancellationNumber,cancellationDate,creditCardType,ActionTypeID)
		SELECT DISTINCT s.confirmationNumber,s.[status],s.confirmationDate,s.cancellationNumber,s.cancellationDate,s.creditCardType,s.ActionTypeID
		FROM #openHosp s
			EXCEPT
		SELECT confirmationNumber,[status],confirmationDate,cancellationNumber,cancellationDate,creditCardType,ActionTypeID
		FROM Reservations.[openHosp].[TransactionStatus]
	-----------------------------------------------------------------------------------

	-- Reservations.[openHosp].[Transactions] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Reservations.[openHosp].[Transactions]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		;WITH cte_Transactions
		AS
		(
			SELECT DISTINCT x.QueueID,
							x.confirmationNumber,x.transactionTimeStamp,
							ts.TransactionStatusID,tr.TransactionDetailID,
							cc.CorpInfoCodeID,
							gst.GuestID,rtc.RateTypeCodeID,pro.PromoCodeID,iat.IATANumberID,
							hot.intHotelID,chn.intChainID,b.BookingSourceID
			FROM #openHosp x
				LEFT JOIN Reservations.[openHosp].[TransactionStatus] ts ON ts.confirmationNumber = x.confirmationNumber AND ts.hashKey = x.TransactionStatus_hashKey
				LEFT JOIN Reservations.[openHosp].[TransactionDetail] tr ON tr.confirmationNumber = x.confirmationNumber AND tr.hashKey = x.TransactionDetail_hashKey
				LEFT JOIN Reservations.[openHosp].[CorpInfoCode] cc ON cc.CorpInfoCode = x.CorpInfoCode
				LEFT JOIN Reservations.[openHosp].[Guest] gst ON gst.Guest_EmailAddressID = x.[Guest_EmailAddressID] AND gst.hashKey = x.[Guest_hashKey]
				LEFT JOIN Reservations.[openHosp].[RateTypeCode] rtc ON rtc.RateTypeCode = x.RateTypeCode AND rtc.RateTypeName = x.RateTypeName
				LEFT JOIN Reservations.[openHosp].[PromoCode] pro ON pro.promotionalCode = x.promotionalCode
				LEFT JOIN Reservations.[openHosp].[IATANumber] iat ON iat.IATANumber = x.IATANumber
				LEFT JOIN Reservations.[openHosp].[hotel] hot ON hot.HotelCode = x.HotelCode AND hot.HotelId = x.HotelId
				LEFT JOIN Reservations.[openHosp].[Chain] chn ON chn.ChainID = x.ChainID
				LEFT JOIN Reservations.[openHosp].[BookingSource] b ON b.Bkg_Src_Cd = x.Bkg_Src_Cd AND b.Booking_Source_Name = x.Booking_Source_Name
		)
		INSERT INTO Reservations.[openHosp].[Transactions](QueueID,confirmationNumber,transactionTimeStamp,timeLoaded,
														TransactionStatusID,TransactionDetailID,
														CorpInfoCodeID,
														BookingSourceID,GuestID,RateTypeCodeID,PromoCodeID,IATANumberID,
														intHotelID,intChainID)
		SELECT QueueID,confirmationNumber,transactionTimeStamp,GETDATE(),
				TransactionStatusID,TransactionDetailID,
				CorpInfoCodeID,
				BookingSourceID,GuestID,RateTypeCodeID,PromoCodeID,IATANumberID,
				intHotelID,intChainID
		FROM cte_Transactions
			EXCEPT
		SELECT QueueID,confirmationNumber,transactionTimeStamp,timeLoaded,TransactionStatusID,TransactionDetailID,CorpInfoCodeID,BookingSourceID,GuestID,RateTypeCodeID,PromoCodeID,IATANumberID,intHotelID,intChainID
		FROM Reservations.[openHosp].[Transactions]
	-----------------------------------------------------------------------------------

	-- POPULATE MRT -------------------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Reservations.[openHosp].[MostRecentTransactions]'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC [openHosp].[PopulateMostRecentTransactions]
	-----------------------------------------------------------------------------------

	-- PRINT STATUS --
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[revenuePace]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-05-25
-- Description:	Pace report for PH revenue
-- =============================================
CREATE PROCEDURE [rpt].[revenuePace]
	-- Add the parameters for the stored procedure here
@startDate date, @endDate date, @asOfDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

DECLARE @lyStart date = dateadd(year,-1,@startDate), @lyEnd date = dateadd(year,-1,@endDate), @lyAsOfDate date = dateadd(year,-1,@asOfDate)


SELECT r.billableMonth
, ISNULL(r.reportingChannel, r.channel) as reportingChannel
, r.RD 
, r.hotelCode
, r.hotelName 
, r.reportingSecondarySource
, r.reportingSubSource
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 1 ELSE 0 END) as bookings
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 0 ELSE 1 END) as lyBookings
, SUM(CASE WHEN r.BillableDate >= @startDate THEN r.confirmedRoomNights ELSE 0 END) as roomNights
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 0 ELSE r.confirmedRoomNights END) as lyRoomNights
, SUM(CASE WHEN r.BillableDate >= @startDate THEN r.confirmedRevenueUSD ELSE 0 END) as roomRevenueUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 0 ELSE r.confirmedRevenueUSD END) as lyRoomRevenueUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN r.bookAndComm ELSE 0 END) as phReservationFeesUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 0 ELSE r.bookAndComm END) as lyPhReservationFeesUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN r.IPreferCharges ELSE 0 END) as ipreferFeesUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 0 ELSE r.IPreferCharges END) as lyIpreferFeesUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN r.surcharges ELSE 0 END) as surchargesUSD
, SUM(CASE WHEN r.BillableDate >= @startDate THEN 0 ELSE r.surcharges END) as lysurchargesUSD


FROM [Prof_Rpt].[FinalResults] r
WHERE (
	(billableDate BETWEEN @startDate AND @endDate)
	AND (confirmationDate <= @asOfDate
		OR confirmationDate IS NULL)
	AND (r.BookingStatus <> 'Cancelled'
		OR r.CancellationDate IS NULL
		OR r.CancellationDate >= @asOfDate
	)
)

OR (
	(billableDate BETWEEN @lyStart AND @lyEnd)
	AND (confirmationDate <= @lyAsOfDate
		OR confirmationDate IS NULL)
	AND (r.BookingStatus <> 'Cancelled'
		OR r.CancellationDate IS NULL
		OR r.CancellationDate >= @lyAsOfDate
	)
)

GROUP BY r.billableMonth
, ISNULL(r.reportingChannel, r.channel)
, r.RD 
, r.hotelCode
, r.hotelName 
, r.reportingSecondarySource
, r.reportingSubSource


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
