USE ReportGenerator
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReportGenerator    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReportGenerator

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 1:01:38 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetHotelsForCoOpMarketingScorecardDELETEME]'
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetHotelsForCoOpMarketingScorecardDELETEME]
	-- Add the parameters for the stored procedure here
	@start date,
	@finish date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DISTINCT hotels_synxis.hotelCode, hotels_synxis.synxisID as WildcardValue
    FROM Superset.dbo.mostRecentTransactions
    INNER JOIN Core.dbo.hotels_synxis ON mostRecentTransactions.hotelID = hotels_synxis.synxisID
    WHERE arrivalDate BETWEEN @start AND @finish
	AND hotels_Synxis.hotelCode not in
	(
		SELECT HotelCode  
		FROM [ReportGenerator].[dbo].[ReportLog]
		WHERE (ReportType like '%marketing%'
		OR ReportType = 'Co-op Marketing Scorecard')
		AND YEAR(GETDATE()) = YEAR(RunDate)
		AND MONTH(GETDATE()) = MONTH(RunDate)
	)
	ORDER BY hotels_synxis.hotelCode
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[split2]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[split2]
(
  @delimited NVARCHAR(MAX),
  @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
  DECLARE @xml XML
  SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'

  INSERT INTO @t(val)
  SELECT  r.value('.','varchar(MAX)') as item
  FROM  @xml.nodes('/t') as records(r)
  RETURN

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[split]'
GO
CREATE FUNCTION [dbo].[split](
  @delimited NVARCHAR(MAX),
  @delimiter NVARCHAR(100)
) RETURNS @t TABLE (id INT IDENTITY(1,1), val NVARCHAR(MAX))
AS
BEGIN
  DECLARE @xml XML
  SET @xml = N'<t>' + REPLACE(@delimited,@delimiter,'</t><t>') + '</t>'

  INSERT INTO @t(val)
  SELECT  r.value('.','varchar(MAX)') as item
  FROM  @xml.nodes('/t') as records(r)
  RETURN
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreachRunAdHoc]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BreachRunAdHoc]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT synxisID as WildcardValue, code as hotelCode
  FROM [ITOPS_SUPPORT].[dbo].[hotelbrands]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BreachURunAdHoc]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BreachURunAdHoc]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT synxisID as WildCardValue, code as HotelCode
    FROM [ITOPS_SUPPORT].[dbo].[hotelbrands]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetHotelsForCreditMemos]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetHotelsForCreditMemos]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select rtrim(LTRIM(CUSTNMBR)) as HotelCode, rtrim(LTRIM(SOPNUMBE)) as WILDCARDVALUE
	FROM CHISQP01.IC.dbo.sop30200
	WHERE BACHNUMB = 'FCM Credits'
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetHotelsForIpreferBackbill]'
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetHotelsForIpreferBackbill]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT DISTINCT
hr.code  as hotelCode
FROM ITOPS_SUPPORT.dbo.bsiSuperset_20180712 bs
JOIN COre.dbo.hotelsReporting hr
	ON bs.hotel_id = hr.synxisID

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breachHEassistance]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[breachHEassistance]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT DISTINCT h.synxisID as WildcardValue, h.code as hotelCode
  FROM [ITOPS_SUPPORT].[dbo].epiqHE e
  INNER JOIN Core.dbo.hotelsReporting h
	ON e.hotel_ID = h.synxisID
WHERE 
 e.Status IN ('','Invalid')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breachHEmailed]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[breachHEmailed]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT DISTINCT h.synxisID as WildcardValue, h.code as hotelCode
  FROM [ITOPS_SUPPORT].[dbo].epiqHE e
  INNER JOIN Core.dbo.hotelsReporting h
	ON e.hotel_ID = h.synxisID
WHERE 
 e.Status IN ('Notice Sent')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breachPHassistance]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[breachPHassistance]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT DISTINCT h.synxisID as WildcardValue, h.code as hotelCode
  FROM [ITOPS_SUPPORT].[dbo].epiqPH e
  INNER JOIN Core.dbo.hotelsReporting h
	ON e.hotel_ID = h.synxisID
WHERE 
 e.Status IN ('','Invalid')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[breachPHmailed]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[breachPHmailed]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  SELECT DISTINCT h.synxisID as WildcardValue, h.code as hotelCode
  FROM [ITOPS_SUPPORT].[dbo].epiqPH e
  INNER JOIN Core.dbo.hotelsReporting h
	ON e.hotel_ID = h.synxisID
WHERE 
 e.Status IN ('Notice Sent')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
