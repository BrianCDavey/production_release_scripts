USE Dimensional_Warehouse
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 12:14:59 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ProductionDashboardRatePlan_ORG]'
GO
DROP PROCEDURE [rpt].[ProductionDashboardRatePlan_ORG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[billingControlTotals]'
GO
DROP PROCEDURE [rpt].[billingControlTotals]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest]'
GO


ALTER PROCEDURE [dbo].[Populate_Guest]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_Guest([firstName],[lastName],[fullName],[emailAddress],
					[emailDomain],
					[guestAddress],[guestCity],[guestSubState],[guestState],[guestCountry],
					[guestPostalCode],Latitude,Longitude,[phLoyaltyNumber],[phLoyaltyStatus],[phLoyaltyTier],[gender],sourceKey)
	AS
	(
		SELECT DISTINCT g.FirstName,g.LastName,g.FirstName + ' ' + g.LastName,ge.emailAddress,
				SUBSTRING(ge.emailAddress,CHARINDEX('@',ge.emailAddress) + 1,LEN(ge.emailAddress)),
				COALESCE(la.Address1,g.Address1,'') + ' ' + COALESCE(la.Address2,g.Address2,'') + ' ' + COALESCE(la.Address3,''),COALESCE(lcty.City_Text, cty.City_Text,'Unknown'),'Unknown',COALESCE(lst.State_Text,st.State_Text,'Unknown'),COALESCE(lcntry.Country_Text,cntry.Country_Text,'Unknown'),
				COALESCE(lpc.PostalCode_Text,pc.PostalCode_Text,'Unknown'),ISNULL(la.Latitude, 0),ISNULL(la.Longitude,0),ISNULL(ln.loyaltyNumber,'Unknown'),'Unknown','Unknown','Unknown',g.GuestID
		FROM Reservations.dbo.Guest g
			LEFT JOIN Reservations.dbo.Transactions trn ON trn.GuestID = g.GuestID
			LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = trn.LoyaltyNumberID
			LEFT JOIN Reservations.dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
			LEFT JOIN Reservations.dbo.[Location] l ON l.LocationID = g.LocationID
			LEFT JOIN Reservations.dbo.City cty ON cty.CityID = l.CityID
			LEFT JOIN Reservations.dbo.[State] st ON st.StateID = l.StateID
			LEFT JOIN Reservations.dbo.Country cntry ON cntry.CountryID = l.CountryID
			LEFT JOIN Reservations.dbo.PostalCode pc ON pc.PostalCodeID = l.PostalCodeID
			LEFT JOIN Reservations.dbo.Region reg ON reg.RegionID = l.RegionID
			LEFT JOIN Locations.dbo.[Address] la ON g.location_AddressID = la.AddressID
			LEFT JOIN Locations.dbo.[Location] ll ON l.LocationID = la.LocationID
			LEFT JOIN Locations.dbo.City lcty ON lcty.CityID = ll.CityID
			LEFT JOIN Locations.dbo.[State] lst ON lst.StateID = ll.StateID
			LEFT JOIN Locations.dbo.Country lcntry ON lcntry.CountryID = ll.CountryID
			LEFT JOIN Locations.dbo.PostalCode lpc ON lpc.PostalCodeID = ll.PostalCodeID


			EXCEPT
		SELECT [First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],0,0,[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],[sourceKey]
		FROM [dim].[Guest]
	),
	cte_Final([firstName],[lastName],[fullName],[emailAddress],[emailDomain],[guestAddress],[guestCity],[guestSubState],[guestState],[guestCountry],[guestPostalCode],Latitude,Longitude,[phLoyaltyNumber],[phLoyaltyStatus],[phLoyaltyTier],[gender],sourceKey)
	AS
	(
		SELECT MAX([firstName]),MAX([lastName]),MAX([fullName]),MAX([emailAddress]),MAX([emailDomain]),MAX([guestAddress]),
			MAX([guestCity]),MAX([guestSubState]),MAX([guestState]),MAX([guestCountry]),MAX([guestPostalCode]),MAX(Latitude),
			MAX(Longitude),MAX([phLoyaltyNumber]),MAX([phLoyaltyStatus]),MAX([phLoyaltyTier]),MAX([gender]),MAX(sourceKey)
		FROM cte_Guest
		GROUP BY sourceKey
	)
	MERGE INTO [dim].[Guest] AS tgt
	USING
	(
		SELECT ISNULL([firstName],'Unknown') AS [First Name],ISNULL([lastName],'Unknown') AS [Last Name],ISNULL([fullName],'Unknown') AS [Full Name],ISNULL([emailAddress],'Unknown') AS [Email Address],
			ISNULL([emailDomain],'Unknown') AS [Email Domain],ISNULL([guestAddress],'Unknown') AS [Guest Address],ISNULL([guestCity],'Unknown') AS [Guest City],
			ISNULL([guestSubState],'Unknown') AS [Guest Sub State],ISNULL([guestState],'Unknown') AS [Guest State],ISNULL([guestCountry],'Unknown') AS [Guest Country],
			ISNULL([guestPostalCode],'Unknown') AS [Guest Postal Code],geography::Point(Latitude,Longitude,4326) AS [Guest GeoCode],
			ISNULL([phLoyaltyNumber],'Unknown') AS [PH Loyalty Number],ISNULL([phLoyaltyStatus],'Unknown') AS [PH Loyalty Status],ISNULL([phLoyaltyTier],'Unknown') AS [PHLoyalty Tier],
			ISNULL([gender],'Unknown') AS [Gender],sourceKey
		FROM cte_Final
	) AS src ON src.[sourceKey] = tgt.[sourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [First Name] = src.[First Name],
				[Last Name] = src.[Last Name],
				[Full Name] = src.[Full Name],
				[Email Address] = src.[Email Address],
				[Email Domain] = src.[Email Domain],
				[Guest Address] = src.[Guest Address],
				[Guest City] = src.[Guest City],
				[Guest Sub State] = src.[Guest Sub State],
				[Guest State] = src.[Guest State],
				[Guest Country] = src.[Guest Country],
				[Guest Postal Code] = src.[Guest Postal Code],
				[Guest GeoCode] = src.[Guest GeoCode],
				[PH Loyalty Number] = src.[PH Loyalty Number],
				[PH Loyalty Status] = src.[PH Loyalty Status],
				[PHLoyalty Tier] = src.[PHLoyalty Tier],
				[Gender] = src.[Gender]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],[sourceKey])
		VALUES([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],[sourceKey])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[Halo_Scorecard]'
GO

ALTER procedure [rpt].[Halo_Scorecard]
	@hotelcode varchar(10),
	@YTD datetime

AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--set @hotelcode = 'BOSHA'
--set @YTD = GETDATE()


DECLARE @SOY DATE = CAST('1-1-' + STR(YEAR(@YTD)) AS DATE)

--get all rate codes from CRM where the rate is attached to a marketing opportunity that is included in the ROI guarantee and the program year requested
;with marketingRates AS (
SELECT	DISTINCT TRIM(P.PHG_RateCode) as rateCode
FROM	Hotels.dbo.Hotel AS H
		JOIN LocalCRM.dbo.Opportunity AS O
			ON H.CRM_HotelID = O.customerid
		JOIN LocalCRM.dbo.opportunityproduct AS OP
			ON	O.opportunityid = OP.opportunityid
		JOIN LocalCRM.dbo.product AS P
			ON	OP.productid = P.productid
WHERE H.HotelCode = @hotelCode
AND O.phg_inroiguarantee = 1
AND P.phg_programyearname = YEAR(@YTD)
AND P.phg_ratecode NOT IN ('N/A','',' ','None')
),
--get all confirmation numbers from our marketing halo data (booking made not on our brand site that were driven by our marketing efforts)
halo as (
SELECT DISTINCT r.[Confirmation Number]
FROM Marketing.crafted.Conversion c
JOIN Dimensional_Warehouse.fact.Reservation r
	ON c.ConfirmationNumber = r.[Confirmation Number]
JOIN Dimensional_Warehouse.dim.DimDate cd
	ON r.[Confirmation Date] = cd.dateKey
WHERE cd.[Full Date] BETWEEN @SOY AND @YTD
),
--get all bookings where they are either in Halo or in Marketing Rate
bookings as (
SELECT	h.[Hotel Name]
,		r.[Confirmation Number]
,		r.[Room Nights]
,		r.[Revenue USD Confirmation Date]
,		rc.[Rate Code]
,		CASE bs.[PH Channel]
			WHEN 'Voice - Brand' THEN 'Voice'
			WHEN 'Voice - Hotel' THEN 'Voice'
			WHEN 'Voice - Call Gated' THEN 'Voice'
			ELSE bs.[PH Channel]
		END AS channel
,		CASE WHEN halo.[Confirmation Number] IS NULL THEN 0 ELSE 1 END AS isHalo
,		CASE WHEN mr.rateCode IS NULL THEN 0 ELSE 1 END AS isMarketingRate

FROM Dimensional_Warehouse.fact.Reservation r
JOIN Dimensional_Warehouse.dim.Rate rc
	ON r.rateKey = rc.rateKey
JOIN Dimensional_Warehouse.dim.BookingSource bs
	ON r.bookingSourceKey = bs.bookingSourceKey
JOIN Dimensional_Warehouse.dim.Hotel h
	ON r.hotelKey = h.hotelKey
JOIN Dimensional_Warehouse.dim.DimDate cd
	ON r.[Confirmation Date] = cd.dateKey
JOIN Dimensional_Warehouse.dim.BookingStatus stat
	ON r.bookingStatusKey = stat.bookingStatusKey
LEFT JOIN halo
	ON r.[Confirmation Number] = halo.[Confirmation Number]
LEFT JOIN marketingRates mr
	ON rc.[Rate Code] = mr.rateCode

WHERE	h.[Hotel Code] = @HotelCode
AND		cd.[Full Date] BETWEEN @SOY AND @YTD
AND		bs.[PH Channel] <> 'OTA Connect'
AND		stat.[Booking Status Name] <> 'Cancelled'
AND (halo.[Confirmation Number] IS NOT NULL OR mr.rateCode IS NOT NULL)
)

--split the bookings into their booked channel when marketing rate, or force channel "Halo" if halo
SELECT	b.[Hotel Name] AS HotelName
,		SUM(b.[Room Nights]) AS RoomNights
,		COUNT(b.[Confirmation Number]) AS Bookings
,		SUM(b.[Revenue USD Confirmation Date]) AS Revenue
,		b.channel AS Channel

		FROM	bookings as b

		WHERE	isMarketingRate = 1
		
		GROUP BY	[Hotel Name]
		,			channel

UNION ALL

SELECT	b.[Hotel Name] AS HotelName
,		SUM(b.[Room Nights]) AS RoomNights
,		COUNT(b.[Confirmation Number]) AS Bookings
,		SUM(b.[Revenue USD Confirmation Date]) AS Revenue
,		'Halo' AS channel

FROM	bookings as b

WHERE	isHalo = 1
		AND isMarketingRate = 0
		
GROUP BY	b.[Hotel Name]

ORDER BY RoomNights desc



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[RollingHaloRevenue]'
GO
-- =============================================
-- Author:		Jake Smith
-- Create date: August 28, 2019
-- Description:	Get Halo Revenue into the Rolling Mid Year GM Report
-- =============================================
CREATE PROCEDURE [rpt].[RollingHaloRevenue] 
	-- Add the parameters for the stored procedure here
 @startDate date
,@endDate  date
,@hotelCode varchar(250)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

;with marketingRates AS (
SELECT	DISTINCT TRIM(P.PHG_RateCode) as rateCode
,P.phg_programyearname as 'Year'
FROM	Hotels.dbo.Hotel AS H
		JOIN LocalCRM.dbo.Opportunity AS O
			ON H.CRM_HotelID = O.customerid
		JOIN LocalCRM.dbo.opportunityproduct AS OP
			ON	O.opportunityid = OP.opportunityid
		JOIN LocalCRM.dbo.product AS P
			ON	OP.productid = P.productid
WHERE H.HotelCode = @hotelCode
AND O.phg_inroiguarantee = 1
--AND P.phg_programyearname = YEAR(@startDate) --We need to figure out how to do this across the entire date range (multiple years, phg_programyearname)
AND P.phg_ratecode NOT IN ('N/A','',' ','None')
),
halo as (
SELECT DISTINCT r.[Confirmation Number]
FROM Marketing.crafted.Conversion c
JOIN Dimensional_Warehouse.fact.Reservation r
	ON c.ConfirmationNumber = r.[Confirmation Number]
JOIN Dimensional_Warehouse.dim.DimDate cd
	ON r.[Confirmation Date] = cd.dateKey
WHERE cd.[Full Date] BETWEEN @startDate AND @endDate
),
bookings as (
SELECT	h.[Hotel Name]
,		r.[Confirmation Number]
,		r.[Room Nights]
,		r.[Revenue USD Confirmation Date]
,		rc.[Rate Code]
,		r.[Length Of Stay]
,		cd.[Calendar Year]
,		CASE bs.[PH Channel]
			WHEN 'Voice - Brand' THEN 'Voice'
			WHEN 'Voice - Hotel' THEN 'Voice'
			WHEN 'Voice - Call Gated' THEN 'Voice'
			ELSE bs.[PH Channel]
		END AS channel
,		CASE WHEN halo.[Confirmation Number] IS NULL THEN 0 ELSE 1 END AS isHalo
,		CASE WHEN mr.rateCode IS NULL THEN 0 ELSE 1 END AS isMarketingRate

FROM Dimensional_Warehouse.fact.Reservation r
JOIN Dimensional_Warehouse.dim.Rate rc
	ON r.rateKey = rc.rateKey
JOIN Dimensional_Warehouse.dim.BookingSource bs
	ON r.bookingSourceKey = bs.bookingSourceKey
JOIN Dimensional_Warehouse.dim.Hotel h
	ON r.hotelKey = h.hotelKey
JOIN Dimensional_Warehouse.dim.DimDate cd
	ON r.[Confirmation Date] = cd.dateKey
JOIN Dimensional_Warehouse.dim.BookingStatus stat
	ON r.bookingStatusKey = stat.bookingStatusKey
LEFT JOIN halo
	ON r.[Confirmation Number] = halo.[Confirmation Number]
LEFT JOIN marketingRates mr
	ON rc.[Rate Code] = mr.rateCode and cd.[Calendar Year] = mr.Year


WHERE	h.[Hotel Code] = @hotelCode
AND		cd.[Full Date] BETWEEN @startDate AND @endDate
AND 
		bs.[PH Channel] <> 'OTA Connect'
AND		stat.[Booking Status Name] <> 'Cancelled'
AND (halo.[Confirmation Number] IS NOT NULL OR mr.rateCode IS NOT NULL)
)

SELECT	b.[Hotel Name] AS HotelName
,		SUM(b.[Room Nights]) AS RoomNights
,		COUNT(b.[Confirmation Number]) AS Bookings
,		CAST(AVG(b.[Length Of Stay]) as float) as LengthofStay

,		SUM(b.[Revenue USD Confirmation Date]) AS Revenue
,		b.channel AS Channel
,		b.[Calendar Year] as CalendarYear

		FROM	bookings as b

		WHERE	isMarketingRate = 1
		
		GROUP BY	[Hotel Name]
		,			[Calendar Year]
		,			channel



UNION ALL

SELECT	b.[Hotel Name] AS HotelName
,		SUM(b.[Room Nights]) AS RoomNights
,		COUNT(b.[Confirmation Number]) AS Bookings
,		CAST(AVG(b.[Length Of Stay]) as float) as LengthofStay
,		SUM(b.[Revenue USD Confirmation Date]) AS Revenue

,		'Halo' AS channel
,		b.[Calendar Year] as 'Year'



FROM	bookings as b

WHERE	isHalo = 1
		AND isMarketingRate = 0
		
GROUP BY	b.[Hotel Name]
,			b.[Calendar Year]
,			channel


ORDER BY RoomNights desc
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[RollupMidYearReport]'
GO
-- =============================================
-- Author:		Jake Smith
-- Create date: August 28, 2019
-- Description:	Provides the data for the dataset 'Dataset1' in the Mid-Year GM report.
-- =============================================
CREATE PROCEDURE [rpt].[RollupMidYearReport]
	-- Add the parameters for the stored procedure here
 @startDate date
,@endDate  date
,@hotelCode varchar(250)

AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT dt.[Calendar Year] as 'Year' 
,SUM(res.[Room Nights]) as RoomNights
, COUNT (res.[Confirmation Number]) as Reservations
, SUM(res.[Revenue USD Confirmation Date]) as Revenue
, SUM(res.[Revenue USD Confirmation Date])/SUM(CAST(res.[Room Nights] as decimal)) as ADR
, AVG (CAST (res.[Length Of Stay] as decimal)) as ALOS
, AVG (CAST (res.[Lead Time] as decimal)) as LeadTime
FROM fact.Reservation res 
 JOIN dim.Hotel ho on res.hotelKey = ho.hotelKey
 JOIN dim.BookingStatus bk on res.bookingStatusKey = bk.bookingStatusKey
 JOIN dim.DimDate dt on res.[Arrival Date] = dt.dateKey
WHERE ho.[Hotel Code] = @hotelCode
AND bk.bookingStatusKey = 1
AND dt.[Full Date] between @startDate and @endDate
GROUP BY dt.[Calendar Year]
ORDER BY dt.[Calendar Year] ASC

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Warehouse_ORG]'
GO

ALTER PROCEDURE [dbo].[Populate_Warehouse_ORG]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
/*
	EXEC dbo.AddZeroKeyNullRecords

	-- dim.BookingSource --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[BookingSource]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		;WITH cte_CRS(BookingSourceID,[Channel],[SecondarySource],[SubSource],[croCode],[croName],[ibeSourceName],[bookingSystem])
		AS
		(
			SELECT bs.BookingSourceID,c.channel,ss.secondarySource,sub.subSource,acro.CRO_Code,acro.CRO_Code,auth.ibeSourceName,
				CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN 'SynXis' ELSE 'Unknown' END
			FROM Reservations.dbo.CRS_BookingSource bs
				LEFT JOIN Reservations.dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
				LEFT JOIN Reservations.dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
				LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
				LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
				LEFT JOIN (SELECT CASE WHEN CRO_Code = '' THEN 'Unknown' ELSE CRO_Code END AS CRO_Code,CRO_CodeID FROM Reservations.authority.CRO_Code) acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
				LEFT JOIN Reservations.authority.ibeSource auth ON auth.ibeSourceID = ibe.auth_ibeSourceID
		),
		cte_PH(BookingSourceID,[Channel],[SecondarySource],[SubSource])
		AS
		(
			SELECT bs.PH_BookingSourceID,c.PH_Channel,ss.PH_SecondaryChannel,sub.PH_SubChannel
			FROM Reservations.dbo.PH_BookingSource bs
				LEFT JOIN Reservations.dbo.PH_Channel c ON c.PH_ChannelID = bs.PH_ChannelID
				LEFT JOIN Reservations.dbo.PH_SecondaryChannel ss ON ss.PH_SecondaryChannelID = bs.PH_SecondaryChannelID
				LEFT JOIN Reservations.dbo.PH_SubChannel sub ON sub.PH_SubChannelID = bs.PH_SubChannelID
		),
		cte_BS([phChannel],[phSecondarySource],[phSubSource],[bookingSystem],[crsChannel],[crsSecondarySource],[crsSubSource],[croCode],[croName],[ibeTemplate],CRS_sourceKey,PH_sourceKey)
		AS
		(
			SELECT ISNULL(ph.Channel,'Unknown'),ISNULL(ph.SecondarySource,'Unknown'),ISNULL(ph.SubSource,'Unknown'),crs.bookingSystem,
				ISNULL(crs.Channel,'Unknown'),ISNULL(crs.SecondarySource,'Unknown'),
				ISNULL(crs.SubSource,'Unknown'),ISNULL(crs.croCode,'Unknown'),
				ISNULL(crs.croName,'Unknown'),ISNULL(crs.ibeSourceName,'Unknown'),
				STRING_AGG(CONVERT(varchar(MAX),crs.BookingSourceID),','),STRING_AGG(CONVERT(varchar(MAX),ph.BookingSourceID),',')
			FROM (SELECT DISTINCT CRS_BookingSourceID,PH_BookingSourceID FROM Reservations.dbo.Transactions) t
				INNER JOIN cte_CRS crs ON crs.BookingSourceID = t.CRS_BookingSourceID
				LEFT JOIN cte_PH ph ON ph.BookingSourceID = t.PH_BookingSourceID
			WHERE crs.BookingSourceID IS NOT NULL
			GROUP BY ISNULL(ph.Channel,'Unknown'),ISNULL(ph.SecondarySource,'Unknown'),ISNULL(ph.SubSource,'Unknown'),crs.bookingSystem,
				ISNULL(crs.Channel,'Unknown'),ISNULL(crs.SecondarySource,'Unknown'),
				ISNULL(crs.SubSource,'Unknown'),ISNULL(crs.croCode,'Unknown'),
				ISNULL(crs.croName,'Unknown'),ISNULL(crs.ibeSourceName,'Unknown')
		)									
		INSERT INTO [dim].[BookingSource]([PH Channel],[phSecondarySource],[PH Sub Source],[Booking System],[CRS Channel],[CRS Secondary Source],[CRS Sub Source],[CRO Code],[CRO Name],[IBE Source],[CRS_sourceKey],[PH_sourceKey])
		SELECT [phChannel],[phSecondarySource],[phSubSource],[bookingSystem],[crsChannel],[crsSecondarySource],[crsSubSource],[croCode],[croName],[ibeTemplate],[CRS_sourceKey],[PH_sourceKey]
		FROM cte_BS
			EXCEPT
		SELECT [PH Channel],[phSecondarySource],[PH Sub Source],[Booking System],[CRS Channel],[CRS Secondary Source],[CRS Sub Source],[CRO Code],[CRO Name],[IBE Source],[CRS_sourceKey],[PH_sourceKey]
		FROM [dim].[BookingSource]
	-----------------------------------------------------------

	-- temp.BookingSource -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [temp].[BookingSource]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		TRUNCATE TABLE temp.BookingSource

		INSERT INTO temp.BookingSource(bookingSourceKey,[Booking System],CRS_sourceKey,PH_sourceKey)
		SELECT DISTINCT bookingSourceKey,[Booking System],crs.value AS CRS_sourceKey,ph.value AS PH_sourceKey
		FROM dim.BookingSource
			CROSS APPLY string_split(ISNULL(CRS_sourceKey,0),',') crs
			CROSS APPLY string_split(ISNULL(PH_sourceKey,0),',') ph
	-----------------------------------------------------------

	-- dim.BookingStatus --------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- dim.CorporateAccount -----------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.CRS_Chain ------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[CRS_Chain]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[CRS_Chain]([CRS Chain ID],[CRS Chain Name])
		SELECT DISTINCT c.CRS_ChainID,c.ChainName
		FROM Reservations.dbo.Chain c
			EXCEPT
		SELECT [CRS Chain ID],[CRS Chain Name] FROM [dim].[CRS_Chain]
	-----------------------------------------------------------

	-- dim.Currency -------------------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- POPULATE DimDate ---------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE DimDate'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		DECLARE @minDate date,@maxDate date
		SELECT @minDate = MIN(minDate),@maxDate = MAX(maxDate)
		FROM
			(
				SELECT MIN(confirmationDate) AS minDate,MAX(confirmationDate) AS maxDate FROM Reservations.dbo.TransactionStatus
					UNION ALL
				SELECT MIN(arrivalDate) AS minDate,MAX(arrivalDate) AS maxDate FROM Reservations.dbo.TransactionDetail
					UNION ALL
				SELECT MIN(departureDate) AS minDate,MAX(departureDate) AS maxDate FROM Reservations.dbo.TransactionDetail
			) x

		EXEC [dim].[Populate_DimDate_MinMax] @minDate,@maxDate;
	-----------------------------------------------------------

	-- fact.CurrencyExchange ----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [fact].[CurrencyExchange]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO [fact].[CurrencyExchange]([currencyKey],[DateKey],[USD Exchange Rate])
		SELECT DISTINCT cur.currencyKey,dt.DateKey,dr.toUSD
		FROM CurrencyRates.dbo.dailyRates dr
			INNER JOIN dim.Currency cur ON cur.[Currency Code] = dr.code
			INNER JOIN dim.DimDate dt ON dt.[Full Date] = dr.rateDate
			EXCEPT
		SELECT [currencyKey],[DateKey],[USD Exchange Rate] FROM [fact].[CurrencyExchange]
	-----------------------------------------------------------

	-- dim.DimDate --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[DimDate]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		DECLARE @dimDate date = DATEADD(month,6,GETDATE())
		EXEC [dim].[Populate_DimDate] @dimDate
	-----------------------------------------------------------

	-- dim.FormOfPayment --------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[FormOfPayment]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[FormOfPayment]([Form of Payment Code],[Form of Payment Name])
		SELECT DISTINCT creditCardType,creditCardType
		FROM Reservations.[dbo].[TransactionStatus] ts
			LEFT JOIN [dim].[FormOfPayment] fop ON fop.[Form of Payment Code] = ts.creditCardType
		WHERE fop.formOfPaymentKey IS NULL
			EXCEPT
		SELECT [Form of Payment Code],[Form of Payment Name] FROM [dim].[FormOfPayment]
	-----------------------------------------------------------

	-- dim.Guest ----------------------------------------
		-- NOT IN PHASE I: guestLatLong,phLoyaltyStatus,phLoyaltyTier,gender

		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[Guest]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		;WITH cte_Guest([firstName],[lastName],[fullName],[emailAddress],
						[emailDomain],
						[guestAddress],[guestCity],[guestSubState],[guestState],[guestCountry],
						[guestPostalCode],Latitude,Longitude,[phLoyaltyNumber],[phLoyaltyStatus],[phLoyaltyTier],[gender],sourceKey)
		AS
		(
			SELECT DISTINCT g.FirstName,g.LastName,g.FirstName + ' ' + g.LastName,ge.emailAddress,
					SUBSTRING(ge.emailAddress,CHARINDEX('@',ge.emailAddress) + 1,LEN(ge.emailAddress)),
					g.Address1 + ' ' + ISNULL(g.Address2,''),ISNULL(cty.City_Text,'Unknown'),'Unknown',ISNULL(st.State_Text,'Unknown'),ISNULL(cntry.Country_Text,'Unknown'),
					ISNULL(pc.PostalCode_Text,'Unknown'),0,0,ISNULL(ln.loyaltyNumber,'Unknown'),'Unknown','Unknown','Unknown',g.GuestID
			FROM Reservations.dbo.Guest g
				LEFT JOIN Reservations.dbo.Transactions trn ON trn.GuestID = g.GuestID
				LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = trn.LoyaltyNumberID
				LEFT JOIN Reservations.dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
				LEFT JOIN Reservations.dbo.[Location] l ON l.LocationID = g.LocationID
				LEFT JOIN Reservations.dbo.City cty ON cty.CityID = l.CityID
				LEFT JOIN Reservations.dbo.[State] st ON st.StateID = l.StateID
				LEFT JOIN Reservations.dbo.Country cntry ON cntry.CountryID = l.CountryID
				LEFT JOIN Reservations.dbo.PostalCode pc ON pc.PostalCodeID = l.PostalCodeID
				LEFT JOIN Reservations.dbo.Region reg ON reg.RegionID = l.RegionID
				EXCEPT
			SELECT [First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],0,0,[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],[sourceKey]
			FROM [dim].[Guest]
		),
		cte_Final([firstName],[lastName],[fullName],[emailAddress],[emailDomain],[guestAddress],[guestCity],[guestSubState],[guestState],[guestCountry],[guestPostalCode],Latitude,Longitude,[phLoyaltyNumber],[phLoyaltyStatus],[phLoyaltyTier],[gender],sourceKey)
		AS
		(
			SELECT MAX([firstName]),MAX([lastName]),MAX([fullName]),MAX([emailAddress]),MAX([emailDomain]),MAX([guestAddress]),
				MAX([guestCity]),MAX([guestSubState]),MAX([guestState]),MAX([guestCountry]),MAX([guestPostalCode]),MAX(Latitude),
				MAX(Longitude),MAX([phLoyaltyNumber]),MAX([phLoyaltyStatus]),MAX([phLoyaltyTier]),MAX([gender]),MAX(sourceKey)
			FROM cte_Guest
			GROUP BY sourceKey
		)
		INSERT INTO [dim].[Guest]([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],[sourceKey])
		SELECT ISNULL([firstName],'Unknown'),ISNULL([lastName],'Unknown'),ISNULL([fullName],'Unknown'),ISNULL([emailAddress],'Unknown'),
				ISNULL([emailDomain],'Unknown'),ISNULL([guestAddress],'Unknown'),ISNULL([guestCity],'Unknown'),
				ISNULL([guestSubState],'Unknown'),ISNULL([guestState],'Unknown'),ISNULL([guestCountry],'Unknown'),
				ISNULL([guestPostalCode],'Unknown'),geography::Point(Latitude,Longitude,4326),
				ISNULL([phLoyaltyNumber],'Unknown'),ISNULL([phLoyaltyStatus],'Unknown'),ISNULL([phLoyaltyTier],'Unknown'),
				ISNULL([gender],'Unknown'),sourceKey
		FROM cte_Final
	-----------------------------------------------------------

	-- dim.Hotel ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[Hotel]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		;WITH cte_ResRev(HotelID,AvgNightlyRate)
		AS
		(
			SELECT h.Hotel_hotelID,SUM(td.reservationRevenue)/CONVERT(decimal(38,2),SUM(td.nights))/CONVERT(decimal(38,2),SUM(td.rooms)) AS AvgNightlyRate
			FROM Reservations.dbo.Transactions t
				INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
				INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
				INNER JOIN Reservations.dbo.hotel h ON h.HotelID = t.HotelID
			WHERE ts.confirmationDate BETWEEN DATEADD(year,-1,GETDATE()) AND GETDATE()
			GROUP BY h.Hotel_hotelID
		),
		cte_Col(HotelID,Code,CollectionName,rowNum)
		AS
		(
			SELECT hcol.HotelID,col.Code,col.CollectionName,ROW_NUMBER() OVER(PARTITION BY hcol.HotelID ORDER BY col.Hierarchy)
			FROM Hotels.dbo.[Collection] col
				INNER JOIN Hotels.dbo.Hotel_Collection hcol ON hcol.CollectionID = col.CollectionID
		),
		cte_LOC(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text,Region_Text)
		AS
		(
			SELECT l.LocationID,cty.City_Text,st.State_Text,cnty.Country_Text,post.PostalCode_Text,reg.Region_Text
			FROM Hotels.dbo.[Location] l
			LEFT JOIN Hotels.dbo.City cty ON cty.CityID = l.CityID
			LEFT JOIN Hotels.dbo.[State] st ON st.StateID = l.StateID
			LEFT JOIN Hotels.dbo.Country cnty ON cnty.CountryID = l.CountryID
			LEFT JOIN Hotels.dbo.PostalCode post ON post.PostalCodeID = l.PostalCodeID
			LEFT JOIN Hotels.dbo.Region reg ON reg.RegionID = l.RegionID
		),
		cte_User(HotelID,WindowsAccount,FirstName,LastName,PHG_RoleName)
		AS
		(
			SELECT pur.HotelID,pu.WindowsAccount,p.FirstName,p.LastName,pr.PHG_RoleName
			FROM Hotels.dbo.Hotel_PHG_User_Role pur
				INNER JOIN Hotels.dbo.PHG_User pu ON pu.PHG_UserID = pur.PHG_UserID
				INNER JOIN Hotels.dbo.PHG_Role pr ON pr.PHG_RoleID = pur.PHG_RoleID
				INNER JOIN Hotels.dbo.Person p ON p.PersonID = pu.PersonID
		),
		cte_HotelParent(HotelID,ParentCompanyID,rowNum)
		AS
		(
			SELECT hpc.HotelID,hpc.ParentCompanyID,ROW_NUMBER() OVER(PARTITION BY hpc.HotelID ORDER BY hpc.StartDate DESC,hpc.ParentRelationshipID) rowNum
			FROM Hotels.dbo.Hotel_ParentCompany hpc
		),
		cte_hotel([hotelCode],[hotelName],[hotelAddress],[hotelCity],[hotelSubState],[hotelState],[hotelCountry],
					[hotelPostalCode],Latitude,Longitude,[hotelType],[roomCount],[seasonalHotel],[hotelAverageNightlyRate],
					[phGeographicRegionCode],[phGeographicRegionName],[phAreaManagerUsername],[phAreaManagerName],
					[phRegionalDirectorUsername],[phRegionalDirectorName],[phRevenueAccountManagerUsername],
					[phRevenueAccountManager],[phRdSupervisorUsername],[phRdSupervisor],[phRamSupervisorUsername],[phRamSupervisor],
					[phPrimaryCollectionCode],[phPrimaryCollectionName],
					[hePrimaryCollectionCode],[hePrimaryCollectionName],
					[resPrimaryCollectionCode],[resPrimaryCollectionName],
					[primaryParent],[primary3rdParty],[primary3rdPartyCollection],[loyaltyParticipant],[loyaltyOptInParticipant],sourceKey,
					StatusCodeName,[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID])
		AS
		(
			SELECT DISTINCT CASE WHEN h.SynXisCode IS NULL THEN ISNULL(hop.HotelCode,h.HotelCode) ELSE h.HotelCode END,
				h.HotelName,ISNULL(h.Address1,'Unknown') + ISNULL(' ' + h.Address2,''),ISNULL(loc.City_Text,'Unknown'),'Unknown',
				ISNULL(loc.State_Text,'Unknown'),ISNULL(loc.Country_Text,'Unknown'),ISNULL(loc.PostalCode_Text,'Unknown'),ISNULL(h.Latitude,0),ISNULL(h.Longitude,0),'Unknown',
				ISNULL(h.Rooms,0),0,ISNULL(rev.AvgNightlyRate,0),
				ISNULL(loc.Region_Text,'Unknown'),ISNULL(loc.Region_Text,'Unknown'),
				ISNULL(usrArea.WindowsAccount,'Unknown'),ISNULL(usrArea.FirstName,'Unknown') + ' ' + ISNULL(usrArea.LastName,''),
				ISNULL(usrReg.WindowsAccount,'Unknown'),ISNULL(usrReg.FirstName,'Unknown') + ' ' + ISNULL(usrReg.LastName,''),
				ISNULL(usrRev.WindowsAccount,'Unknown'),ISNULL(usrRev.FirstName,'Unknown') + ' ' + ISNULL(usrRev.LastName,''),
				ISNULL(usrAD.WindowsAccount,'Unknown'),ISNULL(usrAD.FirstName,'Unknown') + ' ' + ISNULL(usrAD.LastName,''),
				ISNULL(usrRAM.WindowsAccount,'Unknown'),ISNULL(usrRAM.FirstName,'Unknown') + ' ' + ISNULL(usrRAM.LastName,''),
				ISNULL(ph.Code,'Unknown'),ISNULL(ph.CollectionName,'Unknown'),
				ISNULL(he.Code,'Unknown'),ISNULL(he.CollectionName,'Unknown'),
				ISNULL(res.Code,'Unknown'),ISNULL(res.CollectionName,'Unknown'),
				ISNULL(parent.ParentCompanyName,'Unknown'),'Unknown','Unknown',0,0,
				h.HotelID,sc.StatusCodeName,
				ISNULL(h.[SynXisCode],'Unknown'),ISNULL(CONVERT(nvarchar(50),h.[SynXisID]),'Unknown'),ISNULL(h.[OpenHospCode],'Unknown'),ISNULL(CONVERT(nvarchar(50),h.[OpenHospID]),'Unknown')
			FROM Hotels.[dbo].[Hotel] h
				LEFT JOIN Hotels.authority.Hotels_OpenHosp hop ON hop.openHospID = h.OpenHospID
				LEFT JOIN Hotels.[dbo].[StatusCode] sc ON sc.[StatusCodeID] = h.[StatusCodeID]
				LEFT JOIN cte_ResRev rev ON rev.HotelID = h.HotelID
				LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 1) ph ON ph.HotelID = h.HotelID
				LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 2 AND Code IN('HE','HW')) he ON he.HotelID = h.HotelID
				LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 2 AND Code IN('RES')) res ON res.HotelID = h.HotelID
				LEFT JOIN cte_HotelParent hparent ON hparent.HotelID = h.HotelID AND hparent.rowNum = 1
				LEFT JOIN Hotels.dbo.ParentCompany parent ON parent.ParentCompanyID = hparent.ParentCompanyID
				LEFT JOIN cte_LOC loc ON loc.LocationID = h.LocationID
				LEFT JOIN cte_User usrArea ON usrArea.HotelID = h.HotelID AND usrArea.PHG_RoleName = 'Area Managing Director'
				LEFT JOIN cte_User usrReg ON usrReg.HotelID = h.HotelID AND usrReg.PHG_RoleName = 'Regional Director'
				LEFT JOIN cte_User usrRev ON usrRev.HotelID = h.HotelID AND usrRev.PHG_RoleName = 'Revenue Account Manager'
				LEFT JOIN cte_User usrAD ON usrAD.HotelID = h.HotelID AND usrAD.PHG_RoleName = 'Account Director'
				LEFT JOIN cte_User usrRAM ON usrRAM.HotelID = h.HotelID AND usrRAM.PHG_RoleName = 'Regional Admin'
			WHERE h.HotelCode IS NOT NULL
				EXCEPT
			SELECT [Hotel Code],[Hotel Name],[Hotel Address],[Hotel City],[Hotel Sub State],[Hotel State],[Hotel Country],[Hotel Postal Code],[Hotel GeoCode].Lat AS Latitude,[Hotel GeoCode].Long AS Longitude,[Hotel Type],[Room Count],[Seasonal Hotel],[Hotel Average Nightly Rate],[PH Geographic Region Code],[PH Geographic Region Name],[PH AMD User Name],[PH AMD Name],[PH RD User Name],[PH RD Name],[PH RAM User Name],[PH RAM],[PH RD Supervisor User Name],[PH RD Supervisor],[PH RAM Supervisor User Name],[PH RAM Supervisor],[PH Primary Collection Code],[PH Primary Collection Name],[Historic Hotel Primary Collection Code],[Historic Hotel Primary Collection Name],[Residence Hotel Primary Collection Code],[Residence Hotel Primary Collection Name],[Primary Owner/Management],[Primary 3rd Party],[Primary 3rd Party Collection],[I Prefer Participant],[I Prefer Opt-In Participant],[sourceKey],[Status Code],[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID]
			FROM [dim].[Hotel]
		)

		INSERT INTO [dim].[Hotel]([Hotel Code],[Hotel Name],[Hotel Address],[Hotel City],[Hotel Sub State],[Hotel State],[Hotel Country],[Hotel Postal Code],[Hotel GeoCode],[Hotel Type],[Room Count],[Seasonal Hotel],[Hotel Average Nightly Rate],[PH Geographic Region Code],[PH Geographic Region Name],[PH AMD User Name],[PH AMD Name],[PH RD User Name],[PH RD Name],[PH RAM User Name],[PH RAM],[PH RD Supervisor User Name],[PH RD Supervisor],[PH RAM Supervisor User Name],[PH RAM Supervisor],[PH Primary Collection Code],[PH Primary Collection Name],[Historic Hotel Primary Collection Code],[Historic Hotel Primary Collection Name],[Residence Hotel Primary Collection Code],[Residence Hotel Primary Collection Name],[Primary Owner/Management],[Primary 3rd Party],[Primary 3rd Party Collection],[I Prefer Participant],[I Prefer Opt-In Participant],[sourceKey],[Status Code],[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID])
		SELECT [hotelCode],[hotelName],[hotelAddress],[hotelCity],[hotelSubState],[hotelState],[hotelCountry],
				[hotelPostalCode],geography::Point(Latitude,Longitude,4326),[hotelType],[roomCount],[seasonalHotel],[hotelAverageNightlyRate],
				[phGeographicRegionCode],[phGeographicRegionName],[phAreaManagerUsername],[phAreaManagerName],
				[phRegionalDirectorUsername],[phRegionalDirectorName],[phRevenueAccountManagerUsername],
				[phRevenueAccountManager],[phRdSupervisorUsername],[phRdSupervisor],[phRamSupervisorUsername],
				[phRamSupervisor],[phPrimaryCollectionCode],[phPrimaryCollectionName],[hePrimaryCollectionCode],
				[hePrimaryCollectionName],[resPrimaryCollectionCode],[resPrimaryCollectionName],[primaryParent],
				[primary3rdParty],[primary3rdPartyCollection],[loyaltyParticipant],[loyaltyOptInParticipant],sourceKey,StatusCodeName,
				[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID]
		FROM cte_hotel c
	-----------------------------------------------------------

	-- dim.Hotel_PH_Collection --------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[Hotel_PH_Collection]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[Hotel_PH_Collection]([hotelKey],[Start Date],[End Date],[Collection Code],[Collection Name],[priorityOrder]
												,[Brand Logo],[Collection Logo])
		SELECT DISTINCT dh.hotelKey,dStart.DateKey,dEnd.DateKey,c.Code,c.CollectionName,ISNULL(c.Hierarchy,0),
						logo.BrandURL,logo.CollectionURL
		FROM Hotels.dbo.Hotel_Collection hc
			INNER JOIN Hotels.dbo.[Collection] c ON c.CollectionID = hc.CollectionID
			INNER JOIN Hotels.dbo.Hotel h ON h.HotelID = hc.HotelID
			INNER JOIN dim.Hotel dh ON dh.sourceKey = h.HotelID
			LEFT JOIN dim.DimDate dStart ON dStart.[Full Date] = hc.StartDate
			LEFT JOIN dim.DimDate dEnd ON dEnd.[Full Date] = hc.EndDate
			LEFT JOIN [dbo].[Logos_BrandCollection] logo ON logo.CollectionCode = c.Code
		WHERE dStart.DateKey IS NOT NULL
			EXCEPT
		SELECT [hotelKey],[Start Date],[End Date],[Collection Code],[Collection Name],[priorityOrder],[Brand Logo],[Collection Logo]
		FROM [dim].[Hotel_PH_Collection]
	-----------------------------------------------------------

	-- UPDATE dim.Hotel for Collections -----------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': UPDATE [dim].[Hotel]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		-- For Each Hotel Give the collectionCode that is the lowest in priorty & highest in date
		;WITH cte_MinOrder(hotelKey,collectionCode,priorityOrder)
		AS
		(
			SELECT hc.hotelKey,hc.[Collection Code],MIN(hc.priorityOrder)
			FROM dim.Hotel_PH_Collection hc
			GROUP BY hc.hotelKey,hc.[Collection Code]
		),
		cte_PH(hotelKey,[Collection Code],[Collection Name],priorityOrder)
		AS
		(
			SELECT col.hotelKey,col.[Collection Code],col.[Collection Name],col.priorityOrder
			FROM dim.Hotel_PH_Collection col
				INNER JOIN cte_MinOrder mo ON mo.hotelKey = col.hotelKey AND mo.collectionCode = col.[Collection Code] AND mo.priorityOrder = col.priorityOrder
		),
		cte_HEC(hotelKey,[Collection Code],[Collection Name],priorityOrder)
		AS
		(
			SELECT col.hotelKey,col.[Collection Code],col.[Collection Name],col.priorityOrder
			FROM dim.Hotel_PH_Collection col
				INNER JOIN cte_MinOrder mo ON mo.hotelKey = col.hotelKey AND mo.collectionCode = col.[Collection Code] AND mo.priorityOrder = col.priorityOrder AND mo.collectionCode IN('HE','HW')
		),
		cte_RES(hotelKey,[Collection Code],[Collection Name],priorityOrder)
		AS
		(
			SELECT col.hotelKey,col.[Collection Code],col.[Collection Name],col.priorityOrder
			FROM dim.Hotel_PH_Collection col
				INNER JOIN cte_MinOrder mo ON mo.hotelKey = col.hotelKey AND mo.collectionCode = col.[Collection Code] AND mo.priorityOrder = col.priorityOrder AND mo.collectionCode IN('PRR')
		)
		UPDATE h
			SET [PH Primary Collection Code] = ISNULL((SELECT TOP 1 [Collection Code] FROM cte_PH WHERE hotelKey = h.hotelKey ORDER BY priorityOrder),'Unknown'),
				[PH Primary Collection Name] = ISNULL((SELECT TOP 1 [Collection Name] FROM cte_PH WHERE hotelKey = h.hotelKey ORDER BY priorityOrder),'Unknown'),
				[Historic Hotel Primary Collection Code] = ISNULL((SELECT TOP 1 [Collection Code] FROM cte_HEC WHERE hotelKey = h.hotelKey ORDER BY priorityOrder),'Unknown'),
				[Historic Hotel Primary Collection Name] = ISNULL((SELECT TOP 1 [Collection Name] FROM cte_HEC WHERE hotelKey = h.hotelKey ORDER BY priorityOrder),'Unknown'),
				[Residence Hotel Primary Collection Code] = ISNULL((SELECT TOP 1 [Collection Code] FROM cte_RES WHERE hotelKey = h.hotelKey ORDER BY priorityOrder),'Unknown'),
				[Residence Hotel Primary Collection Name] = ISNULL((SELECT TOP 1 [Collection Name] FROM cte_RES WHERE hotelKey = h.hotelKey ORDER BY priorityOrder),'Unknown')
		FROM dim.Hotel h
	-----------------------------------------------------------

	-- UPDATE dim.Hotel for Collections -----------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': UPDATE [dim].[Hotel]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		UPDATE h
			SET [PH Primary Collection Logo] = PH_logo.CollectionURL,
				[PH Primary Collection Brand Logo] = PH_logo.BrandURL,
				[Historic Hotel Primary Collection Logo] = HE_logo.CollectionURL,
				[Historic Hotel Primary Collection Brand Logo] = HE_logo.BrandURL,
				[Residence Primary Collection Logo] = RE_logo.CollectionURL,
				[Residence Primary Collection Brand Logo] = RE_logo.BrandURL
		FROM dim.Hotel h
			LEFT JOIN dbo.Logos_BrandCollection PH_logo ON PH_logo.CollectionCode = h.[PH Primary Collection Code]
			LEFT JOIN dbo.Logos_BrandCollection HE_logo ON HE_logo.CollectionCode = h.[Historic Hotel Primary Collection Code]
			LEFT JOIN dbo.Logos_BrandCollection RE_logo ON RE_logo.CollectionCode = h.[Residence Hotel Primary Collection Code]
	-----------------------------------------------------------


	-- dim.Hotel_PH_Program -----------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[Hotel_PH_Program]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[Hotel_PH_Program]([hotelKey],[Start Date],[End Date],[Program Name])
		SELECT DISTINCT h.hotelKey,dtStart.DateKey,dtEnd.DateKey,p.ProgramName
		FROM Hotels.dbo.Program p
			INNER JOIN Hotels.dbo.Hotel_Program hp ON hp.ProgramID = p.ProgramID
			INNER JOIN dim.Hotel h ON h.sourceKey = hp.HotelID
			INNER JOIN dim.DimDate dtStart ON dtStart.[Full Date] = hp.StartDate
			INNER JOIN dim.DimDate dtEnd ON dtEnd.[Full Date] = hp.StartDate
			EXCEPT
		SELECT [hotelKey],[Start Date],[End Date],[Program Name]
		FROM [dim].[Hotel_PH_Program]
	-----------------------------------------------------------

	-- dim.HotelExpereince ------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[HotelExpereince]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[HotelExpereince]([hotelKey],[experience])
		SELECT DISTINCT h.hotelKey,e.ExperienceName
		FROM Hotels.dbo.Experience e 
			INNER JOIN Hotels.dbo.Hotel_Experience he ON he.ExperienceID = e.ExperienceID
			INNER JOIN [dim].[Hotel] h ON h.sourceKey = he.HotelID
		WHERE e.ExperienceName IS NOT NULL
			EXCEPT
		SELECT [hotelKey],[experience] FROM [dim].[HotelExpereince]
	-----------------------------------------------------------

	-- dim.HotelParentCompany ---------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[HotelParentCompany]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[HotelParentCompany]([hotelKey],[Start Date],[End Date],[CRM ID],[Parent Name])
		SELECT h.hotelKey,dtStart.DateKey,dtEnd.DateKey,pc.CRM_ParentCompanyID,pc.ParentCompanyName
		FROM Hotels.dbo.ParentCompany pc
			INNER JOIN Hotels.dbo.Hotel_ParentCompany hpc ON hpc.ParentCompanyID = pc.ParentCompanyID
			INNER JOIN dim.Hotel h ON h.sourceKey = hpc.HotelID
			INNER JOIN dim.DimDate dtStart ON dtStart.[Full Date] = hpc.StartDate
			INNER JOIN dim.DimDate dtEnd ON dtEnd.[Full Date] = hpc.StartDate
			EXCEPT
		SELECT [hotelKey],[Start Date],[End Date],[CRM ID],[Parent Name]
		FROM [dim].[HotelParentCompany]
	-----------------------------------------------------------

	-- dim.LeisureAccount -------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.LoyaltyDetail --------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- dim.PartyDetail ----------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.PseudoCity -----------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.Rate -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[Rate]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		;WITH cte_Rate([rateCode],[rateName],[phRateName],[rateCategory],[phRateCategory],[gdsRateAccessCode],[gdsBookingCode],[commissionable],[corporateCode],[promoCode],[couponCode])
		AS
		(
			SELECT DISTINCT ISNULL(rtc.RateCode,'Unknown'),ISNULL(rtc.RateName,'Unknown'),'Unknown',ISNULL(rc.rateCategoryCode,'Unknown'),
				'Unknown','Unknown','Unknown',
				CASE WHEN ISNULL(td.commisionPercent,0) > 0 THEN 1 ELSE 0 END,ISNULL(cc.corporationCode,'Unknown'),ISNULL(pc.promotionalCode,'Unknown'),
				'Unknown'
			FROM Reservations.dbo.Transactions t
				INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
				LEFT JOIN Reservations.dbo.RateCode rtc ON rtc.RateCodeID = t.RateCodeID
				LEFT JOIN Reservations.dbo.RateCategory rc ON rc.RateCategoryID = t.RateCategoryID
				LEFT JOIN Reservations.dbo.PromoCode pc ON pc.PromoCodeID = t.PromoCodeID
				LEFT JOIN Reservations.dbo.CorporateCode cc ON cc.CorporateCodeID = t.CorporateCodeID
		)
		INSERT INTO [dim].[Rate]([Rate Code],[Rate Name],[PH Rate Name],[Rate Category],[PH Rate Category],[GDS Rate Access Code],[GDS Booking Code],[Commissionable],[Corporate Code],[Promo Code],[Coupon Code])
		SELECT [rateCode],[rateName],[phRateName],[rateCategory],[phRateCategory],
				[gdsRateAccessCode],[gdsBookingCode],[commissionable],[corporateCode],
				[promoCode],[couponCode]
		FROM cte_Rate
			EXCEPT
		SELECT [Rate Code],[Rate Name],[PH Rate Name],[Rate Category],[PH Rate Category],[GDS Rate Access Code],[GDS Booking Code],[Commissionable],[Corporate Code],[Promo Code],[Coupon Code]
		FROM [dim].[Rate]
	-----------------------------------------------------------

	-- dim.Room -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[Room]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT


		INSERT INTO [dim].[Room]([Room Code],[Room Name],[Room Category],[PH Room Type])
		SELECT DISTINCT rt.roomTypeCode,rt.roomTypeName,'Unknown','Unknown'
		FROM Reservations.dbo.RoomType rt
			EXCEPT
		SELECT [Room Code],[Room Name],[Room Category],[PH Room Type] FROM [dim].[Room]
	-----------------------------------------------------------

	-- dim.TravelAgency ---------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[TravelAgency]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		DECLARE @dateKeyToday int
		SELECT @dateKeyToday = DateKey FROM dim.DimDate WHERE [Full Date] = CONVERT(date,GETDATE())

		;WITH cte_LOC(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text,Region_Text)
		AS
		(
			SELECT l.LocationID,cty.City_Text,st.State_Text,cnty.Country_Text,post.PostalCode_Text,reg.Region_Text
			FROM Reservations.dbo.[Location] l
			LEFT JOIN Reservations.dbo.City cty ON cty.CityID = l.CityID
			LEFT JOIN Reservations.dbo.[State] st ON st.StateID = l.StateID
			LEFT JOIN Reservations.dbo.Country cnty ON cnty.CountryID = l.CountryID
			LEFT JOIN Reservations.dbo.PostalCode post ON post.PostalCodeID = l.PostalCodeID
			LEFT JOIN Reservations.dbo.Region reg ON reg.RegionID = l.RegionID
		),
		cte_TA([iataNumber],[travelAgencyCrmIGuid],[travelAgencyName],
				[travelAgencyAddress],[travelAgencyCity],[travelAgencySubstate],[travelAgencyState],[travelAgencyCountry],[travelAgencyPostalCode],
				Latitude,Longitude,[phAccountManagerUsername],[phAccountManagerName],[phAmSupervisorUsername],[phAmSupervisor],[primaryTravelAgentGroup],
				[rowStartDate],[rowEndDate])
		AS
		(
			SELECT DISTINCT iata.IATANumber,CONVERT(uniqueidentifier,'00000000-0000-0000-0000-000000000000'),ISNULL(ta.[Name],'Unknown'),
				ISNULL(ta.Address1,'Unknown') + ' ' + ISNULL(ta.Address2,''),ISNULL(loc.City_Text,'Unknown'),'Unknown',ISNULL(loc.State_Text,'Unknown'),ISNULL(loc.Country_Text,'Unknown'),ISNULL(loc.PostalCode_Text,'Unknown'),
				0,0,'Unknown','Unknown','Unknown','Unknown','Unknown',
				@dateKeyToday AS rowStartDate,NULL AS [rowEndDate]
			FROM Reservations.dbo.IATANumber iata
				LEFT JOIN Reservations.dbo.TravelAgent ta ON iata.IATANumberID = ta.IATANumberID
				LEFT JOIN cte_LOC loc ON loc.LocationID = ta.LocationID
			--WHERE ISNUMERIC(iata.IATANumber) = 1
				EXCEPT
			SELECT [IATA Number],[Travel Agency CRM Guid],[Travel Agency Name],[Travel Agency Address],[Travel Agency City],[Travel Agency Sub State],[Travel Agency State],[Travel Agency Country],[Travel Agency Postal Code],0,0,[PH Account Manager User Name],[PH Account Manager Name],[PH Account Manager Supervisor User Name],[PH Account Manager Supervisor],[Primary Travel Agent Group],
					@dateKeyToday AS [Start Date],NULL AS [End Date]
			FROM [dim].[TravelAgency]
		)
		INSERT INTO [dim].[TravelAgency]([IATA Number],[Travel Agency CRM Guid],[Travel Agency Name],[Travel Agency Address],[Travel Agency City],[Travel Agency Sub State],[Travel Agency State],[Travel Agency Country],[Travel Agency Postal Code],[Travel Agency GeoCode],[PH Account Manager User Name],[PH Account Manager Name],[PH Account Manager Supervisor User Name],[PH Account Manager Supervisor],[Primary Travel Agent Group],[Start Date],[End Date])
		SELECT [iataNumber],[travelAgencyCrmIGuid],[travelAgencyName],
				[travelAgencyAddress],[travelAgencyCity],[travelAgencySubstate],[travelAgencyState],[travelAgencyCountry],[travelAgencyPostalCode],
				geography::Point(Latitude,Longitude,4326),[phAccountManagerUsername],[phAccountManagerName],[phAmSupervisorUsername],[phAmSupervisor],[primaryTravelAgentGroup],
				[rowStartDate],[rowEndDate]
		FROM cte_TA

		;WITH cte_TA([travelAgencyKey],rowNum)
		AS
		(
			SELECT [travelAgencyKey],ROW_NUMBER() OVER(PARTITION BY [IATA Number] ORDER BY [Start Date])
			FROM dim.TravelAgency
			WHERE [End Date] IS NULL
		)
		UPDATE ta
			SET [End Date] = @dateKeyToday
		FROM [dim].[TravelAgency] ta
			INNER JOIN cte_TA c ON c.travelAgencyKey = ta.travelAgencyKey
		WHERE c.rowNum > 1
	-----------------------------------------------------------

	-- dim.TravelAgencyGroup ----------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.VoiceAgent -----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [dim].[VoiceAgent]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		INSERT INTO [dim].[VoiceAgent]([Voice Agent User Name])
		SELECT DISTINCT v.VoiceAgent
		FROM Reservations.dbo.VoiceAgent v
			EXCEPT
		SELECT [Voice Agent User Name] FROM [dim].[VoiceAgent]
	-----------------------------------------------------------

	-- fact.Reservation ---------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [fact].[Reservation]'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		 -- #CUR_DATE ------------------------------
		IF OBJECT_ID('tempdb..#CUR_DATE') IS NOT NULL
			DROP TABLE #CUR_DATE;
		CREATE TABLE #CUR_DATE
		(
			FullDate date NOT NULL,
			currencyCode nvarchar(3) NOT NULL,
			DateKey int NOT NULL,
			USD_ExchangeRate decimal(18,9) NOT NULL,
			currencyKey int NOT NULL,
			PRIMARY KEY CLUSTERED(FullDate,currencyCode)
		)

		INSERT INTO #CUR_DATE(FullDate,currencyCode,DateKey,USD_ExchangeRate,currencyKey)
		SELECT DISTINCT dd.[Full Date],cur.[Currency Code],dd.DateKey,ce.[USD Exchange Rate],cur.currencyKey
		FROM dim.DimDate dd
			INNER JOIN fact.CurrencyExchange ce ON ce.DateKey = dd.DateKey
			INNER JOIN dim.Currency cur ON cur.currencyKey = ce.currencyKey
		WHERE cur.[Currency Code] IN(SELECT DISTINCT currency FROM Reservations.dbo.TransactionDetail)
			AND dd.[Full Date] BETWEEN @minDate AND @maxDate
		--------------------------------------------

		TRUNCATE TABLE [fact].[Reservation]

		INSERT INTO [fact].[Reservation]([Booking System],[Confirmation Number],[GDS Booked],[GDS Record Locator],
										[Confirmation Date],[Arrival Date],[Departure Date],[Cancellation Date],
										[hotelKey],[bookingSourceKey],[voiceAgentKey],[rateKey],[roomKey],[crsChainKey],
										[formOfPaymentKey],[bookingStatusKey],[partyDetailsKey],[loyaltyDetailsKey],[guestKey],
										[pseudoCityKey],[travelAgencyKey],[corporateAccountKey],[leisureAccountKey],[Lead Time],
										[Length Of Stay],[Rooms],[Nights],[Room Nights],[Original Currency],
										[Revenue Original Currency],[Travel Agency Commission Original Currency],
										[Revenue USD Confirmation Date],[Travel Agency Commission USD Confirmation Date],
										[Revenue USD Arrival Date],[Travel Agency Commission USD Arrival Date],
										[Revenue USD Departure Date],[Travel Agency Commission USD Departure Date],
										[Travel Agency Commission Percentage],
										[Count Of Bookings],[Loyalty Program Points],[PH Cost USD],[PH Revenue USD],[PH Surcharges USD],
										[PH Profit USD],[PH Brand Revenue])
		SELECT DISTINCT ISNULL(dbs.[Booking System],0),t.confirmationNumber,'Unknown','Unknown',
				confDate.DateKey,arvlDate.DateKey,depDate.DateKey,cnclDate.DateKey,
				ISNULL(dh.hotelKey,0),ISNULL(dbs.bookingSourceKey,0),ISNULL(dva.voiceAgentKey,0),ISNULL(dr.rateKey,0),ISNULL(droom.roomKey,0),ISNULL(dchain.crsChainKey,0),
				ISNULL(fop.formOfPaymentKey,0),ISNULL(dbstat.bookingStatusKey,0), 0 ,ISNULL(dld.loyaltyDetailsKey,0),ISNULL(dg.guestKey,0),
				0 ,ISNULL(dta.travelAgencyKey,0), 0 , 0 ,ISNULL(DATEDIFF(day,confDate.[Full Date],arvlDate.[Full Date]),0),
				ISNULL(DATEDIFF(day,arvlDate.[Full Date],depDate.[Full Date]),0),td.rooms,td.nights,td.rooms * td.nights,ISNULL(CurConfDate.currencyKey,0),
				ISNULL(td.reservationRevenue,0),ISNULL(td.[commisionPercent] * td.reservationRevenue,0),
				ISNULL(CurConfDate.USD_ExchangeRate * td.reservationRevenue,0),ISNULL(CurConfDate.USD_ExchangeRate * td.[commisionPercent] * td.reservationRevenue,0),
				ISNULL(curArvlDate.USD_ExchangeRate * td.reservationRevenue,0),ISNULL(curArvlDate.USD_ExchangeRate * td.[commisionPercent] * td.reservationRevenue,0),
				ISNULL(curDepDate.USD_ExchangeRate * td.reservationRevenue,0),ISNULL(curDepDate.USD_ExchangeRate * td.[commisionPercent] * td.reservationRevenue,0),
				ISNULL(td.[commisionPercent],0),
				1,0,0,0,0,
				0,'Unknown'
		FROM Reservations.dbo.Transactions t
			--INNER JOIN Reservations.dbo.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
			INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			LEFT JOIN #CUR_DATE CurConfDate ON CurConfDate.FullDate = ts.confirmationDate AND CurConfDate.currencyCode = td.currency
			LEFT JOIN #CUR_DATE curArvlDate ON curArvlDate.FullDate = td.arrivalDate AND curArvlDate.currencyCode = td.currency
			LEFT JOIN #CUR_DATE curDepDate ON curDepDate.FullDate = td.departureDate AND curDepDate.currencyCode = td.currency
			LEFT JOIN dim.DimDate confDate ON confDate.[Full Date] = ts.confirmationDate
			LEFT JOIN dim.DimDate arvlDate ON arvlDate.[Full Date] = td.arrivalDate
			LEFT JOIN dim.DimDate depDate ON depDate.[Full Date] = td.departureDate
			LEFT JOIN dim.DimDate cnclDate ON cnclDate.[Full Date] = ts.cancellationDate
			LEFT JOIN Reservations.dbo.hotel rh ON rh.HotelID = t.HotelID
			LEFT JOIN dim.Hotel dh ON dh.sourceKey = rh.Hotel_hotelID
			LEFT JOIN temp.BookingSource dbs ON dbs.CRS_sourceKey = t.CRS_BookingSourceID AND dbs.PH_sourceKey = ISNULL(t.PH_BookingSourceID,0)
			LEFT JOIN Reservations.dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
			LEFT JOIN dim.VoiceAgent dva ON dva.[Voice Agent User Name] = va.VoiceAgent

			LEFT JOIN Reservations.dbo.RateCode rtc ON rtc.RateCodeID = t.RateCodeID
			LEFT JOIN Reservations.dbo.RateCategory rc ON rc.RateCategoryID = t.RateCategoryID
			LEFT JOIN Reservations.dbo.PromoCode pc ON pc.PromoCodeID = t.PromoCodeID
			LEFT JOIN Reservations.dbo.CorporateCode cc ON cc.CorporateCodeID = t.CorporateCodeID
			LEFT JOIN dim.Rate dr ON dr.[Rate Code] = ISNULL(rtc.RateCode,'Unknown')
								AND dr.[Rate Name] = ISNULL(rtc.RateName,'Unknown')
								AND dr.[Rate Category] = ISNULL(rc.rateCategoryCode,'Unknown')
								AND dr.[Promo Code] = ISNULL(pc.promotionalCode,'Unknown')
								AND dr.[Corporate Code] = ISNULL(cc.corporationCode,'Unknown')
								AND dr.Commissionable = CASE WHEN ISNULL(td.commisionPercent,0) > 0 THEN 1 ELSE 0 END
			LEFT JOIN Reservations.dbo.RoomType rrt ON rrt.RoomTypeID = t.RoomTypeID
			LEFT JOIN [dim].[Room] droom ON droom.[Room Name] = rrt.roomTypeName AND droom.[Room Code] = rrt.roomTypeCode
			LEFT JOIN Reservations.dbo.Chain rchain ON rchain.ChainID = t.ChainID
			LEFT JOIN dim.CRS_Chain dchain ON dchain.[CRS Chain ID] = rchain.CRS_ChainID
			LEFT JOIN dim.FormOfPayment fop ON fop.[Form of Payment Code] = ts.creditCardType
			LEFT JOIN dim.BookingStatus dbstat ON dbstat.[Booking Status Name] = ts.[status]
			LEFT JOIN dim.LoyaltyDetail dld ON dld.IsOptIn = ISNULL(td.optIn,0) AND dld.IsTagged = ISNULL(td.LoyaltyNumberTagged,0) AND dld.IsValid = ISNULL(td.LoyaltyNumberValidated,0)
			LEFT JOIN dim.Guest dg ON dg.sourceKey = t.GuestID
			LEFT JOIN Reservations.dbo.TravelAgent rta ON rta.TravelAgentID = t.TravelAgentID
			LEFT JOIN Reservations.dbo.IATANumber ia ON ia.IATANumberID = rta.IATANumberID
			LEFT JOIN (SELECT [IATA Number],MAX(travelAgencyKey) AS travelAgencyKey FROM [dim].[TravelAgency] GROUP BY [IATA Number]) dta ON dta.[IATA Number] = ia.IATANumber
	-----------------------------------------------------------

	-- PRINT STATUS --
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
*/
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [dbo].[checkContractDates]'
GO
REVOKE EXECUTE ON  [dbo].[checkContractDates] TO [INDECORP\SQ_PR_GROUP_DBRead]
GO
REVOKE REFERENCES ON  [dbo].[checkContractDates] TO [INDECORP\SQ_PR_GROUP_DBRead]
GO
REVOKE VIEW DEFINITION ON  [dbo].[checkContractDates] TO [INDECORP\SQ_PR_GROUP_DBRead]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering permissions on  [rpt].[Halo_Scorecard]'
GO
REVOKE EXECUTE ON  [rpt].[Halo_Scorecard] TO [reportuser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
