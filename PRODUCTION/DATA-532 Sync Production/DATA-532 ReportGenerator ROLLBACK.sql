USE ReportGenerator
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReportGenerator    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReportGenerator

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 1:02:08 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[breachPHmailed]'
GO
DROP PROCEDURE [dbo].[breachPHmailed]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[breachPHassistance]'
GO
DROP PROCEDURE [dbo].[breachPHassistance]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[breachHEmailed]'
GO
DROP PROCEDURE [dbo].[breachHEmailed]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[breachHEassistance]'
GO
DROP PROCEDURE [dbo].[breachHEassistance]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetHotelsForIpreferBackbill]'
GO
DROP PROCEDURE [dbo].[GetHotelsForIpreferBackbill]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetHotelsForCreditMemos]'
GO
DROP PROCEDURE [dbo].[GetHotelsForCreditMemos]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BreachURunAdHoc]'
GO
DROP PROCEDURE [dbo].[BreachURunAdHoc]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BreachRunAdHoc]'
GO
DROP PROCEDURE [dbo].[BreachRunAdHoc]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[split]'
GO
DROP FUNCTION [dbo].[split]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[split2]'
GO
DROP FUNCTION [dbo].[split2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetHotelsForCoOpMarketingScorecardDELETEME]'
GO
DROP PROCEDURE [dbo].[GetHotelsForCoOpMarketingScorecardDELETEME]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
