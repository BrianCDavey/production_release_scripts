USE BusinessPortalApp
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.BusinessPortalApp    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.BusinessPortalApp

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 11:28:39 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_datareader'
GO
ALTER ROLE [db_datareader] ADD MEMBER [INDECORP\SQ_PR_GROUP_DBRead]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [INDECORP\SQ_PR_GROUP_DBUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [linkuser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datareader] ADD MEMBER [reportuser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering members of role db_datawriter'
GO
ALTER ROLE [db_datawriter] ADD MEMBER [INDECORP\SQ_PR_GROUP_DBUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datawriter] ADD MEMBER [linkuser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER ROLE [db_datawriter] ADD MEMBER [reportuser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[qrptStatement_SRS]'
GO
DROP PROCEDURE [dbo].[qrptStatement_SRS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwSharepoint_UserSalesTerritory]'
GO
CREATE VIEW [dbo].[vwSharepoint_UserSalesTerritory]
AS
SELECT     TOP (100) PERCENT Lists.tp_Title, RTRIM(UserData.nvarchar3) AS UserId, RTRIM(UserData.nvarchar4) AS SALSTERR
FROM         BusinessPortal_WSS_Content_13306.dbo.Lists AS Lists INNER JOIN
                      BusinessPortal_WSS_Content_13306.dbo.UserData AS UserData ON Lists.tp_ID = UserData.tp_ListId
WHERE     (Lists.tp_Title = N'UserSalesTerritory')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwCustEmailTo]'
GO
ALTER view [dbo].[vwCustEmailTo]
AS
SELECT     CUSTNMBR, Email_Type, Email_Recipient
FROM         chisqp01.IC.dbo.RM00106
WHERE     (Email_Type = 1)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwCustEmailCC]'
GO
ALTER view [dbo].[vwCustEmailCC]
AS
SELECT     CUSTNMBR, Email_Type, Email_Recipient
FROM         chisqp01.IC.dbo.RM00106
WHERE     (Email_Type = 2)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwCustEmailBC]'
GO
ALTER view [dbo].[vwCustEmailBC]
AS
SELECT     CUSTNMBR, Email_Type, Email_Recipient
FROM         chisqp01.IC.dbo.RM00106
WHERE     (Email_Type = 3)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwCustomers]'
GO
ALTER view [dbo].[vwCustomers]
AS
SELECT     TOP (100) PERCENT Customers.[Customer Number], Customers.[Customer Name], Customers.[Address 1], 
                      Customers.[Address 2], Customers.City, Customers.State, Customers.Zip, Customers.[Phone 1], 
                      Customers.[Aging Bucket1], Customers.[Aging Bucket2], Customers.[Aging Bucket3], Customers.[Aging Bucket4], 
                      Customers.[Aging Bucket5], Customers.[Aging Bucket6], Customers.[Aging Bucket7], 
                      Customers.[Accounts Receivable Account Number], Customers.[Address 3], Customers.[Address Code], 
                      Customers.[Average Days To Pay - Life], Customers.[Average Days To Pay - Year], Customers.[Average Days to Pay - LYR], 
                      Customers.[Balance Type], Customers.[Bank Branch], Customers.[Bank Name], Customers.[COGS Account Number], 
                      Customers.[Cash Account Number], Customers.[Checkbook ID], Customers.Comment1, Customers.Comment2, 
                      Customers.[Contact Person], Customers.[Corporate Customer Number], Customers.Country, Customers.[Created Date], 
                      Customers.[Credit Card Exp Date], Customers.[Credit Card ID], Customers.[Credit Limit Amount], 
                      Customers.[Credit Limit Period], Customers.[Credit Limit Period Amount], Customers.[Credit Limit Type], 
                      Customers.[Currency ID], Customers.[Customer Balance], Customers.[Customer Class], Customers.[Customer Discount], 
                      Customers.[Default Cash Account Type], Customers.[Deposits Received], Customers.[Discount Grace Period], 
                      Customers.[Discounts Available Account Number], Customers.[Discounts Taken Account Number], 
                      Customers.[Document Format ID], Customers.[Due Date Grace Period], Customers.Fax, 
                      Customers.[Finance Charge Account Number], Customers.[Finance Charge Amt Type], Customers.[Finance Charge Dollar], 
                      Customers.[Finance Charge ID], Customers.[Finance Charge Percent], Customers.[Finance Charges CYTD], 
                      Customers.[Finance Charges LYR Calendar], Customers.[First Invoice Date], Customers.[Governmental Corporate ID], 
                      Customers.[Governmental Individual ID], Customers.[High Balance LTD], Customers.[High Balance LYR], 
                      Customers.[High Balance YTD], Customers.Hold, Customers.Inactive, Customers.[Inventory Account Number], 
                      Customers.[Keep Calendar History], Customers.[Keep Distribution History], Customers.[Keep Period History], 
                      Customers.[Keep Trx History], Customers.[Last Aged], Customers.[Last Finance Charge Amount], 
                      Customers.[Last NSF Check Date], Customers.[Last Payment Amount], Customers.[Last Payment Date], 
                      Customers.[Last Statement Amount], Customers.[Last Statement Date], Customers.[Last Transaction Amount], 
                      Customers.[Last Transaction Date], Customers.[Max Writeoff Amount], Customers.[Maximum Writeoff Type], 
                      Customers.[Minimum Payment Dollar], Customers.[Minimum Payment Percent], Customers.[Minimum Payment Type], 
                      Customers.[Modified Date], Customers.[Non Current Scheduled Payments], Customers.[Note Index], 
                      Customers.[Number ADTP Documents - LYR], Customers.[Number ADTP Documents - Life], 
                      Customers.[Number ADTP Documents - Year], Customers.[Number Of NSF Checks Life], Customers.[Number Of NSF Checks YTD], 
                      Customers.[On Order Amount], Customers.[Order Fulfillment Shortage Default], Customers.[Payment Terms ID], 
                      Customers.[Phone 2], Customers.[Phone 3], Customers.[Post Results To], Customers.PriceLevel, 
                      Customers.[Primary Billto Address Code], Customers.[Primary Shipto Address Code], Customers.Priority, 
                      Customers.[Rate Type ID], Customers.Retainage, Customers.[Revalue Customer], Customers.[Sales Account Number], 
                      Customers.[Sales Order Returns Account Number], Customers.[Sales Territory], Customers.[Salesperson ID], 
                      Customers.[Send Email Statements], Customers.[Ship Complete Document], Customers.[Shipping Method], 
                      Customers.[Short Name], Customers.[Statement Address Code], Customers.[Statement Cycle], 
                      Customers.[Statement Name], Customers.[Tax Exempt 1], Customers.[Tax Exempt 2], 
                      Customers.[Tax Registration Number], Customers.[Tax Schedule ID], Customers.[Total # FC LTD], 
                      Customers.[Total # FC LYR], Customers.[Total # FC YTD], Customers.[Total # Invoices LTD], 
                      Customers.[Total # Invoices LYR], Customers.[Total # Invoices YTD], Customers.[Total Amount Of NSF Checks Life], 
                      Customers.[Total Amount Of NSF Checks YTD], Customers.[Total Bad Deb LYR], Customers.[Total Bad Debt LTD], 
                      Customers.[Total Bad Debt YTD], Customers.[Total Cash Received LTD], Customers.[Total Cash Received LYR], 
                      Customers.[Total Cash Received YTD], Customers.[Total Costs LTD], Customers.[Total Costs LYR], 
                      Customers.[Total Costs YTD], Customers.[Total Discounts Available YTD], Customers.[Total Discounts Taken LTD], 
                      Customers.[Total Discounts Taken LYR], Customers.[Total Discounts Taken YTD], Customers.[Total Finance Charges LTD], 
                      Customers.[Total Finance Charges LYR], Customers.[Total Finance Charges YTD], Customers.[Total Returns LTD], 
                      Customers.[Total Returns LYR], Customers.[Total Returns YTD], Customers.[Total Sales LTD], 
                      Customers.[Total Sales LYR], Customers.[Total Sales YTD], Customers.[Total Waived FC LTD], 
                      Customers.[Total Waived FC LYR], Customers.[Total Waived FC YTD], Customers.[Total Writeoffs LTD], 
                      Customers.[Total Writeoffs LYR], Customers.[Total Writeoffs YTD], Customers.[UPS Zone], 
                      Customers.[Unpaid Finance Charges YTD], Customers.[Unposted Cash Amount], Customers.[Unposted Other Cash Amount], 
                      Customers.[Unposted Other Sales Amount], Customers.[Unposted Sales Amount], Customers.[User Defined 1], 
                      Customers.[User Defined 2], Customers.[Write Offs LIFE], Customers.[Write Offs LYR], Customers.[Write Offs YTD], 
                      Customers.[Writeoff Account Number], dbo.vwCustEmailAddresses.EmailRecipientTo, dbo.vwCustEmailAddresses.EmailRecipientCC, 
                      dbo.vwCustEmailAddresses.EmailRecipientBC, CN00500.CRDTMGR AS [Credit Manager], 
                      CASE CN00500.PreferredContactMethod WHEN 1 THEN 'Email' WHEN 2 THEN 'Fax' WHEN 3 THEN 'Letter' WHEN 4 THEN 'Phone' ELSE 'Other' END
                       AS [Preffered Contact Method], CASE CN00500.NOMAIL WHEN 0 THEN 'Fales' ELSE 'True' END AS [Do Not Send Lettersl], 
                      CASE CN00500.Time_Zone WHEN 7 THEN 'GMT - 6:00 - Central Time' ELSE 'Other' END AS [Time Zone], 
                      CASE CN00500.CN_Credit_Control_Cycle WHEN 1 THEN 'No Credit Cycle' WHEN 2 THEN 'Weekly' WHEN 3 THEN 'Biweekly' WHEN 4 THEN 'Semimonthly'
                       WHEN 5 THEN 'Monthly' WHEN 6 THEN 'Bimonthly' ELSE 'Quarterly' END AS [Credit Control Cycle], CN00500.USRTAB01 AS [User List 1], 
                      CN00500.USRTAB09 AS [Use List 2], CN00500.USERDEF1 AS [User Text Field 1], CN00500.USERDEF2 AS [User Text Field 2], 
                      CN00500.USRDAT01 AS [User Date 1], CN00500.User_Defined_CB1 AS [User Checkbox 1], 
                      CN00500.User_Defined_CB2 AS [User Checkbox 2]
FROM         chisqp01.IC.dbo.Customers Customers LEFT OUTER JOIN
                       chisqp01.IC.dbo.CN00500 CN00500 ON Customers.[Customer Number] = CN00500.CUSTNMBR LEFT OUTER JOIN
                      dbo.vwCustEmailAddresses ON Customers.[Customer Number] = dbo.vwCustEmailAddresses.CUSTNMBR
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwTestCustomerRestricted]'
GO

CREATE VIEW [dbo].[vwTestCustomerRestricted]
AS
SELECT     SUSER_SNAME() AS suserSName, CURRENT_USER AS currentUser, SESSION_USER AS sessionUser, SYSTEM_USER AS systemUser, 
                      vwcustomers_1.[Customer Number], vwcustomers_1.[Customer Name], vwcustomers_1.[Sales Territory], vwcustomers_1.[Salesperson ID], 
                      vwcustomers_1.[Send Email Statements], vwcustomers_1.[Ship Complete Document], vwcustomers_1.[Shipping Method], vwcustomers_1.[Short Name]
FROM         dbo.vwcustomers AS vwcustomers_1 INNER JOIN
                      dbo.vwSharepoint_UserSalesTerritory ON 
                      vwcustomers_1.[Sales Territory] = dbo.vwSharepoint_UserSalesTerritory.SALSTERR COLLATE SQL_Latin1_General_CP1_CI_AS

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwSharepoint_ReportParameters_DateRange]'
GO
CREATE VIEW [dbo].[vwSharepoint_ReportParameters_DateRange]
AS
SELECT     TOP (100) PERCENT Lists.tp_Title, RTRIM(UserData.nvarchar1) AS DateToFilter, UserData.datetime1 AS DateFrom, UserData.datetime2 AS DateTo
FROM         BusinessPortal_WSS_Content_13306.dbo.Lists AS Lists INNER JOIN
                      BusinessPortal_WSS_Content_13306.dbo.UserData AS UserData ON Lists.tp_ID = UserData.tp_ListId
WHERE     (Lists.tp_Title = N'ReportParameters_dateRange')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwSharepoint_ReportParameters_DateRange_CurrentYearToDate]'
GO
CREATE VIEW [dbo].[vwSharepoint_ReportParameters_DateRange_CurrentYearToDate]
AS
SELECT     DateToFilter, MAX(DateFrom) AS DateFrom, MAX(DateTo) AS DateTo
FROM         dbo.vwSharepoint_ReportParameters_DateRange
WHERE     (DateToFilter = N'CurrentYearToDate')
GROUP BY DateToFilter
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[udfUserSalesTerritory]'
GO

CREATE FUNCTION [dbo].[udfUserSalesTerritory] 
(@AppUser varchar(64)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT     SALSTERR
FROM         dbo.vwSharepoint_UserSalesTerritory
WHERE UserId = @AppUser
GROUP BY SALSTERR
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwSalesLineItems]'
GO
ALTER view [dbo].[vwSalesLineItems]
AS
SELECT     SalesLineItems.[Customer Number], SalesLineItems.[SOP Number], SalesLineItems.[SOP Type], SalesLineItems.[Document Date], SalesLineItems.[GL Posting Date], 
              SalesLineItems.[Item Description], SalesLineItems.[Item Short Name], CASE WHEN [SOP TYPE] = 'Return' THEN [Extended Price] * - 1 ELSE [Extended Price] END AS ExtendedPrice, 
                      SalesLineItems.[Posting Status], SalesLineItems.[Void Status]
                      , CONVERT(DATETIME, CONVERT(CHAR(4), YEAR(GETDATE())) + '0101') as CYTD_DateFrom
                      , GETDATE() AS CYTD_DateTo
                      --, dbo.vwSharepoint_ReportParameters_DateRange_CurrentYearToDate.DateFrom AS CYTD_DateFrom, 
                      --dbo.vwSharepoint_ReportParameters_DateRange_CurrentYearToDate.DateTo AS CYTD_DateTo
FROM         chisqp01.IC.dbo.SalesLineItems AS SalesLineItems 
--CROSS JOIN
--                     dbo.vwSharepoint_ReportParameters_DateRange_CurrentYearToDate
WHERE     (SalesLineItems.[Void Status] = 'Normal') AND (SalesLineItems.[Posting Status] <> 'Unposted') 
AND (SalesLineItems.[GL Posting Date] BETWEEN CONVERT(DATETIME, CONVERT(CHAR(4), YEAR(GETDATE())) + '0101') AND GETDATE())
                      --dbo.vwSharepoint_ReportParameters_DateRange_CurrentYearToDate.DateFrom AND dbo.vwSharepoint_ReportParameters_DateRange_CurrentYearToDate.DateTo)

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[udfUserSalesTerritory_Customer]'
GO


CREATE FUNCTION [dbo].[udfUserSalesTerritory_Customer] 
(@AppUser varchar(64)
)
RETURNS TABLE 
AS
RETURN 
(
SELECT     vwcustomers_1.[Customer Number]
FROM         dbo.udfUserSalesTerritory(@AppUser) AS udfUserSalesTerritory INNER JOIN
                      dbo.vwcustomers AS vwcustomers_1
                      ON udfUserSalesTerritory.SALSTERR  COLLATE SQL_Latin1_General_CP1_CI_AS
 = vwcustomers_1.[Sales Territory]
GROUP BY vwcustomers_1.[Customer Number]
)

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[uspSalesLineItems_Restricted]'
GO



CREATE PROCEDURE [dbo].[uspSalesLineItems_Restricted] 
@AppUser varchar(64),@CustomerNumber char(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT     dbo.vwSalesLineItems.[Customer Number], dbo.vwSalesLineItems.[SOP Number], dbo.vwSalesLineItems.[SOP Type], dbo.vwSalesLineItems.[Document Date], 
                      dbo.vwSalesLineItems.[GL Posting Date], dbo.vwSalesLineItems.[Item Short Name], dbo.vwSalesLineItems.ExtendedPrice, dbo.vwSalesLineItems.[Posting Status], 
                      dbo.vwSalesLineItems.[Void Status], dbo.vwSalesLineItems.CYTD_DateFrom, dbo.vwSalesLineItems.CYTD_DateTo
FROM         dbo.vwSalesLineItems INNER JOIN
                      dbo.udfUserSalesTerritory_Customer(@appUser) AS udfUserSalesTerritory_1 ON 
                      dbo.vwSalesLineItems.[Customer Number] = udfUserSalesTerritory_1.[Customer Number] 
                      WHERE dbo.vwSalesLineItems.[Customer Number] = @CustomerNumber

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[udfUserSecurity]'
GO



ALTER FUNCTION [dbo].[udfUserSecurity] 
(@AppUser varchar(64)
)
RETURNS TABLE 
AS
RETURN 
(

SELECT [Customer Number]

FROM dbo.vwCustomers
WHERE (
	SELECT TOP 1 DomainName
	FROM LocalCRM.dbo.SystemUser
	WHERE SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%'
	) LIKE '%@historichotels.org%'
AND [Customer Class] = 'HISTORIC'

UNION ALL

SELECT
  [Customer Number]
FROM dbo.vwCustomers
WHERE EXISTS (SELECT
  1
FROM LocalCRM.dbo.SystemUser
INNER JOIN LocalCRM.dbo.SystemUserRoles
  ON SystemUser.SystemUserId = SystemUserRoles.SystemUserId
WHERE SystemUserRoles.RoleId IN ('E007A965-C059-DF11-9CA1-005056922997' -- PHG Exec and AMD
, '8A9F5977-1A69-DF11-A5A5-005056922997' -- PHG Finance
, '23B713F7-4845-E611-80E4-000D3A914DF8' -- PHG Exec and AMD
, '6E1B1190-4845-E611-80E4-000D3A914DF8' -- PHG Finance
, '20A03CD3-4F7C-E611-8117-000D3A9011F5' -- Contracts
)
AND
--had to add an explicit exception for Neglys Zambrano as her domain name is NZambrano-nunez but email is just NZambrano
SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%')
OR LTRIM(RTRIM([Customer Number])) COLLATE SQL_Latin1_General_CP1_CI_AS IN 
	(
		SELECT
		  Account.AccountNumber
		FROM LocalCRM.dbo.Account
		INNER JOIN LocalCRM.dbo.SystemUser
		  ON SystemUser.FullName IN (Account.phg_regionalmanageridName
		  , Account.phg_areamanageridName
		  , Account.phg_accountdirectoridname
		  , Account.phg_revenueaccountmanageridName
		  , Account.phg_regionaladministrationidName
		  , Account.[PHG_PHGContactIdName]
		  , Account.phg_corporateglobalaccountmanageridname
		  , Account.phg_touroperatorglobalaccountmanageridname
		  , Account.[phg_alliancepartnermanageridName]
		  , Account.[phg_EnterpriseAccountLeadName]
		  , Account.[phg_EnterpriseRevenueLeadName]
		  )
		WHERE SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%'
	)
)

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwReceivablesTransactions]'
GO
ALTER view [dbo].[vwReceivablesTransactions]
AS
SELECT     [Customer Number], [Customer Name], [Document Number], 
                      CASE WHEN [Document Type] = 'Allowance' THEN 'Credit Memo' ELSE [Document Type] END AS DocumentType, [Document Date], [Currency ID], 
                      [Customer PO Number], [Original Trx Amount] AS AmountUSD, [Originating Original Trx Amount] AS AmountOrig, [Document Status], [Void Status]
FROM         chisqp01.IC.dbo.ReceivablesTransactions AS ReceivablesTransactions
WHERE     ([Void Status] = 'Normal') AND ([Document Status] IN ('Posted', 'History'))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwSharepoint_eDistributionCatalog]'
GO

CREATE VIEW [dbo].[vwSharepoint_eDistributionCatalog]
AS
SELECT     TOP (100) PERCENT Lists.tp_Title, RTRIM(UserData.tp_LeafName) AS eDoc, UserData.tp_ID, UserData.ntext2 AS eDistributionStatus, UserData.ntext3 AS EmailBody, 
                      CAST(RTRIM(UserData.nvarchar10) AS varchar(21)) AS SOPNumber, RTRIM(UserData.nvarchar11) AS DocumentType, CAST(RTRIM(UserData.nvarchar29) 
                      AS varchar(15)) AS DocumentId, CAST(RTRIM(UserData.nvarchar12) AS varchar(15)) AS CustomerNumber, RTRIM(UserData.nvarchar13) AS CustomerName, 
                      RTRIM(UserData.nvarchar14) AS CustomerClass, RTRIM(UserData.nvarchar15) AS CustomerPONumber, RTRIM(UserData.nvarchar16) AS BatchNumber, 
                      RTRIM(UserData.nvarchar17) AS CurrencyId, RTRIM(UserData.nvarchar18) AS eDistOutputType, RTRIM(UserData.nvarchar19) AS EmailSubject, 
                      RTRIM(UserData.nvarchar20) AS EmailFrom, RTRIM(UserData.nvarchar21) AS EmailTo, RTRIM(UserData.nvarchar22) AS EmailCC, RTRIM(UserData.nvarchar23) 
                      AS EmailBCC, RTRIM(UserData.nvarchar24) AS PublicURL_DocLibrary, RTRIM(UserData.nvarchar26) AS PublicURL_Customer, 
                      UserData.float1 AS OriginatingDocAmount, UserData.datetime1 AS DocumentDate, UserData.ntext4 AS EmailBodyRT, UserData.ntext5 AS EmailBodyERT, 
                      RTRIM(UserData.nvarchar28) AS eDistributionStatusCode, UserData.float2 AS SortOrder, UserData.datetime2 AS eDateSent, RTRIM(UserData.nvarchar33) 
                      AS ReportGroup, RTRIM(UserData.nvarchar31) AS GPCreatedBy, RTRIM(UserData.nvarchar32) AS GPModifiedBy, UserData.tp_Created, UserData.tp_Modified, 
                      UserData.tp_DirName, UserData.tp_LeafName
FROM         BusinessPortal_WSS_Content_13306.dbo.Lists AS Lists INNER JOIN
                      BusinessPortal_WSS_Content_13306.dbo.UserData AS UserData ON Lists.tp_ID = UserData.tp_ListId
--WHERE     (Lists.tp_Title = N'eDistribution Catalog')

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[uspSharepoint_eDistributionDocList_byDocNumber]'
GO






CREATE PROCEDURE [dbo].[uspSharepoint_eDistributionDocList_byDocNumber] 
@DocumentNumber varchar(21),@CustomerNumber char(15)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT     SOPNumber, CustomerNumber,  tp_DirName, tp_LeafName, DocumentType, DocumentId, CustomerPONumber, BatchNumber, CurrencyId, 
                      eDistOutputType, EmailTo, EmailCC, EmailBCC, OriginatingDocAmount, eDistributionStatusCode, eDateSent

--FROM         eDistributionApp.dbo.vwSharepoint_eDistributionCatalog
FROM         dbo.vwSharepoint_eDistributionCatalog
WHERE     (SOPNumber = @DocumentNumber) AND (CustomerNumber = @CustomerNumber)
END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwCustomerAddress]'
GO
ALTER view [dbo].[vwCustomerAddress]
AS
SELECT     TOP (100) PERCENT CustomerAddress.[Customer Number], CustomerAddress.[Customer Name], CustomerAddress.[Address 1], 
                      CustomerAddress.[Address 2], CustomerAddress.City, CustomerAddress.State, CustomerAddress.Zip, 
                      CustomerAddress.[Phone 1], CustomerAddress.[Accounts Receivable Account Number], CustomerAddress.[Address 3], 
                      CustomerAddress.[Address Code], CustomerAddress.[Balance Type], CustomerAddress.[Bank Branch], 
                      CustomerAddress.[Bank Name], CustomerAddress.[COGS Account Number], CustomerAddress.[Cash Account Number], 
                      CustomerAddress.[Checkbook ID], CustomerAddress.Comment1, CustomerAddress.Comment2, 
                      CustomerAddress.[Contact Person], CustomerAddress.[Corporate Customer Number], CustomerAddress.Country, 
                      CustomerAddress.[Created Date], CustomerAddress.[Credit Card Exp Date], CustomerAddress.[Credit Card ID], 
                      CustomerAddress.[Credit Limit Amount], CustomerAddress.[Credit Limit Period], CustomerAddress.[Credit Limit Period Amount], 
                      CustomerAddress.[Credit Limit Type], CustomerAddress.[Writeoff Account Number], dbo.vwCustEmailAddresses.EmailRecipientTo, 
                      dbo.vwCustEmailAddresses.EmailRecipientCC, dbo.vwCustEmailAddresses.EmailRecipientBC, CN00500.CRDTMGR AS [Credit Manager], 
                      CASE CN00500.PreferredContactMethod WHEN 1 THEN 'Email' WHEN 2 THEN 'Fax' WHEN 3 THEN 'Letter' WHEN 4 THEN 'Phone' ELSE 'Other' END
                       AS [Preffered Contact Method], CASE CN00500.NOMAIL WHEN 0 THEN 'Fales' ELSE 'True' END AS [Do Not Send Lettersl], 
                      CASE CN00500.Time_Zone WHEN 7 THEN 'GMT - 6:00 - Central Time' ELSE 'Other' END AS [Time Zone], 
                      CASE CN00500.CN_Credit_Control_Cycle WHEN 1 THEN 'No Credit Cycle' WHEN 2 THEN 'Weekly' WHEN 3 THEN 'Biweekly' WHEN 4 THEN 'Semimonthly'
                       WHEN 5 THEN 'Monthly' WHEN 6 THEN 'Bimonthly' ELSE 'Quarterly' END AS [Credit Control Cycle], CN00500.USRTAB01 AS [User List 1], 
                      CN00500.USRTAB09 AS [Use List 2], CN00500.USERDEF1 AS [User Text Field 1], CN00500.USERDEF2 AS [User Text Field 2], 
                      CN00500.USRDAT01 AS [User Date 1], CN00500.User_Defined_CB1 AS [User Checkbox 1], 
                      CN00500.User_Defined_CB2 AS [User Checkbox 2], CustomerAddress.[Currency ID], CustomerAddress.[Customer Class], 
                      CustomerAddress.[Customer Discount], CustomerAddress.[Default Cash Account Type], CustomerAddress.[Discount Grace Period], 
                      CustomerAddress.[Discounts Avail Account Number], CustomerAddress.[Discounts Taken Account Number], 
                      CustomerAddress.[Document Format ID], CustomerAddress.[Due Date Grace Period], CustomerAddress.Fax, 
                      CustomerAddress.[Finance Charge Account Number], CustomerAddress.[Finance Charge Amt Type], 
                      CustomerAddress.[Finance Charge Dollar], CustomerAddress.[Finance Charge ID], CustomerAddress.[Finance Charge Percent], 
                      CustomerAddress.[First Invoice Date], CustomerAddress.[Governmental Corporate ID], 
                      CustomerAddress.[Governmental Individual ID], CustomerAddress.Hold, CustomerAddress.INet1, CustomerAddress.INet2, 
                      CustomerAddress.INet3, CustomerAddress.INet4, CustomerAddress.INet5, CustomerAddress.INet6, 
                      CustomerAddress.INet7, CustomerAddress.INet8, CustomerAddress.Inactive, CustomerAddress.[Inventory Account Number],
                       CustomerAddress.[Keep Calendar History], CustomerAddress.[Keep Distribution History], CustomerAddress.[Keep Period History], 
                      CustomerAddress.[Keep Trx History], CustomerAddress.[Max Writeoff Amount], CustomerAddress.[Maximum Writeoff Type], 
                      CustomerAddress.[Minimum Payment Dollar], CustomerAddress.[Minimum Payment Percent], 
                      CustomerAddress.[Minimum Payment Type], CustomerAddress.[Modified Date], CustomerAddress.[Note Index], 
                      CustomerAddress.[Order Fulfillment Shortage Default], CustomerAddress.[Payment Terms ID], CustomerAddress.[Phone 2], 
                      CustomerAddress.[Phone 3], CustomerAddress.[Post Results To], CustomerAddress.PriceLevel, 
                      CustomerAddress.[Primary Billto Address Code], CustomerAddress.[Primary Shipto Address Code], CustomerAddress.Priority, 
                      CustomerAddress.[Rate Type ID], CustomerAddress.[Revalue Customer], CustomerAddress.[Sales Account Number], 
                      CustomerAddress.[Sales Territory], CustomerAddress.[Salesperson ID], CustomerAddress.[Salesperson ID from Customer Master], 
                      CustomerAddress.[Shipping Method], CustomerAddress.[Short Name], CustomerAddress.[Site ID], 
                      CustomerAddress.[Statement Address Code], CustomerAddress.[Statement Cycle], CustomerAddress.[Statement Name], 
                      CustomerAddress.[Tax Exempt 1], CustomerAddress.[Tax Exempt 2], CustomerAddress.[Tax Registration Number], 
                      CustomerAddress.[Tax Schedule ID], CustomerAddress.[Territory ID], CustomerAddress.[UPS Zone], 
                      CustomerAddress.[User Defined 1], CustomerAddress.[User Defined 1 from Customer Master], CustomerAddress.[User Defined 2], 
                      CustomerAddress.[User Defined 2 from Customer Master], SY01200.INETINFO
FROM         chisqp01.IC.dbo.CustomerAddress AS CustomerAddress LEFT OUTER JOIN
             chisqp01.IC.dbo.SY01200 AS SY01200 ON CustomerAddress.[Customer Number] = SY01200.Master_ID AND 
             CustomerAddress.[Address Code] = SY01200.ADRSCODE LEFT OUTER JOIN
             chisqp01.IC.dbo.CN00500 AS CN00500 ON CustomerAddress.[Customer Number] = CN00500.CUSTNMBR LEFT OUTER JOIN
             dbo.vwCustEmailAddresses ON CustomerAddress.[Customer Number] = dbo.vwCustEmailAddresses.CUSTNMBR
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwSalesTerritories_AssignedAndUnassigned]'
GO
CREATE VIEW [dbo].[vwSalesTerritories_AssignedAndUnassigned]
AS
SELECT     TOP (100) PERCENT ISNULL(CustSalesTerritory.SalesTerritory, '(NotInGP)') AS GPSalesTerritory, ISNULL(CustSalesTerritory.CountOfMembers, 0) AS CountOfMembers,
                       ISNULL(dbo.vwSharepoint_UserSalesTerritory.SALSTERR, '(NoUserAssigned)') AS UserSalesTerritory, dbo.vwSharepoint_UserSalesTerritory.UserId
FROM         (SELECT     COUNT(*) AS CountOfMembers, SalesTerritory
                       FROM          dbo.vwCustomerSalesTerritories
                       GROUP BY SalesTerritory) AS CustSalesTerritory FULL OUTER JOIN
                      dbo.vwSharepoint_UserSalesTerritory ON 
                      CustSalesTerritory.SalesTerritory = dbo.vwSharepoint_UserSalesTerritory.SALSTERR COLLATE SQL_Latin1_General_CP1_CI_AS
ORDER BY CustSalesTerritory.SalesTerritory, dbo.vwSharepoint_UserSalesTerritory.SALSTERR
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vwCollectionNotes]'
GO
ALTER view [dbo].[vwCollectionNotes]
AS
SELECT     TOP (100) PERCENT Notes.CUSTNMBR AS CustomerCode, Cust.CUSTNAME AS CustomerName, Cust.CUSTCLAS AS CustomerClass, 
                      CustSetp.CRDTMGR AS CollectorCode, Manager.CollectorName, Manager.Position_Name AS CollectorPosition, Manager.CN_Email_Address AS CollectorEmail, 
                      Manager.CN_Collector_Phone1 AS CollectorPhone1, Manager.CN_Collector_Phone2 AS CollectorPhone2, Manager.CN_Collector_Fax AS CollectorFax, 
                      CASE WHEN CustSetp.PreferredContactMethod = 1 THEN 'Email' WHEN CustSetp.PreferredContactMethod = 2 THEN 'Fax' WHEN CustSetp.PreferredContactMethod = 3
                       THEN 'Letter' WHEN CustSetp.PreferredContactMethod = 4 THEN 'Phone' ELSE 'Other' END AS PreferredContactMethod, 
                      CustSetp.ADRSCODE AS CollectionAddressCode, CollectionAddress.[Address 1] AS CollectionAddress1, CollectionAddress.[Address 2] AS CollectionAddress2, 
                      CollectionAddress.City AS CollectionAddressCity, CollectionAddress.State AS CollectionAddressState, CollectionAddress.Zip AS CollectionAddressZip, 
                      CollectionAddress.[Phone 1] AS CollectionPhone1, CollectionAddress.[Address 3] AS CollectionAddress3, 
                      CollectionAddress.[Contact Person] AS CollectionContactPerson, Cust.CPRCSTNM AS ParentCustomerCode, CASE WHEN Notes.Contact_Date = '1/1/1900' THEN NULL 
                      ELSE Notes.Contact_Date END AS ContactDate, CASE WHEN Notes.Contact_Time = '1/1/1900' THEN NULL ELSE Notes.Contact_Time END AS ContactTime, 
                      Notes.RevisionNumber, Notes.Caller_ID_String AS CallerID, Notes.Action_Promised AS ActionPromisedCode, 
                      Action.Action_Promised_Descripti AS ActionPromisedDescription, CASE WHEN Notes.Action_Date = '1/1/1900' THEN NULL ELSE Notes.Action_Date END AS ActionDate,
                       Notes.Action_Assigned_To AS ActionAssignedTo, CASE WHEN [notes].[Action_Completed] = 0 THEN 'No' ELSE 'Yes' END AS ActionCompleted, 
                      CASE WHEN Notes.Action_Completed_Date = '1/1/1900' THEN NULL ELSE Notes.Action_Completed_Date END AS ActionCompletedDate, 
                      CASE WHEN Notes.Action_Completed_Time = '1/1/1900' THEN NULL ELSE Notes.Action_Completed_Time END AS ActionCompletedTime, 
                      Notes.Amount_Promised AS AmountPromised, Notes.Amount_Received AS AmountReceived, Notes.Note_Display_String AS Note, Notes.CNTCPRSN AS ContactPerson,
                       Notes.ADRSCODE AS ContactAddress, Notes.USERDEF1 AS Language, Notes.USERDEF2 AS UserDefined2, CASE WHEN Notes.USRDAT01 = '1/1/1900' THEN NULL 
                      ELSE Notes.USRDAT01 END AS TermDate, 
                      CASE WHEN Notes.PRIORT = 1 THEN 'Low' WHEN Notes.PRIORT = 2 THEN 'Normal' WHEN Notes.PRIORT = 3 THEN 'High' WHEN Notes.PRIORT = 0 THEN ' ' END AS Priority,
                       Notes.NOTEINDX, Notes.DEX_ROW_ID, FullNotes.TXTFIELD AS FullNotes, Cust.SALSTERR AS Territory, 
                      CASE WHEN Notes.ActionType = 1 THEN 'None' WHEN Notes.ActionType = 2 THEN 'Dispute' WHEN Notes.ActionType = 3 THEN 'Promise To Pay' WHEN Notes.ActionType
                       = 4 THEN 'Special' END AS ActionType, ParentCust.CUSTNAME AS ParentCustomerName
--FROM         lnkEDistGP.ic.dbo.cn00500 AS CustSetp LEFT OUTER JOIN
--                      lnkEDistGP.dynamics.dbo.CN01400 AS Manager ON CustSetp.CRDTMGR = Manager.CollectorID LEFT OUTER JOIN
--                      lnkEDistGP.IC.dbo.CustomerAddress AS CollectionAddress ON CustSetp.CUSTNMBR = CollectionAddress.[Customer Number] AND 
--                      CustSetp.ADRSCODE = CollectionAddress.[Address Code] LEFT OUTER JOIN
--                      lnkEDistGP.ic.dbo.rm00101 AS ParentCust RIGHT OUTER JOIN
--                      lnkEDistGP.ic.dbo.rm00101 AS Cust ON ParentCust.CUSTNMBR = Cust.CPRCSTNM ON CustSetp.CUSTNMBR = Cust.CUSTNMBR RIGHT OUTER JOIN
--                      lnkEDistGP.ic.dbo.CN00200 AS Action RIGHT OUTER JOIN
--                      lnkEDistGP.ic.dbo.CN00100 AS Notes ON Action.ActionType = Notes.ActionType AND Action.Action_Promised = Notes.Action_Promised ON 
--                      CustSetp.CUSTNMBR = Notes.CUSTNMBR LEFT OUTER JOIN
--                      lnkEDistGP.ic.dbo.CN00300 AS FullNotes ON Notes.NOTEINDX = FullNotes.NOTEINDX
FROM         chisqp01.ic.dbo.cn00500 AS CustSetp LEFT OUTER JOIN
                      chisqp01.dynamics.dbo.CN01400 AS Manager ON CustSetp.CRDTMGR = Manager.CollectorID LEFT OUTER JOIN
                      chisqp01.IC.dbo.CustomerAddress AS CollectionAddress ON CustSetp.CUSTNMBR = CollectionAddress.[Customer Number] AND 
                      CustSetp.ADRSCODE = CollectionAddress.[Address Code] LEFT OUTER JOIN
                      chisqp01.ic.dbo.rm00101 AS ParentCust RIGHT OUTER JOIN
                      chisqp01.ic.dbo.rm00101 AS Cust ON ParentCust.CUSTNMBR = Cust.CPRCSTNM ON CustSetp.CUSTNMBR = Cust.CUSTNMBR RIGHT OUTER JOIN
                      chisqp01.ic.dbo.CN00200 AS Action RIGHT OUTER JOIN
                      chisqp01.ic.dbo.CN00100 AS Notes ON Action.ActionType = Notes.ActionType AND Action.Action_Promised = Notes.Action_Promised ON 
                      CustSetp.CUSTNMBR = Notes.CUSTNMBR LEFT OUTER JOIN
                      chisqp01.ic.dbo.CN00300 AS FullNotes ON Notes.NOTEINDX = FullNotes.NOTEINDX
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[index_fragmentation]'
GO
CREATE TABLE [dbo].[index_fragmentation]
(
[Schema] [sys].[sysname] NOT NULL,
[Table] [sys].[sysname] NOT NULL,
[Index] [sys].[sysname] NULL,
[avg_fragmentation_in_percent] [float] NULL,
[page_count] [bigint] NULL,
[create_date] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vwSharepoint_eDistributionCatalog_DocumentCountByDocumentNumber]'
GO


CREATE VIEW [dbo].[vwSharepoint_eDistributionCatalog_DocumentCountByDocumentNumber]
AS
SELECT     SOPNumber, CustomerNumber, COUNT(*) AS CountOfEDistDocs, MIN(eDoc) AS FirsteDocName
FROM         eDistributionApp.dbo.vwSharepoint_eDistributionCatalog
--FROM         dbo.vwSharepoint_eDistributionCatalog
GROUP BY SOPNumber, CustomerNumber


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating extended properties'
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ReceivablesTransactions"
            Begin Extent = 
               Top = 13
               Left = 16
               Bottom = 167
               Right = 344
            End
            DisplayFlags = 280
            TopColumn = 284
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwReceivablesTransactions', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwReceivablesTransactions', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "SalesLineItems"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 146
               Right = 397
            End
            DisplayFlags = 280
            TopColumn = 512
         End
         Begin Table = "vwSharepoint_ReportParameters_DateRange_CurrentYearToDate"
            Begin Extent = 
               Top = 6
               Left = 435
               Bottom = 117
               Right = 622
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwSalesLineItems', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwSalesLineItems', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[26] 4[23] 2[17] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwSharepoint_UserSalesTerritory"
            Begin Extent = 
               Top = 7
               Left = 275
               Bottom = 100
               Right = 492
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CustSalesTerritory"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 84
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1815
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2775
         Alias = 1890
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwSalesTerritories_AssignedAndUnassigned', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwSalesTerritories_AssignedAndUnassigned', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Lists"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 291
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UserData"
            Begin Extent = 
               Top = 6
               Left = 329
               Bottom = 114
               Right = 527
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 2340
         Width = 1500
         Width = 2370
         Width = 2550
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_ReportParameters_DateRange', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_ReportParameters_DateRange', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwSharepoint_ReportParameters_DateRange"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 189
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_ReportParameters_DateRange_CurrentYearToDate', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_ReportParameters_DateRange_CurrentYearToDate', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Lists"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 291
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "UserData"
            Begin Extent = 
               Top = 6
               Left = 329
               Bottom = 114
               Right = 527
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 35
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
', 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_UserSalesTerritory', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane2', N'         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_UserSalesTerritory', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_UserSalesTerritory', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwSharepoint_eDistributionCatalog (eDistributionApp.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 250
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 4140
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_eDistributionCatalog_DocumentCountByDocumentNumber', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=1
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwSharepoint_eDistributionCatalog_DocumentCountByDocumentNumber', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[35] 4[19] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vwcustomers_1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 299
            End
            DisplayFlags = 280
            TopColumn = 107
         End
         Begin Table = "vwSharepoint_UserSalesTerritory"
            Begin Extent = 
               Top = 6
               Left = 337
               Bottom = 99
               Right = 488
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 185
         Width = 284
         Width = 2475
         Width = 1500
         Width = 1500
         Width = 1860
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Wi', 'SCHEMA', N'dbo', 'VIEW', N'vwTestCustomerRestricted', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_addextendedproperty N'MS_DiagramPane2', N'dth = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1995
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'vwTestCustomerRestricted', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'vwTestCustomerRestricted', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
