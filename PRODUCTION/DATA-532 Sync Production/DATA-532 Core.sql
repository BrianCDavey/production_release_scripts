USE Core
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Core    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Core

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 12:05:26 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[sysdiagrams]'
GO
DROP TABLE [dbo].[sysdiagrams]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[iata_processIataNumber]'
GO
DROP PROCEDURE [dbo].[iata_processIataNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[MonthlyAggrigateByHotel]'
GO
DROP PROCEDURE [Reporting].[MonthlyAggrigateByHotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[iata_prepIataGroup]'
GO
DROP PROCEDURE [dbo].[iata_prepIataGroup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[HotelLookupByCode]'
GO
DROP PROCEDURE [Reporting].[HotelLookupByCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[HotelLookup]'
GO
DROP PROCEDURE [Reporting].[HotelLookup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [Reporting].[FindHotelByRes]'
GO
DROP PROCEDURE [Reporting].[FindHotelByRes]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[hotelsReporting]'
GO


ALTER VIEW [dbo].[hotelsReporting]
AS
SELECT	h.crmGuid,h.code,dbo.hotels_Synxis.synxisID,h.hotelName,h.physicalAddress1,h.physicalAddress2,h.physicalCity,h.[state], 
		account.phg_stateprovinceidname AS subAreaName,h.country,account.phg_countryidname AS shortName,h.postalCode,
		CAST(h.geoLocation AS nvarchar(MAX)) AS geoLocationWKT, 
		h.yearBuilt,h.yearLastRenovated,h.floors,h.primaryPhone,h.primaryEmail,h.primaryWebsite,h.openingDate,h.totalRooms,h.currencyCode,
		h.competitiveSet,h.geographicRegionCode,h.geographicRegionCode AS geographicRegionName,
		COALESCE (activeBrands.code, inactiveBrands.code) AS mainBrandCode, 
		COALESCE (account.phg_regionalmanageridName, N'None') AS phg_regionalmanageridName, 
		COALESCE (account.phg_areamanageridName, N'None') AS phg_areamanageridName, 
		account.statuscodename, 
		COALESCE (account.phg_revenueaccountmanageridName, N'None') AS phg_revenueaccountmanageridName, 
		COALESCE (account.phg_regionaladministrationidName, N'None') AS phg_regionaladministrationidName, 
		account.PHG_InvoicePastDueStatus, 
		COALESCE (account.phg_accountdirectoridName, N'None') AS phg_AccountManagerName,
		COALESCE (account.phg_marketingcontactidName, N'None') AS phg_marketingcontactidName, 
		COALESCE (account.phg_accountsreceivablecontactidName, N'None') AS phg_accountsreceivablecontactidName,
		COALESCE (account.phg_ipreferexecutiveadminidName, N'None') as phg_ipreferexecutiveadminidName,
		COALESCE (account.PHG_FinanceRegion, N'None') as PHG_FinanceRegion,
		dbo.hotels_OpenHospitality.openHospitalityCode
FROM dbo.hotels h
	LEFT JOIN
		(
			SELECT hb.hotelCode, MIN(subBrands.heirarchy) AS mainHeirarchy
			FROM dbo.hotels_brands hb
				INNER JOIN dbo.brands AS subBrands ON hb.brandCode = subBrands.code
			WHERE (hb.endDatetime IS NULL)
				AND (hb.startDatetime <= GETDATE())
				OR (GETDATE() BETWEEN hb.startDatetime AND hb.endDatetime)
			GROUP BY hb.hotelCode
		) AS hotelActiveBrands ON h.code = hotelActiveBrands.hotelCode
	LEFT JOIN dbo.brands AS activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy
	LEFT JOIN
		(
			SELECT hotelCode, MIN(heirarchy) AS mainHeirarchy
			FROM
				(
					SELECT hb2.hotelCode, hb2.brandCode, dbo.brands.heirarchy
					FROM dbo.hotels_brands AS hb2
						INNER JOIN
							(
								SELECT hotelCode, MAX(endDatetime) AS maxEnd
								FROM dbo.hotels_brands AS hb1
								GROUP BY hotelCode
							) AS maxDate ON hb2.endDatetime = maxDate.maxEnd
						INNER JOIN dbo.brands ON hb2.brandCode = dbo.brands.code
					GROUP BY hb2.hotelCode, hb2.brandCode, dbo.brands.heirarchy
				) AS maxBrands
			GROUP BY hotelCode
		) AS hotelInactiveBrands ON h.code = hotelInactiveBrands.hotelCode
	LEFT JOIN dbo.brands AS inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy
	LEFT JOIN LocalCRM.dbo.Account AS account ON h.crmGuid = account.AccountId
	LEFT JOIN dbo.hotels_OpenHospitality ON h.code = dbo.hotels_OpenHospitality.hotelCode
	LEFT JOIN dbo.hotels_Synxis ON h.code = dbo.hotels_Synxis.hotelCode
	LEFT JOIN dbo.geographicRegions ON h.geographicRegionCode = dbo.geographicRegions.code

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[hotelMCMStoCore]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 04/15/2010
-- Description:	Inserts existing hotel from MCMS into Core
-- =============================================
CREATE PROCEDURE [dbo].[hotelMCMStoCore] 
	-- Add the parameters for the stored procedure here
	@pkMemberID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Core.dbo.hotels 
SELECT null, LEFT(sMemberCode, 10), sMemberName, sMemberAddress1, sMemberAddress2 + ' ' + sMemberAddress3 + ' ' + sMemberAddress4,
sCity, sStateINDEName, sCountryINDEName, sPostalCode, 
geography::STPointFromText('POINT(' + CAST(nLongitude as VARCHAR(20)) + ' ' + CAST(nLatitude as VARCHAR(20)) + ')', 4326),
CAST(sBuiltYr as int), null, nnbrfloors, sMemberPhone1, sMemberEmail, sURLAlias, null, nNbrRoomsPromoted, null, null, null, sMemberName
FROM SQLN04.MemberContent.dbo.Member INNER JOIN
SQLN04.MemberContent.dbo.StateINDE ON Member.fkStateINDEID = StateINDE.pkStateINDEID INNER JOIN
SQLN04.MemberContent.dbo.CountryINDE ON Member.fkCountryINDEID = CountryINDE.pkCountryINDEID
WHERE Member.pkMemberID = @pkMemberID;

INSERT INTO Core.dbo.hotels_MCMS
SELECT code, pkMemberID
FROM Core.dbo.hotels INNER JOIN
SQLN04.MemberContent.dbo.Member on hotels.code = LEFT(Member.sMemberCode, 10)
WHERE Member.pkMemberID = @pkMemberID;

INSERT INTO Core.dbo.hotels_brands
SELECT hotelcode, sBrandCode, Member.dCreated, Member.dEnd
FROM Core.dbo.hotels_MCMS INNER JOIN
SQLN04.MemberContent.dbo.Member ON hotels_MCMS.fkMemberID = Member.pkMemberID 
INNER JOIN SQLN04.MemberContent.dbo.Brand ON Member.fkBrandID = brand.pkBrandID
WHERE hotelcode + sBrandCode NOT IN (SELECT hotelcode + brandCode FROM Core.dbo.hotels_brands);

	
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[JayGroupHotelReport]'
GO


CREATE PROCEDURE [dbo].[JayGroupHotelReport]

	@AsOfDate DATETIME

AS
BEGIN

	SET NOCOUNT ON;
	
	IF (@AsOfDate IS NULL) OR (@AsOfDate = NULL)
		BEGIN
			SET @AsOfDate = CONVERT(CHAR(10), GETDATE(), 110)
		END
		

	--  I realize this might look silly separating the string of text like this, but did it so it
	--  is easier to read and/or modify if/when the time comes
	SELECT		'Hotel Name|' +
				'Hotel Property Code|' +
				'Hotel Primary Brand Code|' +
				'AMD Name|' +
				'RD Name|' +
				'Hotel Address1|' +
				'Hotel Address2|' +
				'Hotel City|' +
				'Hotel Postal Code|' +
				'Hotel State|' +
				'Hotel Latitude|' +
				'Hotel Longitude|' +
				'Hotel ISO Code Country|' +
				'iPrefer Contact|' +
				'Hotel Status|' +
				'OBR'


	SELECT		REPLACE(REPLACE(REPLACE(WKT.hotelName,'|','') + '|' +
				REPLACE(WKT.code,'|','') + '|' +
				case when WKT.mainBrandCode is null then '' else REPLACE(WKT.mainBrandCode,'|','') end + '|' +
				(case when A.phg_areamanageridName is null then '' else REPLACE(A.phg_areamanageridName,'|','') end) COLLATE database_default + '|' +
				(case when A.phg_regionalmanageridName is null then '' else REPLACE(A.phg_regionalmanageridName,'|','') end) COLLATE database_default + '|' +
				case when WKT.[physicalAddress1] is null then '' else REPLACE(WKT.[physicalAddress1],'|','') end + '|' +
				case when WKT.[physicalAddress2] is null then '' else REPLACE(WKT.[physicalAddress2],'|','') end + '|' +
				case when WKT.[physicalCity] is null then '' else REPLACE(WKT.[physicalCity],'|','') end + '|' +
				case when WKT.[postalCode] is null then '' else REPLACE(WKT.[postalCode],'|','') end + '|' +
				case when WKT.[state] is null then '' else REPLACE(WKT.[state],'|','') end + '|' +
				case when H.geoLocation is null then '|' else CAST(H.geoLocation.Lat AS varchar(15)) + '|' + CAST(H.geoLocation.Long AS varchar(15)) end + '|' +
				case when H.[country] is null then '' else REPLACE(H.[country],'|','') end + '|' +
				ISNULL(iPrefer.email, '') + '|' +
				ISNULL(CAST(active.hotelStatus AS VARCHAR(8)), 'Inactive')
				, CHAR(10), ''), CHAR(13), '') AS hotelRecord

	FROM		Core.dbo.hotels H
				JOIN
				Core.dbo.hotelsWKT WKT
				on
				H.code = WKT.code
				JOIN
				MIASQP01.PHG_MSCRM.dbo.Account A
				ON
				A.AccountId = WKT.crmGuid
				LEFT JOIN
				(	SELECT		DISTINCT H.code
					,			'Active' AS hotelStatus
					
					FROM		Core.dbo.hotels H
								JOIN
								Core.dbo.hotels_brands HB
								ON
								H.code = HB.hotelCode
								
					WHERE		EXISTS(	SELECT		*
									
										FROM		Core.dbo.hotels H1
													JOIN
													Core.dbo.hotels_brands HB1
													ON
													H1.code = HB1.hotelCode
													
										WHERE		H1.code = H.code
													AND
													GETDATE() BETWEEN HB1.startDatetime AND ISNULL(HB1.endDatetime, '01/01/2999')
										)
				) AS active
				ON
				H.code = active.code
				LEFT JOIN
				(	SELECT		H.code
					,			C.EMailAddress1 AS email
					
					FROM		Core.dbo.hotels H
								JOIN
								MIASQP01.PHG_MSCRM.dbo.PHG_contactrole CR
								ON
								H.crmGuid = CR.phg_accountid
								JOIN
								MIASQP01.PHG_MSCRM.dbo.Contact C
								ON
								CR.PHG_contactid = C.ContactID
								
					WHERE		CR.phg_contactroleid =	CASE WHEN	EXISTS(	SELECT	*
				
																			FROM	Core.dbo.hotels PHG_H
																					JOIN
																					MIASQP01.PHG_MSCRM.dbo.PHG_contactrole PHG_CR
																					ON
																					PHG_H.crmGuid = PHG_CR.phg_accountid
																					
																			WHERE	PHG_H.code = H.code
																					AND
																					PHG_CR.PHG_contactroletypeId = '2BE43303-0320-DF11-B51D-005056922997'
																					AND
																					@AsOfDate
																						BETWEEN
																						CONVERT(VARCHAR(10), ISNULL(PHG_CR.PHG_StartDate, '1/1/1900'), 120)
																						AND
																						CONVERT(VARCHAR(10), ISNULL(PHG_CR.PHG_EndDate, '12/31/2100'), 120)
																			)
																THEN	(	SELECT	TOP 1 PHG_CR.phg_contactroleid
																
																			FROM	Core.dbo.hotels PHG_H
																					JOIN
																					MIASQP01.PHG_MSCRM.dbo.PHG_contactrole PHG_CR
																					ON
																					PHG_H.crmGuid = PHG_CR.phg_accountid
																
																			WHERE	PHG_H.code = H.code
																					AND
																					PHG_CR.PHG_contactroletypeId = '2BE43303-0320-DF11-B51D-005056922997'
																					AND
																					@AsOfDate
																						BETWEEN
																						CONVERT(VARCHAR(10), ISNULL(PHG_CR.PHG_StartDate, '1/1/1900'), 120)
																						AND
																						CONVERT(VARCHAR(10), ISNULL(PHG_CR.PHG_EndDate, '12/31/2100'), 120)
																		)
															ELSE	(	SELECT	TOP 1 PHG_CR.phg_contactroleid
															
																		FROM	Core.dbo.hotels PHG_H
																				JOIN
																				MIASQP01.PHG_MSCRM.dbo.PHG_contactrole PHG_CR
																				ON
																				PHG_H.crmGuid = PHG_CR.phg_accountid
																				
																		WHERE	PHG_H.code = H.code
																				AND
																				PHG_CR.PHG_contactroletypeId = '25E43303-0320-DF11-B51D-005056922997'
																				AND
																				@AsOfDate
																					BETWEEN
																					CONVERT(VARCHAR(10), ISNULL(PHG_CR.PHG_StartDate, '1/1/1900'), 120)
																					AND
																					CONVERT(VARCHAR(10), ISNULL(PHG_CR.PHG_EndDate, '12/31/2100'), 120)
																	)
														END
				) AS iPrefer
				ON
				H.code = iPrefer.code

END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[selBookingParameters]'
GO

CREATE PROCEDURE [dbo].[selBookingParameters]
@style int
,@hotelCode nvarchar(10)
AS 
set nocount on
IF ((select bookingEngineIndex from hotels_bookingEngines where hotelCode = @hotelCode) = 1)
	BEGIN	
		
		if not exists(select style from bookingPegasus where style = @style and hotelCode = @hotelCode)			
			set @style=0
						
		select P.InstanceId,P.lookAndFeelId, P.propertyCodeType, B.gdsChainCode, e.bookingEngineIndex
		from bookingPegasus P, hotels_brands HB, brands B, hotels_bookingEngines e	  
		where P.hotelCode = @hotelCode and 
		      P.style = @style and 
		      P.hotelCode = HB.hotelCode and 
		      HB.brandCode = B.code and
		      e.hotelCode = @hotelCode
	END
ELSE
	BEGIN
				
		if not exists(select style from bookingSynxis where style = @style and hotelCode = @hotelCode)			
			set @style=0

	    select s.lookAndFeelId, e.bookingEngineIndex 
        from bookingSynxis s, hotels_bookingEngines e 
        where e.hotelCode = @hotelCode and 
              s.hotelCode=@hotelCode and 
              s.style = @style
              
	END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ffGuestNameTest]'
GO



-- =============================================
-- Author:		Kris Scott
-- Create date: 02/16/10
-- Description:	the underlying query for the Frequent Flier Invoice Back up document (guest name report) for the GP TEST company
-- =============================================
CREATE PROCEDURE [dbo].[ffGuestNameTest] 
	-- Add the parameters for the stored procedure here
	@SOPNumber nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT 
COALESCE(sopHeaderWork.DOCDATE, sopHeaderHistory.DOCDATE) as invoiceDate,
COALESCE(sopHeaderWork.CURNCYID, sopHeaderHistory.CURNCYID) as currencyCode,
ihamms.hotelCode,
customerMaster.STMTNAME as hotelName,
      [guestFirstName],
      [guestLastName],
  RTRIM(standardItemCodeMaster.ITEMNMBR) AS standardItemCode,
  standardItemCodeMaster.ITEMDESC as standardItemDescription,
      [standardStaysQuantity],
      COALESCE(standardLineItemWork.ORUNTPRC,standardLineItemHistory.ORUNTPRC) as standardUnitPrice, --or is it ORUNTPRC?
  RTRIM(bonusItemCodeMaster.ITEMNMBR) AS bonusItemCode,
  bonusItemCodeMaster.ITEMDESC as bonusItemDescription,
      [bonusStaysQuantity],
      COALESCE(bonusLineItemWork.ORUNTPRC,bonusLineItemHistory.ORUNTPRC) as bonusUnitPrice
      ,[confirmationNumber]
      ,[airlineNumber]
      ,[arrivalDate]
      ,[departureDate]
      ,[roomNights]
FROM Core.dbo.ihamms
INNER JOIN MIASQP03.TEST.dbo.RM00101 AS customerMaster ON ihamms.hotelCode = LTRIM(RTRIM(customerMaster.CUSTNMBR))
LEFT OUTER JOIN MIASQP03.TEST.dbo.SOP10100 AS sopHeaderWork ON ihamms.sopNumber = sopHeaderWork.SOPNUMBE
LEFT OUTER JOIN MIASQP03.TEST.dbo.SOP30200 AS sopHeaderHistory ON ihamms.sopNumber = sopHeaderHistory.SOPNUMBE
LEFT OUTER JOIN MIASQP03.TEST.dbo.IV00101 AS standardItemCodeMaster ON ihamms.standardStaysItemCode = LTRIM(RTRIM(standardItemCodeMaster.ITEMNMBR))
LEFT OUTER JOIN MIASQP03.TEST.dbo.SY03900 AS standardItemCodeNotes ON standardItemCodeMaster.NOTEINDX = standardItemCodeNotes.NOTEINDX
LEFT OUTER JOIN MIASQP03.TEST.dbo.IV00101 AS bonusItemCodeMaster ON ihamms.standardStaysItemCode = LTRIM(RTRIM(bonusItemCodeMaster.ITEMNMBR))
LEFT OUTER JOIN MIASQP03.TEST.dbo.SY03900 AS bonusItemCodeNotes ON bonusItemCodeMaster.NOTEINDX = bonusItemCodeNotes.NOTEINDX
LEFT OUTER JOIN MIASQP03.TEST.dbo.SOP10200 AS standardLineItemWork ON standardLineItemWork.SOPNUMBE = ihamms.sopNumber AND standardItemCodeMaster.ITEMNMBR = standardLineItemWork.ITEMNMBR
LEFT OUTER JOIN MIASQP03.TEST.dbo.SOP30300 AS standardLineItemHistory ON standardLineItemHistory.SOPNUMBE = ihamms.sopNumber AND standardItemCodeMaster.ITEMNMBR = standardLineItemHistory.ITEMNMBR
LEFT OUTER JOIN MIASQP03.TEST.dbo.SOP10200 AS bonusLineItemWork ON bonusLineItemWork.SOPNUMBE = ihamms.sopNumber AND bonusItemCodeMaster.ITEMNMBR = bonusLineItemWork.ITEMNMBR
LEFT OUTER JOIN MIASQP03.TEST.dbo.SOP30300 AS bonusLineItemHistory ON bonusLineItemHistory.SOPNUMBE = ihamms.sopNumber AND bonusItemCodeMaster.ITEMNMBR = bonusLineItemHistory.ITEMNMBR


WHERE ihamms.sopNumber = @SOPNumber

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[FCM_APR2018]'
GO
CREATE TABLE [dbo].[FCM_APR2018]
(
[iataNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[IPrefer_Memeber_Card_Request_Report]'
GO


CREATE procedure [dbo].[IPrefer_Memeber_Card_Request_Report]
	@StartDate date,
	@EndDate date
as 


select	* 
from	miasqz01.Core.dbo.IPrefer_Member_Card_Request_Report
where	Ordered_Date between @StartDate and @EndDate
order by Ordered_Date desc

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[loadHistoricPegasusAreaInfo]'
GO
CREATE PROCEDURE [dbo].[loadHistoricPegasusAreaInfo]

	@areaCode VARCHAR(16)
AS

BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM  SQLN04.MemberContent.dbo.View1 WHERE sCityExpCode = @areaCode)
		BEGIN
			SELECT	TOP 1 'US', sStateExpName, sCityExpName
			FROM	SQLN04.MemberContent.dbo.View1
			WHERE	sCityExpCode = @areaCode
		END
	ELSE IF EXISTS (SELECT 1 FROM SQLN04.MemberContent.dbo.View1 WHERE sStateExpCode = @areaCode)
		BEGIN
			SELECT	TOP 1 'US', sStateExpName, ''
			FROM	SQLN04.MemberContent.dbo.View1
			WHERE	sStateExpCode = @areaCode
		END
	ELSE
		BEGIN
			SELECT	'US', '', ''
		END
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[loadPegasusAreaInfo]'
GO

CREATE PROCEDURE [dbo].[loadPegasusAreaInfo] 

	@countryName VARCHAR(40)
,	@stateName VARCHAR(40)
,	@cityName VARCHAR(64)

AS

BEGIN
	
	SET NOCOUNT ON;
	
	SELECT	@countryName = NULL WHERE @countryName = ''
	SELECT	@stateName = NULL WHERE @stateName = ''
	SELECT	@cityName = NULL WHERE @cityName = ''
	
	
	--  State name and city name was not passed or is null.  Retrieve Country code if country name was passed
	IF (@stateName IS NULL AND @cityName IS NULL)
		BEGIN
			SELECT	TOP 1 View1.sCountryExpCode, 'search results'
			FROM	SQLN04.MemberContent.dbo.View1
			WHERE	View1.sCountryExpName = @countryName
		END
		
	--  City name was passed, country name may have been
	ELSE IF (@stateName IS NULL AND @cityName IS NOT NULL)
		BEGIN
			--  See if country and city exist
			IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCountryExpName = @countryName
								AND
								View1.sCityExpName = @cityName )
				--  Country/city name passed exist, retrieve city code
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sCityExpName = @cityName
				END

			--  See if city name passed exists without using country name
			ELSE IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCityExpName = @cityName )
				--  City name exists without using country.  Retrieve city code without using country as a parameter
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCityExpName = @cityName
				END
				
			-- City does not appear to exist.  Retrieve country code for country if it was passed.
			ELSE
				BEGIN
					SELECT	TOP 1 View1.sCountryExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
				END
		END
	
	--  State name was passed, city name was not
	ELSE IF (@stateName IS NOT NULL AND @cityName IS NULL)
		BEGIN
			--  See if country and state name passed exists
			IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCountryExpName = @countryName
								AND
								View1.sStateExpName = @stateName )
				--  Country and state name passed exist, retrieve code
				BEGIN
					SELECT	TOP 1 View1.sStateExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sStateExpName = @stateName
				END

			--  See if state name by itself exists
			ELSE IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sStateExpName = @stateName )
						
				--  Retrieve code for just the state name (no country)
				BEGIN
					SELECT	TOP 1 View1.sStateExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sStateExpName = @stateName
				END
				
			--  State name doesn't exist or is misspelled.  Retrieve just the country code if it was passed.
			ELSE
				BEGIN
					SELECT	TOP 1 View1.sCountryExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
				END
		END
		
	--  Country, State and City name were passed
	ELSE
		BEGIN
			--  See if Country, State and City name exist
			IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCountryExpName = @countryName
								AND
								View1.sStateExpName = @stateName
								AND
								View1.sCityExpName = @cityName )
				--  Retrieve code for country, state, and city
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sStateExpName = @stateName
							AND
							View1.sCityExpName = @cityName
				END

			--  See if state and city without country passed exists
			ELSE IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sStateExpName = @stateName
								AND
								View1.sCityExpName = @cityName )
				--  It does				
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sStateExpName = @stateName
							AND
							View1.sCityExpName = @cityName
				END
			
			--  See if country and city exists without using state as a parameter	
			ELSE IF EXISTS (	SELECT	1
								FROM	SQLN04.MemberContent.dbo.View1
								WHERE	View1.sCountryExpName = @countryName
										AND
										View1.sCityExpName = @cityName )
				--  It does
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sCityExpName = @cityName
				END
				
			ELSE
				--  Doesn't exist with country/state/city passed.
				--	Doesn't exist with state/city
				--  Doesn't exist with country/city
				--  Just retrieve the code for country
				BEGIN
					SELECT	TOP 1 View1.sCountryExpCode, 'search results'
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
				END
		END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[loadSynxisAreaInfo]'
GO

CREATE PROCEDURE [dbo].[loadSynxisAreaInfo] 

	@countryName VARCHAR(40)
,	@stateName VARCHAR(40)
,	@cityName VARCHAR(64)

AS
BEGIN
	
	SET NOCOUNT ON;
	
	
	SELECT	@countryName = NULL WHERE @countryName = ''
	SELECT	@stateName = NULL WHERE @stateName = ''
	SELECT	@cityName = NULL WHERE @cityName = ''
	
	
	
	--  State name and city name was not passed or is null.  Retrieve Country code if country name was passed
	IF (@stateName IS NULL AND @cityName IS NULL)
		BEGIN
			SELECT	TOP 1 View1.sCountryExpCode
			FROM	SQLN04.MemberContent.dbo.View1
			WHERE	View1.sCountryExpName = @countryName
		END
		
	--  City name was passed, country name may have been
	ELSE IF (@stateName IS NULL AND @cityName IS NOT NULL)
		BEGIN
			--  See if country and city exist
			IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCountryExpName = @countryName
								AND
								View1.sCityExpName = @cityName )
				--  Country/city name passed exist, retrieve city code
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sCityExpName = @cityName
				END

			--  See if city name passed exists without using country name
			ELSE IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCityExpName = @cityName )
				--  City name exists without using country.  Retrieve city code without using country as a parameter
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCityExpName = @cityName
				END
				
			-- City does not appear to exist.  Retrieve country code for country if it was passed.
			ELSE
				BEGIN
					SELECT	TOP 1 View1.sCountryExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
				END
		END
	
	--  State name was passed, city name was not
	ELSE IF (@stateName IS NOT NULL AND @cityName IS NULL)
		BEGIN
			--  See if country and state name passed exists
			IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCountryExpName = @countryName
								AND
								View1.sStateExpName = @stateName )
				--  Country and state name passed exist, retrieve code
				BEGIN
					SELECT	TOP 1 View1.sStateExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sStateExpName = @stateName
				END

			--  See if state name by itself exists
			ELSE IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sStateExpName = @stateName )
						
				--  Retrieve code for just the state name (no country)
				BEGIN
					SELECT	TOP 1 View1.sStateExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sStateExpName = @stateName
				END
				
			--  State name doesn't exist or is misspelled.  Retrieve just the country code if it was passed.
			ELSE
				BEGIN
					SELECT	TOP 1 View1.sCountryExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
				END
		END
		
	--  Country, State and City name were passed
	ELSE
		BEGIN
			--  See if Country, State and City name exist
			IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sCountryExpName = @countryName
								AND
								View1.sStateExpName = @stateName
								AND
								View1.sCityExpName = @cityName )
				--  Retrieve code for country, state, and city
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sStateExpName = @stateName
							AND
							View1.sCityExpName = @cityName
				END

			--  See if state and city without country passed exists
			ELSE IF EXISTS (	SELECT	1
						FROM	SQLN04.MemberContent.dbo.View1
						WHERE	View1.sStateExpName = @stateName
								AND
								View1.sCityExpName = @cityName )
				--  It does				
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sStateExpName = @stateName
							AND
							View1.sCityExpName = @cityName
				END
			
			--  See if country and city exists without using state as a parameter	
			ELSE IF EXISTS (	SELECT	1
								FROM	SQLN04.MemberContent.dbo.View1
								WHERE	View1.sCountryExpName = @countryName
										AND
										View1.sCityExpName = @cityName )
				--  It does
				BEGIN
					SELECT	TOP 1 View1.sCityExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
							AND
							View1.sCityExpName = @cityName
				END
				
			ELSE
				--  Doesn't exist with country/state/city passed.
				--	Doesn't exist with state/city
				--  Doesn't exist with country/city
				--  Just retrieve the code for country
				BEGIN
					SELECT	TOP 1 View1.sCountryExpCode
					FROM	SQLN04.MemberContent.dbo.View1
					WHERE	View1.sCountryExpName = @countryName
				END
		END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
