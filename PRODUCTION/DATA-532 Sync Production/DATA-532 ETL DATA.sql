USE ETL
GO

/*
Run this script on:

CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/1/2019 9:44:31 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Add rows to [dbo].[ServerFilePath]')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'Adara', 'FILE PATTERN', '%monthly[_]rpt[_][0-9][0-9][0-9][0-9][_][0-9][0-9][_][0-9][0-9].csv')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'CraftedImportGlobalBooking', 'FILE PATTERN', 'Global Booking Data[_]%[0-9][0-9][0-9][0-9][_]Preferred%.csv')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'FTP', 'c1sftp01.epsilon.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'FTP DIR', '/NonProd/LoyaltyFiles/Imports/Currency')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'PASSWORD', '_qPwN7omur')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'USERNAME', 'prhg_p_phg')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'FTP', 'c1sftp01.epsilon.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'FTP DIR', '/NonProd/LoyaltyFiles/Imports/Stores')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'PASSWORD', '_qPwN7omur')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'USERNAME', 'prhg_p_phg')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'DOWNLOAD', 'G:\EpsilonExport\PointTransaction\')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'FTP', 'c1sftp01.epsilon.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'FTP DIR', '/NonProd/LoyaltyFiles/Imports/Transactions')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'PASSWORD', '_qPwN7omur')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'USERNAME', 'prhg_p_phg')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'DOWNLOAD', 'G:\EpsilonExport\Reservation\')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'FTP', 'c1sftp01.epsilon.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'FTP DIR', '/NonProd/LoyaltyFiles/Imports/Reservations')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'PASSWORD', '_qPwN7omur')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'USERNAME', 'prhg_p_phg')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackCurrency', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Currency')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackHotel', 'FILE PATTERN', '%PHG[_]STORE%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%.csv')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackHotel', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Stores')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackPointTransaction', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Transactions')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackReservation', 'DOWNLOAD', 'G:\Epsilon\EpsilonHandbackReservation\')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportMember', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Extracts')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportPoint', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Extracts')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportRedemption', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Extracts')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportReward', 'DOWNLOAD', 'G:\Epsilon\EpsilonImportReward\')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportReward', 'FTP DIR', '/home/phg_epsilon_loyalty/downloads/Prod/Extracts')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_ABC', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_AMEX', 'EXCEL SHEET', 'All GBT LOBs')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_AMEX', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_BCD', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_CCRA', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_CWT', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_FCM', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_HRG', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_RADIUS', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_THOR', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_TRAVEL_TRANSPORT', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'IATA_ULTRAMAR', 'FILE PATTERN', '%')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-ST-01\WAREHOUSE', 'Adara', 'FILE PATTERN', '%monthly[_]rpt[_][0-9][0-9][0-9][0-9][_][0-9][0-9][_][0-9][0-9].csv')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('LOCAL', 'Adara', 'FILE PATTERN', '%monthly[_]rpt[_][0-9][0-9][0-9][0-9][_][0-9][0-9][_][0-9][0-9].csv')
PRINT(N'Operation applied to 44 rows out of 44')
COMMIT TRANSACTION
GO
