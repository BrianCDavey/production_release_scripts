USE IC
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.IC    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.IC

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 12:27:06 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Workflow_Priority]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create function [dbo].[DYN_FUNC_Workflow_Priority] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Low' when @iIntEnum = 2 then 'Normal' when @iIntEnum = 3 then 'High' else ''  end  RETURN(@oVarcharValuestring)  END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Workflow_Approval_Status]'
GO

create function [dbo].[DYN_FUNC_Workflow_Approval_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Not Submitted' when @iIntEnum = 2 then 'Submitted' when @iIntEnum = 3 then 'Not Needed' when @iIntEnum = 4 then 'Pending Approval' when @iIntEnum = 5 then 'Pending Changes' when @iIntEnum = 6 then 'Approved' when @iIntEnum = 7 then 'Rejected' when @iIntEnum = 8 then 'Ended' when @iIntEnum = 9 then 'Not Enabled' when @iIntEnum = 10 then 'Disabled' else ''  end  RETURN(@oVarcharValuestring)  END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Typical_Balance]'
GO


create function [dbo].[DYN_FUNC_Typical_Balance] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Debit' when @iIntEnum = 1 then 'Credit' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Transaction_Type]'
GO


create function [dbo].[DYN_FUNC_Transaction_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Standard' when @iIntEnum = 1 then 'Reversing' when @iIntEnum = 2 then 'Clearing' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Series_GL_Trx]'
GO


create function [dbo].[DYN_FUNC_Series_GL_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'All' when @iIntEnum = 2 then 'Financial' when @iIntEnum = 3 then 'Sales' when @iIntEnum = 4 then 'Purchasing' when @iIntEnum = 5 then 'Inventory' when @iIntEnum = 6 then 'Payroll' when @iIntEnum = 7 then 'Project' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Reversing_Year]'
GO


create function [dbo].[DYN_FUNC_Reversing_Year] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then ''  else ltrim(str(@iIntEnum))  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Reversing_Closed_Year]'
GO


create function [dbo].[DYN_FUNC_Reversing_Closed_Year] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then ''  else ltrim(str(@iIntEnum))  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Rate_Calculation_Method]'
GO

create function [dbo].[DYN_FUNC_Rate_Calculation_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Multiply' when @iIntEnum = 1 then 'Divide' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Printing_Status]'
GO


create function [dbo].[DYN_FUNC_Printing_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Print' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Posting_Type]'
GO


create function [dbo].[DYN_FUNC_Posting_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Balance Sheet' when @iIntEnum = 1 then 'Profit and Loss' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_Sales_In]'
GO


create function [dbo].[DYN_FUNC_Post_Sales_In] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Detail' when @iIntEnum = 2 then 'Summary' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_Purchasing_In]'
GO


create function [dbo].[DYN_FUNC_Post_Purchasing_In] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Detail' when @iIntEnum = 2 then 'Summary' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_Payroll_In]'
GO


create function [dbo].[DYN_FUNC_Post_Payroll_In] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Detail' when @iIntEnum = 2 then 'Summary' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_Inventory_In]'
GO


create function [dbo].[DYN_FUNC_Post_Inventory_In] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Detail' when @iIntEnum = 2 then 'Summary' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Originating_Type]'
GO


create function [dbo].[DYN_FUNC_Originating_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Normal' when @iIntEnum = 2 then 'Clearing' when @iIntEnum = 3 then 'Recurring' when @iIntEnum = 4 then 'Business Form' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Originating_TRX_Type]'
GO


create function [dbo].[DYN_FUNC_Originating_TRX_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Standard' when @iIntEnum = 1 then 'Reversing' when @iIntEnum = 2 then 'Clearing' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Originating_DTA_Series]'
GO


create function [dbo].[DYN_FUNC_Originating_DTA_Series] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'All' when @iIntEnum = 1 then 'Financial' when @iIntEnum = 2 then 'Sales' when @iIntEnum = 3 then 'Purchasing' when @iIntEnum = 4 then 'Inventory' when @iIntEnum = 5 then 'Payroll' when @iIntEnum = 6 then 'Project' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Open_Year]'
GO


create function [dbo].[DYN_FUNC_Open_Year] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then ''  else ltrim(str(@iIntEnum))  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_MC_Transaction_State]'
GO

create function [dbo].[DYN_FUNC_MC_Transaction_State] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Euro' when @iIntEnum = 1 then 'Non-Denomination to Non-Denomination' when @iIntEnum = 2 then 'Non-Denomination to Euro' when @iIntEnum = 3 then 'Non-Denomination to Denomination' when @iIntEnum = 4 then 'Denomination to Non-Denomination' when @iIntEnum = 5 then 'Denomination to Denomination' when @iIntEnum = 6 then 'Denomination to Euro' when @iIntEnum = 7 then 'Euro to Denomination' when @iIntEnum = 8 then 'Euro to Non-Denomination' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Line_Status]'
GO


create function [dbo].[DYN_FUNC_Line_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Gain Or Loss' when @iIntEnum = 2 then 'Normal Rounding' when @iIntEnum = 3 then 'One Sided' when @iIntEnum = 4 then 'Other Rounding' when @iIntEnum = 5 then 'Standard' when @iIntEnum = 6 then 'Unit Account' when @iIntEnum = 7 then 'Exchange Rate Variance' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_GL_Ledger_Name]'
GO


create function [dbo].[DYN_FUNC_GL_Ledger_Name] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = (select Ledger_Name from GL40001 where Ledger_ID = @iIntEnum)  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_GL_Ledger_Description]'
GO


create function [dbo].[DYN_FUNC_GL_Ledger_Description] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = (select Ledger_Description from GL40001 where Ledger_ID = @iIntEnum)  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Fixed_Or_Variable]'
GO


create function [dbo].[DYN_FUNC_Fixed_Or_Variable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Fixed Allocation' when @iIntEnum = 2 then 'Variable Allocation' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_GL_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Status_GL_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Work' when @iIntEnum = 2 then 'Open' when @iIntEnum = 3 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Decimal_Places_QTYS]'
GO


create function [dbo].[DYN_FUNC_Decimal_Places_QTYS] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then '0' when @iIntEnum = 2 then '1' when @iIntEnum = 3 then '2' when @iIntEnum = 4 then '3' when @iIntEnum = 5 then '4' when @iIntEnum = 6 then '5' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Conversion_Method]'
GO


create function [dbo].[DYN_FUNC_Conversion_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Current' when @iIntEnum = 2 then 'Weighted Average' when @iIntEnum = 3 then 'Historical' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Closed_Year]'
GO


create function [dbo].[DYN_FUNC_Closed_Year] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then ''  else ltrim(str(@iIntEnum))  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Boolean_All]'
GO

create function [dbo].[DYN_FUNC_Boolean_All] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Yes' else 'No' end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Balance_For_Calculation]'
GO


create function [dbo].[DYN_FUNC_Balance_For_Calculation] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Net Change' when @iIntEnum = 1 then 'Period Balances' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Account_Type]'
GO


create function [dbo].[DYN_FUNC_Account_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Posting Account' when @iIntEnum = 2 then 'Unit Account' when @iIntEnum = 3 then 'Posting Allocation Account' when @iIntEnum = 4 then 'Unit Allocation Account' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Account_Category_Number]'
GO


create function [dbo].[DYN_FUNC_Account_Category_Number] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) declare @CategoryDesc varchar(100) select @CategoryDesc = (select rtrim(ACCATDSC) from GL00102 where ACCATNUM = @iIntEnum) set @oVarcharValuestring = isnull(@CategoryDesc,'') RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppJournalInquiry]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppJournalInquiry]  (  @action int,  @JRNENTRY int,  @RCTRXSEQ numeric(19,5),  @YEAR1 smallint,  @TRXDATE datetime  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenJournalInq'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&JRNENTRY=' + ltrim(str(@JRNENTRY))   + '&RCTRXSEQ=' + ltrim(str(@RCTRXSEQ))  + '&YEAR1=' + ltrim(str(@YEAR1))  + '&TRXDATE=' + rtrim(convert(char,@TRXDATE, 101))  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppJournalEntry]'
GO


 CREATE FUNCTION [dbo].[dgppJournalEntry]  (  @action int,  @JRNENTRY int,  @RCTRXSEQ numeric(19,5),  @DCSTATUS int,  @DOCTYPE int   )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenJournal'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&JRNENTRY=' + ltrim(str(@JRNENTRY))   + '&RCTRXSEQ=' + ltrim(str(@RCTRXSEQ))  + '&DCSTATUS=' + ltrim(str(@DCSTATUS))  + '&DOCTYPE=' + ltrim(str(@DOCTYPE))  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppAccountIndex]'
GO

 CREATE FUNCTION [dbo].[dgppAccountIndex]  (  @action int,  @AccountIndex int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenAcctIndx'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&ACTINDX=' + ltrim(str(@AccountIndex))  RETURN(@URIstring) END   
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[AccountTransactions]'
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE VIEW [dbo].[AccountTransactions] AS select ['Transaction Amounts Work'].[JRNENTRY] as 'Journal Entry', 'Series' = dbo.DYN_FUNC_Series_GL_Trx(['Transaction Work'].[SERIES]), ['Transaction Work'].[TRXDATE] as 'TRX Date', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['Account Master'].[ACTINDX]) as 'Account Number', rtrim(['Account Master'].[ACTDESCR]) as 'Account Description', ['Transaction Amounts Work'].[DEBITAMT] as 'Debit Amount', ['Transaction Amounts Work'].[CRDTAMNT] as 'Credit Amount',  'Account Category Number' = dbo.DYN_FUNC_Account_Category_Number(['Account Master'].[ACCATNUM]), rtrim(['Account Master'].[ACTDESCR]) as 'Account Description from Account Master', ['Transaction Amounts Work'].[ACTINDX] as 'Account Index', 'Account Type' = dbo.DYN_FUNC_Account_Type(['Account Master'].[ACCTTYPE]), 'Account Type from Account Master' = dbo.DYN_FUNC_Account_Type(['Account Master'].[ACCTTYPE]), 'Active' = dbo.DYN_FUNC_Boolean_All(['Account Master'].[ACTIVE]), 'Adjust for Inflation' = dbo.DYN_FUNC_Boolean_All(['Account Master'].[ADJINFL]), NULL  as 'Back Out JE', 'Balance For Calculation' = dbo.DYN_FUNC_Balance_For_Calculation(['Transaction Amounts Work'].[BALFRCLC]), 'Balance For Calculation from Account Master' = dbo.DYN_FUNC_Balance_For_Calculation(['Account Master'].[BALFRCLC]), rtrim(['Transaction Amounts Work'].[BACHNUMB]) as 'Batch Number', rtrim(['Transaction Work'].[BCHSOURC]) as 'Batch Source', 'Closed Year' = dbo.DYN_FUNC_Closed_Year(['Transaction Work'].[CLOSEDYR]), 'Conversion Method' = dbo.DYN_FUNC_Conversion_Method(['Account Master'].[CNVRMTHD]), NULL  as 'Correcting JE', rtrim(['Transaction Amounts Work'].[CorrespondingUnit]) as 'CorrespondingUnit', ['Account Master'].[CREATDDT] as 'Created Date', rtrim(['Currency Setup'].[CURNCYID]) as 'Currency ID', ['Transaction Amounts Work'].[CURRNIDX] as 'Currency Index', rtrim(['Transaction Work'].[DTAControlNum]) as 'DTA Control Number', ['Transaction Amounts Work'].[DTA_GL_Status] as 'DTA GL Status', ['Transaction Work'].[DTA_Index] as 'DTA Index', ['Transaction Work'].[DTATRXType] as 'DTA TRX Type', 'Decimal Places' = dbo.DYN_FUNC_Decimal_Places_QTYS(['Transaction Amounts Work'].[DECPLACS]), 'Decimal Places from Account Master' = dbo.DYN_FUNC_Decimal_Places_QTYS(['Account Master'].[DECPLACS]), ['Transaction Work'].[DENXRATE] as 'Denomination Exchange Rate', rtrim(['Transaction Amounts Work'].[DSCRIPTN]) as 'Description', ['Transaction Work'].[DOCDATE] as 'Document Date', 'Document Status' = dbo.DYN_FUNC_Document_Status_GL_Trx(1), ['Transaction Work'].[ERRSTATE] as 'Error State', ['Transaction Amounts Work'].[EXCHDATE] as 'Exchange Date', ['Transaction Amounts Work'].[XCHGRATE] as 'Exchange Rate', rtrim(['Transaction Amounts Work'].[EXGTBLID]) as 'Exchange Table ID', 'Fixed Or Variable' = dbo.DYN_FUNC_Fixed_Or_Variable(['Transaction Amounts Work'].[FXDORVAR]), 'Fixed Or Variable from Account Master' = dbo.DYN_FUNC_Fixed_Or_Variable(['Account Master'].[FXDORVAR]), ['Account Master'].[HSTRCLRT] as 'Historical Rate', 'History TRX' = dbo.DYN_FUNC_Boolean_All(['Transaction Work'].[HISTRX]), NULL  as 'History Year', 'ICDists' = dbo.DYN_FUNC_Boolean_All(['Transaction Work'].[ICDISTS]), 'IC TRX' = dbo.DYN_FUNC_Boolean_All(['Transaction Work'].[ICTRX]), ['Account Master'].[INFLAEQU] as 'Inflation Equity Account Index', ['Account Master'].[INFLAREV] as 'Inflation Revenue Account Index', rtrim(['Transaction Amounts Work'].[INTERID]) as 'Intercompany ID', ['Transaction Work'].[LSTDTEDT] as 'Last Date Edited', rtrim(['Transaction Work'].[LASTUSER]) as 'Last User', 'Line Status' = dbo.DYN_FUNC_Line_Status(['Transaction Amounts Work'].[LNESTAT]), 'MC Transaction State' = dbo.DYN_FUNC_MC_Transaction_State(['Transaction Amounts Work'].[MCTRXSTT]), rtrim(['Account Master'].[MNACSGMT]) as 'Main Account Segment', ['Account Master'].[MODIFDT] as 'Modified Date', ['Transaction Work'].[NOTEINDX] as 'Note Index', ['Account Master'].[NOTEINDX] as 'Note Index from Account Master', 'Open Year' = dbo.DYN_FUNC_Open_Year(['Transaction Work'].[OPENYEAR]), ['Transaction Work'].[Original_JE] as 'Original JE', rtrim(['Transaction Work'].[ORCOMID]) as 'Originating Company ID', rtrim(['Transaction Amounts Work'].[ORCTRNUM]) as 'Originating Control Number', ['Transaction Amounts Work'].[ORCRDAMT] as 'Originating Credit Amount', 'Originating DTA Series' = dbo.DYN_FUNC_Originating_DTA_Series(['Transaction Work'].[OrigDTASeries]), ['Transaction Amounts Work'].[ORDBTAMT] as 'Originating Debit Amount', rtrim(['Transaction Amounts Work'].[ORDOCNUM]) as 'Originating Document Number', ['Transaction Work'].[ORIGINJE] as 'Originating Journal Entry', rtrim(['Transaction Amounts Work'].[ORMSTRID]) as 'Originating Master ID', rtrim(['Transaction Amounts Work'].[ORMSTRNM]) as 'Originating Master Name', ['Transaction Work'].[ORPSTDDT] as 'Originating Posted Date', ['Transaction Amounts Work'].[OrigSeqNum] as 'Originating Sequence Number', NULL  as 'Originating Source', rtrim(['Transaction Work'].[ORTRXSRC]) as 'Originating TRX Source', 'Originating TRX Type' = dbo.DYN_FUNC_Originating_TRX_Type(['Transaction Amounts Work'].[ORTRXTYP]), NULL  as 'Originating Type', ['Transaction Work'].[PERIODID] as 'Period ID', NULL  as 'Period Posting Number',  ''  as 'Approval User ID', NULL  as 'Approval Date', NULL  as 'Polled Transaction', 'Post Inventory In' = dbo.DYN_FUNC_Post_Inventory_In(['Account Master'].[PostIvIn]), 'Post Payroll In' = dbo.DYN_FUNC_Post_Payroll_In(['Account Master'].[PostPRIn]), 'Post Purchasing In' = dbo.DYN_FUNC_Post_Purchasing_In(['Account Master'].[PostPurchIn]), 'Post Sales In' = dbo.DYN_FUNC_Post_Sales_In(['Account Master'].[PostSlsIn]), NULL  as 'Posting Number', 'Posting Type' = dbo.DYN_FUNC_Posting_Type(['Account Master'].[PSTNGTYP]), 'Posting Type from Account Master' = dbo.DYN_FUNC_Posting_Type(['Account Master'].[PSTNGTYP]), 'Printing Status' = dbo.DYN_FUNC_Printing_Status(['Transaction Work'].[PRNTSTUS]), NULL  as 'Quick Offset', 'Rate Calculation Method' = dbo.DYN_FUNC_Rate_Calculation_Method(['Transaction Work'].[RTCLCMTD]), rtrim(['Transaction Work'].[RATETPID]) as 'Rate Type ID', 'Recurring TRX' = dbo.DYN_FUNC_Boolean_All(['Transaction Work'].[RCRNGTRX]), ['Transaction Work'].[RCTRXSEQ] as 'Recurring TRX Sequence', rtrim(['Transaction Work'].[REFRENCE]) as 'Reference', 'Reversing Closed Year' = dbo.DYN_FUNC_Reversing_Closed_Year(['Transaction Work'].[REVCLYR]), ['Transaction Work'].[RVRSNGDT] as 'Reversing Date', 'Reversing History TRX' = dbo.DYN_FUNC_Boolean_All(['Transaction Work'].[REVHIST]), ['Transaction Work'].[REVPRDID] as 'Reversing Period ID', rtrim(['Transaction Work'].[RVTRXSRC]) as 'Reversing TRX Source', 'Reversing Year' = dbo.DYN_FUNC_Reversing_Year(['Transaction Work'].[REVYEAR]), ['Account Master'].[ACTNUMBR_1] as 'Segment1', ['Account Master'].[ACTNUMBR_2] as 'Segment2', ['Account Master'].[ACTNUMBR_3] as 'Segment3', ['Account Master'].[ACTNUMBR_4] as 'Segment4', ['Transaction Amounts Work'].[SQNCLINE] as 'Sequence Number', rtrim(['Transaction Work'].[SOURCDOC]) as 'Source Document', rtrim(['Transaction Work'].[TRXSORCE]) as 'TRX Source', ['Transaction Work'].[Tax_Date] as 'Tax Date', ['Transaction Amounts Work'].[TIME1] as 'Time', 'Transaction Type' = dbo.DYN_FUNC_Transaction_Type(['Transaction Work'].[TRXTYPE]), 'Typical Balance' = dbo.DYN_FUNC_Typical_Balance(['Account Master'].[TPCLBLNC]), rtrim(['Account Master'].[USERDEF1]) as 'User Defined 1',  rtrim(['Account Master'].[USERDEF2]) as 'User Defined 2', rtrim(['Transaction Work'].[USWHPSTD]) as 'User Who Posted', 'Voided' = dbo.DYN_FUNC_Boolean_All(['Transaction Work'].[VOIDED]), '4' as 'Segments', 'Workflow Approval Status' = dbo.DYN_FUNC_Workflow_Approval_Status(['Posting Definitions Master'].[Workflow_Approval_Status]), 'Workflow Priority' = dbo.DYN_FUNC_Workflow_Priority(['Posting Definitions Master'].[Workflow_Priority]), 'Ledger Name' = dbo.DYN_FUNC_GL_Ledger_Name(['Transaction Work'].[Ledger_ID]), 'Ledger Description' = dbo.DYN_FUNC_GL_Ledger_Description(['Transaction Work'].[Ledger_ID]), 'Account Index For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['Transaction Amounts Work'].[ACTINDX] ), 'Journal Entry For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppJournalEntry(1,['Transaction Amounts Work'].[JRNENTRY],['Transaction Work'].[RCTRXSEQ],1,1 )  from [GL10001] as ['Transaction Amounts Work'] with (NOLOCK) left outer join [GL00100] as ['Account Master'] with (NOLOCK) on ['Transaction Amounts Work'].[ACTINDX] = ['Account Master'].[ACTINDX] left outer join [GL10000] as ['Transaction Work'] with (NOLOCK) on ['Transaction Amounts Work'].[JRNENTRY] = ['Transaction Work'].[JRNENTRY] left outer join [DYNAMICS].[dbo].[MC40200] as ['Currency Setup'] with (NOLOCK) on ['Transaction Amounts Work'].[CURRNIDX] = ['Currency Setup'].[CURRNIDX] left outer join [SY00500] as ['Posting Definitions Master'] with (NOLOCK) on ['Transaction Work'].[BCHSOURC] = ['Posting Definitions Master'].[BCHSOURC]  and ['Transaction Work'].[BACHNUMB] = ['Posting Definitions Master'].[BACHNUMB]  union all select ['Year-to-Date Transaction Open'].[JRNENTRY] as 'Journal Entry', 'Series' = dbo.DYN_FUNC_Series_GL_Trx(['Year-to-Date Transaction Open'].[SERIES]), ['Year-to-Date Transaction Open'].[TRXDATE] as 'TRX Date', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['Account Master'].[ACTINDX]) as 'Account Number', rtrim(['Account Master'].[ACTDESCR]) as 'Account Description', ['Year-to-Date Transaction Open'].[DEBITAMT] as 'Debit Amount', ['Year-to-Date Transaction Open'].[CRDTAMNT] as 'Credit Amount',  'Account Category Number' = dbo.DYN_FUNC_Account_Category_Number(['Account Master'].[ACCATNUM]), rtrim(['Account Master'].[ACTDESCR]) as 'Account Description from Account Master', ['Year-to-Date Transaction Open'].[ACTINDX] as 'Account Index', 'Account Type' = dbo.DYN_FUNC_Account_Type(['Account Master'].[ACCTTYPE]), 'Account Type from Account Master' = dbo.DYN_FUNC_Account_Type(['Account Master'].[ACCTTYPE]), 'Active' = dbo.DYN_FUNC_Boolean_All(['Account Master'].[ACTIVE]), 'Adjust for Inflation' = dbo.DYN_FUNC_Boolean_All(['Account Master'].[ADJINFL]), ['Year-to-Date Transaction Open'].[Back_Out_JE] as 'Back Out JE', NULL  as 'Balance For Calculation', 'Balance For Calculation from Account Master' = dbo.DYN_FUNC_Balance_For_Calculation(['Account Master'].[BALFRCLC]), NULL  as 'Batch Number', NULL  as 'Batch Source', NULL  as 'Closed Year', 'Conversion Method' = dbo.DYN_FUNC_Conversion_Method(['Account Master'].[CNVRMTHD]), ['Year-to-Date Transaction Open'].[Correcting_JE] as 'Correcting JE', rtrim(['Year-to-Date Transaction Open'].[CorrespondingUnit]) as 'CorrespondingUnit', ['Account Master'].[CREATDDT] as 'Created Date', rtrim(['Currency Setup'].[CURNCYID]) as 'Currency ID', ['Year-to-Date Transaction Open'].[CURRNIDX] as 'Currency Index', NULL  as 'DTA Control Number', ['Year-to-Date Transaction Open'].[DTA_GL_Status] as 'DTA GL Status', ['Year-to-Date Transaction Open'].[DTA_Index] as 'DTA Index', NULL  as 'DTA TRX Type', NULL  as 'Decimal Places', 'Decimal Places from Account Master' = dbo.DYN_FUNC_Decimal_Places_QTYS(['Account Master'].[DECPLACS]), ['Year-to-Date Transaction Open'].[DENXRATE] as 'Denomination Exchange Rate', rtrim(['Year-to-Date Transaction Open'].[DSCRIPTN]) as 'Description', ['Year-to-Date Transaction Open'].[DOCDATE] as 'Document Date', 'Document Status' = dbo.DYN_FUNC_Document_Status_GL_Trx(2), NULL  as 'Error State', ['Year-to-Date Transaction Open'].[EXCHDATE] as 'Exchange Date', ['Year-to-Date Transaction Open'].[XCHGRATE] as 'Exchange Rate', rtrim(['Year-to-Date Transaction Open'].[EXGTBLID]) as 'Exchange Table ID', NULL  as 'Fixed Or Variable', 'Fixed Or Variable from Account Master' = dbo.DYN_FUNC_Fixed_Or_Variable(['Account Master'].[FXDORVAR]), ['Account Master'].[HSTRCLRT] as 'Historical Rate', 'History TRX' = dbo.DYN_FUNC_Boolean_All(0), NULL  as 'History Year', NULL  as 'ICDists', 'IC TRX' = dbo.DYN_FUNC_Boolean_All(['Year-to-Date Transaction Open'].[ICTRX]), ['Account Master'].[INFLAEQU] as 'Inflation Equity Account Index', ['Account Master'].[INFLAREV] as 'Inflation Revenue Account Index', NULL  as 'Intercompany ID', ['Year-to-Date Transaction Open'].[LSTDTEDT] as 'Last Date Edited', rtrim(['Year-to-Date Transaction Open'].[LASTUSER]) as 'Last User', NULL  as 'Line Status', 'MC Transaction State' = dbo.DYN_FUNC_MC_Transaction_State(['Year-to-Date Transaction Open'].[MCTRXSTT]), rtrim(['Account Master'].[MNACSGMT]) as 'Main Account Segment', ['Account Master'].[MODIFDT] as 'Modified Date', ['Year-to-Date Transaction Open'].[NOTEINDX] as 'Note Index', ['Account Master'].[NOTEINDX] as 'Note Index from Account Master', 'Open Year' = dbo.DYN_FUNC_Open_Year(['Year-to-Date Transaction Open'].[OPENYEAR]), ['Year-to-Date Transaction Open'].[Original_JE] as 'Original JE', rtrim(['Year-to-Date Transaction Open'].[ORCOMID]) as 'Originating Company ID', rtrim(['Year-to-Date Transaction Open'].[ORCTRNUM]) as 'Originating Control Number', ['Year-to-Date Transaction Open'].[ORCRDAMT] as 'Originating Credit Amount', 'Originating DTA Series' = dbo.DYN_FUNC_Originating_DTA_Series(['Year-to-Date Transaction Open'].[OrigDTASeries]), ['Year-to-Date Transaction Open'].[ORDBTAMT] as 'Originating Debit Amount', rtrim(['Year-to-Date Transaction Open'].[ORDOCNUM]) as 'Originating Document Number', ['Year-to-Date Transaction Open'].[ORIGINJE] as 'Originating Journal Entry', rtrim(['Year-to-Date Transaction Open'].[ORMSTRID]) as 'Originating Master ID', rtrim(['Year-to-Date Transaction Open'].[ORMSTRNM]) as 'Originating Master Name', ['Year-to-Date Transaction Open'].[ORPSTDDT] as 'Originating Posted Date', ['Year-to-Date Transaction Open'].[OrigSeqNum] as 'Originating Sequence Number', rtrim(['Year-to-Date Transaction Open'].[ORGNTSRC]) as 'Originating Source', rtrim(['Year-to-Date Transaction Open'].[ORTRXSRC]) as 'Originating TRX Source', 'Originating TRX Type' = dbo.DYN_FUNC_Originating_TRX_Type(['Year-to-Date Transaction Open'].[ORTRXTYP]), 'Originating Type' = dbo.DYN_FUNC_Originating_Type(['Year-to-Date Transaction Open'].[ORGNATYP]), ['Year-to-Date Transaction Open'].[PERIODID] as 'Period ID', ['Year-to-Date Transaction Open'].[PPSGNMBR] as 'Period Posting Number',  ['Year-to-Date Transaction Open'].[APRVLUSERID] as 'Approval User ID', ['Year-to-Date Transaction Open'].[APPRVLDT] as 'Approval Date', 'Polled Transaction' = dbo.DYN_FUNC_Boolean_All(['Year-to-Date Transaction Open'].[POLLDTRX]), 'Post Inventory In' = dbo.DYN_FUNC_Post_Inventory_In(['Account Master'].[PostIvIn]), 'Post Payroll In' = dbo.DYN_FUNC_Post_Payroll_In(['Account Master'].[PostPRIn]), 'Post Purchasing In' = dbo.DYN_FUNC_Post_Purchasing_In(['Account Master'].[PostPurchIn]), 'Post Sales In' = dbo.DYN_FUNC_Post_Sales_In(['Account Master'].[PostSlsIn]), ['Year-to-Date Transaction Open'].[PSTGNMBR] as 'Posting Number', 'Posting Type' = dbo.DYN_FUNC_Posting_Type(['Account Master'].[PSTNGTYP]), 'Posting Type from Account Master' = dbo.DYN_FUNC_Posting_Type(['Account Master'].[PSTNGTYP]), NULL  as 'Printing Status', ['Year-to-Date Transaction Open'].[QKOFSET] as 'Quick Offset', 'Rate Calculation Method' = dbo.DYN_FUNC_Rate_Calculation_Method(['Year-to-Date Transaction Open'].[RTCLCMTD]), rtrim(['Year-to-Date Transaction Open'].[RATETPID]) as 'Rate Type ID', NULL  as 'Recurring TRX', ['Year-to-Date Transaction Open'].[RCTRXSEQ] as 'Recurring TRX Sequence', rtrim(['Year-to-Date Transaction Open'].[REFRENCE]) as 'Reference', NULL  as 'Reversing Closed Year', NULL  as 'Reversing Date', NULL  as 'Reversing History TRX', NULL  as 'Reversing Period ID', NULL  as 'Reversing TRX Source', NULL  as 'Reversing Year', ['Account Master'].[ACTNUMBR_1] as 'Segment1', ['Account Master'].[ACTNUMBR_2] as 'Segment2', ['Account Master'].[ACTNUMBR_3] as 'Segment3', ['Account Master'].[ACTNUMBR_4] as 'Segment4', ['Year-to-Date Transaction Open'].[SEQNUMBR] as 'Sequence Number', rtrim(['Year-to-Date Transaction Open'].[SOURCDOC]) as 'Source Document', rtrim(['Year-to-Date Transaction Open'].[TRXSORCE]) as 'TRX Source', NULL  as 'Tax Date', ['Year-to-Date Transaction Open'].[TIME1] as 'Time', NULL  as 'Transaction Type', 'Typical Balance' = dbo.DYN_FUNC_Typical_Balance(['Account Master'].[TPCLBLNC]), rtrim(['Account Master'].[USERDEF1]) as 'User Defined 1',  rtrim(['Account Master'].[USERDEF2]) as 'User Defined 2', rtrim(['Year-to-Date Transaction Open'].[USWHPSTD]) as 'User Who Posted', 'Voided' = dbo.DYN_FUNC_Boolean_All(['Year-to-Date Transaction Open'].[VOIDED]), '4' as 'Segments', NULL  as 'Workflow Approval Status', NULL  as 'Workflow Priority', 'Ledger Name' = dbo.DYN_FUNC_GL_Ledger_Name(['Year-to-Date Transaction Open'].[Ledger_ID]), 'Ledger Description' = dbo.DYN_FUNC_GL_Ledger_Description(['Year-to-Date Transaction Open'].[Ledger_ID]), 'Account Index For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['Year-to-Date Transaction Open'].[ACTINDX] ), 'Journal Entry For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppJournalInquiry(1,['Year-to-Date Transaction Open'].[JRNENTRY],['Year-to-Date Transaction Open'].[RCTRXSEQ],['Year-to-Date Transaction Open'].[OPENYEAR],['Year-to-Date Transaction Open'].[TRXDATE] )  from [GL20000] as ['Year-to-Date Transaction Open'] with (NOLOCK) left outer join [GL00100] as ['Account Master'] with (NOLOCK) on ['Year-to-Date Transaction Open'].[ACTINDX] = ['Account Master'].[ACTINDX] left outer join [DYNAMICS].[dbo].[MC40200] as ['Currency Setup'] with (NOLOCK) on ['Year-to-Date Transaction Open'].[CURRNIDX] = ['Currency Setup'].[CURRNIDX]  union all select ['Account Transaction History'].[JRNENTRY] as 'Journal Entry', 'Series' = dbo.DYN_FUNC_Series_GL_Trx(['Account Transaction History'].[SERIES]), ['Account Transaction History'].[TRXDATE] as 'TRX Date', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['Account Master'].[ACTINDX]) as 'Account Number', rtrim(['Account Master'].[ACTDESCR]) as 'Account Description', ['Account Transaction History'].[DEBITAMT] as 'Debit Amount', ['Account Transaction History'].[CRDTAMNT] as 'Credit Amount',  'Account Category Number' = dbo.DYN_FUNC_Account_Category_Number(['Account Master'].[ACCATNUM]), rtrim(['Account Master'].[ACTDESCR]) as 'Account Description from Account Master', ['Account Transaction History'].[ACTINDX] as 'Account Index', 'Account Type' = dbo.DYN_FUNC_Account_Type(['Account Master'].[ACCTTYPE]), 'Account Type from Account Master' = dbo.DYN_FUNC_Account_Type(['Account Master'].[ACCTTYPE]), 'Active' = dbo.DYN_FUNC_Boolean_All(['Account Master'].[ACTIVE]), 'Adjust for Inflation' = dbo.DYN_FUNC_Boolean_All(['Account Master'].[ADJINFL]), NULL  as 'Back Out JE', NULL  as 'Balance For Calculation', 'Balance For Calculation from Account Master' = dbo.DYN_FUNC_Balance_For_Calculation(['Account Master'].[BALFRCLC]), NULL  as 'Batch Number', NULL  as 'Batch Source', NULL  as 'Closed Year', 'Conversion Method' = dbo.DYN_FUNC_Conversion_Method(['Account Master'].[CNVRMTHD]), ['Account Transaction History'].[Correcting_JE] as 'Correcting JE', rtrim(['Account Transaction History'].[CorrespondingUnit]) as 'CorrespondingUnit', ['Account Master'].[CREATDDT] as 'Created Date', rtrim(['Currency Setup'].[CURNCYID]) as 'Currency ID', ['Account Transaction History'].[CURRNIDX] as 'Currency Index', NULL  as 'DTA Control Number', ['Account Transaction History'].[DTA_GL_Status] as 'DTA GL Status', ['Account Transaction History'].[DTA_Index] as 'DTA Index', NULL  as 'DTA TRX Type', NULL  as 'Decimal Places', 'Decimal Places from Account Master' = dbo.DYN_FUNC_Decimal_Places_QTYS(['Account Master'].[DECPLACS]), ['Account Transaction History'].[DENXRATE] as 'Denomination Exchange Rate', rtrim(['Account Transaction History'].[DSCRIPTN]) as 'Description', ['Account Transaction History'].[DOCDATE] as 'Document Date', 'Document Status' = dbo.DYN_FUNC_Document_Status_GL_Trx(3), NULL  as 'Error State', ['Account Transaction History'].[EXCHDATE] as 'Exchange Date', ['Account Transaction History'].[XCHGRATE] as 'Exchange Rate', rtrim(['Account Transaction History'].[EXGTBLID]) as 'Exchange Table ID', NULL  as 'Fixed Or Variable', 'Fixed Or Variable from Account Master' = dbo.DYN_FUNC_Fixed_Or_Variable(['Account Master'].[FXDORVAR]), ['Account Master'].[HSTRCLRT] as 'Historical Rate', 'History TRX' = dbo.DYN_FUNC_Boolean_All(1), 'History Year' = ['Account Transaction History'].[HSTYEAR], NULL  as 'ICDists', 'IC TRX' = dbo.DYN_FUNC_Boolean_All(['Account Transaction History'].[ICTRX]), ['Account Master'].[INFLAEQU] as 'Inflation Equity Account Index', ['Account Master'].[INFLAREV] as 'Inflation Revenue Account Index', NULL  as 'Intercompany ID', ['Account Transaction History'].[LSTDTEDT] as 'Last Date Edited', rtrim(['Account Transaction History'].[LASTUSER]) as 'Last User', NULL  as 'Line Status', 'MC Transaction State' = dbo.DYN_FUNC_MC_Transaction_State(['Account Transaction History'].[MCTRXSTT]), rtrim(['Account Master'].[MNACSGMT]) as 'Main Account Segment', ['Account Master'].[MODIFDT] as 'Modified Date', ['Account Transaction History'].[NOTEINDX] as 'Note Index', ['Account Master'].[NOTEINDX] as 'Note Index from Account Master', NULL  as 'Open Year', ['Account Transaction History'].[Original_JE] as 'Original JE', rtrim(['Account Transaction History'].[ORCOMID]) as 'Originating Company ID', rtrim(['Account Transaction History'].[ORCTRNUM]) as 'Originating Control Number', ['Account Transaction History'].[ORCRDAMT] as 'Originating Credit Amount', 'Originating DTA Series' = dbo.DYN_FUNC_Originating_DTA_Series(['Account Transaction History'].[OrigDTASeries]), ['Account Transaction History'].[ORDBTAMT] as 'Originating Debit Amount', rtrim(['Account Transaction History'].[ORDOCNUM]) as 'Originating Document Number', ['Account Transaction History'].[ORIGINJE] as 'Originating Journal Entry', rtrim(['Account Transaction History'].[ORMSTRID]) as 'Originating Master ID', rtrim(['Account Transaction History'].[ORMSTRNM]) as 'Originating Master Name', ['Account Transaction History'].[ORPSTDDT] as 'Originating Posted Date', ['Account Transaction History'].[OrigSeqNum] as 'Originating Sequence Number', NULL  as 'Originating Source', rtrim(['Account Transaction History'].[ORTRXSRC]) as 'Originating TRX Source', 'Originating TRX Type' = dbo.DYN_FUNC_Originating_TRX_Type(['Account Transaction History'].[ORTRXTYP]), 'Originating Type' = dbo.DYN_FUNC_Originating_Type(['Account Transaction History'].[ORGNATYP]), ['Account Transaction History'].[PERIODID] as 'Period ID', ['Account Transaction History'].[PPSGNMBR] as 'Period Posting Number',  ['Account Transaction History'].[APRVLUSERID] as 'Approval User ID', ['Account Transaction History'].[APPRVLDT] as 'Approval Date', 'Polled Transaction' = dbo.DYN_FUNC_Boolean_All(['Account Transaction History'].[POLLDTRX]), 'Post Inventory In' = dbo.DYN_FUNC_Post_Inventory_In(['Account Master'].[PostIvIn]), 'Post Payroll In' = dbo.DYN_FUNC_Post_Payroll_In(['Account Master'].[PostPRIn]), 'Post Purchasing In' = dbo.DYN_FUNC_Post_Purchasing_In(['Account Master'].[PostPurchIn]), 'Post Sales In' = dbo.DYN_FUNC_Post_Sales_In(['Account Master'].[PostSlsIn]), ['Account Transaction History'].[PSTGNMBR] as 'Posting Number', 'Posting Type' = dbo.DYN_FUNC_Posting_Type(['Account Master'].[PSTNGTYP]), 'Posting Type from Account Master' = dbo.DYN_FUNC_Posting_Type(['Account Master'].[PSTNGTYP]), NULL  as 'Printing Status', ['Account Transaction History'].[QKOFSET] as 'Quick Offset', 'Rate Calculation Method' = dbo.DYN_FUNC_Rate_Calculation_Method(['Account Transaction History'].[RTCLCMTD]), rtrim(['Account Transaction History'].[RATETPID]) as 'Rate Type ID', NULL  as 'Recurring TRX', ['Account Transaction History'].[RCTRXSEQ] as 'Recurring TRX Sequence', rtrim(['Account Transaction History'].[REFRENCE]) as 'Reference', NULL  as 'Reversing Closed Year', NULL  as 'Reversing Date', NULL  as 'Reversing History TRX', NULL  as 'Reversing Period ID', NULL  as 'Reversing TRX Source', NULL  as 'Reversing Year', ['Account Master'].[ACTNUMBR_1] as 'Segment1', ['Account Master'].[ACTNUMBR_2] as 'Segment2', ['Account Master'].[ACTNUMBR_3] as 'Segment3', ['Account Master'].[ACTNUMBR_4] as 'Segment4', ['Account Transaction History'].[SEQNUMBR] as 'Sequence Number', rtrim(['Account Transaction History'].[SOURCDOC]) as 'Source Document', rtrim(['Account Transaction History'].[TRXSORCE]) as 'TRX Source', NULL  as 'Tax Date', ['Account Transaction History'].[TIME1] as 'Time', NULL  as 'Transaction Type',  'Typical Balance' = dbo.DYN_FUNC_Typical_Balance(['Account Master'].[TPCLBLNC]), rtrim(['Account Master'].[USERDEF1]) as 'User Defined 1', rtrim(['Account Master'].[USERDEF2]) as 'User Defined 2', rtrim(['Account Transaction History'].[USWHPSTD]) as 'User Who Posted', 'Voided' = dbo.DYN_FUNC_Boolean_All(['Account Transaction History'].[VOIDED]), '4' as 'Segments', NULL  as 'Workflow Approval Status', NULL  as 'Workflow Priority', 'Ledger Name' = dbo.DYN_FUNC_GL_Ledger_Name(['Account Transaction History'].[Ledger_ID]), 'Ledger Description' = dbo.DYN_FUNC_GL_Ledger_Description(['Account Transaction History'].[Ledger_ID]), 'Account Index For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['Account Transaction History'].[ACTINDX] ), 'Journal Entry For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppJournalInquiry(1,['Account Transaction History'].[JRNENTRY],['Account Transaction History'].[RCTRXSEQ],['Account Transaction History'].[HSTYEAR],['Account Transaction History'].[TRXDATE]  )  from [GL30000] as ['Account Transaction History'] with (NOLOCK) left outer join [GL00100] as ['Account Master'] with (NOLOCK) on ['Account Transaction History'].[ACTINDX] = ['Account Master'].[ACTINDX] left outer join [DYNAMICS].[dbo].[MC40200] as ['Currency Setup'] with (NOLOCK) on ['Account Transaction History'].[CURRNIDX] = ['Currency Setup'].[CURRNIDX] 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Void_Status]'
GO

create function [dbo].[DYN_FUNC_Void_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Normal' when @iIntEnum = 1 then 'Voided' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_TRX_Frequency]'
GO

create function [dbo].[DYN_FUNC_TRX_Frequency] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Weekly' when @iIntEnum = 2 then 'Biweekly' when @iIntEnum = 3 then 'Semimonthly' when @iIntEnum = 4 then 'Monthly' when @iIntEnum = 5 then 'Bimonthly' when @iIntEnum = 6 then 'Quarterly' when @iIntEnum = 7 then 'Miscellaneous' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Tax_Schedule_Source]'
GO

create function [dbo].[DYN_FUNC_Tax_Schedule_Source] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Error' when @iIntEnum = 1 then 'Using Site Schedule ID' when @iIntEnum = 2 then 'Using Ship To Schedule ID' when @iIntEnum = 3 then 'Using Single Schedule' when @iIntEnum = 4 then 'Schedule ID Empty' when @iIntEnum = 5 then 'Schedule ID Not Found' when @iIntEnum = 6 then 'Shipping Method Not Found' when @iIntEnum = 7 then 'Setup File Missing/Damaged' when @iIntEnum = 8 then 'Site Location Not Found' when @iIntEnum = 9 then 'Address Record Not Found' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Statement_Cycle]'
GO

create function [dbo].[DYN_FUNC_Statement_Cycle] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'No Statement' when @iIntEnum = 2 then 'Weekly' when @iIntEnum = 3 then 'Biweekly' when @iIntEnum = 4 then 'Semimonthly' when @iIntEnum = 5 then 'Monthly' when @iIntEnum = 6 then 'Bimonthly' when @iIntEnum = 7 then 'Quarterly' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_SOP_Type]'
GO
SET QUOTED_IDENTIFIER ON
GO


create function [dbo].[DYN_FUNC_SOP_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Quote' when @iIntEnum = 2 then 'Order' when @iIntEnum = 3 then 'Invoice' when @iIntEnum = 4 then 'Return' when @iIntEnum = 5 then 'Back Order' when @iIntEnum = 6 then 'Fulfillment Order' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_SOP_Status]'
GO
SET QUOTED_IDENTIFIER OFF
GO

create function [dbo].[DYN_FUNC_SOP_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) if @iIntEnum = 1  select @oVarcharValuestring =    rtrim(SQL_MSG)    from     DYNAMICS.dbo.MESSAGES    WITH (NOLOCK)    where     MSGNUM = 17212    and         Language_ID =          0 else if @iIntEnum = 8  select @oVarcharValuestring =    rtrim(SQL_MSG)    from     DYNAMICS.dbo.MESSAGES    WITH (NOLOCK)    where     MSGNUM = 6916    and          Language_ID =          0 else if @iIntEnum = 9  select @oVarcharValuestring =    rtrim(SQL_MSG)    from     DYNAMICS.dbo.MESSAGES    WITH (NOLOCK)    where     MSGNUM = 5966    and          Language_ID =          0 else  set @oVarcharValuestring =   (select ISNULL(SOPSTSDESCR,'') from SOP40101 where SOPSTATUS = @iIntEnum)   RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Priority]'
GO

create function [dbo].[DYN_FUNC_Priority] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'None' when @iIntEnum = 2 then '1' when @iIntEnum = 3 then '2' when @iIntEnum = 4 then '3' when @iIntEnum = 5 then '4' when @iIntEnum = 6 then '5' when @iIntEnum = 7 then '6' when @iIntEnum = 8 then '7' when @iIntEnum = 9 then '8' when @iIntEnum = 10 then '9' when @iIntEnum = 11 then '10' when @iIntEnum = 12 then '11' when @iIntEnum = 13 then '12' when @iIntEnum = 14 then '13' when @iIntEnum = 15 then '14' when @iIntEnum = 16 then '15' when @iIntEnum = 17 then '16' when @iIntEnum = 18 then '17' when @iIntEnum = 19 then '18' when @iIntEnum = 20 then '19' when @iIntEnum = 21 then '20' when @iIntEnum = 22 then '21' when @iIntEnum = 23 then '22' when @iIntEnum = 24 then '23' when @iIntEnum = 25 then '24' when @iIntEnum = 26 then '25' when @iIntEnum = 27 then '26' when @iIntEnum = 28 then '27' when @iIntEnum = 29 then '28' when @iIntEnum = 30 then '29' when @iIntEnum = 31 then '30' when @iIntEnum = 32 then '31' when @iIntEnum = 33 then '32' when @iIntEnum = 34 then '33' when @iIntEnum = 35 then '34' when @iIntEnum = 36 then '35' when @iIntEnum = 37 then '36' when @iIntEnum = 38 then '37' when @iIntEnum = 39 then '38' when @iIntEnum = 40 then '39' when @iIntEnum = 41 then '40' when @iIntEnum = 42 then '41' when @iIntEnum = 43 then '42' when @iIntEnum = 44 then '43' when @iIntEnum = 45 then '44' when @iIntEnum = 46 then '45' when @iIntEnum = 47 then '46' when @iIntEnum = 48 then '47' when @iIntEnum = 49 then '48' when @iIntEnum = 50 then '49' when @iIntEnum = 51 then '50' when @iIntEnum = 52 then '51' when @iIntEnum = 53 then '52' when @iIntEnum = 54 then '53' when @iIntEnum = 55 then '54' when @iIntEnum = 56 then '55' when @iIntEnum = 57 then '56' when @iIntEnum = 58 then '57' when @iIntEnum = 59 then '58' when @iIntEnum = 60 then '59' when @iIntEnum = 61 then '60' when @iIntEnum = 62 then '61' when @iIntEnum = 63 then '62' when @iIntEnum = 64 then '63' when @iIntEnum = 65 then '64' when @iIntEnum = 66 then '65' when @iIntEnum = 67 then '66' when @iIntEnum = 68 then '67' when @iIntEnum = 69 then '68' when @iIntEnum = 70 then '69' when @iIntEnum = 71 then '70' when @iIntEnum = 72 then '71' when @iIntEnum = 73 then '72' when @iIntEnum = 74 then '73' when @iIntEnum = 75 then '74' when @iIntEnum = 76 then '75' when @iIntEnum = 77 then '76' when @iIntEnum = 78 then '77' when @iIntEnum = 79 then '78' when @iIntEnum = 80 then '79' when @iIntEnum = 81 then '80' when @iIntEnum = 82 then '81' when @iIntEnum = 83 then '82' when @iIntEnum = 84 then '83' when @iIntEnum = 85 then '84' when @iIntEnum = 86 then '85' when @iIntEnum = 87 then '86' when @iIntEnum = 88 then '87' when @iIntEnum = 89 then '88' when @iIntEnum = 90 then '89' when @iIntEnum = 91 then '90' when @iIntEnum = 92 then '91' when @iIntEnum = 93 then '92' when @iIntEnum = 94 then '93' when @iIntEnum = 95 then '94' when @iIntEnum = 96 then '95' when @iIntEnum = 97 then '96' when @iIntEnum = 98 then '97' when @iIntEnum = 99 then '98' when @iIntEnum = 100 then '99' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Posting_Status_SOP_Line_Items]'
GO

create function [dbo].[DYN_FUNC_Posting_Status_SOP_Line_Items] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Unposted' when @iIntEnum = 2 then 'Posted' when @iIntEnum = 3 then 'Posted With Error' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_Results_To_Customer]'
GO

create function [dbo].[DYN_FUNC_Post_Results_To_Customer] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Receivables/Discount Acct' when @iIntEnum = 1 then 'Sales Offset Acct' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Original_Type]'
GO

create function [dbo].[DYN_FUNC_Original_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Quote' when @iIntEnum = 2 then 'Order' when @iIntEnum = 3 then 'Invoice' when @iIntEnum = 4 then 'Return' when @iIntEnum = 5 then 'Back Order' when @iIntEnum = 6 then 'Fulfillment Order' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Order_Fulfillment_Shortage_Default]'
GO

create function [dbo].[DYN_FUNC_Order_Fulfillment_Shortage_Default] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'None' when @iIntEnum = 2 then 'Back Order Remaining' when @iIntEnum = 3 then 'Cancel Remaining' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Misc_Taxable]'
GO

create function [dbo].[DYN_FUNC_Misc_Taxable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on customers' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Minimum_Payment_Type]'
GO

create function [dbo].[DYN_FUNC_Minimum_Payment_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Minimum' when @iIntEnum = 1 then 'Percent' when @iIntEnum = 2 then 'Amount' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Maximum_Writeoff_Type]'
GO

create function [dbo].[DYN_FUNC_Maximum_Writeoff_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Not Allowed' when @iIntEnum = 1 then 'Unlimited' when @iIntEnum = 2 then 'Maximum' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Freight_Taxable]'
GO

create function [dbo].[DYN_FUNC_Freight_Taxable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on customers' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Finance_Charge_Amt_Type]'
GO

create function [dbo].[DYN_FUNC_Finance_Charge_Amt_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'None' when @iIntEnum = 1 then 'Percent' when @iIntEnum = 2 then 'Amount' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_Sls_Trx]'
GO

create function [dbo].[DYN_FUNC_Document_Status_Sls_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Default_Cash_Account_Type]'
GO

create function [dbo].[DYN_FUNC_Default_Cash_Account_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Checkbook' when @iIntEnum = 1 then 'Customer' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Credit_Limit_Type]'
GO

create function [dbo].[DYN_FUNC_Credit_Limit_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Credit' when @iIntEnum = 1 then 'Unlimited' when @iIntEnum = 2 then 'Amount' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Commission_Applied_To]'
GO

create function [dbo].[DYN_FUNC_Commission_Applied_To] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Sales' when @iIntEnum = 1 then 'Invoice Total' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Balance_Type]'
GO

create function [dbo].[DYN_FUNC_Balance_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Open Item' when @iIntEnum = 1 then 'Balance Forward' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Allocate_By]'
GO

create function [dbo].[DYN_FUNC_Allocate_By] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Line Item' when @iIntEnum = 2 then 'Document/Batch' else ''  end  RETURN(@oVarcharValuestring)  END  
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppSalespersonID]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppSalespersonID]  (  @action int,  @SalespersonID varchar(15),  @ModuleID int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @ModIdString varchar(15),  @FunctionName varchar(50),  @EncodedSalespersonID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedSalespersonID = DYNAMICS.dbo.encodeUrlString(rtrim(@SalespersonID));   if @ModuleID < 1  select @ModIdString = '2'  else  select @ModIdString = ltrim(str(@ModuleID))  select @URIstring = '&Act=' + @ActionType + '&Func=OpenSlsprsn' + '&SLPRSNID=' + ltrim(@EncodedSalespersonID) + '&ModuleID=' + @ModIdString   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppSalesOrderSOP]'
GO


 CREATE FUNCTION [dbo].[dgppSalesOrderSOP]  (  @action int,  @DCSTATUS int,  @SOURCE int,  @SOPNUMBE varchar(20),  @SOPTYPE int,  @ModuleID int,  @CurrencyView int,  @CallerID int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedSOPNUMBE varchar(255)   select @FunctionName = 'OpenSOP'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedSOPNUMBE = DYNAMICS.dbo.encodeUrlString(rtrim(@SOPNUMBE));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&SOURCE=' + ltrim(str(@SOURCE)) +  '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&SOPNUMBE=' + ltrim(@EncodedSOPNUMBE) +  '&SOPTYPE=' + ltrim(str(@SOPTYPE)) +  '&ModuleID=' + ltrim(str(@ModuleID)) +  '&CurncyView=' + ltrim(str(@CurrencyView)) +  '&CallID=' + ltrim(str(@CallerID))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppSalesOrderRM]'
GO


 CREATE FUNCTION [dbo].[dgppSalesOrderRM]  (  @action int,  @DCSTATUS int,  @SOURCE int,  @SCHEDULE_NUMBER varchar(20),  @Status int,  @DOCTYPE int,  @CUSTNMBR varchar(15),  @DOCNUMBR varchar(20),  @VOIDED int,  @CurrencyView int,  @CallerID int,  @INVCNMBR varchar(20),  @SOPTYPE int,  @SOPNUMBE varchar(20),  @ModuleID int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedINVCNMBR varchar(255),  @EncodedCUSTNMBR varchar(255),  @EncodedDOCNUMBR varchar(255),  @EncodedSOPNUMBE varchar(255),  @EncodedSCHEDULE_NUMBER varchar(255)   select @FunctionName = 'OpenSOPRM'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedINVCNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@INVCNMBR))  select @EncodedCUSTNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@CUSTNMBR))  select @EncodedDOCNUMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR))  select @EncodedSOPNUMBE = DYNAMICS.dbo.encodeUrlString(rtrim(@SOPNUMBE))  select @EncodedSCHEDULE_NUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@SCHEDULE_NUMBER))   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&SOURCE=' + ltrim(str(@SOURCE)) +  '&SCHEDULE_NUMBER=' + ltrim(@EncodedSCHEDULE_NUMBER) +  '&Status=' + ltrim(str(@Status)) +  '&DOCTYPE=' + ltrim(str(@DOCTYPE)) +  '&CUSTNMBR=' + ltrim(@EncodedCUSTNMBR) +  '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBR) +  '&VOIDED=' + ltrim(str(@VOIDED)) +  '&CurncyView=' + ltrim(str(@CurrencyView)) +  '&CallID=' + ltrim(str(@CallerID)) +  '&INVCNMBR=' + ltrim(@EncodedINVCNMBR) +  '&SOPTYPE=' + ltrim(str(@SOPTYPE)) +  '&SOPNUMBE=' + ltrim(@EncodedSOPNUMBE) +  '&ModuleID=' + ltrim(str(@ModuleID))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppSalesOrderIVC]'
GO


 CREATE FUNCTION [dbo].[dgppSalesOrderIVC]  (  @action int,  @DCSTATUS int,  @SOURCE int,  @INVCNMBR varchar(20),  @DOCTYPE int,  @CUSTNMBR varchar(15),  @DOCNUMBR varchar(20)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedINVCNMBR varchar(255),  @EncodedCUSTNMBR varchar(255),  @EncodedDOCNUMBR varchar(255)   select @FunctionName = 'OpenSOPIVC'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedINVCNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@INVCNMBR));  select @EncodedCUSTNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@CUSTNMBR));  select @EncodedDOCNUMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&SOURCE=' + ltrim(str(@SOURCE)) +  '&INVCNMBR=' + ltrim(@EncodedINVCNMBR) +  '&DOCTYPE=' + ltrim(str(@DOCTYPE)) +  '&CUSTNMBR=' + ltrim(@EncodedCUSTNMBR) +  '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBR)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppSalesOrder]'
GO


 CREATE FUNCTION [dbo].[dgppSalesOrder]  (  @action int,  @DCSTATUS int,  @SOURCE int,  @SCHEDULE_NUMBER varchar(20),  @Status int,  @DOCTYPE int,  @CUSTNMBR varchar(15),  @DOCNUMBR varchar(20),  @VOIDED int,  @INVCNMBR varchar(20),  @SOPTYPE int,  @SOPNUMBE varchar(20)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @URIstring varchar(255)   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring =   case @SOURCE  when 10 then dbo.dgppSalesOrderIVC(1,@DCSTATUS,@SOURCE,@INVCNMBR,@DOCTYPE,@CUSTNMBR,@DOCNUMBR)  when 11 then dbo.dgppSalesOrderSOP(1,@DCSTATUS,@SOURCE,@SOPNUMBE,@SOPTYPE,11,1,1102)  else dbo.dgppSalesOrderRM(1,@DCSTATUS,@SOURCE,@SCHEDULE_NUMBER,@Status,@DOCTYPE,@CUSTNMBR,@DOCNUMBR,@VOIDED,1,1102,@INVCNMBR,@SOPTYPE,@SOPNUMBE,11)  end   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppCustomerID]'
GO


 CREATE FUNCTION [dbo].[dgppCustomerID]  (  @action int,  @CustomerNumber varchar(15)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedCustomerNumber varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenCustNmbr'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedCustomerNumber = DYNAMICS.dbo.encodeUrlString(rtrim(@CustomerNumber));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&CUSTNMBR=' + ltrim(@EncodedCustomerNumber)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppCheckbookID]'
GO

 CREATE FUNCTION [dbo].[dgppCheckbookID]  (  @action int,  @CheckbookID varchar(15)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedCheckbookID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenChkBkID'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedCheckbookID = DYNAMICS.dbo.encodeUrlString(rtrim(@CheckbookID));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&CHEKBKID=' + @EncodedCheckbookID  RETURN(@URIstring) END   
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SalesTransactions]'
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE VIEW [dbo].[SalesTransactions] AS select 'SOP Type' = dbo.DYN_FUNC_SOP_Type(['Sales Transaction Work'].[SOPTYPE]), rtrim(['Sales Transaction Work'].[SOPNUMBE]) as 'SOP Number', ['Sales Transaction Work'].[DOCDATE] as 'Document Date', rtrim(['Sales Transaction Work'].[CUSTNMBR]) as 'Customer Number', rtrim(['Sales Transaction Work'].[CUSTNAME]) as 'Customer Name', rtrim(['Sales Transaction Work'].[CSTPONBR]) as 'Customer PO Number', rtrim(['Sales Transaction Work'].[PRSTADCD]) as 'Primary Shipto Address Code', 'Sales Document Status' = dbo.DYN_FUNC_SOP_Status(['Sales Transaction Work'].[SOPSTATUS]), ['Sales Transaction Work'].[DOCAMNT] as 'Document Amount',  ['Sales Transaction Work'].[ACCTAMNT] as 'Account Amount', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMARACC]) as 'Accounts Receivable Account Number', ['Sales Transaction Work'].[ACTLSHIP] as 'Actual Ship Date', rtrim(['Sales Transaction Work'].[ADDRESS1]) as 'Address 1', rtrim(['RM Customer MSTR'].[ADDRESS1]) as 'Address 1 from Customer Master', rtrim(['Sales Transaction Work'].[ADDRESS2]) as 'Address 2', rtrim(['RM Customer MSTR'].[ADDRESS2]) as 'Address 2 from Customer Master', rtrim(['Sales Transaction Work'].[ADDRESS3]) as 'Address 3', rtrim(['RM Customer MSTR'].[ADDRESS3]) as 'Address 3 from Customer Master', rtrim(['RM Customer MSTR'].[ADRSCODE]) as 'Address Code from Customer Master', ['Customer Master Summary'].[AGPERAMT_1] as 'Aging Bucket1', ['Customer Master Summary'].[AGPERAMT_2] as 'Aging Bucket2', ['Customer Master Summary'].[AGPERAMT_3] as 'Aging Bucket3', ['Customer Master Summary'].[AGPERAMT_4] as 'Aging Bucket4', ['Customer Master Summary'].[AGPERAMT_5] as 'Aging Bucket5', ['Customer Master Summary'].[AGPERAMT_6] as 'Aging Bucket6', ['Customer Master Summary'].[AGPERAMT_7] as 'Aging Bucket7', 'Allocate By' = dbo.DYN_FUNC_Allocate_By(['Sales Transaction Work'].[ALLOCABY]), 'Apply Withholding' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[APLYWITH]), ['Customer Master Summary'].[AVDTPLIF] as 'Average Days To Pay - Life', ['Customer Master Summary'].[AVGDTPYR] as 'Average Days To Pay - Year', ['Customer Master Summary'].[AVDTPLYR] as 'Average Days to Pay - LYR', ['Sales Transaction Work'].[BACKDATE] as 'Back Order Date', ['Sales Transaction Work'].[BKTFRTAM] as 'Backout Freight Amount', ['Sales Transaction Work'].[BKTMSCAM] as 'Backout Misc Amount', ['Sales Transaction Work'].[BCKTXAMT] as 'Backout Tax Amount', 'Balance Type' = dbo.DYN_FUNC_Balance_Type(['RM Customer MSTR'].[BALNCTYP]), rtrim(['RM Customer MSTR'].[BNKBRNCH]) as 'Bank Branch', rtrim(['RM Customer MSTR'].[BANKNAME]) as 'Bank Name', 'Based On Invoice Total' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[BSIVCTTL]), rtrim(['Sales Transaction Work'].[BACHNUMB]) as 'Batch Number', rtrim(['Sales Transaction Work'].[BCHSOURC]) as 'Batch Source', ['Sales Transaction Work'].[CODAMNT] as 'COD Amount', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMCOSACC]) as 'COGS Account Number', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMCSHACC]) as 'Cash Account Number', rtrim(['RM Customer MSTR'].[CHEKBKID]) as 'Checkbook ID', rtrim(['Sales Transaction Work'].[CITY]) as 'City', rtrim(['RM Customer MSTR'].[CITY]) as 'City from Customer Master', rtrim(['RM Customer MSTR'].[COMMENT1]) as 'Comment1 from Customer Master', rtrim(['RM Customer MSTR'].[COMMENT2]) as 'Comment2 from Customer Master', rtrim(['Sales User-Defined Work History'].[COMMENT_1]) as 'Comment 1', rtrim(['Sales User-Defined Work History'].[COMMENT_2]) as 'Comment 2', rtrim(['Sales User-Defined Work History'].[COMMENT_3]) as 'Comment 3', rtrim(['Sales User-Defined Work History'].[COMMENT_4]) as 'Comment 4', rtrim(['Sales Transaction Work'].[COMMNTID]) as 'Comment ID', ['Sales Transaction Work'].[COMMAMNT] as 'Commission Amount', 'Commission Applied To' = dbo.DYN_FUNC_Commission_Applied_To(['Sales Transaction Work'].[COMAPPTO]), ['Sales Transaction Work'].[CMMSLAMT] as 'Commission Sale Amount', rtrim(['Sales Transaction Work'].[CNTCPRSN]) as 'Contact Person', rtrim(['RM Customer MSTR'].[CNTCPRSN]) as 'Contact Person from Customer Master', rtrim(['RM Customer MSTR'].[CPRCSTNM]) as 'Corporate Customer Number', 'Correction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[CORRCTN]), 'Correction to Nonexisting Transaction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[CORRNXST]), rtrim(['Sales Transaction Work'].[COUNTRY]) as 'Country', rtrim(['RM Customer MSTR'].[COUNTRY]) as 'Country from Customer Master', ['Sales Transaction Work'].[CREATDDT] as 'Created Date', ['RM Customer MSTR'].[CREATDDT] as 'Created Date from Customer Master', ['RM Customer MSTR'].[CCRDXPDT] as 'Credit Card Exp Date', rtrim(['RM Customer MSTR'].[CRCARDID]) as 'Credit Card ID', ['RM Customer MSTR'].[CRLMTAMT] as 'Credit Limit Amount', ['RM Customer MSTR'].[CRLMTPER] as 'Credit Limit Period', ['RM Customer MSTR'].[CRLMTPAM] as 'Credit Limit Period Amount', 'Credit Limit Type' = dbo.DYN_FUNC_Credit_Limit_Type(['RM Customer MSTR'].[CRLMTTYP]), rtrim(['Sales Transaction Work'].[CURNCYID]) as 'Currency ID', rtrim(['RM Customer MSTR'].[CURNCYID]) as 'Currency ID from Customer Master',  ['Sales Transaction Work'].[CURRNIDX] as 'Currency Index', ['Customer Master Summary'].[CUSTBLNC] as 'Customer Balance', rtrim(['RM Customer MSTR'].[CUSTCLAS]) as 'Customer Class', ['RM Customer MSTR'].[CUSTDISC]/100.00 as 'Customer Discount', rtrim(['RM Customer MSTR'].[CUSTNAME]) as 'Customer Name from Customer Master', ['Sales Transaction Work'].[DTLSTREP] as 'Date Last Repeated', ['Sales Transaction Work'].[DYSTINCR] as 'Days to Increment', 'Default Cash Account Type' = dbo.DYN_FUNC_Default_Cash_Account_Type(['RM Customer MSTR'].[DEFCACTY]), ['Sales Transaction Work'].[DENXRATE] as 'Denomination Exchange Rate', ['Sales Transaction Work'].[DEPRECVD] as 'Deposit Received', ['Customer Master Summary'].[DEPRECV] as 'Deposits Received', rtrim(['Sales Transaction Work'].[DSTBTCH1]) as 'Dest Batch 1', rtrim(['Sales Transaction Work'].[DSTBTCH2]) as 'Dest Batch 2', ['Sales Transaction Work'].[DISAVAMT] as 'Discount Available Amount', ['Sales Transaction Work'].[DISCFRGT] as 'Discount Available Freight', ['Sales Transaction Work'].[DISCMISC] as 'Discount Available Misc', ['Sales Transaction Work'].[DISAVTKN] as 'Discount Available Taken', ['Sales Transaction Work'].[DISCDATE] as 'Discount Date', ['Sales Transaction Work'].[DSCDLRAM] as 'Discount Dollar Amount', ['RM Customer MSTR'].[DISGRPER] as 'Discount Grace Period', ['Sales Transaction Work'].[DSCPCTAM]/100.00 as 'Discount Percent Amount', ['Sales Transaction Work'].[DISCRTND] as 'Discount Returned', ['Sales Transaction Work'].[DISTKNAM] as 'Discount Taken Amount', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMAVACC]) as 'Discounts Available Account Number', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMTAKACC]) as 'Discounts Taken Account Number', rtrim(['RM Customer MSTR'].[DOCFMTID]) as 'Document Format ID', rtrim(['Sales Transaction Work'].[DOCID]) as 'Document ID', rtrim(['Sales Transaction Work'].[DOCNCORR]) as 'Document Number Corrected', 'Document Status' = dbo.DYN_FUNC_Document_Status_Sls_Trx(1), ['Sales Transaction Work'].[DUEDATE] as 'Due Date', ['RM Customer MSTR'].[DUEGRPER] as 'Due Date Grace Period', 'EC Transaction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[ECTRX]), 'Exceptional Demand from Sales Transaction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[EXCEPTIONALDEMAND]), ['Sales Transaction Work'].[EXCHDATE] as 'Exchange Date', ['Sales Transaction Work'].[XCHGRATE] as 'Exchange Rate', rtrim(['Sales Transaction Work'].[EXGTBLID]) as 'Exchange Table ID', ['Sales Transaction Work'].[EXTDCOST] as 'Extended Cost', rtrim(['Sales Transaction Work'].[FAXNUMBR]) as 'Fax Number', rtrim(['RM Customer MSTR'].[FAX]) as 'Fax from Customer Master', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMFCGACC]) as 'Finance Charge Account Number', 'Finance Charge Amt Type' = dbo.DYN_FUNC_Finance_Charge_Amt_Type(['RM Customer MSTR'].[FNCHATYP]), ['RM Customer MSTR'].[FINCHDLR] as 'Finance Charge Dollar', rtrim(['RM Customer MSTR'].[FINCHID]) as 'Finance Charge ID', ['RM Customer MSTR'].[FNCHPCNT]/100.00 as 'Finance Charge Percent', ['Customer Master Summary'].[FNCHCYTD] as 'Finance Charges CYTD', ['Customer Master Summary'].[FNCHLYRC] as 'Finance Charges LYR Calendar', ['RM Customer MSTR'].[FRSTINDT] as 'First Invoice Date', ['Sales Transaction Work'].[FRTAMNT] as 'Freight Amount', rtrim(['Sales Transaction Work'].[FRTSCHID]) as 'Freight Schedule ID', ['Sales Transaction Work'].[FRTTXAMT] as 'Freight Tax Amount', 'Freight Taxable' = dbo.DYN_FUNC_Freight_Taxable(['Sales Transaction Work'].[FRGTTXBL]), ['Sales Transaction Work'].[FUFILDAT] as 'Fulfillment Date', ['Sales Transaction Work'].[GLPOSTDT] as 'GL Posting Date', rtrim(['RM Customer MSTR'].[GOVCRPID]) as 'Governmental Corporate ID', rtrim(['RM Customer MSTR'].[GOVINDID]) as 'Governmental Individual ID', ['Customer Master Summary'].[HIBALLTD] as 'High Balance LTD', ['Customer Master Summary'].[HIBALLYR] as 'High Balance LYR', ['Customer Master Summary'].[HIBALYTD] as 'High Balance YTD', 'Hold' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[HOLD]), 'Inactive' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[INACTIVE]), (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMIVACC]) as 'Inventory Account Number', ['Sales Transaction Work'].[INVODATE] as 'Invoice Date', 'Keep Calendar History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPCALHST]), 'Keep Distribution History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPDSTHST]), 'Keep Period History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPERHIST]), 'Keep Trx History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPTRXHST]), ['Customer Master Summary'].[LASTAGED] as 'Last Aged', ['Customer Master Summary'].[LSTFCHAM] as 'Last Finance Charge Amount', ['Customer Master Summary'].[LSTNSFCD] as 'Last NSF Check Date', ['Customer Master Summary'].[LPYMTAMT] as 'Last Payment Amount', ['Customer Master Summary'].[LASTPYDT] as 'Last Payment Date', ['Customer Master Summary'].[LSTSTAMT] as 'Last Statement Amount', ['Customer Master Summary'].[LASTSTDT] as 'Last Statement Date', ['Customer Master Summary'].[LSTTRXAM] as 'Last Transaction Amount', ['Customer Master Summary'].[LSTTRXDT] as 'Last Transaction Date', rtrim(['Sales Transaction Work'].[LOCNCODE]) as 'Location Code', 'MC Transaction State' = dbo.DYN_FUNC_MC_Transaction_State(['Sales Transaction Work'].[MCTRXSTT]),  ['Sales Transaction Work'].[MRKDNAMT] as 'Markdown Amount', ['Sales Transaction Work'].[MSTRNUMB] as 'Master Number', ['RM Customer MSTR'].[MXWROFAM] as 'Max Writeoff Amount', 'Maximum Writeoff Type' = dbo.DYN_FUNC_Maximum_Writeoff_Type(['RM Customer MSTR'].[MXWOFTYP]), ['RM Customer MSTR'].[MINPYDLR] as 'Minimum Payment Dollar', ['RM Customer MSTR'].[MINPYPCT]/100.00 as 'Minimum Payment Percent', 'Minimum Payment Type' = dbo.DYN_FUNC_Minimum_Payment_Type(['RM Customer MSTR'].[MINPYTYP]), ['Sales Transaction Work'].[MISCAMNT] as 'Misc Amount', rtrim(['Sales Transaction Work'].[MSCSCHID]) as 'Misc Schedule ID', ['Sales Transaction Work'].[MSCTXAMT] as 'Misc Tax Amount', 'Misc Taxable' = dbo.DYN_FUNC_Misc_Taxable(['Sales Transaction Work'].[MISCTXBL]), ['Sales Transaction Work'].[MODIFDT] as 'Modified Date', ['RM Customer MSTR'].[MODIFDT] as 'Modified Date from Customer Master', ['Sales Transaction Work'].[NCOMAMNT] as 'Non-Commissioned Amount', ['Customer Master Summary'].[NCSCHPMT] as 'Non Current Scheduled Payments', ['Sales Transaction Work'].[NOTEINDX] as 'Note Index', ['RM Customer MSTR'].[NOTEINDX] as 'Note Index from Customer Master', ['Customer Master Summary'].[NUMADTPR] as 'Number ADTP Documents - LYR', ['Customer Master Summary'].[NUMADTPL] as 'Number ADTP Documents - Life', ['Customer Master Summary'].[NUMADTPY] as 'Number ADTP Documents - Year', ['Customer Master Summary'].[NONSFLIF] as 'Number Of NSF Checks Life', ['Customer Master Summary'].[NONSFYTD] as 'Number Of NSF Checks YTD', ['Customer Master Summary'].[ONORDAMT] as 'On Order Amount', ['Sales Transaction Work'].[ORDRDATE] as 'Order Date', 'Order Fulfillment Shortage Default' = dbo.DYN_FUNC_Order_Fulfillment_Shortage_Default(['RM Customer MSTR'].[ORDERFULFILLDEFAULT]), rtrim(['Sales Transaction Work'].[ORIGNUMB]) as 'Original Number', 'Original Type' = dbo.DYN_FUNC_Original_Type(['Sales Transaction Work'].[ORIGTYPE]), ['Sales Transaction Work'].[ORACTAMT] as 'Originating Account Amount', ['Sales Transaction Work'].[ORBKTFRT] as 'Originating Backout Freight Amount', ['Sales Transaction Work'].[ORBKTMSC] as 'Originating Backout Misc Amount', ['Sales Transaction Work'].[OBTAXAMT] as 'Originating Backout Tax Amount', ['Sales Transaction Work'].[ORCODAMT] as 'Originating COD Amount', ['Sales Transaction Work'].[OCOMMAMT] as 'Originating Commission Amount', ['Sales Transaction Work'].[ORCOSAMT] as 'Originating Commission Sales Amount', ['Sales Transaction Work'].[ORDEPRVD] as 'Originating Deposit Received', ['Sales Transaction Work'].[ORDAVAMT] as 'Originating Discount Available Amount', ['Sales Transaction Work'].[ORDAVFRT] as 'Originating Discount Available Freight', ['Sales Transaction Work'].[ORDAVMSC] as 'Originating Discount Available Misc', ['Sales Transaction Work'].[ORDATKN] as 'Originating Discount Available Taken', ['Sales Transaction Work'].[ORDDLRAT] as 'Originating Discount Dollar Amount', ['Sales Transaction Work'].[ORDISRTD] as 'Originating Discount Returned', ['Sales Transaction Work'].[ORDISTKN] as 'Originating Discount Taken Amount', ['Sales Transaction Work'].[ORDOCAMT] as 'Originating Document Amount', ['Sales Transaction Work'].[OREXTCST] as 'Originating Extended Cost', ['Sales Transaction Work'].[ORFRTAMT] as 'Originating Freight Amount', ['Sales Transaction Work'].[ORFRTTAX] as 'Originating Freight Tax Amount', ['Sales Transaction Work'].[ORMRKDAM] as 'Originating Markdown Amount', ['Sales Transaction Work'].[ORMISCAMT] as 'Originating Misc Amount', ['Sales Transaction Work'].[ORMSCTAX] as 'Originating Misc Tax Amount', ['Sales Transaction Work'].[ORNCMAMT] as 'Originating Non-Commissioned Amount', ['Sales Transaction Work'].[ORPMTRVD] as 'Originating Payment Received', ['Sales Transaction Work'].[OREMSUBT] as 'Originating Remaining Subtotal', ['Sales Transaction Work'].[ORSUBTOT] as 'Originating Subtotal', ['Sales Transaction Work'].[ORTAXAMT] as 'Originating Tax Amount', ['Sales Transaction Work'].[OTAXTAMT] as 'Originating Taxable Tax Amount', ['Sales Transaction Work'].[ORTDISAM] as 'Originating Trade Discount Amount', rtrim(['Sales Transaction Work'].[PCKSLPNO]) as 'Packing Slip Number', ['Sales Transaction Work'].[PYMTRCVD] as 'Payment Received', rtrim(['Sales Transaction Work'].[PYMTRMID]) as 'Payment Terms ID', rtrim(['RM Customer MSTR'].[PYMTRMID]) as 'Payment Terms ID from Customer Master', rtrim(['RM Customer MSTR'].[PHONE1]) as 'Phone 1 from Customer Master', rtrim(['RM Customer MSTR'].[PHONE2]) as 'Phone 2 from Customer Master', rtrim(['Sales Transaction Work'].[PHONE3]) as 'Phone 3', rtrim(['RM Customer MSTR'].[PHONE3]) as 'Phone 3 from Customer Master', rtrim(['Sales Transaction Work'].[PHNUMBR1]) as 'Phone Number 1', rtrim(['Sales Transaction Work'].[PHNUMBR2]) as 'Phone Number 2', rtrim(['Sales Transaction Work'].[PICTICNU]) as 'Picking Ticket Number', 'Post Results To' = dbo.DYN_FUNC_Post_Results_To_Customer(['RM Customer MSTR'].[Post_Results_To]), ['Sales Transaction Work'].[POSTEDDT] as 'Posted Date', rtrim(['Sales Transaction Work'].[PTDUSRID]) as 'Posted User ID', 'Posting Status' = dbo.DYN_FUNC_Posting_Status_SOP_Line_Items(['Sales Transaction Work'].[PSTGSTUS]), rtrim(['Sales Transaction Work'].[PRCLEVEL]) as 'PriceLevel', rtrim(['RM Customer MSTR'].[PRCLEVEL]) as 'PriceLevel from Customer Master', rtrim(['Sales Transaction Work'].[PRBTADCD]) as 'Primary Billto Address Code', rtrim(['RM Customer MSTR'].[PRBTADCD]) as 'Primary Billto Address Code from Customer Master', rtrim(['RM Customer MSTR'].[PRSTADCD]) as 'Primary Shipto Address Code from Customer Master', 'Priority from Customer Master' = dbo.DYN_FUNC_Priority(['RM Customer MSTR'].[CUSTPRIORITY]),  'Prospect' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[PROSPECT]), ['Sales Transaction Work'].[QUOTEDAT] as 'Quote Date', ['Sales Transaction Work'].[QUOEXPDA] as 'Quote Expiration Date', 'Rate Calculation Method' = dbo.DYN_FUNC_Rate_Calculation_Method(['Sales Transaction Work'].[RTCLCMTD]), rtrim(['Sales Transaction Work'].[RATETPID]) as 'Rate Type ID', rtrim(['RM Customer MSTR'].[RATETPID]) as 'Rate Type ID from Customer Master', rtrim(['Sales Transaction Work'].[REFRENCE]) as 'Reference', ['Sales Transaction Work'].[REMSUBTO] as 'Remaining Subtotal', 'Repeating' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[REPTING]), ['Sales Transaction Work'].[ReqShipDate] as 'Requested Ship Date', ['Customer Master Summary'].[RETAINAG] as 'Retainage', ['Sales Transaction Work'].[RETUDATE] as 'Return Date', 'Revalue Customer' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[Revalue_Customer]), ['Sales Transaction Work'].[SALEDATE] as 'Sale Date', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMSLSACC]) as 'Sales Account Number', rtrim(['Sales Transaction Work'].[SALSTERR]) as 'Sales Territory', rtrim(['RM Customer MSTR'].[SALSTERR]) as 'Sales Territory from Customer Master', rtrim(['Sales Transaction Work'].[SLPRSNID]) as 'Salesperson ID', rtrim(['RM Customer MSTR'].[SLPRSNID]) as 'Salesperson ID from Customer Master', ['Sales Transaction Work'].[SEQNCORR] as 'Sequence Number Corrected', rtrim(['Sales Transaction Work'].[ShipToName]) as 'ShipToName', 'Ship Complete Document' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[SHIPCOMPLETE]), 'Ship Complete Document from Customer Master' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[SHIPCOMPLETE]), 'Shipping Document' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[SHPPGDOC]), rtrim(['Sales Transaction Work'].[SHIPMTHD]) as 'Shipping Method', rtrim(['RM Customer MSTR'].[SHIPMTHD]) as 'Shipping Method from Customer Master', rtrim(['RM Customer MSTR'].[SHRTNAME]) as 'Short Name', 'Simplified' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[SIMPLIFD]), rtrim(['Sales Transaction Work'].[STATE]) as 'State', rtrim(['RM Customer MSTR'].[STATE]) as 'State from Customer Master', rtrim(['RM Customer MSTR'].[STADDRCD]) as 'Statement Address Code', 'Statement Cycle' = dbo.DYN_FUNC_Statement_Cycle(['RM Customer MSTR'].[STMTCYCL]), rtrim(['RM Customer MSTR'].[STMTNAME]) as 'Statement Name', ['Sales Transaction Work'].[SUBTOTAL] as 'Subtotal', 'TRX Frequency' = dbo.DYN_FUNC_TRX_Frequency(['Sales Transaction Work'].[TRXFREQU]), rtrim(['Sales Transaction Work'].[TRXSORCE]) as 'TRX Source', ['Sales Transaction Work'].[TAXAMNT] as 'Tax Amount', ['Sales Transaction Work'].[Tax_Date] as 'Tax Date', 'Tax Engine Called' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction Work'].[TXENGCLD]), rtrim(['Sales Transaction Work'].[TAXEXMT1]) as 'Tax Exempt 1', rtrim(['RM Customer MSTR'].[TAXEXMT1]) as 'Tax Exempt 1 from Customer Master', rtrim(['Sales Transaction Work'].[TAXEXMT2]) as 'Tax Exempt 2', rtrim(['RM Customer MSTR'].[TAXEXMT2]) as 'Tax Exempt 2 from Customer Master', rtrim(['Sales Transaction Work'].[TXRGNNUM]) as 'Tax Registration Number', rtrim(['RM Customer MSTR'].[TXRGNNUM]) as 'Tax Registration Number from Customer Master', rtrim(['Sales Transaction Work'].[TAXSCHID]) as 'Tax Schedule ID', rtrim(['RM Customer MSTR'].[TAXSCHID]) as 'Tax Schedule ID from Customer Master', 'Tax Schedule Source' = dbo.DYN_FUNC_Tax_Schedule_Source(['Sales Transaction Work'].[TXSCHSRC]), ['Sales Transaction Work'].[TXBTXAMT] as 'Taxable Tax Amount', ['Sales Transaction Work'].[TIME1] as 'Time', ['Sales Transaction Work'].[TIMESPRT]/100.00 as 'Times Printed', ['Sales Transaction Work'].[TIMEREPD] as 'Times Repeated', ['Sales Transaction Work'].[TIMETREP] as 'Times To Repeat', ['Customer Master Summary'].[TTLFCLTD] as 'Total # FC LTD', ['Customer Master Summary'].[TTLFCLYR] as 'Total # FC LYR', ['Customer Master Summary'].[TTLFCYTD] as 'Total # FC YTD', ['Customer Master Summary'].[TTLINLTD] as 'Total # Invoices LTD', ['Customer Master Summary'].[TTLINLYR] as 'Total # Invoices LYR', ['Customer Master Summary'].[TTLINYTD] as 'Total # Invoices YTD', ['Customer Master Summary'].[TNSFCLIF] as 'Total Amount Of NSF Checks Life', ['Customer Master Summary'].[TNSFCYTD] as 'Total Amount Of NSF Checks YTD', ['Customer Master Summary'].[TBDDTLYR] as 'Total Bad Deb LYR', ['Customer Master Summary'].[TBDDTLTD] as 'Total Bad Debt LTD', ['Customer Master Summary'].[TBDDTYTD] as 'Total Bad Debt YTD', ['Customer Master Summary'].[TCSHRLTD] as 'Total Cash Received LTD', ['Customer Master Summary'].[TCSHRLYR] as 'Total Cash Received LYR', ['Customer Master Summary'].[TCSHRYTD] as 'Total Cash Received YTD', ['Customer Master Summary'].[TCOSTLTD] as 'Total Costs LTD', ['Customer Master Summary'].[TCOSTLYR] as 'Total Costs LYR', ['Customer Master Summary'].[TCOSTYTD] as 'Total Costs YTD', ['Customer Master Summary'].[TDISAYTD] as 'Total Discounts Available YTD', ['Customer Master Summary'].[TDTKNLTD] as 'Total Discounts Taken LTD', ['Customer Master Summary'].[TDTKNLYR] as 'Total Discounts Taken LYR', ['Customer Master Summary'].[TDTKNYTD] as 'Total Discounts Taken YTD', ['Customer Master Summary'].[TFNCHLTD] as 'Total Finance Charges LTD', ['Customer Master Summary'].[TFNCHLYR] as 'Total Finance Charges LYR', ['Customer Master Summary'].[TFNCHYTD] as 'Total Finance Charges YTD', ['Customer Master Summary'].[TTLRTLTD] as 'Total Returns LTD', ['Customer Master Summary'].[TTLRTLYR] as 'Total Returns LYR',  ['Customer Master Summary'].[TTLRTYTD] as 'Total Returns YTD', ['Customer Master Summary'].[TTLSLLTD] as 'Total Sales LTD', ['Customer Master Summary'].[TTLSLLYR] as 'Total Sales LYR', ['Customer Master Summary'].[TTLSLYTD] as 'Total Sales YTD', ['Customer Master Summary'].[TWVFCLTD] as 'Total Waived FC LTD', ['Customer Master Summary'].[TWVFCLYR] as 'Total Waived FC LYR', ['Customer Master Summary'].[TWVFCYTD] as 'Total Waived FC YTD', ['Customer Master Summary'].[TWROFLTD] as 'Total Writeoffs LTD', ['Customer Master Summary'].[TWROFLYR] as 'Total Writeoffs LYR', ['Customer Master Summary'].[TWROFYTD] as 'Total Writeoffs YTD', ['Sales Transaction Work'].[TRDISAMT] as 'Trade Discount Amount', ['Sales Transaction Work'].[TRDISPCT] as 'Trade Discount Percent', rtrim(['Sales Transaction Work'].[UPSZONE]) as 'UPS Zone', rtrim(['RM Customer MSTR'].[UPSZONE]) as 'UPS Zone from Customer Master', ['Customer Master Summary'].[UPFCHYTD] as 'Unpaid Finance Charges YTD', ['Customer Master Summary'].[UNPSTDCA] as 'Unposted Cash Amount', ['Customer Master Summary'].[UNPSTOCA] as 'Unposted Other Cash Amount', ['Customer Master Summary'].[UNPSTOSA] as 'Unposted Other Sales Amount', ['Customer Master Summary'].[UNPSTDSA] as 'Unposted Sales Amount', rtrim(['Sales Transaction Work'].[USDOCID1]) as 'Use Document ID 1', rtrim(['Sales Transaction Work'].[USDOCID2]) as 'Use Document ID 2', rtrim(['Sales User-Defined Work History'].[USERDEF1]) as 'User Defined 1', rtrim(['RM Customer MSTR'].[USERDEF1]) as 'User Defined 1 from Customer Master', rtrim(['Sales User-Defined Work History'].[USERDEF2]) as 'User Defined 2', rtrim(['RM Customer MSTR'].[USERDEF2]) as 'User Defined 2 from Customer Master', rtrim(['Sales User-Defined Work History'].[USRDEF03]) as 'User Defined 3', rtrim(['Sales User-Defined Work History'].[USRDEF04]) as 'User Defined 4', rtrim(['Sales User-Defined Work History'].[USRDEF05]) as 'User Defined 5', ['Sales User-Defined Work History'].[USRDAT01] as 'User Defined Date 1', ['Sales User-Defined Work History'].[USRDAT02] as 'User Defined Date 2', rtrim(['Sales User-Defined Work History'].[USRTAB01]) as 'User Defined Table 1', rtrim(['Sales User-Defined Work History'].[USRTAB09]) as 'User Defined Table 2', rtrim(['Sales User-Defined Work History'].[USRTAB03]) as 'User Defined Table 3', rtrim(['Sales Transaction Work'].[USER2ENT]) as 'User To Enter', 'Void Status' = dbo.DYN_FUNC_Void_Status(['Sales Transaction Work'].[VOIDSTTS]), ['Sales Transaction Work'].[WITHHAMT] as 'Withholding Amount', ['Customer Master Summary'].[WROFSLIF] as 'Write Offs LIFE', ['Customer Master Summary'].[WROFSLYR] as 'Write Offs LYR', ['Customer Master Summary'].[WROFSYTD] as 'Write Offs YTD', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMWRACC]) as 'Writeoff Account Number', rtrim(['Sales Transaction Work'].[ZIPCODE]) as 'Zip Code', rtrim(['RM Customer MSTR'].[ZIP]) as 'Zip from Customer Master', 'Workflow Approval Status Credit Limit' = dbo.DYN_FUNC_Workflow_Approval_Status(['Sales Transaction Work'].[WorkflowApprStatCreditLm]), 'Workflow Priority Credit Limit' = dbo.DYN_FUNC_Workflow_Priority(['Sales Transaction Work'].[WorkflowPriorityCreditLm]), 'Workflow Approval Status Quote' = dbo.DYN_FUNC_Workflow_Approval_Status(['Sales Transaction Work'].[WorkflowApprStatusQuote]), 'Workflow Priority Quote' = dbo.DYN_FUNC_Workflow_Priority(['Sales Transaction Work'].[WorkflowPriorityQuote]),  'Accounts Receivable Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMARACC] ), 'Cash Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMCSHACC] ), 'Checkbook ID For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppCheckbookID(1,['RM Customer MSTR'].[CHEKBKID] ), 'COGS Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMCOSACC] ),  'Customer Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppCustomerID(1,['Sales Transaction Work'].[CUSTNMBR] ), 'Discounts Available Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMAVACC] ), 'Discounts Taken Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMTAKACC] ), 'Finance Charge Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMFCGACC] ),  'Inventory Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMIVACC] ), 'Sales Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMSLSACC] ), 'Salesperson ID For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppSalespersonID(1,['Sales Transaction Work'].[SLPRSNID],2 ), 'SOP Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppSalesOrder(1,1,11,'',0,0,['Sales Transaction Work'].[CUSTNMBR],'',0,'',['Sales Transaction Work'].[SOPTYPE],['Sales Transaction Work'].[SOPNUMBE] ),  'Writeoff Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMWRACC] )  from [SOP10100] as ['Sales Transaction Work'] with (NOLOCK) left outer join [RM00101] as ['RM Customer MSTR'] with (NOLOCK) on ['Sales Transaction Work'].[CUSTNMBR] = ['RM Customer MSTR'].[CUSTNMBR] left outer join [RM00103] as ['Customer Master Summary'] with (NOLOCK) on ['Sales Transaction Work'].[CUSTNMBR] = ['Customer Master Summary'].[CUSTNMBR] left outer join [SOP10106] as ['Sales User-Defined Work History'] with (NOLOCK) on ['Sales Transaction Work'].[SOPTYPE] = ['Sales User-Defined Work History'].[SOPTYPE]  and ['Sales Transaction Work'].[SOPNUMBE] = ['Sales User-Defined Work History'].[SOPNUMBE]  union all select 'SOP Type' = dbo.DYN_FUNC_SOP_Type(['Sales Transaction History'].[SOPTYPE]), rtrim(['Sales Transaction History'].[SOPNUMBE]) as 'SOP Number', ['Sales Transaction History'].[DOCDATE] as 'Document Date', rtrim(['Sales Transaction History'].[CUSTNMBR]) as 'Customer Number', rtrim(['Sales Transaction History'].[CUSTNAME]) as 'Customer Name', rtrim(['Sales Transaction History'].[CSTPONBR]) as 'Customer PO Number', rtrim(['Sales Transaction History'].[PRSTADCD]) as 'Primary Shipto Address Code', 'Sales Document Status' = dbo.DYN_FUNC_SOP_Status(['Sales Transaction History'].[SOPSTATUS]), ['Sales Transaction History'].[DOCAMNT] as 'Document Amount',  ['Sales Transaction History'].[ACCTAMNT] as 'Account Amount', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMARACC]) as 'Accounts Receivable Account Number', ['Sales Transaction History'].[ACTLSHIP] as 'Actual Ship Date', rtrim(['Sales Transaction History'].[ADDRESS1]) as 'Address 1', rtrim(['RM Customer MSTR'].[ADDRESS1]) as 'Address 1 from Customer Master', rtrim(['Sales Transaction History'].[ADDRESS2]) as 'Address 2', rtrim(['RM Customer MSTR'].[ADDRESS2]) as 'Address 2 from Customer Master', rtrim(['Sales Transaction History'].[ADDRESS3]) as 'Address 3', rtrim(['RM Customer MSTR'].[ADDRESS3]) as 'Address 3 from Customer Master', rtrim(['RM Customer MSTR'].[ADRSCODE]) as 'Address Code from Customer Master', ['Customer Master Summary'].[AGPERAMT_1] as 'Aging Bucket1', ['Customer Master Summary'].[AGPERAMT_2] as 'Aging Bucket2', ['Customer Master Summary'].[AGPERAMT_3] as 'Aging Bucket3', ['Customer Master Summary'].[AGPERAMT_4] as 'Aging Bucket4', ['Customer Master Summary'].[AGPERAMT_5] as 'Aging Bucket5', ['Customer Master Summary'].[AGPERAMT_6] as 'Aging Bucket6', ['Customer Master Summary'].[AGPERAMT_7] as 'Aging Bucket7', 'Allocate By' = dbo.DYN_FUNC_Allocate_By(['Sales Transaction History'].[ALLOCABY]), 'Apply Withholding' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[APLYWITH]), ['Customer Master Summary'].[AVDTPLIF] as 'Average Days To Pay - Life', ['Customer Master Summary'].[AVGDTPYR] as 'Average Days To Pay - Year', ['Customer Master Summary'].[AVDTPLYR] as 'Average Days to Pay - LYR', ['Sales Transaction History'].[BACKDATE] as 'Back Order Date', ['Sales Transaction History'].[BKTFRTAM] as 'Backout Freight Amount', ['Sales Transaction History'].[BKTMSCAM] as 'Backout Misc Amount', ['Sales Transaction History'].[BCKTXAMT] as 'Backout Tax Amount', 'Balance Type' = dbo.DYN_FUNC_Balance_Type(['RM Customer MSTR'].[BALNCTYP]), rtrim(['RM Customer MSTR'].[BNKBRNCH]) as 'Bank Branch', rtrim(['RM Customer MSTR'].[BANKNAME]) as 'Bank Name', 'Based On Invoice Total' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[BSIVCTTL]), rtrim(['Sales Transaction History'].[BACHNUMB]) as 'Batch Number', rtrim(['Sales Transaction History'].[BCHSOURC]) as 'Batch Source', ['Sales Transaction History'].[CODAMNT] as 'COD Amount', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMCOSACC]) as 'COGS Account Number', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMCSHACC]) as 'Cash Account Number', rtrim(['RM Customer MSTR'].[CHEKBKID]) as 'Checkbook ID', rtrim(['Sales Transaction History'].[CITY]) as 'City', rtrim(['RM Customer MSTR'].[CITY]) as 'City from Customer Master', rtrim(['RM Customer MSTR'].[COMMENT1]) as 'Comment1 from Customer Master', rtrim(['RM Customer MSTR'].[COMMENT2]) as 'Comment2 from Customer Master', rtrim(['Sales User-Defined Work History'].[COMMENT_1]) as 'Comment 1', rtrim(['Sales User-Defined Work History'].[COMMENT_2]) as 'Comment 2', rtrim(['Sales User-Defined Work History'].[COMMENT_3]) as 'Comment 3', rtrim(['Sales User-Defined Work History'].[COMMENT_4]) as 'Comment 4', rtrim(['Sales Transaction History'].[COMMNTID]) as 'Comment ID', ['Sales Transaction History'].[COMMAMNT] as 'Commission Amount', 'Commission Applied To' = dbo.DYN_FUNC_Commission_Applied_To(['Sales Transaction History'].[COMAPPTO]), ['Sales Transaction History'].[CMMSLAMT] as 'Commission Sale Amount', rtrim(['Sales Transaction History'].[CNTCPRSN]) as 'Contact Person', rtrim(['RM Customer MSTR'].[CNTCPRSN]) as 'Contact Person from Customer Master', rtrim(['RM Customer MSTR'].[CPRCSTNM]) as 'Corporate Customer Number', 'Correction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[CORRCTN]), NULL  as 'Correction to Nonexisting Transaction', rtrim(['Sales Transaction History'].[COUNTRY]) as 'Country', rtrim(['RM Customer MSTR'].[COUNTRY]) as 'Country from Customer Master', ['Sales Transaction History'].[CREATDDT] as 'Created Date', ['RM Customer MSTR'].[CREATDDT] as 'Created Date from Customer Master', ['RM Customer MSTR'].[CCRDXPDT] as 'Credit Card Exp Date', rtrim(['RM Customer MSTR'].[CRCARDID]) as 'Credit Card ID', ['RM Customer MSTR'].[CRLMTAMT] as 'Credit Limit Amount', ['RM Customer MSTR'].[CRLMTPER] as 'Credit Limit Period', ['RM Customer MSTR'].[CRLMTPAM] as 'Credit Limit Period Amount', 'Credit Limit Type' = dbo.DYN_FUNC_Credit_Limit_Type(['RM Customer MSTR'].[CRLMTTYP]), rtrim(['Sales Transaction History'].[CURNCYID]) as 'Currency ID', rtrim(['RM Customer MSTR'].[CURNCYID]) as 'Currency ID from Customer Master',  ['Sales Transaction History'].[CURRNIDX] as 'Currency Index', ['Customer Master Summary'].[CUSTBLNC] as 'Customer Balance', rtrim(['RM Customer MSTR'].[CUSTCLAS]) as 'Customer Class', ['RM Customer MSTR'].[CUSTDISC]/100.00 as 'Customer Discount', rtrim(['RM Customer MSTR'].[CUSTNAME]) as 'Customer Name from Customer Master', ['Sales Transaction History'].[DTLSTREP] as 'Date Last Repeated', ['Sales Transaction History'].[DYSTINCR] as 'Days to Increment', 'Default Cash Account Type' = dbo.DYN_FUNC_Default_Cash_Account_Type(['RM Customer MSTR'].[DEFCACTY]), ['Sales Transaction History'].[DENXRATE] as 'Denomination Exchange Rate', ['Sales Transaction History'].[DEPRECVD] as 'Deposit Received', ['Customer Master Summary'].[DEPRECV] as 'Deposits Received', rtrim(['Sales Transaction History'].[DSTBTCH1]) as 'Dest Batch 1', rtrim(['Sales Transaction History'].[DSTBTCH2]) as 'Dest Batch 2', ['Sales Transaction History'].[DISAVAMT] as 'Discount Available Amount', ['Sales Transaction History'].[DISCFRGT] as 'Discount Available Freight', ['Sales Transaction History'].[DISCMISC] as 'Discount Available Misc', ['Sales Transaction History'].[DISAVTKN] as 'Discount Available Taken', ['Sales Transaction History'].[DISCDATE] as 'Discount Date', ['Sales Transaction History'].[DSCDLRAM] as 'Discount Dollar Amount', ['RM Customer MSTR'].[DISGRPER] as 'Discount Grace Period', ['Sales Transaction History'].[DSCPCTAM]/100.00 as 'Discount Percent Amount', ['Sales Transaction History'].[DISCRTND] as 'Discount Returned', ['Sales Transaction History'].[DISTKNAM] as 'Discount Taken Amount', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMAVACC]) as 'Discounts Available Account Number', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMTAKACC]) as 'Discounts Taken Account Number', rtrim(['RM Customer MSTR'].[DOCFMTID]) as 'Document Format ID', rtrim(['Sales Transaction History'].[DOCID]) as 'Document ID', rtrim(['Sales Transaction History'].[DOCNCORR]) as 'Document Number Corrected', 'Document Status' = dbo.DYN_FUNC_Document_Status_Sls_Trx(2), ['Sales Transaction History'].[DUEDATE] as 'Due Date', ['RM Customer MSTR'].[DUEGRPER] as 'Due Date Grace Period', 'EC Transaction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[ECTRX]), 'Exceptional Demand from Sales Transaction' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[EXCEPTIONALDEMAND]), ['Sales Transaction History'].[EXCHDATE] as 'Exchange Date', ['Sales Transaction History'].[XCHGRATE] as 'Exchange Rate', rtrim(['Sales Transaction History'].[EXGTBLID]) as 'Exchange Table ID', ['Sales Transaction History'].[EXTDCOST] as 'Extended Cost', rtrim(['Sales Transaction History'].[FAXNUMBR]) as 'Fax Number', rtrim(['RM Customer MSTR'].[FAX]) as 'Fax from Customer Master', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMFCGACC]) as 'Finance Charge Account Number', 'Finance Charge Amt Type' = dbo.DYN_FUNC_Finance_Charge_Amt_Type(['RM Customer MSTR'].[FNCHATYP]), ['RM Customer MSTR'].[FINCHDLR] as 'Finance Charge Dollar', rtrim(['RM Customer MSTR'].[FINCHID]) as 'Finance Charge ID', ['RM Customer MSTR'].[FNCHPCNT]/100.00 as 'Finance Charge Percent', ['Customer Master Summary'].[FNCHCYTD] as 'Finance Charges CYTD', ['Customer Master Summary'].[FNCHLYRC] as 'Finance Charges LYR Calendar', ['RM Customer MSTR'].[FRSTINDT] as 'First Invoice Date', ['Sales Transaction History'].[FRTAMNT] as 'Freight Amount', rtrim(['Sales Transaction History'].[FRTSCHID]) as 'Freight Schedule ID', ['Sales Transaction History'].[FRTTXAMT] as 'Freight Tax Amount', 'Freight Taxable' = dbo.DYN_FUNC_Freight_Taxable(['Sales Transaction History'].[FRGTTXBL]), ['Sales Transaction History'].[FUFILDAT] as 'Fulfillment Date', ['Sales Transaction History'].[GLPOSTDT] as 'GL Posting Date', rtrim(['RM Customer MSTR'].[GOVCRPID]) as 'Governmental Corporate ID', rtrim(['RM Customer MSTR'].[GOVINDID]) as 'Governmental Individual ID', ['Customer Master Summary'].[HIBALLTD] as 'High Balance LTD', ['Customer Master Summary'].[HIBALLYR] as 'High Balance LYR', ['Customer Master Summary'].[HIBALYTD] as 'High Balance YTD', 'Hold' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[HOLD]), 'Inactive' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[INACTIVE]), (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMIVACC]) as 'Inventory Account Number', ['Sales Transaction History'].[INVODATE] as 'Invoice Date', 'Keep Calendar History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPCALHST]), 'Keep Distribution History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPDSTHST]), 'Keep Period History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPERHIST]), 'Keep Trx History' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[KPTRXHST]), ['Customer Master Summary'].[LASTAGED] as 'Last Aged', ['Customer Master Summary'].[LSTFCHAM] as 'Last Finance Charge Amount', ['Customer Master Summary'].[LSTNSFCD] as 'Last NSF Check Date', ['Customer Master Summary'].[LPYMTAMT] as 'Last Payment Amount', ['Customer Master Summary'].[LASTPYDT] as 'Last Payment Date', ['Customer Master Summary'].[LSTSTAMT] as 'Last Statement Amount', ['Customer Master Summary'].[LASTSTDT] as 'Last Statement Date', ['Customer Master Summary'].[LSTTRXAM] as 'Last Transaction Amount', ['Customer Master Summary'].[LSTTRXDT] as 'Last Transaction Date',  rtrim(['Sales Transaction History'].[LOCNCODE]) as 'Location Code', 'MC Transaction State' = dbo.DYN_FUNC_MC_Transaction_State(['Sales Transaction History'].[MCTRXSTT]), ['Sales Transaction History'].[MRKDNAMT] as 'Markdown Amount', ['Sales Transaction History'].[MSTRNUMB] as 'Master Number', ['RM Customer MSTR'].[MXWROFAM] as 'Max Writeoff Amount', 'Maximum Writeoff Type' = dbo.DYN_FUNC_Maximum_Writeoff_Type(['RM Customer MSTR'].[MXWOFTYP]), ['RM Customer MSTR'].[MINPYDLR] as 'Minimum Payment Dollar', ['RM Customer MSTR'].[MINPYPCT]/100.00 as 'Minimum Payment Percent', 'Minimum Payment Type' = dbo.DYN_FUNC_Minimum_Payment_Type(['RM Customer MSTR'].[MINPYTYP]), ['Sales Transaction History'].[MISCAMNT] as 'Misc Amount', rtrim(['Sales Transaction History'].[MSCSCHID]) as 'Misc Schedule ID', ['Sales Transaction History'].[MSCTXAMT] as 'Misc Tax Amount', 'Misc Taxable' = dbo.DYN_FUNC_Misc_Taxable(['Sales Transaction History'].[MISCTXBL]), ['Sales Transaction History'].[MODIFDT] as 'Modified Date', ['RM Customer MSTR'].[MODIFDT] as 'Modified Date from Customer Master', ['Sales Transaction History'].[NCOMAMNT] as 'Non-Commissioned Amount', ['Customer Master Summary'].[NCSCHPMT] as 'Non Current Scheduled Payments', ['Sales Transaction History'].[NOTEINDX] as 'Note Index', ['RM Customer MSTR'].[NOTEINDX] as 'Note Index from Customer Master', ['Customer Master Summary'].[NUMADTPR] as 'Number ADTP Documents - LYR', ['Customer Master Summary'].[NUMADTPL] as 'Number ADTP Documents - Life', ['Customer Master Summary'].[NUMADTPY] as 'Number ADTP Documents - Year', ['Customer Master Summary'].[NONSFLIF] as 'Number Of NSF Checks Life', ['Customer Master Summary'].[NONSFYTD] as 'Number Of NSF Checks YTD', ['Customer Master Summary'].[ONORDAMT] as 'On Order Amount', ['Sales Transaction History'].[ORDRDATE] as 'Order Date', 'Order Fulfillment Shortage Default' = dbo.DYN_FUNC_Order_Fulfillment_Shortage_Default(['RM Customer MSTR'].[ORDERFULFILLDEFAULT]), rtrim(['Sales Transaction History'].[ORIGNUMB]) as 'Original Number', 'Original Type' = dbo.DYN_FUNC_Original_Type(['Sales Transaction History'].[ORIGTYPE]), ['Sales Transaction History'].[ORACTAMT] as 'Originating Account Amount', ['Sales Transaction History'].[ORBKTFRT] as 'Originating Backout Freight Amount', ['Sales Transaction History'].[ORBKTMSC] as 'Originating Backout Misc Amount', ['Sales Transaction History'].[OBTAXAMT] as 'Originating Backout Tax Amount', ['Sales Transaction History'].[ORCODAMT] as 'Originating COD Amount', ['Sales Transaction History'].[OCOMMAMT] as 'Originating Commission Amount', ['Sales Transaction History'].[ORCOSAMT] as 'Originating Commission Sales Amount', ['Sales Transaction History'].[ORDEPRVD] as 'Originating Deposit Received', ['Sales Transaction History'].[ORDAVAMT] as 'Originating Discount Available Amount', ['Sales Transaction History'].[ORDAVFRT] as 'Originating Discount Available Freight', ['Sales Transaction History'].[ORDAVMSC] as 'Originating Discount Available Misc', ['Sales Transaction History'].[ORDATKN] as 'Originating Discount Available Taken', ['Sales Transaction History'].[ORDDLRAT] as 'Originating Discount Dollar Amount', ['Sales Transaction History'].[ORDISRTD] as 'Originating Discount Returned', ['Sales Transaction History'].[ORDISTKN] as 'Originating Discount Taken Amount', ['Sales Transaction History'].[ORDOCAMT] as 'Originating Document Amount', ['Sales Transaction History'].[OREXTCST] as 'Originating Extended Cost', ['Sales Transaction History'].[ORFRTAMT] as 'Originating Freight Amount', ['Sales Transaction History'].[ORFRTTAX] as 'Originating Freight Tax Amount', ['Sales Transaction History'].[ORMRKDAM] as 'Originating Markdown Amount', ['Sales Transaction History'].[ORMISCAMT] as 'Originating Misc Amount', ['Sales Transaction History'].[ORMSCTAX] as 'Originating Misc Tax Amount', ['Sales Transaction History'].[ORNCMAMT] as 'Originating Non-Commissioned Amount', ['Sales Transaction History'].[ORPMTRVD] as 'Originating Payment Received', ['Sales Transaction History'].[OREMSUBT] as 'Originating Remaining Subtotal', ['Sales Transaction History'].[ORSUBTOT] as 'Originating Subtotal', ['Sales Transaction History'].[ORTAXAMT] as 'Originating Tax Amount', ['Sales Transaction History'].[OTAXTAMT] as 'Originating Taxable Tax Amount', ['Sales Transaction History'].[ORTDISAM] as 'Originating Trade Discount Amount', rtrim(['Sales Transaction History'].[PCKSLPNO]) as 'Packing Slip Number', ['Sales Transaction History'].[PYMTRCVD] as 'Payment Received', rtrim(['Sales Transaction History'].[PYMTRMID]) as 'Payment Terms ID', rtrim(['RM Customer MSTR'].[PYMTRMID]) as 'Payment Terms ID from Customer Master', rtrim(['RM Customer MSTR'].[PHONE1]) as 'Phone 1 from Customer Master', rtrim(['RM Customer MSTR'].[PHONE2]) as 'Phone 2 from Customer Master', rtrim(['Sales Transaction History'].[PHONE3]) as 'Phone 3', rtrim(['RM Customer MSTR'].[PHONE3]) as 'Phone 3 from Customer Master', rtrim(['Sales Transaction History'].[PHNUMBR1]) as 'Phone Number 1', rtrim(['Sales Transaction History'].[PHNUMBR2]) as 'Phone Number 2', rtrim(['Sales Transaction History'].[PICTICNU]) as 'Picking Ticket Number', 'Post Results To' = dbo.DYN_FUNC_Post_Results_To_Customer(['RM Customer MSTR'].[Post_Results_To]), ['Sales Transaction History'].[POSTEDDT] as 'Posted Date', rtrim(['Sales Transaction History'].[PTDUSRID]) as 'Posted User ID', 'Posting Status' = dbo.DYN_FUNC_Posting_Status_SOP_Line_Items(['Sales Transaction History'].[PSTGSTUS]), rtrim(['Sales Transaction History'].[PRCLEVEL]) as 'PriceLevel', rtrim(['RM Customer MSTR'].[PRCLEVEL]) as 'PriceLevel from Customer Master', rtrim(['Sales Transaction History'].[PRBTADCD]) as 'Primary Billto Address Code',  rtrim(['RM Customer MSTR'].[PRBTADCD]) as 'Primary Billto Address Code from Customer Master', rtrim(['RM Customer MSTR'].[PRSTADCD]) as 'Primary Shipto Address Code from Customer Master', 'Priority from Customer Master' = dbo.DYN_FUNC_Priority(['RM Customer MSTR'].[CUSTPRIORITY]), 'Prospect' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[PROSPECT]), ['Sales Transaction History'].[QUOTEDAT] as 'Quote Date', ['Sales Transaction History'].[QUOEXPDA] as 'Quote Expiration Date', 'Rate Calculation Method' = dbo.DYN_FUNC_Rate_Calculation_Method(['Sales Transaction History'].[RTCLCMTD]), rtrim(['Sales Transaction History'].[RATETPID]) as 'Rate Type ID', rtrim(['RM Customer MSTR'].[RATETPID]) as 'Rate Type ID from Customer Master', rtrim(['Sales Transaction History'].[REFRENCE]) as 'Reference', ['Sales Transaction History'].[REMSUBTO] as 'Remaining Subtotal', 'Repeating' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[REPTING]), ['Sales Transaction History'].[ReqShipDate] as 'Requested Ship Date', ['Customer Master Summary'].[RETAINAG] as 'Retainage', ['Sales Transaction History'].[RETUDATE] as 'Return Date', 'Revalue Customer' = dbo.DYN_FUNC_Boolean_All(['RM Customer MSTR'].[Revalue_Customer]), ['Sales Transaction History'].[SALEDATE] as 'Sale Date', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMSLSACC]) as 'Sales Account Number', rtrim(['Sales Transaction History'].[SALSTERR]) as 'Sales Territory', rtrim(['RM Customer MSTR'].[SALSTERR]) as 'Sales Territory from Customer Master', rtrim(['Sales Transaction History'].[SLPRSNID]) as 'Salesperson ID', rtrim(['RM Customer MSTR'].[SLPRSNID]) as 'Salesperson ID from Customer Master', ['Sales Transaction History'].[SEQNCORR] as 'Sequence Number Corrected', rtrim(['Sales Transaction History'].[ShipToName]) as 'ShipToName', 'Ship Complete Document' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[SHIPCOMPLETE]), 'Ship Complete Document from Customer Master' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[SHIPCOMPLETE]), 'Shipping Document' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[SHPPGDOC]), rtrim(['Sales Transaction History'].[SHIPMTHD]) as 'Shipping Method', rtrim(['RM Customer MSTR'].[SHIPMTHD]) as 'Shipping Method from Customer Master', rtrim(['RM Customer MSTR'].[SHRTNAME]) as 'Short Name', 'Simplified' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[SIMPLIFD]), rtrim(['Sales Transaction History'].[STATE]) as 'State', rtrim(['RM Customer MSTR'].[STATE]) as 'State from Customer Master', rtrim(['RM Customer MSTR'].[STADDRCD]) as 'Statement Address Code', 'Statement Cycle' = dbo.DYN_FUNC_Statement_Cycle(['RM Customer MSTR'].[STMTCYCL]), rtrim(['RM Customer MSTR'].[STMTNAME]) as 'Statement Name', ['Sales Transaction History'].[SUBTOTAL] as 'Subtotal', 'TRX Frequency' = dbo.DYN_FUNC_TRX_Frequency(['Sales Transaction History'].[TRXFREQU]), rtrim(['Sales Transaction History'].[TRXSORCE]) as 'TRX Source', ['Sales Transaction History'].[TAXAMNT] as 'Tax Amount', ['Sales Transaction History'].[Tax_Date] as 'Tax Date', 'Tax Engine Called' = dbo.DYN_FUNC_Boolean_All(['Sales Transaction History'].[TXENGCLD]), rtrim(['Sales Transaction History'].[TAXEXMT1]) as 'Tax Exempt 1', rtrim(['RM Customer MSTR'].[TAXEXMT1]) as 'Tax Exempt 1 from Customer Master', rtrim(['Sales Transaction History'].[TAXEXMT2]) as 'Tax Exempt 2', rtrim(['RM Customer MSTR'].[TAXEXMT2]) as 'Tax Exempt 2 from Customer Master', rtrim(['Sales Transaction History'].[TXRGNNUM]) as 'Tax Registration Number', rtrim(['RM Customer MSTR'].[TXRGNNUM]) as 'Tax Registration Number from Customer Master', rtrim(['Sales Transaction History'].[TAXSCHID]) as 'Tax Schedule ID', rtrim(['RM Customer MSTR'].[TAXSCHID]) as 'Tax Schedule ID from Customer Master', 'Tax Schedule Source' = dbo.DYN_FUNC_Tax_Schedule_Source(['Sales Transaction History'].[TXSCHSRC]), ['Sales Transaction History'].[TXBTXAMT] as 'Taxable Tax Amount', ['Sales Transaction History'].[TIME1] as 'Time', ['Sales Transaction History'].[TIMESPRT]/100.00 as 'Times Printed', ['Sales Transaction History'].[TIMEREPD] as 'Times Repeated', ['Sales Transaction History'].[TIMETREP] as 'Times To Repeat', ['Customer Master Summary'].[TTLFCLTD] as 'Total # FC LTD', ['Customer Master Summary'].[TTLFCLYR] as 'Total # FC LYR', ['Customer Master Summary'].[TTLFCYTD] as 'Total # FC YTD', ['Customer Master Summary'].[TTLINLTD] as 'Total # Invoices LTD', ['Customer Master Summary'].[TTLINLYR] as 'Total # Invoices LYR', ['Customer Master Summary'].[TTLINYTD] as 'Total # Invoices YTD', ['Customer Master Summary'].[TNSFCLIF] as 'Total Amount Of NSF Checks Life', ['Customer Master Summary'].[TNSFCYTD] as 'Total Amount Of NSF Checks YTD', ['Customer Master Summary'].[TBDDTLYR] as 'Total Bad Deb LYR', ['Customer Master Summary'].[TBDDTLTD] as 'Total Bad Debt LTD', ['Customer Master Summary'].[TBDDTYTD] as 'Total Bad Debt YTD', ['Customer Master Summary'].[TCSHRLTD] as 'Total Cash Received LTD', ['Customer Master Summary'].[TCSHRLYR] as 'Total Cash Received LYR', ['Customer Master Summary'].[TCSHRYTD] as 'Total Cash Received YTD', ['Customer Master Summary'].[TCOSTLTD] as 'Total Costs LTD', ['Customer Master Summary'].[TCOSTLYR] as 'Total Costs LYR', ['Customer Master Summary'].[TCOSTYTD] as 'Total Costs YTD', ['Customer Master Summary'].[TDISAYTD] as 'Total Discounts Available YTD', ['Customer Master Summary'].[TDTKNLTD] as 'Total Discounts Taken LTD', ['Customer Master Summary'].[TDTKNLYR] as 'Total Discounts Taken LYR', ['Customer Master Summary'].[TDTKNYTD] as 'Total Discounts Taken YTD',  ['Customer Master Summary'].[TFNCHLTD] as 'Total Finance Charges LTD', ['Customer Master Summary'].[TFNCHLYR] as 'Total Finance Charges LYR', ['Customer Master Summary'].[TFNCHYTD] as 'Total Finance Charges YTD', ['Customer Master Summary'].[TTLRTLTD] as 'Total Returns LTD', ['Customer Master Summary'].[TTLRTLYR] as 'Total Returns LYR', ['Customer Master Summary'].[TTLRTYTD] as 'Total Returns YTD', ['Customer Master Summary'].[TTLSLLTD] as 'Total Sales LTD', ['Customer Master Summary'].[TTLSLLYR] as 'Total Sales LYR', ['Customer Master Summary'].[TTLSLYTD] as 'Total Sales YTD', ['Customer Master Summary'].[TWVFCLTD] as 'Total Waived FC LTD', ['Customer Master Summary'].[TWVFCLYR] as 'Total Waived FC LYR', ['Customer Master Summary'].[TWVFCYTD] as 'Total Waived FC YTD', ['Customer Master Summary'].[TWROFLTD] as 'Total Writeoffs LTD', ['Customer Master Summary'].[TWROFLYR] as 'Total Writeoffs LYR', ['Customer Master Summary'].[TWROFYTD] as 'Total Writeoffs YTD', ['Sales Transaction History'].[TRDISAMT] as 'Trade Discount Amount', ['Sales Transaction History'].[TRDISPCT] as 'Trade Discount Percent', rtrim(['Sales Transaction History'].[UPSZONE]) as 'UPS Zone', rtrim(['RM Customer MSTR'].[UPSZONE]) as 'UPS Zone from Customer Master', ['Customer Master Summary'].[UPFCHYTD] as 'Unpaid Finance Charges YTD', ['Customer Master Summary'].[UNPSTDCA] as 'Unposted Cash Amount', ['Customer Master Summary'].[UNPSTOCA] as 'Unposted Other Cash Amount', ['Customer Master Summary'].[UNPSTOSA] as 'Unposted Other Sales Amount', ['Customer Master Summary'].[UNPSTDSA] as 'Unposted Sales Amount', rtrim(['Sales Transaction History'].[USDOCID1]) as 'Use Document ID 1', rtrim(['Sales Transaction History'].[USDOCID2]) as 'Use Document ID 2', rtrim(['Sales User-Defined Work History'].[USERDEF1]) as 'User Defined 1', rtrim(['RM Customer MSTR'].[USERDEF1]) as 'User Defined 1 from Customer Master', rtrim(['Sales User-Defined Work History'].[USERDEF2]) as 'User Defined 2', rtrim(['RM Customer MSTR'].[USERDEF2]) as 'User Defined 2 from Customer Master', rtrim(['Sales User-Defined Work History'].[USRDEF03]) as 'User Defined 3', rtrim(['Sales User-Defined Work History'].[USRDEF04]) as 'User Defined 4', rtrim(['Sales User-Defined Work History'].[USRDEF05]) as 'User Defined 5', ['Sales User-Defined Work History'].[USRDAT01] as 'User Defined Date 1', ['Sales User-Defined Work History'].[USRDAT02] as 'User Defined Date 2', rtrim(['Sales User-Defined Work History'].[USRTAB01]) as 'User Defined Table 1', rtrim(['Sales User-Defined Work History'].[USRTAB09]) as 'User Defined Table 2', rtrim(['Sales User-Defined Work History'].[USRTAB03]) as 'User Defined Table 3', rtrim(['Sales Transaction History'].[USER2ENT]) as 'User To Enter', 'Void Status' = dbo.DYN_FUNC_Void_Status(['Sales Transaction History'].[VOIDSTTS]), ['Sales Transaction History'].[WITHHAMT] as 'Withholding Amount', ['Customer Master Summary'].[WROFSLIF] as 'Write Offs LIFE', ['Customer Master Summary'].[WROFSLYR] as 'Write Offs LYR', ['Customer Master Summary'].[WROFSYTD] as 'Write Offs YTD', (select rtrim([ACTNUMST]) from [GL00105] as ['Account Index Master'] where ['Account Index Master'].[ACTINDX] = ['RM Customer MSTR'].[RMWRACC]) as 'Writeoff Account Number', rtrim(['Sales Transaction History'].[ZIPCODE]) as 'Zip Code', rtrim(['RM Customer MSTR'].[ZIP]) as 'Zip from Customer Master', 'Workflow Approval Status Credit Limit' = dbo.DYN_FUNC_Workflow_Approval_Status(['Sales Transaction History'].[WorkflowApprStatCreditLm]), 'Workflow Priority Credit Limit' = dbo.DYN_FUNC_Workflow_Priority(['Sales Transaction History'].[WorkflowPriorityCreditLm]), 'Workflow Approval Status Quote' = dbo.DYN_FUNC_Workflow_Approval_Status(['Sales Transaction History'].[WorkflowApprStatusQuote]), 'Workflow Priority Quote' = dbo.DYN_FUNC_Workflow_Priority(['Sales Transaction History'].[WorkflowPriorityQuote]),  'Accounts Receivable Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMARACC] ), 'Cash Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMCSHACC] ), 'Checkbook ID For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppCheckbookID(1,['RM Customer MSTR'].[CHEKBKID] ), 'COGS Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMCOSACC] ),  'Customer Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppCustomerID(1,['Sales Transaction History'].[CUSTNMBR] ), 'Discounts Available Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMAVACC] ), 'Discounts Taken Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMTAKACC] ), 'Finance Charge Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMFCGACC] ),  'Inventory Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMIVACC] ), 'Sales Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMSLSACC] ), 'Salesperson ID For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppSalespersonID(1,['Sales Transaction History'].[SLPRSNID],2 ), 'SOP Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppSalesOrder(1,3,11,'',0,0,['Sales Transaction History'].[CUSTNMBR],'',0,'',['Sales Transaction History'].[SOPTYPE],['Sales Transaction History'].[SOPNUMBE] ),  'Writeoff Account Number For Drillback' = 'dgpp://DGPB/?Db=&Srv=CHISQP01&Cmp=IC&Prod=0' +dbo.dgppAccountIndex(1,['RM Customer MSTR'].[RMWRACC] )  from [SOP30200] as ['Sales Transaction History'] with (NOLOCK) left outer join [RM00101] as ['RM Customer MSTR'] with (NOLOCK) on ['Sales Transaction History'].[CUSTNMBR] = ['RM Customer MSTR'].[CUSTNMBR] left outer join [RM00103] as ['Customer Master Summary'] with (NOLOCK) on ['Sales Transaction History'].[CUSTNMBR] = ['Customer Master Summary'].[CUSTNMBR] left outer join [SOP10106] as ['Sales User-Defined Work History'] with (NOLOCK) on ['Sales Transaction History'].[SOPTYPE] = ['Sales User-Defined Work History'].[SOPTYPE]  and ['Sales Transaction History'].[SOPNUMBE] = ['Sales User-Defined Work History'].[SOPNUMBE] 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_1099_Box_Type]'
GO


create function [dbo].[DYN_FUNC_1099_Box_Type] (@i1099Type integer, @i1099BoxNumber integer) returns varchar(100) as  begin  declare @oBoxtextanddescription varchar(100), @Boxtext varchar(5), @Boxdescription varchar(40), @Offset integer if @i1099Type > 1 begin  select @Boxdescription = rtrim(TEN99BOXDSCRPTN),    @Boxtext = rtrim(TEN99BOXTEXT)  from PM40104  where TEN99TYPE = @i1099Type - 1 and TEN99BOXNUMBER = @i1099BoxNumber  select @oBoxtextanddescription = @Boxtext + ' '  if @Boxdescription <> ''  select @oBoxtextanddescription = @oBoxtextanddescription + @Boxdescription  else   begin    select @Offset =    case      when @i1099Type = 2 then 8500     when @i1099Type = 3 then 8530     when @i1099Type = 4 then 8560    end    select @Boxdescription =     rtrim(SQL_MSG)      from        DYNAMICS.dbo.MESSAGES      WITH (NOLOCK)      where         MSGNUM = @Offset + @i1099BoxNumber      and         Language_ID =          0   select @oBoxtextanddescription = @oBoxtextanddescription + @Boxdescription   end end else  select @oBoxtextanddescription = '' RETURN(@oBoxtextanddescription) END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Aging_Bucket]'
GO


create function [dbo].[DYN_FUNC_Aging_Bucket] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) declare @PeriodDesc varchar(100) select @PeriodDesc = (select rtrim(RMPERDSC) from RM40201 where INDEX1 = @iIntEnum) set @oVarcharValuestring = isnull(@PeriodDesc,'') RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Prefix_RM_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Prefix_RM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = (select DOCABREV from RM40401 where RMDTYPAL = @iIntEnum)  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Type_PM_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Type_PM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = (select DOCTYNAM from PM40102 where DOCTYPE = @iIntEnum)  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Type_RM_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Type_RM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = (select DOCDESCR from RM40401 where RMDTYPAL = @iIntEnum)  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_RM_Document_TypeAll]'
GO


create function [dbo].[DYN_FUNC_RM_Document_TypeAll] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = (select DOCDESCR from RM40401 where RMDTYPAL = @iIntEnum)  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_1099_Type]'
GO


create function [dbo].[DYN_FUNC_1099_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Not a 1099 Vendor' when @iIntEnum = 2 then 'Dividend' when @iIntEnum = 3 then 'Interest' when @iIntEnum = 4 then 'Miscellaneous' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_ABC_Code]'
GO


create function [dbo].[DYN_FUNC_ABC_Code] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then '(none)' when @iIntEnum = 2 then 'A' when @iIntEnum = 3 then 'B' when @iIntEnum = 4 then 'C' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Address_Source]'
GO


create function [dbo].[DYN_FUNC_Address_Source] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'None' when @iIntEnum = 1 then 'Ship To' when @iIntEnum = 2 then 'Purchasing' when @iIntEnum = 3 then 'Site' when @iIntEnum = 4 then 'Company' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Attendance_TRX_Status]'
GO


create function [dbo].[DYN_FUNC_Attendance_TRX_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Posted' when @iIntEnum = 2 then 'Processing' when @iIntEnum = 3 then 'Processed' when @iIntEnum = 4 then 'Voided' when @iIntEnum = 5 then 'Accrual' when @iIntEnum = 6 then 'Auto Accrual' else ''  end  RETURN(@oVarcharValuestring)  END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Batch_Frequency]'
GO


create function [dbo].[DYN_FUNC_Batch_Frequency] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Single Use' when @iIntEnum = 2 then 'Weekly' when @iIntEnum = 3 then 'Biweekly' when @iIntEnum = 4 then 'Semimonthly' when @iIntEnum = 5 then 'Monthly' when @iIntEnum = 6 then 'Bimonthly' when @iIntEnum = 7 then 'Quarterly' when @iIntEnum = 8 then 'Miscellaneous' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_CM_Trx_Type]'
GO


create function [dbo].[DYN_FUNC_CM_Trx_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Deposit' when @iIntEnum = 2 then 'Receipt' when @iIntEnum = 3 then 'Check' when @iIntEnum = 4 then 'Withdrawal' when @iIntEnum = 5 then 'Increase Adjustment' when @iIntEnum = 6 then 'Decrease Adjustment' when @iIntEnum = 7 then 'Transfer' when @iIntEnum = 101 then 'Interest Income' when @iIntEnum = 102 then 'Other Income' when @iIntEnum = 103 then 'Other Expense' when @iIntEnum = 104 then 'Service Charge' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Cash_Receipt_Type]'
GO


create function [dbo].[DYN_FUNC_Cash_Receipt_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Check' when @iIntEnum = 1 then 'Cash' when @iIntEnum = 2 then 'Credit Card' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Computer_TRX_Type]'
GO


create function [dbo].[DYN_FUNC_Computer_TRX_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Pay Code' when @iIntEnum = 2 then 'Deduction' when @iIntEnum = 3 then 'Benefit' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Control_Blanket_By]'
GO


create function [dbo].[DYN_FUNC_Control_Blanket_By] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Value' else 'Quantity' end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Control_Type_CM]'
GO


create function [dbo].[DYN_FUNC_Control_Type_CM] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Transaction' when @iIntEnum = 2 then 'Receipt' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Control_Type_PM_Trx]'
GO


create function [dbo].[DYN_FUNC_Control_Type_PM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Voucher' when @iIntEnum = 1 then 'Payment' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Cost_Calculation_Method]'
GO


create function [dbo].[DYN_FUNC_Cost_Calculation_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Percent of Extended Cost' when @iIntEnum = 2 then 'Flat Amount' when @iIntEnum = 3 then 'Flat Amount Per Unit' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Cost_Selection]'
GO


create function [dbo].[DYN_FUNC_Cost_Selection] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Vendor Last Originating Invoice Cost' when @iIntEnum = 2 then 'Item Current Cost' when @iIntEnum = 3 then 'Item Standard Cost' when @iIntEnum = 4 then 'Specified Cost (In Functional Currency)' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Credit_Limit]'
GO


create function [dbo].[DYN_FUNC_Credit_Limit] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Credit' when @iIntEnum = 1 then 'Unlimited' when @iIntEnum = 2 then 'Amount' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Currency_Decimals]'
GO


create function [dbo].[DYN_FUNC_Currency_Decimals] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then '0' when @iIntEnum = 2 then '1' when @iIntEnum = 3 then '2' when @iIntEnum = 4 then '3' when @iIntEnum = 5 then '4' when @iIntEnum = 6 then '5' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_DTA_Series]'
GO


create function [dbo].[DYN_FUNC_DTA_Series] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'All' when @iIntEnum = 2 then 'Financial' when @iIntEnum = 3 then 'Sales' when @iIntEnum = 4 then 'Purchasing' when @iIntEnum = 5 then 'Inventory' when @iIntEnum = 6 then 'Payroll' when @iIntEnum = 7 then 'Project' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Decimal_Places]'
GO


create function [dbo].[DYN_FUNC_Decimal_Places] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then '0' when @iIntEnum = 1 then '1' when @iIntEnum = 2 then '2' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Decimal_Places_Currency]'
GO


create function [dbo].[DYN_FUNC_Decimal_Places_Currency] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then '0' when @iIntEnum = 2 then '1' when @iIntEnum = 3 then '2' when @iIntEnum = 4 then '3' when @iIntEnum = 5 then '4' when @iIntEnum = 6 then '5' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Deposit_Type]'
GO


create function [dbo].[DYN_FUNC_Deposit_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Deposit with Receipts' when @iIntEnum = 2 then 'Deposit without Receipts' when @iIntEnum = 3 then 'Clearing Deposit' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Display_In_Lookups]'
GO


create function [dbo].[DYN_FUNC_Display_In_Lookups] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Sales' when @iIntEnum = 2 then 'Inventory Control' when @iIntEnum = 3 then 'Purchasing' when @iIntEnum = 4 then 'Payroll' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_CM_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Status_CM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Receipt' when @iIntEnum = 2 then 'Transaction' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_GL_Sum]'
GO


create function [dbo].[DYN_FUNC_Document_Status_GL_Sum] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Current' when @iIntEnum = 2 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_IV_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Status_IV_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_PM_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Status_PM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' when @iIntEnum = 3 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_POP_Line_Items]'
GO


create function [dbo].[DYN_FUNC_Document_Status_POP_Line_Items] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Open' when @iIntEnum = 2 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_Purch_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Status_Purch_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Open' when @iIntEnum = 2 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_RM_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Status_RM_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' when @iIntEnum = 3 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Status_SOP_Line_Items]'
GO


create function [dbo].[DYN_FUNC_Document_Status_SOP_Line_Items] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Type_IV_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Type_IV_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Adjustment' when @iIntEnum = 2 then 'Variance' when @iIntEnum = 3 then 'Transfer' when @iIntEnum = 4 then 'Receipt' when @iIntEnum = 5 then 'Return' when @iIntEnum = 6 then 'Sale' when @iIntEnum = 7 then 'Bill of Materials' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Document_Type_Tax_Detail_Trx]'
GO


create function [dbo].[DYN_FUNC_Document_Type_Tax_Detail_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Financial General Entry' when @iIntEnum = 2 then 'Receivables Sales / Invoices' when @iIntEnum = 3 then 'Receivables Scheduled Payments' when @iIntEnum = 4 then 'Receivables Debit Memos' when @iIntEnum = 5 then 'Receivables Finance Charges' when @iIntEnum = 6 then 'Receivables Service / Repairs' when @iIntEnum = 7 then 'Receivables Warranty' when @iIntEnum = 8 then 'Receivables Credit Memo' when @iIntEnum = 9 then 'Receivables Returns' when @iIntEnum = 10 then 'Receivables Payments' when @iIntEnum = 11 then 'Invoicing Invoice' when @iIntEnum = 12 then 'Invoicing Return' when @iIntEnum = 13 then 'Invoicing Packing Slip' when @iIntEnum = 14 then 'Sales Invoice' when @iIntEnum = 15 then 'Sales Return' when @iIntEnum = 16 then 'Payables Invoice' when @iIntEnum = 17 then 'Payables Finance Charge' when @iIntEnum = 18 then 'Payables Misc Charge' when @iIntEnum = 19 then 'Payables Return' when @iIntEnum = 20 then 'Payables Credit Memo' when @iIntEnum = 21 then 'Payables Payment' when @iIntEnum = 22 then 'Payables Schedule' when @iIntEnum = 23 then 'Purchasing Invoice' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_EIC_Filing_Status]'
GO


create function [dbo].[DYN_FUNC_EIC_Filing_Status] (@iStringStatus varchar(10)) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iStringStatus = '' then 'Not Eligible' else   (select ISNULL(STSDESCR,'') from DYNAMICS..UPR41301 where TAXCODE = 'EIC' and TXFLGSTS = @iStringStatus)   end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Ethnic_Origin]'
GO


create function [dbo].[DYN_FUNC_Ethnic_Origin] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'White' when @iIntEnum = 2 then 'American Indian or Alaska Native' when @iIntEnum = 3 then 'Black' when @iIntEnum = 4 then 'Asian or Pacific Islander' when @iIntEnum = 5 then 'Hispanic' when @iIntEnum = 6 then 'Other' when @iIntEnum = 7 then 'N/A' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Federal_Filing_Status]'
GO


create function [dbo].[DYN_FUNC_Federal_Filing_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Exempt' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Forecast_Consumption_Period]'
GO


create function [dbo].[DYN_FUNC_Forecast_Consumption_Period] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Days' when @iIntEnum = 2 then 'Weeks' when @iIntEnum = 3 then 'Months' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Free_On_Board]'
GO


create function [dbo].[DYN_FUNC_Free_On_Board] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'None' when @iIntEnum = 2 then 'Origin' when @iIntEnum = 3 then 'Destination' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Functional_Currency_Decimals]'
GO


create function [dbo].[DYN_FUNC_Functional_Currency_Decimals] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then '0' when @iIntEnum = 2 then '1' when @iIntEnum = 3 then '2' when @iIntEnum = 4 then '3' when @iIntEnum = 5 then '4' when @iIntEnum = 6 then '5' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_GL_Header_Valid]'
GO


create function [dbo].[DYN_FUNC_GL_Header_Valid] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Valid Account Number' when @iIntEnum = 2 then 'Valid Journal Entry' when @iIntEnum = 3 then 'Valid Posting Date' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_GL_Line_Valid]'
GO


create function [dbo].[DYN_FUNC_GL_Line_Valid] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Valid Account Number' when @iIntEnum = 2 then 'Valid Offset Account Number' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Gender]'
GO


create function [dbo].[DYN_FUNC_Gender] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Male' when @iIntEnum = 2 then 'Female' when @iIntEnum = 3 then 'N/A' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_HR_Status]'
GO


create function [dbo].[DYN_FUNC_HR_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Active' when @iIntEnum = 2 then 'Family Leave' when @iIntEnum = 3 then 'Leave of Absence' when @iIntEnum = 4 then 'Maternity' when @iIntEnum = 5 then 'Retired' when @iIntEnum = 6 then 'Separated' when @iIntEnum = 7 then 'Suspended' when @iIntEnum = 8 then 'Terminated' when @iIntEnum = 9 then 'Other' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_History_Year]'
GO


create function [dbo].[DYN_FUNC_History_Year] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then ''  else ltrim(str(@iIntEnum))  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_IV_Item_Taxable]'
GO


create function [dbo].[DYN_FUNC_IV_Item_Taxable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on customers' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Include_In_Catalog]'
GO


create function [dbo].[DYN_FUNC_Include_In_Catalog] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Procurement		' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Item_Tracking_Option]'
GO


create function [dbo].[DYN_FUNC_Item_Tracking_Option] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'None' when @iIntEnum = 2 then 'Serial Numbers' when @iIntEnum = 3 then 'Lot Numbers' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Item_Type]'
GO


create function [dbo].[DYN_FUNC_Item_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Sales Inventory' when @iIntEnum = 2 then 'Discontinued' when @iIntEnum = 3 then 'Kit' when @iIntEnum = 4 then 'Misc Charges' when @iIntEnum = 5 then 'Services' when @iIntEnum = 6 then 'Flat Fee' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Kit_COGS_Account_Source]'
GO


create function [dbo].[DYN_FUNC_Kit_COGS_Account_Source] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'From Component Item' when @iIntEnum = 1 then 'From Kit Item' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Line_Origin]'
GO


create function [dbo].[DYN_FUNC_Line_Origin] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Manual' when @iIntEnum = 2 then 'e. Req.' when @iIntEnum = 3 then 'SOP' when @iIntEnum = 4 then 'MRP' when @iIntEnum = 5 then 'SMS-CL' when @iIntEnum = 6 then 'SMS-RT' when @iIntEnum = 7 then 'SMS-DP' when @iIntEnum = 8 then 'MOP' when @iIntEnum = 9 then 'PO Gen' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_MaritalStatus]'
GO


create function [dbo].[DYN_FUNC_MaritalStatus] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Married' when @iIntEnum = 2 then 'Single' when @iIntEnum = 3 then 'N/A' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Markdown_Type]'
GO


create function [dbo].[DYN_FUNC_Markdown_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Percent' when @iIntEnum = 1 then 'Dollar' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Maximum_Invoice_Amount_For_Vendors]'
GO


create function [dbo].[DYN_FUNC_Maximum_Invoice_Amount_For_Vendors] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Maximum' when @iIntEnum = 1 then 'Amount' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Order_Policy]'
GO


create function [dbo].[DYN_FUNC_Order_Policy] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Not Planned' when @iIntEnum = 2 then 'Lot for Lot' when @iIntEnum = 3 then 'Fixed Order Quantity' when @iIntEnum = 4 then 'Period Order Quantity' when @iIntEnum = 5 then 'Order Point' when @iIntEnum = 6 then 'Manually Planned' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_POP_Order_Method]'
GO


create function [dbo].[DYN_FUNC_POP_Order_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Order To Independent Site' when @iIntEnum = 2 then 'Order To Master Site' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_POP_Tax_Schedule_Source]'
GO


create function [dbo].[DYN_FUNC_POP_Tax_Schedule_Source] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'None' when @iIntEnum = 1 then 'Company' when @iIntEnum = 2 then 'Single-use' when @iIntEnum = 3 then 'Purchase' when @iIntEnum = 4 then 'Ship To' when @iIntEnum = 5 then 'Site' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_POP_Type]'
GO


create function [dbo].[DYN_FUNC_POP_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Shipment' when @iIntEnum = 2 then 'Invoice' when @iIntEnum = 3 then 'Shipment/Invoice' when @iIntEnum = 4 then 'Return' when @iIntEnum = 5 then 'Return w/Credit' when @iIntEnum = 6 then 'IV Return' when @iIntEnum = 7 then 'IV Return w/Credit' when @iIntEnum = 8 then 'In-Transit' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_POP_Vendor_Selection]'
GO


create function [dbo].[DYN_FUNC_POP_Vendor_Selection] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Site Primary Vendor' when @iIntEnum = 2 then 'Vendor With Lowest Cost' when @iIntEnum = 3 then 'Vendor With Shortest Lead Time' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_PO_Line_Status]'
GO


create function [dbo].[DYN_FUNC_PO_Line_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'New' when @iIntEnum = 2 then 'Released' when @iIntEnum = 3 then 'Change Order' when @iIntEnum = 4 then 'Received' when @iIntEnum = 5 then 'Closed' when @iIntEnum = 6 then 'Canceled' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_PO_Line_Status_Orig]'
GO


create function [dbo].[DYN_FUNC_PO_Line_Status_Orig] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'New' when @iIntEnum = 2 then 'Released' when @iIntEnum = 3 then 'Change Order' when @iIntEnum = 4 then 'Received' when @iIntEnum = 5 then 'Closed' when @iIntEnum = 6 then 'Canceled' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_PO_Status]'
GO


create function [dbo].[DYN_FUNC_PO_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'New' when @iIntEnum = 2 then 'Released' when @iIntEnum = 3 then 'Change Order' when @iIntEnum = 4 then 'Received' when @iIntEnum = 5 then 'Closed' when @iIntEnum = 6 then 'Canceled' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_PO_Status_Orig]'
GO


create function [dbo].[DYN_FUNC_PO_Status_Orig] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'New' when @iIntEnum = 2 then 'Released' when @iIntEnum = 3 then 'Change Order' when @iIntEnum = 4 then 'Received' when @iIntEnum = 5 then 'Closed' when @iIntEnum = 6 then 'Canceled' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_PO_Type]'
GO


create function [dbo].[DYN_FUNC_PO_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Standard' when @iIntEnum = 2 then 'Drop-Ship' when @iIntEnum = 3 then 'Blanket' when @iIntEnum = 4 then 'Drop-Ship Blanket' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Payment_Entry_Type]'
GO


create function [dbo].[DYN_FUNC_Payment_Entry_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Check' when @iIntEnum = 1 then 'Cash' when @iIntEnum = 2 then 'Credit Card' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Payroll_Record_Type]'
GO


create function [dbo].[DYN_FUNC_Payroll_Record_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Pay Codes' when @iIntEnum = 2 then 'Deductions' when @iIntEnum = 3 then 'Benefits' when @iIntEnum = 4 then 'State Taxes' when @iIntEnum = 5 then 'Local Taxes' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_Results_To_Vendor]'
GO


create function [dbo].[DYN_FUNC_Post_Results_To_Vendor] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Payables/Discount Acct' when @iIntEnum = 1 then 'Purchasing Offset Acct' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Post_To_Cash_Account_From]'
GO


create function [dbo].[DYN_FUNC_Post_To_Cash_Account_From] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Checkbook' when @iIntEnum = 1 then 'Vendor' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Posting_Status_MDA]'
GO


create function [dbo].[DYN_FUNC_Posting_Status_MDA] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' when @iIntEnum = 3 then 'Posted With Error' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Posting_Status_POP_Receipt_Lines]'
GO


create function [dbo].[DYN_FUNC_Posting_Status_POP_Receipt_Lines] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Posting_Status_POP_Receipts]'
GO


create function [dbo].[DYN_FUNC_Posting_Status_POP_Receipts] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Unposted' when @iIntEnum = 2 then 'Posted' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Price_Method]'
GO


create function [dbo].[DYN_FUNC_Price_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Currency Amount' when @iIntEnum = 2 then '% of List Price' when @iIntEnum = 3 then '% Markup - Current Cost' when @iIntEnum = 4 then '% Markup - Standard Cost' when @iIntEnum = 5 then '% Margin - Current Cost' when @iIntEnum = 6 then '% Margin - Standard Cost' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Purchase_Freight_Taxable]'
GO


create function [dbo].[DYN_FUNC_Purchase_Freight_Taxable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on vendor' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Purchase_IV_Item_Taxable]'
GO


create function [dbo].[DYN_FUNC_Purchase_IV_Item_Taxable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on vendor' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Purchase_Misc_Taxable]'
GO


create function [dbo].[DYN_FUNC_Purchase_Misc_Taxable] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on vendor' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Purchase_Receipt_Type]'
GO


create function [dbo].[DYN_FUNC_Purchase_Receipt_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Adjustment' when @iIntEnum = 2 then 'Variance' when @iIntEnum = 3 then 'Transfer' when @iIntEnum = 4 then 'Override' when @iIntEnum = 5 then 'Receipt' when @iIntEnum = 6 then 'Return' when @iIntEnum = 7 then 'Assembly' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Purchase_Tax_Options]'
GO


create function [dbo].[DYN_FUNC_Purchase_Tax_Options] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on vendor' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Purchasing_Status]'
GO


create function [dbo].[DYN_FUNC_Purchasing_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'None' when @iIntEnum = 2 then 'Needs Purchase' when @iIntEnum = 3 then 'Purchased' when @iIntEnum = 4 then 'Partially Received' when @iIntEnum = 5 then 'Fully Received' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_QTY_Type]'
GO


create function [dbo].[DYN_FUNC_QTY_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'On Hand' when @iIntEnum = 2 then 'Returned' when @iIntEnum = 3 then 'In Use' when @iIntEnum = 4 then 'In Service' when @iIntEnum = 5 then 'Damaged' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Rate_Calc_Method]'
GO


create function [dbo].[DYN_FUNC_Rate_Calc_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Multiply' when @iIntEnum = 1 then 'Divide' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Line_Items]'
GO


create function [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Line_Items] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Multiply' when @iIntEnum = 1 then 'Divide' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Receipts]'
GO


create function [dbo].[DYN_FUNC_Rate_Calc_Method_POP_Receipts] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Multiply' when @iIntEnum = 2 then 'Divide' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Receipt_Type]'
GO


create function [dbo].[DYN_FUNC_Receipt_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Check' when @iIntEnum = 2 then 'Cash' when @iIntEnum = 3 then 'Credit Card' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Receiving_MC_Transaction_State]'
GO


create function [dbo].[DYN_FUNC_Receiving_MC_Transaction_State] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Euro' when @iIntEnum = 1 then 'Non-Denomination to Non-Denomination' when @iIntEnum = 2 then 'Non-Denomination to Euro' when @iIntEnum = 3 then 'Non-Denomination to Denomination' when @iIntEnum = 4 then 'Denomination to Non-Denomination' when @iIntEnum = 5 then 'Denomination to Denomination' when @iIntEnum = 6 then 'Denomination to Euro' when @iIntEnum = 7 then 'Euro to Denomination' when @iIntEnum = 8 then 'Euro to Non-Denomination' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Receiving_Rate_Calc_Method]'
GO


create function [dbo].[DYN_FUNC_Receiving_Rate_Calc_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Multiply' when @iIntEnum = 1 then 'Divide' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Record_Status]'
GO


create function [dbo].[DYN_FUNC_Record_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Begin' when @iIntEnum = 2 then 'Commit' when @iIntEnum = 3 then 'Voiding' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Record_Type]'
GO


create function [dbo].[DYN_FUNC_Record_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Overall' when @iIntEnum = 2 then 'Site' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Replenishment_Level]'
GO


create function [dbo].[DYN_FUNC_Replenishment_Level] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Order Point Quantity' when @iIntEnum = 2 then 'Order-Up-To Level' when @iIntEnum = 3 then 'Vendor EOQ' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Replenishment_Method]'
GO


create function [dbo].[DYN_FUNC_Replenishment_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Make' when @iIntEnum = 2 then 'Buy' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Requisition_Document_Status]'
GO


create function [dbo].[DYN_FUNC_Requisition_Document_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Work' when @iIntEnum = 2 then 'History' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Requisition_Line_Status]'
GO


create function [dbo].[DYN_FUNC_Requisition_Line_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'New' when @iIntEnum = 2 then 'Ordered' when @iIntEnum = 3 then 'Canceled' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Requisition_Status]'
GO


create function [dbo].[DYN_FUNC_Requisition_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Saved' when @iIntEnum = 2 then 'Submitted' when @iIntEnum = 3 then 'Partially Purchased' when @iIntEnum = 4 then 'Purchased' when @iIntEnum = 5 then 'Canceled' when @iIntEnum = 6 then 'Voided' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_SOP_Distribution_Type]'
GO


create function [dbo].[DYN_FUNC_SOP_Distribution_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'SALES' when @iIntEnum = 2 then 'RECV' when @iIntEnum = 3 then 'CASH' when @iIntEnum = 4 then 'TAKEN' when @iIntEnum = 5 then 'AVAIL' when @iIntEnum = 6 then 'TRADE' when @iIntEnum = 7 then 'FREIGHT' when @iIntEnum = 8 then 'MISC' when @iIntEnum = 9 then 'TAXES' when @iIntEnum = 10 then 'MARK' when @iIntEnum = 11 then 'COMMEXP' when @iIntEnum = 12 then 'COMMPAY' when @iIntEnum = 13 then 'OTHER' when @iIntEnum = 14 then 'COGS' when @iIntEnum = 15 then 'INV' when @iIntEnum = 16 then 'RETURNS' when @iIntEnum = 17 then 'IN USE' when @iIntEnum = 18 then 'IN SERVICE' when @iIntEnum = 19 then 'DAMAGED' when @iIntEnum = 20 then 'UNIT' when @iIntEnum = 21 then 'DEPOSITS' when @iIntEnum = 22 then 'ROUND' when @iIntEnum = 23 then 'REBATE' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_SOP_HDR_Flags]'
GO


create function [dbo].[DYN_FUNC_SOP_HDR_Flags] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Freight Transferred' when @iIntEnum = 2 then 'Misc Transferred' when @iIntEnum = 3 then 'Trade Discount Transferred' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Salary_Change]'
GO


create function [dbo].[DYN_FUNC_Salary_Change] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Reallocate Dollars' when @iIntEnum = 2 then 'Reallocate Hours' when @iIntEnum = 3 then 'Reduce Dollars' when @iIntEnum = 4 then 'Reduce Hours' when @iIntEnum = 5 then 'Additional Amount' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Series_Tax_Detail_Trx]'
GO


create function [dbo].[DYN_FUNC_Series_Tax_Detail_Trx] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Financial' when @iIntEnum = 2 then 'Receivables' when @iIntEnum = 3 then 'Invoicing' when @iIntEnum = 4 then 'Sales' when @iIntEnum = 5 then 'Payables' when @iIntEnum = 6 then 'Purchasing' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Sick_Time_Accrual_Method]'
GO


create function [dbo].[DYN_FUNC_Sick_Time_Accrual_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Hours Worked' when @iIntEnum = 1 then 'Set Hours' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Source_Indicator]'
GO


create function [dbo].[DYN_FUNC_Source_Indicator] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then '(none)' when @iIntEnum = 2 then 'Issue' when @iIntEnum = 3 then 'Reverse Issue' when @iIntEnum = 4 then 'Finished Good Post' when @iIntEnum = 5 then 'Reverse Finished Good Post' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Status_Group]'
GO


create function [dbo].[DYN_FUNC_Status_Group] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Active' when @iIntEnum = 2 then 'Closed' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Summary_Type]'
GO


create function [dbo].[DYN_FUNC_Summary_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Calendar' when @iIntEnum = 2 then 'Fiscal' else ''  end  RETURN(@oVarcharValuestring)  END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Tax_Detail_Type]'
GO


create function [dbo].[DYN_FUNC_Tax_Detail_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Sales' when @iIntEnum = 2 then 'Purchases' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Tax_Options]'
GO


create function [dbo].[DYN_FUNC_Tax_Options] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Taxable' when @iIntEnum = 2 then 'Nontaxable' when @iIntEnum = 3 then 'Base on customers' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Tax_Schedule_Source_from_Sales_Line_Item]'
GO


create function [dbo].[DYN_FUNC_Tax_Schedule_Source_from_Sales_Line_Item] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No Error' when @iIntEnum = 1 then 'Using Site Schedule ID' when @iIntEnum = 2 then 'Using Ship To Schedule ID' when @iIntEnum = 3 then 'Using Single Schedule' when @iIntEnum = 4 then 'Schedule ID Empty' when @iIntEnum = 5 then 'Schedule ID Not Found' when @iIntEnum = 6 then 'Shipping Method Not Found' when @iIntEnum = 7 then 'Setup File Missing/Damaged' when @iIntEnum = 8 then 'Site Location Not Found' when @iIntEnum = 9 then 'Address Record Not Found' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Time_Type]'
GO


create function [dbo].[DYN_FUNC_Time_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Hourly' when @iIntEnum = 2 then 'Salary' when @iIntEnum = 3 then 'Overtime' when @iIntEnum = 4 then 'Benefit' when @iIntEnum = 5 then 'Absent' when @iIntEnum = 6 then 'Other' else ''  end  RETURN(@oVarcharValuestring)  END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Tip_Type]'
GO


create function [dbo].[DYN_FUNC_Tip_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Directly' when @iIntEnum = 2 then 'Indirectly' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Transfer_From_QTY_Type]'
GO


create function [dbo].[DYN_FUNC_Transfer_From_QTY_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'On Hand' when @iIntEnum = 2 then 'Returned' when @iIntEnum = 3 then 'In Use' when @iIntEnum = 4 then 'In Service' when @iIntEnum = 5 then 'Damaged' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Transfer_To_QTY_Type]'
GO


create function [dbo].[DYN_FUNC_Transfer_To_QTY_Type] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'On Hand' when @iIntEnum = 2 then 'Returned' when @iIntEnum = 3 then 'In Use' when @iIntEnum = 4 then 'In Service' when @iIntEnum = 5 then 'Damaged' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Type_of_Employment]'
GO


create function [dbo].[DYN_FUNC_Type_of_Employment] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Full Time Regular' when @iIntEnum = 2 then 'Full Time Temp' when @iIntEnum = 3 then 'Part Time Regular' when @iIntEnum = 4 then 'Part Time Temp' when @iIntEnum = 5 then 'Intern' when @iIntEnum = 6 then 'Other' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Vacation_Accrual_Method]'
GO


create function [dbo].[DYN_FUNC_Vacation_Accrual_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Hours Worked' when @iIntEnum = 1 then 'Set Hours' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Valuation_Method]'
GO


create function [dbo].[DYN_FUNC_Valuation_Method] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'FIFO Perpetual' when @iIntEnum = 2 then 'LIFO Perpetual' when @iIntEnum = 3 then 'Average Perpetual' when @iIntEnum = 4 then 'FIFO Periodic' when @iIntEnum = 5 then 'LIFO Periodic' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Vendor_Status]'
GO


create function [dbo].[DYN_FUNC_Vendor_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Active' when @iIntEnum = 2 then 'Inactive' when @iIntEnum = 3 then 'Temporary' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Voided]'
GO


create function [dbo].[DYN_FUNC_Voided] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'No' when @iIntEnum = 1 then 'Yes' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Which_Cash_Account_For_Pay]'
GO


create function [dbo].[DYN_FUNC_Which_Cash_Account_For_Pay] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Checkbook' when @iIntEnum = 1 then 'Employee' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Workflow_Status]'
GO


create function [dbo].[DYN_FUNC_Workflow_Status] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 1 then 'Not Submitted' when @iIntEnum = 3 then 'No Action Needed' when @iIntEnum = 4 then 'Pending User Action' when @iIntEnum = 5 then 'Recalled' when @iIntEnum = 6 then 'Completed' when @iIntEnum = 7 then 'Rejected' when @iIntEnum = 9 then 'Not Activated' else ''  end  RETURN(@oVarcharValuestring)  END 

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DYN_FUNC_Writeoff]'
GO


create function [dbo].[DYN_FUNC_Writeoff] (@iIntEnum integer) returns varchar(100) as  begin  declare @oVarcharValuestring varchar(100) set @oVarcharValuestring = case  when @iIntEnum = 0 then 'Not Allowed' when @iIntEnum = 1 then 'Unlimited' when @iIntEnum = 2 then 'Maximum' else ''  end  RETURN(@oVarcharValuestring)  END  

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppAcctNumberDetail]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppAcctNumberDetail]  (  @action int,  @AccountIndex int,  @Year int,  @DateRange smallint,  @StartDate datetime,  @EndDate datetime,  @DocSrcRange smallint,  @StartDocSrc varchar(10),  @EndDocSrc varchar(10),  @CurrencyIDRange smallint,  @StartCurrencyID varchar(15),  @EndCurrencyID varchar(15)   )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedAcctNumber varchar(255),  @EncodedStartDocSrc varchar(255),  @EncodedEndDocSrc varchar(255),  @EncodedStartCurrencyID varchar(255),  @EncodedEndCurrencyID varchar(255),  @URIstring varchar(360)   select @FunctionName = 'OpenAcctDtl'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedStartDocSrc = DYNAMICS.dbo.encodeUrlString(rtrim(@StartDocSrc))  select @EncodedEndDocSrc = DYNAMICS.dbo.encodeUrlString(rtrim(@EndDocSrc))  select @EncodedStartCurrencyID = DYNAMICS.dbo.encodeUrlString(rtrim(@StartCurrencyID))  select @EncodedEndCurrencyID = DYNAMICS.dbo.encodeUrlString(rtrim(@EndCurrencyID))   select @URIstring = '&Act=' + @ActionType + '&Func=' + @FunctionName +   '&ACTINDX=' + ltrim(str(@AccountIndex)) +   '&Year=' + ltrim(str(@Year)) +   '&DateRange=' + ltrim(str(@DateRange)) + '&StartDate=' +  rtrim(convert(char,@StartDate, 101))  + '&EndDate=' + rtrim(convert(char,@EndDate, 101)) +  '&DocSrcRange=' + ltrim(str(@DocSrcRange)) + '&StartDocSrc=' + @EncodedStartDocSrc + '&EndDocSrc=' + @EncodedEndDocSrc +   '&CurrIDRange=' + ltrim(str(@CurrencyIDRange)) + '&StartCurrID=' + @EncodedStartCurrencyID + '&EndCurrID=' + @EncodedEndCurrencyID  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppAssetIndex]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppAssetIndex]  (  @action int,  @ASSETINDEX int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenAssetIndex'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&ASSETINDEX=' + ltrim(str(@ASSETINDEX)) + '&SORTBY=2' + '&ASSETID=&ASSETIDSUF=0'   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppBudgetTrxSummary]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppBudgetTrxSummary]  (  @action int,  @AccountIndex int,  @BudgetID varchar(15)   )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedBudgetID varchar(255),  @URIstring varchar(255)   select @FunctionName = 'OpenBudTrxSum'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedBudgetID = DYNAMICS.dbo.encodeUrlString(rtrim(@BudgetID))   select @URIstring = '&Act=' + @ActionType + '&Func=' + @FunctionName +   '&ACTINDX=' + ltrim(str(@AccountIndex)) +   '&BudgetID=' + ltrim(@EncodedBudgetID)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppCMTransactionZoom]'
GO


 CREATE FUNCTION [dbo].[dgppCMTransactionZoom]   (   @action INT,   @RecordNum NUMERIC(19,5)   )  RETURNS varchar(2000)  AS  BEGIN   DECLARE @ActionType VARCHAR(15),   @FunctionName VARCHAR(20),   @URIstring VARCHAR(255)    SELECT @FunctionName = 'OpenCMZoom'    IF @action=1   SELECT @ActionType = 'OPEN'   ELSE   SELECT @ActionType = 'OPEN'    SELECT @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&CMRECNUM=' + ltrim(str(abs(@RecordNum)))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppContractNumber]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppContractNumber]  (  @action int,  @CUSTNMBR varchar(15),  @PACONTNUMBER varchar(11)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedCUSTNMBR varchar(255),  @EncodedPACONTNUMBER varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenContractNumber'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedCUSTNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@CUSTNMBR));  select @EncodedPACONTNUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@PACONTNUMBER));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&CUSTNMBR=' + ltrim(@EncodedCUSTNMBR) + '&PACONTNUMBER='+ltrim(@EncodedPACONTNUMBER) + '&ActionID=2'  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppEmployeeID]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppEmployeeID]  (  @action int,  @EmployeeID varchar(15)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedEmployeeID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenEmpID'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedEmployeeID = DYNAMICS.dbo.encodeUrlString(rtrim(@EmployeeID));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&EMPLOYID=' + ltrim(@EncodedEmployeeID)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppEquipmentID]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppEquipmentID]  (  @action int,  @EQUIPID int   )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @COMPID = DB_NAME()   select @FunctionName = 'OpenEquipmentID'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&EQUIPID=' + ltrim(str(@EQUIPID))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppGLBatchEntry]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppGLBatchEntry]  (  @action int,  @BACHNUMB char(15),  @BCHSOURC char(15),  @CREATDDT datetime,  @TIME1 datetime  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenGLBatchEntry'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&BACHNUMB=' + ltrim(@BACHNUMB)   + '&BCHSOURC=' + ltrim(@BCHSOURC)  + '&CREATDDT=' + ltrim(@CREATDDT)  + '&TIME1=' + ltrim(@TIME1)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppItemID]'
GO


 CREATE FUNCTION [dbo].[dgppItemID]  (  @action int,  @ItemNumber varchar(30),  @LocationCode varchar(10)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedItemNumber varchar(255),  @EncodedLocationCode varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenItemNmbr'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedItemNumber = DYNAMICS.dbo.encodeUrlString(rtrim(@ItemNumber))  select @EncodedLocationCode = DYNAMICS.dbo.encodeUrlString(rtrim(@LocationCode))   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&ITEMNMBR=' + ltrim(@EncodedItemNumber) + '&LOCNCODE=' + ltrim(@EncodedLocationCode) + '&CallID=0&FocusWindow=true'  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppItemTransactionNumber]'
GO


 CREATE FUNCTION [dbo].[dgppItemTransactionNumber]  (  @action int,  @ITEMNMBR varchar(30),  @IVDOCTYP int,  @DOCNUMBR varchar(20),  @DCSTATUS int   )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedITEMNMBR varchar(255),  @EncodedDOCNUMBR varchar(255)   select @FunctionName = 'OpenItmTrxNmbr'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedITEMNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@ITEMNMBR))  select @EncodedDOCNUMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR))   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&ITEMNMBR=' + ltrim(@EncodedITEMNMBR)   + '&IVDOCTYP=' + ltrim(str(@IVDOCTYP))   + '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBR)  + '&DCSTATUS=' + ltrim(str(@DCSTATUS))  + '&ShowUnpostedTrxOnly=false'  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppOpenPMTrxNmbr]'
GO


 CREATE FUNCTION [dbo].[dgppOpenPMTrxNmbr]  (  @action int,  @BusObj char(30)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenPMTransaction'   if @action=1  select @ActionType = 'Open_for_Workflow'  else  select @ActionType = 'Open_for_Workflow'   select @URIstring =  '&Act='+@ActionType  + '&Func=' + @FunctionName  + '&sBusObjKey=' + ltrim(@BusObj)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPMBatchEntry]'
GO


 CREATE FUNCTION [dbo].[dgppPMBatchEntry]  (  @action int,  @BACHNUMB char(15),  @BCHSOURC char(15),  @CREATDDT datetime,  @TIME1 datetime  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenPMBatchEntry'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&BACHNUMB=' + ltrim(@BACHNUMB)   + '&BCHSOURC=' + ltrim(@BCHSOURC)  + '&CREATDDT=' + ltrim(@CREATDDT)  + '&TIME1=' + ltrim(@TIME1)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPTEExpenseDocument]'
GO


 CREATE FUNCTION [dbo].[dgppPTEExpenseDocument]  (  @action int,  @PTE_Empl_Expense_Doc_No varchar(100),  @bHistory int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedPTE_Empl_Expense_Doc_No varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenPTEExp'   if @action=1  select @ActionType = 'OpenWindow'  else  select @ActionType = 'OpenWindow'  select @EncodedPTE_Empl_Expense_Doc_No = DYNAMICS.dbo.encodeUrlString(rtrim(@PTE_Empl_Expense_Doc_No));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&sPTEExpNo=' + ltrim(@EncodedPTE_Empl_Expense_Doc_No) + '&bHistory=' + ltrim(str(@bHistory))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPTETimesheet]'
GO


 CREATE FUNCTION [dbo].[dgppPTETimesheet]  (  @action int,  @PTETimesheetNumber varchar(100),  @bHistory int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedPTETimesheetNumber varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenPTETs'   if @action=1  select @ActionType = 'OpenWindow'  else  select @ActionType = 'OpenWindow'  select @EncodedPTETimesheetNumber = DYNAMICS.dbo.encodeUrlString(rtrim(@PTETimesheetNumber));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&sPTETsNo=' + ltrim(@EncodedPTETimesheetNumber) + '&bHistory=' + ltrim(str(@bHistory))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPayablesTransactionNumber]'
GO


 CREATE FUNCTION [dbo].[dgppPayablesTransactionNumber]  (  @action int,  @DOCTYPE int,  @VCHRNMBR varchar(20),  @DCSTATUS int,  @Currency_View int,  @CallerID int,  @TRXSORCE varchar(50),  @Origin varchar(50)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedVCHRNMBR varchar(255),  @EncodedTRXSORCE varchar(255),  @EncodedOrigin varchar(255)   select @FunctionName = 'OpenPMTrxNmbr'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVCHRNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@VCHRNMBR))  select @EncodedTRXSORCE = DYNAMICS.dbo.encodeUrlString(rtrim(@TRXSORCE))  select @EncodedOrigin = DYNAMICS.dbo.encodeUrlString(rtrim(@Origin))  select @URIstring = '&Act=' + @ActionType   + '&Func=' + @FunctionName   + '&DOCTYPE=' + ltrim(str(@DOCTYPE))   + '&VCHRNMBR=' + ltrim(@EncodedVCHRNMBR)  + '&DCSTATUS=' + ltrim(str(@DCSTATUS))  + '&CurncyView=' + ltrim(str(@Currency_View))  + '&CallID=' + ltrim(str(@CallerID))  + '&TRXSORCE=' + ltrim(@EncodedTRXSORCE)  + '&Origin=' + ltrim(@EncodedOrigin)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppProjectNumber]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppProjectNumber]  (  @action int,  @CUSTNMBR varchar(15),  @PACONTNUMBER varchar(11),  @PAPROJNUMBER varchar(15),  @PAprojid varchar(15)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedCUSTNMBR varchar(255),  @EncodedPACONTNUMBER varchar(255),  @EncodedPAPROJNUMBER varchar(255),  @EncodedPAprojid varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenProjectNumber'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedCUSTNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@CUSTNMBR));  select @EncodedPACONTNUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@PACONTNUMBER));  select @EncodedPAPROJNUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@PAPROJNUMBER));  select @EncodedPAprojid = DYNAMICS.dbo.encodeUrlString(rtrim(@PAprojid));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&CUSTNMBR=' + ltrim(@EncodedCUSTNMBR) + '&PACONTNUMBER='+ltrim(@EncodedPACONTNUMBER) + '&ActionID=2'+  '&PAPROJNUMBER='+ ltrim(@EncodedPAPROJNUMBER) + '&EncodedPAprojid=' + ltrim(@EncodedPAprojid)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPurchaseOrderPM]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppPurchaseOrderPM]  (  @action int,  @SOURCE int,  @DCSTATUS int,  @DOCTYPE int,  @VENDORID varchar(15),  @DOCNUMBR varchar(20),  @SCHEDULE_NUMBER varchar(20),  @Status int,  @VCHRNMBR varchar(20),  @CurrencyView int,  @CallerID int,  @GroupBox int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedVENDORID varchar(255),  @EncodedDOCNUMBER varchar(255),  @EncodedSCHEDULE_NUMBER varchar(255)    select @FunctionName = 'OpenPOPM'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVENDORID = DYNAMICS.dbo.encodeUrlString(rtrim(@VENDORID));  select @EncodedDOCNUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR));  select @EncodedSCHEDULE_NUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@SCHEDULE_NUMBER));   select @URIstring = '&Act='+@ActionType+'&Funce=' + @FunctionName +   '&SOURCE=' + ltrim(str(@SOURCE)) +  '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&DOCTYPE=' + ltrim(str(@DOCTYPE)) +   '&VENDORID=' + ltrim(@EncodedVENDORID) +  '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBER) +  '&SCHEDULE_NUMBER=' + ltrim(@EncodedSCHEDULE_NUMBER) +  '&Status=' + ltrim(str(@Status)) +  '&VCHRNMBR=' + ltrim(@EncodedDOCNUMBER) +  '&CurncyView=' + ltrim(str(@CurrencyView)) +  '&CallID=' + ltrim(str(@CallerID)) +  '&GROUPBOX=' + ltrim(str(@GroupBox))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPurchaseOrderPOTRX]'
GO


 CREATE FUNCTION [dbo].[dgppPurchaseOrderPOTRX]  (  @action int,  @SOURCE int,  @DCSTATUS int,  @POSTATUS int,  @VENDORID varchar(15),  @PONUMBER varchar(20),  @ActionID int,  @MenuID int,  @CurrencyView int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedVENDORID varchar(255),  @EncodedPONUMBER varchar(255)    select @FunctionName = 'OpenPOPOTrx'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVENDORID = DYNAMICS.dbo.encodeUrlString(rtrim(@VENDORID));  select @EncodedPONUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@PONUMBER));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&SOURCE=' + ltrim(str(@SOURCE)) +  '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&POSTATUS=' + ltrim(str(@POSTATUS)) +  '&VENDORID=' + ltrim(@EncodedVENDORID) +  '&PONUMBER=' + ltrim(@EncodedPONUMBER) +  '&ActID=' + ltrim(str(@ActionID)) +  '&MenuID=' + ltrim(str(@MenuID)) +  '&CurncyView=' + ltrim(str(@CurrencyView))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPurchaseOrderReceipt]'
GO


 CREATE FUNCTION [dbo].[dgppPurchaseOrderReceipt]  (  @action int,  @SOURCE int,  @DCSTATUS int,  @DOCTYPE int,  @VENDORID varchar(15),  @DOCNUMBR varchar(20),  @POPIVCNO varchar(17),  @MenuID int,  @CurrencyView int,  @POPRCTNM varchar(17),  @PONUMBER varchar(20)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @OriginID int,  @EncodedVENDORID varchar(255),  @EncodedDOCNUMBR varchar(255),  @EncodedPOPIVCNO varchar(255),  @EncodedPOPRCTNM varchar(255),  @EncodedPONUMBER varchar(255)   select @FunctionName = 'OpenPORcpt'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVENDORID = DYNAMICS.dbo.encodeUrlString(rtrim(@VENDORID));  select @EncodedDOCNUMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR));  select @EncodedPOPIVCNO = DYNAMICS.dbo.encodeUrlString(rtrim(@POPIVCNO));  select @EncodedPOPRCTNM = DYNAMICS.dbo.encodeUrlString(rtrim(@POPRCTNM));  select @EncodedPONUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@PONUMBER));   if @DCSTATUS = 1   select @OriginID=4   else  select @OriginID=3    select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&SOURCE=' + ltrim(str(@SOURCE)) +  '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&DOCTYPE=' + ltrim(str(@DOCTYPE)) +   '&VENDORID=' + ltrim(@EncodedVENDORID) +  '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBR) +  '&POPIVCNO=' + ltrim(@EncodedPOPIVCNO) +  '&MenuID=' + ltrim(str(@MenuID)) +  '&OriginID=' + ltrim(str(@OriginID)) +  '&CurncyView=' + ltrim(str(@CurrencyView)) +  '&POPRCTNM=' + ltrim(@EncodedPOPRCTNM) +  '&PONUMBER=' + ltrim(@EncodedPONUMBER)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPurchaseOrderReturn]'
GO


 CREATE FUNCTION [dbo].[dgppPurchaseOrderReturn]  (  @action int,  @SOURCE int,  @DCSTATUS int,  @VENDORID varchar(15),  @DOCTYPE int,  @DOCNUMBR varchar(20),  @POPRCTNM varchar(17),  @MenuID int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @OriginID int,  @EncodedVENDORID varchar(255),  @EncodedDOCNUMBR varchar(255),  @EncodedPOPRCTNM varchar(255)    select @FunctionName = 'OpenPORtrn'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVENDORID = DYNAMICS.dbo.encodeUrlString(rtrim(@VENDORID));  select @EncodedDOCNUMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR));  select @EncodedPOPRCTNM = DYNAMICS.dbo.encodeUrlString(rtrim(@POPRCTNM));   if @DCSTATUS = 1   select @OriginID=4   else  select @OriginID=3    select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&SOURCE=' + ltrim(str(@SOURCE)) +  '&DCSTATUS=' + ltrim(str(@DCSTATUS)) +  '&VENDORID=' + ltrim(@EncodedVENDORID) +  '&DOCTYPE=' + ltrim(str(@DOCTYPE)) +   '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBR) +  '&POPRCTNM=' + ltrim(@EncodedPOPRCTNM) +  '&MenuID=' + ltrim(str(@MenuID)) +  '&OriginID=' + ltrim(str(@OriginID))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPurchaseOrder]'
GO


 CREATE FUNCTION [dbo].[dgppPurchaseOrder]  (  @action int,  @SOURCE int,  @DCSTATUS int,  @DOCTYPE int,  @POSTATUS int,  @VENDORID varchar(15),  @PONUMBER varchar(20),  @POPIVCNO varchar(17),  @POPRCTNM varchar(17),  @DOCNUMBR varchar(20),  @SCHEDULE_NUMBER varchar(20),  @VCHRNMBR varchar(20)   )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedVENDORID varchar(255),  @EncodedPONUMBER varchar(255),  @EncodedDOCTYPE varchar(255)   select @FunctionName = 'OpenPO'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVENDORID = DYNAMICS.dbo.encodeUrlString(rtrim(@VENDORID));  select @EncodedPONUMBER = DYNAMICS.dbo.encodeUrlString(rtrim(@PONUMBER));  select @EncodedDOCTYPE = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCTYPE));   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&POSTATUS=' + ltrim(str(@POSTATUS))   + '&VENDORID=' + ltrim(@EncodedVENDORID)   + '&PONUMBER=' + ltrim(@EncodedPONUMBER)   + '&DOCTYPE=' + ltrim(@EncodedDOCTYPE)   + '&ActID=-1'  + '&MenuID=2'   + '&CurncyView=1'   RETURN(@URIstring)  END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppPurchaseRequisitionNumber]'
GO


 CREATE FUNCTION [dbo].[dgppPurchaseRequisitionNumber]  (  @action int,  @POPRequisitionNumber varchar(18),  @bHistory int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedPOPRequisitionNumber varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenReqInq'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'  select @EncodedPOPRequisitionNumber = DYNAMICS.dbo.encodeUrlString(rtrim(@POPRequisitionNumber));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&sReqID=' + ltrim(@EncodedPOPRequisitionNumber) + '&bHistory=' + ltrim(str(@bHistory))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppRMANumber]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppRMANumber]  (  @action int,  @RETDOCID varchar(15),  @Return_Record_Type int  )   RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedRETDOCID varchar(255)   select @COMPID = DB_NAME()   select @FunctionName = 'OpenRMANumber'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedRETDOCID = DYNAMICS.dbo.encodeUrlString(rtrim(@RETDOCID));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&RETDOCID=' + ltrim(@EncodedRETDOCID) + '&Return_Record_Type=' + ltrim(str(@Return_Record_Type))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppRMBatchEntry]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppRMBatchEntry]  (  @action int,  @BACHNUMB char(15),  @BCHSOURC char(15),  @CREATDDT datetime,  @TIME1 datetime  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenRMBatchEntry'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&BACHNUMB=' + ltrim(@BACHNUMB)   + '&BCHSOURC=' + ltrim(@BCHSOURC)  + '&CREATDDT=' + ltrim(@CREATDDT)  + '&TIME1=' + ltrim(@TIME1)  + '&PREWIN=1'  + '&USEDATEFROM=0'  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppRTVNumber]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppRTVNumber]  (  @action int,  @RTV_Number varchar(15),  @RTV_Type varchar(11)  )   RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedRTV_Number varchar(255),  @EncodedRTV_Type varchar(255)   select @COMPID = DB_NAME()   select @FunctionName = 'OpenRTVNumber'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedRTV_Number = DYNAMICS.dbo.encodeUrlString(rtrim(@RTV_Number));  select @EncodedRTV_Type = DYNAMICS.dbo.encodeUrlString(rtrim(@RTV_Type));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&RTV_Number=' + ltrim(@EncodedRTV_Number) + '&RTV_Type=' + ltrim(@EncodedRTV_Type)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppReceivablesTransactionNumber]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppReceivablesTransactionNumber]  (  @action int,  @RMDTYPAL int,  @CUSTNMBR varchar(15),  @DOCNUMBR varchar(20),  @DCSTATUS int,  @TRXSORCE varchar(100),  @TRXDSCRN varchar(100),  @Currency_View int,  @CallerID int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedCUSTNMBR varchar(255),  @EncodedDOCNUMBR varchar(255),  @EncodedTRXSORCE varchar(255),  @EncodedTRXDSCRN varchar(255)   select @FunctionName = 'OpenRMTrxNmbr'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedCUSTNMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@CUSTNMBR));  select @EncodedDOCNUMBR = DYNAMICS.dbo.encodeUrlString(rtrim(@DOCNUMBR));  select @EncodedTRXSORCE = DYNAMICS.dbo.encodeUrlString(rtrim(@TRXSORCE));  select @EncodedTRXDSCRN = DYNAMICS.dbo.encodeUrlString(rtrim(@TRXDSCRN));   select @URIstring = '&Act='+@ActionType  + '&Func=' + @FunctionName   + '&CUSTNMBR=' + ltrim(@EncodedCUSTNMBR)  + '&DOCNUMBR=' + ltrim(@EncodedDOCNUMBR)   + '&RMDTYPAL=' + ltrim(str(@RMDTYPAL))  + '&DCSTATUS=' + ltrim(str(@DCSTATUS))  + '&TRXSORCE=' + ltrim(@EncodedTRXSORCE)  + '&TRXDSCRN=' + ltrim(@EncodedTRXDSCRN)   + '&CurncyView=' + ltrim(str(@Currency_View))   + '&CallID=' + ltrim(str(@CallerID))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppServiceRecordType]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppServiceRecordType]  (  @action int,  @CALLNBR varchar(11),  @SRVRECTYPE int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedCALLNBR varchar(255)   select @COMPID = DB_NAME()   select @FunctionName = 'OpenServiceRecordType'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedCALLNBR = DYNAMICS.dbo.encodeUrlString(rtrim(@CALLNBR));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&SRVRECTYPE=' + ltrim(str(@SRVRECTYPE)) + '&CALLNBR=' + ltrim(@EncodedCALLNBR)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppSmartListView]'
GO
SET QUOTED_IDENTIFIER ON
GO


 CREATE FUNCTION [dbo].[dgppSmartListView]  (  @action int,  @BusObj char(30)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255)   select @FunctionName = 'OpenSmartListView'   if @action=1  select @ActionType = 'OpenWindow'  else  select @ActionType = 'OpenWindow'   select @URIstring =  '&Act='+@ActionType  + '&Func=' + @FunctionName  + '&sBusObjKey=' + ltrim(@BusObj)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppUPRESSDirectDeposit]'
GO


 CREATE FUNCTION [dbo].[dgppUPRESSDirectDeposit]  (  @action int,  @EmployeeId varchar(100)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedEmployeeID varchar(255),  @EncodedPayScheduleID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenESSDD'   if @action=1  select @ActionType = 'OpenWindow'  else  select @ActionType = 'OpenWindow'  select @EncodedEmployeeID = DYNAMICS.dbo.encodeUrlString(rtrim(@EmployeeId));   select @URIstring =  '&Act='+@ActionType  + '&Func=' + @FunctionName  + '&sEmpID=' + ltrim(@EncodedEmployeeID)  + '&nCallerID=' + ltrim(str(1))  + '&bFrmMain=' + ltrim(str(0))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppUPRESSEmpProfile]'
GO


 CREATE FUNCTION [dbo].[dgppUPRESSEmpProfile]  (  @action int,  @EmployeeId varchar(100),   @CreateDATE datetime,   @CreateTIME datetime  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedEmployeeID varchar(255),  @EncodedPayScheduleID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenESSProf'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'  select @EncodedEmployeeID = DYNAMICS.dbo.encodeUrlString(rtrim(@EmployeeId));   select @URIstring =  '&Act='+@ActionType  + '&Func=' + @FunctionName  + '&sEmpID=' + ltrim(@EncodedEmployeeID)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppUPRESSSkillsTraining]'
GO


 CREATE FUNCTION [dbo].[dgppUPRESSSkillsTraining]  (  @action int,  @EmployeeId varchar(100)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedEmployeeID varchar(255),  @EncodedPayScheduleID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenESSSkills'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'  select @EncodedEmployeeID = DYNAMICS.dbo.encodeUrlString(rtrim(@EmployeeId));   select @URIstring =  '&Act='+@ActionType  + '&Func=' + @FunctionName  + '&sEmpID=' + ltrim(@EncodedEmployeeID)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppUPRESSW4]'
GO


 CREATE FUNCTION [dbo].[dgppUPRESSW4]  (  @action int,  @EmployeeId varchar(100)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedEmployeeID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenESSW4'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'  select @EncodedEmployeeID = DYNAMICS.dbo.encodeUrlString(rtrim(@EmployeeId));   select @URIstring =  '&Act='+@ActionType  + '&Func=' + @FunctionName  + '&sEmpID=' + ltrim(@EncodedEmployeeID)   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppUPRTimecard]'
GO


 CREATE FUNCTION [dbo].[dgppUPRTimecard]  (  @action int,  @EmployeeId varchar(100),  @PayScheduleId varchar(100),  @Year varchar(100),  @PeriodId varchar(100),  @bHistory int  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedEmployeeID varchar(255),  @EncodedPayScheduleID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenUPRTc'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'  select @EncodedEmployeeID = DYNAMICS.dbo.encodeUrlString(rtrim(@EmployeeId));  select @EncodedPayScheduleID = DYNAMICS.dbo.encodeUrlString(rtrim(@PayScheduleId));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&sEmpID=' + ltrim(@EncodedEmployeeID) +  '&sPsID=' + ltrim(@EncodedPayScheduleID) +  '&nYear=' + ltrim(@Year) +  '&nPerID=' + ltrim(@PeriodId) + '&bHistory=' + ltrim(str(@bHistory))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppVendorID]'
GO


 CREATE FUNCTION [dbo].[dgppVendorID]  (  @action int,  @VendorID varchar(15)  )  RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @EncodedVendorID varchar(255),  @URIstring varchar(255)  select @COMPID = DB_NAME()   select @FunctionName = 'OpenVendorID'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedVendorID = DYNAMICS.dbo.encodeUrlString(rtrim(@VendorID));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +  '&VENDORID=' + ltrim(@EncodedVendorID)  RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[dgppWorkOrderNumber]'
GO
SET QUOTED_IDENTIFIER OFF
GO


 CREATE FUNCTION [dbo].[dgppWorkOrderNumber]  (  @action int,  @WORKORDNUM varchar(11),  @WORECTYPE int  )   RETURNS varchar(2000) AS BEGIN   DECLARE @COMPID varchar(5),  @ActionType varchar(15),  @FunctionName varchar(50),  @URIstring varchar(255),  @EncodedWORKORDNUM varchar(255)   select @COMPID = DB_NAME()   select @FunctionName = 'OpenWorkOrderNumber'   if @action=1  select @ActionType = 'OPEN'  else  select @ActionType = 'OPEN'   select @EncodedWORKORDNUM = DYNAMICS.dbo.encodeUrlString(rtrim(@WORKORDNUM));   select @URIstring = '&Act='+@ActionType+'&Func=' + @FunctionName +   '&WORKORDNUM=' + ltrim(@EncodedWORKORDNUM) + '&WORECTYPE=' + ltrim(str(@WORECTYPE))   RETURN(@URIstring) END   

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
