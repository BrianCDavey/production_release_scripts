USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 12:57:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[LiabilityType]'
GO
ALTER TABLE [dbo].[LiabilityType] DROP CONSTRAINT [PK_dbo_LiabilityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[LiabilityType]'
GO
ALTER TABLE [epsilon].[LiabilityType] DROP CONSTRAINT [PK_epsilon_LiabilityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_LiabilityType]'
GO
DROP PROCEDURE [epsilon].[Populate_LiabilityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_LiabilityType]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_LiabilityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[LiabilityType]'
GO
DROP TABLE [dbo].[LiabilityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[LiabilityType]'
GO
DROP TABLE [epsilon].[LiabilityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PointActivity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PointActivity] DROP
COLUMN [LiabilityTypeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ActivityCauseSystem]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActivityCauseSystem] ALTER COLUMN [ActivityCauseSystemName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [epsilon].[ActivityCauseSystem]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[ActivityCauseSystem] ALTER COLUMN [ActivityCauseSystemName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_MemberEmailOptions]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_MemberEmailOptions] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_MemberEmailOptions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.MemberEmailOptions AS tgt
	USING
	(
		SELECT meo.MemberEmailOptionsID ,mp.[MemberProfileID],eo.[EmailOptionsID],CASE WHEN meo.[OptionStatus] = 'Y' THEN 1 WHEN  meo.[OptionStatus] = 'N' THEN 0 ELSE NULL END AS [OptionStatus]
		FROM [Epsilon].[MemberEmailOptions] meo
			INNER JOIN dbo.MemberProfile mp ON mp.Epsilon_ID = meo.[MemberProfileID]
			INNER JOIN Epsilon.EmailOptions beo ON beo.[EmailOptionsID] = meo.[EmailOptionsID]
			INNER JOIN dbo.EmailOptions eo ON eo.[EmailOptionsName] = beo.[EmailOptionsName]
	) AS src ON src.[MemberProfileID] = tgt.[MemberProfileID] AND src.[EmailOptionsID] = tgt.[EmailOptionsID]
	WHEN MATCHED THEN
		UPDATE
			SET [OptionStatus] = src.[OptionStatus],
			Epsilon_ID = src.MemberEmailOptionsID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MemberProfileID],[EmailOptionsID],[OptionStatus],Epsilon_ID)
		VALUES(src.[MemberProfileID],src.[EmailOptionsID],src.[OptionStatus],src.MemberEmailOptionsID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_Member]'
GO









-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of dbo import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Member] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[Epsilon_Populate_Member]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

		-- Loyalty.dbo.Epsilon_Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.dbo.Epsilon_Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.dbo.Epsilon_Populate_LoyaltyNumber 
	---------------------------------------------------------------------------
	
	-- Guests.dbo.Epsilon_Populate_Location -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.dbo.Epsilon_Populate_Location'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.dbo.Epsilon_Populate_Location 
	---------------------------------------------------------------------------

	-- Guests.dbo.Epsilon_Populate_GuestInfo ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.dbo.Epsilon_Populate_GuestInfo'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.dbo.Epsilon_Populate_GuestInfo @QueueID
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_Tier ------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Tier'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Tier 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_Enrollment_Source -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Enrollment_Source'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Enrollment_Source 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_Enrollment_Promotion -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Enrollment_Promotion'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Enrollment_Promotion 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_TravelAgency -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_TravelAgency'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_TravelAgency 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberType -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_MemberType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_MemberType 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberType -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_EmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_EmailOptions 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberProfile ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_MemberProfile'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_MemberProfile @QueueID
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberEmailOptions ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_MemberEmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_MemberEmailOptions 
	---------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [epsilon].[PointActivity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[PointActivity] DROP
COLUMN [LiabilityTypeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_PointActivity]'
GO















-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  
		systemPointActivityID AS [External_SourceKey]
		, [PointActivityID] AS [Internal_SourceKey]
		, CAST(REPLACE(LEFT(NULLIF([ActivityDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityDate],''),2)   AS datetime ) AS [ActivityDate]
		, ac.[ActivityTypeID]
		, pt.[PointTypeID]
		, acs.[ActivityCauseSystemID]
		, [ActivityCauseID]
		, uu.UserUpdatedID AS [ActivityCauseUserID]
		, CAST(NULLIF([ActivityCauseAmount],'') AS decimal(20,5)) AS [ActivityCauseAmount]
		, [ActivityCauseCurrency]
		, CAST(REPLACE(LEFT(NULLIF([ActivityCauseDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityCauseDate],''),2)   AS datetime ) AS [ActivityCauseDate]
		, CAST(REPLACE(LEFT(NULLIF([CurrencyExchangeDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CurrencyExchangeDate],''),2)   AS datetime ) AS [CurrencyExchangeDate]
		, CAST(NULLIF([CurrencyExchangeRate],'') AS decimal(10,9) ) AS [CurrencyExchangeRate]
		, CAST(NULLIF([Points],'') AS decimal(15,5) ) AS [Points]
		, CAST(NULLIF([PointLiabilityUSD],'') AS decimal(10,2) ) AS [PointLiabilityUSD]
		, [Notes]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		,  [QueueID]
		FROM epsilon.PointActivity pa
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = pa.LoyaltyNumberID
		LEFT JOIN dbo.ActivityType ac ON pa.ActivityTypeID  = ac.Epsilon_ID
		LEFT JOIN dbo.PointType pt ON pt.Epsilon_ID  = pa.PointTypeID
		LEFT JOIN dbo.ActivityCauseSystem acs ON acs.Epsilon_ID  = pa.ActivityCauseSystemID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = pa.ActivityCauseUserID
		--WHERE pa.QueueID = @QueueID
	) AS src ON 
	ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	AND tgt.Internal_SourceKey = src.Internal_SourceKey
	AND ISNULL(tgt.[ActivityDate],'1900-01-01')  = ISNULL(src.[ActivityDate],'1900-01-01')
	AND ISNULL(tgt.[ActivityTypeID],'') = ISNULL(src.[ActivityTypeID],'')
	AND ISNULL(tgt.[PointTypeID],'') = ISNULL(src.[PointTypeID],'')
	AND ISNULL(tgt.[ActivityCauseSystemID],'') = ISNULL(src.[ActivityCauseSystemID],'')
	AND ISNULL(tgt.[ActivityCauseID],'') = ISNULL(src.[ActivityCauseID],'')
	AND ISNULL(tgt.[ActivityCauseUserID],'') = ISNULL(src.[ActivityCauseUserID],'')
	AND ISNULL(tgt.[ActivityCauseAmount],0) = ISNULL(src.[ActivityCauseAmount],0)
	AND ISNULL(tgt.[ActivityCauseCurrency],'') = ISNULL(src.[ActivityCauseCurrency],'')
	AND ISNULL(tgt.[ActivityCauseDate],'1900-01-01') = ISNULL(src.[ActivityCauseDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeDate],'1900-01-01') = ISNULL(src.[CurrencyExchangeDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeRate],0) = ISNULL(src.[CurrencyExchangeRate],0)
	AND ISNULL(tgt.[Points],0) = ISNULL(src.[Points],0)
	AND ISNULL(tgt.[PointLiabilityUSD],0) = ISNULL(src.[PointLiabilityUSD],0)
	AND ISNULL(tgt.[Notes],'') = ISNULL(src.[Notes],'')
	AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [ActivityDate], [ActivityTypeID], [PointTypeID], [ActivityCauseSystemID], [ActivityCauseID], [ActivityCauseUserID], [ActivityCauseAmount], [ActivityCauseCurrency], [ActivityCauseDate], [CurrencyExchangeDate], [CurrencyExchangeRate], [Points], [PointLiabilityUSD], [Notes], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey],  [QueueID],[External_SourceKey]	)
		VALUES( src.[ActivityDate], src.[ActivityTypeID], src.[PointTypeID], src.[ActivityCauseSystemID], src.[ActivityCauseID], src.[ActivityCauseUserID], src.[ActivityCauseAmount], src.[ActivityCauseCurrency], src.[ActivityCauseDate], src.[CurrencyExchangeDate], src.[CurrencyExchangeRate], src.[Points], src.[PointLiabilityUSD], src.[Notes], src.[LoyaltyNumberID], src.[DataSourceID], src.[Internal_SourceKey],  src.[QueueID],src.[External_SourceKey])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_Point]'
GO














-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Point] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[Epsilon_Populate_Point]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
		-- Loyalty.dbo.Epsilon_Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.dbo.Epsilon_Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.dbo.Epsilon_Populate_LoyaltyNumber 
	---------------------------------------------------------------------------


		-- dbo.Epsilon_Populate_ActivityType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_ActivityType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_ActivityType 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_PointType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_PointType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_PointType 
	--------------------------------------------------------------------------
	
	-- dbo.Epsilon_Populate_ActivityCauseSystem ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_ActivityCauseSystem'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_ActivityCauseSystem 
	----------------------------------------------------------------------------


	-- dbo.Epsilon_Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_UserUpdated 
	----------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_PointActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_PointActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_PointActivity @QueueID
	----------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_RedemptionActivity]'
GO











-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of dbo import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RedemptionActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_RedemptionActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.RedemptionActivity AS tgt
	USING
	(
		SELECT DISTINCT ra.[RedemptionActivityID] AS [Internal_SourceKey]
		, [MemberRewardNumber]
		,  CAST(REPLACE(LEFT(NULLIF([CreateDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CreateDate],''),2)   AS datetime )  AS [CreateDate]
		, CAST(REPLACE(LEFT(NULLIF([LastUpdated],''),18),'.',':') + ' ' + RIGHT(NULLIF([LastUpdated],''),2)   AS datetime )  AS [LastUpdated]
		, uu.[UserUpdatedID]
		, [RedeemedHotelID]
		, rt.[RewardTypeID]
		, r.[RewardID]
		, CAST(NULLIF([RewardCost],'') AS decimal(20,2) )  AS [RewardCost]
		, [RewardCurrency]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		, [QueueID]
		,[SystemRedemptionID] AS [External_SourceKey]
		FROM epsilon.[RedemptionActivity] ra
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = ra.LoyaltyNumberID
		LEFT JOIN dbo.Reward r ON r.Epsilon_ID  = ra.RewardID
		LEFT JOIN dbo.RewardType rt ON rt.Epsilon_ID  = ra.RewardTypeID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = ra.UserUpdatedID
		WHERE ra.QueueID = @QueueID
	) AS src ON 
		 ISNULL(tgt.[Internal_SourceKey],'') = ISNULL(src.[Internal_SourceKey],'')
	 AND ISNULL(tgt.[MemberRewardNumber],'') = ISNULL(src.[MemberRewardNumber],'')
	 AND ISNULL(tgt.[CreateDate],'1900-01-01') = ISNULL(src.[CreateDate],'1900-01-01')
	 AND ISNULL(tgt.[LastUpdated],'1900-01-01') = ISNULL(src.[LastUpdated],'1900-01-01')
	 AND ISNULL(tgt.[UserUpdatedID],'') = ISNULL(src.[UserUpdatedID],'')
	 AND ISNULL(tgt.[RedeemedHotelID],'') = ISNULL(src.[RedeemedHotelID],'')
	 AND ISNULL(tgt.[RewardTypeID],'') = ISNULL(src.[RewardTypeID],'')
	 AND ISNULL(tgt.[RewardID],'') = ISNULL(src.[RewardID],'')
	 AND ISNULL(tgt.[RewardCost],0) = ISNULL(src.[RewardCost],0)
	 AND ISNULL(tgt.[RewardCurrency],'') = ISNULL(src.[RewardCurrency],'')
	 AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	 AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	 AND ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [MemberRewardNumber], [CreateDate], [LastUpdated], [UserUpdatedID], [RedeemedHotelID], [RewardTypeID], [RewardID], [RewardCost], [RewardCurrency], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey], [External_SourceKey], [QueueID] )
		VALUES( src.[MemberRewardNumber], src.[CreateDate], src.[LastUpdated], src.[UserUpdatedID], src.[RedeemedHotelID], src.[RewardTypeID], src.[RewardID], src.[RewardCost], src.[RewardCurrency], src.[LoyaltyNumberID],src.[DataSourceID], src.[Internal_SourceKey], src.[External_SourceKey], src.[QueueID] )
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [epsilon].[Populate_PointActivity]'
GO








-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [epsilon].[Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  iep.ACT_POINT_ID AS [SystemPointActivityID]
		, iep.QueueID AS QueueID
		, iep.CREATE_DATE AS [ActivityDate]
		, ac.[ActivityTypeID] AS [ActivityTypeID]
		, pt.[PointTypeID] AS [PointTypeID]
		, iep.Update_User AS [UpdateUser]
		, acs.[ActivityCauseSystemID] AS [ActivityCauseSystemID]
		, COALESCE(NULLIF(iep.ACT_TRANSACTION_ID,''),NULLIF(iep.ACT_ADJUSTMENT_ID,''),NULLIF(iep.ACT_ACTIVITY_ID,''),iep.ACT_ORDER_REWARD_ID) AS [ActivityCauseID]
		, iep.[Transaction_Number] AS [TransactionNumber]
		, uu.[UserUpdatedID] AS [ActivityCauseUserID]
		, iep.TRANSACTION_NET_AMOUNT AS [ActivityCauseAmount]
		, iep.CURRENCY_CODE AS [ActivityCauseCurrency]
		, iep.TRANSACTION_DATE AS [ActivityCauseDate]
		, iep.CURRENCY_EXCHANGE_DATE AS [CurrencyExchangeDate]
		, iep.CURRENCY_EXCHANGE_RATE AS [CurrencyExchangeRate]
		, iep.NUM_POINTS AS [Points]
		, iep.USD_PTS_LIABILITY AS [PointLiabilityUSD]
		, iep.TRANS_DESCRIPTION AS [Notes]
		, ln.LoyaltyNumberID
		FROM ETL.dbo.Import_EpsilonPoint iep
		INNER JOIN epsilon.LoyaltyNumber ln ON ln.LoyaltyNumberName = iep.Card_Number
		LEFT JOIN epsilon.ActivityType ac ON iep.DISPLAY_POINT_TYPE  = ac.ActivityTypeName
		LEFT JOIN epsilon.PointType pt ON pt.PointTypeName  = iep.SHORT_DESC
		LEFT JOIN epsilon.ActivityCauseSystem acs ON acs.ActivityCauseSystemName  = iep.TXN_SOURCE_CODE
		LEFT JOIN epsilon.UserUpdated uu ON uu.UserUpdatedName = iep.CREATE_USER
		WHERE iep.QueueID = @QueueID
	) AS src ON 
	src.[SystemPointActivityID] = tgt.[SystemPointActivityID]
	AND src.[ActivityDate] = tgt.[ActivityDate]
	AND src.[ActivityTypeID] = tgt.[ActivityTypeID]
	AND src.[PointTypeID] = tgt.[PointTypeID]
	AND src.[UpdateUser] = tgt.[UpdateUser]
	AND src.[ActivityCauseSystemID] = tgt.[ActivityCauseSystemID]
	AND src.[ActivityCauseID] = tgt.[ActivityCauseID]
	AND src.[TransactionNumber] = tgt.[TransactionNumber]
	AND src.[ActivityCauseUserID] = tgt.[ActivityCauseUserID]
	AND src.[ActivityCauseAmount] = tgt.[ActivityCauseAmount]
	AND src.[ActivityCauseCurrency] = tgt.[ActivityCauseCurrency]
	AND src.[ActivityCauseDate] = tgt.[ActivityCauseDate]
	AND src.[CurrencyExchangeDate] = tgt.[CurrencyExchangeDate]
	AND src.[CurrencyExchangeRate] = tgt.[CurrencyExchangeRate]
	AND src.[Points] = tgt.[Points]
	AND src.[PointLiabilityUSD] = tgt.[PointLiabilityUSD]
	AND src.[Notes] = tgt.[Notes]
	AND src.LoyaltyNumberID = tgt.LoyaltyNumberID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(	[SystemPointActivityID]
		,QueueID
	,[ActivityDate]
	,[ActivityTypeID]
	,[PointTypeID]
	,[UpdateUser]
	,[ActivityCauseSystemID]
	,[ActivityCauseID]
	,[TransactionNumber]
	,[ActivityCauseUserID]
	,[ActivityCauseAmount]
	,[ActivityCauseCurrency]
	,[ActivityCauseDate]
	,[CurrencyExchangeDate]
	,[CurrencyExchangeRate]
	,[Points]
	,[PointLiabilityUSD]
	,[Notes]
	,LoyaltyNumberID)
		VALUES([SystemPointActivityID]
		,QueueID
	,[ActivityDate]
	,[ActivityTypeID]
	,[PointTypeID]
	,[UpdateUser]
	,[ActivityCauseSystemID]
	,[ActivityCauseID]
	,[TransactionNumber]
	,[ActivityCauseUserID]
	,[ActivityCauseAmount]
	,[ActivityCauseCurrency]
	,[ActivityCauseDate]
	,[CurrencyExchangeDate]
	,[CurrencyExchangeRate]
	,[Points]
	,[PointLiabilityUSD]
	,[Notes]
	,LoyaltyNumberID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [epsilon].[Populate_Point]'
GO










-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Point] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
ALTER PROCEDURE [epsilon].[Populate_Point]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
		-- Loyalty.epsilon.Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.epsilon.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.epsilon.Populate_LoyaltyNumber @QueueID
	---------------------------------------------------------------------------


		-- epsilon.Populate_ActivityType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_ActivityType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_ActivityType @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_PointType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_PointType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_PointType @QueueID
	--------------------------------------------------------------------------
	
	-- epsilon.Populate_ActivityCauseSystem ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_ActivityCauseSystem'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_ActivityCauseSystem @QueueID
	----------------------------------------------------------------------------


	-- epsilon.Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_UserUpdated @QueueID
	----------------------------------------------------------------------------

	-- epsilon.Populate_PointActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_PointActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_PointActivity @QueueID
	----------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
