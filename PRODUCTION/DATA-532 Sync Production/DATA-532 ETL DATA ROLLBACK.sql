/*
Run this script on:

CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/1/2019 9:45:38 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Delete rows from [dbo].[ServerFilePath]')
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'Adara' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%monthly[_]rpt[_][0-9][0-9][0-9][0-9][_][0-9][0-9][_][0-9][0-9].csv'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'CraftedImportGlobalBooking' AND [Section] = 'FILE PATTERN' AND [FilePath] = 'Global Booking Data[_]%[0-9][0-9][0-9][0-9][_]Preferred%.csv'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'FTP' AND [FilePath] = 'c1sftp01.epsilon.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'FTP DIR' AND [FilePath] = '/NonProd/LoyaltyFiles/Imports/Currency'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'PASSWORD' AND [FilePath] = '_qPwN7omur'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'USERNAME' AND [FilePath] = 'prhg_p_phg'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'FTP' AND [FilePath] = 'c1sftp01.epsilon.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'FTP DIR' AND [FilePath] = '/NonProd/LoyaltyFiles/Imports/Stores'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'PASSWORD' AND [FilePath] = '_qPwN7omur'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'USERNAME' AND [FilePath] = 'prhg_p_phg'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\EpsilonExport\PointTransaction\'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'FTP' AND [FilePath] = 'c1sftp01.epsilon.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'FTP DIR' AND [FilePath] = '/NonProd/LoyaltyFiles/Imports/Transactions'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'PASSWORD' AND [FilePath] = '_qPwN7omur'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'USERNAME' AND [FilePath] = 'prhg_p_phg'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\EpsilonExport\Reservation\'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'FTP' AND [FilePath] = 'c1sftp01.epsilon.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'FTP DIR' AND [FilePath] = '/NonProd/LoyaltyFiles/Imports/Reservations'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'PASSWORD' AND [FilePath] = '_qPwN7omur'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'USERNAME' AND [FilePath] = 'prhg_p_phg'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackCurrency' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Currency'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackHotel' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%PHG[_]STORE%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%.csv'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackHotel' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Stores'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackPointTransaction' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Transactions'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackReservation' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\Epsilon\EpsilonHandbackReservation\'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportMember' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Extracts'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportPoint' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Extracts'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportRedemption' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Extracts'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportReward' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\Epsilon\EpsilonImportReward\'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportReward' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon_loyalty/downloads/Prod/Extracts'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_ABC' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_AMEX' AND [Section] = 'EXCEL SHEET' AND [FilePath] = 'All GBT LOBs'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_AMEX' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_BCD' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_CCRA' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_CWT' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_FCM' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_HRG' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_RADIUS' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_THOR' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_TRAVEL_TRANSPORT' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'IATA_ULTRAMAR' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-ST-01\WAREHOUSE' AND [Application] = 'Adara' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%monthly[_]rpt[_][0-9][0-9][0-9][0-9][_][0-9][0-9][_][0-9][0-9].csv'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'LOCAL' AND [Application] = 'Adara' AND [Section] = 'FILE PATTERN' AND [FilePath] = '%monthly[_]rpt[_][0-9][0-9][0-9][0-9][_][0-9][0-9][_][0-9][0-9].csv'
PRINT(N'Operation applied to 44 rows out of 44')
COMMIT TRANSACTION
GO
