USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/30/2019 1:05:03 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Charges]'
GO
ALTER TABLE [dbo].[Charges] DROP CONSTRAINT [PK_Charges_Reservation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Charges_20190528_transactionIdReload]'
GO
ALTER TABLE [dbo].[Charges_20190528_transactionIdReload] DROP CONSTRAINT [PK_Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] from [dbo].[Charges]'
GO
DROP INDEX [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] from [dbo].[Charges]'
GO
DROP INDEX [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_hotelCode_billableDate_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_hotelCode_billableDate_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_runID_classificationID_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_runID_classificationID_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_runID_classificationID_sopNumber_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_runID_classificationID_sopNumber_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] from [dbo].[Charges]'
GO
DROP INDEX [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_transactionSourceID_transactionKey_sopNumber] from [dbo].[Charges]'
GO
DROP INDEX [IX_transactionSourceID_transactionKey_sopNumber] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_runID_classificationID_IC_Others] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_runID_classificationID_IC_Others] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_runID_classificationID_sopNumber_IC_Others] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_runID_classificationID_sopNumber_IC_Others] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_transactionSourceID_transactionKey_sopNumber] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_transactionSourceID_transactionKey_sopNumber] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_classificationID_IC_confirmationNumber_hotelCode_chargeValueInUSD] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_classificationID_IC_confirmationNumber_hotelCode_chargeValueInUSD] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_classificationID_confirmationNumber_IC_itemCode] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_classificationID_confirmationNumber_IC_itemCode] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_IC_classificationID_hotelCode_chargeValueInUSD] from [dbo].[Charges_20190528_transactionIdReload]'
GO
DROP INDEX [IX_confirmationNumber_IC_classificationID_hotelCode_chargeValueInUSD] ON [dbo].[Charges_20190528_transactionIdReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BillyControlTotals]'
GO
DROP PROCEDURE [dbo].[BillyControlTotals]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [test].[Populate_MrtForCalculation_ORIGINAL]'
GO
DROP PROCEDURE [test].[Populate_MrtForCalculation_ORIGINAL]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [work].[Populate_MrtForCalculation_ORIGINAL]'
GO
DROP PROCEDURE [work].[Populate_MrtForCalculation_ORIGINAL]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Charges]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Charges] ALTER COLUMN [transactionKey] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] ON [dbo].[Charges] ([arrivalDate], [sopNumber]) INCLUDE ([confirmationNumber], [hotelCode], [transactionKey], [transactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] ON [dbo].[Charges] ([billableDate], [hotelCode], [transactionSourceID], [clauseID]) INCLUDE ([roomNights], [roomRevenueInHotelCurrency], [transactionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hotelCode_billableDate_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_hotelCode_billableDate_IC_Others] ON [dbo].[Charges] ([hotelCode], [billableDate]) INCLUDE ([clauseID], [roomNights], [roomRevenueInHotelCurrency], [transactionKey], [transactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID_classificationID_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_runID_classificationID_IC_Others] ON [dbo].[Charges] ([runID], [classificationID]) INCLUDE ([chargeValueInHotelCurrency], [chargeValueInUSD], [transactionKey], [transactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID_classificationID_sopNumber_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_runID_classificationID_sopNumber_IC_Others] ON [dbo].[Charges] ([runID], [classificationID], [sopNumber]) INCLUDE ([chargeValueInHotelCurrency], [chargeValueInUSD], [transactionKey], [transactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] ON [dbo].[Charges] ([sopNumber], [hotelCode], [confirmationNumber]) INCLUDE ([transactionKey], [transactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_transactionSourceID_transactionKey_sopNumber] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_transactionSourceID_transactionKey_sopNumber] ON [dbo].[Charges] ([transactionSourceID], [transactionKey], [sopNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Charges] on [dbo].[Charges]'
GO
ALTER TABLE [dbo].[Charges] ADD CONSTRAINT [PK_Charges] PRIMARY KEY NONCLUSTERED  ([chargeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[LoadBillyErrorTable]'
GO


-- =============================================
-- Author:		Kris Scott
-- Create date: 04/29/2016
-- Description:	
-- History: 2019-06-03 Ti Yao Swith to Reservations DB version
-- EXEC [work].[LoadBillyErrorTable] 19646
-- =============================================
ALTER PROCEDURE [work].[LoadBillyErrorTable]
	@RunID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'


--DECLARE @RunID int = 19646
declare	@startDate DATE, 
		@endDate DATE,
		@maxCalcDate DATE;
		
-- get current run dates
select	@startDate = startdate, 
		@endDate = enddate,
		@maxCalcDate = RunDate
from	[work].[Run]
WHERE runID = @runID

  SELECT
	c.transactionSourceID, c.transactionKey
  INTO #nonBillable
  FROM dbo.Charges as c
  WHERE c.billableDate BETWEEN @startDate AND @endDate
  AND c.[classificationID] = 4
  
  CREATE TABLE #mrtWithoutCharges
  (
	[hotelCode] nvarchar(50)
    ,[hotelName] nvarchar(250)
    ,[synxisID] nvarchar(50)
    ,[confirmationNumber] nvarchar(max)
	,classificationID int
    ,clauseID int
    ,[channel] nvarchar(max)
    ,[secondarySource] nvarchar(max)
    ,[subSourceCode] nvarchar(max)
    ,[CROCode] nvarchar(max)
    ,[templateAbbreviation] nvarchar(max)
    ,[xbeTemplateName] nvarchar(max)
    ,[rateCategoryCode] nvarchar(max)
    ,[rateTypeCode] nvarchar(max)
    ,[arrivalDate] date
    ,[OpenHospitalityID] nvarchar(50)
    ,currency nvarchar(5)
	,bookingChargesCount int
	,commissionChargesCount int
	,iPreferChargesCount int
	,phHotelCode nvarchar(50)
	,gpHotelCode nvarchar(50)
	,gpCurrency nvarchar(5)
	,resCurrency nvarchar(5)
  )

INSERT into	#mrtWithoutCharges
   select 
	COALESCE(hrs.code, hro.code,hh.HotelCode) as [hotelCode]
    ,hh.HotelName as [hotelName]
    ,hh.synXisID as [synxisID]
    ,t.confirmationNumber as [confirmationNumber]
	,cd.classificationID
    ,brule.clauseID
    ,cha.channel as [channel]
    ,sec.secondarySource as [secondarySource]
    ,sub.subSourceCode as [subSourceCode]
    ,acro.CRO_Code as [CROCode]
    ,ISNULL(pt.[siteAbbreviation], 'HOTEL') as [templateAbbreviation]
    ,aibe.ibeSourceName as [xbeTemplateName]
    ,rc.rateCategoryCode as [rateCategoryCode]
    ,rac.RateCode as [rateTypeCode]
    ,td.arrivalDate as [arrivalDate]
    ,hh.openhospID as [OpenHospitalityID]
    ,td.currency
	,bc.bookingChargesCount
	,cc.commissionChargesCount
	,ipc.iPreferChargesCount
	,COALESCE(hrs.code, hro.code, NULL) as phHotelCode
	,gpc.custnmbr as gpHotelCode
	,gpc.CURNCYID as gpCurrency
	,cur.CURNCYID as resCurrency
FROM Reservations.dbo.Transactions t 
	INNER JOIN Reservations.dbo.TransactionStatus ts  ON ts.TransactionStatusID = t.TransactionStatusID
	INNER JOIN Reservations.dbo.TransactionDetail td  ON td.TransactionDetailID = t.TransactionDetailID
	LEFT JOIN Reservations.dbo.Chain ch  ON ch.ChainID = t.ChainID
	LEFT JOIN Reservations.dbo.hotel ht  ON ht.HotelID = t.HotelID
	LEFT JOIN Hotels.dbo.Hotel hh  ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
	LEFT JOIN Reservations.dbo.CRS_BookingSource bs  ON bs.BookingSourceID = t.CRS_BookingSourceID
	LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
	LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
	LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
	LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
	LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
	LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
	LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
	LEFT JOIN Reservations.dbo.RateCategory rc  ON rc.RateCategoryID = t.RateCategoryID
	LEFT JOIN Reservations.dbo.IATANumber iata  ON iata.IATANumberID = t.IATANumberID
	LEFT JOIN Reservations.dbo.TravelAgent ta  ON ta.TravelAgentID = t.TravelAgentID
	LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
	LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
	LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
	LEFT JOIN Reservations.synxis.transactions tr ON tr.TransactionID = t.sourceKey AND t.DataSourceID = @SynXis_DS
	LEFT JOIN Reservations.synxis.BillingDescription bd ON tr.BillingDescriptionID = bd.BillingDescriptionID

	LEFT OUTER JOIN [dbo].[Templates] pt ON aibe.ibeSourceName = pt.[xbeTemplateName]
	LEFT OUTER JOIN dbo.Charges cd on t.TransactionID = cd.transactionKey AND cd.transactionSourceID IN (1,2)
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on t.TransactionID = bc.transactionKey AND bc.transactionSourceID IN (1,2)
	LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on t.TransactionID = cc.transactionKey AND cc.transactionSourceID IN (1,2)	
	LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON t.TransactionID = ipc.transactionKey AND ipc.transactionSourceID IN (1,2)	
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hrs ON hh.synXisID = hrs.synxisID
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hro ON hh.openhospID = hro.openHospitalityCode
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON COALESCE(hro.code, hrs.code) = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON td.currency = cur.CURNCYID		AND td.arrivalDate = cur.EXCHDATE
	LEFT OUTER JOIN #nonBillable nb	on t.TransactionID = nb.transactionKey AND nb.transactionSourceID IN (1,2)
where (cd.hotelCode IS NULL OR (bookingChargesCount IS NULL OR bookingChargesCount <> 1) OR (commissionChargesCount > 1))
and td.arrivalDate BETWEEN @startDate AND @endDate
AND t.timeLoaded < DATEADD(day,-2,@maxCalcDate)
AND ts.status <> 'Cancelled'
AND nb.transactionKey IS NULL; --only want to see bookings not handled by non-billable charges


INSERT into	#mrtWithoutCharges
   select 
	tdr.Hotel_Code as [hotelCode]
    ,hr.hotelName as [hotelName]
    ,'' as [synxisID]
    ,tdr.Booking_ID as [confirmationNumber]
	,cd.classificationID
    ,brule.clauseID
    ,'iPrefer Manual Entry' as [channel]
    ,tdr.Transaction_Source as [secondarySource]
    ,tdr.Remarks as [subSourceCode]
    ,'' as [CROCode]
    ,'' as [templateAbbreviation]
    ,'' as [xbeTemplateName]
    ,'' as [rateCategoryCode]
    ,'' as [rateTypeCode]
    ,tdr.Reward_Posting_Date as [arrivalDate]
    ,'' as [OpenHospitalityID]
    ,tdr.Currency_Code
	,bc.bookingChargesCount
	,cc.commissionChargesCount
	,ipc.iPreferChargesCount
	,hr.code as phHotelCode
	,gpc.custnmbr as gpHotelCode
	,gpc.CURNCYID as gpCurrency
	,cur.CURNCYID as resCurrency
from superset.bsi.TransactionDetailedReport tdr
	LEFT OUTER JOIN Core.dbo.hotelsReporting hr ON tdr.Hotel_Code = hr.code
	LEFT OUTER JOIN dbo.Charges cd on tdr.Transaction_Id = cd.transactionKey AND cd.transactionSourceID = 3 --iprefer manual points
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on tdr.Transaction_Id = bc.transactionKey AND bc.transactionSourceID = 3
	LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on tdr.Transaction_Id = cc.transactionKey AND cc.transactionSourceID = 3	
	LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON tdr.Transaction_Id = ipc.transactionKey AND ipc.transactionSourceID = 3	
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON hr.code = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON tdr.Currency_Code = cur.CURNCYID AND tdr.Reward_Posting_Date = cur.EXCHDATE
	LEFT OUTER JOIN #nonBillable nb	on tdr.Transaction_Id = nb.transactionKey AND nb.transactionSourceID = 3 --iprefer manual points
where (cd.hotelCode IS NULL OR (ipc.iPreferChargesCount IS NULL OR ipc.iPreferChargesCount  <> 1))
and tdr.Reward_Posting_Date BETWEEN @startDate AND @endDate
AND tdr.Reward_Posting_Date < DATEADD(day,-2,@maxCalcDate)
AND tdr.Reservation_Revenue <> 0
AND tdr.Points_Earned <> 0
AND tdr.Transaction_Source IN ('Admin Portal'
	,'Hotel Portal'
	)
AND nb.transactionKey IS NULL --only want to see bookings not handled by non-billable charges;



	--	empty the billy error table
	truncate table dbo.BillyCalcErrors

	-- reinsert new error records into the table
	insert into dbo.BillyCalcErrors
	(errorMessage, hotelCode, hotelName, synxisID, openHospitalityID, confirmationNumber, clauseID, channel, secondarySource, subSourceCode, CROCode, templateAbbreviation, xbeTemplateName, rateCategoryCode, rateTypeCode, arrivalDate)


SELECT 'Unable to map hotel to CRM' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NULL

UNION ALL

SELECT 'No hotel found in GP' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NULL

UNION ALL

SELECT 'No currency found on GP customer card' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NULL

UNION ALL

SELECT 'No currency exchange rate found in GP for ' + currency as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND mrt.arrivalDate < GETDATE()
AND resCurrency IS NULL 

UNION ALL

SELECT DISTINCT 'Multiple booking charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND mrt.bookingChargesCount > 1
AND mrt.classificationID = 1

UNION ALL

SELECT DISTINCT 'No booking or I Prefer charge found' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (
	(mrt.bookingChargesCount = 0 OR mrt.bookingChargesCount IS NULL)
	AND (mrt.ipreferChargesCount = 0 OR mrt.ipreferChargesCount IS NULL)
	)

UNION ALL

SELECT DISTINCT 'Multiple commission charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.commissionChargesCount > 1)
AND mrt.classificationID = 2


UNION ALL


SELECT DISTINCT 'Multiple iPrefer charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.iPreferChargesCount > 1)
AND mrt.classificationID = 5


UNION ALL

SELECT 'Unknown error' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.bookingChargesCount = 1)
--AND (mrt.surchargeCount IS NULL OR mrt.surchargeCount <= 1)
AND (mrt.commissionChargesCount IS NULL OR mrt.commissionChargesCount <= 1)
;

DROP TABLE #mrtWithoutCharges;
DROP TABLE #nonBillable;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[mrtJoined_Reservation]'
GO








ALTER VIEW [work].[mrtJoined_Reservation]
AS

     SELECT 
			1 AS transactionSourceID, -- hardcode 1 use transactionID for the difference between openhospitality and sabre
			t.TransactionID as transactionKey,
            t.confirmationNumber,
			hh.HotelCode AS phgHotelCode,
            COALESCE(hh.openhospID, hh.synXisID) AS crsHotelID,
            hh.HotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
			CASE WHEN ch.ChainID = 22 THEN 56 ELSE ch.ChainID END AS [ChainID],--change from CRS_ChainID since open hospitality CRS_ChainID is not integer
            ch.chainName,
            ts.status AS bookingStatus,
            CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN td.billingDescription ELSE 'Unknown' END AS synxisBillingDescription,
            cha.channel AS bookingChannel,
            sec.secondarySource AS bookingSecondarySource,
            sub.subSourceCode AS bookingSubSourceCode,
            COALESCE(te.templateGroupID, 2) AS bookingTemplateGroupId,
			CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END AS bookingTemplateAbbreviation,
            aibe.ibeSourceName AS xbeTemplateName,
            acro.CRO_Code AS CROcode,
            CASE WHEN acro.CRO_Code IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            rc.rateCategoryCode AS bookingRateCategoryCode,
            rac.RateCode AS bookingRateCode,
            ISNULL(iata.IATANumber,'') AS bookingIATA,
            t.transactionTimeStamp,
            ts.confirmationDate,
            td.arrivalDate,
            td.departureDate,
            ts.cancellationDate,
            ts.cancellationNumber,
            td.nights,
            td.rooms,
            td.nights * td.rooms AS roomNights,
            td.reservationRevenue AS roomRevenueInBookingCurrency,
            td.currency AS bookingCurrencyCode,
            t.timeLoaded,
			work.[billyItemCode](CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN td.billingDescription ELSE 'Unknown' END,cha.channel,sec.secondarySource,sub.subSourceCode,CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END,croCodes.croGroupID, 123) AS [ItemCode], --hard code chainId since OH chainID is not int
			CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN ts.confirmationDate ELSE td.arrivaldate END) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            CASE WHEN bs.openHospID IS NOT NULL THEN 2 WHEN bs.synXisID IS NOT NULL THEN 1 ELSE 'Unknown' END AS CRSSourceID,
			lp.LoyaltyProgram,
			ISNULL(ln.loyaltyNumber,'') AS loyaltyNumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],
			--force tag if negative iprefer returns
			 CASE WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberValidated,0)
					WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					WHEN work.billyLoyaltyFlipFlag(t.confirmationNumber) = 1 THEN 1
					ELSE ISNULL(td.LoyaltyNumberValidated,0) END AS LoyaltyNumberValidated
				--new change
			,CASE WHEN ISNULL(td.LoyaltyNumberTagged,0) = 0 THEN 0
					 WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberTagged,0)
					 WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					 ELSE ISNULL(td.LoyaltyNumberTagged,0)
					 END as LoyaltyNumberTagged
		FROM Reservations.dbo.Transactions t WITH(NOLOCK)
		INNER JOIN Reservations.dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN Reservations.dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN Reservations.dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN Reservations.dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
		LEFT JOIN Reservations.dbo.CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
		LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
		LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
		LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
		LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
		LEFT JOIN Reservations.dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN Reservations.dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN Reservations.dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID

        LEFT JOIN work.hotelActiveBrands ON hh.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hh.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hh.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON td.currency = bookingCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = bookingCE.EXCHDATE
		LEFT JOIN [Superset].[BSI].[TransactionDetailedReport] tdr ON tdr.Booking_ID = t.confirmationNumber AND tdr.Hotel_Code = hh.HotelCode
		AND MONTH(tdr.Reward_Posting_Date) = MONTH(td.arrivalDate) AND YEAR(tdr.Reward_Posting_Date) = YEAR(td.arrivalDate) AND tdr.Reservation_Revenue <> 0 AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source IN ('Admin Portal','Hotel Portal')
		LEFT JOIN [dbo].[CROCodes] ON acro.CRO_Code = croCodes.croCode
		LEFT JOIN dbo.Templates te ON te.xbeTemplateName = aibe.ibeSourceName

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Charges_20190528_transactionIdReload]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Charges_20190528_transactionIdReload] ALTER COLUMN [transactionKey] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_EXCHDATE] on [work].[local_exchange_rates]'
GO
CREATE NONCLUSTERED INDEX [IX_EXCHDATE] ON [work].[local_exchange_rates] ([EXCHDATE])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
