USE CmsImport
GO

/*
Run this script on:

        CHI-SQ-DP-01\CHISQZ01.CmsImport    -  This database will be modified

to synchronize it with:

        CHISQZ01.CmsImport

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/2/2020 1:24:41 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_PHR_Members]'
GO
ALTER PROCEDURE [dbo].[mpid_PHR_Members]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.SystemUser WHERE InternalEMailAddress IS NOT NULL AND IsDisabled = 0)
	BEGIN
		TRUNCATE TABLE dbo.PhrMembers_Hotels
		TRUNCATE TABLE dbo.PhrMembers_Inactive


		--  Retrieve PHR Members who are using the demo hotel
		DECLARE @TestHotelId nvarchar(MAX),
				@TestHotelIdXml nvarchar(MAX);

		SELECT @TestHotelId = CAST(cmsContentXml.xml as xml).value('(/Hotel/@key)[1]','nvarchar(MAX)')
		FROM MemberPortal.dbo.cmsContentXml
		WHERE CAST(cmsContentXml.xml as xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
			AND CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelName)[1]','nvarchar(MAX)') = 'PHG Test Hotel'

		SELECT	@TestHotelIdXml = '%<i>umb://document/' + REPLACE(@TestHotelId, '-', '') + '</i>%'


		DECLARE @PhgTestHotelUsers TABLE(emailAddress nvarchar(100),TestHotelId nvarchar(MAX))

		INSERT INTO @PhgTestHotelUsers(emailAddress,TestHotelId)
		SELECT email,@TestHotelId
		FROM (SELECT CAST(cmsContentXml.xml as xml).value('(/PHRMember/@id)[1]','nvarchar(MAX)') AS id,
					CAST(cmsContentXml.xml as xml).value('(/PHRMember/@loginName)[1]','nvarchar(MAX)') AS email,
					CONVERT(XML, '<i>' + REPLACE(CAST(cmsContentXml.xml as xml).value('(/PHRMember/hotels)[1]','nvarchar(MAX)'), ',', '</i><i>') + '</i>').query('.') AS hotelXML
				FROM MemberPortal.dbo.cmsContentXml
				WHERE CAST(cmsContentXml.xml as xml).value('(/PHRMember/@nodeType)[1]','nvarchar(MAX)') = 10458
					AND CAST(cmsContentXml.xml as xml).value('(/PHRMember/hotels)[1]','nvarchar(MAX)') IS NOT NULL
			  ) xmlData
		WHERE CAST(xmldata.hotelxml as nvarchar(max)) like @TestHotelIdXml


		-- Grab users that should have all hotels
		DECLARE @PhgAllHotelUsers TABLE(emailAddress nvarchar(100))

		INSERT INTO @PhgAllHotelUsers(emailAddress)
		SELECT emailAddress FROM dbo.PhrMembers_AllHotels

		UPDATE dbo.PhrMembers
			SET updateMember = 0


		DECLARE @hotels TABLE(emailAddress nvarchar(100),HotelIDs nvarchar(MAX))
	
		INSERT INTO @hotels(emailAddress,HotelIDs)
		SELECT emailAddress, HotelNodeID
		FROM (SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
							CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelAccountDirector)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0
			
					UNION
			
				SELECT	DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
								CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelMarketingContact)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE	CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION

				SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
								CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelRegionalDirector)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION

				SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
								CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelAreaManagingDirector)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION

				SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
								CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelRevenueAccountManager)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION

				SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
								CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelAccountsReceivableContact)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION
				
				SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
						CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID

				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
                                                                                                                  AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
                                                                                                                  AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelRegionalAdministration)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION

				SELECT DISTINCT CAST(PhgEmployeeXML.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]', 'nvarchar(MAX)') AS emailAddress,
								CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM MemberPortal.dbo.cmsContentXml HotelXML
					INNER JOIN MemberPortal.dbo.cmsContentXml PhgEmployeeXML ON CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
																			AND CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@nodeType)[1]', 'nvarchar(MAX)') = 2145
																			AND CAST(HotelXML.xml AS xml).value('(/Hotel/hotelExecutiveVicePresident)[1]','nvarchar(MAX)') = 'umb://document/' + REPLACE(CAST(PhgEmployeeXML.xml AS xml).value('(/PhgEmployee/@key)[1]', 'nvarchar(MAX)'), '-', '')
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0

				UNION

				SELECT emailAddress,CAST(HotelXML.xml AS XML).value('(/Hotel/@id)[1]', 'nvarchar(MAX)') AS HotelNodeID
				FROM @PhgAllHotelUsers,
					 MemberPortal.dbo.cmsContentXml HotelXML
				WHERE CAST(HotelXML.xml AS xml).value('(/Hotel/disabled)[1]','bit') = 0
					AND CAST(HotelXML.xml AS xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729

				UNION

				SELECT DISTINCT D.emailAddress,D.TestHotelId AS HotelNodeID
				FROM @PhgTestHotelUsers D
			) AS unioned



		INSERT INTO dbo.PhrMembers_Hotels(emailAddress,defaultHotel,hotels)
		SELECT DISTINCT H1.emailAddress,
					CAST((SELECT TOP 1 dh.HotelIDs FROM @hotels dh WHERE dh.emailAddress = H1.emailAddress) AS nvarchar(100)) AS defaultHotel,
					CAST((SELECT H2.HotelIDs + ',' FROM @hotels H2 WHERE H2.emailAddress = H1.emailAddress FOR XML PATH('')) AS nvarchar(MAX)) AS hotels
		FROM @hotels H1



		UPDATE dbo.PhrMembers_Hotels
			SET hotels = LEFT(hotels,LEN(hotels) - 1)



		DECLARE @PhrMembersData TABLE(TableName nvarchar(6),
										firstName nvarchar(100),
										lastName nvarchar(100),
										title nvarchar(100),
										email nvarchar(100),
										defaultHotel nvarchar(100),
										hotels nvarchar(MAX)
									 );

		INSERT INTO @PhrMembersData(TableName,firstName,lastName,title,email,defaultHotel,hotels)
		SELECT MIN(TableName) AS TableName,firstName,lastName,title,email,defaultHotel,hotels
		FROM (SELECT 'SOURCE' AS TableName,FirstName AS firstName,LastName AS lastName,Title AS title,InternalEMailAddress AS email,H.defaultHotel,H.hotels
				FROM LocalCRM.dbo.SystemUser
					LEFT JOIN dbo.PhrMembers_Hotels H ON SystemUser.InternalEMailAddress = H.emailAddress COLLATE SQL_Latin1_General_CP1_CI_AS
				WHERE InternalEMailAddress IS NOT NULL
					AND IsDisabled = 0

				UNION

				SELECT 'TARGET' AS TableName,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/firstName)[1]','nvarchar(MAX)') AS firstName,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/lastName)[1]','nvarchar(MAX)') AS lastName,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/title)[1]','nvarchar(MAX)') AS title,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/@loginName)[1]','nvarchar(MAX)') AS email,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/defaultHotel)[1]','nvarchar(MAX)') AS defaultHotel,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/hotels)[1]','nvarchar(MAX)') AS hotels
				FROM MemberPortal.dbo.cmsContentXml cmsXML
					INNER JOIN MemberPortal.dbo.cmsMember ON CAST(cmsXML.xml AS xml).value('(/PHRMember/@id)[1]', 'nvarchar(MAX)') = cmsMember.nodeId
				WHERE CAST(cmsXML.xml AS xml).value('(/PHRMember/@nodeType)[1]','nvarchar(MAX)') = 10458
					AND CAST(cmsXML.xml AS xml).value('(/PHRMember/umbracoMemberApproved)[1]','nvarchar(MAX)') = 1
			  ) AS unioned
		GROUP BY firstName,lastName,title,email,defaultHotel,hotels
		HAVING COUNT(*) = 1



		UPDATE dbo.PhrMembers
			SET firstName = P.firstName,
				lastName = P.lastName,
				title = P.title,
				updateMember = 1
		FROM dbo.PhrMembers
			INNER JOIN @PhrMembersData P ON PhrMembers.email = P.email
			INNER JOIN MemberPortal.dbo.cmsMember ON p.email = cmsMember.LoginName
			INNER JOIN MemberPortal.dbo.cmsContentXml cmsXML ON cmsMember.nodeId = CAST(cmsXML.xml AS xml).value('(/PHRMember/@id)[1]', 'nvarchar(MAX)')
															AND CAST(cmsXML.xml AS xml).value('(/PHRMember/@nodeType)[1]','nvarchar(MAX)') = 10458
															AND	CAST(cmsXML.xml AS xml).value('(/PHRMember/umbracoMemberApproved)[1]','nvarchar(MAX)') = 1
		WHERE P.TableName = 'SOURCE'



		INSERT INTO dbo.PhrMembers(firstName,lastName,title,email,updateMember,sendEmail)
		SELECT firstName,lastName,title,email,1,1
		FROM @PhrMembersData P
		WHERE P.TableName = 'SOURCE'
			AND P.email NOT IN(SELECT N.email FROM dbo.PhrMembers AS N)

		UPDATE p
			SET p.updateMember = 1
		FROM dbo.PhrMembers p
			LEFT OUTER JOIN MemberPortal.dbo.cmsMember c ON p.email = c.Email
		WHERE c.nodeId IS NULL

		INSERT INTO dbo.PhrMembers_Inactive(email)
		SELECT T.email
		FROM @PhrMembersData T
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS ( SELECT 1 FROM @PhrMembersData S WHERE S.TableName = 'SOURCE' AND S.email = T.email );
		
	
		UPDATE dbo.PhrMembers
			SET	updateMember = 0
		WHERE email IN(SELECT emailAddress FROM dbo.PhrMembers_ExcludeMembers)
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
