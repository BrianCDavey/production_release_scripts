/*
Run this script on:

        (local)\WAREHOUSE.BusinessPortalApp    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.BusinessPortalApp

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 11/10/2021 1:24:05 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[uspCustomerSummary_All]'
GO





ALTER PROCEDURE [dbo].[uspCustomerSummary_All] 
@AppUser varchar(64)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * 
	INTO #accountList
	FROM dbo.[udfUserSecurity](@AppUser);

SELECT     dbo.vwCustomerSummary.[Customer Number], dbo.vwCustomerSummary.[Customer Name], dbo.vwCustomerSummary.[Aging Bucket1], 
                      dbo.vwCustomerSummary.[Aging Bucket2], dbo.vwCustomerSummary.[Aging Bucket3], dbo.vwCustomerSummary.[Aging Bucket4], 
                      dbo.vwCustomerSummary.[Aging Bucket5], dbo.vwCustomerSummary.[Aging Bucket6], dbo.vwCustomerSummary.[Customer Balance], 
                      dbo.vwCustomerSummary.[Customer Class], dbo.vwCustomerSummary.[Last Payment Amount], dbo.vwCustomerSummary.[Last Payment Date], 
                      dbo.vwCustomerSummary.[Last Statement Amount], dbo.vwCustomerSummary.[Last Statement Date], dbo.vwCustomerSummary.[Sales Territory], 
                      dbo.vwCustomerSummary.[Statement Name],dbo.vwCustomerSummary.CYTDSales,
                      dbo.vwCustomerSummary.[Last Aged], PercentComplete,
                      COALESCE(Account.phg_regionalmanageridName
						,	Account.phg_areamanageridName
						,	Account.phg_revenueaccountmanageridName
						,	Account.phg_regionaladministrationidName
						,	Account.[PHG_PHGContactIdName]
						,	Account.[phg_corporateglobalaccountmanageridName]
						,	Account.[phg_touroperatorglobalaccountmanageridName]
						,	Account.[phg_alliancepartnermanageridName]
						, 'None') as phg_regionalmanageridName, 
                      CAST(Account.AccountId AS VARCHAR(100)) AS crmGuid

FROM         dbo.vwCustomerSummary 
	INNER JOIN #accountList as [udfUserSecurity]
		ON dbo.vwCustomerSummary.[Customer Number] = [udfUserSecurity].[Customer Number]
	LEFT JOIN LocalCRM.dbo.Account
        ON dbo.vwCustomerSummary.[Customer Number] = Account.AccountNumber 
                      
WHERE dbo.vwCustomerSummary.[Customer Balance] <> 0 OR dbo.vwCustomerSummary.CYTDSales <> 0;

DROP TABLE #accountList

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
