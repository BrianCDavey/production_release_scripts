USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/28/2020 9:53:42 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[iPreferAutoEnrollReservationsMissingNumbers]'
GO

ALTER   PROCEDURE [rpt].[iPreferAutoEnrollReservationsMissingNumbers] 
	@byArrival bit = 0,
	@startDate date,
	@endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_MRT
	AS
	(
		SELECT t.TransactionID,t.confirmationNumber,hh.HotelCode,td.arrivalDate,ts.confirmationDate,ts.[status],
				td.LoyaltyNumberValidated,ge.emailAddress AS customerEmail,rc.RateCode AS RateTypeCode,
				bs.ChannelReportLabel,bs.subSourceReportLabel,td.optIn,bs.channel,hh.HotelName,g.salutation,
				g.FirstName AS guestFirstName,g.LastName AS guestLastName,
				g.Address1 AS customerAddress1,g.Address2 AS customerAddress2,
				loc.City_Text AS customerCity,loc.State_Text AS customerState,loc.PostalCode_Text AS customerPostalCode,
				g.phone AS customerPhone,loc.Country_Text AS customerCountry
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN Reservations.dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
			LEFT JOIN Reservations.dbo.hotel h WITH(NOLOCK) ON h.HotelID = t.HotelID
			LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = h.Hotel_hotelID
			LEFT JOIN Reservations.dbo.Guest g WITH(NOLOCK) ON g.GuestID = t.GuestID
				LEFT JOIN Reservations.dbo.Guest_EmailAddress ge WITH(NOLOCK) ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
				LEFT JOIN Reservations.dbo.vw_Location loc ON loc.LocationID = g.LocationID
			LEFT JOIN Reservations.dbo.RateCode rc WITH(NOLOCK) ON rc.RateCodeID = t.RateCodeID
			LEFT JOIN Reservations.dbo.vw_CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
			where 
			(
			(@byArrival = 1 AND td.arrivalDate BETWEEN @startDate AND @endDate)
			OR
			(@byArrival = 0 AND ts.confirmationdate BETWEEN @startDate AND @endDate)
			)
			AND	ts.[status] <> 'Cancelled'
			AND td.LoyaltyNumberValidated <> 1
	),
	cte_NoOptIns
	AS
	(
		SELECT mrt.TransactionID
		FROM cte_MRT mrt
			LEFT JOIN Hotels.authority.IPrefer_Checkbox ip ON mrt.hotelCode = ip.hotelCode
		where 
		mrt.customerEmail <> ''
		-- opt in logic but in reverse
		AND 
		mrt.rateTypeCode <> 'MKTIPM'
		AND NOT(mrt.ChannelReportLabel = 'IBE - Hotel' AND mrt.optIn = 1 AND ip.hotelCode IS NOT NULL) --opted in Hotel site bookings
		AND
		(
			NOT(
				mrt.optIn = 1
				AND mrt.ChannelReportLabel = 'IBE - PHG'
				AND mrt.subSourceReportLabel NOT IN(
													'HHA Call Center',
													'HHA Call Center - iPrefer Partners ON HHA.org',
													'www.historichotels.org',
													'www.historichotels.org-cro',
													'www.historichotels.org-flex',
													'www.historichotels.org-gcomi',
													'www.historichotels.org-partner',
													'www.historichotelsworldwide.com',
													'www.historichotelsworldwide.com-flex',
													'Active International',
													'LuxLink',
													'Sky Auction',
													'www.phgoffers.com-choice',
													'www.phgoffers.com-lion',
													'www.phgoffers.com-svc',
													'www.preferredhotelgroup.com-luxlink',
													'www.preferredhotelgroup.com-perx',
													'www.preferredhotelgroup.com-skyauction',
													'www.preferredhotelgroup.com-amex'
													)
				)
		) -- end opt in logic
	)
	
	SELECT 
		mrt.confirmationdate,
		mrt.arrivalDate,
		mrt.confirmationNumber,
		mrt.channel,
		mrt.HotelCode,
		mrt.HotelName,
		mrt.RateTypeCode,
		mrt.salutation,
		mrt.guestFirstName,
		mrt.guestLastName,
		mrt.customerAddress1,
		mrt.customerAddress2,
		mrt.customerCity,
		mrt.customerState,
		mrt.customerPostalCode,
		mrt.customerPhone,
		mrt.customerCountry,
		mrt.customerEmail,
		ln.LoyaltyNumberName AS existingIPreferMemberNumber
	FROM cte_MRT mrt
		INNER JOIN cte_NoOptIns noi ON noi.TransactionID = mrt.TransactionID
		INNER JOIN Guests.dbo.Guest_EmailAddress ge WITH(NOLOCK) ON mrt.customerEmail = ge.emailAddress
		INNER JOIN Guests.dbo.Guest g WITH(NOLOCK) ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
		INNER JOIN Loyalty.dbo.MemberProfile mp WITH(NOLOCK) ON g.LoyaltyNumberID = mp.LoyaltyNumberID
		INNER JOIN Loyalty.dbo.LoyaltyNumber ln WITH(NOLOCK) ON ln.LoyaltyNumberID = g.LoyaltyNumberID
	WHERE mp.MemberStatus = 1
	ORDER BY arrivaldate asc


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
