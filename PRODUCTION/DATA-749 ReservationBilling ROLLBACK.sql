USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/17/2020 6:22:40 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IPreferGuestNameReport_Reservation]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-05-30
-- Description:	This proc is a test version of IPreferGuestNameReport_2017 pointing to IC DB called by SSRS GuestNameReport_Test
-- EXEC [dbo].[IPreferGuestNameReport_Reservation] 'RES0179263           '
-- =============================================


ALTER  procedure [dbo].[IPreferGuestNameReport_Reservation]
	@SOPNumber varchar(20)
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

---- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'

--DECLARE @SOPNumber varchar(20) = 'RES0115115'
-- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'


DECLARE @currencymaster TABLE (
								CURNCYID char(15),
								CURRNIDX smallint,
								NOTEINDX numeric(19, 5),
								CRNCYDSC char(31),
								CRNCYSYM char(3),
								CNYSYMAR_1 smallint,
								CNYSYMAR_2 smallint,
								CNYSYMAR_3 smallint,
								CYSYMPLC smallint,
								INCLSPAC tinyint,
								NEGSYMBL smallint,
								NGSMAMPC smallint,
								NEGSMPLC smallint,
								DECSYMBL smallint,
								DECPLCUR smallint,
								THOUSSYM smallint,
								CURTEXT_1 char(25),
								CURTEXT_2 char(25),
								CURTEXT_3 char(25),
								ISOCURRC char(3),
								CURLNGID smallint,
								DEX_ROW_TS datetime,
								DEX_ROW_ID int
							);

insert into @currencymaster
select	CURNCYID,
		CURRNIDX,
		NOTEINDX,
		CRNCYDSC,
		CRNCYSYM,
		CNYSYMAR_1,
		CNYSYMAR_2,
		CNYSYMAR_3,
		CYSYMPLC,
		INCLSPAC,
		NEGSYMBL,
		NGSMAMPC,
		NEGSMPLC,
		DECSYMBL,
		DECPLCUR,
		THOUSSYM,
		CURTEXT_1,
		CURTEXT_2,
		CURTEXT_3,
		ISOCURRC,
		CURLNGID,
		DEX_ROW_TS,
		DEX_ROW_ID
FROM DYNAMICS.dbo.MC40200



DECLARE @GP_SOPNumber_ItemCodes TABLE (	
										ITEMNMBR varchar(50),
										ItemGrouping varchar(250)
									);

INSERT INTO @GP_SOPNumber_ItemCodes(ITEMNMBR,ItemGrouping)
SELECT itemCode.ITEMNMBR,
	CASE
		WHEN itemCode.ITEMDESC <> COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC) THEN COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC)
		ELSE
			CASE COALESCE(RTRIM(itemClasses.ITMCLSDC), '') WHEN '' THEN ''
			ELSE RTRIM(itemClasses.ITMCLSDC) + ' '
		END
		+
		CASE COALESCE(RTRIM(itemCode.ITMSHNAM),'')
			WHEN '' THEN '' 
			ELSE RTRIM(itemCode.ITMSHNAM)
		END 
	END as itemGrouping	
FROM IC.dbo.IV00101 AS itemCode 
	LEFT JOIN IC.dbo.SY03900 itemNotes ON itemCode.NOTEINDX = itemNotes.NOTEINDX 
	LEFT JOIN IC.dbo.IV40600 itemClassifications ON itemClassifications.USCATNUM = 1 AND itemCode.USCATVLS_1 = itemClassifications.USCATVAL 
	LEFT JOIN IC.dbo.IV40400 itemClasses ON itemCode.ITMCLSCD = itemClasses.ITMCLSCD
	LEFT JOIN IC.dbo.SOP30300 historyLines ON historyLines.SOPNUMBE = 'abc' AND itemCode.itemCode = historyLines.ITEMNMBR
	LEFT JOIN IC.dbo.SOP10200 workLines ON  workLines.SOPNUMBE = 'ABC' AND itemCode.itemCode = workLines.ITEMNMBR

--EXEC sp_ExecuteSQL @SqlCommand;


SELECT	COALESCE(main.currency, iPrefer.currency) AS currency
,		COALESCE(main.arrivalDate, iPrefer.arrivalDate) AS arrivalDate
,		COALESCE(main.guestLastName, iPrefer.guestLastName) AS guestLastName
,		COALESCE(main.guestFirstName, iPrefer.guestFirstName) AS guestFirstName
,		COALESCE(main.confirmationNumber, iPrefer.confirmationNumber) AS confirmationNumber
,		COALESCE(main.confirmationDate, iPrefer.confirmationDate) AS confirmationDate
,		COALESCE(main.consortia, iPrefer.consortia) AS consortia
,		COALESCE(main.IATANumber, iPrefer.IATANumber) AS IATANumber
,		COALESCE(main.rateTypeCode, iPrefer.rateTypeCode) AS rateTypeCode
,		COALESCE(CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END , iPrefer.iPreferNumber, '') AS iPreferNumber
,		COALESCE(main.averageDailyRate, iPrefer.averageDailyRate) AS averageDailyRate
,		COALESCE(main.roomNights, iPrefer.roomNights) AS roomNights
,		COALESCE(main.reservationRevenue, iPrefer.reservationRevenue) AS reservationRevenue
,		COALESCE(main.iPreferCharge, iPrefer.iPreferCharge) AS iPreferCharge
,		COALESCE(main.commCharge, iPrefer.commCharge) AS commCharge
,		COALESCE(main.bookCharge, iPrefer.bookCharge) AS bookCharge
,		COALESCE(main.surcharge, iPrefer.surcharge) AS surcharge
,		ISNULL(main.totalCharge, 0) + ISNULL(iPrefer.totalCharge, 0) AS totalCharge
,		COALESCE(REPLACE(main.itemGrouping, 'zzzzz! ', ''), iPrefer.itemGrouping, 'Other Booking Sources' ) AS itemGrouping --remove hardcode value zzzzz!
,		COALESCE(main.billingDate, iPrefer.billingDate) as billingDate

FROM (
Select	chargedReservations.hotelCurrencyCode as currency, 
		chargedReservations.arrivalDate, 
		UPPER(MRT.guestLastName) AS guestLastName, 
		UPPER(MRT.guestFirstName) AS guestFirstName, 
		chargedReservations.confirmationNumber, 
		MRT.confirmationDate,
		ReservationBilling.dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		MRT.rateTypeCode,
		chargedReservations.loyaltyNumber AS iPreferNumber,
		Case When chargedReservations.roomNights <> 0
			THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
			ELSE 0 END as averageDailyRate, 
		chargedReservations.roomNights, 
		chargedReservations.roomRevenueInHotelCurrency as reservationRevenue, 
				SUM(case when chargedReservations.[classificationID] = 5 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS iPreferCharge,
		SUM(case when chargedReservations.[classificationID] = 1 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS bookCharge, 
		SUM(case when chargedReservations.[classificationID] = 2 -- commission
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS commCharge, 
		SUM(case when chargedReservations.classificationID = 3 -- surcharges
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS surcharge,
		SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
		MIN(case when chargedReservations.[classificationID] NOT IN (3,5) -- surcharges I prefer
				 then LTRIM(RTRIM(GPSopIC.itemGrouping))
				when chargedReservations.[classificationID] = 5 
				 then 'zzzzz! Hotel & Manual I Prefer Point Awards' --append zzzzz! on purpose for max logic to work
				 when chargedReservations.classificationID = 3 AND mrt.channel IN ('Channel Connect','IDS') AND itemCode = 'AMEXTMC'
					then 'zzzzz! Expedia/Amex OTA'
			END ) AS itemGrouping,
		chargedReservations.billableDate as billingDate
		
FROM	@GP_SOPNumber_ItemCodes GPSopIC

JOIN ReservationBilling.dbo.Charges chargedReservations with (nolock)
	ON  chargedReservations.sopNumber = @SOPNumber
	AND	chargedReservations.itemCode = GPSopIC.ITEMNMBR 

LEFT OUTER JOIN 	Reservations.dbo.mostrecenttransactions MRT with (nolock)
	ON	chargedReservations.confirmationNumber = MRT.confirmationNumber 

WHERE chargedReservations.classificationID <> 5
OR (chargedReservations.classificationID = 5 AND chargedReservations.chargeValueInUSD <> 0.00 AND chargedReservations.itemCode NOT IN ('IPREFERMANUAL_I', 'PMS_I') )

GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
		chargedReservations.arrivalDate, MRT.guestLastName, 
		MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
		MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber
) AS main
full outer join
(
SELECT	IPC.Hotel_Currency AS currency,
		CAST(IPC.Arrival_Date AS DATE) AS arrivalDate,
		UPPER(SUBSTRING(Account_Statement_Import.Member_Name, CHARINDEX(' ', Account_Statement_Import.Member_Name) + 1, LEN(Account_Statement_Import.Member_Name))) AS guestLastName,
		UPPER(SUBSTRING(Account_Statement_Import.Member_Name, 1, NULLIF(CHARINDEX(' ', Account_Statement_Import.Member_Name) - 1, -1))) AS guestFirstName,
		IPC.Booking_ID AS confirmationNumber,
		IPC.Booking_Date AS confirmationDate,
		ReservationBilling.dbo.getBilledConsortia_NewBilly(IPC.Booking_ID) AS consortia,
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		COALESCE (MRT.rateTypeCode, N'') AS rateTypeCode,
		IPC.Membership_Number AS iPreferNumber,
		Case When DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date) <> 0
			 then ROUND(IPC.Room_Revenue ,currencyMaster.DECPLCUR-1) / (DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date)) 
			 ELSE 0 end
		AS averageDailyRate,
		DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date) AS roomNights,
		ROUND(IPC.Room_Revenue ,currencyMaster.DECPLCUR-1) AS reservationRevenue,
		SUM(IPC.Billable_Amount_Hotel_Currency) AS iPreferCharge,
		NULL AS bookCharge, 
		NULL AS commCharge, 
		NULL AS surcharge,
		SUM(IPC.Billable_Amount_Hotel_Currency) AS totalCharge,
		'Hotel & Manual I Prefer Point Awards' AS itemGrouping,
		IPC.Billing_Date as billingDate

FROM Superset.[BSI].[IPrefer_Charges] IPC

LEFT JOIN Reservations.dbo.mostrecenttransactions MRT
	ON IPC.Booking_ID = MRT.confirmationNumber
			
JOIN	Superset.BSI.Account_Statement_Import
	ON	IPC.Booking_ID = Account_Statement_Import.Booking_ID
     AND IPC.Membership_Number = Account_Statement_Import.Membership_Number
     AND IPC.Reservation_Amount = Account_Statement_Import.Reservation_Amount	
	 AND IPC.Billing_Concept = Account_Statement_Import.Billing_Concept
	
JOIN @currencyMaster as currencyMaster
	ON	IPC.Hotel_Currency = currencyMaster.CURNCYID

WHERE	IPC.SOPNumber = @SOPNumber 
AND IPC.Billing_Concept <> 'iPrefer Direct to Prop'


GROUP BY IPC.Arrival_Date,
		IPC.SOPNumber,
		Account_Statement_Import.Member_Name,
		IPC.Membership_Number,
		IPC.Booking_ID,
		MRT.confirmationDate,
		MRT.IATANumber,
		MRT.rateTypeCode,
		IPC.Booking_Date,
		DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date),
		--IPC.Billing_Concept,
		IPC.Hotel_Code,
		Account_Statement_Import.Hotel_Name,
		IPC.Hotel_Currency,
		IPC.Reservation_Amount_USD,
		IPC.Room_Revenue,
		--IPC.Flat_Charges,
		--IPC.Percentage_Charges, 
		currencyMaster.DECPLCUR,
		IPC.Billing_Date
) AS iPrefer
ON main.confirmationNumber = iPrefer.confirmationNumber
UNION ALL


--Display New billy Iprefer manual point award
SELECT	COALESCE(main.currency, iPrefer.currency) AS currency
,		COALESCE(main.arrivalDate, iPrefer.arrivalDate) AS arrivalDate
,		COALESCE(main.guestLastName, iPrefer.guestLastName) AS guestLastName
,		COALESCE(main.guestFirstName, iPrefer.guestFirstName) AS guestFirstName
,		COALESCE(main.confirmationNumber, iPrefer.confirmationNumber) AS confirmationNumber
,		COALESCE(main.confirmationDate, iPrefer.confirmationDate) AS confirmationDate
,		COALESCE(main.consortia, iPrefer.consortia) AS consortia
,		COALESCE(main.IATANumber, iPrefer.IATANumber) AS IATANumber
,		COALESCE(main.rateTypeCode, iPrefer.rateTypeCode) AS rateTypeCode
,		COALESCE(CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END , iPrefer.iPreferNumber, '') AS iPreferNumber
,		COALESCE(main.averageDailyRate, iPrefer.averageDailyRate) AS averageDailyRate
,		COALESCE(main.roomNights, iPrefer.roomNights) AS roomNights
,		COALESCE(main.reservationRevenue, iPrefer.reservationRevenue) AS reservationRevenue
,		COALESCE(main.iPreferCharge, iPrefer.iPreferCharge) AS iPreferCharge
,		COALESCE(main.commCharge, iPrefer.commCharge) AS commCharge
,		COALESCE(main.bookCharge, iPrefer.bookCharge) AS bookCharge
,		COALESCE(main.surcharge, iPrefer.surcharge) AS surcharge
,		ISNULL(main.totalCharge, 0) + ISNULL(iPrefer.totalCharge, 0) AS totalCharge
,		COALESCE(REPLACE(main.itemGrouping, 'zzzzz! ', ''), iPrefer.itemGrouping, 'Other Booking Sources' ) AS itemGrouping --remove hardcode value zzzzz!
,		COALESCE(main.billingDate, iPrefer.billingDate) as billingDate

FROM (
Select	chargedReservations.hotelCurrencyCode as currency, 
		chargedReservations.arrivalDate, 
		UPPER(MRT.guestLastName) AS guestLastName, 
		UPPER(MRT.guestFirstName) AS guestFirstName, 
		chargedReservations.confirmationNumber, 
		MRT.confirmationDate,
		ReservationBilling.dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		MRT.rateTypeCode,
		chargedReservations.loyaltyNumber AS iPreferNumber,
		Case When chargedReservations.roomNights <> 0
			THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
			ELSE 0 END as averageDailyRate, 
		chargedReservations.roomNights, 
		chargedReservations.roomRevenueInHotelCurrency as reservationRevenue, 
				SUM(case when chargedReservations.[classificationID] = 5 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS iPreferCharge,
		SUM(case when chargedReservations.[classificationID] = 1 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS bookCharge, 
		SUM(case when chargedReservations.[classificationID] = 2 -- commission
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS commCharge, 
		SUM(case when chargedReservations.classificationID = 3 -- surcharges
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS surcharge,
		SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
		MIN(case when chargedReservations.[classificationID] NOT IN (3,5) -- surcharges I prefer
				 then LTRIM(RTRIM(GPSopIC.itemGrouping))
				when chargedReservations.[classificationID] = 5 
				 then 'zzzzz! Hotel & Manual I Prefer Point Awards' --append zzzzz! on purpose for max logic to work
				 when chargedReservations.classificationID = 3 AND mrt.channel IN ('Channel Connect','IDS') AND itemCode = 'AMEXTMC'
					then 'zzzzz! Expedia/Amex OTA'
			END ) AS itemGrouping,
		chargedReservations.billableDate as billingDate
		
FROM	@GP_SOPNumber_ItemCodes GPSopIC

JOIN ReservationBilling.dbo.Charges chargedReservations with (nolock)
	ON  chargedReservations.sopNumber = @SOPNumber
	AND	chargedReservations.itemCode = GPSopIC.ITEMNMBR 

LEFT OUTER JOIN 	Reservations.dbo.mostrecenttransactions MRT with (nolock)
	ON	chargedReservations.confirmationNumber = MRT.confirmationNumber 

WHERE chargedReservations.classificationID = 5 AND chargedReservations.chargeValueInUSD <> 0.00 AND chargedReservations.itemCode IN ('IPREFERMANUAL_I', 'PMS_I') 

GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
		chargedReservations.arrivalDate, MRT.guestLastName, 
		MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
		MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber
) AS main
full outer join
(
SELECT	IPC.Hotel_Currency AS currency,
		CAST(IPC.Arrival_Date AS DATE) AS arrivalDate,
		UPPER(SUBSTRING(Account_Statement_Import.Member_Name, CHARINDEX(' ', Account_Statement_Import.Member_Name) + 1, LEN(Account_Statement_Import.Member_Name))) AS guestLastName,
		UPPER(SUBSTRING(Account_Statement_Import.Member_Name, 1, NULLIF(CHARINDEX(' ', Account_Statement_Import.Member_Name) - 1, -1))) AS guestFirstName,
		IPC.Booking_ID AS confirmationNumber,
		IPC.Booking_Date AS confirmationDate,
		ReservationBilling.dbo.getBilledConsortia_NewBilly(IPC.Booking_ID) AS consortia,
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		COALESCE (MRT.rateTypeCode, N'') AS rateTypeCode,
		IPC.Membership_Number AS iPreferNumber,
		Case When DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date) <> 0
			 then ROUND(IPC.Room_Revenue ,currencyMaster.DECPLCUR-1) / (DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date)) 
			 ELSE 0 end
		AS averageDailyRate,
		DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date) AS roomNights,
		ROUND(IPC.Room_Revenue ,currencyMaster.DECPLCUR-1) AS reservationRevenue,
		SUM(IPC.Billable_Amount_Hotel_Currency) AS iPreferCharge,
		NULL AS bookCharge, 
		NULL AS commCharge, 
		NULL AS surcharge,
		SUM(IPC.Billable_Amount_Hotel_Currency) AS totalCharge,
		'Hotel & Manual I Prefer Point Awards' AS itemGrouping,
		IPC.Billing_Date as billingDate

FROM Superset.[BSI].[IPrefer_Charges] IPC

LEFT JOIN Reservations.dbo.mostrecenttransactions MRT
	ON IPC.Booking_ID = MRT.confirmationNumber
			
JOIN	Superset.BSI.Account_Statement_Import
	ON	IPC.Booking_ID = Account_Statement_Import.Booking_ID
     AND IPC.Membership_Number = Account_Statement_Import.Membership_Number
     AND IPC.Reservation_Amount = Account_Statement_Import.Reservation_Amount	
	 AND IPC.Billing_Concept = Account_Statement_Import.Billing_Concept
	
JOIN @currencyMaster as currencyMaster
	ON	IPC.Hotel_Currency = currencyMaster.CURNCYID

WHERE	IPC.SOPNumber = @SOPNumber 
AND IPC.Billing_Concept = 'iPrefer Direct to Prop'


GROUP BY IPC.Arrival_Date,
		IPC.SOPNumber,
		Account_Statement_Import.Member_Name,
		IPC.Membership_Number,
		IPC.Booking_ID,
		MRT.confirmationDate,
		MRT.IATANumber,
		MRT.rateTypeCode,
		IPC.Booking_Date,
		DATEDIFF( d, IPC.Arrival_Date, IPC.Departure_Date),
		--IPC.Billing_Concept,
		IPC.Hotel_Code,
		Account_Statement_Import.Hotel_Name,
		IPC.Hotel_Currency,
		IPC.Reservation_Amount_USD,
		IPC.Room_Revenue,
		--IPC.Flat_Charges,
		--IPC.Percentage_Charges, 
		currencyMaster.DECPLCUR,
		IPC.Billing_Date
) AS iPrefer
ON main.confirmationNumber = iPrefer.confirmationNumber


ORDER BY itemGrouping, arrivalDate, guestLastName, confirmationNumber
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
