USE Reservations
GO

/*
Run this script on:

        CHI-SQ-ST-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/27/2020 10:19:46 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[iPreferAlliancePoints]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-02-28
-- Description:	Proc for Tokyu I Prefer program
-- =============================================
CREATE PROCEDURE [rpt].[iPreferAlliancePoints] 
	-- Add the parameters for the stored procedure here
	@iPreferNumber varchar(10) = '63DBA3' --the special account set up for the alliance partner points
	,@startDate date
	,@endDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select 
		convert(varchar, ISNULL(td.arrivalDate, tdr.Reward_Posting_Date), 107) as [Transaction Date],
		CASE WHEN TRIM(ISNULL(ln.loyaltyNumber,'')) = '' THEN 'None' ELSE ln.loyaltyNumber END as [Partner Member Number],
		tdr.Points_Earned as [Points],
		(tdr.Points_Earned * 0.002) as [Value USD],
		5 as [Handling Fee USD],
		(tdr.Points_Earned * 0.002)	- 5 as [Amount to Partner USD],
		CASE WHEN t.confirmationNumber IS NULL THEN tdr.Remarks + ' Booking ID: ' + tdr.Booking_ID ELSE
		hh.HotelName + ', Arrival ' + convert(varchar, td.arrivalDate, 107) + ', Departure ' + convert(varchar, td.departureDate, 107) + ', CONF# ' + t.confirmationNumber END as [Notes]

	from Loyalty.dbo.TransactionDetailedReport tdr
	LEFT JOIN Reservations.dbo.Transactions t
		ON tdr.Booking_ID = t.confirmationNumber
	LEFT JOIN Reservations.dbo.TransactionDetail td
		ON t.TransactionDetailID = td.TransactionDetailID
	LEFT JOIN Reservations.dbo.LoyaltyNumber ln
		ON t.LoyaltyNumberID = ln.LoyaltyNumberID
	LEFT JOIN Reservations.dbo.hotel rh
		ON t.HotelID = rh.HotelID
	LEFT JOIN Hotels.dbo.hotel hh
		ON rh.Hotel_hotelID = hh.HotelID
	where tdr.iPrefer_Number = @iPreferNumber
	AND Points_Earned <> 0
	AND ISNULL(td.arrivalDate, tdr.Reward_Posting_Date) BETWEEN @startDate AND @endDate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[iPreferBillingReconciliation]'
GO



CREATE PROCEDURE [rpt].[iPreferBillingReconciliation]
	@StartDate DATE,
	@EndDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @StartDate DATE = '2018-01-01'
--DECLARE @EndDate DATE = '2018-05-31'

;WITH chargeCTE AS(
	SELECT
	C.confirmationNumber as 'Booking ID'
	,ISNULL(SUM(C.chargeValueInUSD), 0) as 'Billy charges in USD'
	,ISNULL(SUM(C.roomRevenueInUSD), 0) as 'Reservation Revenue USD'
	,ISNULL(SUM(TDR.Points_Earned), 0) as 'Points Awarded'


	FROM 
		ReservationBilling.dbo.Charges as C
		LEFT JOIN Loyalty.dbo.TransactionDetailedReport as TDR ON C.confirmationNumber = TDR.Booking_ID
	
	WHERE 
		C.arrivalDate BETWEEN @StartDate AND @EndDate
		and C.classificationID = 5
	
	GROUP BY C.confirmationNumber
	
	HAVING (ISNULL(SUM(C.chargeValueInUSD), 0) = 0
			AND ISNULL(SUM(TDR.Points_Earned), 0) <> 0 )
		OR (ISNULL(SUM(TDR.Points_Earned), 0) = 0 
			AND ISNULL(SUM(C.chargeValueInUSD), 0) <> 0)
	)

	,loyaltyCTE AS (
	
	SELECT
	TDR2.Booking_ID as 'Booking ID'
	,ISNULL(SUM(C2.chargeValueInUSD), 0) as 'Billy charges in USD'
	,ISNULL(SUM(C2.roomRevenueInUSD), 0) as 'Reservation Revenue USD'
	,ISNULL(SUM(TDR2.Points_Earned), 0) as 'Points Awarded'


	FROM 
		ReservationBilling.dbo.Charges as C2
		RIGHT JOIN Loyalty.dbo.TransactionDetailedReport as TDR2 ON C2.confirmationNumber = TDR2.Booking_ID
	
	WHERE 
		TDR2.Transaction_Date BETWEEN @StartDate AND @EndDate AND
		 (C2.chargeID is null or C2.classificationID = 5)
	
	GROUP BY TDR2.Booking_ID
	
	HAVING (ISNULL(SUM(C2.chargeValueInUSD), 0) = 0
			AND ISNULL(SUM(TDR2.Points_Earned), 0) <> 0 )
		OR (ISNULL(SUM(TDR2.Points_Earned), 0) = 0 
			AND ISNULL(SUM(C2.chargeValueInUSD), 0) <> 0)
	)


SELECT
	CASE WHEN (CCTE.[Billy charges in USD] = 0 AND CCTE.[Points Awarded] <> 0) OR (LCTE.[Billy charges in USD] = 0 AND LCTE.[Points Awarded] <> 0)  THEN 'Points but not billed'
	WHEN (CCTE.[Points Awarded] = 0 AND CCTE.[Billy charges in USD] <> 0) OR (LCTE.[Points Awarded]= 0 AND LCTE.[Billy charges in USD] <> 0) THEN 'Billed but no points' 
	END as 'Status'
	,CASE WHEN MRT.confirmationNumber is not null THEN 'Superset' ELSE 'Manual' END as 'Transaction Source'
	,CASE WHEN CCTE.[Booking ID] IS not null THEN CCTE.[Booking ID]
	WHEN LCTE.[Booking ID] is not null then LCTE.[Booking ID]
	ELSE MRT.confirmationNumber	END as 'Booking ID'
	,CASE WHEN MRT.confirmationNumber is not null THEN MRT.channel ELSE 'Manual' END as 'Booking Source'
	,ISNULL(CCTE.[Billy charges in USD],0) + ISNULL(LCTE.[Billy charges in USD],0) as 'Billy charges in USD'
	,ISNULL(CCTE.[Reservation Revenue USD],0) + ISNULL(LCTE.[Reservation Revenue USD],0) as 'Reservation Revenue USD'
	,ISNULL(CCTE.[Points Awarded],0) + ISNULL(LCTE.[Points Awarded] ,0) as 'Points Awarded'
	,CASE WHEN MRT.loyaltyNumber is null  THEN 'None' 
	WHEN MRT.loyaltyNumber = '' THEN 'None'
	ELSE MRT.loyaltyNumber END as 'I Prefer Number'
	,CASE WHEN MRT.loyaltyNumber is null or mrt.loyaltyNumber='' THEN 'None'
	WHEN MRT.LoyaltyNumberValidated is null or mrt.LoyaltyNumberValidated= 0 THEN 'Invalid'
	WHEN MRT.LoyaltyNumberValidated = 1 THEN 
		CASE WHEN MRT.LoyaltyNumberTagged = 1 THEN 'Tagged' 
			ELSE 'Valid' 
			END
	END as 'Loyalty Number Status'

FROM chargeCTE AS CCTE
LEFT JOIN loyaltyCTE AS LCTE ON LCTE.[Booking ID] = CCTE.[Booking ID]
LEFT JOIN Reservations.dbo.mostrecenttransactions AS MRT 
ON  CCTE.[Booking ID] = MRT.confirmationNumber 
OR LCTE.[Booking ID] = MRT.confirmationNumber

END





GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[iPreferFraudReport]'
GO


-- =============================================
-- Author:		Jake Smith
-- Create date: 2020-01-23
-- Description:	
-- =============================================
CREATE PROCEDURE [rpt].[iPreferFraudReport] 
	-- Add the parameters for the stored procedure here
	@arrivalDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


WITH footemp (loyaltyNumber, hotelCode)
	AS (SELECT
	 LoyaltyNumber, hotelcode
	FROM
		  Reservations.dbo.mostrecenttransactionsreporting
	WHERE
		confirmationDate = arrivalDate
		AND	arrivalDate = @arrivalDate
		AND loyaltyNumber <> ''
		AND loyaltyNumber IS NOT NULL
	GROUP BY
		 LoyaltyNumber, hotelcode),

morefoo (loyaltyNumber)
	AS (SELECT 
			LoyaltyNumber
		FROM footemp
		GROUP BY LoyaltyNumber
		HAVING count(*) > 1)

SELECT 
	mrt.loyaltyNumber,
	confirmationNumber,
	confirmationDate,
	arrivalDate,
	departureDate,
	hotelCode,
	hotelName,
	reservationRevenueUSD,
	status
FROM 
     Reservations.dbo.Mostrecenttransactionsreporting mrt
JOIN morefoo as foo ON
	foo.LoyaltyNumber = mrt.loyaltynumber
WHERE
	confirmationDate = arrivalDate
AND
	arrivalDate = @arrivalDate

END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[iPreferProduction]'
GO


-- =============================================
-- Author:		Kris Scott
-- Create date: 06/16/2014
-- Description:	IPrefer production report by hotel/rd
-- =============================================
CREATE PROCEDURE [rpt].[iPreferProduction]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @YTDbookings table(hotelCode nvarchar(10), ytdBookings int, ytdRevenue decimal(18,2))
INSERT INTO @YTDbookings
SELECT hr.code
, COUNT(confirmationNumber) as ytdBookings
, SUM(mrt.reservationRevenueUSD) as ytdRevenue
FROM Reservations.dbo.MostRecentTransactions as mrt
INNER JOIN Hotels.dbo.HotelsReporting as hr on 
	mrt.hotelID = hr.synxisID

WHERE mrt.arrivalDate BETWEEN '01/01/' + CAST(YEAR(GETDATE()) as nvarchar(4)) AND DATEADD(ww, DATEDIFF(ww,0,GETDATE()), -2) --previous saturday
and mrt.loyaltyProgram IN ('iPrefer','')
and mrt.status <> 'Cancelled'
and mrt.loyaltyNumber <> ''

GROUP BY hr.code;

DECLARE @weekBookings table(hotelCode nvarchar(10), weekBookings int, weekRevenue decimal(18,2))
INSERT INTO @weekBookings
SELECT hr.code
, COUNT(confirmationNumber) as weekBookings
, SUM(mrt.reservationRevenueUSD) as weekRevenue
FROM Reservations.dbo.MostRecentTransactions as mrt
INNER JOIN Hotels.dbo.HotelsReporting as hr on 
	mrt.hotelID = hr.synxisID

WHERE mrt.confirmationDate 
	BETWEEN DATEADD(ww, DATEDIFF(ww,0,GETDATE()), -8) --previous sunday
	AND DATEADD(ww, DATEDIFF(ww,0,GETDATE()), -2) --following satuday
and mrt.loyaltyProgram IN ('iPrefer','')
and mrt.status <> 'Cancelled'
and mrt.loyaltyNumber <> ''

GROUP BY hr.code;

DECLARE @certificates table(hotelCode nvarchar(10), certCount int, certAmounts decimal(18,2))
INSERT INTO @certificates
SELECT [Hotel_Code],
count([Voucher_Number]),
SUM(Reservations.dbo.convertCurrencyToUSD(Voucher_Value, Voucher_Currency, Redemption_Date))
FROM Loyalty.dbo.[Account_Statement_Hotel_Redemption_Import]
WHERE [Redemption_Date] between '01/01/' + CAST(YEAR(GETDATE()) as nvarchar(4)) and DATEADD(ww, DATEDIFF(ww,0,GETDATE()), -2)
GROUP BY Hotel_Code;

SELECT hr.phg_regionalmanageridName as RD
, hr.hotelName as Hotel
, YTDbookings.ytdBookings as [YTD Bookings]
, YTDbookings.ytdRevenue as [YTD Room Revenue]
, weekBookings.weekBookings as [Last Week Bookings]
, weekBookings.weekRevenue as [Last Week Revenue]
, certs.certCount as [YTD Certificates]
, certs.certAmounts as [YTD Certificate Value]
FROM Hotels.dbo.hotelsReporting as hr
INNER JOIN Hotels.dbo.Hotel_IPreferReward as hbr 
	on hr.SynXisID = hbr.HotelID
	and hbr.IPreferRewardID IS NOT NULL
	and hbr.IPreferRewardID <> ''
	and hbr.startDate <= DATEADD(ww, DATEDIFF(ww,0,GETDATE()), -2)
	and (hbr.endDate is null OR hbr.endDate >= '01/01/' + CAST(YEAR(GETDATE()) as nvarchar(4)))
LEFT OUTER JOIN @YTDbookings as YTDbookings on hr.code = YTDbookings.hotelCode
LEFT OUTER JOIN @weekBookings as weekBookings on hr.code = weekBookings.hotelCode
LEFT OUTER JOIN @certificates as certs on hr.Code = certs.hotelCode

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[ips_iPreferScorecardArrivalData]'
GO




CREATE PROCEDURE [rpt].[ips_iPreferScorecardArrivalData] 
	@MyDate INT
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardArrivalData.sql
--DECLARE @MyDate INT = 2017

DECLARE @IPreferCharges TABLE(	earliestBooking DATETIME,
								Membership_Number NVARCHAR(6)
							);

INSERT INTO @IPreferCharges
SELECT	MIN(billableDate) as earliestbooking, loyaltyNumber
FROM	ReservationBilling.dbo.Charges
WHERE	billableDate >= '2014-05-01'
AND		classificationID = 5
GROUP BY	loyaltyNumber;

SELECT	HR.code,
		COUNT(*) as arrivals,
		SUM(MRT.reservationRevenueUSD) as hotelrevenue,
		CASE WHEN MRT.secondarySource = 'iPrefer APP' then 'iPrefer APP' 
			WHEN MRT.channelreportlabel IS NULL then 'Direct to Property'
			ELSE MRT.channelreportlabel END as bookedchannel,
		SUM(IC.roomRevenueInUSD) as phgrevenue,
		CASE WHEN MRT.ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END AS MemberRate,
		ISNULL(MRT.OptIn, 'False') as Checkbox,
		ISNULL(MRT.status, 'Confirmed') as bookingStatus,
		CASE WHEN CC.ConfirmationNumber IS NULL then 'NO' ELSE 'YES' END AS MarketingCampaign,
		CASE WHEN ICH.hotelCode IS NULL then 'NO' ELSE 'YES' END as CheckboxHotel,
		CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < MRT.confirmationDate THEN 'NO' ELSE 'YES' END as FirstBooking,
		AVG(DATEDIFF(day,MRT.confirmationDate, MRT.arrivalDate)) as averageleadtime,
		MONTH(MRT.arrivalDate) as arrivalmonth

FROM	Reservations.dbo.mostrecenttransactionsreporting MRT
		JOIN
		Hotels.dbo.hotelsReporting HR
		ON
		HR.synxisID = MRT.hotelID
		JOIN
		ReservationBilling.dbo.Charges IC
		ON
		IC.confirmationNumber = MRT.confirmationNumber
		LEFT OUTER JOIN
		Hotels.authority.IPrefer_Checkbox ICH
		ON
		HR.code = ICH.hotelCode
		LEFT OUTER JOIN 
		@IPreferCharges IPC
		ON
		MRT.loyaltyNumber = IPC.Membership_Number
		JOIN
		Loyalty.dbo.Customer_Profile_Import CPI
		ON
		MRT.loyaltyNumber = CPI.iPrefer_Number
		LEFT OUTER JOIN 
		Marketing.crafted.Conversion CC
		ON
		MRT.confirmationnumber = CC.ConfirmationNumber

WHERE	YEAR(MRT.arrivalDate) = @MyDate
		AND
		IC.loyaltyNumber <> ''



GROUP BY	
			HR.code,
			CASE WHEN MRT.secondarySource = 'iPrefer APP' then 'iPrefer APP'
				WHEN MRT.channelreportlabel IS NULL then 'Direct to Property'
				ELSE MRT.channelreportlabel END,
			CASE WHEN MRT.ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END,
			MRT.OptIn,
			MRT.status,
			CASE WHEN CC.ConfirmationNumber IS NULL then 'NO' ELSE 'YES' END,
			CASE WHEN ICH.hotelCode IS NULL then 'NO' ELSE 'YES' END,
			CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < MRT.confirmationDate THEN 'NO' ELSE 'YES' END,
			MONTH(MRT.arrivalDate);
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[ips_iPreferScorecardBookingData]'
GO





CREATE PROCEDURE [rpt].[ips_iPreferScorecardBookingData] 
	@MyDate INT
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardBookingData.sql
--DECLARE @MyDate INT = 2017

DECLARE @IPreferCharges TABLE(	earliestBooking DATETIME,
								Membership_Number NVARCHAR(6)
							);

INSERT INTO @IPreferCharges
SELECT	MIN(billableDate) as earliestbooking, loyaltyNumber
FROM	ReservationBilling.dbo.Charges
WHERE	billableDate >= '2017-01-01'
AND		classificationID = 5
GROUP BY	loyaltyNumber
ORDER BY loyaltyNumber;


SELECT	HR.code,
		COUNT(*) as bookings,
		SUM(MRT.reservationRevenueUSD) as hotelrevenue,
		CASE WHEN MRT.secondarySource = 'iPrefer APP' then 'iPrefer APP' 
			WHEN MRT.channelreportlabel IS NULL then 'Direct to Property'
			ELSE MRT.channelreportlabel END as bookedchannel,
		SUM(CASE WHEN MRT.channelreportlabel = 'IBE - PHG' then (MRT.reservationRevenueUSD * 0.12) WHEN MRT.channelreportlabel = 'Voice - PHG' THEN (MRT.reservationRevenueUSD * 0.15) ELSE (MRT.reservationRevenueUSD *0.025) END)as phgprojectedrevenue,
		CASE WHEN MRT.ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END AS MemberRate,
		ISNULL(MRT.OptIn, 'False') as Checkbox,
		ISNULL(MRT.status, 'Confirmed') as bookingStatus,
		CASE WHEN HD.ConfirmationNumber IS NULL then 'NO' ELSE 'YES' END AS MarketingCampaign,
		CASE WHEN ICH.hotelCode IS NULL then 'NO' ELSE 'YES' END as CheckboxHotel,
		CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < MRT.confirmationDate THEN 'NO' ELSE 'YES' END as FirstBooking,
		AVG(DATEDIFF(day,MRT.confirmationDate, MRT.arrivalDate)) as averageleadtime,
		MONTH(MRT.confirmationDate) as bookedmonth

FROM	Reservations.dbo.mostrecenttransactionsreporting MRT
		JOIN
		Hotels.dbo.hotelsReporting HR
		ON
		HR.SynXisID = MRT.hotelid
		LEFT OUTER JOIN
		Hotels.authority.IPrefer_Checkbox ICH
		ON
		HR.code = ICH.hotelCode
		LEFT OUTER JOIN
		@IPreferCharges IPC
		ON
		MRT.loyaltyNumber = IPC.Membership_Number
		JOIN
		Loyalty.dbo.Customer_Profile_Import CPI
		ON
		MRT.loyaltyNumber = CPI.iPrefer_Number
		LEFT OUTER JOIN
		Marketing.crafted.Conversion HD
		ON
		MRT.confirmationNumber = HD.ConfirmationNumber

WHERE	YEAR(MRT.confirmationDate) = @MyDate
		AND
		MRT.loyaltyNumber <> ''

GROUP BY	HR.code,
			CASE WHEN MRT.secondarySource = 'iPrefer APP' then 'iPrefer APP' 
				WHEN MRT.channelreportlabel IS NULL then 'Direct to Property'
				ELSE MRT.channelreportlabel END,
			CASE WHEN MRT.ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END,
			MRT.OptIn,
			MRT.status,
			CASE WHEN HD.ConfirmationNumber IS NULL then 'NO' ELSE 'YES' END,
			CASE WHEN ICH.hotelCode IS NULL then 'NO' ELSE 'YES' END,
			CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < MRT.confirmationDate THEN 'NO' ELSE 'YES' END,
			MONTH(MRT.confirmationDate);
	
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[ips_iPreferScorecardHotelWebsiteBookings]'
GO




CREATE PROCEDURE [rpt].[ips_iPreferScorecardHotelWebsiteBookings] 
	@MyDate INT
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardHotelWebsiteBookings.sql
--DECLARE @MyDate INT = 2017

SELECT	HR.code,
		MRT.status as bookingStatus,
		COUNT(*) as totalHotelWebsiteBookings, 
		SUM(MRT.reservationRevenueUSD) as hotelrevenue,
		CASE WHEN ICH.hotelCode IS NULL then 'NO' ELSE 'YES' END as CheckboxHotel,
		MRT.optIn,
		MONTH(MRT.confirmationDate) as bookingmonth

FROM	Reservations.dbo.mostrecenttransactionsreporting MRT
		JOIN
		Hotels.dbo.hotelsReporting HR
		ON
		HR.synxisID = MRT.hotelID
		LEFT OUTER JOIN
		Hotels.authority.IPrefer_Checkbox ICH
		ON
		HR.code = ICH.hotelCode

WHERE	YEAR(MRT.confirmationDate) = @MyDate
		AND
		MRT.ChannelReportLabel = 'IBE - Hotel'

GROUP BY	HR.code,
			MRT.status,
			CASE WHEN ICH.hotelCode IS NULL then 'NO' ELSE 'YES' END,
			MRT.optIn,
			MONTH(MRT.confirmationDate)
	
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[ips_iPreferScorecardHotelInfo]'
GO


CREATE PROCEDURE [rpt].[ips_iPreferScorecardHotelInfo] 
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardHotelInfo.sql
select 
	code, 
	hotelname, 
	statuscodename as type,
	phg_regionalmanageridname as RD,
	CASE WHEN ipc.hotelCode IS NULL THEN 'NO' ELSE 'YES' END AS CheckboxHotel
FROM
	Hotels.dbo.hotelsreporting
LEFT OUTER JOIN
	Hotels.authority.IPrefer_Checkbox  ipc
ON
	hotelsreporting.code = ipc.hotelCode
WHERE
	statuscodename in ('Member Hotel','Former Member', 'Renewal Hotel');
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
