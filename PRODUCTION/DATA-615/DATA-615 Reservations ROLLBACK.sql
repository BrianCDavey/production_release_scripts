USE Reservations
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-ST-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/27/2020 10:20:35 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ips_iPreferScorecardHotelInfo]'
GO
DROP PROCEDURE [rpt].[ips_iPreferScorecardHotelInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ips_iPreferScorecardHotelWebsiteBookings]'
GO
DROP PROCEDURE [rpt].[ips_iPreferScorecardHotelWebsiteBookings]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ips_iPreferScorecardBookingData]'
GO
DROP PROCEDURE [rpt].[ips_iPreferScorecardBookingData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[ips_iPreferScorecardArrivalData]'
GO
DROP PROCEDURE [rpt].[ips_iPreferScorecardArrivalData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[iPreferProduction]'
GO
DROP PROCEDURE [rpt].[iPreferProduction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[iPreferFraudReport]'
GO
DROP PROCEDURE [rpt].[iPreferFraudReport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[iPreferBillingReconciliation]'
GO
DROP PROCEDURE [rpt].[iPreferBillingReconciliation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [rpt].[iPreferAlliancePoints]'
GO
DROP PROCEDURE [rpt].[iPreferAlliancePoints]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
