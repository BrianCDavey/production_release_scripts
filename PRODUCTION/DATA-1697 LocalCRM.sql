USE LocalCRM
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.LocalCRM    -  This database will be modified

to synchronize it with:

        (local)\WAREHOUSE.LocalCRM

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 8/5/2021 5:37:24 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [staging].[Account2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [staging].[Account2] ADD
[phg_pegasusrt4id] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Account2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Account2] ADD
[phg_pegasusrt4id] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[crm_synchronize_Account2]'
GO






ALTER PROCEDURE [dbo].[crm_synchronize_Account2]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM staging.Account2)
	BEGIN
		MERGE INTO dbo.Account2 AS tgt
		USING (SELECT * FROM staging.Account2) AS src ON src.Accountid = tgt.Accountid
		WHEN MATCHED THEN
			UPDATE
				SET [accountid] = src.[accountid],
					[phg_globalsalesadministrator] = src.[phg_globalsalesadministrator],
					[phg_globalsalesadministratorname] = src.[phg_globalsalesadministratorname],
					[phg_globalsalesadministratoryominame] = src.[phg_globalsalesadministratoryominame],
					[phg_reportingcurrencyid] = src.[phg_reportingcurrencyid],
					[phg_reportingcurrencyidname] = src.[phg_reportingcurrencyidname],
					phg_ipreferquarterlyenrollmentgoal = src.phg_ipreferquarterlyenrollmentgoal,
					[phg_tacticalrevenuemanagement] = src.[phg_tacticalrevenuemanagement],
					[phg_tacticalrevenuemanagementname] = src.[phg_tacticalrevenuemanagementname],
					[phg_tacticalrevenuemanagementyominame] = src.[phg_tacticalrevenuemanagementyominame],
					[phg_pegasusrt4id] = src.[phg_pegasusrt4id]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([accountid],[phg_globalsalesadministrator],[phg_globalsalesadministratorname],[phg_globalsalesadministratoryominame],[phg_reportingcurrencyid],[phg_reportingcurrencyidname],phg_ipreferquarterlyenrollmentgoal,[phg_tacticalrevenuemanagement],[phg_tacticalrevenuemanagementname],[phg_tacticalrevenuemanagementyominame],[phg_pegasusrt4id])
			VALUES(src.[accountid],src.[phg_globalsalesadministrator],src.[phg_globalsalesadministratorname],src.[phg_globalsalesadministratoryominame],src.[phg_reportingcurrencyid],src.[phg_reportingcurrencyidname],src.phg_ipreferquarterlyenrollmentgoal,src.[phg_tacticalrevenuemanagement],src.[phg_tacticalrevenuemanagementname],src.[phg_tacticalrevenuemanagementyominame],src.[phg_pegasusrt4id])
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
