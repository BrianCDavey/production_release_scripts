USE Reservations
GO

/*
Run this script on:

        chi-lt-00032377.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 3:07:50 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_PointsExport]'
GO
DROP PROCEDURE [dbo].[Epsilon_PointsExport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_CurrencyExport]'
GO
DROP PROCEDURE [dbo].[Epsilon_CurrencyExport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_CertificatesExport]'
GO
DROP PROCEDURE [dbo].[Epsilon_CertificatesExport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_AdjustmentExport]'
GO
DROP PROCEDURE [dbo].[Epsilon_AdjustmentExport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_ReservationsExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_ReservationsExport]
 @startDate date, @endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--DECLARE @startDate date = '2019-06-01', @endDate date = '2019-06-30'

;WITH iopt AS (
  SELECT HotelID
      ,ISNULL([StartDate],'1900-01-01') as startDate
      ,ISNULL([EndDate],'9999-09-09') as endDate
  FROM [Hotels].[dbo].[Hotel_ProTools] hpt
  WHERE ProToolsID = 14
)
, jed AS (
		SELECT 
			t.confirmationNumber
			, 
			REPLACE(
			CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
				(
					SELECT
						ISNULL(t2.TransactionID,'') as PhgTransactionId
						, ISNULL(t2.confirmationNumber,'') as ConfirmationNumber
						, ISNULL(hh.SynXisID, hh.OpenHospID) as CrsHotelCode
						, ISNULL(xe.toUSD,0) as UsdExchangeRate
						, ISNULL(xe.rateDate,'9999-09-09') as UsdExchangeDate
						, CASE 
							WHEN t2.synXisID IS NOT NULL THEN 'Synxis' 
							WHEN t2.openHospID IS NOT NULL THEN 'Open Hospitality' 
							ELSE 'ERROR WITH CRS MAPPING' 
						  END as CrsSource
						, ISNULL(pc.PH_Channel,'') as BookingChannel
						, ISNULL(pbs.PH_ChannelID,0) as BookingChannelID
						, ISNULL(rc.RateCode,'') as RateCode
						, ISNULL(rc.RateName,'') as RateName
						, ISNULL(rcat.rateCategoryCode,'') as RateCategory
						, ISNULL(corp.corporationCode,'') as CorpCode
						, ISNULL(rt.roomTypeCode,'') as RoomCode
						, ISNULL(rt.roomTypeName,'') as RoomName
						, '' as RoomCategory
						, ISNULL(td.rooms,0) as RoomCount
						, ISNULL(td.totalGuestCount,0) as GuestCount
						, ISNULL(td.childrenCount,0) as ChildCount
						, ISNULL(t.itineraryNumber,0) as IteneraryNumber
						, ISNULL(i.IATANumber,'') as IataNumber
						, '' as PsuedoCityCode
						, ISNULL(ta.Name,'') as TravelAgencyName
						, '' as TravelAgentFirstName
						, '' as TravelAgentLastName
						, ISNULL(ta.Email,'') as TravelAgentEmailAddress
						, ISNULL(ts.creditCardType,'') as CreditCardType
						, ISNULL(ts.cancellationNumber,'') as CancellationNumber
						, ISNULL(ts.cancellationDate,'') as CancellationDate
						, ISNULL(g.FirstName,'') as GuestFirstName
						, ISNULL(g.LastName,'') as GuestLastName
						, ISNULL(ge.emailAddress,'') as GuestEmail
						, CASE WHEN iopt.HotelID IS NULL THEN '' ELSE td.optIn END as LoyaltyOptIn
						, CASE WHEN iopt.HotelID IS NULL THEN td.optIn ELSE '' END as MarketingOptIn
						, ISNULL(td.arrivalDate,'') as ReportDate
					FROM Reservations.dbo.Transactions t2
						LEFT JOIN Reservations.dbo.TransactionDetail td
							ON t2.TransactionDetailID = td.TransactionDetailID
						LEFT JOIN Reservations.dbo.TransactionStatus ts
							ON t2.TransactionStatusID = ts.TransactionStatusID
						LEFT JOIN Reservations.dbo.hotel h
							ON t2.HotelID = h.HotelID
						LEFT JOIN Hotels.dbo.Hotel hh
							ON h.Hotel_hotelID = hh.HotelID
						LEFT JOIN CurrencyRates.dbo.dailyRates xe
							ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
							AND td.currency = xe.code						
						LEFT JOIN Reservations.dbo.PH_BookingSource pbs
							ON t2.PH_BookingSourceID = pbs.PH_BookingSourceID
						LEFT JOIN Reservations.dbo.PH_Channel pc
							ON pbs.PH_ChannelID = pc.PH_ChannelID
						LEFT JOIN Reservations.dbo.RateCode rc
							ON t2.RateCodeID = rc.RateCodeID						
						LEFT JOIN Reservations.dbo.RateCategory rcat
							ON t2.RateCategoryID = rcat.RateCategoryID
						LEFT JOIN Reservations.dbo.CorporateCode corp
							ON t2.CorporateCodeID = corp.CorporateCodeID
						LEFT JOIN Reservations.dbo.RoomType rt
							ON t2.RoomTypeID = rt.RoomTypeID
						LEFT JOIN Reservations.dbo.IATANumber i
							ON t2.IATANumberID = i.IATANumberID
						LEFT JOIN Reservations.dbo.TravelAgent ta
							ON t2.TravelAgentID = ta.TravelAgentID
						LEFT JOIN Reservations.dbo.Guest g
							ON t2.GuestID = g.GuestID
						LEFT JOIN Reservations.dbo.Guest_EmailAddress ge
							ON g.Guest_EmailAddressID = ge.Guest_EmailAddressID
						LEFT JOIN iopt
							ON h.Hotel_hotelID = iopt.hotelId
							AND ts.confirmationDate BETWEEN iopt.startDate AND iopt.endDate
						
					WHERE t.confirmationNumber = t2.confirmationNumber
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				)
				,(
				SELECT * FROM
					(SELECT 
						ISNULL(tag.id,0) as TravelAgencyGroupID
						, ISNULL(tag.name,'') as TravelAgencyGroupName
					FROM Reservations.dbo.Transactions t3
						JOIN Reservations.dbo.TransactionStatus ts
							ON t3.TransactionStatusID = ts.TransactionStatusID
						JOIN Reservations.dbo.IATANumber i
							ON t3.IATANumberID = i.IATANumberID
						JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag
							ON i.IATANumber = taitag.travelAgentId
							AND ts.confirmationDate BETWEEN taitag.startDate and taitag.endDate
						JOIN Core.dbo.travelAgentGroups tag
							ON taitag.travelAgentGroupID = tag.id
					WHERE t.confirmationNumber = t3.confirmationNumber
					UNION ALL
					SELECT 
						0 as TravelAgencyGroupID
						, '' as TravelAgencyGroupName
					WHERE NOT EXISTS (SELECT 1
					FROM Reservations.dbo.Transactions t3
						JOIN Reservations.dbo.TransactionStatus ts
							ON t3.TransactionStatusID = ts.TransactionStatusID
						JOIN Reservations.dbo.IATANumber i
							ON t3.IATANumberID = i.IATANumberID
						JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag
							ON i.IATANumber = taitag.travelAgentId
							AND ts.confirmationDate BETWEEN taitag.startDate and taitag.endDate
						JOIN Core.dbo.travelAgentGroups tag
							ON taitag.travelAgentGroupID = tag.id
					WHERE t.confirmationNumber = t3.confirmationNumber
					)) as sub 
					FOR JSON PATH, ROOT('TravelAgencyGroups')
				)



				,(
				SELECT * FROM
					(SELECT DISTINCT ISNULL(rbs.sourceId,0) as BookingSourceID
								, ISNULL(rbs.sourceName,'') as BookingSourceName
						FROM Reservations.dbo.Transactions t4
						JOIN Reservations.dbo.TransactionDetail td
							ON t.TransactionDetailID = td.TransactionDetailID
						JOIN Reservations.dbo.CRS_BookingSource cbs
							ON t.CRS_BookingSourceID = cbs.BookingSourceID
						JOIN Reservations.dbo.CRS_Channel cc
							ON cbs.ChannelID = cc.ChannelID
						JOIN Reservations.dbo.CRS_SecondarySource c2s
							ON cbs.SecondarySourceID = c2s.SecondarySourceID
						JOIN Reservations.dbo.CRS_SubSource css
							ON cbs.SubSourceID = css.SubSourceID
						JOIN Reservations.dbo.ibeSource ibs
							ON cbs.ibeSourceNameID = ibs.ibeSourceID
						JOIN Reservations.authority.ibeSource aibs
							ON ibs.auth_ibeSourceID = aibs.ibeSourceID
						JOIN ReservationBilling.dbo.Templates rbt
							ON aibs.ibeSourceName = rbt.xbeTemplateName
						JOIN Reservations.dbo.CROCode cro
							ON cbs.CROCodeID = cro.CROCodeID
						JOIN Reservations.authority.CRO_Code acro
							ON cro.auth_CRO_CodeID = acro.CRO_CodeID
						JOIN ReservationBilling.dbo.CROCodes rbc
							ON acro.CRO_Code = rbc.croCode
						JOIN [ReservationBilling].[dbo].[Charges] ch
							ON t.confirmationNumber = ch.confirmationNumber
							AND ch.classificationID = 5
						JOIN ReservationBilling.dbo.Clauses cl
							ON ch.clauseID = cl.clauseID
						JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg
							ON cl.clauseID = cicg.clauseID
						JOIN ReservationBilling.dbo.CriteriaGroups cg
							ON cicg.criteriaGroupID = cg.criteriaGroupID
						JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
							ON cg.criteriaGroupID = cgic.criteriaGroupID
						JOIN ReservationBilling.dbo.Criteria c
							ON cgic.criteriaID = c.criteriaID
						JOIN ReservationBilling.dbo.Criteria_SourceGroups csg
							ON c.criteriaID = csg.criteriaID
						JOIN ReservationBilling.dbo.SourceGroups sg
							ON csg.sourceGroupID = sg.sourceGroupID
						JOIN ReservationBilling.dbo.SourceGroups_IncludeSources sgis
							ON sg.sourceGroupID = sgis.sourceGroupID
						JOIN ReservationBilling.dbo.Sources rbs
						ON sgis.sourceID = rbs.sourceId
						AND (rbs.channel = '*' OR rbs.channel = cc.channel)
						AND (rbs.secondarySource = '*' OR rbs.secondarySource = c2s.secondarySource)
						AND (rbs.subsource = '*' OR rbs.subsource = css.subSourceCode)
						AND (rbs.templateGroupID = 0 OR rbs.templateGroupID = rbt.templateGroupID)
						AND (rbs.croGroupID = 0 OR rbs.croGroupID = rbc.croGroupID)

					WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5
					UNION ALL
						SELECT 0 as BookingSourceID
							, '' as BookingSourceName
						WHERE NOT EXISTS ( SELECT 1
						FROM Reservations.dbo.Transactions t4
						JOIN Reservations.dbo.TransactionDetail td
							ON t.TransactionDetailID = td.TransactionDetailID
						JOIN Reservations.dbo.CRS_BookingSource cbs
							ON t.CRS_BookingSourceID = cbs.BookingSourceID
						JOIN Reservations.dbo.CRS_Channel cc
							ON cbs.ChannelID = cc.ChannelID
						JOIN Reservations.dbo.CRS_SecondarySource c2s
							ON cbs.SecondarySourceID = c2s.SecondarySourceID
						JOIN Reservations.dbo.CRS_SubSource css
							ON cbs.SubSourceID = css.SubSourceID
						JOIN Reservations.dbo.ibeSource ibs
							ON cbs.ibeSourceNameID = ibs.ibeSourceID
						JOIN Reservations.authority.ibeSource aibs
							ON ibs.auth_ibeSourceID = aibs.ibeSourceID
						JOIN ReservationBilling.dbo.Templates rbt
							ON aibs.ibeSourceName = rbt.xbeTemplateName
						JOIN Reservations.dbo.CROCode cro
							ON cbs.CROCodeID = cro.CROCodeID
						JOIN Reservations.authority.CRO_Code acro
							ON cro.auth_CRO_CodeID = acro.CRO_CodeID
						JOIN ReservationBilling.dbo.CROCodes rbc
							ON acro.CRO_Code = rbc.croCode
						JOIN [ReservationBilling].[dbo].[Charges] ch
							ON t.confirmationNumber = ch.confirmationNumber
							AND ch.classificationID = 5
						JOIN ReservationBilling.dbo.Clauses cl
							ON ch.clauseID = cl.clauseID
						JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg
							ON cl.clauseID = cicg.clauseID
						JOIN ReservationBilling.dbo.CriteriaGroups cg
							ON cicg.criteriaGroupID = cg.criteriaGroupID
						JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
							ON cg.criteriaGroupID = cgic.criteriaGroupID
						JOIN ReservationBilling.dbo.Criteria c
							ON cgic.criteriaID = c.criteriaID
						JOIN ReservationBilling.dbo.Criteria_SourceGroups csg
							ON c.criteriaID = csg.criteriaID
						JOIN ReservationBilling.dbo.SourceGroups sg
							ON csg.sourceGroupID = sg.sourceGroupID
						JOIN ReservationBilling.dbo.SourceGroups_IncludeSources sgis
							ON sg.sourceGroupID = sgis.sourceGroupID
						JOIN ReservationBilling.dbo.Sources rbs
						ON sgis.sourceID = rbs.sourceId
						AND (rbs.channel = '*' OR rbs.channel = cc.channel)
						AND (rbs.secondarySource = '*' OR rbs.secondarySource = c2s.secondarySource)
						AND (rbs.subsource = '*' OR rbs.subsource = css.subSourceCode)
						AND (rbs.templateGroupID = 0 OR rbs.templateGroupID = rbt.templateGroupID)
						AND (rbs.croGroupID = 0 OR rbs.croGroupID = rbc.croGroupID)

					WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5
					)) as sub 
					FOR JSON PATH, ROOT('BookingSources')
				)
			),'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',')  as jsonExternalData
		FROM Reservations.dbo.Transactions t
			JOIN Reservations.dbo.TransactionDetail td
				ON t.TransactionDetailID = td.TransactionDetailID
		WHERE td.arrivalDate BETWEEN @startDate AND @endDate
	)


SELECT
	'01' as [01],
	t.confirmationNumber AS RESERVATION_NUMBER,
	NULL AS ACT_RESERVATION_ID,
	NULL AS PROFILE_ID,
	l.loyaltyNumber AS CARD_NUMBER,
	CASE 
		WHEN act.actionType LIKE '%N%' THEN 'PR'  
		WHEN act.actionType LIKE '%M%' THEN 'UI'
		WHEN act.actionType LIKE '%X%' THEN 'VD' 
		ELSE 'PR'
	END AS TRANSACTION_TYPE_CODE,
	'Migrated Transaction' AS RESERVATION_DESCRIPTION,
	td.arrivalDate AS ARRIVAL_DATE,
	td.departureDate AS DEPARTURE_DATE,
	td.reservationRevenue AS RESERVATION_REVENUE,
	NULL AS ELIGIBLE_REVENUE,
	NULL AS TAX_AMOUNT,
	NULL AS POST_SALES_ADJUSTMNT_AMT,
	NULL AS SHIPPING_HANDLING,
	NULL AS STATUS,
	NULL AS DEVICE_ID,
	NULL AS DEVICE_USERID,
	NULL AS AUTHORIZATION_CODE,
	CASE WHEN act.actionType NOT LIKE '%N%' THEN t.confirmationNumber ELSE NULL END AS ORIGINAL_RESERVATION_NUMBER,
	'True' AS SKIP_MOMENT_ENGINE_IND,
	td.currency AS CURRENCY_CODE,
	'Preferred Hotel Group' AS ACCT_SRC_CODE,
	NULL AS SRC_ACCOUNT_NUM,
	t.transactionTimeStamp AS ACTIVITY_DATE,
	t.QueueID AS CLIENT_FILE_ID,
	NULL AS CLIENT_FILE_REC_NUM,
	NULL AS BRAND_ORG_CODE,
	NULL AS ASSOCIATE_NUM,
	td.reservationRevenue AS GROSS_AMOUNT,
	td.reservationRevenue AS NET_AMT,
	td.reservationRevenue * xe.toUSD AS PROG_CURR_GROSS_AMT,
	NULL AS TXN_SEQ_NUM,
	'PHG File' AS TXN_SOURCE_CODE,
	NULL AS TXN_SUBTYPE_CODE,
	NULL AS TIER,
	hh.HotelCode AS TRANS_LOCATION,
	NULL AS [ ], --intentionally left blank per spec
	'PHG' AS PROGRAM_CODE,
	NULL AS EXCHANGE_RATE_ID,
	NULL AS GRATUITY,
	NULL AS SRC_CUSTOMER_ID,
	NULL AS DISC_AMT,
	NULL AS RESERVATION_CHANNEL_CODE,
	ts.confirmationDate AS BOOKING_DATE,
	promo.promotionalCode AS PROMO_CODE,
	NULL AS BUYER_TYPE_CODE,
	NULL AS RMB_METHOD_CODE,
	NULL AS RMB_VENDOR_CODE,
	NULL AS RMB_DATE,
	NULL AS [  ], --intentionally left blank per spec
	reservationRevenue * xe.toUSD AS PROG_CURR_RES_TOTAL,
	NULL AS PROG_CURR_TAX_AMT,
	NULL AS PROG_CURR_POST_SALES_ADJ_AMT,
	NULL AS PROG_CURR_SHIPPING_HANDLING,
	reservationRevenue * xe.toUSD AS PROG_CURR_ELIG_REVENUE,
	NULL AS PROG_CURR_GRATUITY,
	NULL AS TXN_TYPE_CODE,
	CASE WHEN act.actionType NOT LIKE '%N%' THEN hh.hotelCode ELSE NULL END AS ORIGINAL_STORE_CODE, 
	td.arrivalDate AS ORIGINAL_RESERVATION_ARRIVAL,
	td.departureDate AS ORIGINAL_RESERVATION_DEPARTURE,
	NULL AS SUSPEND_REASON,
	NULL AS SUSPEND_RESERVATION,
	NULL AS POSTING_KEY_CODE,
	NULL AS POSTING_KEY_VALUE,
	REPLACE(REPLACE(
	--REPLACE(	
	jed.jsonExternalData
	--,'"','""')
	,CHAR(10),''),CHAR(13),'') AS JSON_EXTERNAL_DATA,
	CASE 
		WHEN t.synXisID IS NOT NULL THEN 'Synxis' 
		WHEN t.openHospID IS NOT NULL THEN 'Open Hospitality' 
		ELSE 'ERROR WITH CRS MAPPING' 
	END as CRS_SOURCE
	, CASE WHEN LoyaltyNumberValidated = 1 THEN 'PHG' ELSE 'EPS' END as CARD_NUMBER_SRC
	, g.FirstName as GUEST_FIRST_NAME
	, g.LastName as GUEST_LAST_NAME
	, ge.emailAddress as GUEST_EMAIL_ADDRESS

 FROM Reservations.dbo.Transactions t
	JOIN Reservations.dbo.TransactionDetail td
		ON t.TransactionDetailID = td.TransactionDetailID
	JOIN Reservations.dbo.TransactionStatus ts
		ON t.TransactionStatusID = ts.TransactionStatusID
	JOIN Reservations.dbo.ActionType act
		ON ts.ActionTypeID = act.ActionTypeID
	LEFT JOIN Reservations.dbo.LoyaltyNumber l
		ON t.LoyaltyNumberID = l.LoyaltyNumberID
	JOIN CurrencyRates.dbo.dailyRates xe
		ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
		AND td.currency = xe.code
	JOIN Reservations.dbo.hotel h
		ON t.HotelID = h.HotelID
	JOIN Hotels.dbo.hotel hh
		ON h.Hotel_hotelID = hh.HotelID
	JOIN Reservations.dbo.PH_BookingSource pbs
		ON t.PH_BookingSourceID = pbs.PH_BookingSourceID
	JOIN Reservations.dbo.PH_Channel pc
		ON pbs.PH_ChannelID = pc.PH_ChannelID
	JOIN Reservations.dbo.PH_SecondaryChannel psc
		ON pbs.PH_SecondaryChannelID = psc.PH_SecondaryChannelID
	JOIN Reservations.dbo.RateCode rc
		ON t.RateCodeID = rc.RateCodeID
	LEFT JOIN Reservations.dbo.Guest g
		ON t.GuestID = g.GuestID
	LEFT JOIN Reservations.dbo.Guest_EmailAddress ge
		ON g.Guest_EmailAddressID = ge.Guest_EmailAddressID
	LEFT JOIN Reservations.dbo.PromoCode promo
		ON t.PromoCodeID = promo.PromoCodeID
	LEFT JOIN jed
		ON t.confirmationNumber = jed.confirmationNumber
	WHERE td.arrivalDate BETWEEN @startDate AND @endDate	

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_TransactionsExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_TransactionsExport]
 @startDate date, @endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--DECLARE @startDate date = '2019-06-01', @endDate date = '2019-06-30'

;WITH iopt AS (
  SELECT HotelID
      ,ISNULL([StartDate],'1900-01-01') as startDate
      ,ISNULL([EndDate],'9999-09-09') as endDate
  FROM [Hotels].[dbo].[Hotel_ProTools] hpt
  WHERE ProToolsID = 14
)
, jed AS (
		SELECT 
			t.confirmationNumber
			, 
			REPLACE(
			CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
				(
					SELECT
						ISNULL(t2.TransactionID,'') as PhgTransactionId
						, ISNULL(t2.confirmationNumber,'') as ConfirmationNumber
						, ISNULL(hh.SynXisID, hh.OpenHospID) as CrsHotelCode
						, ISNULL(xe.toUSD,0) as UsdExchangeRate
						, ISNULL(xe.rateDate,'9999-09-09') as UsdExchangeDate
						, CASE 
							WHEN t2.synXisID IS NOT NULL THEN 'Synxis' 
							WHEN t2.openHospID IS NOT NULL THEN 'Open Hospitality' 
							ELSE 'ERROR WITH CRS MAPPING' 
						  END as CrsSource
						, ISNULL(pc.PH_Channel,'') as BookingChannel
						, ISNULL(pbs.PH_ChannelID,0) as BookingChannelID
						, ISNULL(rc.RateCode,'') as RateCode
						, ISNULL(rc.RateName,'') as RateName
						, ISNULL(rcat.rateCategoryCode,'') as RateCategory
						, ISNULL(corp.corporationCode,'') as CorpCode
						, ISNULL(rt.roomTypeCode,'') as RoomCode
						, ISNULL(rt.roomTypeName,'') as RoomName
						, '' as RoomCategory
						, ISNULL(td.rooms,0) as RoomCount
						, ISNULL(td.totalGuestCount,0) as GuestCount
						, ISNULL(td.childrenCount,0) as ChildCount
						, ISNULL(t.itineraryNumber,0) as IteneraryNumber
						, ISNULL(i.IATANumber,'') as IataNumber
						, '' as PsuedoCityCode
						, ISNULL(ta.Name,'') as TravelAgencyName
						, '' as TravelAgentFirstName
						, '' as TravelAgentLastName
						, ISNULL(ta.Email,'') as TravelAgentEmailAddress
						, ISNULL(ts.creditCardType,'') as CreditCardType
						, ISNULL(ts.cancellationNumber,'') as CancellationNumber
						, ISNULL(ts.cancellationDate,'') as CancellationDate
						, ISNULL(g.FirstName,'') as GuestFirstName
						, ISNULL(g.LastName,'') as GuestLastName
						, ISNULL(ge.emailAddress,'') as GuestEmail
						, CASE WHEN iopt.HotelID IS NULL THEN '' ELSE td.optIn END as LoyaltyOptIn
						, CASE WHEN iopt.HotelID IS NULL THEN td.optIn ELSE '' END as MarketingOptIn
						, ISNULL(td.arrivalDate,'') as ReportDate
					FROM Reservations.dbo.Transactions t2
						LEFT JOIN Reservations.dbo.TransactionDetail td
							ON t2.TransactionDetailID = td.TransactionDetailID
						LEFT JOIN Reservations.dbo.TransactionStatus ts
							ON t2.TransactionStatusID = ts.TransactionStatusID
						LEFT JOIN Reservations.dbo.hotel h
							ON t2.HotelID = h.HotelID
						LEFT JOIN Hotels.dbo.Hotel hh
							ON h.Hotel_hotelID = hh.HotelID
						LEFT JOIN CurrencyRates.dbo.dailyRates xe
							ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
							AND td.currency = xe.code						
						LEFT JOIN Reservations.dbo.PH_BookingSource pbs
							ON t2.PH_BookingSourceID = pbs.PH_BookingSourceID
						LEFT JOIN Reservations.dbo.PH_Channel pc
							ON pbs.PH_ChannelID = pc.PH_ChannelID
						LEFT JOIN Reservations.dbo.RateCode rc
							ON t2.RateCodeID = rc.RateCodeID						
						LEFT JOIN Reservations.dbo.RateCategory rcat
							ON t2.RateCategoryID = rcat.RateCategoryID
						LEFT JOIN Reservations.dbo.CorporateCode corp
							ON t2.CorporateCodeID = corp.CorporateCodeID
						LEFT JOIN Reservations.dbo.RoomType rt
							ON t2.RoomTypeID = rt.RoomTypeID
						LEFT JOIN Reservations.dbo.IATANumber i
							ON t2.IATANumberID = i.IATANumberID
						LEFT JOIN Reservations.dbo.TravelAgent ta
							ON t2.TravelAgentID = ta.TravelAgentID
						LEFT JOIN Reservations.dbo.Guest g
							ON t2.GuestID = g.GuestID
						LEFT JOIN Reservations.dbo.Guest_EmailAddress ge
							ON g.Guest_EmailAddressID = ge.Guest_EmailAddressID
						LEFT JOIN iopt
							ON h.Hotel_hotelID = iopt.hotelId
							AND ts.confirmationDate BETWEEN iopt.startDate AND iopt.endDate
						
					WHERE t.confirmationNumber = t2.confirmationNumber
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				)
				,(
				SELECT * FROM
					(SELECT 
						ISNULL(tag.id,0) as TravelAgencyGroupID
						, ISNULL(tag.name,'') as TravelAgencyGroupName
					FROM Reservations.dbo.Transactions t3
						JOIN Reservations.dbo.TransactionStatus ts
							ON t3.TransactionStatusID = ts.TransactionStatusID
						JOIN Reservations.dbo.IATANumber i
							ON t3.IATANumberID = i.IATANumberID
						JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag
							ON i.IATANumber = taitag.travelAgentId
							AND ts.confirmationDate BETWEEN taitag.startDate and taitag.endDate
						JOIN Core.dbo.travelAgentGroups tag
							ON taitag.travelAgentGroupID = tag.id
					WHERE t.confirmationNumber = t3.confirmationNumber
					UNION ALL
					SELECT 
						0 as TravelAgencyGroupID
						, '' as TravelAgencyGroupName
					WHERE NOT EXISTS (SELECT 1
					FROM Reservations.dbo.Transactions t3
						JOIN Reservations.dbo.TransactionStatus ts
							ON t3.TransactionStatusID = ts.TransactionStatusID
						JOIN Reservations.dbo.IATANumber i
							ON t3.IATANumberID = i.IATANumberID
						JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag
							ON i.IATANumber = taitag.travelAgentId
							AND ts.confirmationDate BETWEEN taitag.startDate and taitag.endDate
						JOIN Core.dbo.travelAgentGroups tag
							ON taitag.travelAgentGroupID = tag.id
					WHERE t.confirmationNumber = t3.confirmationNumber
					)) as sub 
					FOR JSON PATH, ROOT('TravelAgencyGroups')
				)



				,(
				SELECT * FROM
					(SELECT DISTINCT ISNULL(rbs.sourceId,0) as BookingSourceID
								, ISNULL(rbs.sourceName,'') as BookingSourceName
						FROM Reservations.dbo.Transactions t4
						JOIN Reservations.dbo.TransactionDetail td
							ON t.TransactionDetailID = td.TransactionDetailID
						JOIN Reservations.dbo.CRS_BookingSource cbs
							ON t.CRS_BookingSourceID = cbs.BookingSourceID
						JOIN Reservations.dbo.CRS_Channel cc
							ON cbs.ChannelID = cc.ChannelID
						JOIN Reservations.dbo.CRS_SecondarySource c2s
							ON cbs.SecondarySourceID = c2s.SecondarySourceID
						JOIN Reservations.dbo.CRS_SubSource css
							ON cbs.SubSourceID = css.SubSourceID
						JOIN Reservations.dbo.ibeSource ibs
							ON cbs.ibeSourceNameID = ibs.ibeSourceID
						JOIN Reservations.authority.ibeSource aibs
							ON ibs.auth_ibeSourceID = aibs.ibeSourceID
						JOIN ReservationBilling.dbo.Templates rbt
							ON aibs.ibeSourceName = rbt.xbeTemplateName
						JOIN Reservations.dbo.CROCode cro
							ON cbs.CROCodeID = cro.CROCodeID
						JOIN Reservations.authority.CRO_Code acro
							ON cro.auth_CRO_CodeID = acro.CRO_CodeID
						JOIN ReservationBilling.dbo.CROCodes rbc
							ON acro.CRO_Code = rbc.croCode
						JOIN [ReservationBilling].[dbo].[Charges] ch
							ON t.confirmationNumber = ch.confirmationNumber
							AND ch.classificationID = 5
						JOIN ReservationBilling.dbo.Clauses cl
							ON ch.clauseID = cl.clauseID
						JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg
							ON cl.clauseID = cicg.clauseID
						JOIN ReservationBilling.dbo.CriteriaGroups cg
							ON cicg.criteriaGroupID = cg.criteriaGroupID
						JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
							ON cg.criteriaGroupID = cgic.criteriaGroupID
						JOIN ReservationBilling.dbo.Criteria c
							ON cgic.criteriaID = c.criteriaID
						JOIN ReservationBilling.dbo.Criteria_SourceGroups csg
							ON c.criteriaID = csg.criteriaID
						JOIN ReservationBilling.dbo.SourceGroups sg
							ON csg.sourceGroupID = sg.sourceGroupID
						JOIN ReservationBilling.dbo.SourceGroups_IncludeSources sgis
							ON sg.sourceGroupID = sgis.sourceGroupID
						JOIN ReservationBilling.dbo.Sources rbs
						ON sgis.sourceID = rbs.sourceId
						AND (rbs.channel = '*' OR rbs.channel = cc.channel)
						AND (rbs.secondarySource = '*' OR rbs.secondarySource = c2s.secondarySource)
						AND (rbs.subsource = '*' OR rbs.subsource = css.subSourceCode)
						AND (rbs.templateGroupID = 0 OR rbs.templateGroupID = rbt.templateGroupID)
						AND (rbs.croGroupID = 0 OR rbs.croGroupID = rbc.croGroupID)

					WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5
					UNION ALL
						SELECT 0 as BookingSourceID
							, '' as BookingSourceName
						WHERE NOT EXISTS ( SELECT 1
						FROM Reservations.dbo.Transactions t4
						JOIN Reservations.dbo.TransactionDetail td
							ON t.TransactionDetailID = td.TransactionDetailID
						JOIN Reservations.dbo.CRS_BookingSource cbs
							ON t.CRS_BookingSourceID = cbs.BookingSourceID
						JOIN Reservations.dbo.CRS_Channel cc
							ON cbs.ChannelID = cc.ChannelID
						JOIN Reservations.dbo.CRS_SecondarySource c2s
							ON cbs.SecondarySourceID = c2s.SecondarySourceID
						JOIN Reservations.dbo.CRS_SubSource css
							ON cbs.SubSourceID = css.SubSourceID
						JOIN Reservations.dbo.ibeSource ibs
							ON cbs.ibeSourceNameID = ibs.ibeSourceID
						JOIN Reservations.authority.ibeSource aibs
							ON ibs.auth_ibeSourceID = aibs.ibeSourceID
						JOIN ReservationBilling.dbo.Templates rbt
							ON aibs.ibeSourceName = rbt.xbeTemplateName
						JOIN Reservations.dbo.CROCode cro
							ON cbs.CROCodeID = cro.CROCodeID
						JOIN Reservations.authority.CRO_Code acro
							ON cro.auth_CRO_CodeID = acro.CRO_CodeID
						JOIN ReservationBilling.dbo.CROCodes rbc
							ON acro.CRO_Code = rbc.croCode
						JOIN [ReservationBilling].[dbo].[Charges] ch
							ON t.confirmationNumber = ch.confirmationNumber
							AND ch.classificationID = 5
						JOIN ReservationBilling.dbo.Clauses cl
							ON ch.clauseID = cl.clauseID
						JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg
							ON cl.clauseID = cicg.clauseID
						JOIN ReservationBilling.dbo.CriteriaGroups cg
							ON cicg.criteriaGroupID = cg.criteriaGroupID
						JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
							ON cg.criteriaGroupID = cgic.criteriaGroupID
						JOIN ReservationBilling.dbo.Criteria c
							ON cgic.criteriaID = c.criteriaID
						JOIN ReservationBilling.dbo.Criteria_SourceGroups csg
							ON c.criteriaID = csg.criteriaID
						JOIN ReservationBilling.dbo.SourceGroups sg
							ON csg.sourceGroupID = sg.sourceGroupID
						JOIN ReservationBilling.dbo.SourceGroups_IncludeSources sgis
							ON sg.sourceGroupID = sgis.sourceGroupID
						JOIN ReservationBilling.dbo.Sources rbs
						ON sgis.sourceID = rbs.sourceId
						AND (rbs.channel = '*' OR rbs.channel = cc.channel)
						AND (rbs.secondarySource = '*' OR rbs.secondarySource = c2s.secondarySource)
						AND (rbs.subsource = '*' OR rbs.subsource = css.subSourceCode)
						AND (rbs.templateGroupID = 0 OR rbs.templateGroupID = rbt.templateGroupID)
						AND (rbs.croGroupID = 0 OR rbs.croGroupID = rbc.croGroupID)

					WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5
					)) as sub 
					FOR JSON PATH, ROOT('BookingSources')
				)
			),'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',')  as jsonExternalData
		FROM Reservations.dbo.Transactions t
			JOIN Reservations.dbo.TransactionDetail td
				ON t.TransactionDetailID = td.TransactionDetailID
		WHERE td.arrivalDate BETWEEN @startDate AND @endDate
	)


SELECT
	'01' as [01],
	t.confirmationNumber AS TRANSACTION_NUMBER,
	NULL AS ACT_TRANSACTION_ID,
	NULL AS PROFILE_ID,
	l.loyaltyNumber AS CARD_NUMBER,
	CASE 
		WHEN act.actionType LIKE '%N%' THEN 'PR'  
		WHEN act.actionType LIKE '%M%' THEN 'UI'
		WHEN act.actionType LIKE '%X%' THEN 'VD' 
		ELSE 'PR'
	END AS TRANSACTION_TYPE_CODE,
	'Migrated Transaction' AS TRANS_DESCRIPTION,
	td.arrivalDate AS TRANSACTION_DATE,
	td.departureDate AS END_DATE,
	td.reservationRevenue AS TRANSACTION_NET_AMOUNT,
	NULL AS ELIGIBLE_REVENUE,
	NULL AS TAX_AMOUNT,
	NULL AS POST_SALES_ADJUSTMNT_AMT,
	NULL AS SHIPPING_HANDLING,
	NULL AS STATUS,
	NULL AS DEVICE_ID,
	NULL AS DEVICE_USERID,
	NULL AS AUTHORIZATION_CODE,
	CASE WHEN act.actionType NOT LIKE '%N%' THEN t.confirmationNumber ELSE NULL END AS ORIGINAL_TRANSACTION_NUMBER,
	'True' AS SKIP_MOMENT_ENGINE_IND,
	td.currency AS CURRENCY_CODE,
	'Preferred Hotel Group' AS ACCT_SRC_CODE,
	NULL AS SRC_ACCOUNT_NUM,
	t.transactionTimeStamp AS ACTIVITY_DATE,
	t.QueueID AS CLIENT_FILE_ID,
	NULL AS CLIENT_FILE_REC_NUM,
	NULL AS BRAND_ORG_CODE,
	NULL AS ASSOCIATE_NUM,
	td.reservationRevenue AS GROSS_AMOUNT,
	td.reservationRevenue AS NET_AMT,
	td.reservationRevenue * xe.toUSD AS PROG_CURR_GROSS_AMT,
	NULL AS TXN_SEQ_NUM,
	'PHG File' AS TXN_SOURCE_CODE,
	NULL AS TXN_SUBTYPE_CODE,
	NULL AS TIER,
	hh.HotelCode AS TRANS_LOCATION,
	NULL AS [ ], --intentionally left blank per spec
	'PHG' AS PROGRAM_CODE,
	NULL AS EXCHANGE_RATE_ID,
	NULL AS GRATUITY,
	NULL AS SRC_CUSTOMER_ID,
	NULL AS DISC_AMT,
	NULL AS TXN_CHANNEL_CODE,
	ts.confirmationDate AS TXN_BUSINESS_DATE,
	promo.promotionalCode AS PROMO_CODE,
	NULL AS BUYER_TYPE_CODE,
	NULL AS RMB_METHOD_CODE,
	NULL AS RMB_VENDOR_CODE,
	NULL AS RMB_DATE,
	NULL AS [  ], --intentionally left blank per spec
	reservationRevenue * xe.toUSD AS PROG_CURR_NET_AMT,
	NULL AS PROG_CURR_TAX_AMT,
	NULL AS PROG_CURR_POST_SALES_ADJ_AMT,
	NULL AS PROG_CURR_SHIPPING_HANDLING,
	reservationRevenue * xe.toUSD AS PROG_CURR_ELIG_REVENUE,
	NULL AS PROG_CURR_GRATUITY,
	NULL AS TXN_TYPE_CODE,
	CASE WHEN act.actionType NOT LIKE '%N%' THEN hh.hotelCode ELSE NULL END AS ORIGINAL_STORE_CODE, 
	td.arrivalDate AS ORIGINAL_TRANSACTION_DATE,
	td.departureDate AS ORIGINAL_TRANSACTION_END_DATE,
	NULL AS SUSPEND_REASON,
	NULL AS SUSPEND_TRANSACTION,
	REPLACE(REPLACE(
	--REPLACE(	
	jed.jsonExternalData
	--,'"','""')
	,CHAR(10),''),CHAR(13),'') AS JSON_EXTERNAL_DATA,
	NULL AS POSTING_KEY_CODE,
	NULL AS POSTING_KEY_VALUE

 FROM Reservations.dbo.Transactions t
	JOIN Reservations.dbo.TransactionDetail td
		ON t.TransactionDetailID = td.TransactionDetailID
	JOIN Reservations.dbo.TransactionStatus ts
		ON t.TransactionStatusID = ts.TransactionStatusID
	JOIN Reservations.dbo.ActionType act
		ON ts.ActionTypeID = act.ActionTypeID
	LEFT JOIN Reservations.dbo.LoyaltyNumber l
		ON t.LoyaltyNumberID = l.LoyaltyNumberID
	JOIN CurrencyRates.dbo.dailyRates xe
		ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
		AND td.currency = xe.code
	JOIN Reservations.dbo.hotel h
		ON t.HotelID = h.HotelID
	JOIN Hotels.dbo.hotel hh
		ON h.Hotel_hotelID = hh.HotelID
	JOIN Reservations.dbo.PH_BookingSource pbs
		ON t.PH_BookingSourceID = pbs.PH_BookingSourceID
	JOIN Reservations.dbo.PH_Channel pc
		ON pbs.PH_ChannelID = pc.PH_ChannelID
	JOIN Reservations.dbo.PH_SecondaryChannel psc
		ON pbs.PH_SecondaryChannelID = psc.PH_SecondaryChannelID
	JOIN Reservations.dbo.RateCode rc
		ON t.RateCodeID = rc.RateCodeID
	LEFT JOIN Reservations.dbo.PromoCode promo
		ON t.PromoCodeID = promo.PromoCodeID
	LEFT JOIN jed
		ON t.confirmationNumber = jed.confirmationNumber
	WHERE td.arrivalDate BETWEEN @startDate AND @endDate
	AND td.LoyaltyNumberValidated = 1	

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
