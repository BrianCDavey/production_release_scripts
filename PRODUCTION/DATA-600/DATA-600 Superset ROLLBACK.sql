USE Superset
GO

/*
Run this script on:

        chi-lt-00032377.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 3:11:36 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetNumeric]'
GO
DROP FUNCTION [dbo].[GetNumeric]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_InquiriesExport]'
GO
DROP PROCEDURE [dbo].[Epsilon_InquiriesExport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport01]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport01]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

;WITH firstActivity AS (
	SELECT ln.loyaltyNumber, MIN(ts.confirmationDate) as minDate
	FROM Reservations.dbo.Transactions t
	JOIN Reservations.dbo.TransactionStatus ts
		ON t.TransactionStatusID = ts.TransactionStatusID
	JOIN Reservations.dbo.LoyaltyNumber ln
		ON t.LoyaltyNumberID = ln.LoyaltyNumberID
	GROUP BY ln.loyaltyNumber
)
,jed AS (
		SELECT 
			cpi.iprefer_number
			,REPLACE(
				--CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
					(SELECT * FROM 
							(
								SELECT
									NULL AS MemberType
									,NULL AS PointBalance
									,NULL AS TierDate
									,NULL AS SubSourceCode
									,NULL AS LastActivityDate
									,NULL AS CampaignCode
									,NULL AS PromoCode
									,NULL AS Hotlisted
									,NULL AS TierExpire
								FROM [Superset].[BSI].[Customer_Profile_Import] cpi2
								WHERE cpi.iprefer_number = cpi2.iprefer_number
					
								UNION ALL
					
								SELECT '' AS MemberType
									,'' AS PointBalance
									,'' AS TierDate
									,'' AS SubSourceCode
									,'' AS LastActivityDate
									,'' AS CampaignCode
									,'' AS PromoCode
									,'' AS Hotlisted
									,'' AS TierExpire
								WHERE NOT EXISTS (SELECT 1
									FROM [Superset].[BSI].[Customer_Profile_Import] cpi2
									WHERE cpi.iprefer_number = cpi2.iprefer_number
								)
							) as sub 
							FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
						)
			--)
			,'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',') as jsonExternalData
					FROM [Superset].[BSI].[Customer_Profile_Import] cpi

	)
SELECT 
	'01' AS RecordNumber,
	NULL AS ProfileId,
	Membership_Date AS JoinDate,
	NULL AS CompanyName,
	NULL AS BirthDate,
	NULL AS Gender,
	NULL AS MaritalStatus,
	NULL AS LanguageCode,
	'Marketo' AS GlobalOptSource,
	NULL AS GlobalOptDate,
	NULL AS GlobalOptOut,
	NULL AS Suffix,
	Last_Name AS LastName,
	NULLIF(LEFT(TRIM(middle_name),1),'') AS MiddleInit,
	First_Name AS FirstName,
	NULL AS Prefix,
	cpi.iPrefer_Number AS ClientAccountId,
	CASE Member_Status WHEN 'DISABLED' THEN 'I' ELSE 'A' END AS [Status],
	'Preferred Hotel Group' AS AccountSourceCode,
	cpi.iPrefer_Number AS SourceAccountNumber,
	ISNULL(Disabled_Date,Import_Date) AS ActivityDate,
	NULL AS CreateFileId,
	NULL AS CreateRecordNumber,
	NULL AS SrcPrefix,
	First_Name AS SrcFirstName,
	NULLIF(TRIM(Middle_Name),'') AS SrcMiddleInit,
	Last_Name AS SrcLastName,
	NULL AS SrcSuffix,
	NULL AS SrcGender,
	NULL AS SrcUnparsedName,
	NULL AS SrcFirmName,
	NULL AS BrandOrgCode,
	cpi.iPrefer_Number AS InteractionId,
	NULL AS PrefChannelCode,
	NULL AS Title,
	NULL AS CountryCode,
	NULL AS LanguageReqInd,
	NULL AS DwellingCode,
	NULL AS SalutationId,
	NULL AS TitleCode,
	NULL AS MaturitySuffix,
	NULLIF(TRIM(Middle_Name),'') AS MiddleName,
	fa.minDate AS OrigActivityDate,
	NULL AS ProfessionalSuffix,
	NULL AS OwnershipCode,
	NULL AS StmtCycle,
	NULL AS ClientLastUpDateDate,
	cpi.iPrefer_Number AS CardNumber,
	Disabled_Date AS EndDate,
	NULL AS TierReasonCode,
	NULL AS TierCode,
	Enrollment_Source AS EnrollChannelCode,
	Remarks AS StatusChangeReason,
	NULL AS EnrollmentStatus,
	NULL AS SourceCode,
	NULL AS ProductName,
	'PHG' AS ProgramCode,
	NULL AS ProductLevelCode,
	NULL AS VulGarInd,
	jed.jsonExternalData AS JsonExternalData,
	NULLIF(TRIM(SignedupHotelCode),'') AS EnrollmentStoreCode,
	h.HotelName AS EnrollmentStoreName

FROM [Superset].[BSI].[Customer_Profile_Import] cpi
LEFT JOIN firstActivity fa
	ON cpi.iPrefer_Number = fa.loyaltyNumber
LEFT JOIN jed
	ON cpi.iPrefer_Number = jed.iPrefer_Number
LEFT JOIN Hotels.dbo.Hotel h
	ON cpi.SignedupHotelCode = h.HotelCode;




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport02]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport02]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

SELECT 
'02' AS RecordNumber
,NULL AS AddressId
,address AS AddressLine1
,address2 AS AddressLine2
,NULL AS AddressLine3
,city AS City
,state AS StateCode
,country AS CountryCode
,zip_postal AS PostalCode
,NULL AS ChannelCode
,NULL AS LocationCode
,'True' AS IsPreferred
,'False' AS DoNotStandardize
,NULL AS UniqueZipInd
,NULL AS MailScore
,NULL AS Latitude
,NULL AS Longitude
,NULL AS NoStatInd
,NULL AS AceErrorCode
,NULL AS MultiTypeCode
,NULL AS NcoaReturnCode
,NULL AS SeasonalInd
,NULL AS EducationInd
,NULL AS AddressDefInd
,NULL AS EndDate
,NULL AS Status
,import_date AS ActivityDate
,NULL AS CreateFileId
,NULL AS CreateRecordNumber
,address AS SrcAddrLine1
,address2 AS SrcAddrLine2
,NULL AS SrcAddrLine3
,city AS SrcCity
,state AS SrcState
,zip_Postal AS SrcPostalCd
,country AS SrcCountry
,NULL AS SrcIsoCountryCd
,Iprefer_Number AS InteractionId
,NULL AS AddressLine4
,NULL AS NameMatchInd
,NULL AS PacActionCode
,NULL AS PacFootNote
,NULL AS SrcCompanyName
,NULL AS SrcFirmName
,NULL AS Phone1
,NULL AS Phone2
,NULL AS BrandOrgCode
,'Preferred Hotel Group' AS AccountSourceCode
,iprefer_number AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import];



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport03]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport03]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
'03' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, Email AS EmailAddress
, NULL AS EmailId
, 'True' AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, Import_Date AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, Email AS SrcEmailAddr
, NULL AS ContactPointId
, IPrefer_Number AS InteractionId
, NULL AS EmailType
, NULL AS SpamStatus
, NULL AS EmailFormatPref
, 'Preferred Hotel Group' AS AccountSourceCode
, IPrefer_Number AS SourceAccountNumber
, NULL AS BrandOrgCode
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import];




END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport04]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport04]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT
'04' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, NULL AS PhoneNumber
, NULL AS PhoneId
, NULL AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, NULL AS Extension
, NULL AS PhoneCountryCode
, NULL AS OptStatus
, NULL AS OptInDate
, NULL AS Frequency
, NULL AS NeverBefore
, NULL AS NeverAfter
, NULL AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, NULL AS SrcPhoneNumber
, NULL AS ContactPointId
, Iprefer_Number AS InteractionId
, NULL AS BrandOrgCode
, NULL AS AcceptsText
, 'Preferred Hotel Group' AS AccountSourceCode
, Iprefer_Number AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import];



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_PointsExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_PointsExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT
		CASE WHEN Transaction_Source = 'SFTP' THEN tdr.Booking_ID ELSE NULL END AS TransactionID,
		ISNULL(otn.[Membership Number],tdr.iPrefer_Number) AS CardNumber,
		COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) as NumPoints,
		tdr.Arrival_Date as ActivityDate,
		NULL AS certificateNumber,
		CASE WHEN Transaction_Source = 'SFTP' THEN NULL ELSE tdr.Transaction_Id END as ActAdjustmentId
	  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
	  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
		ON tdr.iPrefer_Number = otn.[Old Membership Number]
	WHERE COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) <> 0
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_CertificatesExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_CertificatesExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT 
	  [Voucher_Number] AS CertificateNumber,
	  [Membership_Number] AS CardNumber,
	  'Redeemed' AS CertificateStatus,
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  Voucher_Value * toUSD AS RetailValue, --needs to be in USD
	  Redemption_Date AS CreateDate,
	  NULL AS ExpireDate,
	  NULL AS UpdateDate,
	  Hotel_Code AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  Voucher_Value AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Account_Statement_Hotel_Redemption_Import] c
  JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_AdjustmentExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_AdjustmentExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	tdr.Transaction_Id AS ActAdjustmentId,
	ISNULL(otn.[Membership Number],tdr.iPrefer_Number) AS CardNumber,
	Campaign AS AdjustmentReasonCode,
	Remarks AS AdjustmentComments,
	COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) as NumPoints,
	tdr.Reward_Posting_Date as ActivityDate,
	NULL as CreateUser,
	tdr.Hotel_Code as StoreCode
  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
	ON tdr.iPrefer_Number = otn.[Old Membership Number]
WHERE TRIM(tdr.Booking_ID) <> ''
AND COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) <> 0
AND Transaction_Source != 'SFTP'

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
