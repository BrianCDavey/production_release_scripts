USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 8:25:39 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[All_Member_Report_Staging]'
GO
CREATE TABLE [BSI].[All_Member_Report_Staging]
(
[iPreferNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TierCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tierdate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PointBalance] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JoinDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Suffix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Prefix] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PromoCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonExternalData] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnrollmentStoreCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone Number] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotlisted] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IATANumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[All_Member_Report_Import]'
GO
CREATE TABLE [BSI].[All_Member_Report_Import]
(
[iPrefer_Number] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[First_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Middle_Name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Last_Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip_Postal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current_Points_Balance] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Membership_Date] [date] NULL,
[e-statement] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Special_Offers] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Surveys] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Partner_Offers] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SignedupHotelCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Import_Date] [datetime] NOT NULL,
[File_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Enrollment_Source] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Disabled_Date] [date] NULL,
[Profile_Update_Date] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMemberEnabled] AS (case [Member_Status] when 'ENABLED' then CONVERT([bit],(1),(0)) when 'DISABLED' then CONVERT([bit],(0),(0)) end) PERSISTED,
[BirthDate] [date] NULL,
[Hotlisted] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PromoCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TierDate] [date] NULL,
[ActivityDate] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_All_Member_Report_Import] on [BSI].[All_Member_Report_Import]'
GO
ALTER TABLE [BSI].[All_Member_Report_Import] ADD CONSTRAINT [PK_All_Member_Report_Import] PRIMARY KEY CLUSTERED  ([iPrefer_Number])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_All_Member_Report_Import]'
GO



CREATE PROCEDURE [BSI].[Populate_All_Member_Report_Import]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	TRUNCATE TABLE [BSI].[All_Member_Report_Import]

	INSERT INTO [BSI].[All_Member_Report_Import]
           ([iPrefer_Number]
           ,[First_Name]
           ,[Middle_Name]
           ,[Last_Name]
           ,[Zip_Postal]
           ,[City]
           ,[Address]
           ,[Address2]
           ,[State]
           ,[Country]
           ,[Email]
           ,[Current_Points_Balance]
           ,[Tier]
           ,[Membership_Date]
           ,[e-statement]
           ,[Special_Offers]
           ,[Member_Surveys]
           ,[Partner_Offers]
           ,[SignedupHotelCode]
           ,[Import_Date]
           ,[File_Name]
           ,[Enrollment_Source]
           ,[Member_Status]
           ,[Disabled_Date]
           ,[Profile_Update_Date]
           ,[Remarks]
		   ,[BirthDate]
		   ,Hotlisted
		   ,PromoCode
		   ,PhoneNumber
		   ,[TierDate]
		   ,ActivityDate)

	SELECT	
			[iPreferNumber]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[PostalCode]
           ,[City]
           ,[AddressLine1]
           ,[AddressLine2] 
           ,StateCode
           ,CountryCode
           ,[Email]
           ,PointBalance AS [Current_Points_Balance]
           ,[TierCode]
           ,JoinDate AS [Membership_Date]
           ,NULL AS [e-statement]
           ,NULL AS [Special_Offers]
           ,NULL AS [Member_Surveys]
           ,NULL AS [Partner_Offers]
           ,EnrollmentStoreCode AS [SignedupHotelCode]
           ,GETDATE() AS [Import_Date]  --add this later
           ,NULL AS [File_Name]  -- add this later
           ,NULL AS [Enrollment_Source]  --check this later
           ,[Status]
           ,NULL [Disabled_Date]  --check this later
           ,NULL AS [Profile_Update_Date]
           ,NULL AS [Remarks] --check this later
		   ,NULL AS BirthDate
		   ,Hotlisted AS Hotlisted
		   ,PromoCode AS PromoCode
		   ,RIGHT([Phone Number],20)
		   ,CAST(MIN([TierDate]) AS DATE) AS [TierDate]
		   ,ActivityDate
		   FROM [BSI].[All_Member_Report_Staging]
		   GROUP BY
		   [iPreferNumber]
           ,[FirstName]
           ,[MiddleName]
           ,[LastName]
           ,[PostalCode]
           ,[City]
           ,[AddressLine1]
           ,[AddressLine2] 
           ,StateCode
           ,CountryCode
           ,[Email]
           ,PointBalance
           ,[TierCode]
           ,JoinDate
		   ,EnrollmentStoreCode
		   ,[Status]
		   ,Hotlisted
		   ,PromoCode
		   ,[Phone Number]
		   ,ActivityDate
		
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Reward_Certificate_Liability_Report_Staging]'
GO
CREATE TABLE [BSI].[Reward_Certificate_Liability_Report_Staging]
(
[Redemption Date] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Certificate Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member Number] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email Id] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member First Name] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member Surname] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type of Redemption] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product Redeemed] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Points Redeemded] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current Order Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Liability USD] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USD Value ISSUED] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Reward_Certificate_Liability_Report_Import]'
GO
CREATE TABLE [BSI].[Reward_Certificate_Liability_Report_Import]
(
[Redemption_Date] [datetime] NULL,
[Membership_Number] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Type] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Number] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Value] [numeric] (18, 2) NULL,
[Voucher_Currency] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payable_Value_USD] [numeric] (18, 2) NULL,
[filename] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importdate] [datetime] NULL,
[id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Currency_Conversion_Date] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Reward_Certificate_Liability_Report_Import] on [BSI].[Reward_Certificate_Liability_Report_Import]'
GO
ALTER TABLE [BSI].[Reward_Certificate_Liability_Report_Import] ADD CONSTRAINT [PK_Reward_Certificate_Liability_Report_Import] PRIMARY KEY NONCLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Reward_Certificate_Liability_Report_Import]'
GO

CREATE PROCEDURE [BSI].[Populate_Reward_Certificate_Liability_Report_Import]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

		INSERT INTO [Superset].[BSI].[Reward_Certificate_Liability_Report_Import]
		(		[Redemption_Date]
		      ,[Membership_Number]
		      ,[Member_Name]
		      ,[Voucher_Type]
		      ,[Voucher_Number]
		      ,[Voucher_Name]
		      ,[Voucher_Value]
		      ,[Voucher_Currency]
		      ,[Payable_Value_USD]
		      ,[filename]
		      ,[importdate]
		      ,[Currency_Conversion_Date])
		SELECT 
		[Redemption Date],
		[Member Number],
		[Member First Name] + ' ' + [Member Surname],
		[Type of Redemption],
		[Certificate Number],
		[Product Redeemed],
		REPLACE(REPLACE(REPLACE(REPLACE([Product Redeemed],'US$',''),'£',''),'€',''), 'Reward Certificate',''),
		CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END,
		RIGHT([Liability USD], LEN([Liability USD])-1),
		NULL,
		GETDATE(),
		[Redemption Date]
		FROM [Superset].[BSI].[Reward_Certificate_Liability_Report_Staging]
		
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[LoyaltyPrimeSubEpsilonReasonMapping]'
GO
CREATE TABLE [BSI].[LoyaltyPrimeSubEpsilonReasonMapping]
(
[Type of sub query] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Reason] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[LogQueryStatusReport_Staging]'
GO
CREATE TABLE [BSI].[LogQueryStatusReport_Staging]
(
[Date of Query] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Date of Query Closed] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type of Query] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Type of sub query] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reference Number] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member Number] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Confirmation Number] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Query Text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Logged By] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Assigned User] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Query Logged By] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Query Closed By] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Adjustment_Reason_Codes_Mapping]'
GO
CREATE TABLE [BSI].[Adjustment_Reason_Codes_Mapping]
(
[Campaign] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Campaign Type Redemption] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current Status] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Epsilon Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Member_Type_Matching_Staging]'
GO
CREATE TABLE [BSI].[Member_Type_Matching_Staging]
(
[Promo Code] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Promotional Campaign] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Points Awarded] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tier Upgrade] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Current Status] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source vs Promotion] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member Type] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Enrollment_Source_Mapping]'
GO
CREATE TABLE [BSI].[Enrollment_Source_Mapping]
(
[Enrollment Source Code] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tier] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mapped LP Enrollment Source] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Enrollment_Channel_Mapping]'
GO
CREATE TABLE [BSI].[Enrollment_Channel_Mapping]
(
[LP Enrollment_Source] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Epsilon Channel Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
