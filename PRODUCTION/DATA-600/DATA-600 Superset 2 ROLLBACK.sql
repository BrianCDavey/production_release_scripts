USE Superset
GO

/*
Run this script on:

        chi-lt-00032377.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 8:24:58 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[All_Member_Report_Import]'
GO
ALTER TABLE [BSI].[All_Member_Report_Import] DROP CONSTRAINT [PK_All_Member_Report_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Reward_Certificate_Liability_Report_Import]'
GO
ALTER TABLE [BSI].[Reward_Certificate_Liability_Report_Import] DROP CONSTRAINT [PK_Reward_Certificate_Liability_Report_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Enrollment_Channel_Mapping]'
GO
DROP TABLE [BSI].[Enrollment_Channel_Mapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Enrollment_Source_Mapping]'
GO
DROP TABLE [BSI].[Enrollment_Source_Mapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Member_Type_Matching_Staging]'
GO
DROP TABLE [BSI].[Member_Type_Matching_Staging]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Adjustment_Reason_Codes_Mapping]'
GO
DROP TABLE [BSI].[Adjustment_Reason_Codes_Mapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[LogQueryStatusReport_Staging]'
GO
DROP TABLE [BSI].[LogQueryStatusReport_Staging]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[LoyaltyPrimeSubEpsilonReasonMapping]'
GO
DROP TABLE [BSI].[LoyaltyPrimeSubEpsilonReasonMapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Reward_Certificate_Liability_Report_Import]'
GO
DROP PROCEDURE [BSI].[Populate_Reward_Certificate_Liability_Report_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Reward_Certificate_Liability_Report_Import]'
GO
DROP TABLE [BSI].[Reward_Certificate_Liability_Report_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Reward_Certificate_Liability_Report_Staging]'
GO
DROP TABLE [BSI].[Reward_Certificate_Liability_Report_Staging]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_All_Member_Report_Import]'
GO
DROP PROCEDURE [BSI].[Populate_All_Member_Report_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[All_Member_Report_Import]'
GO
DROP TABLE [BSI].[All_Member_Report_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[All_Member_Report_Staging]'
GO
DROP TABLE [BSI].[All_Member_Report_Staging]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
