USE Hotels
GO

/*
Run this script on:

        chi-lt-00032377.Hotels    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Hotels

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 3:03:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetNumeric]'
GO
DROP FUNCTION [dbo].[GetNumeric]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_HotelExport]'
GO


ALTER PROCEDURE [dbo].[Epsilon_HotelExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH minContact AS (
		SELECT h.HotelID, h.HotelCode, h.CRM_HotelID, MIN(hcr.contactID) as minContactID
		FROM Hotels.dbo.Hotel h
		JOIN Hotels.dbo.Hotel_Contact_Role hcr
			ON h.HotelID = hcr.HotelID
			AND hcr.RoleID = 28 --iprefer ambassador
		GROUP BY h.HotelID, h.HotelCode, h.CRM_HotelID
	),
	jed AS (
		SELECT 
			h.HotelID
			,REPLACE(
			CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
				(
					SELECT
						ISNULL(h2.HotelCode,'') as [HotelCode] 
						, ISNULL(h2.Rooms,'') as [TotalRooms]
						, ISNULL(phg_ipreferquarterlyenrollmentgoal,0)/90 as EnrollmentGoal
					FROM Hotels.dbo.hotel h2
					LEFT JOIN LocalCRM.dbo.Account2 as a
						ON h2.CRM_HotelID = a.accountid
					WHERE h.HotelID = h2.HotelID 
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				)
				,(
				SELECT * FROM 
					(SELECT 
						ISNULL(phg_contractedprogramstypename,'') as ProgramParticipationStatus
						, CAST(ISNULL(phg_startdate,'1900-01-01') as DATE) as ProgramParticipationStartDate
						, CAST(ISNULL(phg_enddate,'9999-09-09') as DATE) as ProgramParticipationEndDate
					FROM LocalCRM.dbo.phg_contractedprograms cp
					WHERE cp.phg_contractedprogramstypename LIKE '%i%prefer%'
					AND h.CRM_HotelID = cp.phg_accountnameid
					UNION ALL
					SELECT '' as ProgramParticipationStatus
						, '' as ProgramParticipationStartDate
						, '' as ProgramParticipationEndDate
					WHERE NOT EXISTS (SELECT 1
						FROM LocalCRM.dbo.phg_contractedprograms cp
						WHERE cp.phg_contractedprogramstypename LIKE '%i%prefer%'
						AND h.CRM_HotelID = cp.phg_accountnameid
					)) as sub 


					FOR JSON PATH, ROOT('IPreferParticipation')
				)
				,
				(SELECT * FROM 
					(
					SELECT ISNULL(c.CollectionName,'') as [CollectionName]
						, ISNULL(c.Code,'') as [CollectionCode]
						, CAST(ISNULL(hc.StartDate,'1900-01-01') as DATE) as [CollectionParticipationStartDate]
						, CAST(ISNULL(hc.EndDate,'9999-09-09') as DATE) as [CollectionParticipationEndDate] 
					FROM Hotels.dbo.Hotel_Collection hc
					JOIN Hotels.dbo.Collection c
						ON hc.CollectionID = c.CollectionID 
					WHERE h.HotelID = hc.HotelID 
						UNION ALL
					SELECT '' as [CollectionName]
						, '' as [CollectionCode]
						, '' as [CollectionParticipationStartDate]
						, '' as [CollectionParticipationEndDate] 
					WHERE NOT EXISTS (SELECT 1
					FROM Hotels.dbo.Hotel_Collection hc
					JOIN Hotels.dbo.Collection c
						ON hc.CollectionID = c.CollectionID 
					WHERE h.HotelID = hc.HotelID 
					)) as sub 

					FOR JSON PATH, ROOT('Collections')
				)
				,(SELECT * FROM 
					(
					SELECT ISNULL(iprd.IPreferReward,'') as [BookingReward]
						, CAST(ISNULL(hipr.StartDate,'1900-01-01') as DATE) as [BookingRewardStartDate]
						, CAST(ISNULL(hipr.EndDate,'9999-09-09') as DATE) as [BookingRewardEndDate]
					FROM Hotels.dbo.Hotel_IPreferReward hipr
					JOIN Hotels.dbo.IPreferReward ipr
						ON hipr.IPreferRewardID = ipr.IPreferRewardID
					JOIN Hotels.dbo.IPreferReward_Detail iprd
						ON ipr.IPreferRewardID = iprd.IPreferRewardID
					WHERE h.HotelID = hipr.HotelID 
						UNION ALL
					SELECT '' as [BookingReward]
						, '' as [BookingRewardStartDate]
						, '' as [BookingRewardEndDate]
					WHERE NOT EXISTS (SELECT 1
					FROM Hotels.dbo.Hotel_IPreferReward hipr
					JOIN Hotels.dbo.IPreferReward ipr
						ON hipr.IPreferRewardID = ipr.IPreferRewardID
					JOIN Hotels.dbo.IPreferReward_Detail iprd
						ON ipr.IPreferRewardID = iprd.IPreferRewardID
					WHERE h.HotelID = hipr.HotelID 
					)) as sub 

					FOR JSON PATH, ROOT('IPreferBookingRewards')
				)
				,(SELECT * FROM 
					(
					SELECT
						ISNULL(pc.ParentCompanyID,'') as [GroupCompanyID]
						, ISNULL(pc.ParentCompanyName,'') as [GroupName]
						, ISNULL(pr.ParentRelationshipName,'') as [GroupRelationshipType]
						, CAST(ISNULL(hpc.StartDate,'1900-01-01') as DATE) as [GroupStartDate]
						, CAST(ISNULL(hpc.EndDate,'9999-09-09') as DATE) as [GroupEndDate]
					FROM Hotels.dbo.Hotel_ParentCompany hpc
					JOIN Hotels.dbo.ParentCompany pc
						ON hpc.ParentCompanyID = pc.ParentCompanyID
					JOIN Hotels.dbo.ParentRelationship pr
						ON hpc.ParentRelationshipID = pr.ParentRelationshipID
					WHERE h.HotelID = hpc.HotelID 
						UNION ALL
					SELECT
						'' as [GroupCompanyID]
						, '' as [GroupName]
						, '' as [GroupRelationshipType]
						, '' as [GroupStartDate]
						, '' as [GroupEndDate]
					WHERE NOT EXISTS (SELECT 1
					FROM Hotels.dbo.Hotel_ParentCompany hpc
					JOIN Hotels.dbo.ParentCompany pc
						ON hpc.ParentCompanyID = pc.ParentCompanyID
					JOIN Hotels.dbo.ParentRelationship pr
						ON hpc.ParentRelationshipID = pr.ParentRelationshipID
					WHERE h.HotelID = hpc.HotelID 
					)) as sub 

					FOR JSON PATH, ROOT('HotelGroups')
				)
				,(SELECT * FROM 
					(
					SELECT DISTINCT
						 ISNULL(rbcrc.rateCode,'') as [ExcludedRateCode]
						, CAST(ISNULL(rbcrc.startDate, '1900-01-01') as DATE) as [ExcludedRateStartDate]
						, CAST(ISNULL(rbcrc.endDate, '9999-09-09') as DATE) as [ExcludedRateEndDate]
					  FROM 
					  [ReservationBilling].[dbo].[Criteria] rbc
					  JOIN [ReservationBilling].[dbo].[Criteria_RateCodes] rbcrc
						ON rbc.criteriaID = rbcrc.criteriaID
					  JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
						ON rbc.criteriaID = cgic.criteriaID
						JOIN ReservationBilling.dbo.Clauses_ExcludeCriteriaGroups cecg
							ON cgic.criteriaGroupID = cecg.criteriaGroupID
						JOIN ReservationBilling.dbo.Clauses cl
							ON cecg.clauseID = cl.clauseID
					  WHERE rbc.criteriaName LIKE '%I%Prefer%'
					  AND rbc.criteriaName LIKE '%Excluded%Rates%'
					  AND h.HotelCode = cl.hotelCode
					  	UNION ALL
					SELECT '' as [ExcludedRateCode]
						, '' as [ExcludedRateStartDate]
						, '' as [ExcludedRateEndDate]
					WHERE NOT EXISTS (SELECT 1
					FROM 
					  [ReservationBilling].[dbo].[Criteria] rbc
					  JOIN [ReservationBilling].[dbo].[Criteria_RateCodes] rbcrc
						ON rbc.criteriaID = rbcrc.criteriaID
					  JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
						ON rbc.criteriaID = cgic.criteriaID
						JOIN ReservationBilling.dbo.Clauses_ExcludeCriteriaGroups cecg
							ON cgic.criteriaGroupID = cecg.criteriaGroupID
						JOIN ReservationBilling.dbo.Clauses cl
							ON cecg.clauseID = cl.clauseID
					  WHERE rbc.criteriaName LIKE '%I%Prefer%'
					  AND rbc.criteriaName LIKE '%Excluded%Rates%'
					  AND h.HotelCode = cl.hotelCode
					)) as sub 

				  FOR JSON PATH, ROOT('ExcludedRateCodes')
				)
			),'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',') as jsonExternalData
		FROM Hotels.dbo.Hotel h

	)
	SELECT
		dh.[Hotel Code] as [StoreCode],
		dh.[Hotel Name] as [StoreName],
		NULL as [ChainPriority],
		'PHG Hotels' as [ChainName],
		NULL as [FranchiseCode],
		NULL as [ParentStoreCode],
		h.Address1 as [AddressLine1],
		h.Address2 as [AddressLine2],
		NULL as [AddressLine3],
		dh.[Hotel City] as [City],
		hs.State_Text as [StateCode],
		LEFT(dh.[Hotel Postal Code],10) as [PostalCode],
		crmc.phg_code3 as [CountryCode],
		NULL as [Urbanization],
		h.Latitude as [Latitude],
		h.Longitude as [Longitude],
		NULL as [Gaid],
		hr.Region_Code as [RegionCode],
		hr.Region_Text as [RegionLevel],
		accountclassificationcodename as [StoreType],
		p.FirstName as [ManagerFirstName],
		NULL as [ManagerMiddleInitial],
		p.LastName as [ManagerLastName],
		ISNULL(p.FirstName,'') + ' ' + ISNULL(p.LastName,'') as [ManagerFullName],
		p.Email as [ManagerEmailAddr],
		crmA.telephone1 as [PhoneNumber],
		NULL as [DMA],
		NULL as [SalesArea],
		NULL as [TotalArea],
		NULL as [DivCode],
		NULL as [StoreOpenDate],
		NULL as [StoreCloseDate],
		NULL as [RemodelDate],
		NULL as [HierarchyCode],
		'PHG' as [BrandOrgCode],
		ISNULL(crmA.modifiedon,crmA.createdon) as [ActivityDate],
		NULL as [ClientFileId],
		crmA.accountid as [ClientFileRecordNumber],
		'A' as [Status],
		REPLACE(REPLACE(
		--REPLACE(
		jed.jsonExternalData
		--,'"','""')
		,CHAR(10),''),CHAR(13),'') as [JsonExternalData],
		NULL as [GeofenceRadius],
		NULL as [StoreConfiguration],
		NULL as [StoreCompFlag],
		NULL as [StoreTelexNumber],
		NULL as [StoreFaxNumber],
		NULL as [CompStartDate],
		NULL as [CompEndDate],
		NULL as [StoreSizeBand]

	FROM Dimensional_Warehouse.dim.Hotel dh
	JOIN Hotels.dbo.Hotel h
		ON dh.sourceKey = h.HotelID
	LEFT JOIN LocalCRM.dbo.Account crmA
		ON h.CRM_HotelID = crmA.accountid
	LEFT JOIN Hotels.dbo.Location l
		ON h.LocationID = l.LocationID
	LEFT JOIN Hotels.dbo.Country hc
		ON l.CountryID = hc.CountryID
	LEFT JOIN LocalCRM.dbo.phg_country crmc
		ON hc.CRM_CountryID = crmc.phg_countryid
	LEFT JOIN Hotels.dbo.State hs
		ON l.StateID = hs.StateID
	LEFT JOIN Hotels.dbo.Region hr
		ON l.RegionID = hr.RegionID
	LEFT JOIN minContact mc
		ON h.HotelID = mc.HotelID
	LEFT JOIN Hotels.dbo.Contact c
		ON mc.minContactID = c.ContactID
	LEFT JOIN Hotels.dbo.Person p
		ON c.PersonID = p.PersonID
	LEFT JOIN jed
		ON h.HotelID = jed.HotelID
	WHERE dh.[Status Code] IN ('Member Hotel','Former Member','Renewal Hotel')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
