USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/20/2020 8:15:36 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sourceCodes_SupersetMigration]'
GO
CREATE TABLE [dbo].[sourceCodes_SupersetMigration]
(
[primaryChannelDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[primaryChannelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondaryChannelDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondaryChannelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subChannelDescription] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subChannelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceCodesID] [int] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_SourceCodes_SupersetMigration] on [dbo].[sourceCodes_SupersetMigration]'
GO
ALTER TABLE [dbo].[sourceCodes_SupersetMigration] ADD CONSTRAINT [PK_SourceCodes_SupersetMigration] PRIMARY KEY CLUSTERED  ([SourceCodesID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SourceCodes]'
GO


ALTER VIEW [dbo].[SourceCodes]
AS
SELECT ISNULL(CAST((row_number() OVER (ORDER BY primaryChannelDescription )) AS int), 0) 
AS EDMXID, primaryChannelDescription, primaryChannelCode, secondaryChannelDescription, secondaryChannelCode, subChannelDescription, 
                         subChannelCode
FROM ReservationBilling.dbo.sourceCodes_SupersetMigration AS sourcecodes_1
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [work].[Move_Charges_BillableDate]'
GO


CREATE PROCEDURE [work].[Move_Charges_BillableDate]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @invoiceDate DATE;

	SELECT @invoiceDate = MAX(invoiceDate) FROM ReservationBilling.dbo.charges 

	UPDATE ReservationBilling.dbo.charges
	SET billableDate = DATEADD(month, DATEDIFF(month, 0, @invoiceDate), 0)
	WHERE runID = @RunID
	AND billableDate < DATEADD(month, DATEDIFF(month, 0, @invoiceDate), 0) -- Start of latest invoice month
	AND sopNumber IS NULL --Not invoiced

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
