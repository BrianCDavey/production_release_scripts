USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/17/2020 6:22:06 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IPreferGuestNameReport_Reservation]'
GO








-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-05-30
-- Description:	This proc is a test version of IPreferGuestNameReport_2017 pointing to IC DB called by SSRS GuestNameReport_Test
-- EXEC [dbo].[IPreferGuestNameReport_Reservation] 'RES0179263           '
-- =============================================


ALTER  procedure [dbo].[IPreferGuestNameReport_Reservation]
	@SOPNumber varchar(20)
AS
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

---- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'

--DECLARE @SOPNumber varchar(20) = 'RES0115115'
-- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'


DECLARE @currencymaster TABLE (
								CURNCYID char(15),
								CURRNIDX smallint,
								NOTEINDX numeric(19, 5),
								CRNCYDSC char(31),
								CRNCYSYM char(3),
								CNYSYMAR_1 smallint,
								CNYSYMAR_2 smallint,
								CNYSYMAR_3 smallint,
								CYSYMPLC smallint,
								INCLSPAC tinyint,
								NEGSYMBL smallint,
								NGSMAMPC smallint,
								NEGSMPLC smallint,
								DECSYMBL smallint,
								DECPLCUR smallint,
								THOUSSYM smallint,
								CURTEXT_1 char(25),
								CURTEXT_2 char(25),
								CURTEXT_3 char(25),
								ISOCURRC char(3),
								CURLNGID smallint,
								DEX_ROW_TS datetime,
								DEX_ROW_ID int
							);

insert into @currencymaster
select	CURNCYID,
		CURRNIDX,
		NOTEINDX,
		CRNCYDSC,
		CRNCYSYM,
		CNYSYMAR_1,
		CNYSYMAR_2,
		CNYSYMAR_3,
		CYSYMPLC,
		INCLSPAC,
		NEGSYMBL,
		NGSMAMPC,
		NEGSMPLC,
		DECSYMBL,
		DECPLCUR,
		THOUSSYM,
		CURTEXT_1,
		CURTEXT_2,
		CURTEXT_3,
		ISOCURRC,
		CURLNGID,
		DEX_ROW_TS,
		DEX_ROW_ID
FROM DYNAMICS.dbo.MC40200



DECLARE @GP_SOPNumber_ItemCodes TABLE (	
										ITEMNMBR varchar(50),
										ItemGrouping varchar(250)
									);

INSERT INTO @GP_SOPNumber_ItemCodes(ITEMNMBR,ItemGrouping)
SELECT itemCode.ITEMNMBR,
	CASE
		WHEN itemCode.ITEMDESC <> COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC) THEN COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC)
		ELSE
			CASE COALESCE(RTRIM(itemClasses.ITMCLSDC), '') WHEN '' THEN ''
			ELSE RTRIM(itemClasses.ITMCLSDC) + ' '
		END
		+
		CASE COALESCE(RTRIM(itemCode.ITMSHNAM),'')
			WHEN '' THEN '' 
			ELSE RTRIM(itemCode.ITMSHNAM)
		END 
	END as itemGrouping	
FROM IC.dbo.IV00101 AS itemCode 
	LEFT JOIN IC.dbo.SY03900 itemNotes ON itemCode.NOTEINDX = itemNotes.NOTEINDX 
	LEFT JOIN IC.dbo.IV40600 itemClassifications ON itemClassifications.USCATNUM = 1 AND itemCode.USCATVLS_1 = itemClassifications.USCATVAL 
	LEFT JOIN IC.dbo.IV40400 itemClasses ON itemCode.ITMCLSCD = itemClasses.ITMCLSCD
	LEFT JOIN IC.dbo.SOP30300 historyLines ON historyLines.SOPNUMBE = 'abc' AND itemCode.itemCode = historyLines.ITEMNMBR
	LEFT JOIN IC.dbo.SOP10200 workLines ON  workLines.SOPNUMBE = 'ABC' AND itemCode.itemCode = workLines.ITEMNMBR

--EXEC sp_ExecuteSQL @SqlCommand;


SELECT	main.currency AS currency
,		main.arrivalDate AS arrivalDate
,		main.guestLastName AS guestLastName
,		main.guestFirstName AS guestFirstName
,		main.confirmationNumber AS confirmationNumber
,		main.confirmationDate AS confirmationDate
,		main.consortia AS consortia
,		main.IATANumber AS IATANumber
,		main.rateTypeCode AS rateTypeCode
,		CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END AS iPreferNumber
,		main.averageDailyRate AS averageDailyRate
,		main.roomNights AS roomNights
,		main.reservationRevenue AS reservationRevenue
,		main.iPreferCharge AS iPreferCharge
,		main.commCharge AS commCharge
,		main.bookCharge AS bookCharge
,		main.surcharge AS surcharge
,		ISNULL(main.totalCharge, 0) AS totalCharge
,		ISNULL(REPLACE(main.itemGrouping, 'zzzzz! ', ''), 'Other Booking Sources' ) AS itemGrouping --remove hardcode value zzzzz!
,		main.billingDate as billingDate

FROM (
Select	chargedReservations.hotelCurrencyCode as currency, 
		chargedReservations.arrivalDate, 
		UPPER(MRT.guestLastName) AS guestLastName, 
		UPPER(MRT.guestFirstName) AS guestFirstName, 
		chargedReservations.confirmationNumber, 
		MRT.confirmationDate ,
		ReservationBilling.dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		MRT.rateTypeCode,
		chargedReservations.loyaltyNumber AS iPreferNumber,
		Case When chargedReservations.roomNights <> 0
			THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
			ELSE 0 END as averageDailyRate, 
		chargedReservations.roomNights, 
		chargedReservations.roomRevenueInHotelCurrency as reservationRevenue, 
				SUM(case when chargedReservations.[classificationID] = 5 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS iPreferCharge,
		SUM(case when chargedReservations.[classificationID] = 1 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS bookCharge, 
		SUM(case when chargedReservations.[classificationID] = 2 -- commission
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS commCharge, 
		SUM(case when chargedReservations.classificationID = 3 -- surcharges
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS surcharge,
		SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
		MIN(case when chargedReservations.[classificationID] NOT IN (3,5) -- surcharges I prefer
				 then LTRIM(RTRIM(GPSopIC.itemGrouping))
				when chargedReservations.[classificationID] = 5 
				 then 'zzzzz! Hotel & Manual I Prefer Point Awards' --append zzzzz! on purpose for max logic to work
				 when chargedReservations.classificationID = 3 AND mrt.channel IN ('Channel Connect','IDS') AND itemCode = 'AMEXTMC'
					then 'zzzzz! Expedia/Amex OTA'
			END ) AS itemGrouping,
		chargedReservations.billableDate as billingDate
		
FROM	@GP_SOPNumber_ItemCodes GPSopIC

JOIN ReservationBilling.dbo.Charges chargedReservations with (nolock)
	ON  chargedReservations.sopNumber = @SOPNumber
	AND	chargedReservations.itemCode = GPSopIC.ITEMNMBR 

LEFT OUTER JOIN 	Reservations.dbo.mostrecenttransactions MRT with (nolock)
	ON	chargedReservations.confirmationNumber = MRT.confirmationNumber 

WHERE chargedReservations.classificationID <> 5
OR (chargedReservations.classificationID = 5 AND chargedReservations.chargeValueInUSD <> 0.00 AND chargedReservations.itemCode NOT IN ('IPREFERMANUAL_I', 'PMS_I') )

GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
		chargedReservations.arrivalDate, MRT.guestLastName, 
		MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
		MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber
) AS main

UNION ALL


--Display New billy Iprefer manual point award
SELECT	main.currency AS currency
,		main.arrivalDate AS arrivalDate
,		main.guestLastName AS guestLastName
,		main.guestFirstName AS guestFirstName
,		main.confirmationNumber AS confirmationNumber
,		main.confirmationDate AS confirmationDate
,		main.consortia AS consortia
,		main.IATANumber AS IATANumber
,		main.rateTypeCode AS rateTypeCode
,		CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END AS iPreferNumber
,		main.averageDailyRate AS averageDailyRate
,		main.roomNights AS roomNights
,		main.reservationRevenue AS reservationRevenue
,		main.iPreferCharge AS iPreferCharge
,		main.commCharge AS commCharge
,		main.bookCharge AS bookCharge
,		main.surcharge AS surcharge
,		ISNULL(main.totalCharge, 0) AS totalCharge
,		ISNULL(REPLACE(main.itemGrouping, 'zzzzz! ', ''), 'Other Booking Sources' ) AS itemGrouping --remove hardcode value zzzzz!
,		main.billingDate as billingDate

FROM (
Select	chargedReservations.hotelCurrencyCode as currency, 
		chargedReservations.arrivalDate, 
		COALESCE(UPPER(MRT.guestLastName), UPPER(cpi.Last_Name)) AS guestLastName, 
		COALESCE(UPPER(MRT.guestFirstName), UPPER(cpi.First_Name)) AS guestFirstName, 
		chargedReservations.confirmationNumber, 
		ISNULL(MRT.confirmationDate, chargedReservations.billableDate) AS confirmationDate,
		ReservationBilling.dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		MRT.rateTypeCode,
		chargedReservations.loyaltyNumber AS iPreferNumber,
		Case When chargedReservations.roomNights <> 0
			THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
			ELSE 0 END as averageDailyRate, 
		chargedReservations.roomNights, 
		chargedReservations.roomRevenueInHotelCurrency as reservationRevenue, 
				SUM(case when chargedReservations.[classificationID] = 5 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS iPreferCharge,
		SUM(case when chargedReservations.[classificationID] = 1 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS bookCharge, 
		SUM(case when chargedReservations.[classificationID] = 2 -- commission
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS commCharge, 
		SUM(case when chargedReservations.classificationID = 3 -- surcharges
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS surcharge,
		SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
		MIN(case when chargedReservations.[classificationID] NOT IN (3,5) -- surcharges I prefer
				 then LTRIM(RTRIM(GPSopIC.itemGrouping))
				when chargedReservations.[classificationID] = 5 
				 then 'zzzzz! Hotel & Manual I Prefer Point Awards' --append zzzzz! on purpose for max logic to work
				 when chargedReservations.classificationID = 3 AND mrt.channel IN ('Channel Connect','IDS') AND itemCode = 'AMEXTMC'
					then 'zzzzz! Expedia/Amex OTA'
			END ) AS itemGrouping,
		chargedReservations.billableDate as billingDate
		
FROM	@GP_SOPNumber_ItemCodes GPSopIC

JOIN ReservationBilling.dbo.Charges chargedReservations with (nolock)
	ON  chargedReservations.sopNumber = @SOPNumber
	AND	chargedReservations.itemCode = GPSopIC.ITEMNMBR 

LEFT OUTER JOIN 	Reservations.dbo.mostrecenttransactions MRT with (nolock)
	ON	chargedReservations.confirmationNumber = MRT.confirmationNumber 

LEFT OUTER JOIN loyalty.dbo.Customer_Profile_Import cpi 
	ON chargedReservations.loyaltyNumber = cpi.iPrefer_Number

WHERE chargedReservations.classificationID = 5 AND chargedReservations.chargeValueInUSD <> 0.00 AND chargedReservations.itemCode IN ('IPREFERMANUAL_I', 'PMS_I') 

GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
		chargedReservations.arrivalDate, MRT.guestLastName, 
		MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
		MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber,
		 cpi.Last_Name,cpi.First_Name
) AS main

ORDER BY itemGrouping, arrivalDate, guestLastName, confirmationNumber
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
