USE Dimensional_Warehouse
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/24/2020 9:15:37 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_HotelParentCompany]'
GO



ALTER PROCEDURE [dbo].[Populate_HotelParentCompany]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @MinDate date,@MaxDate date;
	SELECT @MaxDate = MAX([Full Date]), @MinDate = MIN([Full Date]) FROM dim.DimDate

	;WITH cte_HotelParentCompany
	AS
	(
		SELECT DISTINCT h.hotelKey,dtStart.DateKey AS StartDate,dtEnd.DateKey AS EndDate,pc.CRM_ParentCompanyID,pc.ParentCompanyName,
				ROW_NUMBER() OVER(PARTITION BY h.hotelKey ORDER BY hpc.[StartDate] DESC) AS rowNum
		FROM Hotels.dbo.ParentCompany pc
			INNER JOIN Hotels.dbo.Hotel_ParentCompany hpc ON hpc.ParentCompanyID = pc.ParentCompanyID
			INNER JOIN dim.Hotel h ON h.sourceKey = hpc.HotelID
			LEFT JOIN dim.DimDate dtStart ON dtStart.[Full Date] = ISNULL(hpc.StartDate,@MinDate)
			LEFT JOIN dim.DimDate dtEnd ON dtEnd.[Full Date] = ISNULL(hpc.EndDate,@MaxDate)
		WHERE ISNULL(hpc.EndDate,@MaxDate) >= getdate()
	)
	MERGE INTO [dim].[HotelParentCompany] AS tgt
	USING
	(
		SELECT DISTINCT hotelKey,StartDate,EndDate,CRM_ParentCompanyID,ParentCompanyName
		FROM cte_HotelParentCompany
		WHERE rowNum = 1
	) AS src ON src.hotelKey = tgt.[hotelKey] AND src.CRM_ParentCompanyID = tgt.[CRM ID]
	WHEN MATCHED THEN
		UPDATE
			SET [Start Date] = src.StartDate,
				[End Date] = src.EndDate,
				[Parent Name] = src.ParentCompanyName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([hotelKey],[Start Date],[End Date],[CRM ID],[Parent Name])
		VALUES(hotelKey,StartDate,EndDate,CRM_ParentCompanyID,ParentCompanyName)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Hotel]'
GO


ALTER PROCEDURE [dbo].[Populate_Hotel]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE @maxInt int = 2147483647;

	;WITH cte_ResRev(HotelID,AvgNightlyRate)
	AS
	(
		SELECT h.Hotel_hotelID,SUM(td.reservationRevenue)/CONVERT(decimal(38,2),SUM(td.nights))/CONVERT(decimal(38,2),SUM(td.rooms)) AS AvgNightlyRate
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN Reservations.dbo.hotel h ON h.HotelID = t.HotelID
		WHERE ts.confirmationDate BETWEEN DATEADD(year,-1,GETDATE()) AND GETDATE()
		GROUP BY h.Hotel_hotelID
	),
	cte_Col(HotelID,Code,CollectionName,rowNum)
	AS
	(
		SELECT hcol.HotelID,col.Code,col.CollectionName,ROW_NUMBER() OVER(PARTITION BY hcol.HotelID ORDER BY ISNULL(col.Hierarchy,@maxInt))
		FROM Hotels.dbo.[Collection] col
			INNER JOIN Hotels.dbo.Hotel_Collection hcol ON hcol.CollectionID = col.CollectionID
	),
	cte_LOC(LocationID,CityName,StateCode,CountryName,PostalCodeName,RegionName)
	AS
	(
		SELECT l.LocationID,cty.CityName,st.StateCode,cnty.CountryName,post.PostalCodeName,reg.RegionName
		FROM Hotels.dbo.[Location] l
		LEFT JOIN Hotels.dbo.City cty ON cty.CityID = l.CityID
		LEFT JOIN Hotels.dbo.[State] st ON st.StateID = l.StateID
		LEFT JOIN Hotels.dbo.Country cnty ON cnty.CountryID = l.CountryID
		LEFT JOIN Hotels.dbo.PostalCode post ON post.PostalCodeID = l.PostalCodeID
		LEFT JOIN Hotels.dbo.Region reg ON reg.RegionID = l.RegionID
	),
	cte_User(HotelID,WindowsAccount,FirstName,LastName,PHG_RoleName)
	AS
	(
		SELECT pur.HotelID,pu.WindowsAccount,p.FirstName,p.LastName,pr.PHG_RoleName
		FROM Hotels.dbo.Hotel_PHG_User_Role pur
			INNER JOIN Hotels.dbo.PHG_User pu ON pu.PHG_UserID = pur.PHG_UserID
			INNER JOIN Hotels.dbo.PHG_Role pr ON pr.PHG_RoleID = pur.PHG_RoleID
			INNER JOIN Hotels.dbo.Person p ON p.PersonID = pu.PersonID
	),
	cte_HotelParent(HotelID,ParentCompanyID,rowNum)
	AS
	(
		SELECT hpc.HotelID,hpc.ParentCompanyID,ROW_NUMBER() OVER(PARTITION BY hpc.HotelID ORDER BY hpc.StartDate DESC,hpc.ParentRelationshipID) rowNum
		FROM Hotels.dbo.Hotel_ParentCompany hpc
	),
	cte_hotel([hotelCode],[hotelName],[hotelAddress],[hotelCity],[hotelSubState],[hotelState],[hotelCountry],
				[hotelPostalCode],Latitude,Longitude,[hotelType],[roomCount],[seasonalHotel],[hotelAverageNightlyRate],
				[phGeographicRegionCode],[phGeographicRegionName],[phAreaManagerUsername],[phAreaManagerName],
				[phRegionalDirectorUsername],[phRegionalDirectorName],[phRevenueAccountManagerUsername],
				[phRevenueAccountManager],[phRdSupervisorUsername],[phRdSupervisor],[phRamSupervisorUsername],[phRamSupervisor],
				[phPrimaryCollectionCode],[phPrimaryCollectionName],
				[hePrimaryCollectionCode],[hePrimaryCollectionName],
				[resPrimaryCollectionCode],[resPrimaryCollectionName],
				[primaryParent],[primary3rdParty],[primary3rdPartyCollection],[loyaltyParticipant],[loyaltyOptInParticipant],sourceKey,
				StatusCodeName,[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID],ReportingCurrency)
	AS
	(
		SELECT DISTINCT CASE WHEN h.SynXisCode IS NULL THEN ISNULL(hop.HotelCode,h.HotelCode) ELSE h.HotelCode END,
			h.HotelName,ISNULL(h.Address1,'Unknown') + ISNULL(' ' + h.Address2,''),ISNULL(loc.CityName,'Unknown'),'Unknown',
			ISNULL(loc.StateCode,'Unknown'),ISNULL(loc.CountryName,'Unknown'),ISNULL(loc.PostalCodeName,'Unknown'),ISNULL(h.Latitude,0),ISNULL(h.Longitude,0),'Unknown',
			ISNULL(h.Rooms,0),0,ISNULL(rev.AvgNightlyRate,0),
			ISNULL(loc.RegionName,'Unknown'),ISNULL(loc.RegionName,'Unknown'),
			ISNULL(usrArea.WindowsAccount,'Unknown'),ISNULL(usrArea.FirstName,'Unknown') + ' ' + ISNULL(usrArea.LastName,''),
			ISNULL(usrReg.WindowsAccount,'Unknown'),ISNULL(usrReg.FirstName,'Unknown') + ' ' + ISNULL(usrReg.LastName,''),
			ISNULL(usrRev.WindowsAccount,'Unknown'),ISNULL(usrRev.FirstName,'Unknown') + ' ' + ISNULL(usrRev.LastName,''),
			ISNULL(usrAD.WindowsAccount,'Unknown'),ISNULL(usrAD.FirstName,'Unknown') + ' ' + ISNULL(usrAD.LastName,''),
			ISNULL(usrRAM.WindowsAccount,'Unknown'),ISNULL(usrRAM.FirstName,'Unknown') + ' ' + ISNULL(usrRAM.LastName,''),
			ISNULL(ph.Code,'Unknown'),ISNULL(ph.CollectionName,'Unknown'),
			ISNULL(he.Code,'Unknown'),ISNULL(he.CollectionName,'Unknown'),
			ISNULL(res.Code,'Unknown'),ISNULL(res.CollectionName,'Unknown'),
			ISNULL(parent.ParentCompanyName,'Unknown'),'Unknown','Unknown',0,0,
			h.HotelID,sc.StatusCodeName,
			ISNULL(h.[SynXisCode],'Unknown'),ISNULL(CONVERT(nvarchar(50),h.[SynXisID]),'Unknown'),ISNULL(h.[OpenHospCode],'Unknown'),ISNULL(CONVERT(nvarchar(50),h.[OpenHospID]),'Unknown'),
			ISNULL(h.ReportingCurrency,'Unknown')
		FROM Hotels.[dbo].[Hotel] h
			LEFT JOIN Hotels.authority.Hotels_OpenHosp hop ON hop.openHospID = h.OpenHospID
			LEFT JOIN Hotels.[dbo].[StatusCode] sc ON sc.[StatusCodeID] = h.[StatusCodeID]
			LEFT JOIN cte_ResRev rev ON rev.HotelID = h.HotelID
			LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 1) ph ON ph.HotelID = h.HotelID
			LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 2 AND Code IN('HE','HW')) he ON he.HotelID = h.HotelID
			LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 2 AND Code IN('RES')) res ON res.HotelID = h.HotelID
			LEFT JOIN cte_HotelParent hparent ON hparent.HotelID = h.HotelID AND hparent.rowNum = 1
			LEFT JOIN Hotels.dbo.ParentCompany parent ON parent.ParentCompanyID = hparent.ParentCompanyID
			LEFT JOIN cte_LOC loc ON loc.LocationID = h.LocationID
			LEFT JOIN cte_User usrArea ON usrArea.HotelID = h.HotelID AND usrArea.PHG_RoleName = 'Area Managing Director'
			LEFT JOIN cte_User usrReg ON usrReg.HotelID = h.HotelID AND usrReg.PHG_RoleName = 'Regional Director'
			LEFT JOIN cte_User usrRev ON usrRev.HotelID = h.HotelID AND usrRev.PHG_RoleName = 'Revenue Account Manager'
			LEFT JOIN cte_User usrAD ON usrAD.HotelID = h.HotelID AND usrAD.PHG_RoleName = 'Account Director'
			LEFT JOIN cte_User usrRAM ON usrRAM.HotelID = h.HotelID AND usrRAM.PHG_RoleName = 'Regional Admin'
		WHERE h.HotelCode IS NOT NULL
			AND h.CRM_HotelID IS NOT NULL
	)
	MERGE INTO [dim].[Hotel] AS tgt
	USING
	(
		SELECT [hotelCode],[hotelName],[hotelAddress],[hotelCity],[hotelSubState],[hotelState],[hotelCountry],
				[hotelPostalCode],geography::Point(Latitude,Longitude,4326) AS [Hotel GeoCode],[hotelType],[roomCount],[seasonalHotel],[hotelAverageNightlyRate],
				[phGeographicRegionCode],[phGeographicRegionName],[phAreaManagerUsername],[phAreaManagerName],
				[phRegionalDirectorUsername],[phRegionalDirectorName],[phRevenueAccountManagerUsername],
				[phRevenueAccountManager],[phRdSupervisorUsername],[phRdSupervisor],[phRamSupervisorUsername],
				[phRamSupervisor],[phPrimaryCollectionCode],[phPrimaryCollectionName],[hePrimaryCollectionCode],
				[hePrimaryCollectionName],[resPrimaryCollectionCode],[resPrimaryCollectionName],[primaryParent],
				[primary3rdParty],[primary3rdPartyCollection],[loyaltyParticipant],[loyaltyOptInParticipant],sourceKey,StatusCodeName,
				[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID],ReportingCurrency
		FROM cte_hotel c
	) AS src ON src.sourceKey = tgt.[sourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [Hotel Code] = src.[hotelCode],
				[Hotel Name] = src.[hotelName],
				[Hotel Address] = src.[hotelAddress],
				[Hotel City] = src.[hotelCity],
				[Hotel Sub State] = src.[hotelSubState],
				[Hotel State] = src.[hotelState],
				[Hotel Country] = src.[hotelCountry],
				[Hotel Postal Code] = src.[hotelPostalCode],
				[Hotel GeoCode] = src.[Hotel GeoCode],
				[Hotel Type] = src.[hotelType],
				[Room Count] = src.[roomCount],
				[Seasonal Hotel] = src.[seasonalHotel],
				[Hotel Average Nightly Rate] = src.[hotelAverageNightlyRate],
				[PH Geographic Region Code] = src.[phGeographicRegionCode],
				[PH Geographic Region Name] = src.[phGeographicRegionName],
				[PH AMD User Name] = src.[phAreaManagerUsername],
				[PH AMD Name] = src.[phAreaManagerName],
				[PH RD User Name] = src.[phRegionalDirectorUsername],
				[PH RD Name] = src.[phRegionalDirectorName],
				[PH RAM User Name] = src.[phRevenueAccountManagerUsername],
				[PH RAM] = src.[phRevenueAccountManager],
				[PH RD Supervisor User Name] = src.[phRdSupervisorUsername],
				[PH RD Supervisor] = src.[phRdSupervisor],
				[PH RAM Supervisor User Name] = src.[phRamSupervisorUsername],
				[PH RAM Supervisor] = src.[phRamSupervisor],
				[PH Primary Collection Code] = src.[phPrimaryCollectionCode],
				[PH Primary Collection Name] = src.[phPrimaryCollectionName],
				[Historic Hotel Primary Collection Code] = src.[hePrimaryCollectionCode],
				[Historic Hotel Primary Collection Name] = src.[hePrimaryCollectionName],
				[Residence Hotel Primary Collection Code] = src.[resPrimaryCollectionCode],
				[Residence Hotel Primary Collection Name] = src.[resPrimaryCollectionName],
				[Primary Owner/Management] = src.[primaryParent],
				[Primary 3rd Party] = src.[primary3rdParty],
				[Primary 3rd Party Collection] = src.[primary3rdPartyCollection],
				[I Prefer Participant] = src.[loyaltyParticipant],
				[I Prefer Opt-In Participant] = src.[loyaltyOptInParticipant],
				[sourceKey] = src.sourceKey,
				[Status Code] = src.StatusCodeName,
				[SynXisCode] = src.[SynXisCode],
				[SynXisID] = src.[SynXisID],
				[OpenHospCode] = src.[OpenHospCode],
				[OpenHospID] = src.[OpenHospID],
				[Hotel Reporting Currency] = src.ReportingCurrency
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Hotel Code],[Hotel Name],[Hotel Address],[Hotel City],[Hotel Sub State],[Hotel State],[Hotel Country],[Hotel Postal Code],[Hotel GeoCode],[Hotel Type],[Room Count],[Seasonal Hotel],[Hotel Average Nightly Rate],[PH Geographic Region Code],[PH Geographic Region Name],[PH AMD User Name],[PH AMD Name],[PH RD User Name],[PH RD Name],[PH RAM User Name],[PH RAM],[PH RD Supervisor User Name],[PH RD Supervisor],[PH RAM Supervisor User Name],[PH RAM Supervisor],[PH Primary Collection Code],[PH Primary Collection Name],[Historic Hotel Primary Collection Code],[Historic Hotel Primary Collection Name],[Residence Hotel Primary Collection Code],[Residence Hotel Primary Collection Name],[Primary Owner/Management],[Primary 3rd Party],[Primary 3rd Party Collection],[I Prefer Participant],[I Prefer Opt-In Participant],[sourceKey],[Status Code],[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID],[Hotel Reporting Currency])
		VALUES([hotelCode],src.[hotelName],src.[hotelAddress],src.[hotelCity],src.[hotelSubState],src.[hotelState],src.[hotelCountry],src.[hotelPostalCode],src.[Hotel GeoCode],src.[hotelType],src.[roomCount],src.[seasonalHotel],src.[hotelAverageNightlyRate],src.[phGeographicRegionCode],src.[phGeographicRegionName],src.[phAreaManagerUsername],src.[phAreaManagerName],src.[phRegionalDirectorUsername],src.[phRegionalDirectorName],src.[phRevenueAccountManagerUsername],src.[phRevenueAccountManager],src.[phRdSupervisorUsername],src.[phRdSupervisor],src.[phRamSupervisorUsername],src.[phRamSupervisor],src.[phPrimaryCollectionCode],src.[phPrimaryCollectionName],src.[hePrimaryCollectionCode],src.[hePrimaryCollectionName],src.[resPrimaryCollectionCode],src.[resPrimaryCollectionName],src.[primaryParent],src.[primary3rdParty],src.[primary3rdPartyCollection],src.[loyaltyParticipant],src.[loyaltyOptInParticipant],src.sourceKey,src.StatusCodeName,src.[SynXisCode],src.[SynXisID],src.[OpenHospCode],src.[OpenHospID],src.ReportingCurrency)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
