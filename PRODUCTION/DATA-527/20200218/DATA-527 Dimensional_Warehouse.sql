USE Dimensional_Warehouse
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/18/2020 1:08:08 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_HotelParentCompany]'
GO



ALTER PROCEDURE [dbo].[Populate_HotelParentCompany]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @MinDate date,@MaxDate date;
	SELECT @MaxDate = MAX([Full Date]), @MinDate = MIN([Full Date]) FROM dim.DimDate

	;WITH cte_HotelParentCompany
	AS
	(
		SELECT DISTINCT h.hotelKey,dtStart.DateKey AS StartDate,dtEnd.DateKey AS EndDate,pc.CRM_ParentCompanyID,pc.ParentCompanyName,
				ROW_NUMBER() OVER(PARTITION BY h.hotelKey ORDER BY hpc.[StartDate] DESC) AS rowNum
		FROM Hotels.dbo.ParentCompany pc
			INNER JOIN Hotels.dbo.Hotel_ParentCompany hpc ON hpc.ParentCompanyID = pc.ParentCompanyID
			INNER JOIN dim.Hotel h ON h.sourceKey = hpc.HotelID
			LEFT JOIN dim.DimDate dtStart ON dtStart.[Full Date] = ISNULL(hpc.StartDate,@MinDate)
			LEFT JOIN dim.DimDate dtEnd ON dtEnd.[Full Date] = ISNULL(hpc.EndDate,@MaxDate)
		WHERE ISNULL(hpc.EndDate,@MaxDate) >= getdate()
	)
	MERGE INTO [dim].[HotelParentCompany] AS tgt
	USING
	(
		SELECT DISTINCT hotelKey,StartDate,EndDate,CRM_ParentCompanyID,ParentCompanyName
		FROM cte_HotelParentCompany
		WHERE rowNum = 1
	) AS src ON src.hotelKey = tgt.[hotelKey] AND src.CRM_ParentCompanyID = tgt.[CRM ID]
	WHEN MATCHED THEN
		UPDATE
			SET [Start Date] = src.StartDate,
				[End Date] = src.EndDate,
				[Parent Name] = src.ParentCompanyName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([hotelKey],[Start Date],[End Date],[CRM ID],[Parent Name])
		VALUES(hotelKey,StartDate,EndDate,CRM_ParentCompanyID,ParentCompanyName)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
