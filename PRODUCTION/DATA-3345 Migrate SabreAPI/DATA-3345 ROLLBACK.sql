USE Reservations
GO

/*
Run this script on:

        wus2-data-dp-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 1/25/2024 5:00:32 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[CRS_BookingSource]'
GO
ALTER TABLE [dbo].[CRS_BookingSource] DROP CONSTRAINT [FK_dbo_BookingSource_CROCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_DataSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_Guest_CompanyNameID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_Hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_LoyaltyProgramID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_PH_BookingSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_VoiceAgentID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[TravelAgent]'
GO
ALTER TABLE [dbo].[TravelAgent] DROP CONSTRAINT [FK_dbo_TravelAgent_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] DROP CONSTRAINT [FK_dbo_TravelAgent_LocationID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[ibeSource]'
GO
ALTER TABLE [dbo].[ibeSource] DROP CONSTRAINT [FK_dbo_ibeSource_auth_ibeSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_RoomCategory]'
GO
ALTER TABLE [map].[sabreAPI_RoomCategory] DROP CONSTRAINT [FK_map_sabreAPI_RoomCategory_RoomCategoryID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[BookingSource]'
GO
ALTER TABLE [sabreAPI].[BookingSource] DROP CONSTRAINT [FK_SabreAPI_BookingSource_ChannelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[BookingSource] DROP CONSTRAINT [FK_SabreAPI_BookingSource_CROCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[BookingSource] DROP CONSTRAINT [FK_SabreAPI_BookingSource_SecondarySourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[BookingSource] DROP CONSTRAINT [FK_SabreAPI_BookingSource_SubSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[BookingSource] DROP CONSTRAINT [FK_SabreAPI_BookingSource_xbeTemplateID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[Guest]'
GO
ALTER TABLE [sabreAPI].[Guest] DROP CONSTRAINT [FK_SabreAPI_Guest_Guest_EmailAddressID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Guest] DROP CONSTRAINT [FK_SabreAPI_Guest_LocationID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[Location]'
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [FK_SabreAPI_Location_Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [FK_SabreAPI_Location_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [FK_SabreAPI_Location_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [FK_SabreAPI_Location_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [FK_SabreAPI_Location_Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [FK_SabreAPI_Location_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[MostRecentTransaction]'
GO
ALTER TABLE [sabreAPI].[MostRecentTransaction] DROP CONSTRAINT [FK_SabreAPI_MostRecentTransaction_TransactionID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[TransactionStatus]'
GO
ALTER TABLE [sabreAPI].[TransactionStatus] DROP CONSTRAINT [FK_SabreAPI_TransactionStatus_actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[Transactions]'
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_BillingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_Consortia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_Guest_CompanyNameID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_Hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_LastModified_ChannelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_PMSRateTypeCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_RateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_TransactionsExtended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_UserName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [FK_SabreAPI_Transactions_VIP_Level]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[TravelAgent]'
GO
ALTER TABLE [sabreAPI].[TravelAgent] DROP CONSTRAINT [FK_SabreAPI_TravelAgent_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[TravelAgent] DROP CONSTRAINT [FK_SabreAPI_TravelAgent_LocationID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[TransactionStatus]'
GO
ALTER TABLE [dbo].[TransactionStatus] DROP CONSTRAINT [FK_dbo_TransactionStatus_actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[openHosp_CRS_BookingSource]'
GO
ALTER TABLE [map].[openHosp_CRS_BookingSource] DROP CONSTRAINT [FK_map_openHosp_CRS_BookingSource_BookingSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[pegasus_CRS_BookingSource]'
GO
ALTER TABLE [map].[pegasus_CRS_BookingSource] DROP CONSTRAINT [FK_map_pegasus_CRS_BookingSource_BookingSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_CRS_BookingSource]'
GO
ALTER TABLE [map].[sabreAPI_CRS_BookingSource] DROP CONSTRAINT [FK_map_sabreAPI_CRS_BookingSource_BookingSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_CRS_BookingSource]'
GO
ALTER TABLE [map].[synXis_CRS_BookingSource] DROP CONSTRAINT [FK_map_synXis_CRS_BookingSource_BookingSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[openHosp_Chain]'
GO
ALTER TABLE [map].[openHosp_Chain] DROP CONSTRAINT [FK_map_openHosp_Chain_intChainID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[pegasus_Chain]'
GO
ALTER TABLE [map].[pegasus_Chain] DROP CONSTRAINT [FK_map_pegasus_Chain_intChainID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_Chain]'
GO
ALTER TABLE [map].[sabreAPI_Chain] DROP CONSTRAINT [FK_map_sabreAPI_Chain_intChainID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_Chain]'
GO
ALTER TABLE [map].[synXis_Chain] DROP CONSTRAINT [FK_map_synXis_Chain_intChainID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_CorporateCode]'
GO
ALTER TABLE [map].[sabreAPI_CorporateCode] DROP CONSTRAINT [FK_map_sabreAPI_CorporateCode_CorporateCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_CorporateCode]'
GO
ALTER TABLE [map].[synXis_CorporateCode] DROP CONSTRAINT [FK_map_synXis_CorporateCode_CorporateCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [FK_dbo_Guest_Guest_EmailAddressID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [FK_dbo_Guest_LocationID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[openHosp_IATANumber]'
GO
ALTER TABLE [map].[openHosp_IATANumber] DROP CONSTRAINT [FK_map_openHosp_IATANumber_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[pegasus_IATANumber]'
GO
ALTER TABLE [map].[pegasus_IATANumber] DROP CONSTRAINT [FK_map_pegasus_IATANumber_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] DROP CONSTRAINT [FK_map_sabreAPI_IATANumber_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_IATANumber]'
GO
ALTER TABLE [map].[synXis_IATANumber] DROP CONSTRAINT [FK_map_synXis_IATANumber_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[openHosp_PromoCode]'
GO
ALTER TABLE [map].[openHosp_PromoCode] DROP CONSTRAINT [FK_map_openHosp_PromoCode_PromoCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[pegasus_PromoCode]'
GO
ALTER TABLE [map].[pegasus_PromoCode] DROP CONSTRAINT [FK_map_pegasus_PromoCode_PromoCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] DROP CONSTRAINT [FK_map_sabreAPI_PromoCode_PromoCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_PromoCode]'
GO
ALTER TABLE [map].[synXis_PromoCode] DROP CONSTRAINT [FK_map_synXis_PromoCode_PromoCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[openHosp_RateCode]'
GO
ALTER TABLE [map].[openHosp_RateCode] DROP CONSTRAINT [FK_map_openHosp_RateCode_RateTypeCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[pegasus_RateCode]'
GO
ALTER TABLE [map].[pegasus_RateCode] DROP CONSTRAINT [FK_map_pegasus_RateCode_RateTypeCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] DROP CONSTRAINT [FK_map_sabreAPI_RateCode_RateTypeCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_RateCode]'
GO
ALTER TABLE [map].[synXis_RateCode] DROP CONSTRAINT [FK_map_synXis_RateCode_RateTypeCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] DROP CONSTRAINT [FK_map_sabreAPI_RoomType_RoomTypeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_RoomType]'
GO
ALTER TABLE [map].[synXis_RoomType] DROP CONSTRAINT [FK_map_synXis_RoomType_RoomTypeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] DROP CONSTRAINT [FK_map_sabreAPI_TravelAgent_TravelAgentID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_TravelAgent]'
GO
ALTER TABLE [map].[synXis_TravelAgent] DROP CONSTRAINT [FK_map_synXis_TravelAgent_TravelAgentID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[openHosp_hotel]'
GO
ALTER TABLE [map].[openHosp_hotel] DROP CONSTRAINT [FK_map_openHosp_hotel_intHotelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[pegasus_hotel]'
GO
ALTER TABLE [map].[pegasus_hotel] DROP CONSTRAINT [FK_map_pegasus_hotel_intHotelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] DROP CONSTRAINT [FK_map_sabreAPI_hotel_intHotelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[synXis_hotel]'
GO
ALTER TABLE [map].[synXis_hotel] DROP CONSTRAINT [FK_map_synXis_hotel_intHotelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[ActionType]'
GO
ALTER TABLE [dbo].[ActionType] DROP CONSTRAINT [PK_dbo_ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Area]'
GO
ALTER TABLE [dbo].[Area] DROP CONSTRAINT [PK_dbo_Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CROCode]'
GO
ALTER TABLE [dbo].[CROCode] DROP CONSTRAINT [PK_dbo_CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CRS_BookingSource]'
GO
ALTER TABLE [dbo].[CRS_BookingSource] DROP CONSTRAINT [PK_dbo_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CRS_Channel]'
GO
ALTER TABLE [dbo].[CRS_Channel] DROP CONSTRAINT [PK_dbo_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CRS_SecondarySource]'
GO
ALTER TABLE [dbo].[CRS_SecondarySource] DROP CONSTRAINT [PK_dbo_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CRS_SubSource]'
GO
ALTER TABLE [dbo].[CRS_SubSource] DROP CONSTRAINT [PK_dbo_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Chain]'
GO
ALTER TABLE [dbo].[Chain] DROP CONSTRAINT [PK_dbo_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[City]'
GO
ALTER TABLE [dbo].[City] DROP CONSTRAINT [PK_dbo_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CorporateCode]'
GO
ALTER TABLE [dbo].[CorporateCode] DROP CONSTRAINT [PK_dbo_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Country]'
GO
ALTER TABLE [dbo].[Country] DROP CONSTRAINT [PK_dbo_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Guest_EmailAddress]'
GO
ALTER TABLE [dbo].[Guest_EmailAddress] DROP CONSTRAINT [PK_dbo_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[IATANumber]'
GO
ALTER TABLE [dbo].[IATANumber] DROP CONSTRAINT [PK_dbo_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [PK_dbo_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[PostalCode]'
GO
ALTER TABLE [dbo].[PostalCode] DROP CONSTRAINT [PK_dbo_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[PromoCode]'
GO
ALTER TABLE [dbo].[PromoCode] DROP CONSTRAINT [PK_dbo_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RateCode]'
GO
ALTER TABLE [dbo].[RateCode] DROP CONSTRAINT [PK_dbo_RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Region]'
GO
ALTER TABLE [dbo].[Region] DROP CONSTRAINT [PK_dbo_Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RoomCategory]'
GO
ALTER TABLE [dbo].[RoomCategory] DROP CONSTRAINT [PK_dbo_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RoomType]'
GO
ALTER TABLE [dbo].[RoomType] DROP CONSTRAINT [PK_dbo_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[State]'
GO
ALTER TABLE [dbo].[State] DROP CONSTRAINT [PK_dbo_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [PK_dbo_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[TravelAgent]'
GO
ALTER TABLE [dbo].[TravelAgent] DROP CONSTRAINT [PK_dbo_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[hotel]'
GO
ALTER TABLE [dbo].[hotel] DROP CONSTRAINT [PK_dbo_hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[ibeSource]'
GO
ALTER TABLE [dbo].[ibeSource] DROP CONSTRAINT [PK_dbo_xbeTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_RoomCategory]'
GO
ALTER TABLE [map].[sabreAPI_RoomCategory] DROP CONSTRAINT [PK_map_sabreAPI_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[ActionType]'
GO
ALTER TABLE [sabreAPI].[ActionType] DROP CONSTRAINT [PK_SabreAPI_ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Area]'
GO
ALTER TABLE [sabreAPI].[Area] DROP CONSTRAINT [PK_SabreAPI_Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[BillingDescription]'
GO
ALTER TABLE [sabreAPI].[BillingDescription] DROP CONSTRAINT [PK_SabreAPI_BillingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[BookingSource]'
GO
ALTER TABLE [sabreAPI].[BookingSource] DROP CONSTRAINT [PK_SabreAPI_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[CROCode]'
GO
ALTER TABLE [sabreAPI].[CROCode] DROP CONSTRAINT [PK_SabreAPI_CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Chain]'
GO
ALTER TABLE [sabreAPI].[Chain] DROP CONSTRAINT [PK_SabreAPI_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Channel]'
GO
ALTER TABLE [sabreAPI].[Channel] DROP CONSTRAINT [PK_SabreAPI_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[City]'
GO
ALTER TABLE [sabreAPI].[City] DROP CONSTRAINT [PK_SabreAPI_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Consortia]'
GO
ALTER TABLE [sabreAPI].[Consortia] DROP CONSTRAINT [PK_SabreAPI_Consortia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[CorporateCode]'
GO
ALTER TABLE [sabreAPI].[CorporateCode] DROP CONSTRAINT [PK_SabreAPI_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Country]'
GO
ALTER TABLE [sabreAPI].[Country] DROP CONSTRAINT [PK_SabreAPI_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Guest]'
GO
ALTER TABLE [sabreAPI].[Guest] DROP CONSTRAINT [PK_SabreAPI_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Guest_CompanyName]'
GO
ALTER TABLE [sabreAPI].[Guest_CompanyName] DROP CONSTRAINT [PK_SabreAPI_Guest_CompanyNameID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Guest_EmailAddress]'
GO
ALTER TABLE [sabreAPI].[Guest_EmailAddress] DROP CONSTRAINT [PK_SabreAPI_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[IATANumber]'
GO
ALTER TABLE [sabreAPI].[IATANumber] DROP CONSTRAINT [PK_SabreAPI_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Location]'
GO
ALTER TABLE [sabreAPI].[Location] DROP CONSTRAINT [PK_SabreAPI_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[LoyaltyNumber]'
GO
ALTER TABLE [sabreAPI].[LoyaltyNumber] DROP CONSTRAINT [PK_SabreAPI_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[LoyaltyProgram]'
GO
ALTER TABLE [sabreAPI].[LoyaltyProgram] DROP CONSTRAINT [PK_SabreAPI_LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[MostRecentTransaction]'
GO
ALTER TABLE [sabreAPI].[MostRecentTransaction] DROP CONSTRAINT [PK_SabreAPI_Reservation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[PMSRateTypeCode]'
GO
ALTER TABLE [sabreAPI].[PMSRateTypeCode] DROP CONSTRAINT [PK_SabreAPI_PMSRateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[PostalCode]'
GO
ALTER TABLE [sabreAPI].[PostalCode] DROP CONSTRAINT [PK_SabreAPI_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[PromoCode]'
GO
ALTER TABLE [sabreAPI].[PromoCode] DROP CONSTRAINT [PK_SabreAPI_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[RateCategory]'
GO
ALTER TABLE [sabreAPI].[RateCategory] DROP CONSTRAINT [PK_SabreAPI_RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[RateTypeCode]'
GO
ALTER TABLE [sabreAPI].[RateTypeCode] DROP CONSTRAINT [PK_SabreAPI_RateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Region]'
GO
ALTER TABLE [sabreAPI].[Region] DROP CONSTRAINT [PK_SabreAPI_Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[RoomCategory]'
GO
ALTER TABLE [sabreAPI].[RoomCategory] DROP CONSTRAINT [PK_SabreAPI_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[RoomType]'
GO
ALTER TABLE [sabreAPI].[RoomType] DROP CONSTRAINT [PK_SabreAPI_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[SecondarySource]'
GO
ALTER TABLE [sabreAPI].[SecondarySource] DROP CONSTRAINT [PK_SabreAPI_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[State]'
GO
ALTER TABLE [sabreAPI].[State] DROP CONSTRAINT [PK_SabreAPI_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[SubSource]'
GO
ALTER TABLE [sabreAPI].[SubSource] DROP CONSTRAINT [PK_SabreAPI_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[TransactionDetail]'
GO
ALTER TABLE [sabreAPI].[TransactionDetail] DROP CONSTRAINT [PK_SabreAPI_TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[TransactionStatus]'
GO
ALTER TABLE [sabreAPI].[TransactionStatus] DROP CONSTRAINT [PK_SabreAPI_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[TransactionsExtended]'
GO
ALTER TABLE [sabreAPI].[TransactionsExtended] DROP CONSTRAINT [PK_SabreAPI_TransactionsExtended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[Transactions]'
GO
ALTER TABLE [sabreAPI].[Transactions] DROP CONSTRAINT [PK_SabreAPI_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[TravelAgent]'
GO
ALTER TABLE [sabreAPI].[TravelAgent] DROP CONSTRAINT [PK_SabreAPI_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[UserName]'
GO
ALTER TABLE [sabreAPI].[UserName] DROP CONSTRAINT [PK_SabreAPI_UserName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[VIP_Level]'
GO
ALTER TABLE [sabreAPI].[VIP_Level] DROP CONSTRAINT [PK_SabreAPI_VIP_Level]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[hotel]'
GO
ALTER TABLE [sabreAPI].[hotel] DROP CONSTRAINT [PK_SabreAPI_hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[xbeTemplate]'
GO
ALTER TABLE [sabreAPI].[xbeTemplate] DROP CONSTRAINT [PK_SabreAPI_xbeTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[ActionType]'
GO
ALTER TABLE [dbo].[ActionType] DROP CONSTRAINT [DF_actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[ActionType]'
GO
ALTER TABLE [sabreAPI].[ActionType] DROP CONSTRAINT [DF_actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[RateTypeCode]'
GO
ALTER TABLE [sabreAPI].[RateTypeCode] DROP CONSTRAINT [DF_rateTaxInclusive]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[ActionType]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[ActionType]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[CROCode]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[CROCode]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[CRS_BookingSource]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[CRS_BookingSource]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[CRS_Channel]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[CRS_Channel]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[CRS_SecondarySource]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[CRS_SecondarySource]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[CRS_SubSource]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[CRS_SubSource]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[Chain]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[Chain]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_City_Text] from [dbo].[City]'
GO
DROP INDEX [IX_City_Text] ON [dbo].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[City]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[City]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_CorporateCodeID_corporationCode] from [dbo].[CorporateCode]'
GO
DROP INDEX [IX_CorporateCodeID_corporationCode] ON [dbo].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[CorporateCode]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[CorporateCode]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[Country]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[Country]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_emailAddress] from [dbo].[Guest_EmailAddress]'
GO
DROP INDEX [IX_emailAddress] ON [dbo].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[Guest_EmailAddress]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[Guest_EmailAddress]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IATANumberID_IATANumber] from [dbo].[IATANumber]'
GO
DROP INDEX [IX_IATANumberID_IATANumber] ON [dbo].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[IATANumber]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[IATANumber]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[Location]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[Location]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_PostalCode_Text_IC_synXisID_openHospID] from [dbo].[PostalCode]'
GO
DROP INDEX [UX_PostalCode_Text_IC_synXisID_openHospID] ON [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[PostalCode]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[PostalCode]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PromoCodeID_promotionalCode] from [dbo].[PromoCode]'
GO
DROP INDEX [IX_PromoCodeID_promotionalCode] ON [dbo].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[PromoCode]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[PromoCode]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RateCodeID_RateCode] from [dbo].[RateCode]'
GO
DROP INDEX [IX_RateCodeID_RateCode] ON [dbo].[RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[RateCode]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[RateCode]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[Region]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[Region]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_RoomTypeID_roomTypeName] from [dbo].[RoomType]'
GO
DROP INDEX [UX_RoomTypeID_roomTypeName] ON [dbo].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RoomTypeID_roomTypeName_roomTypeCode] from [dbo].[RoomType]'
GO
DROP INDEX [IX_RoomTypeID_roomTypeName_roomTypeCode] ON [dbo].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[RoomType]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[RoomType]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_State_Text] from [dbo].[State]'
GO
DROP INDEX [IX_State_Text] ON [dbo].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[State]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[State]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey] from [dbo].[Transactions]'
GO
DROP INDEX [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey_CSV] from [dbo].[Transactions]'
GO
DROP INDEX [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey_CSV] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IATANumberID_IC_TransactionID_TravelAgentID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_IATANumberID_IC_TransactionID_TravelAgentID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_POPULATE_FACT_RES] from [dbo].[Transactions]'
GO
DROP INDEX [IX_POPULATE_FACT_RES] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_confirmationNumber] from [dbo].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_confirmationNumber] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_IC_FK_Cols] from [dbo].[Transactions]'
GO
DROP INDEX [IX_confirmationNumber_IC_FK_Cols] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_confirmationNumber_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [UX_confirmationNumber_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [IX_confirmationNumber_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_CRS_BookingSourceID_IC_sourceKey_TransactionID_confirmationNumber_transactionTimeStamp] from [dbo].[Transactions]'
GO
DROP INDEX [IX_CRS_BookingSourceID_IC_sourceKey_TransactionID_confirmationNumber_transactionTimeStamp] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_GuestID_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [IX_GuestID_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionDetailID_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [IX_TransactionDetailID_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionStatusID_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [IX_TransactionStatusID_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LoyaltyNumberID_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [IX_LoyaltyNumberID_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_transactionTimeStamp_IC_Others] from [dbo].[Transactions]'
GO
DROP INDEX [IX_transactionTimeStamp_IC_Others] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionStatusID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_TransactionStatusID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionDetailID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_TransactionDetailID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_CorporateCodeID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_CorporateCodeID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RoomTypeID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_RoomTypeID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RateCategoryID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_RateCategoryID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_CRS_BookingSourceID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_CRS_BookingSourceID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_GuestID_LoyaltyNumberID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_GuestID_LoyaltyNumberID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LoyaltyNumberID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_LoyaltyNumberID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_GuestID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_GuestID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RateCodeID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_RateCodeID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PromoCodeID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_PromoCodeID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TravelAgentID_IC_sourceKey] from [dbo].[Transactions]'
GO
DROP INDEX [IX_TravelAgentID_IC_sourceKey] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_VoiceAgentID_IC_sourceKey] from [dbo].[Transactions]'
GO
DROP INDEX [IX_VoiceAgentID_IC_sourceKey] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IATANumberID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_IATANumberID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_HotelID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_HotelID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ChainID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_ChainID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Guest_CompanyNameID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_Guest_CompanyNameID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PH_BookingSourceID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_PH_BookingSourceID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_DataSourceID_IC_sourceKey] from [dbo].[Transactions]'
GO
DROP INDEX [IX_DataSourceID_IC_sourceKey] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TravelAgentID_IATANumberID] from [dbo].[TravelAgent]'
GO
DROP INDEX [IX_TravelAgentID_IATANumberID] ON [dbo].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IATANumberID] from [dbo].[TravelAgent]'
GO
DROP INDEX [IX_IATANumberID] ON [dbo].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[TravelAgent]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[TravelAgent]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Hotel_hotelID] from [dbo].[hotel]'
GO
DROP INDEX [IX_Hotel_hotelID] ON [dbo].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[hotel]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[hotel]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_auth_ibeSourceID] from [dbo].[ibeSource]'
GO
DROP INDEX [IX_auth_ibeSourceID] ON [dbo].[ibeSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[ibeSource]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[ibeSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[ibeSource]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[ibeSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Area_Text] from [sabreAPI].[Area]'
GO
DROP INDEX [IX_Area_Text] ON [sabreAPI].[Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_Area] from [sabreAPI].[Area]'
GO
DROP INDEX [UX_SabreAPI_Area] ON [sabreAPI].[Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_billingDescription] from [sabreAPI].[BillingDescription]'
GO
DROP INDEX [UX_SabreAPI_billingDescription] ON [sabreAPI].[BillingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ChannelID_SecondarySourceID_SubSourceID_CROCodeID_xbeTemplateID] from [sabreAPI].[BookingSource]'
GO
DROP INDEX [IX_ChannelID_SecondarySourceID_SubSourceID_CROCodeID_xbeTemplateID] ON [sabreAPI].[BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_CROCode] from [sabreAPI].[CROCode]'
GO
DROP INDEX [UX_SabreAPI_CROCode] ON [sabreAPI].[CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_ChainID_IC_ChainName] from [sabreAPI].[Chain]'
GO
DROP INDEX [UX_SabreAPI_ChainID_IC_ChainName] ON [sabreAPI].[Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_channel] from [sabreAPI].[Channel]'
GO
DROP INDEX [UX_SabreAPI_channel] ON [sabreAPI].[Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_City] from [sabreAPI].[City]'
GO
DROP INDEX [UX_SabreAPI_City] ON [sabreAPI].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_consortiaName] from [sabreAPI].[Consortia]'
GO
DROP INDEX [UX_SabreAPI_consortiaName] ON [sabreAPI].[Consortia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_corporationCode] from [sabreAPI].[CorporateCode]'
GO
DROP INDEX [UX_SabreAPI_corporationCode] ON [sabreAPI].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Country_Text] from [sabreAPI].[Country]'
GO
DROP INDEX [IX_Country_Text] ON [sabreAPI].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_Country] from [sabreAPI].[Country]'
GO
DROP INDEX [UX_SabreAPI_Country] ON [sabreAPI].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Guest_EmailAddressID_hashKey] from [sabreAPI].[Guest]'
GO
DROP INDEX [IX_Guest_EmailAddressID_hashKey] ON [sabreAPI].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_hashKey] from [sabreAPI].[Guest]'
GO
DROP INDEX [IX_hashKey] ON [sabreAPI].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_CompanyName] from [sabreAPI].[Guest_CompanyName]'
GO
DROP INDEX [UX_SabreAPI_CompanyName] ON [sabreAPI].[Guest_CompanyName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_EmailAddress] from [sabreAPI].[Guest_EmailAddress]'
GO
DROP INDEX [UX_SabreAPI_EmailAddress] ON [sabreAPI].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_IATANumber] from [sabreAPI].[IATANumber]'
GO
DROP INDEX [UX_SabreAPI_IATANumber] ON [sabreAPI].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_IATANumber_str] from [sabreAPI].[IATANumber]'
GO
DROP INDEX [UX_SabreAPI_IATANumber_str] ON [sabreAPI].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_CityID_StateID_CountryID_PostalCodeID_AreaID_RegionID] from [sabreAPI].[Location]'
GO
DROP INDEX [UX_SabreAPI_CityID_StateID_CountryID_PostalCodeID_AreaID_RegionID] ON [sabreAPI].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_loyaltyNumber] from [sabreAPI].[LoyaltyNumber]'
GO
DROP INDEX [UX_SabreAPI_loyaltyNumber] ON [sabreAPI].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_loyaltyProgram] from [sabreAPI].[LoyaltyProgram]'
GO
DROP INDEX [UX_SabreAPI_loyaltyProgram] ON [sabreAPI].[LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_TransactionID] from [sabreAPI].[MostRecentTransaction]'
GO
DROP INDEX [UX_TransactionID] ON [sabreAPI].[MostRecentTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_PMSRateTypeCode] from [sabreAPI].[PMSRateTypeCode]'
GO
DROP INDEX [UX_SabreAPI_PMSRateTypeCode] ON [sabreAPI].[PMSRateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_PostalCode] from [sabreAPI].[PostalCode]'
GO
DROP INDEX [UX_SabreAPI_PostalCode] ON [sabreAPI].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_promotionalCode] from [sabreAPI].[PromoCode]'
GO
DROP INDEX [UX_SabreAPI_promotionalCode] ON [sabreAPI].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_rateCategoryName_rateCategoryCode] from [sabreAPI].[RateCategory]'
GO
DROP INDEX [UX_SabreAPI_rateCategoryName_rateCategoryCode] ON [sabreAPI].[RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_All_Columns] from [sabreAPI].[RateTypeCode]'
GO
DROP INDEX [UX_SabreAPI_All_Columns] ON [sabreAPI].[RateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Region_Text] from [sabreAPI].[Region]'
GO
DROP INDEX [IX_Region_Text] ON [sabreAPI].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_Region] from [sabreAPI].[Region]'
GO
DROP INDEX [UX_SabreAPI_Region] ON [sabreAPI].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_roomTypeName_roomTypeCode_PMSRoomTypeCode] from [sabreAPI].[RoomType]'
GO
DROP INDEX [UX_SabreAPI_roomTypeName_roomTypeCode_PMSRoomTypeCode] ON [sabreAPI].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_secondarySource] from [sabreAPI].[SecondarySource]'
GO
DROP INDEX [UX_SabreAPI_secondarySource] ON [sabreAPI].[SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_State] from [sabreAPI].[State]'
GO
DROP INDEX [UX_SabreAPI_State] ON [sabreAPI].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_subSource_subSourceCode] from [sabreAPI].[SubSource]'
GO
DROP INDEX [UX_SabreAPI_subSource_subSourceCode] ON [sabreAPI].[SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_hashKey] from [sabreAPI].[TransactionDetail]'
GO
DROP INDEX [IX_confirmationNumber_hashKey] ON [sabreAPI].[TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_hashKey_IC_All] from [sabreAPI].[TransactionStatus]'
GO
DROP INDEX [IX_confirmationNumber_hashKey_IC_All] ON [sabreAPI].[TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_hashKey] from [sabreAPI].[TransactionsExtended]'
GO
DROP INDEX [IX_confirmationNumber_hashKey] ON [sabreAPI].[TransactionsExtended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionID_IC_BillingDescriptionID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_TransactionID_IC_BillingDescriptionID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_KeyColumns] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_KeyColumns] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionDetailID_TransactionsExtendedID_IC_Others] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_TransactionDetailID_TransactionsExtendedID_IC_Others] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_POPULATE_DBO] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_POPULATE_DBO] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_TransactionStatusID_IC_transactionTimeStamp_hashKey] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_confirmationNumber_TransactionStatusID_IC_transactionTimeStamp_hashKey] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_hashKey] from [sabreAPI].[TravelAgent]'
GO
DROP INDEX [IX_hashKey] ON [sabreAPI].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_userName] from [sabreAPI].[UserName]'
GO
DROP INDEX [UX_SabreAPI_userName] ON [sabreAPI].[UserName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_vipLevel] from [sabreAPI].[VIP_Level]'
GO
DROP INDEX [UX_vipLevel] ON [sabreAPI].[VIP_Level]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_HotelCode_HotelID] from [sabreAPI].[hotel]'
GO
DROP INDEX [IX_HotelCode_HotelID] ON [sabreAPI].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_SabreAPI_xbeTemplateName_xbeShellName] from [sabreAPI].[xbeTemplate]'
GO
DROP INDEX [UX_SabreAPI_xbeTemplateName_xbeShellName] ON [sabreAPI].[xbeTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[load_SabreAPI_Test_Import]'
GO
DROP PROCEDURE [dbo].[load_SabreAPI_Test_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[BillingDescriptionMapping]'
GO
DROP TABLE [sabreAPI].[BillingDescriptionMapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[CalculatedBillingDescription_old]'
GO
DROP FUNCTION [sabreAPI].[CalculatedBillingDescription_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[UpdateCancelledReservations]'
GO
DROP PROCEDURE [sabreAPI].[UpdateCancelledReservations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[vw_Transactions]'
GO
DROP VIEW [sabreAPI].[vw_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [temp].[Populate_TempTables]'
GO
DROP PROCEDURE [temp].[Populate_TempTables]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[RawDataForLoading]'
GO
DROP FUNCTION [sabreAPI].[RawDataForLoading]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[vw_Location]'
GO
DROP VIEW [sabreAPI].[vw_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[vw_BookingSource]'
GO
DROP VIEW [sabreAPI].[vw_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[SabreAPI_ImportIntoETL]'
GO
DROP PROCEDURE [dbo].[SabreAPI_ImportIntoETL]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[LoadTransactions]'
GO
DROP PROCEDURE [sabreAPI].[LoadTransactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Guest_CompanyName]'
GO
DROP PROCEDURE [sabreAPI].[load_Guest_CompanyName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Guest_EmailAddress]'
GO
DROP PROCEDURE [sabreAPI].[load_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Hotel]'
GO
DROP PROCEDURE [sabreAPI].[load_Hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_IATANumber]'
GO
DROP PROCEDURE [sabreAPI].[load_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_LocationTables]'
GO
DROP PROCEDURE [sabreAPI].[load_LocationTables]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_LoyaltyNumber]'
GO
DROP PROCEDURE [sabreAPI].[load_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_LoyaltyProgram]'
GO
DROP PROCEDURE [sabreAPI].[load_LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_PMSRateTypeCode]'
GO
DROP PROCEDURE [sabreAPI].[load_PMSRateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_PromoCode]'
GO
DROP PROCEDURE [sabreAPI].[load_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_RateCategory]'
GO
DROP PROCEDURE [sabreAPI].[load_RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_RateTypeCode]'
GO
DROP PROCEDURE [sabreAPI].[load_RateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_RawDataForLoading_Temp]'
GO
DROP PROCEDURE [sabreAPI].[load_RawDataForLoading_Temp]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_RoomCategory]'
GO
DROP PROCEDURE [sabreAPI].[load_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_RoomType]'
GO
DROP PROCEDURE [sabreAPI].[load_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_SecondarySource]'
GO
DROP PROCEDURE [sabreAPI].[load_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_SubSource]'
GO
DROP PROCEDURE [sabreAPI].[load_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_TransactionDetail]'
GO
DROP PROCEDURE [sabreAPI].[load_TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Transactions]'
GO
DROP PROCEDURE [sabreAPI].[load_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_TransactionsExtended]'
GO
DROP PROCEDURE [sabreAPI].[load_TransactionsExtended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_TransactionStatus]'
GO
DROP PROCEDURE [sabreAPI].[load_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_TravelAgent]'
GO
DROP PROCEDURE [sabreAPI].[load_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_UserName]'
GO
DROP PROCEDURE [sabreAPI].[load_UserName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_VIP_Level]'
GO
DROP PROCEDURE [sabreAPI].[load_VIP_Level]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_xbeTemplate]'
GO
DROP PROCEDURE [sabreAPI].[load_xbeTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[POPULATE_temp_sabreAPI_RawDataForLoading]'
GO
DROP PROCEDURE [dbo].[POPULATE_temp_sabreAPI_RawDataForLoading]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[CalculatedBillingDescription]'
GO
DROP FUNCTION [sabreAPI].[CalculatedBillingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[CalculatedChannel]'
GO
DROP FUNCTION [sabreAPI].[CalculatedChannel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[CalculatedReservationRevenue]'
GO
DROP FUNCTION [sabreAPI].[CalculatedReservationRevenue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[ChainNameAndIDMapping]'
GO
DROP TABLE [sabreAPI].[ChainNameAndIDMapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[SubsourceMapping]'
GO
DROP TABLE [sabreAPI].[SubsourceMapping]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[PopulateMostRecentTransactions]'
GO
DROP PROCEDURE [sabreAPI].[PopulateMostRecentTransactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[PhoneNumber_Cleaned]'
GO
DROP FUNCTION [dbo].[PhoneNumber_Cleaned]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_ActionType]'
GO
DROP PROCEDURE [sabreAPI].[load_ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_BillingDescription]'
GO
DROP PROCEDURE [sabreAPI].[load_BillingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Populate_Location_SabreAPI]'
GO
DROP PROCEDURE [dbo].[Populate_Location_SabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[Populate_sabreAPI_mapTables]'
GO
DROP PROCEDURE [map].[Populate_sabreAPI_mapTables]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Populate_RoomCategory]'
GO
DROP PROCEDURE [dbo].[Populate_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[RoomCategory]'
GO
DROP TABLE [sabreAPI].[RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_BookingSource]'
GO
DROP PROCEDURE [sabreAPI].[load_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Chain]'
GO
DROP PROCEDURE [sabreAPI].[load_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Channel]'
GO
DROP PROCEDURE [sabreAPI].[load_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [temp].[sabreAPI_RawDataForLoading]'
GO
DROP TABLE [temp].[sabreAPI_RawDataForLoading]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Consortia]'
GO
DROP PROCEDURE [sabreAPI].[load_Consortia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_CorporateCode]'
GO
DROP PROCEDURE [sabreAPI].[load_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_CROCode]'
GO
DROP PROCEDURE [sabreAPI].[load_CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[load_Guest]'
GO
DROP PROCEDURE [sabreAPI].[load_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[RawDataForLoading_Temp]'
GO
DROP TABLE [sabreAPI].[RawDataForLoading_Temp]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[ActionType]'
GO
DROP TABLE [sabreAPI].[ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[VIP_Level]'
GO
DROP TABLE [sabreAPI].[VIP_Level]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[UserName]'
GO
DROP TABLE [sabreAPI].[UserName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[TravelAgent]'
GO
DROP TABLE [sabreAPI].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[TransactionStatus]'
GO
DROP TABLE [sabreAPI].[TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[TransactionsExtended]'
GO
DROP TABLE [sabreAPI].[TransactionsExtended]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[TransactionDetail]'
GO
DROP TABLE [sabreAPI].[TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[RoomType]'
GO
DROP TABLE [sabreAPI].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[RateTypeCode]'
GO
DROP TABLE [sabreAPI].[RateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[RateCategory]'
GO
DROP TABLE [sabreAPI].[RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[PromoCode]'
GO
DROP TABLE [sabreAPI].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[PMSRateTypeCode]'
GO
DROP TABLE [sabreAPI].[PMSRateTypeCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[LoyaltyProgram]'
GO
DROP TABLE [sabreAPI].[LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[LoyaltyNumber]'
GO
DROP TABLE [sabreAPI].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[IATANumber]'
GO
DROP TABLE [sabreAPI].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[hotel]'
GO
DROP TABLE [sabreAPI].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Guest_CompanyName]'
GO
DROP TABLE [sabreAPI].[Guest_CompanyName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[CorporateCode]'
GO
DROP TABLE [sabreAPI].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Consortia]'
GO
DROP TABLE [sabreAPI].[Consortia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Chain]'
GO
DROP TABLE [sabreAPI].[Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[BillingDescription]'
GO
DROP TABLE [sabreAPI].[BillingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[MostRecentTransaction]'
GO
DROP TABLE [sabreAPI].[MostRecentTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Transactions]'
GO
DROP TABLE [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[State]'
GO
DROP TABLE [sabreAPI].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Region]'
GO
DROP TABLE [sabreAPI].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[PostalCode]'
GO
DROP TABLE [sabreAPI].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Country]'
GO
DROP TABLE [sabreAPI].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[City]'
GO
DROP TABLE [sabreAPI].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Area]'
GO
DROP TABLE [sabreAPI].[Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Location]'
GO
DROP TABLE [sabreAPI].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Guest]'
GO
DROP TABLE [sabreAPI].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Guest_EmailAddress]'
GO
DROP TABLE [sabreAPI].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[xbeTemplate]'
GO
DROP TABLE [sabreAPI].[xbeTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[SubSource]'
GO
DROP TABLE [sabreAPI].[SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[SecondarySource]'
GO
DROP TABLE [sabreAPI].[SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[CROCode]'
GO
DROP TABLE [sabreAPI].[CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[BookingSource]'
GO
DROP TABLE [sabreAPI].[BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[Channel]'
GO
DROP TABLE [sabreAPI].[Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_RoomCategory]'
GO
DROP TABLE [map].[sabreAPI_RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RoomCategory]'
GO
DROP TABLE [dbo].[RoomCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CROCode]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_CROCode]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROCodeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[auth_CRO_CodeID] [int] NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_CROCode] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_1_CROCode]([synXisID], [openHospID], [CROCodeID], [auth_CRO_CodeID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [CROCodeID], [auth_CRO_CodeID], [sabreAPI_ID], [pegasusID] FROM [dbo].[CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_CROCode] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CROCode]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_CROCode]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CROCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_CROCode]', N'CROCode', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_CROCode] on [dbo].[CROCode]'
GO
ALTER TABLE [dbo].[CROCode] ADD CONSTRAINT [PK_dbo_CROCode] PRIMARY KEY CLUSTERED ([CROCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CROCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CROCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CROCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CROCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CRS_BookingSource]'
GO
CREATE TABLE [dbo].[RG_Recovery_2_CRS_BookingSource]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookingSourceID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ChannelID] [int] NULL,
[SecondarySourceID] [int] NULL,
[SubSourceID] [int] NULL,
[CROCodeID] [int] NULL,
[ibeSourceNameID] [int] NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_2_CRS_BookingSource] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_2_CRS_BookingSource]([synXisID], [openHospID], [BookingSourceID], [ChannelID], [SecondarySourceID], [SubSourceID], [CROCodeID], [ibeSourceNameID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [BookingSourceID], [ChannelID], [SecondarySourceID], [SubSourceID], [CROCodeID], [ibeSourceNameID], [sabreAPI_ID], [pegasusID] FROM [dbo].[CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_2_CRS_BookingSource] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CRS_BookingSource]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_2_CRS_BookingSource]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_2_CRS_BookingSource]', N'CRS_BookingSource', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_BookingSource] on [dbo].[CRS_BookingSource]'
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD CONSTRAINT [PK_dbo_BookingSource] PRIMARY KEY CLUSTERED ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_BookingSource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_BookingSource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Guest_EmailAddress]'
GO
CREATE TABLE [dbo].[RG_Recovery_3_Guest_EmailAddress]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Guest_EmailAddressID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[emailAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_3_Guest_EmailAddress] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_3_Guest_EmailAddress]([synXisID], [openHospID], [Guest_EmailAddressID], [emailAddress], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [Guest_EmailAddressID], [emailAddress], [sabreAPI_ID], [pegasusID] FROM [dbo].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_3_Guest_EmailAddress] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Guest_EmailAddress]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_3_Guest_EmailAddress]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_3_Guest_EmailAddress]', N'Guest_EmailAddress', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Guest_EmailAddress] on [dbo].[Guest_EmailAddress]'
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD CONSTRAINT [PK_dbo_Guest_EmailAddress] PRIMARY KEY CLUSTERED ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_emailAddress] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_emailAddress] ON [dbo].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Guest_EmailAddress] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Guest_EmailAddress] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Location]'
GO
CREATE TABLE [dbo].[RG_Recovery_4_Location]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[CityID] [int] NOT NULL,
[StateID] [int] NOT NULL,
[CountryID] [int] NOT NULL,
[PostalCodeID] [int] NOT NULL,
[RegionID] [int] NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[AreaID] [int] NOT NULL,
[Locations_LocationID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_4_Location] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_4_Location]([synXisID], [openHospID], [LocationID], [CityID], [StateID], [CountryID], [PostalCodeID], [RegionID], [AreaID], [Locations_LocationID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [LocationID], [CityID], [StateID], [CountryID], [PostalCodeID], [RegionID], [AreaID], [Locations_LocationID], [sabreAPI_ID], [pegasusID] FROM [dbo].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_4_Location] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Location]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_4_Location]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_4_Location]', N'Location', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Location] on [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [PK_dbo_Location] PRIMARY KEY CLUSTERED ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Location]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Location] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Location]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Location] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[ibeSource]'
GO
CREATE TABLE [dbo].[RG_Recovery_5_ibeSource]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ibeSourceID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[auth_ibeSourceID] [int] NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_5_ibeSource] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_5_ibeSource]([synXisID], [openHospID], [ibeSourceID], [auth_ibeSourceID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [ibeSourceID], [auth_ibeSourceID], [sabreAPI_ID], [pegasusID] FROM [dbo].[ibeSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_5_ibeSource] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[ibeSource]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_5_ibeSource]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[ibeSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_5_ibeSource]', N'ibeSource', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_xbeTemplate] on [dbo].[ibeSource]'
GO
ALTER TABLE [dbo].[ibeSource] ADD CONSTRAINT [PK_dbo_xbeTemplate] PRIMARY KEY CLUSTERED ([ibeSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_auth_ibeSourceID] on [dbo].[ibeSource]'
GO
CREATE NONCLUSTERED INDEX [IX_auth_ibeSourceID] ON [dbo].[ibeSource] ([auth_ibeSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[ibeSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[ibeSource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[ibeSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[ibeSource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Area]'
GO
CREATE TABLE [dbo].[RG_Recovery_6_Area]
(
[AreaID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Area_Text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_6_Area] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_6_Area]([AreaID], [Area_Text], [synXisID], [openHospID], [sabreAPI_ID]) SELECT [AreaID], [Area_Text], [synXisID], [openHospID], [sabreAPI_ID] FROM [dbo].[Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_6_Area] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Area]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_6_Area]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Area]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_6_Area]', N'Area', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Area] on [dbo].[Area]'
GO
ALTER TABLE [dbo].[Area] ADD CONSTRAINT [PK_dbo_Area] PRIMARY KEY CLUSTERED ([AreaID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[City]'
GO
CREATE TABLE [dbo].[RG_Recovery_7_City]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CityID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[City_Text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[Locations_CityID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_7_City] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_7_City]([synXisID], [openHospID], [CityID], [City_Text], [Locations_CityID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [CityID], [City_Text], [Locations_CityID], [sabreAPI_ID], [pegasusID] FROM [dbo].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_7_City] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[City]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_7_City]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_7_City]', N'City', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_City] on [dbo].[City]'
GO
ALTER TABLE [dbo].[City] ADD CONSTRAINT [PK_dbo_City] PRIMARY KEY CLUSTERED ([CityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_City_Text] on [dbo].[City]'
GO
CREATE NONCLUSTERED INDEX [IX_City_Text] ON [dbo].[City] ([City_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[City]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[City] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[City]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[City] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Country]'
GO
CREATE TABLE [dbo].[RG_Recovery_8_Country]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Country_Text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[Locations_CountryID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_8_Country] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_8_Country]([synXisID], [openHospID], [CountryID], [Country_Text], [Locations_CountryID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [CountryID], [Country_Text], [Locations_CountryID], [sabreAPI_ID], [pegasusID] FROM [dbo].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_8_Country] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Country]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_8_Country]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_8_Country]', N'Country', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Country] on [dbo].[Country]'
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [PK_dbo_Country] PRIMARY KEY CLUSTERED ([CountryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Country]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Country] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Country]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Country] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[PostalCode]'
GO
CREATE TABLE [dbo].[RG_Recovery_9_PostalCode]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCodeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[PostalCode_Text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[Locations_PostalCodeID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_9_PostalCode] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_9_PostalCode]([synXisID], [openHospID], [PostalCodeID], [PostalCode_Text], [Locations_PostalCodeID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [PostalCodeID], [PostalCode_Text], [Locations_PostalCodeID], [sabreAPI_ID], [pegasusID] FROM [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_9_PostalCode] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[PostalCode]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_9_PostalCode]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_9_PostalCode]', N'PostalCode', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_PostalCode] on [dbo].[PostalCode]'
GO
ALTER TABLE [dbo].[PostalCode] ADD CONSTRAINT [PK_dbo_PostalCode] PRIMARY KEY CLUSTERED ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[PostalCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PostalCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[PostalCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PostalCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_PostalCode_Text_IC_synXisID_openHospID] on [dbo].[PostalCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PostalCode_Text_IC_synXisID_openHospID] ON [dbo].[PostalCode] ([PostalCode_Text]) INCLUDE ([synXisID], [openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Region]'
GO
CREATE TABLE [dbo].[RG_Recovery_10_Region]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Region_Text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end),
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_10_Region] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_10_Region]([synXisID], [openHospID], [RegionID], [Region_Text], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [RegionID], [Region_Text], [sabreAPI_ID], [pegasusID] FROM [dbo].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_10_Region] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Region]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_10_Region]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Region]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_10_Region]', N'Region', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Region] on [dbo].[Region]'
GO
ALTER TABLE [dbo].[Region] ADD CONSTRAINT [PK_dbo_Region] PRIMARY KEY CLUSTERED ([RegionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Region]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Region] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Region]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Region] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[State]'
GO
CREATE TABLE [dbo].[RG_Recovery_11_State]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[State_Text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[Locations_StateID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_11_State] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_11_State]([synXisID], [openHospID], [StateID], [State_Text], [Locations_StateID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [StateID], [State_Text], [Locations_StateID], [sabreAPI_ID], [pegasusID] FROM [dbo].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_11_State] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[State]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_11_State]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_11_State]', N'State', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_State] on [dbo].[State]'
GO
ALTER TABLE [dbo].[State] ADD CONSTRAINT [PK_dbo_State] PRIMARY KEY CLUSTERED ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[State]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[State] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[State]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[State] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_State_Text] on [dbo].[State]'
GO
CREATE NONCLUSTERED INDEX [IX_State_Text] ON [dbo].[State] ([State_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransactionDetail]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TransactionDetail] DROP
COLUMN [sabreAPI_TransExID],
COLUMN [taxAmount],
COLUMN [taxCurrency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Chain]'
GO
CREATE TABLE [dbo].[RG_Recovery_12_Chain]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChainID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[ChainName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRS_ChainID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_12_Chain] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_12_Chain]([synXisID], [openHospID], [ChainID], [ChainName], [CRS_ChainID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [ChainID], [ChainName], [CRS_ChainID], [sabreAPI_ID], [pegasusID] FROM [dbo].[Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_12_Chain] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Chain]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_12_Chain]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_12_Chain]', N'Chain', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Chain] on [dbo].[Chain]'
GO
ALTER TABLE [dbo].[Chain] ADD CONSTRAINT [PK_dbo_Chain] PRIMARY KEY CLUSTERED ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Chain]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Chain] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Chain]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Chain] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Transactions]'
GO
CREATE TABLE [dbo].[RG_Recovery_13_Transactions]
(
[TransactionID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[QueueID] [int] NOT NULL,
[itineraryNumber] [nvarchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[transactionTimeStamp] [datetime] NULL,
[channelConnectConfirmationNumber] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timeLoaded] [datetime] NULL,
[TransactionStatusID] [int] NOT NULL,
[TransactionDetailID] [int] NOT NULL,
[CorporateCodeID] [int] NULL,
[RoomTypeID] [int] NULL,
[RateCategoryID] [int] NULL,
[CRS_BookingSourceID] [int] NULL,
[LoyaltyNumberID] [int] NULL,
[GuestID] [int] NULL,
[RateCodeID] [int] NULL,
[PromoCodeID] [int] NULL,
[TravelAgentID] [int] NULL,
[IATANumberID] [int] NULL,
[HotelID] [int] NULL,
[ChainID] [int] NULL,
[Guest_CompanyNameID] [int] NULL,
[PH_BookingSourceID] [int] NULL,
[VoiceAgentID] [int] NULL,
[LoyaltyProgramID] [int] NULL,
[DataSourceID] [int] NOT NULL,
[sourceKey] [int] NULL,
[LoyaltyNumber_IsImputedFromEmail] [bit] NULL,
[LoyaltyNumber_IsNewMember] [bit] NULL,
[rVersion] [timestamp] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_13_Transactions] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_13_Transactions]([TransactionID], [QueueID], [itineraryNumber], [confirmationNumber], [transactionTimeStamp], [channelConnectConfirmationNumber], [timeLoaded], [TransactionStatusID], [TransactionDetailID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [CRS_BookingSourceID], [LoyaltyNumberID], [GuestID], [RateCodeID], [PromoCodeID], [TravelAgentID], [IATANumberID], [HotelID], [ChainID], [Guest_CompanyNameID], [PH_BookingSourceID], [VoiceAgentID], [LoyaltyProgramID], [DataSourceID], [sourceKey], [LoyaltyNumber_IsImputedFromEmail], [LoyaltyNumber_IsNewMember]) SELECT [TransactionID], [QueueID], [itineraryNumber], [confirmationNumber], [transactionTimeStamp], [channelConnectConfirmationNumber], [timeLoaded], [TransactionStatusID], [TransactionDetailID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [CRS_BookingSourceID], [LoyaltyNumberID], [GuestID], [RateCodeID], [PromoCodeID], [TravelAgentID], [IATANumberID], [HotelID], [ChainID], [Guest_CompanyNameID], [PH_BookingSourceID], [VoiceAgentID], [LoyaltyProgramID], [DataSourceID], [sourceKey], [LoyaltyNumber_IsImputedFromEmail], [LoyaltyNumber_IsNewMember] FROM [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_13_Transactions] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Transactions]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_13_Transactions]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_13_Transactions]', N'Transactions', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Transactions] on [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [PK_dbo_Transactions] PRIMARY KEY CLUSTERED ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ChainID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_ChainID] ON [dbo].[Transactions] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_confirmationNumber_IC_Others] on [dbo].[Transactions]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_confirmationNumber_IC_Others] ON [dbo].[Transactions] ([confirmationNumber]) INCLUDE ([CRS_BookingSourceID], [DataSourceID], [IATANumberID], [PH_BookingSourceID], [RateCategoryID], [RateCodeID], [sourceKey], [TransactionDetailID], [TransactionStatusID], [TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_IC_FK_Cols] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_IC_FK_Cols] ON [dbo].[Transactions] ([confirmationNumber]) INCLUDE ([TransactionStatusID], [TransactionDetailID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [CRS_BookingSourceID], [LoyaltyNumberID], [GuestID], [RateCodeID], [PromoCodeID], [TravelAgentID], [IATANumberID], [HotelID], [ChainID], [Guest_CompanyNameID], [PH_BookingSourceID], [VoiceAgentID], [LoyaltyProgramID], [DataSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_IC_Others] ON [dbo].[Transactions] ([confirmationNumber]) INCLUDE ([transactionTimeStamp], [TransactionStatusID], [TransactionDetailID], [RateCodeID], [HotelID], [DataSourceID], [sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey] ON [dbo].[Transactions] ([confirmationNumber], [transactionTimeStamp]) INCLUDE ([sourceKey], [TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey_CSV] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey_CSV] ON [dbo].[Transactions] ([confirmationNumber], [transactionTimeStamp]) INCLUDE ([sourceKey], [TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CorporateCodeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_CorporateCodeID] ON [dbo].[Transactions] ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CRS_BookingSourceID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_CRS_BookingSourceID] ON [dbo].[Transactions] ([CRS_BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CRS_BookingSourceID_IC_sourceKey_TransactionID_confirmationNumber_transactionTimeStamp] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_CRS_BookingSourceID_IC_sourceKey_TransactionID_confirmationNumber_transactionTimeStamp] ON [dbo].[Transactions] ([CRS_BookingSourceID]) INCLUDE ([confirmationNumber], [sourceKey], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_DataSourceID_IC_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_DataSourceID_IC_sourceKey] ON [dbo].[Transactions] ([DataSourceID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Guest_CompanyNameID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_Guest_CompanyNameID] ON [dbo].[Transactions] ([Guest_CompanyNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GuestID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_GuestID] ON [dbo].[Transactions] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GuestID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_GuestID_IC_Others] ON [dbo].[Transactions] ([GuestID]) INCLUDE ([confirmationNumber], [TransactionStatusID], [TransactionDetailID], [CRS_BookingSourceID], [LoyaltyNumberID], [RateCodeID], [HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GuestID_LoyaltyNumberID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_GuestID_LoyaltyNumberID] ON [dbo].[Transactions] ([GuestID], [LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HotelID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_HotelID] ON [dbo].[Transactions] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID] ON [dbo].[Transactions] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID_IC_TransactionID_TravelAgentID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID_IC_TransactionID_TravelAgentID] ON [dbo].[Transactions] ([IATANumberID]) INCLUDE ([TransactionID], [TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LoyaltyNumberID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_LoyaltyNumberID] ON [dbo].[Transactions] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LoyaltyNumberID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_LoyaltyNumberID_IC_Others] ON [dbo].[Transactions] ([LoyaltyNumberID]) INCLUDE ([transactionTimeStamp], [TransactionDetailID], [CRS_BookingSourceID], [GuestID], [RateCodeID], [HotelID], [PH_BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PH_BookingSourceID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_PH_BookingSourceID] ON [dbo].[Transactions] ([PH_BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PromoCodeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_PromoCodeID] ON [dbo].[Transactions] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_confirmationNumber] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_confirmationNumber] ON [dbo].[Transactions] ([QueueID]) INCLUDE ([confirmationNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RateCategoryID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_RateCategoryID] ON [dbo].[Transactions] ([RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RateCodeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_RateCodeID] ON [dbo].[Transactions] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RoomTypeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_RoomTypeID] ON [dbo].[Transactions] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetailID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetailID] ON [dbo].[Transactions] ([TransactionDetailID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetailID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetailID_IC_Others] ON [dbo].[Transactions] ([TransactionDetailID]) INCLUDE ([confirmationNumber], [transactionTimeStamp], [timeLoaded], [TransactionStatusID], [RateCategoryID], [CRS_BookingSourceID], [LoyaltyNumberID], [RateCodeID], [TravelAgentID], [IATANumberID], [HotelID], [ChainID], [LoyaltyProgramID], [DataSourceID], [sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_POPULATE_FACT_RES] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_POPULATE_FACT_RES] ON [dbo].[Transactions] ([TransactionID], [TransactionStatusID], [TransactionDetailID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [CRS_BookingSourceID], [GuestID], [RateCodeID], [PromoCodeID], [TravelAgentID], [HotelID], [ChainID], [PH_BookingSourceID], [VoiceAgentID]) INCLUDE ([confirmationNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionStatusID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionStatusID] ON [dbo].[Transactions] ([TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionStatusID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionStatusID_IC_Others] ON [dbo].[Transactions] ([TransactionStatusID]) INCLUDE ([confirmationNumber], [TransactionDetailID], [CRS_BookingSourceID], [GuestID], [RateCodeID], [TravelAgentID], [IATANumberID], [HotelID], [DataSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_transactionTimeStamp_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_transactionTimeStamp_IC_Others] ON [dbo].[Transactions] ([transactionTimeStamp]) INCLUDE ([TransactionDetailID], [CRS_BookingSourceID], [LoyaltyNumberID], [GuestID], [RateCodeID], [HotelID], [PH_BookingSourceID], [DataSourceID], [sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TravelAgentID_IC_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TravelAgentID_IC_sourceKey] ON [dbo].[Transactions] ([TravelAgentID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_VoiceAgentID_IC_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_VoiceAgentID_IC_sourceKey] ON [dbo].[Transactions] ([TravelAgentID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CorporateCode]'
GO
CREATE TABLE [dbo].[RG_Recovery_14_CorporateCode]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CorporateCodeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[corporationCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_14_CorporateCode] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_14_CorporateCode]([synXisID], [openHospID], [CorporateCodeID], [corporationCode], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [CorporateCodeID], [corporationCode], [sabreAPI_ID], [pegasusID] FROM [dbo].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_14_CorporateCode] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CorporateCode]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_14_CorporateCode]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_14_CorporateCode]', N'CorporateCode', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_CorporateCode] on [dbo].[CorporateCode]'
GO
ALTER TABLE [dbo].[CorporateCode] ADD CONSTRAINT [PK_dbo_CorporateCode] PRIMARY KEY CLUSTERED ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CorporateCodeID_corporationCode] on [dbo].[CorporateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_CorporateCodeID_corporationCode] ON [dbo].[CorporateCode] ([CorporateCodeID], [corporationCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CorporateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CorporateCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CorporateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CorporateCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[hotel]'
GO
CREATE TABLE [dbo].[RG_Recovery_15_hotel]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HotelID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Hotel_hotelID] [int] NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_15_hotel] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_15_hotel]([synXisID], [openHospID], [HotelID], [Hotel_hotelID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [HotelID], [Hotel_hotelID], [sabreAPI_ID], [pegasusID] FROM [dbo].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_15_hotel] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[hotel]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_15_hotel]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_15_hotel]', N'hotel', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_hotel] on [dbo].[hotel]'
GO
ALTER TABLE [dbo].[hotel] ADD CONSTRAINT [PK_dbo_hotel] PRIMARY KEY CLUSTERED ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Hotel_hotelID] on [dbo].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_Hotel_hotelID] ON [dbo].[hotel] ([Hotel_hotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[hotel] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[hotel] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[IATANumber]'
GO
CREATE TABLE [dbo].[RG_Recovery_16_IATANumber]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IATANumberID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[IATANumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_16_IATANumber] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_16_IATANumber]([synXisID], [openHospID], [IATANumberID], [IATANumber], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [IATANumberID], [IATANumber], [sabreAPI_ID], [pegasusID] FROM [dbo].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_16_IATANumber] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[IATANumber]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_16_IATANumber]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_16_IATANumber]', N'IATANumber', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_IATANumber] on [dbo].[IATANumber]'
GO
ALTER TABLE [dbo].[IATANumber] ADD CONSTRAINT [PK_dbo_IATANumber] PRIMARY KEY CLUSTERED ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID_IATANumber] on [dbo].[IATANumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID_IATANumber] ON [dbo].[IATANumber] ([IATANumberID], [IATANumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[IATANumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[IATANumber] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[IATANumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[IATANumber] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[PromoCode]'
GO
CREATE TABLE [dbo].[RG_Recovery_17_PromoCode]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PromoCodeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[promotionalCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_17_PromoCode] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_17_PromoCode]([synXisID], [openHospID], [PromoCodeID], [promotionalCode], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [PromoCodeID], [promotionalCode], [sabreAPI_ID], [pegasusID] FROM [dbo].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_17_PromoCode] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[PromoCode]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_17_PromoCode]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_17_PromoCode]', N'PromoCode', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_PromoCode] on [dbo].[PromoCode]'
GO
ALTER TABLE [dbo].[PromoCode] ADD CONSTRAINT [PK_dbo_PromoCode] PRIMARY KEY CLUSTERED ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[PromoCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PromoCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[PromoCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PromoCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PromoCodeID_promotionalCode] on [dbo].[PromoCode]'
GO
CREATE NONCLUSTERED INDEX [IX_PromoCodeID_promotionalCode] ON [dbo].[PromoCode] ([PromoCodeID], [promotionalCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[RateCode]'
GO
CREATE TABLE [dbo].[RG_Recovery_18_RateCode]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCodeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[RateName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCode] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[GDSRateAccessCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GDSBookingCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateTaxInclusive] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_18_RateCode] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_18_RateCode]([synXisID], [openHospID], [RateCodeID], [RateName], [RateCode], [sabreAPI_ID], [GDSRateAccessCode], [GDSBookingCode], [rateTaxInclusive], [pegasusID]) SELECT [synXisID], [openHospID], [RateCodeID], [RateName], [RateCode], [sabreAPI_ID], [GDSRateAccessCode], [GDSBookingCode], [rateTaxInclusive], [pegasusID] FROM [dbo].[RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_18_RateCode] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[RateCode]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_18_RateCode]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_18_RateCode]', N'RateCode', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RateCode] on [dbo].[RateCode]'
GO
ALTER TABLE [dbo].[RateCode] ADD CONSTRAINT [PK_dbo_RateCode] PRIMARY KEY CLUSTERED ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[RateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RateCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[RateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RateCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RateCodeID_RateCode] on [dbo].[RateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_RateCodeID_RateCode] ON [dbo].[RateCode] ([RateCodeID], [RateCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[RoomType]'
GO
CREATE TABLE [dbo].[RG_Recovery_19_RoomType]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoomTypeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[roomTypeName] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roomTypeCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[roomCategory] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_19_RoomType] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_19_RoomType]([synXisID], [openHospID], [RoomTypeID], [roomTypeName], [roomTypeCode], [sabreAPI_ID], [roomCategory], [pegasusID]) SELECT [synXisID], [openHospID], [RoomTypeID], [roomTypeName], [roomTypeCode], [sabreAPI_ID], [roomCategory], [pegasusID] FROM [dbo].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_19_RoomType] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[RoomType]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_19_RoomType]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_19_RoomType]', N'RoomType', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RoomType] on [dbo].[RoomType]'
GO
ALTER TABLE [dbo].[RoomType] ADD CONSTRAINT [PK_dbo_RoomType] PRIMARY KEY CLUSTERED ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[RoomType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RoomType] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[RoomType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RoomType] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_RoomTypeID_roomTypeName] on [dbo].[RoomType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_RoomTypeID_roomTypeName] ON [dbo].[RoomType] ([RoomTypeID], [roomTypeName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RoomTypeID_roomTypeName_roomTypeCode] on [dbo].[RoomType]'
GO
CREATE NONCLUSTERED INDEX [IX_RoomTypeID_roomTypeName_roomTypeCode] ON [dbo].[RoomType] ([RoomTypeID], [roomTypeName], [roomTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[TravelAgent]'
GO
CREATE TABLE [dbo].[RG_Recovery_20_TravelAgent]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgentID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[IATANumberID] [int] NULL,
[Name] [nvarchar] (450) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NULL,
[Phone] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[PsuedoCity] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_20_TravelAgent] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_20_TravelAgent]([synXisID], [openHospID], [TravelAgentID], [IATANumberID], [Name], [Address1], [Address2], [LocationID], [Phone], [Fax], [Email], [sabreAPI_ID], [PsuedoCity], [pegasusID]) SELECT [synXisID], [openHospID], [TravelAgentID], [IATANumberID], [Name], [Address1], [Address2], [LocationID], [Phone], [Fax], [Email], [sabreAPI_ID], [PsuedoCity], [pegasusID] FROM [dbo].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_20_TravelAgent] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[TravelAgent]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_20_TravelAgent]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_20_TravelAgent]', N'TravelAgent', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_TravelAgent] on [dbo].[TravelAgent]'
GO
ALTER TABLE [dbo].[TravelAgent] ADD CONSTRAINT [PK_dbo_TravelAgent] PRIMARY KEY CLUSTERED ([TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID] ON [dbo].[TravelAgent] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[TravelAgent] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[TravelAgent] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TravelAgentID_IATANumberID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_TravelAgentID_IATANumberID] ON [dbo].[TravelAgent] ([TravelAgentID], [IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[ActionType]'
GO
CREATE TABLE [dbo].[RG_Recovery_21_ActionType]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionTypeID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[actionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[actionTypeOrder] [int] NOT NULL CONSTRAINT [DF_actionTypeOrder] DEFAULT ((0)),
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_21_ActionType] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_21_ActionType]([synXisID], [openHospID], [ActionTypeID], [actionType], [actionTypeOrder], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [ActionTypeID], [actionType], [actionTypeOrder], [sabreAPI_ID], [pegasusID] FROM [dbo].[ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_21_ActionType] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[ActionType]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_21_ActionType]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[ActionType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_21_ActionType]', N'ActionType', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_ActionType] on [dbo].[ActionType]'
GO
ALTER TABLE [dbo].[ActionType] ADD CONSTRAINT [PK_dbo_ActionType] PRIMARY KEY CLUSTERED ([ActionTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[ActionType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[ActionType] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[ActionType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[ActionType] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CorporateCode]'
GO



ALTER PROCEDURE [dbo].[Populate_CorporateCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CorporateCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[corporationCode]
		FROM
		(
			SELECT [CorporateCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[corporationCode]
			FROM synxis.CorporateCode
			WHERE [CorporateCodeID] IN(SELECT [CorporateCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],CorpInfoCodeID AS [openHospID],NULL AS [pegasusID],CorpInfoCode AS [corporationCode]
			FROM openHosp.CorpInfoCode
			WHERE [CorpInfoCodeID] IN(SELECT [CorpInfoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],CorpInfoCodeID AS [pegasusID],CorpInfoCode AS [corporationCode]
			FROM pegasus.CorpInfoCode
			WHERE [CorpInfoCodeID] IN(SELECT [CorpInfoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [corporationCode]
	) AS src ON src.[corporationCode] = tgt.[corporationCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[corporationCode])
		VALUES([synXisID],[openHospID],[pegasusID],[corporationCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Country]'
GO



ALTER PROCEDURE [dbo].[Populate_Country]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Country] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],ISNULL(NULLIF([Country_Text],''),'unknown') AS [Country_Text]
		FROM
		(
			SELECT [CountryID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[Country_Text]
			FROM synxis.Country
				UNION ALL
			SELECT NULL AS [synXisID],[CountryID] AS [openHospID],NULL AS [pegasusID],[Country_Text]
			FROM openHosp.Country
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[CountryID] AS [pegasusID],[Country_Text]
			FROM pegasus.Country
		) x
		GROUP BY [Country_Text]
	) AS src ON src.[Country_Text] = tgt.[Country_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[Country_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[Country_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CRS_SubSource]'
GO
CREATE TABLE [dbo].[RG_Recovery_22_CRS_SubSource]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SubSourceID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[subSource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[subSourceCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_22_CRS_SubSource] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_22_CRS_SubSource]([synXisID], [openHospID], [SubSourceID], [subSource], [subSourceCode], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [SubSourceID], [subSource], [subSourceCode], [sabreAPI_ID], [pegasusID] FROM [dbo].[CRS_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_22_CRS_SubSource] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CRS_SubSource]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_22_CRS_SubSource]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CRS_SubSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_22_CRS_SubSource]', N'CRS_SubSource', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_SubSource] on [dbo].[CRS_SubSource]'
GO
ALTER TABLE [dbo].[CRS_SubSource] ADD CONSTRAINT [PK_dbo_SubSource] PRIMARY KEY CLUSTERED ([SubSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_SubSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_SubSource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_SubSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_SubSource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CRS_SecondarySource]'
GO
CREATE TABLE [dbo].[RG_Recovery_23_CRS_SecondarySource]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecondarySourceID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[secondarySource] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_23_CRS_SecondarySource] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_23_CRS_SecondarySource]([synXisID], [openHospID], [SecondarySourceID], [secondarySource], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [SecondarySourceID], [secondarySource], [sabreAPI_ID], [pegasusID] FROM [dbo].[CRS_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_23_CRS_SecondarySource] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CRS_SecondarySource]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_23_CRS_SecondarySource]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CRS_SecondarySource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_23_CRS_SecondarySource]', N'CRS_SecondarySource', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_SecondarySource] on [dbo].[CRS_SecondarySource]'
GO
ALTER TABLE [dbo].[CRS_SecondarySource] ADD CONSTRAINT [PK_dbo_SecondarySource] PRIMARY KEY CLUSTERED ([SecondarySourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_SecondarySource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_SecondarySource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_SecondarySource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_SecondarySource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CRS_Channel]'
GO
CREATE TABLE [dbo].[RG_Recovery_24_CRS_Channel]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChannelID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[channel] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_24_CRS_Channel] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_24_CRS_Channel]([synXisID], [openHospID], [ChannelID], [channel], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [ChannelID], [channel], [sabreAPI_ID], [pegasusID] FROM [dbo].[CRS_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_24_CRS_Channel] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CRS_Channel]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_24_CRS_Channel]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CRS_Channel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_24_CRS_Channel]', N'CRS_Channel', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Channel] on [dbo].[CRS_Channel]'
GO
ALTER TABLE [dbo].[CRS_Channel] ADD CONSTRAINT [PK_dbo_Channel] PRIMARY KEY CLUSTERED ([ChannelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_Channel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_Channel] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_Channel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_Channel] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MostRecentTransactions]'
GO








ALTER VIEW [dbo].[MostRecentTransactions]
AS

	SELECT t.TransactionID,ch.ChainName,ch.CRS_ChainID AS [ChainID],hh.HotelName,hh.SynXisID AS [HotelID],synXis.SAPID AS [SAPID],hh.HotelCode,synXis.billingDescription AS [billingDescription],t.transactionTimeStamp,synXis.faxNotificationCount AS [FaxNotificationCount],
			bs.channel,bs.secondarySource,bs.subSource,bs.subSourceCode,synXis.PMSRateTypeCode AS [PMSRateTypeCode],synXis.PMSRoomTypeCode AS [PMSRoomTypeCode],synXis.marketSourceCode AS [marketSourceCode],synXis.marketSegmentCode AS [marketSegmentCode],synXis.userName AS [userName],
			ts.[status],t.confirmationNumber,ts.confirmationDate,ts.cancellationNumber,ts.cancellationDate,ISNULL(gu.salutation,N'') AS salutation,ISNULL(gu.FirstName,N'') AS [guestFirstName],ISNULL(gu.LastName,N'') AS [guestLastName],
			ISNULL(gu.customerID,N'') AS customerID,ISNULL(gu.Address1,N'') AS [customerAddress1],ISNULL(gu.Address2,N'') AS [customerAddress2],ISNULL(NULLIF(gul.City_Text,N''),N'unknown') AS [customerCity],ISNULL(NULLIF(gul.State_Text,N''),N'unknown') AS [customerState],ISNULL(NULLIF(gul.PostalCode_Text,N''),N'unknown') AS [customerPostalCode],ISNULL(gu.phone,'') AS [customerPhone],ISNULL(NULLIF(gul.Country_Text,N''),N'unknown') AS [customerCountry],
			synXis.customerArea AS [customerArea],ISNULL(NULLIF(gul.Region_Text,N''),N'unknown') AS [customerRegion],gcn.CompanyName AS [customerCompanyName],td.arrivalDate,td.departureDate,
			td.bookingLeadTime,rc.rateCategoryName,ISNULL(NULLIF(rc.rateCategoryCode,N''),N'Unassigned') AS rateCategoryCode,COALESCE(synXis.RateTypeName,openHosp.RateTypeName,pegasus.RateTypeName) AS [rateTypeName],rac.RateCode AS [rateTypeCode],rt.roomTypeName,rt.roomTypeCode,
			td.nights,td.averageDailyRate,td.rooms,td.reservationRevenue,td.currency,ISNULL(iata.IATANumber,'') AS IATANumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],ISNULL(ta.Address1,N'') AS [travelAgencyAddress1],ISNULL(ta.Address2,N'') AS [travelAgencyAddress2],ISNULL(NULLIF(tal.City_Text,N''),N'unknown') AS [travelAgencyCity],ISNULL(tal.State_Text,N'unknown') AS [travelAgencyState],ISNULL(tal.PostalCode_Text,N'unknown') AS [travelAgencyPostalCode],
			ISNULL(ta.Phone,N'') AS [travelAgencyPhone],ISNULL(ta.Fax,N'') AS [travelAgencyFax],ISNULL(tal.Country_Text,N'') AS [travelAgencyCountry],synXis.[travelAgencyArea] AS [travelAgencyArea],ISNULL(tal.Region_Text,N'') AS [travelAgencyRegion],ISNULL(ta.Email,N'') AS [travelAgencyEmail],
			synXis.consortiaCount AS [consortiaCount],synXis.consortiaName AS [consortiaName],td.totalPackageRevenue,td.optIn,ISNULL(gue.emailAddress,N'') AS [customerEmail],td.totalGuestCount,td.adultCount,td.childrenCount,ts.creditCardType,
			a.actionType,synXis.shareWith AS [shareWith],
			CASE td.arrivalDOW WHEN 1 THEN 'Sun' WHEN 2 THEN 'Mon' WHEN 3 THEN 'Tue' WHEN 4 THEN 'Wed' WHEN 5 THEN 'Thu' WHEN 6 THEN 'Fri' WHEN 7 THEN 'Sat' END AS ArrivalDOW,
			CASE td.departureDOW WHEN 1 THEN 'Sun' WHEN 2 THEN 'Mon' WHEN 3 THEN 'Tue' WHEN 4 THEN 'Wed' WHEN 5 THEN 'Thu' WHEN 6 THEN 'Fri' WHEN 7 THEN 'Sat' END AS DepartureDOW,
			t.itineraryNumber,synXis.secondaryCurrency AS [secondaryCurrency],synXis.secondaryCurrencyExchangeRate AS [secondaryCurrencyExchangeRate],synXis.secondaryCurrencyAverageDailyRate AS [secondaryCurrencyAverageDailyRate],synXis.secondaryCurrencyReservationRevenue AS [secondaryCurrencyReservationRevenue],synXis.secondaryCurrencyPackageRevenue AS [secondaryCurrencyPackageRevenue],
			td.commisionPercent,synXis.membershipNumber AS [membershipNumber],cc.corporationCode,pc.promotionalCode,bs.CRO_Code AS [CROCode],t.channelConnectConfirmationNumber,td.IsPrimaryGuest AS [primaryGuest],
			lp.LoyaltyProgram AS [loyaltyProgram],ln.loyaltyNumber,synXis.vipLevel AS [vipLevel],bs.ibeSourceName AS [xbeTemplateName],synXis.xbeShellName AS [xbeShellName],synXis.profileTypeSelection AS [profileTypeSelection],
			[dbo].[convertCurrencyToUSD](td.averageDailyRate,td.currency,ts.confirmationDate) AS [averageDailyRateUSD],[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS [reservationRevenueUSD],[dbo].[convertCurrencyToUSD](td.totalPackageRevenue,td.currency,ts.confirmationDate) AS [totalPackageRevenueUSD],
			t.timeLoaded,hh.OpenHospID AS [OpenHospitalityID],hh.PegasusRT4ID AS [PegasusID],ds.CRSSourceID AS [CRSSourceID],td.LoyaltyNumberValidated,td.LoyaltyNumberTagged,td.[IsCrossBrand]
	FROM dbo.Transactions t WITH(NOLOCK)
		INNER JOIN authority.DataSource ds WITH(NOLOCK) ON ds.DataSourceID = t.DataSourceID
		INNER JOIN dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID
		LEFT JOIN dbo.vw_CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN dbo.Guest gu WITH(NOLOCK) ON gu.GuestID = t.GuestID
		LEFT JOIN dbo.vw_Location gul WITH(NOLOCK) ON gul.LocationID = gu.LocationID
		LEFT JOIN dbo.Guest_EmailAddress gue WITH(NOLOCK) ON gue.Guest_EmailAddressID = gu.Guest_EmailAddressID
		LEFT JOIN dbo.Guest_CompanyName gcn WITH(NOLOCK) ON gcn.Guest_CompanyNameID = t.Guest_CompanyNameID
		LEFT JOIN dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN dbo.RoomType rt WITH(NOLOCK) ON rt.RoomTypeID = t.RoomTypeID
		LEFT JOIN dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN dbo.vw_Location tal WITH(NOLOCK) ON tal.LocationID = ta.LocationID
		LEFT JOIN dbo.ActionType a WITH(NOLOCK) ON a.ActionTypeID = ts.ActionTypeID
		LEFT JOIN dbo.CorporateCode cc WITH(NOLOCK) ON cc.CorporateCodeID = t.CorporateCodeID
		LEFT JOIN dbo.PromoCode pc WITH(NOLOCK) ON pc.PromoCodeID = t.PromoCodeID
		LEFT JOIN dbo.LoyaltyNumber ln WITH(NOLOCK) ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN dbo.LoyaltyProgram lp WITH(NOLOCK) ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN dbo.RateCode rac WITH(NOLOCK) ON rac.RateCodeID = t.RateCodeID
		LEFT JOIN
			(
				SELECT t.TransactionID AS synXisID,bd.billingDescription,te.faxNotificationCount,pms.PMSRateTypeCode,rt.PMSRoomTypeCode,
						te.marketSourceCode,te.marketSegmentCode,un.userName,rtc.RateTypeName,te.consortiaCount,con.consortiaName,te.SAPID,
						te.shareWith,td.secondaryCurrency,td.secondaryCurrencyExchangeRate,td.secondaryCurrencyAverageDailyRate,td.secondaryCurrencyReservationRevenue,
						td.secondaryCurrencyPackageRevenue,te.membershipNumber,vip.vipLevel,te.profileTypeSelection,xbe.xbeShellName,
						ga.Area_Text AS [customerArea],taa.Area_Text AS [travelAgencyArea]
				FROM synxis.Transactions t WITH(NOLOCK)
					LEFT JOIN synxis.BillingDescription bd WITH(NOLOCK) ON bd.BillingDescriptionID = t.BillingDescriptionID
					LEFT JOIN synxis.TransactionsExtended te WITH(NOLOCK) ON te.TransactionsExtendedID = t.TransactionsExtendedID
					LEFT JOIN synxis.PMSRateTypeCode pms WITH(NOLOCK) ON pms.PMSRateTypeCodeID = t.PMSRateTypeCodeID
					LEFT JOIN synxis.RoomType rt WITH(NOLOCK) ON rt.RoomTypeID = t.RoomTypeID
					LEFT JOIN synxis.UserName un WITH(NOLOCK) ON un.UserNameID = t.UserNameID
					LEFT JOIN synxis.RateTypeCode rtc WITH(NOLOCK) ON rtc.RateTypeCodeID = t.RateTypeCodeID
					LEFT JOIN synxis.Consortia con WITH(NOLOCK) ON con.ConsortiaID = t.ConsortiaID
					LEFT JOIN synxis.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
					LEFT JOIN synxis.VIP_Level vip WITH(NOLOCK) ON vip.VIP_LevelID = t.VIP_LevelID
					LEFT JOIN synxis.BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.BookingSourceID
					LEFT JOIN synxis.xbeTemplate xbe WITH(NOLOCK) ON xbe.xbeTemplateID = bs.xbeTemplateID
					LEFT JOIN synxis.Guest g WITH(NOLOCK) ON g.GuestID = t.GuestID
						LEFT JOIN synxis.[Location] gloc WITH(NOLOCK) ON gloc.LocationID = g.LocationID
						LEFT JOIN synxis.Area ga WITH(NOLOCK) ON ga.AreaID = gloc.AreaID
					LEFT JOIN synxis.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
						LEFT JOIN synxis.[Location] taloc WITH(NOLOCK) ON taloc.LocationID = ta.LocationID
						LEFT JOIN synxis.Area taa WITH(NOLOCK) ON taa.AreaID = taloc.AreaID
			) synXis ON synXis.synXisID = t.sourceKey AND ds.SourceName = 'SynXis'
		LEFT JOIN
			(
				SELECT t.TransactionID AS openHospID,rtc.RateTypeName
				FROM openHosp.Transactions t WITH(NOLOCK)
					LEFT JOIN openHosp.RateTypeCode rtc WITH(NOLOCK) ON rtc.RateTypeCodeID = t.RateTypeCodeID
			) openHosp ON openHosp.openHospID = t.sourceKey AND ds.SourceName = 'Open Hospitality'
		LEFT JOIN
			(
				SELECT t.TransactionID AS pegasusID,rtc.RateTypeName
				FROM pegasus.Transactions t WITH(NOLOCK)
					LEFT JOIN pegasus.RateTypeCode rtc WITH(NOLOCK) ON rtc.RateTypeCodeID = t.RateTypeCodeID
			) pegasus ON pegasus.pegasusID = t.sourceKey AND ds.SourceName = 'Pegasus'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CROCode]'
GO



ALTER PROCEDURE [dbo].[Populate_CROCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CROCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[auth_CRO_CodeID]
		FROM
		(
			SELECT sCRO.[CROCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM synxis.CROCode sCRO
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = sCRO.CROCode
			WHERE [CROCodeID] IN(
								SELECT DISTINCT [CROCodeID]
								FROM synxis.Transactions t
									INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
								WHERE t.QueueID = @QueueID
								)

			UNION ALL

			SELECT NULL AS [synXisID],bs.BookingSourceID AS [openHospID],NULL AS [pegasusID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obh ON obh.Bkg_Src_Cd = bs.Bkg_Src_Cd
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = obh.CRO_Code
			WHERE obh.CRO_Code != ''

			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],pbs.BookingSourceID AS [pegasusID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.Pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = apbs.CRO_Code
			WHERE apbs.CRO_Code != ''
		) X
		GROUP BY [auth_CRO_CodeID]
	) AS src ON src.[auth_CRO_CodeID] = tgt.[auth_CRO_CodeID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[synXisID] + ',','') + ISNULL(src.[synXisID],'')),''),
				[openHospID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[openHospID] + ',','') + ISNULL(src.[openHospID],'')),''),
				[pegasusID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[pegasusID] + ',','') + ISNULL(src.[pegasusID],'')),'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[auth_CRO_CodeID])
		VALUES([synXisID],[openHospID],[pegasusID],[auth_CRO_CodeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_BookingSource]'
GO



ALTER PROCEDURE [dbo].[Populate_CRS_BookingSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_BookingSource] tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID]
		FROM
		(
			SELECT sbs.[BookingSourceID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.[SubSourceID],dcro.[CROCodeID],dx.[ibeSourceID] AS [xbeTemplateID]
			FROM synxis.BookingSource sbs
				LEFT JOIN (SELECT [ChannelID], value AS synXisID FROM dbo.CRS_Channel CROSS APPLY string_split(synXisID,','))  dc ON dc.synXisID = sbs.ChannelID
				LEFT JOIN (SELECT [SecondarySourceID], value AS synXisID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(synXisID,',')) dss ON dss.synXisID = sbs.SecondarySourceID
				LEFT JOIN (SELECT [SubSourceID], value AS synXisID FROM dbo.CRS_SubSource CROSS APPLY string_split(synXisID,',')) ds ON ds.synXisID = sbs.SubSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS synXisID FROM dbo.CROCode CROSS APPLY string_split(synXisID,',')) dcro ON dcro.synXisID = sbs.CROCodeID
				LEFT JOIN (SELECT [ibeSourceID], value AS synXisID FROM dbo.ibeSource CROSS APPLY string_split(synXisID,',')) dx ON dx.synXisID = sbs.xbeTemplateID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				
			UNION ALL

			SELECT NULL AS [synXisID],obs.[BookingSourceID] AS [openHospID],NULL AS [pegasusID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.SubSourceID,dcro.CROCodeID AS [CROCodeID],dx.ibeSourceID AS [xbeTemplateID]
			FROM openHosp.BookingSource obs
				LEFT JOIN (SELECT [ChannelID], value AS openHospID FROM dbo.CRS_Channel CROSS APPLY string_split(openHospID,',')) dc ON dc.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [SecondarySourceID], value AS openHospID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(openHospID,',')) dss ON dss.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [SubSourceID], value AS openHospID FROM dbo.CRS_SubSource CROSS APPLY string_split(openHospID,',')) ds ON ds.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS openHospID FROM dbo.CROCode CROSS APPLY string_split(openHospID,',')) dcro ON dcro.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [ibeSourceID], value AS openHospID FROM dbo.ibeSource CROSS APPLY string_split(openHospID,',')) dx ON dx.openHospID = obs.BookingSourceID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)

			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],pbs.[BookingSourceID] AS [pegasusID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.SubSourceID,dcro.CROCodeID AS [CROCodeID],dx.ibeSourceID AS [xbeTemplateID]
			FROM pegasus.BookingSource pbs
				LEFT JOIN (SELECT [ChannelID], value AS pegasusID FROM dbo.CRS_Channel CROSS APPLY string_split(pegasusID,',')) dc ON dc.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [SecondarySourceID], value AS pegasusID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(pegasusID,',')) dss ON dss.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [SubSourceID], value AS pegasusID FROM dbo.CRS_SubSource CROSS APPLY string_split(pegasusID,',')) ds ON ds.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS pegasusID FROM dbo.CROCode CROSS APPLY string_split(pegasusID,',')) dcro ON dcro.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [ibeSourceID], value AS pegasusID FROM dbo.ibeSource CROSS APPLY string_split(pegasusID,',')) dx ON dx.pegasusID = pbs.BookingSourceID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) X
		GROUP BY [ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID]
	) AS src ON ISNULL(src.[ChannelID],0) = ISNULL(tgt.[ChannelID],0) AND ISNULL(src.[SecondarySourceID],0) = ISNULL(tgt.[SecondarySourceID],0) AND ISNULL(src.[SubSourceID],0) = ISNULL(tgt.[SubSourceID],0)
				AND ISNULL(src.[CROCodeID],0) = ISNULL(tgt.[CROCodeID],0) AND ISNULL(src.[xbeTemplateID],0) = ISNULL(tgt.[ibeSourceNameID],0)
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[synXisID] + ',','') + ISNULL(src.[synXisID],'')),''),
				[openHospID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[openHospID] + ',','') + ISNULL(src.[openHospID],'')),''),
				[pegasusID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[pegasusID] + ',','') + ISNULL(src.[pegasusID],'')),'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[ibeSourceNameID])
		VALUES([synXisID],[openHospID],[pegasusID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_Channel]'
GO




ALTER PROCEDURE [dbo].[Populate_CRS_Channel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_Channel] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[channel]
		FROM
		(
			SELECT [ChannelID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[channel]
			FROM synxis.Channel
			WHERE [ChannelID] IN(
									SELECT DISTINCT bs.[ChannelID]
									FROM synxis.Transactions t
										INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
									WHERE t.QueueID = @QueueID
								)

				UNION ALL

			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],NULL AS [pegasusID],obs.Channel
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)

				UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],BookingSourceID AS [pegasusID],apbs.Channel
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.Pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [channel]
	) AS src ON src.[channel] = tgt.[channel]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[synXisID] + ',','') + ISNULL(src.[synXisID],'')),''),
				[openHospID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[openHospID] + ',','') + ISNULL(src.[openHospID],'')),''),
				[pegasusID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[pegasusID] + ',','') + ISNULL(src.[pegasusID],'')),'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[channel])
		VALUES([synXisID],[openHospID],[pegasusID],[channel])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_Voice_Hotel]'
GO

ALTER PROCEDURE [dbo].[PH_BookingSource_Voice_Hotel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#34AADC'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			INNER JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND acrog.CRO_Code_Group = 'Hotel Voice Agent'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel','Test Bookings', '','#34AADC'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
			INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			INNER JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
			INNER JOIN dbo.hotel ht  ON ht.HotelID = t.HotelID
			INNER JOIN Hotels.dbo.Hotel hh  ON hh.HotelID = ht.Hotel_hotelID
			INNER JOIN dbo.Guest gu ON gu.GuestID = t.GuestID
			INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND (hh.HotelName LIKE '%test%'OR gu.FirstName LIKE '%test%' OR gu.LastName LIKE '%test%' OR va.VoiceAgent LIKE '%test%'OR va.VoiceAgent LIKE '%sabre.com%'OR va.VoiceAgent LIKE '%preferredhotel%')
			AND (hh.HotelCode <> 'CHIAE' AND va.VoiceAgent <> 'SHSIntegrationTeam@sabre.com(Integration Integration)')
			AND (hh.HotelCode <> 'DALHL' AND va.VoiceAgent <> 'integrations@sabre.com(integration user)')
			AND va.VoiceAgent NOT IN ('SHSintegrationteam@sabre.com(INFOR Integration)','shsintegrationteam@sabre.com(Infor HMS Integ User)','SHS-Database-Development@sabre.com(Sabre DBA2)') 
			AND (sec.secondarySource <> 'Group Rooming List Import')
			AND acro.CRO_Code = ''

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel','Test Bookings', '','#34AADC'
		FROM dbo.Transactions t
		INNER JOIN [operations].[ManualFixes] m ON m.[changeName] = 'PH_BookingSource_Voice' AND m.changeValue = 'Test Bookings' AND t.confirmationNumber = m.confirmationNumber


	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#34AADC'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			INNER JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND acrog.CRO_Code_Group = 'Hotel Voice Agent'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel','Test Bookings', '','#34AADC'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
			INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			INNER JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
			INNER JOIN dbo.hotel ht  ON ht.HotelID = t.HotelID
			INNER JOIN Hotels.dbo.Hotel hh  ON hh.HotelID = ht.Hotel_hotelID
			INNER JOIN dbo.Guest gu ON gu.GuestID = t.GuestID
			INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND (hh.HotelName LIKE '%test%'OR gu.FirstName LIKE '%test%' OR gu.LastName LIKE '%test%' OR va.VoiceAgent LIKE '%test%'OR va.VoiceAgent LIKE '%sabre.com%'OR va.VoiceAgent LIKE '%preferredhotel%')
			AND (hh.HotelCode <> 'CHIAE' AND va.VoiceAgent <> 'SHSIntegrationTeam@sabre.com(Integration Integration)')
			AND (hh.HotelCode <> 'DALHL' AND va.VoiceAgent <> 'integrations@sabre.com(integration user)')
			AND va.VoiceAgent NOT IN ('SHSintegrationteam@sabre.com(INFOR Integration)','shsintegrationteam@sabre.com(Infor HMS Integ User)','SHS-Database-Development@sabre.com(Sabre DBA2)') 
			AND (sec.secondarySource <> 'Group Rooming List Import')
			AND acro.CRO_Code = ''
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel','Test Bookings', '','#34AADC'
		FROM dbo.Transactions t
		INNER JOIN [operations].[ManualFixes] m ON m.[changeName] = 'PH_BookingSource_Voice' AND m.changeValue = 'Test Bookings' AND t.confirmationNumber = m.confirmationNumber
		WHERE t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_Voice_Brand]'
GO


ALTER PROCEDURE [dbo].[PH_BookingSource_Voice_Brand]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND ISNULL(sub.subSourceCode,'') != 'VCG'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'Voice - Brand'
			
		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			INNER JOIN authority.Pegasus_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'Voice - Brand'
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND ISNULL(sub.subSourceCode,'') != 'VCG'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'Voice - Brand'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			INNER JOIN authority.Pegasus_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'Voice - Brand'
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_Tour_Manager]'
GO




ALTER PROCEDURE [dbo].[PH_BookingSource_Tour_Manager]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Tour Manager',ss.secondarySource,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#00FF7D'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Tour Manager')
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Tour Manager',ss.secondarySource,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#00FF7D'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Tour Manager')
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_PMS]'
GO





ALTER PROCEDURE [dbo].[PH_BookingSource_PMS]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT confirmationNumber,'PMS','PMS','PMS','#D0AF00'
		FROM
			(
				SELECT DISTINCT t.confirmationNumber
				FROM dbo.Transactions t
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
				WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
					AND c.channel IN('PMS','PMS Rez Synch')
		
				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
				WHERE hh.HotelCode = 'CHIAE'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'SHSIntegrationTeam@sabre.com(Integration Integration)'

				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
					INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
					INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				WHERE hh.HotelCode = 'DALHL'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'integrations@sabre.com(integration user)'
					AND acro.CRO_Code = ''

				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.Transactions t
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
					INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
					INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
					AND ch.channel = 'Voice'
					AND va.VoiceAgent IN ('SHSintegrationteam@sabre.com(INFOR Integration)','shsintegrationteam@sabre.com(Infor HMS Integ User)','SHS-Database-Development@sabre.com(Sabre DBA2)') 
					AND acro.CRO_Code = ''
			) x
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT confirmationNumber,'PMS','PMS','PMS','#D0AF00'
		FROM
			(
				SELECT DISTINCT t.confirmationNumber
				FROM dbo.Transactions t
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
				WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
					AND c.channel IN('PMS','PMS Rez Synch')
					AND t.QueueID = @QueueID
		
				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
				WHERE hh.HotelCode = 'CHIAE'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'SHSIntegrationTeam@sabre.com(Integration Integration)'
					AND t.QueueID = @QueueID

				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
					INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
					INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				WHERE hh.HotelCode = 'DALHL'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'integrations@sabre.com(integration user)'
					AND acro.CRO_Code = ''
					AND t.QueueID = @QueueID

				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.Transactions t
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
					INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
					INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
					AND ch.channel = 'Voice'
					AND va.VoiceAgent IN ('SHSintegrationteam@sabre.com(INFOR Integration)','shsintegrationteam@sabre.com(Infor HMS Integ User)','SHS-Database-Development@sabre.com(Sabre DBA2)') 
					AND acro.CRO_Code = ''
					AND t.QueueID = @QueueID
			) x
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_OTA_Connect]'
GO


ALTER PROCEDURE [dbo].[PH_BookingSource_OTA_Connect]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------
	DELETE #BOOKING


	-- CREATE ProTools Hotel List -----------------------------------------
	DECLARE @ProTools_HotelIDs TABLE(HotelID int NOT NULL,StartDate date,EndDate date);

	INSERT INTO @ProTools_HotelIDs(HotelID,StartDate,EndDate)
	SELECT DISTINCT h.HotelID,hpt.StartDate,ISNULL(hpt.EndDate,'9999-09-09')
	FROM Hotels.dbo.ProTools pt
		INNER JOIN Hotels.dbo.ProTools_Detail ptd ON ptd.[ProToolsID] = pt.[ProToolsID]
		INNER JOIN Hotels.dbo.Hotel_ProTools_Detail hpt ON hpt.ProTools_DetailID = ptd.ProTools_DetailID
		INNER JOIN dbo.hotel h ON h.Hotel_hotelID = hpt.HotelID
	WHERE pt.ProToolsName = 'OTA Bundle'
	-----------------------------------------------------------------------


	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,CASE WHEN pt.HotelID IS NULL THEN 'OTA Connect' ELSE 'OTA Bundle' END,
				CASE WHEN sub.subSourceCode = 'CCX' THEN 'Channel Connect Express' ELSE c.channel END,
				CASE
					WHEN c.channel = 'Channel Connect' THEN ss.secondarySource
					WHEN ss.secondarySource = 'Pegs ADS' THEN ISNULL(sub.subSource,sub.subSourceCode)
					WHEN c.channel = 'IDS' THEN ISNULL(sub.subSource,ss.secondarySource)
				END,'#FF9500'
		FROM dbo.Transactions t
			INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN @ProTools_HotelIDs pt ON pt.HotelID = t.HotelID AND td.arrivalDate BETWEEN pt.StartDate AND pt.EndDate
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Channel Connect','IDS','Google')
			AND sub.subsourcecode != 'SUITEPR'
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,CASE WHEN pt.HotelID IS NULL THEN 'OTA Connect' ELSE 'OTA Bundle' END,
				CASE WHEN sub.subSourceCode = 'CCX' THEN 'Channel Connect Express' ELSE c.channel END,
				CASE
					WHEN c.channel = 'Channel Connect' THEN ss.secondarySource
					WHEN ss.secondarySource = 'Pegs ADS' THEN ISNULL(sub.subSource,sub.subSourceCode)
					WHEN c.channel = 'IDS' THEN ISNULL(sub.subSource,ss.secondarySource)
				END,'#FF9500'
		FROM dbo.Transactions t
			INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN @ProTools_HotelIDs pt ON pt.HotelID = t.HotelID AND td.arrivalDate BETWEEN pt.StartDate AND pt.EndDate
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Channel Connect','IDS','Google')
			AND sub.subsourcecode != 'SUITEPR'
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_IBE_Hotel]'
GO
ALTER PROCEDURE [dbo].[PH_BookingSource_IBE_Hotel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'IBE - Hotel',autx.ibeSourceName,ss.secondarySource,'#007AFF'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
			LEFT JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			LEFT JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			LEFT JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID NOT IN(	SELECT ibeSource_GroupID
												FROM [authority].[ibeSource_Group]
												WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
											  )
			AND ss.secondarySource != 'IPrefer App'
			AND sub.subsourcecode != 'PHBE'
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'IBE - Hotel',autx.ibeSourceName,ss.secondarySource,'#007AFF'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
			LEFT JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			LEFT JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			LEFT JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID NOT IN(	SELECT ibeSource_GroupID
												FROM [authority].[ibeSource_Group]
												WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
											  )
			AND ss.secondarySource != 'IPrefer App'
			AND sub.subsourcecode != 'PHBE'
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_IBE_Brand]'
GO



ALTER PROCEDURE [dbo].[PH_BookingSource_IBE_Brand]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',autx.ibeSourceName,ss.secondarySource,'#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID IN(	SELECT ibeSource_GroupID
											FROM [authority].[ibeSource_Group]
											WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
										  )
			AND ss.secondarySource != 'IPrefer App'
			AND sub.subsourcecode != 'PHBE'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','I Prefer Mobile App','IPrefer App','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine')
			AND ss.secondarySource = 'IPrefer App'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'IBE - Brand'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			INNER JOIN authority.Pegasus_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'IBE - Brand'

		UNION ALL


		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','Suiteness','Suiteness','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			INNER JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			INNER JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND ss.secondarySource = 'Suiteness'
			AND sub.subsourcecode = 'SUITEPR'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','01 - www.preferredhotels.com','PHBE','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			INNER JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			INNER JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine')
			AND sub.subsourcecode = 'PHBE'
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',autx.ibeSourceName,ss.secondarySource,'#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID IN(	SELECT ibeSource_GroupID
											FROM [authority].[ibeSource_Group]
											WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
										  )
			AND ss.secondarySource != 'IPrefer App'
			AND sub.subsourcecode != 'PHBE'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','I Prefer Mobile App','IPrefer App','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine')
			AND ss.secondarySource = 'IPrefer App'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'IBE - Brand'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			INNER JOIN authority.Pegasus_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND aobs.PH_Channel = 'IBE - Brand'
			AND t.QueueID = @QueueID

		UNION ALL


		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','Suiteness','Suiteness','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			INNER JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			INNER JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND ss.secondarySource = 'Suiteness'
			AND sub.subsourcecode = 'SUITEPR'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','01 - www.preferredhotels.com','PHBE','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			INNER JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			INNER JOIN dbo.CRS_SubSource sub on bs.subsourceid = sub.subsourceid
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine')
			AND sub.subsourcecode = 'PHBE'
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_Group_Import]'
GO



ALTER PROCEDURE [dbo].[PH_BookingSource_Group_Import]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Group Import','None','None','#00FF00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
			
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND sec.secondarySource = 'Group Rooming List Import'
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Group Import','None','None','#00FF00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
			
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND sec.secondarySource = 'Group Rooming List Import'
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_GDS]'
GO


ALTER PROCEDURE [dbo].[PH_BookingSource_GDS]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'GDS',ss.secondarySource,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#4CD964'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel = 'GDS'
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'GDS',ss.secondarySource,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#4CD964'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel = 'GDS'
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_SecondarySource]'
GO



ALTER PROCEDURE [dbo].[Populate_CRS_SecondarySource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_SecondarySource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[secondarySource]
		FROM
		(
			SELECT [SecondarySourceID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[secondarySource]
			FROM synxis.SecondarySource
			WHERE [SecondarySourceID] IN(
									SELECT DISTINCT [SecondarySourceID]
									FROM synxis.Transactions t
										INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
									WHERE t.QueueID = @QueueID
									)

				UNION ALL
			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],NULL AS [pegasusID],obs.Secondary_Source AS [secondarySource]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
			
			UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],BookingSourceID AS [pegasusID],apbs.Secondary_Source AS [secondarySource]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.Pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [secondarySource]
	) AS src ON src.[secondarySource] = tgt.[secondarySource]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[synXisID] + ',','') + ISNULL(src.[synXisID],'')),''),
				[openHospID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[openHospID] + ',','') + ISNULL(src.[openHospID],'')),''),
				[pegasusID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[pegasusID] + ',','') + ISNULL(src.[pegasusID],'')),'')
	WHEN NOT MATCHED BY TARGET THEN
	INSERT([synXisID],[openHospID],[pegasusID],[secondarySource])
	VALUES([synXisID],[openHospID],[pegasusID],[secondarySource])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[vw_Transactions]'
GO



ALTER VIEW [dbo].[vw_Transactions]
AS
	SELECT
		t.transactionTimeStamp,t.confirmationNumber,t.itineraryNumber,t.channelConnectConfirmationNumber,
		td.arrivalDate,td.departureDate,td.bookingLeadTime,td.nights,td.averageDailyRate,td.rooms,td.reservationRevenue,
		td.currency,td.totalPackageRevenue,td.totalGuestCount,td.adultCount,td.childrenCount,td.arrivalDOW,td.departureDOW,
		td.commisionPercent,ts.[status],ts.confirmationDate,ts.cancellationNumber,ts.cancellationDate,td.IsPrimaryGuest,td.optIn,
		bs.channel,bs.secondarySource,bs.subSourceCode,bs.CRO_Code,bs.ibeSourceName,
		ch.chainName,ch.chainID,ch.CRS_ChainID,hh.HotelName,hh.HotelID,hh.HotelCode,td.billingDescription,g.salutation,
		g.FirstName AS guestFirstName,g.LastName AS guestLastName,g.Address1 AS customerAddress1,g.Address2 AS customerAddress2,
		gl.City_Text AS customerCity,gl.State_Text AS customerState,gl.PostalCode_Text AS customerPostalCode,
		g.phone AS customerPhone,gl.Country_Text AS customerCountry,gl.Region_Text customerRegion,
		gc.CompanyName AS customerCompanyName,ge.emailAddress AS customerEmail,
		rc.rateCategoryCode,rtc.RateCode,rt.roomTypeName,rt.roomTypeCode,
		iata.IATANumber,ta.[Name] AS travelAgencyName,ta.Address1 AS travelAgencyAddress1,ta.Address2 AS travelAgencyAddress2,
		tal.City_Text AS travelAgencyCity,tal.State_Text AS travelAgencyState,tal.PostalCode_Text AS travelAgencyPostalCode,
		ta.Phone AS travelAgencyPhone,tal.Country_Text AS travelAgencyCountry,
		tal.Region_Text AS travelAgencyRegion,ta.Email AS travelAgencyEmail,
		cc.corporationCode,pc.promotionalCode,lp.loyaltyProgram,ln.loyaltyNumber,
		
		synXis.SAPID,synXis.faxNotificationCount,synXis.PMSRateTypeCode,synXis.PMSRoomTypeCode,synXis.marketSourceCode,
		synXis.marketSegmentCode,synXis.[customerArea],synXis.[travelAgencyArea],synXis.consortiaCount,
		synXis.consortiaName,td.IsPrimaryGuest AS [primaryGuest],bs.ibeSourceName AS [xbeTemplateName],synXis.xbeShellName
	FROM dbo.Transactions t
		INNER JOIN authority.DataSource ds ON ds.DataSourceID = t.DataSourceID
		INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		INNER JOIN dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
		LEFT JOIN dbo.vw_CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN dbo.Chain ch ON ch.ChainID = t.ChainID
		LEFT JOIN dbo.hotel h ON h.HotelID = t.HotelID
			LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
		LEFT JOIN dbo.Guest g ON g.GuestID = t.GuestID
			LEFT JOIN dbo.vw_Location gl ON gl.LocationID = g.LocationID
			LEFT JOIN dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
		LEFT JOIN dbo.Guest_CompanyName gc ON gc.Guest_CompanyNameID = t.Guest_CompanyNameID
		LEFT JOIN dbo.RateCode rtc ON rtc.RateCodeID = t.RateCodeID
		LEFT JOIN dbo.RateCategory rc ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN dbo.RoomType rt ON rt.RoomTypeID = t.RoomTypeID
		LEFT JOIN dbo.IATANumber iata ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN dbo.TravelAgent ta ON ta.TravelAgentID = t.TravelAgentID
			LEFT JOIN dbo.vw_Location tal ON tal.LocationID = ta.LocationID
		LEFT JOIN dbo.CorporateCode cc ON cc.CorporateCodeID = t.CorporateCodeID
		LEFT JOIN dbo.PromoCode pc ON pc.PromoCodeID = t.PromoCodeID
		LEFT JOIN dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN
		(
			SELECT t.TransactionID AS synXisID,te.SAPID,te.faxNotificationCount,pms.PMSRateTypeCode,rt.PMSRoomTypeCode,
				te.marketSourceCode,te.marketSegmentCode,ga.Area_Text AS [customerArea],taa.Area_Text AS [travelAgencyArea],
				te.consortiaCount,con.consortiaName,xbe.xbeShellName
			FROM synxis.Transactions t
				LEFT JOIN synxis.TransactionsExtended te ON te.TransactionsExtendedID = t.TransactionsExtendedID
				LEFT JOIN synxis.PMSRateTypeCode pms ON pms.PMSRateTypeCodeID = t.PMSRateTypeCodeID
				LEFT JOIN synxis.RoomType rt ON rt.RoomTypeID = t.RoomTypeID
				LEFT JOIN synxis.Guest g ON g.GuestID = t.GuestID
					LEFT JOIN synxis.[Location] gloc ON gloc.LocationID = g.LocationID
					LEFT JOIN synxis.Area ga ON ga.AreaID = gloc.AreaID
				LEFT JOIN synxis.TravelAgent ta ON ta.TravelAgentID = t.TravelAgentID
					LEFT JOIN synxis.[Location] taloc ON taloc.LocationID = ta.LocationID
					LEFT JOIN synxis.Area taa ON taa.AreaID = taloc.AreaID
				LEFT JOIN synxis.Consortia con ON con.ConsortiaID = t.ConsortiaID
				LEFT JOIN synxis.BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.BookingSourceID
				LEFT JOIN synxis.xbeTemplate xbe ON xbe.xbeTemplateID = bs.xbeTemplateID
		) AS synXis ON synXis.synXisID = t.sourceKey AND ds.SourceName = 'SynXis'


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_SubSource]'
GO



ALTER PROCEDURE [dbo].[Populate_CRS_SubSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_SubSource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG(openHospID,',') AS openHospID,STRING_AGG(pegasusID,',') AS pegasusID,[subSource],[subSourceCode]
		FROM
		(
			SELECT [SubSourceID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[subSource],[subSourceCode]
			FROM synxis.SubSource
			WHERE [SubSourceID] IN(
										SELECT DISTINCT [SubSourceID]
										FROM synxis.Transactions t
											INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
										WHERE t.QueueID = @QueueID
										)

				UNION ALL

			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],NULL AS [pegasusID],bs.Bkg_Src_Cd AS [subSource],obs.Sub_Source AS [subSourceCode]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)

				UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],BookingSourceID AS [pegasusID],pbs.Bkg_Src_Cd AS [subSource],apbs.Sub_Source AS [subSourceCode]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [subSource],[subSourceCode]
	) AS src ON src.[subSource] = tgt.[subSource] AND src.[subSourceCode] = tgt.[subSourceCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[synXisID] + ',','') + ISNULL(src.[synXisID],'')),''),
				[openHospID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[openHospID] + ',','') + ISNULL(src.[openHospID],'')),''),
				[pegasusID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[pegasusID] + ',','') + ISNULL(src.[pegasusID],'')),'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],openHospID,pegasusID,[subSource],[subSourceCode])
		VALUES([synXisID],openHospID,pegasusID,[subSource],[subSourceCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest]'
GO




ALTER PROCEDURE [dbo].[Populate_Guest]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
	
	-- #SYNXIS_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#SYNXIS_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #SYNXIS_Guest_EmailAddress;
	CREATE TABLE #SYNXIS_Guest_EmailAddress([Guest_EmailAddressID] int,synXisID int NOT NULL, PRIMARY KEY CLUSTERED(synXisID))
	
	INSERT INTO #SYNXIS_Guest_EmailAddress(Guest_EmailAddressID,synXisID)
	SELECT [Guest_EmailAddressID], value AS synXisID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(synXisID,',')
	------------------------------------------------------
	
	-- #OPEN_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#OPEN_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #OPEN_Guest_EmailAddress;
	CREATE TABLE #OPEN_Guest_EmailAddress([Guest_EmailAddressID] int,openHospID int NOT NULL, PRIMARY KEY CLUSTERED(openHospID))
	
	INSERT INTO #OPEN_Guest_EmailAddress(Guest_EmailAddressID,openHospID)
	SELECT [Guest_EmailAddressID], value AS openHospID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(openHospID,',')
	------------------------------------------------------

	-- #PEGASUS_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#PEGASUS_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #PEGASUS_Guest_EmailAddress;
	CREATE TABLE #PEGASUS_Guest_EmailAddress([Guest_EmailAddressID] int,pegasusID int NOT NULL, PRIMARY KEY CLUSTERED(pegasusID))
	
	INSERT INTO #PEGASUS_Guest_EmailAddress(Guest_EmailAddressID,pegasusID)
	SELECT [Guest_EmailAddressID], value AS pegasusID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(pegasusID,',')
	------------------------------------------------------
	
	-- #SYNXIS_Location ------------------------
	IF OBJECT_ID('tempdb..#SYNXIS_Location') IS NOT NULL
		DROP TABLE #SYNXIS_Location;
	CREATE TABLE #SYNXIS_Location([LocationID] int,synXisID int NOT NULL, PRIMARY KEY CLUSTERED(synXisID))
	
	INSERT INTO #SYNXIS_Location([LocationID],synXisID)
	SELECT [LocationID], value AS synXisID FROM dbo.[Location] CROSS APPLY string_split(synXisID,',')
	------------------------------------------------------ 
	
	-- #OPEN_Location ------------------------
	IF OBJECT_ID('tempdb..#OPEN_Location') IS NOT NULL
		DROP TABLE #OPEN_Location;
	CREATE TABLE #OPEN_Location([LocationID] int,openHospID int NOT NULL, PRIMARY KEY CLUSTERED(openHospID))
	
	INSERT INTO #OPEN_Location([LocationID],openHospID)
	SELECT [LocationID], value AS openHospID FROM dbo.[Location] CROSS APPLY string_split(openHospID,',')
	------------------------------------------------------

	-- #PEGASUS_Location ------------------------
	IF OBJECT_ID('tempdb..#PEGASUS_Location') IS NOT NULL
		DROP TABLE #PEGASUS_Location;
	CREATE TABLE #PEGASUS_Location([LocationID] int,pegasusID int NOT NULL, PRIMARY KEY CLUSTERED(pegasusID))
	
	INSERT INTO #PEGASUS_Location([LocationID],pegasusID)
	SELECT [LocationID], value AS pegasusID FROM dbo.[Location] CROSS APPLY string_split(pegasusID,',')
	------------------------------------------------------
	
	MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
		SELECT MAX([synXisID]) AS [synXisID],MAX([openHospID]) AS [openHospID],MAX([pegasusID]) AS [pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID],
				HASHBYTES('MD5',UPPER(ISNULL([customerID],'')) + '|' + UPPER(ISNULL([salutation],'')) + '|' + UPPER(ISNULL([FirstName],'')) + '|' + UPPER(ISNULL([FirstName],'')) + '|' + UPPER(ISNULL([LastName],'')) + '|' + UPPER(ISNULL([Address1],'')) + '|' + UPPER(ISNULL([Address2],'')) + '|' + UPPER(ISNULL(CONVERT(varchar(20),[LocationID]),'')) + '|' + UPPER(ISNULL([phone],'')) + '|' + UPPER(ISNULL(CONVERT(varchar(20),[Guest_EmailAddressID]),''))) AS hashKey
		FROM
		(
			SELECT [GuestID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM synxis.Guest g
				LEFT JOIN #SYNXIS_Guest_EmailAddress ge ON ge.synXisID = g.Guest_EmailAddressID
				LEFT JOIN #SYNXIS_Location l ON l.synXisID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
			
			UNION ALL
			
			SELECT NULL AS [synXisID],[GuestID] AS [openHospID],NULL AS [pegasusID],NULL AS [customerID],NULL AS [salutation],[FirstName],[LastName],[Address1],NULL AS [Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM openHosp.Guest g
				LEFT JOIN #OPEN_Guest_EmailAddress ge ON ge.openHospID = g.Guest_EmailAddressID
				LEFT JOIN #OPEN_Location l ON l.openHospID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)

			UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [openHospID],[GuestID] AS [pegasusID],NULL AS [customerID],NULL AS [salutation],[FirstName],[LastName],[Address1],NULL AS [Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM pegasus.Guest g
				LEFT JOIN #PEGASUS_Guest_EmailAddress ge ON ge.pegasusID = g.Guest_EmailAddressID
				LEFT JOIN #PEGASUS_Location l ON l.pegasusID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID]
	) AS src ON src.hashKey = tgt.hashKey
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID])
		VALUES([synXisID],[openHospID],[pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID])
	--WHEN NOT MATCHED BY SOURCE THEN
	-- DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_CompanyName]'
GO




ALTER PROCEDURE [dbo].[Populate_Guest_CompanyName]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Guest_CompanyName] AS tgt
	USING
	(
		SELECT DISTINCT [Guest_CompanyNameID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[CompanyName]
		FROM synxis.Guest_CompanyName
		WHERE [Guest_CompanyNameID] IN(SELECT [Guest_CompanyNameID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[CompanyName] = tgt.[CompanyName]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[CompanyName])
		VALUES([synXisID],[openHospID],[pegasusID],[CompanyName])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SabreAPI_Comparison]'
GO


ALTER PROCEDURE [dbo].[SabreAPI_Comparison]
AS
BEGIN
	TRUNCATE TABLE [dbo].[SabreAPI_Compare];

	DECLARE @RunTime datetime = GETDATE();

	IF OBJECT_ID('tempdb..#CONF') IS NOT NULL
		DROP TABLE #CONF;
	CREATE TABLE #CONF(confirmationNumber varchar(20) NOT NULL PRIMARY KEY CLUSTERED(confirmationNumber))

	INSERT INTO #CONF(confirmationNumber)
	SELECT confirmationNumber
	FROM dbo.Transactions


	;WITH cte_Comp
	AS
	(
		SELECT new.[confirmationNumber],
			CASE WHEN ISNULL(new.[itineraryNumber],'') = ISNULL(org.[itineraryNumber],'') THEN NULL ELSE ISNULL(new.[itineraryNumber],'') + '  |  ' + ISNULL(org.[itineraryNumber],'') END AS [itineraryNumber]
			,CASE WHEN ISNULL(new.[channelConnectConfirmationNumber],'') = ISNULL(org.[channelConnectConfirmationNumber],'') THEN NULL ELSE ISNULL(new.[channelConnectConfirmationNumber],'') + '  |  ' + ISNULL(org.[channelConnectConfirmationNumber],'') END AS [channelConnectConfirmationNumber]
			,CASE WHEN ISNULL(new.[arrivalDate],'') = ISNULL(org.[arrivalDate],'') THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.[arrivalDate]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[arrivalDate]),'') END AS [arrivalDate]
			,CASE WHEN ISNULL(new.[departureDate],'') = ISNULL(org.[departureDate],'') THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.[departureDate]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[departureDate]),'') END AS [departureDate]
			,CASE WHEN ISNULL(new.[nights],0) = ISNULL(org.[nights],0) THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.[nights]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[nights]),'') END AS [nights]
			,CASE WHEN ISNULL(new.[averageDailyRate],0.0) = ISNULL(org.[averageDailyRate],0.0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[averageDailyRate]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[averageDailyRate]),'') END AS [averageDailyRate]
			,CASE WHEN ISNULL(new.[rooms],0) = ISNULL(org.[rooms],0) THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.[rooms]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[rooms]),'') END AS [rooms]
			,CASE WHEN ISNULL(new.[reservationRevenue],0.0) = ISNULL(org.[reservationRevenue],0.0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[reservationRevenue]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[reservationRevenue]),'') END AS [reservationRevenue]
			,CASE WHEN ISNULL(new.[currency],0.0) = ISNULL(org.[currency],0.0) THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.[currency]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[currency]),'') END AS [currency]
			,CASE WHEN ISNULL(new.[totalPackageRevenue],0.0) = ISNULL(org.[totalPackageRevenue],0.0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[totalPackageRevenue]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[totalPackageRevenue]),'') END AS [totalPackageRevenue]
			,CASE WHEN ISNULL(new.[totalGuestCount],0) = ISNULL(org.[totalGuestCount],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[totalGuestCount]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[totalGuestCount]),'') END AS [totalGuestCount]
			,CASE WHEN ISNULL(new.[adultCount],0) = ISNULL(org.[adultCount],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[adultCount]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[adultCount]),'') END AS [adultCount]
			,CASE WHEN ISNULL(new.[childrenCount],0) = ISNULL(org.[childrenCount],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[childrenCount]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[childrenCount]),'') END AS [childrenCount]
			,CASE WHEN ISNULL(new.[arrivalDOW],0) = ISNULL(org.[arrivalDOW],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[arrivalDOW]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[arrivalDOW]),'') END AS [arrivalDOW]
			,CASE WHEN ISNULL(new.[departureDOW],0) = ISNULL(org.[departureDOW],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[departureDOW]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[departureDOW]),'') END AS [departureDOW]
			,CASE WHEN ISNULL(new.[commisionPercent],0.0) = ISNULL(org.[commisionPercent],0.0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[commisionPercent]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[commisionPercent]),'') END AS [commisionPercent]
			,CASE WHEN ISNULL(new.[status],'') = ISNULL(org.[status],'') THEN NULL ELSE ISNULL(new.[status],'') + '  |  ' + ISNULL(org.[status],'') END AS [status]
			,CASE WHEN ISNULL(new.[confirmationDate],'') = ISNULL(org.[confirmationDate],'') THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[confirmationDate]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[confirmationDate]),'') END AS [confirmationDate]
			,CASE WHEN ISNULL(new.[cancellationNumber],'') = ISNULL(org.[cancellationNumber],'') THEN NULL ELSE ISNULL(new.[cancellationNumber],'') + '  |  ' + ISNULL(org.[cancellationNumber],'') END AS [cancellationNumber]
			,CASE WHEN ISNULL(new.[cancellationDate],'') = ISNULL(org.[cancellationDate],'') THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[cancellationDate]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[cancellationDate]),'') END AS [cancellationDate]
			,CASE WHEN ISNULL(new.[IsPrimaryGuest],0) = ISNULL(org.[IsPrimaryGuest],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[IsPrimaryGuest]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[IsPrimaryGuest]),'') END AS [IsPrimaryGuest]
			,CASE WHEN ISNULL(new.[optIn],0) = ISNULL(org.[optIn],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[optIn]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[optIn]),'') END AS [optIn]
			,CASE WHEN ISNULL(new.[channel],'') = ISNULL(org.[channel],'') THEN NULL ELSE ISNULL(new.[channel],'') + '  |  ' + ISNULL(org.[channel],'') END AS [channel]
			,CASE WHEN ISNULL(new.[secondarySource],'') = ISNULL(org.[secondarySource],'') THEN NULL ELSE ISNULL(new.[secondarySource],'') + '  |  ' + ISNULL(org.[secondarySource],'') END AS [secondarySource]
			,CASE WHEN ISNULL(new.[subSourceCode],'') = ISNULL(org.[subSourceCode],'') THEN NULL ELSE ISNULL(new.[subSourceCode],'') + '  |  ' + ISNULL(org.[subSourceCode],'') END AS [subSourceCode]
			,CASE WHEN ISNULL(new.[CRO_Code],'') = ISNULL(org.[CRO_Code],'') THEN NULL ELSE ISNULL(new.[CRO_Code],'') + '  |  ' + ISNULL(org.[CRO_Code],'') END AS [CRO_Code]
			,CASE WHEN ISNULL(new.[ibeSourceName],'') = ISNULL(org.[ibeSourceName],'') THEN NULL ELSE ISNULL(new.[ibeSourceName],'') + '  |  ' + ISNULL(org.[ibeSourceName],'') END AS [ibeSourceName]
			,CASE WHEN ISNULL(new.[chainName],'') = ISNULL(org.[chainName],'') THEN NULL ELSE ISNULL(new.[chainName],'') + '  |  ' + ISNULL(org.[chainName],'') END AS [chainName]
			,CASE WHEN ISNULL(new.[HotelName],'') = ISNULL(org.[HotelName],'') THEN NULL ELSE ISNULL(new.[HotelName],'') + '  |  ' + ISNULL(org.[HotelName],'') END AS [HotelName]
			,CASE WHEN ISNULL(new.[HotelID],0) = ISNULL(org.[HotelID],0) THEN NULL ELSE ISNULL(CONVERT(varchar(50),new.[HotelID]),'') + '  |  ' + ISNULL(CONVERT(varchar(50),org.[HotelID]),'') END AS [HotelID]
			,CASE WHEN ISNULL(new.[HotelCode],'') = ISNULL(org.[HotelCode],'') THEN NULL ELSE ISNULL(new.[HotelCode],'') + '  |  ' + ISNULL(org.[HotelCode],'') END AS [HotelCode]
			,CASE WHEN ISNULL(new.[billingDescription],'') = ISNULL(org.[billingDescription],'') THEN NULL ELSE ISNULL(new.[billingDescription],'') + '  |  ' + ISNULL(org.[billingDescription],'') END AS [billingDescription]
			,CASE WHEN ISNULL(new.[salutation],'') = ISNULL(org.[salutation],'') THEN NULL ELSE ISNULL(new.[salutation],'') + '  |  ' + ISNULL(org.[salutation],'') END AS [salutation]
			,CASE WHEN ISNULL(new.[guestFirstName],'') = ISNULL(org.[guestFirstName],'') THEN NULL ELSE ISNULL(new.[guestFirstName],'') + '  |  ' + ISNULL(org.[guestFirstName],'') END AS [guestFirstName]
			,CASE WHEN ISNULL(new.[guestLastName],'') = ISNULL(org.[guestLastName],'') THEN NULL ELSE ISNULL(new.[guestLastName],'') + '  |  ' + ISNULL(org.[guestLastName],'') END AS [guestLastName]
			,CASE WHEN ISNULL(new.[customerAddress1],'') = ISNULL(org.[customerAddress1],'') THEN NULL ELSE ISNULL(new.[customerAddress1],'') + '  |  ' + ISNULL(org.[customerAddress1],'') END AS [customerAddress1]
			,CASE WHEN ISNULL(new.[customerAddress2],'') = ISNULL(org.[customerAddress2],'') THEN NULL ELSE ISNULL(new.[customerAddress2],'') + '  |  ' + ISNULL(org.[customerAddress2],'') END AS [customerAddress2]
			,CASE WHEN ISNULL(new.[customerCity],'') = ISNULL(org.[customerCity],'') THEN NULL ELSE ISNULL(new.[customerCity],'') + '  |  ' + ISNULL(org.[customerCity],'') END AS [customerCity]
			,CASE WHEN ISNULL(new.[customerState],'') = ISNULL(org.[customerState],'') THEN NULL ELSE ISNULL(new.[customerState],'') + '  |  ' + ISNULL(org.[customerState],'') END AS [customerState]

			,CASE WHEN ISNULL(new.[customerPostalCode],'') = ISNULL(org.[customerPostalCode],'') THEN NULL ELSE ISNULL(new.[customerPostalCode],'') + '  |  ' + ISNULL(org.[customerPostalCode],'') END AS [customerPostalCode]
			,CASE WHEN REPLACE(REPLACE(REPLACE(ISNULL(new.[customerPhone],''),'-',''),'+',''),' ','') = REPLACE(REPLACE(REPLACE(ISNULL(org.[customerPhone],''),'-',''),'+',''),' ','') THEN NULL ELSE REPLACE(REPLACE(REPLACE(ISNULL(new.[customerPhone],''),'-',''),'+',''),' ','') + '  |  ' + REPLACE(REPLACE(REPLACE(ISNULL(org.[customerPhone],''),'-',''),'+',''),' ','') END AS [customerPhone]
			,CASE WHEN ISNULL(new.[customerCountry],'') = ISNULL(org.[customerCountry],'') THEN NULL ELSE ISNULL(new.[customerCountry],'') + '  |  ' + ISNULL(org.[customerCountry],'') END AS [customerCountry]
			,CASE WHEN ISNULL(new.[customerRegion],'') = ISNULL(org.[customerRegion],'') THEN NULL ELSE ISNULL(new.[customerRegion],'') + '  |  ' + ISNULL(org.[customerRegion],'') END AS [customerRegion]
			,CASE WHEN ISNULL(new.[customerCompanyName],'') = ISNULL(org.[customerCompanyName],'') THEN NULL ELSE ISNULL(new.[customerCompanyName],'') + '  |  ' + ISNULL(org.[customerCompanyName],'') END AS [customerCompanyName]
			,CASE WHEN ISNULL(new.[customerEmail],'') = ISNULL(org.[customerEmail],'') THEN NULL ELSE ISNULL(new.[customerEmail],'') + '  |  ' + ISNULL(org.[customerEmail],'') END AS [customerEmail]
			,CASE WHEN ISNULL(new.[rateCategoryCode],'') = ISNULL(org.[rateCategoryCode],'') THEN NULL ELSE ISNULL(new.[rateCategoryCode],'') + '  |  ' + ISNULL(org.[rateCategoryCode],'') END AS [rateCategoryCode]
			,CASE WHEN ISNULL(new.[RateCode],'') = ISNULL(org.[RateCode],'') THEN NULL ELSE ISNULL(new.[RateCode],'') + '  |  ' + ISNULL(org.[RateCode],'') END AS [RateCode]
			,CASE WHEN ISNULL(new.[roomTypeName],'') = ISNULL(org.[roomTypeName],'') THEN NULL ELSE ISNULL(new.[roomTypeName],'') + '  |  ' + ISNULL(org.[roomTypeName],'') END AS [roomTypeName]
			,CASE WHEN ISNULL(new.[roomTypeCode],'') = ISNULL(org.[roomTypeCode],'') THEN NULL ELSE ISNULL(new.[roomTypeCode],'') + '  |  ' + ISNULL(org.[roomTypeCode],'') END AS [roomTypeCode]
			,CASE WHEN ISNULL(new.[IATANumber],'') = ISNULL(org.[IATANumber],'') THEN NULL ELSE ISNULL(new.[IATANumber],'') + '  |  ' + ISNULL(org.[IATANumber],'') END AS [IATANumber]
			,CASE WHEN ISNULL(new.[travelAgencyName],'') = ISNULL(org.[travelAgencyName],'') THEN NULL ELSE ISNULL(new.[travelAgencyName],'') + '  |  ' + ISNULL(org.[travelAgencyName],'') END AS [travelAgencyName]
			,CASE WHEN ISNULL(new.[travelAgencyAddress1],'') = ISNULL(org.[travelAgencyAddress1],'') THEN NULL ELSE ISNULL(new.[travelAgencyAddress1],'') + '  |  ' + ISNULL(org.[travelAgencyAddress1],'') END AS [travelAgencyAddress1]
			,CASE WHEN ISNULL(new.[travelAgencyAddress2],'') = ISNULL(org.[travelAgencyAddress2],'') THEN NULL ELSE ISNULL(new.[travelAgencyAddress2],'') + '  |  ' + ISNULL(org.[travelAgencyAddress2],'') END AS [travelAgencyAddress2]
			,CASE WHEN ISNULL(new.[travelAgencyCity],'') = ISNULL(org.[travelAgencyCity],'') THEN NULL ELSE ISNULL(new.[travelAgencyCity],'') + '  |  ' + ISNULL(org.[travelAgencyCity],'') END AS [travelAgencyCity]
			,CASE WHEN ISNULL(new.[travelAgencyState],'') = ISNULL(org.[travelAgencyState],'') THEN NULL ELSE ISNULL(new.[travelAgencyState],'') + '  |  ' + ISNULL(org.[travelAgencyState],'') END AS [travelAgencyState]
			,CASE WHEN ISNULL(new.[travelAgencyPostalCode],'') = ISNULL(org.[travelAgencyPostalCode],'') THEN NULL ELSE ISNULL(new.[travelAgencyPostalCode],'') + '  |  ' + ISNULL(org.[travelAgencyPostalCode],'') END AS [travelAgencyPostalCode]
			,CASE WHEN ISNULL(new.[travelAgencyPhone],'') = ISNULL(org.[travelAgencyPhone],'') THEN NULL ELSE ISNULL(new.[travelAgencyPhone],'') + '  |  ' + ISNULL(org.[travelAgencyPhone],'') END AS [travelAgencyPhone]
			,CASE WHEN ISNULL(new.[travelAgencyCountry],'') = ISNULL(org.[travelAgencyCountry],'') THEN NULL ELSE ISNULL(new.[travelAgencyCountry],'') + '  |  ' + ISNULL(org.[travelAgencyCountry],'') END AS [travelAgencyCountry]
			,CASE WHEN ISNULL(new.[travelAgencyRegion],'') = ISNULL(org.[travelAgencyRegion],'') THEN NULL ELSE ISNULL(new.[travelAgencyRegion],'') + '  |  ' + ISNULL(org.[travelAgencyRegion],'') END AS [travelAgencyRegion]
			,CASE WHEN ISNULL(new.[travelAgencyEmail],'') = ISNULL(org.[travelAgencyEmail],'') THEN NULL ELSE ISNULL(new.[travelAgencyEmail],'') + '  |  ' + ISNULL(org.[travelAgencyEmail],'') END AS [travelAgencyEmail]
			,CASE WHEN ISNULL(new.[corporationCode],'') = ISNULL(org.[corporationCode],'') THEN NULL ELSE ISNULL(new.[corporationCode],'') + '  |  ' + ISNULL(org.[corporationCode],'') END AS [corporationCode]
			,CASE WHEN ISNULL(new.[promotionalCode],'') = ISNULL(org.[promotionalCode],'') THEN NULL ELSE ISNULL(new.[promotionalCode],'') + '  |  ' + ISNULL(org.[promotionalCode],'') END AS [promotionalCode]
			,CASE WHEN ISNULL(new.[loyaltyProgram],'') = ISNULL(org.[loyaltyProgram],'') THEN NULL ELSE ISNULL(new.[loyaltyProgram],'') + '  |  ' + ISNULL(org.[loyaltyProgram],'') END AS [loyaltyProgram]
			,CASE WHEN ISNULL(new.[loyaltyNumber],'') = ISNULL(org.[loyaltyNumber],'') THEN NULL ELSE ISNULL(new.[loyaltyNumber],'') + '  |  ' + ISNULL(org.[loyaltyNumber],'') END AS [loyaltyNumber]

			,CASE WHEN ISNULL(new.SAPID,'') = ISNULL(org.SAPID,'') THEN NULL ELSE ISNULL(new.SAPID,'') + '  |  ' + ISNULL(org.SAPID,'') END AS SAPID
			,CASE WHEN ISNULL(new.faxNotificationCount,'') = ISNULL(org.faxNotificationCount,'') THEN NULL ELSE ISNULL(new.faxNotificationCount,'') + '  |  ' + ISNULL(org.faxNotificationCount,'') END AS faxNotificationCount
			,CASE WHEN ISNULL(new.PMSRateTypeCode,'') = ISNULL(org.PMSRateTypeCode,'') THEN NULL ELSE ISNULL(new.PMSRateTypeCode,'') + '  |  ' + ISNULL(org.PMSRateTypeCode,'') END AS PMSRateTypeCode
			,CASE WHEN ISNULL(new.PMSRoomTypeCode,'') = ISNULL(org.PMSRoomTypeCode,'') THEN NULL ELSE ISNULL(new.PMSRoomTypeCode,'') + '  |  ' + ISNULL(org.PMSRoomTypeCode,'') END AS PMSRoomTypeCode
			,CASE WHEN ISNULL(new.marketSourceCode,'') = ISNULL(org.marketSourceCode,'') THEN NULL ELSE ISNULL(new.marketSourceCode,'') + '  |  ' + ISNULL(org.marketSourceCode,'') END AS marketSourceCode
			,CASE WHEN ISNULL(new.marketSegmentCode,'') = ISNULL(org.marketSegmentCode,'') THEN NULL ELSE ISNULL(new.marketSegmentCode,'') + '  |  ' + ISNULL(org.marketSegmentCode,'') END AS marketSegmentCode
			,CASE WHEN ISNULL(new.[customerArea],'') = ISNULL(org.[customerArea],'') THEN NULL ELSE ISNULL(new.[customerArea],'') + '  |  ' + ISNULL(org.[customerArea],'') END AS [customerArea]
			,CASE WHEN ISNULL(new.[travelAgencyArea],'') = ISNULL(org.[travelAgencyArea],'') THEN NULL ELSE ISNULL(new.[travelAgencyArea],'') + '  |  ' + ISNULL(org.[travelAgencyArea],'') END AS [travelAgencyArea]
			,CASE WHEN ISNULL(new.consortiaCount,0) = ISNULL(org.consortiaCount,0) THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.consortiaCount),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.consortiaCount),'') END AS consortiaCount
			,CASE WHEN ISNULL(new.consortiaName,'') = ISNULL(org.consortiaName,'') THEN NULL ELSE ISNULL(new.consortiaName,'') + '  |  ' + ISNULL(org.consortiaName,'') END AS consortiaName
			,CASE WHEN ISNULL(new.[primaryGuest],0) = ISNULL(org.[primaryGuest],0) THEN NULL ELSE ISNULL(CONVERT(varchar(20),new.[primaryGuest]),'') + '  |  ' + ISNULL(CONVERT(varchar(20),org.[primaryGuest]),'') END AS [primaryGuest]
			,CASE WHEN ISNULL(new.[xbeTemplateName],'') = ISNULL(org.[xbeTemplateName],'') THEN NULL ELSE ISNULL(new.[xbeTemplateName],'') + '  |  ' + ISNULL(org.[xbeTemplateName],'') END AS [xbeTemplateName]
			,CASE WHEN ISNULL(new.xbeShellName,'') = ISNULL(org.xbeShellName,'') THEN NULL ELSE ISNULL(new.xbeShellName,'') + '  |  ' + ISNULL(org.xbeShellName,'') END AS xbeShellName
		FROM [dbo].[vw_Transactions] new
			INNER JOIN [dbo].[vw_Transactions] org ON org.confirmationNumber = new.confirmationNumber
		WHERE new.confirmationNumber IN(SELECT confirmationNumber FROM #CONF)
	 )
	 INSERT INTO [dbo].[SabreAPI_Compare]([RunTime],[confirmationNumber],[itineraryNumber],[channelConnectConfirmationNumber],[arrivalDate],[departureDate],[nights],[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],[adultCount],[childrenCount],[arrivalDOW],[departureDOW],[commisionPercent],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[IsPrimaryGuest],[optIn],[channel],[secondarySource],[subSourceCode],[CRO_Code],[ibeSourceName],[chainName],[HotelName],[HotelID],[HotelCode],[billingDescription],[salutation],[guestFirstName],[guestLastName],[customerAddress1],[customerAddress2],[customerCity],[customerState],[customerPostalCode],[customerPhone],[customerCountry],[customerRegion],[customerCompanyName],[customerEmail],[rateCategoryCode],[RateCode],[roomTypeName],[roomTypeCode],[IATANumber],[travelAgencyName],[travelAgencyAddress1],[travelAgencyAddress2],[travelAgencyCity],[travelAgencyState],[travelAgencyPostalCode],[travelAgencyPhone],[travelAgencyCountry],[travelAgencyRegion],[travelAgencyEmail],[corporationCode],[promotionalCode],[loyaltyProgram],[loyaltyNumber],
											[SAPID],[faxNotificationCount],[PMSRateTypeCode],[PMSRoomTypeCode],[marketSourceCode],[marketSegmentCode],[customerArea],[travelAgencyArea],[consortiaCount],[consortiaName],[primaryGuest],[xbeTemplateName])
	 SELECT @RunTime AS [RunTime],[confirmationNumber],[itineraryNumber],[channelConnectConfirmationNumber],[arrivalDate],[departureDate],[nights],[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],[adultCount],[childrenCount],[arrivalDOW],[departureDOW],[commisionPercent],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[IsPrimaryGuest],[optIn],[channel],[secondarySource],[subSourceCode],[CRO_Code],[ibeSourceName],[chainName],[HotelName],[HotelID],[HotelCode],[billingDescription],[salutation],[guestFirstName],[guestLastName],[customerAddress1],[customerAddress2],[customerCity],[customerState],[customerPostalCode],[customerPhone],[customerCountry],[customerRegion],[customerCompanyName],[customerEmail],[rateCategoryCode],[RateCode],[roomTypeName],[roomTypeCode],[IATANumber],[travelAgencyName],[travelAgencyAddress1],[travelAgencyAddress2],[travelAgencyCity],[travelAgencyState],[travelAgencyPostalCode],[travelAgencyPhone],[travelAgencyCountry],[travelAgencyRegion],[travelAgencyEmail],[corporationCode],[promotionalCode],[loyaltyProgram],[loyaltyNumber],
						[SAPID],[faxNotificationCount],[PMSRateTypeCode],[PMSRoomTypeCode],[marketSourceCode],[marketSegmentCode],[customerArea],[travelAgencyArea],[consortiaCount],[consortiaName],[primaryGuest],[xbeTemplateName]
	 FROM cte_Comp
	 WHERE [itineraryNumber] IS NOT NULL
		OR [channelConnectConfirmationNumber] IS NOT NULL
		OR [arrivalDate] IS NOT NULL
		OR [departureDate] IS NOT NULL
		OR [nights] IS NOT NULL
		OR [averageDailyRate] IS NOT NULL
		OR [rooms] IS NOT NULL
		OR [reservationRevenue] IS NOT NULL
		OR [currency] IS NOT NULL
		OR [totalPackageRevenue] IS NOT NULL
		OR [totalGuestCount] IS NOT NULL
		OR [adultCount] IS NOT NULL
		OR [childrenCount] IS NOT NULL
		OR [arrivalDOW] IS NOT NULL
		OR [departureDOW] IS NOT NULL
		OR [commisionPercent] IS NOT NULL
		OR [status] IS NOT NULL
		OR [confirmationDate] IS NOT NULL
		OR [cancellationNumber] IS NOT NULL
		OR [cancellationDate] IS NOT NULL
		OR [IsPrimaryGuest] IS NOT NULL
		OR [optIn] IS NOT NULL
		OR [channel] IS NOT NULL
		OR [secondarySource] IS NOT NULL
		OR [subSourceCode] IS NOT NULL
		OR [CRO_Code] IS NOT NULL
		OR [ibeSourceName] IS NOT NULL
		OR [chainName] IS NOT NULL
		OR [HotelName] IS NOT NULL
		OR [HotelID] IS NOT NULL
		OR [HotelCode] IS NOT NULL
		OR [billingDescription] IS NOT NULL
		OR [salutation] IS NOT NULL
		OR [guestFirstName] IS NOT NULL
		OR [guestLastName] IS NOT NULL
		OR [customerAddress1] IS NOT NULL
		OR [customerAddress2] IS NOT NULL
		OR [customerCity] IS NOT NULL
		OR [customerState] IS NOT NULL
		OR [customerPostalCode] IS NOT NULL
		OR [customerPhone] IS NOT NULL
		OR [customerCountry] IS NOT NULL
		OR [customerRegion] IS NOT NULL
		OR [customerCompanyName] IS NOT NULL
		OR [customerEmail] IS NOT NULL
		OR [rateCategoryCode] IS NOT NULL
		OR [RateCode] IS NOT NULL
		OR [roomTypeName] IS NOT NULL
		OR [roomTypeCode] IS NOT NULL
		OR [IATANumber] IS NOT NULL
		OR [travelAgencyName] IS NOT NULL
		OR [travelAgencyAddress1] IS NOT NULL
		OR [travelAgencyAddress2] IS NOT NULL
		OR [travelAgencyCity] IS NOT NULL
		OR [travelAgencyState] IS NOT NULL
		OR [travelAgencyPostalCode] IS NOT NULL
		OR [travelAgencyPhone] IS NOT NULL
		OR [travelAgencyCountry] IS NOT NULL
		OR [travelAgencyRegion] IS NOT NULL
		OR [travelAgencyEmail] IS NOT NULL
		OR [corporationCode] IS NOT NULL
		OR [promotionalCode] IS NOT NULL
		OR [loyaltyProgram] IS NOT NULL
		OR [SAPID] IS NOT NULL
		OR [faxNotificationCount] IS NOT NULL
		OR [PMSRateTypeCode] IS NOT NULL
		OR [PMSRoomTypeCode] IS NOT NULL
		OR [marketSourceCode] IS NOT NULL
		OR [marketSegmentCode] IS NOT NULL
		OR [customerArea] IS NOT NULL
		OR [travelAgencyArea] IS NOT NULL
		OR [consortiaCount] IS NOT NULL
		OR [consortiaName] IS NOT NULL
		OR [primaryGuest] IS NOT NULL
		OR [xbeTemplateName] IS NOT NULL 
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Fix_Duplicate_Guest]'
GO


ALTER PROCEDURE [dbo].[Fix_Duplicate_Guest]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT OFF;

	IF OBJECT_ID('tempdb..#DUPS') IS NOT NULL
		DROP TABLE #DUPS;
	
	CREATE TABLE #DUPS
	(
		GoodID int NOT NULL,
		BadID int NOT NULL,

		PRIMARY KEY CLUSTERED(GoodID,BadID)
	)
	
	-- SYNXIS ------------------------------------------------------------------
	;WITH cte_Dups
	AS
	(
		SELECT g.synXisID,g.GuestID,
			ROW_NUMBER() OVER(PARTITION BY g.synXisID ORDER BY g.GuestID) AS rowNum
		FROM dbo.Guest g
			INNER JOIN dbo.Location l ON l.LocationID = g.LocationID
		WHERE g.synxisID IN(SELECT synxisID FROM dbo.Guest GROUP BY synxisID HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d1.GuestID AS GoodID, d2.GuestID AS BadID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.synXisID = d1.synXisID AND d1.GuestID != d2.GuestID
	WHERE d1.rowNum = 1
		AND d2.rowNum > 1
	

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.synXis_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)


	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------

	-- OPEN HOSPITALITY --------------------------------------------------------
	DELETE #DUPS

	;WITH cte_Dups
	AS
	(
		SELECT g.openHospID,g.GuestID,
			ROW_NUMBER() OVER(PARTITION BY g.openHospID ORDER BY g.GuestID) AS rowNum
		FROM dbo.Guest g
			INNER JOIN dbo.Location l ON l.LocationID = g.LocationID
		WHERE g.openHospID IN(SELECT openHospID FROM dbo.Guest GROUP BY openHospID HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d1.GuestID AS GoodID, d2.GuestID AS BadID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.openHospID = d1.openHospID AND d1.GuestID != d2.GuestID
	WHERE d1.rowNum = 1
		AND d2.rowNum > 1
	

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.openHosp_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)


	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------
	
	-- PEGASUS --------------------------------------------------------
	DELETE #DUPS

	;WITH cte_Dups
	AS
	(
		SELECT g.pegasusID,g.GuestID,
			ROW_NUMBER() OVER(PARTITION BY g.pegasusID ORDER BY g.GuestID) AS rowNum
		FROM dbo.Guest g
			INNER JOIN dbo.Location l ON l.LocationID = g.LocationID
		WHERE g.pegasusID IN(SELECT pegasusID FROM dbo.Guest GROUP BY pegasusID HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d1.GuestID AS GoodID, d2.GuestID AS BadID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.pegasusID = d1.pegasusID AND d1.GuestID != d2.GuestID
	WHERE d1.rowNum = 1
		AND d2.rowNum > 1
	

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.pegasus_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)


	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------
	-- hashKey -----------------------------------------------------------------
	DELETE #DUPS

	;WITH cte_Dups
	AS
	(
		SELECT hashKey,GuestID, ROW_NUMBER() OVER(PARTITION BY hashKey ORDER BY GuestID) AS rowNum
		FROM dbo.Guest
		WHERE hashKey IN(SELECT hashKey FROM dbo.Guest GROUP BY hashKey HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d2.GuestID,d1.GuestID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.hashKey = d1.hashKey AND d2.rowNum = 2
	WHERE d1.rowNum = 1

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.synXis_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)

	DELETE map.openHosp_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)

	DELETE map.pegasus_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)

	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_EmailAddress]'
GO




ALTER PROCEDURE [dbo].[Populate_Guest_EmailAddress]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Guest_EmailAddress] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[emailAddress]
		FROM
		(
			SELECT [Guest_EmailAddressID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[emailAddress]
			FROM synxis.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM [synxis].[Transactions] t
												INNER JOIN [synxis].[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
				UNION ALL
			SELECT NULL AS [synXisID],[Guest_EmailAddressID] AS [openHospID],NULL AS [pegasusID],[emailAddress]
			FROM openHosp.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM openHosp.[Transactions] t
												INNER JOIN openHosp.[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[Guest_EmailAddressID] AS [pegasusID],[emailAddress]
			FROM pegasus.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM pegasus.[Transactions] t
												INNER JOIN pegasus.[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
		) x
		GROUP BY [emailAddress]
	) AS src ON src.[emailAddress] = tgt.[emailAddress]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[emailAddress])
		VALUES([synXisID],[openHospID],[pegasusID],[emailAddress])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_FromLocation]'
GO






ALTER PROCEDURE [dbo].[Populate_Guest_FromLocation]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
		
	-- #SYNXIS_Location ------------------------
	IF OBJECT_ID('tempdb..#Locations_Location') IS NOT NULL
		DROP TABLE #Locations_Location;
	CREATE TABLE #Locations_Location([LocationID] int,Locations_LocationID int NOT NULL, SynxisID varchar(max) NULL, OpenHospID varchar(max))
	
	INSERT INTO #Locations_Location([LocationID],Locations_LocationID, SynxisID, OpenHospID)
	SELECT [LocationID], value AS Locations_LocationID, SynxisID, OpenHospID FROM dbo.[Location] CROSS APPLY string_split(Locations_LocationID,',')
	------------------------------------------------------ 
		
	MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
			SELECT a.[Address1],a.[Address2] + ' ' + a.address3 AS [Address2],l.[LocationID], a.AddressID, g.guestID, lhkm.LocationHashKey
			FROM synxis.Guest g
			INNER JOIN Locations.dbo.LocationHashKeyMapping lhkm ON g.Location_LocationHashKey = lhkm.LocationHashKey
			INNER JOIN Locations.dbo.Address a ON lhkm.AddressID = a.AddressID
			LEFT JOIN #Locations_Location l ON l.Locations_LocationID = a.[LocationID] AND (SynxisID IS NOT NULL OR (OpenHospID IS NULL AND SynxisID IS NULL) )


	) AS src ON src.guestID = tgt.synxisID
	WHEN MATCHED THEN
		UPDATE
			SET [Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Location_AddressID] = src.[AddressID],
				[Location_LocationHashKey] = src.LocationHashKey
	--WHEN NOT MATCHED BY SOURCE THEN
	-- DELETE
	;

		MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
			SELECT a.[Address1],a.[Address2] + ' ' + a.address3 AS [Address2],l.[LocationID], a.AddressID, g.guestID,lhkm.LocationHashKey
			FROM OpenHosp.Guest g
			INNER JOIN Locations.dbo.LocationHashKeyMapping lhkm ON g.Location_LocationHashKey = lhkm.LocationHashKey
			INNER JOIN Locations.dbo.Address a ON lhkm.AddressID = a.AddressID
			LEFT JOIN #Locations_Location l ON l.Locations_LocationID = a.[LocationID] AND (OpenHospID IS NOT NULL OR (OpenHospID IS NULL AND SynxisID IS NULL) )

	) AS src ON src.guestID = tgt.openhospid
	WHEN MATCHED THEN
		UPDATE
			SET [Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Location_AddressID] = src.[AddressID],
				[Location_LocationHashKey] = src.LocationHashKey
	--WHEN NOT MATCHED BY SOURCE THEN
	-- DELETE
	;

						
			

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_hotel]'
GO



ALTER PROCEDURE [dbo].[Populate_hotel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	-- ADD MISSING HOTELS to Hotels.dbo.Hotel -----------------
/*
	;WITH cte_Code(HotelCode)
	AS
	(
		SELECT HotelCode FROM Reservations.openHosp.hotel
			EXCEPT
		SELECT OpenHospCode FROM Hotels.dbo.Hotel
	)
	MERGE INTO Hotels.dbo.Hotel AS tgt
	USING
	(
		SELECT h.HotelCode,h.HotelName,h.HotelId AS OpenHospID,h.HotelCode AS OpenHospCode
		FROM Reservations.openHosp.hotel h
			INNER JOIN cte_Code c ON c.HotelCode = h.HotelCode
	) AS src ON src.HotelCode = tgt.HotelCode
	WHEN MATCHED THEN
		UPDATE
			SET OpenHospID = src.OpenHospID,
				OpenHospCode = src.OpenHospCode
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (HotelCode,HotelName,OpenHospID,OpenHospCode)
		VALUES(src.HotelCode,src.HotelName,src.OpenHospID,src.OpenHospCode)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;


	;WITH cte_Code(HotelId)
	AS
	(
		SELECT HotelId FROM Reservations.synXis.hotel
			EXCEPT
		SELECT SynXisID FROM Hotels.dbo.Hotel
	)
	MERGE INTO Hotels.dbo.Hotel AS tgt
	USING
	(
		SELECT h.HotelCode,h.HotelName,h.HotelId AS SynXisID,h.HotelCode AS SynXisCode
		FROM Reservations.synXis.hotel h
			INNER JOIN cte_Code c ON c.HotelId = h.HotelId
	) AS src ON src.HotelCode = tgt.HotelCode
	WHEN MATCHED THEN
		UPDATE
			SET SynXisID = src.SynXisID,
				SynXisCode = src.SynXisCode
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(HotelCode,HotelName,SynXisID,SynXisCode)
		VALUES(src.HotelCode,src.HotelName,src.SynXisID,src.SynXisCode)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
*/
	-----------------------------------------------------------

	MERGE INTO [dbo].[hotel] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[HotelId]
		FROM
		(
			SELECT [intHotelID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],MAX(shh.[HotelId]) AS [HotelId]
			FROM synxis.hotel sh
				INNER JOIN Hotels.[dbo].[Hotel] shh ON shh.[SynXisID] = sh.[HotelId]
			WHERE [intHotelID] IN(SELECT [intHotelID] FROM [synxis].[Transactions])
			GROUP BY [intHotelID]
				UNION ALL
			SELECT NULL AS [synXisID],[intHotelID] AS [openHospID],NULL AS [pegasusID],MAX(ISNULL(ohID.[HotelId],ohCode.[HotelId])) AS [HotelId]
			FROM openHosp.hotel oh
				LEFT JOIN Hotels.[dbo].[Hotel] ohID ON ohID.[OpenHospID] = oh.[HotelId]
				LEFT JOIN Hotels.[dbo].[Hotel] ohCode ON ohCode.OpenHospCode = oh.HotelCode
			WHERE ISNULL(ohID.[HotelId],ohCode.[HotelId]) IS NOT NULL
				AND [intHotelID] IN(SELECT [intHotelID] FROM openHosp.[Transactions])
			GROUP BY [intHotelID]
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[intHotelID] AS [pegasusID],MAX(ISNULL(ohID.[HotelId],ohCode.[HotelId])) AS [HotelId]
			FROM pegasus.hotel oh
				LEFT JOIN Hotels.[dbo].[Hotel] ohID ON ohID.PegasusRT4ID = oh.[HotelId]
				LEFT JOIN Hotels.[dbo].[Hotel] ohCode ON ohCode.OpenHospCode = oh.HotelCode
			WHERE ISNULL(ohID.[HotelId],ohCode.[HotelId]) IS NOT NULL
				AND [intHotelID] IN(SELECT [intHotelID] FROM pegasus.[Transactions])
			GROUP BY [intHotelID]
		) x
		GROUP BY [HotelId]
	) AS src ON src.[HotelId] = tgt.[Hotel_hotelID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[Hotel_hotelID])
		VALUES([synXisID],[openHospID],[pegasusID],[HotelId])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_IATANumber]'
GO



ALTER PROCEDURE [dbo].[Populate_IATANumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[IATANumber] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[IATANumber]
		FROM
		(
			SELECT [IATANumberID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[IATANumber]
			FROM synxis.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[IATANumberID] AS [openHospID],NULL AS [pegasusID],[IATANumber]
			FROM openHosp.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[IATANumberID] AS [pegasusID],[IATANumber]
			FROM pegasus.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		WHERE ISNULL(IATANumber,'') != ''
		GROUP BY [IATANumber]
	) AS src ON src.[IATANumber] = tgt.[IATANumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[IATANumber])
		VALUES([synXisID],[openHospID],[pegasusID],[IATANumber])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_ibeSource]'
GO



ALTER PROCEDURE [dbo].[Populate_ibeSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[ibeSource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],ibe.ibeSourceID
		FROM
		(
			SELECT [xbeTemplateID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[xbeTemplateName]
			FROM synxis.xbeTemplate
			WHERE [xbeTemplateID] IN(
								SELECT DISTINCT [xbeTemplateID]
								FROM synxis.Transactions t
									INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
								WHERE t.QueueID = @QueueID
								)
				UNION ALL
			SELECT NULL AS [synXisID],bs.BookingSourceID AS [openHospID],NULL AS [pegasusID],obs.Template AS [xbeTemplateName]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
				
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],pbs.BookingSourceID AS [pegasusID],apbs.Template AS [xbeTemplateName]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
				
		) x
		INNER JOIN authority.ibeSource ibe ON ibe.ibeSourceName = x.xbeTemplateName
		GROUP BY ibe.ibeSourceID
	) AS src ON src.ibeSourceID = tgt.[auth_ibeSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[synXisID] + ',','') + ISNULL(src.[synXisID],'')),''),
				[openHospID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[openHospID] + ',','') + ISNULL(src.[openHospID],'')),''),
				[pegasusID] = NULLIF([dbo].[Uniquify_CSV](ISNULL(tgt.[pegasusID] + ',','') + ISNULL(src.[pegasusID],'')),'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[auth_ibeSourceID])
		VALUES([synXisID],[openHospID],[pegasusID],ibeSourceID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Location]'
GO




ALTER PROCEDURE [dbo].[Populate_Location]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		EXEC [dbo].[Populate_Location_Synxis] @QueueID
	ELSE IF EXISTS(SELECT * FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
		EXEC [dbo].[Populate_Location_OpenHosp] @QueueID
	ELSE IF EXISTS(SELECT * FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		EXEC [dbo].[Populate_Location_Pegasus] @QueueID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_LoyaltyNumber]'
GO



ALTER PROCEDURE [dbo].[Populate_LoyaltyNumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[LoyaltyNumber] AS tgt
	USING
	(
		SELECT DISTINCT [LoyaltyNumberID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[loyaltyNumber]
		FROM synxis.LoyaltyNumber
		WHERE [LoyaltyNumberID] IN(SELECT [LoyaltyNumberID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
			AND [loyaltyNumber] != ''
	) AS src ON src.[loyaltyNumber] = tgt.[loyaltyNumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[loyaltyNumber])
		VALUES([synXisID],[openHospID],[pegasusID],[loyaltyNumber])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_LoyaltyProgram]'
GO



ALTER PROCEDURE [dbo].[Populate_LoyaltyProgram]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[LoyaltyProgram] AS tgt
	USING
	(
		SELECT DISTINCT [LoyaltyProgramID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[loyaltyProgram]
		FROM synxis.LoyaltyProgram
		WHERE [LoyaltyProgramID] IN(SELECT [LoyaltyProgramID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[loyaltyProgram] = tgt.[loyaltyProgram]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[loyaltyProgram])
		VALUES([synXisID],[openHospID],[pegasusID],[loyaltyProgram])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Upload_LoyaltyNumber]'
GO







ALTER PROCEDURE [dbo].[Upload_LoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ThirtyDaysAgo datetime = DATEADD(day,-30,GETDATE());
	DECLARE @LN_NULL int;

	SELECT @LN_NULL = LoyaltyNumberID FROM dbo.LoyaltyNumber WHERE LEN(loyaltyNumber) = 0


	-- CREATE #MRT_LN -------------------------------------------------------------
	IF OBJECT_ID('tempdb..#MRT_LN') IS NOT NULL
		DROP TABLE #MRT_LN;
	CREATE TABLE #MRT_LN
	(
		TransactionID int NOT NULL,
		[iPrefer_Number] nvarchar(50) NOT NULL,
		PRIMARY KEY CLUSTERED(TransactionID,[iPrefer_Number])
	)
	-------------------------------------------------------------------------------

	-- CREATE & POPULATE #CPI -----------------------------------------------------
	IF OBJECT_ID('tempdb..#MRT_CPI') IS NOT NULL
		DROP TABLE #MRT_CPI;

	CREATE TABLE #MRT_CPI
	(
		[iPrefer_Number] [nvarchar](50) NOT NULL,
		[Email] [nvarchar](255) NOT NULL,
		[Membership_Date] [date] NOT NULL,

		PRIMARY KEY CLUSTERED([Email],[Membership_Date],[iPrefer_Number])
	)

	INSERT INTO #MRT_CPI([iPrefer_Number],[Email],[Membership_Date])
	SELECT DISTINCT
		ln.LoyaltyNumberName AS [iPrefer_Number]
		,ge.emailAddress AS [Email]
		,mp.Enrollment_Date AS [Membership_Date]
	FROM Loyalty.dbo.LoyaltyNumber ln
		LEFT JOIN Guests.dbo.Guest g ON ln.LoyaltyNumberID = g.LoyaltyNumberID
		LEFT JOIN Guests.dbo.Guest_EmailAddress ge on ge.Guest_EmailAddressID = g.Guest_EmailAddressID
		LEFT JOIN Loyalty.dbo.MemberProfile mp ON ln.LoyaltyNumberID = mp.LoyaltyNumberID
	WHERE NULLIF(ge.emailAddress,'') IS NOT NULL
		AND mp.Enrollment_Date IS NOT NULL
		AND mp.[MemberStatus] = 1
	-------------------------------------------------------------------------------

	------------------------------------------------------------------------
	--add loyalty number to the reservation if it came from the I Prefer website and we can match the email address
	------------------------------------------------------------------------
	;WITH cte_MRT
	AS
	(
		SELECT t.TransactionID,ge.emailAddress,td.arrivalDate
		FROM [dbo].Transactions t
			INNER JOIN [dbo].TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN dbo.vw_CRS_BookingSource crs ON crs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN [dbo].Guest g ON g.GuestID = t.GuestID
			INNER JOIN [dbo].Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
		WHERE NULLIF(t.LoyaltyNumberID,@LN_NULL) IS NULL
			AND crs.ibeSource_GroupID IN(3)
			AND t.transactionTimeStamp >= @ThirtyDaysAgo
	)
	INSERT INTO #MRT_LN(TransactionID,iPrefer_Number)
	SELECT mrt.TransactionID,cpi.iPrefer_Number
	FROM #MRT_CPI cpi
		INNER JOIN cte_MRT mrt ON mrt.emailAddress = NULLIF(cpi.Email,'')
							AND mrt.arrivalDate >= cpi.Membership_Date
							AND mrt.arrivalDate < CAST('9999-09-09' AS date)

	INSERT INTO [dbo].LoyaltyNumber(loyaltyNumber)
	SELECT DISTINCT iPrefer_Number FROM #MRT_LN
		EXCEPT
	SELECT loyaltyNumber FROM [dbo].LoyaltyNumber

	UPDATE t
		SET LoyaltyNumberID = ln.LoyaltyNumberID,
			LoyaltyNumber_IsImputedFromEmail = 1
	FROM [dbo].Transactions t
		INNER JOIN #MRT_LN x ON x.TransactionID = t.TransactionID
		INNER JOIN [dbo].LoyaltyNumber ln ON ln.loyaltyNumber = x.iPrefer_Number
	------------------------------------------------------------------------


	------------------------------------------------------------------------
	--add loyalty number to the reservation if it came from the I Prefer website and we can match the email address
	------------------------------------------------------------------------
	DELETE #MRT_LN

		-- POPULATE #MRT --------------------------------------------------------------	
		IF OBJECT_ID('tempdb..#MRT') IS NOT NULL
			DROP TABLE #MRT;

		CREATE TABLE #MRT
		(
			TransactionID int NOT NULL,
			arrivalDate date,
			customerEmail nvarchar(255)
		)

		INSERT INTO #MRT(TransactionID,arrivalDate,customerEmail)
		SELECT mrt.TransactionID,mrt.arrivalDate,mrt.customerEmail
		FROM dbo.MostRecentTransactionsReporting mrt
			LEFT JOIN Core.dbo.iPreferCheckboxHotels ip ON mrt.hotelCode = ip.hotelCode --this tells us if the hotel allows I Prefer opt-ins on their website
		WHERE mrt.transactionTimeStamp >= @ThirtyDaysAgo --reservation was touched in the last 30 days
			AND	NULLIF(mrt.loyaltynumber,N'') IS NULL --there was no loyalty number in the booking
			AND	mrt.customerEmail IS NOT NULL --there was an email address in the booking
		
			AND
			(
				(
					--opted in Brand site bookings
					mrt.optIn = 1
					AND
					mrt.ChannelReportLabel = 'IBE - PHG'
					AND
					mrt.subSourceReportLabel NOT IN('HHA Call Center','HHA Call Center - iPrefer Partners on HHA.org',
												'www.historichotels.org','www.historichotels.org-cro','www.historichotels.org-flex','www.historichotels.org-gcomi','www.historichotels.org-partner','www.historichotelsworldwide.com','www.historichotelsworldwide.com-flex',
												'Active International','LuxLink','Sky Auction',
												'www.phgoffers.com-choice','www.phgoffers.com-lion','www.phgoffers.com-svc',
												'www.preferredhotelgroup.com-luxlink','www.preferredhotelgroup.com-perx','www.preferredhotelgroup.com-skyauction','www.preferredhotelgroup.com-amex')
				)
				OR
				(
					--opted in Hotel site bookings
					mrt.ChannelReportLabel = 'IBE - Hotel' 
					AND 
					mrt.optIn = 1
					AND
					ip.hotelCode IS NOT NULL
				)
				OR
				(
					--I Prefer member rate from any channel
					mrt.rateTypeCode IN (SELECT RateType FROM authority.IPreferRateType WHERE AutoEnroll=1)
				)
				OR
				(
					--I Prefer member rate from any channel this rate code was changed as of 1/1/21 and has previous reservations we don't want tagged
					mrt.ratetypecode = 'MKTMPE' and mrt.confirmationDate > '2020-12-31'
				)
				OR
				(
					--I Prefer member rate from any channel this rate code was changed as of 2/11/21 and has previous reservations we don't want tagged
					mrt.ratetypecode = 'MKTIPVIP' and mrt.confirmationDate > '2021-02-10'
				)
			)
		-------------------------------------------------------------------------------

		-- POPULATE #CPI --------------------------------------------------------------
		IF OBJECT_ID('tempdb..#CPI') IS NOT NULL
			DROP TABLE #CPI;
		CREATE TABLE #CPI
		(
			iPrefer_Number nvarchar(50) NOT NULL,
			Email nvarchar(100) NOT NULL,
			Membership_Date date NOT NULL,
			Disabled_Date date NOT NULL,

			PRIMARY KEY CLUSTERED(Email,Membership_Date,Disabled_Date)
		)

		INSERT INTO #CPI(iPrefer_Number,Email,Membership_Date,Disabled_Date)
		SELECT MAX(iPrefer_Number),Email,DATEADD(DAY,-7,Membership_Date),ISNULL(Disabled_Date,'9999-09-09')
		FROM Loyalty.dbo.Customer_Profile_Import
		WHERE NULLIF(Email,'') IS NOT NULL
			AND [MemberStatus] = 1
		GROUP BY Email,DATEADD(DAY,-7,Membership_Date),ISNULL(Disabled_Date,'9999-09-09')
		-------------------------------------------------------------------------------

		-- POPULATE #MRT_LN -----------------------------------------------------------
		INSERT INTO #MRT_LN(TransactionID,iPrefer_Number)
		SELECT mrt.TransactionID,c.iPrefer_Number
		FROM #MRT mrt
			INNER JOIN #CPI c ON c.Email = mrt.customerEmail --email address between profile and reservation matches
									AND mrt.arrivalDate >= c.Membership_Date --the member was created within 7 days of arrival
									AND mrt.arrivalDate < c.Disabled_Date --profile either is not disabled, or was not disabled at time of arrival
		-------------------------------------------------------------------------------

		-- Loyalty Number -------------------------------------------------------------
		INSERT INTO [dbo].LoyaltyNumber(loyaltyNumber)
		SELECT DISTINCT iPrefer_Number FROM #MRT_LN
			EXCEPT
		SELECT loyaltyNumber FROM [dbo].LoyaltyNumber

		UPDATE t
			SET LoyaltyNumberID = ln.LoyaltyNumberID,
				LoyaltyNumber_IsImputedFromEmail = 1
		FROM [dbo].Transactions t
			INNER JOIN #MRT_LN x ON x.TransactionID = t.TransactionID
			INNER JOIN [dbo].LoyaltyNumber ln ON ln.loyaltyNumber = x.iPrefer_Number
		-------------------------------------------------------------------------------
	------------------------------------------------------------------------


	------------------------------------------------------------------------
	--Set old I Prefer number to new I Prefer number if the guest is still using their old one
	------------------------------------------------------------------------
	DELETE #MRT_LN;

	INSERT INTO #MRT_LN(TransactionID,iPrefer_Number)
	SELECT t.TransactionID,o2n.[Membership Number]
	FROM [dbo].Transactions t
		INNER JOIN [dbo].TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		INNER JOIN [dbo].LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		INNER JOIN [loyalty].[dbo].[IPreferMapping_OldToNew] o2n ON o2n.[Old Membership Number] = ln.loyaltyNumber
	WHERE t.transactionTimeStamp >= @ThirtyDaysAgo --reservation arrives on or after the first day of the current month (don't want to adjust already billed reservations)

	INSERT INTO [dbo].LoyaltyNumber(loyaltyNumber)
	SELECT DISTINCT iPrefer_Number FROM #MRT_LN
		EXCEPT
	SELECT loyaltyNumber FROM [dbo].LoyaltyNumber

	UPDATE t
		SET LoyaltyNumberID = ln.LoyaltyNumberID
	FROM [dbo].Transactions t
		INNER JOIN #MRT_LN x ON x.TransactionID = t.TransactionID
		INNER JOIN [dbo].LoyaltyNumber ln ON ln.loyaltyNumber = x.iPrefer_Number
	WHERE t.LoyaltyNumberID != ln.LoyaltyNumberID
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	--Set loyaltyNumberValidated to true if the I Prefer customer profile record was active at the time the reservation arrived with their I Prefer number
	------------------------------------------------------------------------
	UPDATE td
		SET LoyaltyNumberValidated = CASE WHEN c.iPrefer_Number IS NULL THEN 0 ELSE 1 END
	FROM [dbo].Transactions t
		INNER JOIN [dbo].TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN [dbo].LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN [loyalty].[dbo].[Customer_Profile_Import] c ON c.iPrefer_Number = NULLIF(ln.loyaltyNumber,'')
															AND td.arrivalDate >= DATEADD(DAY,-7,c.Membership_Date) --reservation arrives on or after the profile start date
															AND td.arrivalDate < ISNULL(c.Disabled_Date,'9999-09-09') --profile either is not disabled, or was not disabled at time of arrival
	WHERE t.transactionTimeStamp >= @ThirtyDaysAgo --reservation arrives on or after the first day of the current month (don't want to adjust already billed reservations)
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- SET LoyaltyNumber_IsNewMember
	------------------------------------------------------------------------
	UPDATE t
		SET LoyaltyNumber_IsNewMember = CASE
											WHEN NULLIF(ln.[LoyaltyNumberID],@LN_NULL) IS NULL THEN NULL
											ELSE
												CASE WHEN DATEDIFF(DAY,ts.confirmationDate,mp.Enrollment_Date) BETWEEN 0 AND 3 THEN 1 ELSE 0 END
										END
	FROM Reservations.dbo.Transactions t
		INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		INNER JOIN Loyalty.[dbo].[LoyaltyNumber] lln ON lln.[LoyaltyNumberName] = ln.loyaltyNumber
		INNER JOIN Loyalty.dbo.MemberProfile mp ON mp.LoyaltyNumberID = lln.LoyaltyNumberID
	WHERE t.LoyaltyNumber_IsNewMember IS NULL
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- SET t.LoyaltyNumber_IsImputedFromEmail = 0
	------------------------------------------------------------------------
	UPDATE t
		SET t.LoyaltyNumber_IsImputedFromEmail = 0
	FROM Reservations.dbo.Transactions t
	WHERE t.LoyaltyNumber_IsImputedFromEmail IS NULL
		AND t.LoyaltyNumberID IS NOT NULL
	------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UPDATE_CRS_BookingSourceID]'
GO



ALTER PROCEDURE [dbo].[UPDATE_CRS_BookingSourceID]
	@QueueID int = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_BS(confirmationNumber,BookingSourceID,rowNum)
	AS
	(
		SELECT t.confirmationNumber,tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM synxis.Transactions t
			INNER JOIN synxis.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[synXis_CRS_BookingSource] tbs ON tbs.synXisID = bs.BookingSourceID
			INNER JOIN synxis.Channel ch ON ch.ChannelID = bs.ChannelID
		WHERE NULLIF(ch.channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM synxis.Transactions WHERE QueueID = @QueueID)

		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM openHosp.Transactions t
			INNER JOIN openHosp.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[openHosp_CRS_BookingSource] tbs ON tbs.openHospID = bs.BookingSourceID
			INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM openHosp.Transactions WHERE QueueID = @QueueID)
		
		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,         
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM pegasus.Transactions t
			INNER JOIN pegasus.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[pegasus_CRS_BookingSource] tbs ON tbs.pegasusID = bs.BookingSourceID
			INNER JOIN authority.Pegasus_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM pegasus.Transactions WHERE QueueID = @QueueID)

	)
	UPDATE t
		SET CRS_BookingSourceID = bs.BookingSourceID
	FROM dbo.Transactions t
		INNER JOIN cte_BS bs ON bs.confirmationNumber = t.confirmationNumber
	WHERE bs.rowNum = 1
		AND t.CRS_BookingSourceID != bs.BookingSourceID
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_VoiceAgent]'
GO



ALTER PROCEDURE [dbo].[Populate_VoiceAgent]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[VoiceAgent] AS tgt
	USING
	(
		SELECT DISTINCT [UserNameID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[userName]
		FROM synxis.UserName
		WHERE [UserNameID] IN(SELECT [UserNameID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[userName] = tgt.[VoiceAgent]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[VoiceAgent])
		VALUES([synXisID],[openHospID],[pegasusID],[userName])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TravelAgent]'
GO



ALTER PROCEDURE [dbo].[Populate_TravelAgent]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[TravelAgent] tgt
	USING
	(
		SELECT DISTINCT STRING_AGG(CONVERT(varchar(MAX),t.TravelAgentID),',') AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],i.[IATANumberID],t.[Name],MAX(t.[Address1]) AS [Address1],
						MAX(t.[Address2]) AS [Address2],l.[LocationID],MAX(t.[Phone]) AS [Phone],MAX(t.[Fax]) AS [Fax],t.[Email]
		FROM synxis.TravelAgent t
			INNER JOIN (SELECT [LocationID],value AS synXisID FROM dbo.[Location] CROSS APPLY string_split(synXisID,',')) l ON l.synXisID = t.LocationID
			INNER JOIN (SELECT [IATANumberID],value AS synXisID FROM dbo.IATANumber CROSS APPLY string_split(synXisID,',')) i ON i.synXisID = t.IATANumberID
		WHERE t.[TravelAgentID] IN(SELECT [TravelAgentID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		GROUP BY i.[IATANumberID],t.[Name],l.[LocationID],t.[Email]
	) AS src ON ISNULL(src.[IATANumberID],'') = ISNULL(tgt.[IATANumberID],'') AND ISNULL(src.[Name],'') = ISNULL(tgt.[Name],'') AND ISNULL(src.[LocationID],'') = ISNULL(tgt.[LocationID],'') AND ISNULL(src.[Email],'') = ISNULL(tgt.[Email],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID],
				[Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[Phone] = src.[Phone],
				[Fax] = src.[Fax]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[IATANumberID],[Name],[Address1],[Address2],[LocationID],[Phone],[Fax],[Email])
		VALUES([synXisID],[openHospID],[pegasusID],[IATANumberID],[Name],[Address1],[Address2],[LocationID],[Phone],[Fax],[Email])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionStatus]'
GO



ALTER PROCEDURE [dbo].[Populate_TransactionStatus]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int,@Pegasus_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'
	SELECT @Pegasus_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus'

	MERGE INTO [dbo].[TransactionStatus] AS tgt
	USING
	(
		SELECT [synXisID],[openHospID],[pegasusID],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID],
				[DataSourceID]
		FROM
		(
			SELECT ts.[TransactionStatusID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],t.[confirmationNumber],[status],[confirmationDate],[cancellationNumber],
					CASE
						WHEN [status] = 'Cancelled' AND [cancellationNumber] IS NOT NULL AND [cancellationDate] IS NULL THEN CONVERT(date,t.[transactionTimeStamp])
						ELSE [cancellationDate]
					END AS [cancellationDate],
					[creditCardType],x.[ActionTypeID],
					@SynXis_DS AS [DataSourceID]
			FROM synxis.TransactionStatus ts
				INNER JOIN synxis.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS synXisID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(synXisID,',')) x ON x.synXisID = ts.ActionTypeID
			WHERE t.QueueID = @QueueID
			
			UNION ALL
			
			SELECT NULL AS [synXisID],ts.[TransactionStatusID] AS [openHospID],NULL AS [pegasusID],
				CASE WHEN dups.[confirmationNumber] IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID],@OpenHosp_DS AS [DataSourceID]
			FROM openHosp.TransactionStatus ts
				INNER JOIN openHosp.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS openHospID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(openHospID,',')) x ON x.openHospID = ts.ActionTypeID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = ts.confirmationNumber
			WHERE t.QueueID = @QueueID
			
			UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [openHospID],ts.[TransactionStatusID] AS [pegasusID],
				CASE WHEN dups.[confirmationNumber] IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber], -----unsure if '_OH' should be replaced here
				[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID],@Pegasus_DS AS [DataSourceID]
			FROM pegasus.TransactionStatus ts
				INNER JOIN pegasus.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN pegasus.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS pegasusID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(pegasusID,',')) x ON x.pegasusID = ts.ActionTypeID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = ts.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[status] = src.[status],
				[confirmationDate] = src.[confirmationDate],
				[cancellationNumber] = src.[cancellationNumber],
				[cancellationDate] = src.[cancellationDate],
				[creditCardType] = src.[creditCardType],
				[ActionTypeID] = src.[ActionTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID])
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Transactions]'
GO



ALTER PROCEDURE [dbo].[Populate_Transactions]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int, @Pegasus_DS int,@Blank_LN int,@Blank_IATA int

	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'
	SELECT @Pegasus_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus'
	SELECT @Blank_LN = LoyaltyNumberID FROM dbo.LoyaltyNumber WHERE loyaltyNumber = ''
	SELECT @Blank_IATA = IATANumberID FROM synxis.IATANumber WHERE NULLIF(IATANumber,'') IS NULL

	MERGE INTO [dbo].[Transactions] AS tgt
	USING
	(
		SELECT [synXisID],[openHospID],[pegasusID],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],
				[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],
				[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID,
				[DataSourceID],IsMKTIPM,LoyaltyNumber_IsNewMember,LoyaltyNumber_IsImputedFromEmail
		FROM
		(
			SELECT t.[TransactionID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],t.[QueueID],t.[itineraryNumber],t.[confirmationNumber],
					t.[transactionTimeStamp],t.[channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					cc.[CorporateCodeID],rt.[RoomTypeID],rc.[RateCategoryID],u.UserNameID,
					bs.[BookingSourceID],ln.[LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],ta.[TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,gc.[Guest_CompanyNameID],lp.LoyaltyProgramID,@SynXis_DS AS [DataSourceID],
					CASE WHEN MKTIPM.RateCodeID IS NULL THEN 0 ELSE 1 END AS IsMKTIPM,
					CASE
						WHEN NULLIF(ln.[LoyaltyNumberID],624035) IS NULL THEN NULL
						ELSE
							CASE WHEN DATEDIFF(DAY,ts.confirmationDate,mp.Enrollment_Date) BETWEEN 0 AND 3 THEN 1 ELSE 0 END
					END AS LoyaltyNumber_IsNewMember,
					CASE WHEN ln.[LoyaltyNumberID] IS NULL THEN NULL ELSE 0 END AS LoyaltyNumber_IsImputedFromEmail
			FROM synxis.Transactions t
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.DataSourceID = @SynXis_DS AND ts.sourceKey = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.DataSourceID = @SynXis_DS AND td.sourceKey = t.TransactionDetailID AND td.[synXisTransExID] = t.TransactionsExtendedID
				LEFT JOIN map.[synXis_CorporateCode] cc ON cc.synXisID = t.CorporateCodeID
				LEFT JOIN map.[synXis_RoomType] rt	ON rt.synXisID = t.RoomTypeID
				LEFT JOIN map.[synXis_RateCategory] rc	ON rc.synXisID = t.RateCategoryID
				LEFT JOIN map.[synXis_VoiceAgent] u ON u.synXisID = t.UserNameID
				LEFT JOIN map.[synXis_CRS_BookingSource] bs ON bs.synXisID = t.BookingSourceID
				LEFT JOIN map.[synXis_LoyaltyNumber] ln ON ln.synXisID = t.LoyaltyNumberID
					LEFT JOIN synxis.LoyaltyNumber sln ON sln.LoyaltyNumberID = ln.LoyaltyNumberID
					LEFT JOIN Loyalty.[dbo].[LoyaltyNumber] lln ON lln.[LoyaltyNumberName] = sln.loyaltyNumber
					LEFT JOIN Loyalty.dbo.MemberProfile mp ON mp.LoyaltyNumberID = lln.LoyaltyNumberID
				LEFT JOIN map.[synXis_LoyaltyProgram] lp ON lp.synXisID = t.LoyaltyProgramID
				LEFT JOIN map.[synXis_Guest] g	ON g.synXisID = t.GuestID
				LEFT JOIN map.[synXis_RateCode] rtc ON rtc.synXisID = t.RateTypeCodeID
				LEFT JOIN map.[synXis_PromoCode] pc ON pc.synXisID = t.PromoCodeID
				LEFT JOIN map.[synXis_TravelAgent] ta ON ta.synXisID = t.TravelAgentID
				LEFT JOIN map.[synXis_IATANumber] i ON i.synXisID = t.IATANumberID
				LEFT JOIN map.[synXis_hotel] h	ON h.synXisID = t.intHotelID
				LEFT JOIN map.[synXis_Chain] ch ON ch.synXisID = t.intChainID
				LEFT JOIN map.[synXis_Guest_CompanyName] gc ON gc.synXisID = t.Guest_CompanyNameID
				LEFT JOIN (SELECT RateCodeID FROM dbo.RateCode WHERE RateCode = 'MKTIPM') MKTIPM ON MKTIPM.RateCodeID = rtc.RateTypeCodeID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID
				AND ISNULL(t.confirmationNumber,'') != ''

			UNION ALL

			SELECT NULL AS [synXisID],t.[TransactionID] AS [openHospID],NULL AS [pegasusID],t.[QueueID],NULL AS [itineraryNumber],
					CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
					t.[transactionTimeStamp],NULL AS [channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					NULL AS [CorporateCodeID],NULL AS [RoomTypeID],NULL AS [RateCategoryID],NULL AS [VoiceAgentID],
					bs.[BookingSourceID],NULL AS [LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],NULL AS [TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,NULL AS [Guest_CompanyNameID],NULL AS LoyaltyProgramID,@OpenHosp_DS AS [DataSourceID],
					CASE WHEN MKTIPM.RateCodeID IS NULL THEN 0 ELSE 1 END AS IsMKTIPM,
					NULL AS LoyaltyNumber_IsNewMember,NULL AS LoyaltyNumber_IsImputedFromEmail
			FROM openHosp.Transactions t
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.DataSourceID = @OpenHosp_DS AND ts.sourceKey = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.DataSourceID = @OpenHosp_DS AND td.sourceKey = t.TransactionDetailID
				LEFT JOIN map.[openHosp_CRS_BookingSource] bs ON bs.openHospID = t.BookingSourceID
				LEFT JOIN map.[openHosp_Guest] g ON g.openHospID = t.GuestID
				LEFT JOIN map.[openHosp_RateCode] rtc ON rtc.openHospID = t.RateTypeCodeID
				LEFT JOIN map.[openHosp_PromoCode] pc ON pc.openHospID = t.PromoCodeID
				LEFT JOIN map.[openHosp_IATANumber] i ON i.openHospID = t.IATANumberID
				LEFT JOIN map.[openHosp_hotel] h ON h.openHospID = t.intHotelID
				LEFT JOIN map.[openHosp_Chain] ch ON ch.openHospID = t.intChainID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
				LEFT JOIN (SELECT RateCodeID FROM dbo.RateCode WHERE RateCode = 'MKTIPM') MKTIPM ON MKTIPM.RateCodeID = rtc.RateTypeCodeID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID

			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],t.[TransactionID] AS [pegasusID],t.[QueueID],NULL AS [itineraryNumber],
					CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],      -----'_OH' might need to be changed
					t.[transactionTimeStamp],NULL AS [channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					NULL AS [CorporateCodeID],NULL AS [RoomTypeID],NULL AS [RateCategoryID],NULL AS [VoiceAgentID],
					bs.[BookingSourceID],NULL AS [LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],NULL AS [TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,NULL AS [Guest_CompanyNameID],NULL AS LoyaltyProgramID,@Pegasus_DS AS [DataSourceID],
					CASE WHEN MKTIPM.RateCodeID IS NULL THEN 0 ELSE 1 END AS IsMKTIPM,
					NULL AS LoyaltyNumber_IsNewMember,NULL AS LoyaltyNumber_IsImputedFromEmail
			FROM pegasus.Transactions t
				INNER JOIN pegasus.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.DataSourceID = @Pegasus_DS AND ts.sourceKey = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.DataSourceID = @Pegasus_DS AND td.sourceKey = t.TransactionDetailID
				LEFT JOIN map.[pegasus_CRS_BookingSource] bs ON bs.pegasusID = t.BookingSourceID
				LEFT JOIN map.[pegasus_Guest] g ON g.pegasusID = t.GuestID
				LEFT JOIN map.[pegasus_RateCode] rtc ON rtc.pegasusID = t.RateTypeCodeID
				LEFT JOIN map.[pegasus_PromoCode] pc ON pc.pegasusID = t.PromoCodeID
				LEFT JOIN map.[pegasus_IATANumber] i ON i.pegasusID = t.IATANumberID
				LEFT JOIN map.[pegasus_hotel] h ON h.pegasusID = t.intHotelID
				LEFT JOIN map.[pegasus_Chain] ch ON ch.pegasusID = t.intChainID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
				LEFT JOIN (SELECT RateCodeID FROM dbo.RateCode WHERE RateCode = 'MKTIPM') MKTIPM ON MKTIPM.RateCodeID = rtc.RateTypeCodeID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[QueueID] = src.[QueueID],
				[itineraryNumber] = ISNULL(src.[itineraryNumber],tgt.[itineraryNumber]),
				[transactionTimeStamp] = src.[transactionTimeStamp],
				[channelConnectConfirmationNumber] = src.[channelConnectConfirmationNumber],
				[timeLoaded] = src.[timeLoaded],
				[TransactionStatusID] = src.[TransactionStatusID],
				[TransactionDetailID] = src.[TransactionDetailID],
				[CorporateCodeID] = ISNULL(src.[CorporateCodeID],tgt.[CorporateCodeID]),
				[RoomTypeID] = ISNULL(src.[RoomTypeID],tgt.[RoomTypeID]),
				[RateCategoryID] = ISNULL(src.[RateCategoryID],tgt.[RateCategoryID]),
				[VoiceAgentID] = ISNULL(src.UserNameID,tgt.[VoiceAgentID]),
				[CRS_BookingSourceID] = ISNULL(src.[BookingSourceID],tgt.[CRS_BookingSourceID]),
				[LoyaltyNumberID] = ISNULL(NULLIF(src.[LoyaltyNumberID],@Blank_LN),tgt.[LoyaltyNumberID]), --624035 is blank
				[GuestID] = src.[GuestID],
				[RateCodeID] = CASE WHEN IsMKTIPM = 0 THEN src.RateTypeCodeID ELSE tgt.[RateCodeID] END,
				[PromoCodeID] = ISNULL(src.[PromoCodeID],tgt.[PromoCodeID]),
				[TravelAgentID] = ISNULL(src.[TravelAgentID],tgt.[TravelAgentID]),
				[IATANumberID] = ISNULL(NULLIF(src.[IATANumberID],@Blank_IATA),tgt.[IATANumberID]),
				[HotelID] = ISNULL(src.intHotelID,tgt.[HotelID]),
				[ChainID] = ISNULL(src.intChainID,tgt.[ChainID]),
				[Guest_CompanyNameID] = src.[Guest_CompanyNameID],
				LoyaltyProgramID = ISNULL(src.LoyaltyProgramID,tgt.LoyaltyProgramID),
				LoyaltyNumber_IsNewMember = src.LoyaltyNumber_IsNewMember,
				LoyaltyNumber_IsImputedFromEmail = src.LoyaltyNumber_IsImputedFromEmail
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],[VoiceAgentID],[CRS_BookingSourceID],[LoyaltyNumberID],[GuestID],[RateCodeID],[PromoCodeID],[TravelAgentID],[IATANumberID],[HotelID],[ChainID],[Guest_CompanyNameID],LoyaltyProgramID,LoyaltyNumber_IsNewMember,LoyaltyNumber_IsImputedFromEmail)
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID,LoyaltyNumber_IsNewMember,LoyaltyNumber_IsImputedFromEmail)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionDetail]'
GO



ALTER PROCEDURE [dbo].[Populate_TransactionDetail]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int, @Pegasus_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'
	SELECT @Pegasus_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus'

	MERGE INTO [dbo].[TransactionDetail] AS tgt
	USING
	(
		SELECT [synXisID],[synXisTransExID],[openHospID],[pegasusID],[confirmationNumber],CASE WHEN [nights] = 0 THEN 1 ELSE [nights] END AS [nights],
			[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
			[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
			[departureDOW],[optIn],[IsPrimaryGuest],[DataSourceID],[billingDescription]
		FROM
		(
			SELECT td.[TransactionDetailID] AS [synXisID],x.TransactionsExtendedID AS [synXisTransExID],NULL AS [openHospID],NULL AS [pegasusID],t.[confirmationNumber],[nights],
					[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
					[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
					[departureDOW],x.[optIn],[IsPrimaryGuest],@SynXis_DS AS [DataSourceID],bd.[billingDescription]
			FROM synxis.TransactionDetail td
				INNER JOIN synxis.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN
					(
						SELECT DISTINCT t.TransactionID,te.TransactionsExtendedID,
									CASE te.optIn WHEN 'Y' THEN 1 ELSE 0 END AS optIn,
									te.IsPrimaryGuest
						FROM synxis.TransactionsExtended te
							INNER JOIN synxis.Transactions t ON t.TransactionsExtendedID = te.TransactionsExtendedID
					) x ON x.TransactionID = mrt.TransactionID
				LEFT JOIN synxis.BillingDescription bd ON bd.BillingDescriptionID = t.BillingDescriptionID
			WHERE t.QueueID = @QueueID

				UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [synXisTransExID],td.[TransactionDetailID] AS [openHospID], NULL AS [pegasusID],
				CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[nights],
				NULL AS [averageDailyRate],[rooms],[reservationRevenue],[currency],NULL AS [totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],NULL AS [commisionPercent],[arrivalDate],[departureDate],NULL AS [bookingLeadTime],NULL AS [arrivalDOW],
				NULL AS [departureDOW],NULL AS [optIn],NULL AS [IsPrimaryGuest],@OpenHosp_DS AS [DataSourceID],NULL AS [billingDescription]
			FROM openHosp.TransactionDetail td
				INNER JOIN openHosp.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE t.QueueID = @QueueID

				UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [synXisTransExID], NULL AS [openHospID], td.[TransactionDetailID] AS [pegasusID],
				CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber], ----should '_OH' remain the same
				[nights],
				NULL AS [averageDailyRate],[rooms],[reservationRevenue],[currency],NULL AS [totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],NULL AS [commisionPercent],[arrivalDate],[departureDate],NULL AS [bookingLeadTime],NULL AS [arrivalDOW],
				NULL AS [departureDOW],NULL AS [optIn],NULL AS [IsPrimaryGuest],@Pegasus_DS AS [DataSourceID],NULL AS [billingDescription]
			FROM pegasus.TransactionDetail td
				INNER JOIN pegasus.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN pegasus.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[synXisTransExID] = src.[synXisTransExID],
				[nights] = src.[nights],
				[averageDailyRate] = src.[averageDailyRate],
				[rooms] = src.[rooms],
				[reservationRevenue] = src.[reservationRevenue],
				[currency] = src.[currency],
				[totalPackageRevenue] = src.[totalPackageRevenue],
				[totalGuestCount] = src.[totalGuestCount],
				[adultCount] = src.[adultCount],
				[childrenCount] = src.[childrenCount],
				[commisionPercent] = src.[commisionPercent],
				[arrivalDate] = src.[arrivalDate],
				[departureDate] = src.[departureDate],
				[bookingLeadTime] = src.[bookingLeadTime],
				[arrivalDOW] = src.[arrivalDOW],
				[departureDOW] = src.[departureDOW],
				[optIn] = src.[optIn],
				[IsPrimaryGuest] = src.[IsPrimaryGuest],
				[billingDescription] = src.[billingDescription]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],
				[synXisTransExID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[IsPrimaryGuest],[billingDescription])
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[synXisTransExID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[IsPrimaryGuest],[billingDescription])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_State]'
GO


ALTER PROCEDURE [dbo].[Populate_State]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[State] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[State_Text]
		FROM
		(
			SELECT [StateID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([State_Text]),''),'unknown') AS [State_Text]
			FROM synxis.[State]
				UNION ALL
			SELECT NULL AS [synXisID],[StateID] AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([State_Text]),''),'unknown') AS [State_Text]
			FROM openHosp.[State]
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[StateID] AS [pegasusID],ISNULL(NULLIF(TRIM([State_Text]),''),'unknown') AS [State_Text]
			FROM pegasus.[State]
		) x
		GROUP BY [State_Text]
	) AS src ON src.[State_Text] = tgt.[State_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[State_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[State_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RoomType]'
GO



ALTER PROCEDURE [dbo].[Populate_RoomType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RoomType] AS tgt
	USING
	(
		SELECT STRING_AGG([RoomTypeID],',') AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[roomTypeName],[roomTypeCode]
		FROM synxis.RoomType
		WHERE [RoomTypeID] IN(SELECT [RoomTypeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		GROUP BY [roomTypeName],[roomTypeCode]
	) AS src ON src.[roomTypeName] = tgt.[roomTypeName] AND src.[roomTypeCode] = tgt.[roomTypeCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[roomTypeName],[roomTypeCode])
		VALUES([synXisID],[openHospID],[pegasusID],[roomTypeName],[roomTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Region]'
GO



ALTER PROCEDURE [dbo].[Populate_Region]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Region] AS tgt
	USING
	(
		SELECT DISTINCT [RegionID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF([Region_Text],''),'unknown') AS [Region_Text]
		FROM synxis.Region
	) AS src ON src.[Region_Text] = tgt.[Region_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[Region_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[Region_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RateCode]'
GO



ALTER PROCEDURE [dbo].[Populate_RateCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RateCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[RateTypeName],[RateTypeCode]
		FROM
		(
			SELECT [RateTypeCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[RateTypeName],[RateTypeCode]
			FROM synxis.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[RateTypeCodeID] AS [openHospID],NULL AS [pegasusID],[RateTypeName],[RateTypeCode]
			FROM openHosp.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[RateTypeCodeID] AS [pegasusID],[RateTypeName],[RateTypeCode]
			FROM pegasus.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [RateTypeName],[RateTypeCode]
	) AS src ON src.[RateTypeName] = tgt.[RateName] AND src.[RateTypeCode] = tgt.[RateCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[RateName],[RateCode])
		VALUES([synXisID],[openHospID],[pegasusID],[RateTypeName],[RateTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RateCategory]'
GO



ALTER PROCEDURE [dbo].[Populate_RateCategory]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RateCategory] AS tgt
	USING
	(
		SELECT DISTINCT [RateCategoryID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[rateCategoryName],ISNULL(NULLIF([rateCategoryCode],''),'Unassigned') as [rateCategoryCode]
		FROM synxis.RateCategory
		WHERE [RateCategoryID] IN(SELECT [RateCategoryID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[rateCategoryName] = tgt.[rateCategoryName] AND src.[rateCategoryCode] = tgt.[rateCategoryCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[rateCategoryName],[rateCategoryCode])
		VALUES([synXisID],[openHospID],[pegasusID],[rateCategoryName],[rateCategoryCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PromoCode]'
GO



ALTER PROCEDURE [dbo].[Populate_PromoCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PromoCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[promotionalCode]
		FROM
		(
			SELECT [PromoCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[promotionalCode]
			FROM synxis.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[PromoCodeID] AS [openHospID],NULL AS [pegasusID],[promotionalCode]
			FROM openHosp.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[PromoCodeID] AS [pegasusID],[promotionalCode]
			FROM pegasus.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [promotionalCode]
	) AS src ON src.[promotionalCode] = tgt.[promotionalCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[promotionalCode])
		VALUES([synXisID],[openHospID],[pegasusID],[promotionalCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PostalCode]'
GO




ALTER PROCEDURE [dbo].[Populate_PostalCode]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
	
	-- CREATE & POPULATE #POST ----------------------------
	IF OBJECT_ID('tempdb..#POST') IS NOT NULL
		DROP TABLE #POST;

	CREATE TABLE #POST
	(
		[synXisID] varchar(MAX),
		[openHospID] varchar(MAX),
		[pegasusID] varchar(MAX),
		[PostalCode_Text] nvarchar(255),

		PRIMARY KEY CLUSTERED([PostalCode_Text])
	)

	INSERT INTO #POST(synXisID,openHospID,pegasusID,PostalCode_Text)
	SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[PostalCode_Text]
	FROM
	(
		SELECT [PostalCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([PostalCode_Text]),''),'unknown') AS [PostalCode_Text]
		FROM synxis.PostalCode
			UNION ALL
		SELECT NULL AS [synXisID],[PostalCodeID] AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([PostalCode_Text]),''),'unknown') AS [PostalCode_Text]
		FROM openHosp.PostalCode
			UNION ALL
		SELECT NULL AS [synXisID],NULL AS [openHospID],[PostalCodeID] AS [pegasusID],ISNULL(NULLIF(TRIM([PostalCode_Text]),''),'unknown') AS [PostalCode_Text]
		FROM pegasus.PostalCode
	) x
	GROUP BY [PostalCode_Text]
	-------------------------------------------------------

	MERGE INTO [dbo].[PostalCode] AS tgt
	USING
	(
		SELECT [synXisID],[openHospID],[pegasusID],[PostalCode_Text] FROM #POST
	) AS src ON src.[PostalCode_Text] = tgt.[PostalCode_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[PostalCode_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[PostalCode_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PMSRateTypeCode]'
GO



ALTER PROCEDURE [dbo].[Populate_PMSRateTypeCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PMSRateTypeCode] AS tgt
	USING
	(
		SELECT DISTINCT [PMSRateTypeCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[PMSRateTypeCode]
		FROM synxis.PMSRateTypeCode
		WHERE [PMSRateTypeCodeID] IN(SELECT [PMSRateTypeCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[PMSRateTypeCode] = tgt.[PMSRateTypeCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[PMSRateTypeCode])
		VALUES([synXisID],[openHospID],[pegasusID],[PMSRateTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [map].[Populate_mapTables]'
GO


ALTER PROCEDURE [map].[Populate_mapTables]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	EXEC [map].[Populate_synxis_mapTables] @QueueID

	EXEC [map].[Populate_openhosp_mapTables] @QueueID

	EXEC [map].[Populate_pegasus_mapTables] @QueueID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [authority].[Populate_ibeSource]'
GO

ALTER PROCEDURE [authority].[Populate_ibeSource]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @defaultIBE int
	SELECT @defaultIBE = ibeSource_GroupID FROM authority.ibeSource_Group WHERE ibeSourceName = 'Hotel Website'

	INSERT INTO [authority].[ibeSource]([ibeSource_GroupID],[ibeSourceName],[siteAbbreviation],IsStatic)
	SELECT DISTINCT @defaultIBE,[xbeTemplateName],'HOTEL',CONVERT(bit,0)
	FROM
		(
			SELECT [xbeTemplateName],'HOTEL' AS [xbeShellName]
			FROM synxis.xbeTemplate
			WHERE NULLIF([xbeTemplateName],'') IS NOT NULL
		) x
		EXCEPT
	SELECT @defaultIBE,[ibeSourceName],'HOTEL',CONVERT(bit,0) FROM [authority].[ibeSource]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [authority].[Populate_CRO_Code]'
GO

ALTER PROCEDURE [authority].[Populate_CRO_Code]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @defaultCRO int
	SELECT @defaultCRO = CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group = 'Hotel Voice Agent'

	INSERT INTO [authority].[CRO_Code]([CRO_Code_GroupID],[CRO_Code],IsStatic)
	SELECT @defaultCRO,[CROCode],CONVERT(bit,0) FROM synxis.CROCode
		EXCEPT
	SELECT @defaultCRO,[CRO_Code],CONVERT(bit,0) FROM [authority].[CRO_Code]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_City]'
GO



ALTER PROCEDURE [dbo].[Populate_City]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[City] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[City_Text]
		FROM
		(
			SELECT [CityID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([City_Text]),''),'unknown') AS [City_Text]
			FROM synxis.City
				UNION ALL
			SELECT NULL AS [synXisID],[CityID] AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([City_Text]),''),'unknown') AS [City_Text]
			FROM openHosp.City
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[CityID] AS [pegasusID],ISNULL(NULLIF(TRIM([City_Text]),''),'unknown') AS [City_Text]
			FROM pegasus.City
		) x
		GROUP BY [City_Text]
	) AS src ON src.[City_Text] = tgt.[City_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[City_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[City_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Chain]'
GO



ALTER PROCEDURE [dbo].[Populate_Chain]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Chain] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],NULL AS [pegasusID],[ChainName],[ChainID]
		FROM
			(
				SELECT [intChainID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[ChainName],[ChainID]
				FROM synxis.Chain
					UNION ALL
				SELECT NULL AS [synXisID],[intChainID] AS [openHospID],NULL AS [pegasusID],[ChainName],[ChainID]
				FROM openHosp.Chain
					UNION ALL
				SELECT NULL AS [synXisID],NULL AS [openHospID],[intChainID] AS [pegasusID],'Preferred Hotel Group' AS [ChainName],[ChainID]
				FROM pegasus.Chain
			) x
		GROUP BY [ChainName],[ChainID]
	) AS src ON src.[ChainID] = tgt.[CRS_ChainID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID],
				[ChainName] = src.[ChainName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[ChainName],[CRS_ChainID])
		VALUES(src.[synXisID],src.[openHospID],src.[pegasusID],src.[ChainName],src.[ChainID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_ActionType]'
GO


ALTER PROCEDURE [dbo].[Populate_ActionType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.ActionType AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID]
				,[actionType],[actionTypeOrder]
		FROM
			(
				SELECT [ActionTypeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[actionType],[actionTypeOrder]
				FROM synxis.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM synxis.Transactions t
											INNER JOIN synxis.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
					UNION ALL
				SELECT NULL AS [synXisID],[ActionTypeID] AS [openHospID],NULL AS [pegasusID],[actionType],[actionTypeOrder]
				FROM openHosp.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM openHosp.Transactions t
											INNER JOIN openHosp.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
					UNION ALL
				SELECT NULL AS [synXisID],NULL AS [openHospID],[ActionTypeID] AS [pegasusID],[actionType],[actionTypeOrder]
				FROM pegasus.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM pegasus.Transactions t
											INNER JOIN pegasus.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
			) x
		GROUP BY [actionType],[actionTypeOrder]
	) AS src ON src.[actionType] = tgt.[actionType]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID],
				[actionTypeOrder] = src.[actionTypeOrder]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[actionType],[actionTypeOrder])
		VALUES(src.[synXisID],src.[openHospID],src.[pegasusID],src.[actionType],src.[actionTypeOrder])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

--ALTER TABLE [dbo].[ActionType]
--		ADD pegasusID varchar(MAX) NULL,
--			IsPegasus AS (CASE WHEN pegasusID IS NULL THEN 0 ELSE 1 END) Persisted
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Delete_UnmappedGuests]'
GO


ALTER PROCEDURE [dbo].[Delete_UnmappedGuests]
AS
BEGIN
	IF OBJECT_ID('tempdb..#GUEST_ID') IS NOT NULL
		DROP TABLE #GUEST_ID;
	CREATE TABLE #GUEST_ID(GuestID int NOT NULL, PRIMARY KEY CLUSTERED(GuestID))

	INSERT INTO #GUEST_ID(GuestID)
	SELECT DISTINCT g.GuestID
	FROM dbo.Guest g
		LEFT JOIN dbo.Transactions t ON t.GuestID = g.GuestID
	WHERE t.GuestID IS NULL


	DELETE map.synXis_Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)

	DELETE map.openHosp_Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)

	DELETE map.pegasus_Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)

	DELETE dbo.Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Normalized_dbo]'
GO


ALTER PROCEDURE [dbo].[Populate_Normalized_dbo]
	@TruncateBeforeLoad bit = 0,
	@QueueID int = NULL,
	@StepID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000),
			@errorMessage nvarchar(MAX),
			@count int

	BEGIN TRY
		-- TRUNCATE DBO TABLES -------------------------------------------------
		IF @TruncateBeforeLoad = 1
		BEGIN
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': TRUNCATE DBO TABLES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			--EXEC dbo.TruncateTablesWithinSchema 'dbo'
		END
		-------------------------------------------------------------------

		-- dbo.ActionType -------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.ActionType'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_ActionType @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Channel] ------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Channel'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [dbo].[Populate_CRS_Channel] @QueueID
		-------------------------------------------------------------------

		-- [dbo].[CRS_SecondarySource] ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.CRS_SecondarySource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CRS_SecondarySource @QueueID
		-------------------------------------------------------------------

		-- [dbo].[SubSource] ----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.SubSource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CRS_SubSource @QueueID
		-------------------------------------------------------------------

		-- [authority].[CRO_Code] -----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': authority.CROCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC authority.Populate_CRO_Code
		-------------------------------------------------------------------

		-- [dbo].[CROCode] ------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.CROCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CROCode @QueueID
		-------------------------------------------------------------------

		-- [authority].[ibeSource] ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [authority].ibeSource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC authority.Populate_ibeSource
		-------------------------------------------------------------------

		-- [dbo].[ibeSource] --------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.[ibeSource]'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_ibeSource @QueueID
		-------------------------------------------------------------------

		-- [dbo].[BookingSource] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BookingSource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CRS_BookingSource @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Chain] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Chain'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Chain
		-------------------------------------------------------------------

		-- [dbo].[City] ---------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.City'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_City
		-------------------------------------------------------------------

		-- [dbo].[CorporateCode] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.CorporateCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CorporateCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Country] ------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Country'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Country
		-------------------------------------------------------------------

		-- [dbo].[Guest_EmailAddress] -------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Guest_EmailAddress'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Guest_EmailAddress @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Guest_CompanyName] --------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Guest_CompanyName'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Guest_CompanyName @QueueID
		-------------------------------------------------------------------

		-- [dbo].[hotel] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.hotel'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_hotel @QueueID
		-------------------------------------------------------------------

		-- [dbo].[IATANumber] ---------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.IATANumber'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_IATANumber @QueueID
		-------------------------------------------------------------------

		-- [dbo].[State] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.State'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_State
		-------------------------------------------------------------------

		-- [dbo].[PostalCode] ---------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.PostalCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_PostalCode
		-------------------------------------------------------------------

		-- [dbo].[Region] -------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Region'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Region
		-------------------------------------------------------------------

		-- [dbo].[Location] -----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Location'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Location @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Guest] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Guest'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Guest @QueueID
		-------------------------------------------------------------------

		-- [dbo].[LoyaltyNumber] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.LoyaltyNumber'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_LoyaltyNumber @QueueID
		-------------------------------------------------------------------

		-- [dbo].[LoyaltyProgram] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.LoyaltyProgram'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_LoyaltyProgram @QueueID
		-------------------------------------------------------------------

		-- [dbo].[PMSRateTypeCode] ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.PMSRateTypeCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_PMSRateTypeCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[PromoCode] ----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.PromoCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_PromoCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[RateCategory] -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.RateCategory'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_RateCategory @QueueID
		-------------------------------------------------------------------

		-- [dbo].[RateCode] -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.RateCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_RateCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[RoomType] -----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.RoomType'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_RoomType @QueueID
		-------------------------------------------------------------------

		-- [dbo].[TravelAgent] --------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.TravelAgent'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_TravelAgent @QueueID
		-------------------------------------------------------------------

		-- [dbo].[VoiceAgent] ----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.VoiceAgent'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_VoiceAgent @QueueID
		-------------------------------------------------------------------

		-- [dbo].[TransactionStatus] --------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.TransactionStatus'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_TransactionStatus @QueueID
		-------------------------------------------------------------------

		-- [dbo].[TransactionDetail] --------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.TransactionDetail'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_TransactionDetail @QueueID
		-------------------------------------------------------------------

		-- [dbo].[fix_DuplicateRoomType_TravelAgent] ----------------------
			-- PRINT STATUS --
			--SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.fix_DuplicateRoomType_TravelAgent'
			--RAISERROR(@print,10,1) WITH NOWAIT

			--EXEC dbo.fix_DuplicateRoomType_TravelAgent
		-------------------------------------------------------------------

		-- dbo.Fix_Duplicate_Guest ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Fix_Duplicate_Guest'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Fix_Duplicate_Guest
		-------------------------------------------------------------------
		
		-- POPULATE TEMP TABLES -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE TEMP TABLES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC map.Populate_mapTables @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Fix_Duplicate_TravelAgent] ----------------------
			 -- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Fix_Duplicate_TravelAgent'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [dbo].[Fix_Duplicate_TravelAgent]
		-------------------------------------------------------------------

		-- [dbo].[Transactions] -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Transactions'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Transactions @QueueID
		-------------------------------------------------------------------

		-- dbo.Update_Reservations_For_NotMRT -----------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Update_Reservations_For_NotMRT'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Update_Reservations_For_NotMRT @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Delete_UnmappedGuests] ----------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Delete_UnmappedGuests'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Delete_UnmappedGuests
		-------------------------------------------------------------------
		

		-- dbo.UPDATE_CRS_BookingSourceID ---------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.UPDATE_CRS_BookingSourceID'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.UPDATE_CRS_BookingSourceID @QueueID
		-------------------------------------------------------------------
	
		-- [dbo].[MostRecentTransaction] ----------------------------------
			-- PRINT STATUS --
			--SET @print = '	' + CONVERT(varchar(100),GETDATE(),120) + ': dbo.MostRecentTransaction'
			--RAISERROR(@print,10,1) WITH NOWAIT

			--EXEC dbo.Populate_MostRecentTransaction
		-------------------------------------------------------------------

		-- MANUAL FIXES ---------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': MANUAL FIXES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [operations].[Run_ManualFixes]
		-------------------------------------------------------------------

		-- FIX BAD EMAIL --------------------------------------------------
					-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': MANUAL FIXES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [operations].Fix_Bad_Email
		-------------------------------------------------------------------

		-- UPDATE MRT IATA NUMBERS ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': UPDATE MRT IATA NUMBERS'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.SabreIATA_Update_MRT
		-------------------------------------------------------------------

		-- RUN PH Booking Source Rules ------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': RUN PH Booking Source Rules'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.PH_BookingSource_RunRules @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Upload_LoyaltyNumber] -----------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Upload_LoyaltyNumber'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Upload_LoyaltyNumber
		-------------------------------------------------------------------

		-- [dbo].[Upload_LoyaltyNumber_FromCharges] -----------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Upload_LoyaltyNumber_FromCharges'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [dbo].[Upload_LoyaltyNumber_FromCharges]
		-------------------------------------------------------------------
		
		-- [dbo].[Update_LoyaltyTagged] -----------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Update_LoyaltyTagged'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.[Update_LoyaltyTagged]
		-------------------------------------------------------------------
	
		-- [dbo].Update_TransactionDetail_CrossBrand ----------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Update_TransactionDetail_CrossBrand'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Update_TransactionDetail_CrossBrand
		-------------------------------------------------------------------

		-- FINISHED -------------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT
		-------------------------------------------------------------------

		SELECT @count = COUNT(*) FROM dbo.Transactions WHERE QueueID = @QueueID
		EXEC ETL.dbo.qs_UpdateQueueStatus @QueueID,@StepID,2,'SUCCESS',@count

	END TRY
	BEGIN CATCH
		SELECT @errorMessage =  ERROR_PROCEDURE() + ': ' + ERROR_MESSAGE()

		EXEC ETL.dbo.qs_UpdateQueueStatus @QueueID,@StepID,3,@errorMessage,0

		RAISERROR(@errorMessage,16,1)
	END CATCH
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_RunRules_ORG]'
GO

ALTER PROCEDURE [dbo].[PH_BookingSource_RunRules_ORG]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- CREATE #BOOKING ----------------------------------------------------
		IF OBJECT_ID('tempdb..#BOOKING') IS NOT NULL
			DROP TABLE #BOOKING;

		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	-----------------------------------------------------------------------


	-- POPULATE GDS -------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE GDS'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'GDS',ss.secondarySource,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),''))
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel = 'GDS'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE IBE - Brand -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE IBE - Brand'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',autx.ibeSourceName,ss.secondarySource
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID IN(	SELECT ibeSource_GroupID
											FROM [authority].[ibeSource_Group]
											WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
										  )
			AND ss.secondarySource != 'IPrefer App'
			AND t.QueueID = @QueueID

		UNION ALL


		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','I Prefer Mobile App','IPrefer App'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Booking Engine')
			AND ss.secondarySource = 'IPrefer App'
			AND t.QueueID = @QueueID

		UNION ALL



		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality')
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE IBE - Hotel -----------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE IBE - Hotel'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'IBE - Hotel',autx.ibeSourceName,ss.secondarySource
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			LEFT JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			LEFT JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID NOT IN(	SELECT ibeSource_GroupID
												FROM [authority].[ibeSource_Group]
												WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
											  )
			AND ss.secondarySource != 'IPrefer App'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE OTA Connect -----------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE OTA Connect'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'OTA Connect',
				CASE WHEN sub.subSourceCode = 'CCX' THEN 'Channel Connect Express' ELSE c.channel END,
				CASE
					WHEN c.channel = 'Channel Connect' THEN ss.secondarySource
					WHEN ss.secondarySource = 'Pegs ADS' THEN ISNULL(sub.subSource,sub.subSourceCode)
					WHEN c.channel = 'IDS' THEN ISNULL(sub.subSource,ss.secondarySource)
				END
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Channel Connect','IDS','Google')
			AND t.QueueID = @QueueID


		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE PMS -------------------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE PMS'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT confirmationNumber,'PMS','PMS','PMS'
		FROM
			(
				SELECT DISTINCT t.confirmationNumber
				FROM dbo.Transactions t
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
				WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
					AND c.channel IN('PMS','PMS Rez Synch')
					AND t.QueueID = @QueueID
		
				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
				WHERE hh.HotelCode = 'CHIAE'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'SHSIntegrationTeam@sabre.com(Integration Integration)'
					AND t.QueueID = @QueueID

				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
					INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
					INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				WHERE hh.HotelCode = 'DALHL'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'integrations@sabre.com(integration user)'
					AND acro.CRO_Code = ''
					AND t.QueueID = @QueueID
			) x


		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------
	
	-- POPULATE Voice - Hotel ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Voice - Hotel'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			INNER JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Voice')
			AND acrog.CRO_Code_Group = 'Hotel Voice Agent'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE Voice - Brand ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Voice - Brand'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND ISNULL(sub.subSourceCode,'') != 'VCG'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality')
			AND ISNULL(acro.CRO_Code_GroupID,0) NOT IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE Voice - Call Gated ----------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Voice - Call Gated'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'Voice - Call Gated',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN [authority].[CRO_Code_Group] acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND sub.subSourceCode = 'VCG'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- MAP UNKNOWN PH Booking Sources -------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': MAP UNKNOWN PH Booking Sources'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.PH_BookingSource_MapUnknowns
	-----------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Medallia_GetContactExportData]'
GO



-- =============================================
-- Author:        Ti Yao
-- Create date: 2023-12-11
-- Description:    This stored procedure is used for Medallia Contact Export API call
-- Prototype: EXEC [dbo].[Medallia_GetContactExportData] 
-- History:	By			Date		Modification
-- =============================================
ALTER PROCEDURE [dbo].[Medallia_GetContactExportData]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    SET NOCOUNT ON;

	INSERT INTO [WUS2-DATA-DB-01.APIS].APIs.[Medallia].[MedalliaExport_Contact]
	( [FirstName]
	, [LastName]
	, [Email]
	, [CustomField1]
	, [CustomField2]
	, [CustomField3]
	, [CustomField4]
	, [CustomField5]
	, [CustomField6]
	, [ConfirmationNumber])
	Select
	mrt.guestFirstName [FirstName]
	, mrt.guestLastName [LastName]
	, ISNULL(NULLIF(mrt.customerEmail,''),cpi.Email) [Email]
	, mrt.HotelName [CustomField1]
	, cpi.tier [CustomField2]
	, mrt.arrivalDate [CustomField3]
	, mrt.departureDate [CustomField4]
	, mrt.HotelCode [CustomField5]
	, mrt.loyaltyNumber [CustomField6]
	, mrt.confirmationNumber 

	from dbo.MostRecentTransactions mrt
	LEFT JOIN Loyalty.dbo.Customer_Profile_Import CPI on mrt.loyaltyNumber = CPI.iPrefer_Number
	LEFT JOIN [WUS2-DATA-DB-01.APIS].APIs.[Medallia].[MedalliaExport_Contact] ex ON ex.confirmationnumber = mrt.confirmationNumber 
	INNER JOIN Hotels.dbo.hotelsreporting hr on mrt.hotelcode = hr.code
	where 
	mrt.departureDate between CAST(getdate()-3 as date) AND CAST(getdate()-1 as date)
	and mrt.customerEmail is not null
	and mrt.LoyaltyNumberValidated = 1
	and mrt.[status] = 'confirmed'
	and cpi.MemberStatus =1	
	and ex.confirmationnumber IS NULL
	and hr.[StatusCodeName] in ('member hotel','renewal member')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Area]'
GO




ALTER PROCEDURE [dbo].[Populate_Area]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Area] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],NULL AS [openHospID],[Area_Text]
		FROM
		(
			SELECT [AreaID] AS [synXisID],NULL AS [openHospID],ISNULL(NULLIF([Area_Text],''),'unknown') AS [Area_Text]
			FROM synxis.Area
		) x
		GROUP BY [Area_Text]
	) AS src ON src.[Area_Text] = tgt.[Area_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[Area_Text])
		VALUES([synXisID],[openHospID],[Area_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_Area] FOREIGN KEY ([AreaID]) REFERENCES [dbo].[Area] ([AreaID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_City] FOREIGN KEY ([CityID]) REFERENCES [dbo].[City] ([CityID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [dbo].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[State] ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[CRS_BookingSource]'
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD CONSTRAINT [FK_dbo_BookingSource_CROCodeID] FOREIGN KEY ([CROCodeID]) REFERENCES [dbo].[CROCode] ([CROCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_CRS_BookingSource] FOREIGN KEY ([CRS_BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_Chain] FOREIGN KEY ([ChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_CorporateCode] FOREIGN KEY ([CorporateCodeID]) REFERENCES [dbo].[CorporateCode] ([CorporateCodeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_IATANumber] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_RateCode] FOREIGN KEY ([RateCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_RoomType] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_DataSourceID] FOREIGN KEY ([DataSourceID]) REFERENCES [authority].[DataSource] ([DataSourceID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [dbo].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_Hotel] FOREIGN KEY ([HotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_LoyaltyNumber] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [dbo].[LoyaltyProgram] ([LoyaltyProgramID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_PH_BookingSourceID] FOREIGN KEY ([PH_BookingSourceID]) REFERENCES [dbo].[PH_BookingSource] ([PH_BookingSourceID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_RateCategory] FOREIGN KEY ([RateCategoryID]) REFERENCES [dbo].[RateCategory] ([RateCategoryID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [dbo].[TransactionDetail] ([TransactionDetailID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TransactionStatus] FOREIGN KEY ([TransactionStatusID]) REFERENCES [dbo].[TransactionStatus] ([TransactionStatusID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TravelAgent] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_VoiceAgentID] FOREIGN KEY ([VoiceAgentID]) REFERENCES [dbo].[VoiceAgent] ([VoiceAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TravelAgent]'
GO
ALTER TABLE [dbo].[TravelAgent] ADD CONSTRAINT [FK_dbo_TravelAgent_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [dbo].[TravelAgent] ADD CONSTRAINT [FK_dbo_TravelAgent_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[ibeSource]'
GO
ALTER TABLE [dbo].[ibeSource] ADD CONSTRAINT [FK_dbo_ibeSource_auth_ibeSourceID] FOREIGN KEY ([auth_ibeSourceID]) REFERENCES [authority].[ibeSource] ([ibeSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TransactionStatus]'
GO
ALTER TABLE [dbo].[TransactionStatus] ADD CONSTRAINT [FK_dbo_TransactionStatus_actionTypeOrder] FOREIGN KEY ([ActionTypeID]) REFERENCES [dbo].[ActionType] ([ActionTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_CRS_BookingSource]'
GO
ALTER TABLE [map].[openHosp_CRS_BookingSource] ADD CONSTRAINT [FK_map_openHosp_CRS_BookingSource_BookingSourceID] FOREIGN KEY ([BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_CRS_BookingSource]'
GO
ALTER TABLE [map].[pegasus_CRS_BookingSource] ADD CONSTRAINT [FK_map_pegasus_CRS_BookingSource_BookingSourceID] FOREIGN KEY ([BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_CRS_BookingSource]'
GO
ALTER TABLE [map].[sabreAPI_CRS_BookingSource] ADD CONSTRAINT [FK_map_sabreAPI_CRS_BookingSource_BookingSourceID] FOREIGN KEY ([BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_CRS_BookingSource]'
GO
ALTER TABLE [map].[synXis_CRS_BookingSource] ADD CONSTRAINT [FK_map_synXis_CRS_BookingSource_BookingSourceID] FOREIGN KEY ([BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_Chain]'
GO
ALTER TABLE [map].[openHosp_Chain] ADD CONSTRAINT [FK_map_openHosp_Chain_intChainID] FOREIGN KEY ([intChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_Chain]'
GO
ALTER TABLE [map].[pegasus_Chain] ADD CONSTRAINT [FK_map_pegasus_Chain_intChainID] FOREIGN KEY ([intChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_Chain]'
GO
ALTER TABLE [map].[sabreAPI_Chain] ADD CONSTRAINT [FK_map_sabreAPI_Chain_intChainID] FOREIGN KEY ([intChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_Chain]'
GO
ALTER TABLE [map].[synXis_Chain] ADD CONSTRAINT [FK_map_synXis_Chain_intChainID] FOREIGN KEY ([intChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_CorporateCode]'
GO
ALTER TABLE [map].[sabreAPI_CorporateCode] ADD CONSTRAINT [FK_map_sabreAPI_CorporateCode_CorporateCodeID] FOREIGN KEY ([CorporateCodeID]) REFERENCES [dbo].[CorporateCode] ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_CorporateCode]'
GO
ALTER TABLE [map].[synXis_CorporateCode] ADD CONSTRAINT [FK_map_synXis_CorporateCode_CorporateCodeID] FOREIGN KEY ([CorporateCodeID]) REFERENCES [dbo].[CorporateCode] ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [FK_dbo_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [dbo].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [FK_dbo_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_IATANumber]'
GO
ALTER TABLE [map].[openHosp_IATANumber] ADD CONSTRAINT [FK_map_openHosp_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_IATANumber]'
GO
ALTER TABLE [map].[pegasus_IATANumber] ADD CONSTRAINT [FK_map_pegasus_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] ADD CONSTRAINT [FK_map_sabreAPI_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_IATANumber]'
GO
ALTER TABLE [map].[synXis_IATANumber] ADD CONSTRAINT [FK_map_synXis_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_PromoCode]'
GO
ALTER TABLE [map].[openHosp_PromoCode] ADD CONSTRAINT [FK_map_openHosp_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_PromoCode]'
GO
ALTER TABLE [map].[pegasus_PromoCode] ADD CONSTRAINT [FK_map_pegasus_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] ADD CONSTRAINT [FK_map_sabreAPI_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_PromoCode]'
GO
ALTER TABLE [map].[synXis_PromoCode] ADD CONSTRAINT [FK_map_synXis_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_RateCode]'
GO
ALTER TABLE [map].[openHosp_RateCode] ADD CONSTRAINT [FK_map_openHosp_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_RateCode]'
GO
ALTER TABLE [map].[pegasus_RateCode] ADD CONSTRAINT [FK_map_pegasus_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] ADD CONSTRAINT [FK_map_sabreAPI_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_RateCode]'
GO
ALTER TABLE [map].[synXis_RateCode] ADD CONSTRAINT [FK_map_synXis_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] ADD CONSTRAINT [FK_map_sabreAPI_RoomType_RoomTypeID] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_RoomType]'
GO
ALTER TABLE [map].[synXis_RoomType] ADD CONSTRAINT [FK_map_synXis_RoomType_RoomTypeID] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] ADD CONSTRAINT [FK_map_sabreAPI_TravelAgent_TravelAgentID] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_TravelAgent]'
GO
ALTER TABLE [map].[synXis_TravelAgent] ADD CONSTRAINT [FK_map_synXis_TravelAgent_TravelAgentID] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_hotel]'
GO
ALTER TABLE [map].[openHosp_hotel] ADD CONSTRAINT [FK_map_openHosp_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_hotel]'
GO
ALTER TABLE [map].[pegasus_hotel] ADD CONSTRAINT [FK_map_pegasus_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] ADD CONSTRAINT [FK_map_sabreAPI_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_hotel]'
GO
ALTER TABLE [map].[synXis_hotel] ADD CONSTRAINT [FK_map_synXis_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
