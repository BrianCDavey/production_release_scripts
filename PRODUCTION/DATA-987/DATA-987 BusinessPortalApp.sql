USE BusinessPortalApp
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.BusinessPortalApp    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.BusinessPortalApp

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 4/30/2020 11:10:35 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[udfUserSecurity]'
GO
DROP FUNCTION [dbo].[udfUserSecurity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[udfUserSecurity]'
GO

CREATE FUNCTION [dbo].[udfUserSecurity](@AppUser varchar(64),@quinonez varchar(64))
RETURNS @CUST TABLE([Customer Number] varchar(15))
AS
BEGIN
	IF @AppUser = 'indecorp\pelefson'
	BEGIN
		-- get all hotel codes for pelefson
		INSERT INTO @CUST([Customer Number])
		SELECT [Customer Number]
		FROM dbo.vwCustomers
		WHERE
		(
			SELECT TOP 1 DomainName
			FROM LocalCRM.dbo.SystemUser
			WHERE SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%'
		) LIKE '%@historichotels.org%'
			AND [Customer Class] = 'HISTORIC'

		UNION ALL

		SELECT [Customer Number]
		FROM dbo.vwCustomers
		WHERE EXISTS
		(
			SELECT 1
			FROM LocalCRM.dbo.SystemUser
				INNER JOIN LocalCRM.dbo.SystemUserRoles ON SystemUser.SystemUserId = SystemUserRoles.SystemUserId
			WHERE SystemUserRoles.RoleId IN ('E007A965-C059-DF11-9CA1-005056922997' -- PHG Exec and AMD
												,'8A9F5977-1A69-DF11-A5A5-005056922997' -- PHG Finance
												,'23B713F7-4845-E611-80E4-000D3A914DF8' -- PHG Exec and AMD
												,'6E1B1190-4845-E611-80E4-000D3A914DF8' -- PHG Finance
												,'20A03CD3-4F7C-E611-8117-000D3A9011F5' -- Contracts
											)
			AND
		--had to add an explicit exception for Neglys Zambrano as her domain name is NZambrano-nunez but email is just NZambrano
			SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%')
			OR LTRIM(RTRIM([Customer Number])) COLLATE SQL_Latin1_General_CP1_CI_AS
					IN(
						SELECT Account.AccountNumber
						FROM LocalCRM.dbo.Account
							INNER JOIN LocalCRM.dbo.SystemUser ON SystemUser.FullName 
									IN(Account.phg_regionalmanageridName,Account.phg_areamanageridName,
										Account.phg_accountdirectoridname,Account.phg_revenueaccountmanageridName,
										Account.phg_regionaladministrationidName,Account.[PHG_PHGContactIdName],
										Account.phg_corporateglobalaccountmanageridname,Account.phg_touroperatorglobalaccountmanageridname,
										Account.[phg_alliancepartnermanageridName],Account.[phg_EnterpriseAccountLeadName],
										Account.[phg_EnterpriseRevenueLeadName])
						WHERE SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%'
					   )

		-- union in lquinonez
		UNION ALL

		SELECT [Customer Number]
		FROM dbo.vwCustomers
		WHERE EXISTS
		(
			SELECT 1
			FROM LocalCRM.dbo.SystemUser
				INNER JOIN LocalCRM.dbo.SystemUserRoles ON SystemUser.SystemUserId = SystemUserRoles.SystemUserId
			WHERE SystemUserRoles.RoleId IN ('E007A965-C059-DF11-9CA1-005056922997' -- PHG Exec and AMD
												,'8A9F5977-1A69-DF11-A5A5-005056922997' -- PHG Finance
												,'23B713F7-4845-E611-80E4-000D3A914DF8' -- PHG Exec and AMD
												,'6E1B1190-4845-E611-80E4-000D3A914DF8' -- PHG Finance
												,'20A03CD3-4F7C-E611-8117-000D3A9011F5' -- Contracts
											)
				AND
				--had to add an explicit exception for Neglys Zambrano as her domain name is NZambrano-nunez but email is just NZambrano
				SystemUser.DomainName LIKE 'lquinonez%'
		)
				OR LTRIM(RTRIM([Customer Number])) COLLATE SQL_Latin1_General_CP1_CI_AS
							IN(
								SELECT Account.AccountNumber
								FROM LocalCRM.dbo.Account
									INNER JOIN LocalCRM.dbo.SystemUser ON SystemUser.FullName
											IN (Account.phg_regionalmanageridName,Account.phg_areamanageridName,
												Account.phg_accountdirectoridname,Account.phg_revenueaccountmanageridName,
												Account.phg_regionaladministrationidName,Account.[PHG_PHGContactIdName],
												Account.phg_corporateglobalaccountmanageridname,Account.phg_touroperatorglobalaccountmanageridname,
												Account.[phg_alliancepartnermanageridName],Account.[phg_EnterpriseAccountLeadName],
												Account.[phg_EnterpriseRevenueLeadName])
								WHERE SystemUser.DomainName LIKE 'lquinonez%'
							   )
	END
	-- end bespoke logic and start regular query
	ELSE
	BEGIN
		INSERT INTO @CUST([Customer Number])
		SELECT [Customer Number]
		FROM dbo.vwCustomers
		WHERE
		(
			SELECT TOP 1 DomainName
			FROM LocalCRM.dbo.SystemUser
			WHERE SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%'
		) LIKE '%@historichotels.org%'
			AND [Customer Class] = 'HISTORIC'

		UNION ALL

		SELECT [Customer Number]
		FROM dbo.vwCustomers
		WHERE EXISTS(
						SELECT 1
						FROM LocalCRM.dbo.SystemUser
							INNER JOIN LocalCRM.dbo.SystemUserRoles ON SystemUser.SystemUserId = SystemUserRoles.SystemUserId
						WHERE SystemUserRoles.RoleId IN ('E007A965-C059-DF11-9CA1-005056922997' -- PHG Exec and AMD
															,'8A9F5977-1A69-DF11-A5A5-005056922997' -- PHG Finance
															,'23B713F7-4845-E611-80E4-000D3A914DF8' -- PHG Exec and AMD
															,'6E1B1190-4845-E611-80E4-000D3A914DF8' -- PHG Finance
															,'20A03CD3-4F7C-E611-8117-000D3A9011F5' -- Contracts
														)
							AND
								--had to add an explicit exception for Neglys Zambrano as her domain name is NZambrano-nunez but email is just NZambrano
								SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%')
							OR LTRIM(RTRIM([Customer Number])) COLLATE SQL_Latin1_General_CP1_CI_AS
									IN(
										SELECT Account.AccountNumber
										FROM LocalCRM.dbo.Account
											INNER JOIN LocalCRM.dbo.SystemUser ON SystemUser.FullName IN(Account.phg_regionalmanageridName,Account.phg_areamanageridName,
																											Account.phg_accountdirectoridname,Account.phg_revenueaccountmanageridName,
																											Account.phg_regionaladministrationidName,Account.[PHG_PHGContactIdName],
																											Account.phg_corporateglobalaccountmanageridname,Account.phg_touroperatorglobalaccountmanageridname,
																											Account.[phg_alliancepartnermanageridName],Account.[phg_EnterpriseAccountLeadName],
																											Account.[phg_EnterpriseRevenueLeadName])
										WHERE SystemUser.DomainName LIKE REPLACE(REPLACE(@AppUser, 'INDECORP\', ''), '-nunez', '') + '%'
									   )
	END

	RETURN
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
