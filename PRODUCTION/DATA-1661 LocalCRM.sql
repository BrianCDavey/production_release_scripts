USE LocalCRM
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.LocalCRM    -  This database will be modified

to synchronize it with:

        (local)\WAREHOUSE.LocalCRM

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 8/5/2021 5:13:50 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[crm_synchronize_IPreferBookingReward]'
GO



ALTER PROCEDURE [dbo].[crm_synchronize_IPreferBookingReward]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM staging.IPreferBookingReward)
	BEGIN
		MERGE INTO dbo.IPreferBookingReward AS tgt
		USING (SELECT * FROM staging.IPreferBookingReward) AS src ON src.[versionNumber] = tgt.[versionNumber]
		WHEN MATCHED THEN
			UPDATE
				SET createdBy = src.createdBy,createdByName = src.createdByName,createdByYomiName = src.createdByYomiName,createdOn = src.createdOn,createdOnBehalfBy = src.createdOnBehalfBy,createdOnBehalfByName = src.createdOnBehalfByName,createdOnBehalfByYomiName = src.createdOnBehalfByYomiName,importSequenceNumber = src.importSequenceNumber,modifiedBy = src.modifiedBy,modifiedByName = src.modifiedByName,modifiedByYomiName = src.modifiedByYomiName,modifiedOn = src.modifiedOn,modifiedOnBehalfBy = src.modifiedOnBehalfBy,modifiedOnBehalfByName = src.modifiedOnBehalfByName,modifiedOnBehalfByYomiName = src.modifiedOnBehalfByYomiName,organizationId = src.organizationId,organizationIdName = src.organizationIdName,overriddenCreatedOn = src.overriddenCreatedOn,phg_endDate = src.phg_endDate,phg_hotelId = src.phg_hotelId,phg_hotelIdName = src.phg_hotelIdName,phg_hotelIdYomiName = src.phg_hotelIdYomiName,phg_ipreferBookingRewardId = src.phg_ipreferBookingRewardId,phg_name = src.phg_name,phg_startDate = src.phg_startDate,stateCode = src.stateCode,stateCodeName = src.stateCodeName,statusCode = src.statusCode,statusCodeName = src.statusCodeName,timezoneRuleVersionNumber = src.timezoneRuleVersionNumber,utcConversionTimezoneCode = src.utcConversionTimezoneCode,versionNumber = src.versionNumber,phg_amenitycategory = src.phg_amenitycategory,phg_amenitycategoryname = src.phg_amenitycategoryname,phg_eligibletier= src.phg_eligibletier,phg_eligibletiername = src.phg_eligibletiername
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([createdBy],[createdByName],[createdByYomiName],[createdOn],[createdOnBehalfBy],[createdOnBehalfByName],[createdOnBehalfByYomiName],[importSequenceNumber],[modifiedBy],[modifiedByName],[modifiedByYomiName],[modifiedOn],[modifiedOnBehalfBy],[modifiedOnBehalfByName],[modifiedOnBehalfByYomiName],[organizationId],[organizationIdName],[overriddenCreatedOn],[phg_endDate],[phg_hotelId],[phg_hotelIdName],[phg_hotelIdYomiName],[phg_ipreferBookingRewardId],[phg_name],[phg_startDate],[stateCode],[stateCodeName],[statusCode],[statusCodeName],[timezoneRuleVersionNumber],[utcConversionTimezoneCode],[versionNumber],phg_amenitycategory,phg_amenitycategoryname,phg_eligibletier,phg_eligibletiername)
			VALUES(src.[createdBy],src.[createdByName],src.[createdByYomiName],src.[createdOn],src.[createdOnBehalfBy],src.[createdOnBehalfByName],src.[createdOnBehalfByYomiName],src.[importSequenceNumber],src.[modifiedBy],src.[modifiedByName],src.[modifiedByYomiName],src.[modifiedOn],src.[modifiedOnBehalfBy],src.[modifiedOnBehalfByName],src.[modifiedOnBehalfByYomiName],src.[organizationId],src.[organizationIdName],src.[overriddenCreatedOn],src.[phg_endDate],src.[phg_hotelId],src.[phg_hotelIdName],src.[phg_hotelIdYomiName],src.[phg_ipreferBookingRewardId],src.[phg_name],src.[phg_startDate],src.[stateCode],src.[stateCodeName],src.[statusCode],src.[statusCodeName],src.[timezoneRuleVersionNumber],src.[utcConversionTimezoneCode],src.[versionNumber],src.phg_amenitycategory,src.phg_amenitycategoryname,src.phg_eligibletier,src.phg_eligibletiername)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
