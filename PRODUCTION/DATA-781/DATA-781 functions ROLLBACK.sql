USE functions
GO

/*
Run this script on:

        CHI-SQ-DP-01\LINKED_DB.functions    -  This database will be modified

to synchronize it with:

        CHISQP01.functions

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/13/2020 11:03:36 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[currencyRateInsert]'
GO



/* =================================================================================================
-- Author:		Kris Scott
-- Create date: 5-27-2011
-- Description:	Update currency rates in GP
Changes:

Name				Date		Description
-------------------------------------------------
Laura Culley		11/30/2011	Changed start and end date to accomdate daily currency rate loads
Laura Culley		12/29/2011  Changed end date to be the same date as the start date to accurately calculate 
								the currency Rate.
Brian Davey			26-JUN-2017	Modified SP to RAISERROR when no rows are inserted.
-- ================================================================================================*/
ALTER PROCEDURE [dbo].[currencyRateInsert] 
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @suffixes AS TABLE (suffix nvarchar(10))

	INSERT INTO @suffixes(suffix)
	VALUES('-AVG'),('-BUY'),('-SELL')

	DECLARE @startDate datetime, @endDate datetime

	SET @startDate = (DATEADD(DAY, 0, DATEDIFF(DAY, 0, GETDATE())))
	SET @endDate = @startDate


	INSERT INTO DYNAMICS.dbo.MC00100([EXGTBLID],[CURNCYID],[EXCHDATE],TIME1,[XCHGRATE],[EXPNDATE])
	SELECT [EXGTBLID],[CURNCYID],[EXCHDATE],TIME1,[XCHGRATE],[EXPNDATE]
	FROM
		(
			SELECT DISTINCT [code] + suffix AS EXGTBLID,code AS CURNCYID,@startDate AS EXCHDATE,'1900-01-01 00:00:00.000' AS TIME1,CONVERT(numeric(19,7),fromUSD) AS XCHGRATE,@endDate AS EXPNDATE
			FROM [CurrencyRates].[dbo].[dailyRates]
				INNER JOIN DYNAMICS.dbo.MC40200 ON dailyRates.code = MC40200.CURNCYID
				INNER JOIN @suffixes ON 1=1
			WHERE dailyRates.rateDate = @startDate
	
			EXCEPT

			SELECT [EXGTBLID],[CURNCYID],[EXCHDATE],TIME1,[XCHGRATE],[EXPNDATE]
			FROM DYNAMICS.dbo.MC00100
			WHERE [EXCHDATE] = @startDate
		) xchg
	ORDER BY CURNCYID

	-- verify records were moved
	IF @@ROWCOUNT = 0
		RAISERROR(N'WARNING! No Records Inserted Into Great Plains.',16,1)

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[checkMissingDays_Epsilon]'
GO


-- =============================================
-- Author:		Jake Smith
-- Create date: 2020-03-04
-- Description:	Checks for missing Epsilon days
-- =============================================
CREATE PROCEDURE [dbo].[checkMissingDays_Epsilon]
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  WITH DateRange(Date) AS
     (
         SELECT
             @StartDate Date
         UNION ALL
         SELECT
             DATEADD(day, 1, Date) Date
         FROM
             DateRange
         WHERE
             Date < @EndDate
     )

     SELECT Date as MissingDate 
     FROM DateRange
     EXCEPT 
     SELECT DISTINCT CAST(activityDate as DATE)
     FROM [CHI-SQ-PR-01\WAREHOUSE].loyalty.dbo.PointActivity
     WHERE 
	 CAST(activitydate as DATE) BETWEEN @StartDate AND @EndDate
     --You could remove Maximum Recursion level constraint by specifying a MaxRecusion of zero
     OPTION (MaxRecursion 10000);
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
