/*
Run this script on:

        CHI-SQ-DP-01\LINKED_DB.functions    -  This database will be modified

to synchronize it with:

        CHISQP01.functions

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 5/5/2020 10:11:46 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[preJournalEntryIntegration_old_ConfirmBeforeDelete]'
GO


CREATE PROCEDURE [dbo].[preJournalEntryIntegration_old_ConfirmBeforeDelete] 
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

--get the hotel production totals for invoice header information
DECLARE @hotelPreferences as table
	(	hotelCode nvarchar(10), currencyCode char(3), DECPLCUR int --, PYMTRMID char(21), DSCPCTAM int
	)

INSERT	INTO @hotelPreferences
SELECT	HS.hotelCode,
		customerMaster.CURNCYID,
		currencyMaster.DECPLCUR
		--, customerMaster.PYMTRMID
		--, paymentTerms.DSCPCTAM

-- select * 
FROM	MIASQP02.Superset.BSI.Credit_Memos CM

JOIN	MIASQP01.Core.dbo.hotels_Synxis HS
	ON	CM.hotel_code = HS.hotelcode

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	HS.hotelCode = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC40200 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID

--LEFT OUTER JOIN IC.dbo.SY03300 AS paymentTerms 
--	ON	customerMaster.PYMTRMID = paymentTerms.PYMTRMID

WHERE	CM.Redemption_Date between @startDate and @endDate
AND		CM.Payable_Value_USD <> 0.00
		
GROUP BY HS.hotelCode, customerMaster.CURNCYID, currencyMaster.DECPLCUR --,customerMaster.PYMTRMID --,paymentTerms.DSCPCTAM
ORDER BY HS.hotelCode


--wipe out the single use table for reservation integration
truncate table functions.dbo.JournalEntryIntegration

--fill the single use reservation integration table with the details of this particular integration
INSERT INTO functions.dbo.JournalEntryIntegration
	(	[CustomerCode] ,[BrandCode] ,[InvoiceDate] ,[ItemCode] ,[CurrencyCode] ,[gpSiteId] ,
		[AccountNumber] , [Source] , [EntryType], [Quantity] ,[pricePer] ,[TotalValue_USD] ,
		[TotalValue_HotelCurrency] , [autoCreditAmount] ,[batchID],[poNum]
	)
select	CM.hotel_code , -- [customerCode]
		H.mainbrandcode , --[brandCode]
		@invoiceDate , --[invoiceDate]
		'Credit Memo' , --[itemCode]  ** hard coded for the time being, but could change in the future
		CM.Voucher_Currency , --[currencyCode]
		B.[gpSiteId] , -- from Brands table, based on hotel main brand code ??
		'GL.00100 AccountNumbers' ,
		'Source' , 
		'EntryType ',
		COUNT(CM.Voucher_Number) , -- [quantity]
		SUM( CM.Voucher_Value ) / COUNT(CM.Voucher_Number) as pricePer ,
		CAST(ROUND(SUM(CM.Voucher_Value * (CASE HP.CURNCYID WHEN 'USD' THEN 1.00 ELSE CurrencyMaster.XCHGRATE END)), HP.DECPLCUR-1) as DECIMAL(12,2)) as TotalValue_USD ,
		CAST(ROUND(SUM( CM.Voucher_Value ), HP.DECPLCUR-1) as DECIMAL(12,2)) as TotalValue_HotelCurrency ,		
		0 as autoCreditAmount ,
		'CM ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2), -- ,[batchID]
		'Credit Memos ' + + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2)  -- ,[poNum]
-- select * 
from	@hotelPreferences HP
join	MIASQP02.[Superset].[BSI].[Credit_Memos] CM 
	on	HP.hotelcode = CM.Hotel_Code

join	Core.dbo.HotelsReporting H with (nolock)
	on	CM.hotel_code = H.code

join	Core.dbo.Brands B with (nolock)
	on	H.mainbrandcode = B.code

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	H.Code = customerMaster.CUSTNMBR
--JOIN	 GL.00100 for AccountNumbers
JOIN	DYNAMICS.dbo.MC00100 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID
	AND	CM.Redemption_Date between currencyMaster.EXCHDATE AND currencyMaster.EXPNDATE
	AND	EXGTBLID like '%AVG%'

WHERE	CM.Redemption_Date between @startDate and @endDate
AND		CM.sopNumber is null

GROUP BY CM.hotel_code, H.mainbrandcode , B.[gpSiteId]

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sopDocsForInsert_ConfirmBeforeDelete]'
GO







CREATE VIEW [dbo].[sopDocsForInsert_ConfirmBeforeDelete]
AS
select distinct
		LTRIM(RTRIM(rt.[Document Number])) as sopNumber
		,LTRIM(RTRIM(rt.[Customer Number])) as hotelCode
		,SopHeader.BACHNUMB as batchName
		,LTRIM(RTRIM(COALESCE(SopHeader.DOCID,[Document Type]))) as docType
	,CASE
		WHEN SopHeader.[sopNumbe] = rt.[Document Description]
			THEN CASE 
					WHEN LEN([Customer PO Number]) > 0
						THEN LTRIM(RTRIM([Customer PO Number]))
				END
		ELSE LTRIM(RTRIM([Document Type])) + ' ' + LTRIM(RTRIM([Document Description]))
	END	AS [docDescription]
		,rt.[Currency ID] as currencyCode
		,rt.[Document Date] as docDate
		,SopHeader.[dueDate] as dueDate
		,rt.[Original Trx Amount] as totalAmount		
		,COALESCE(rt.[Original Trx Amount] - rt.[Current Trx Amount], 0) as amountPaid
		,COALESCE(rt.[Current Trx Amount],SopHeader.ORDOCAMT) as balanceDue
	from IC.dbo.ReceivablesTransactions as rt
	LEFT OUTER JOIN IC.dbo.RM00101 custMaster
		ON rt.[Customer Number] = custMaster.CUSTNMBR
	LEFT OUTER JOIN DYNAMICS.dbo.MC40200 AS currencyMaster 
		ON rt.[Currency ID] = currencyMaster.CURNCYID
	LEFT OUTER JOIN IC.dbo.SOP30200 as SopHeader
		ON rt.[Document Number] = SopHeader.SOPNUMBE
	LEFT OUTER JOIN MIASQS01.[CmsImport].[dbo].financialDocuments pn
		on rt.[Document Number] = pn.sopNumber
	WHERE rt.[Document Date] >= DATEADD(YEAR,-3,GETDATE())
	AND pn.sopNumber IS NULL
	AND rt.[Document Number] <> 'INV0116745'













GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[sopDocsForPreferredNetUpdate_ConfirmBeforeDelete]'
GO



CREATE VIEW [dbo].[sopDocsForPreferredNetUpdate_ConfirmBeforeDelete]
AS
	select distinct
		LTRIM(RTRIM(SopHeader.[sopNumbe])) as sopNumber
		,LTRIM(RTRIM(SopHeader.[CUSTNMBR])) as hotelCode
		,LTRIM(RTRIM(SopHeader.DOCID)) as docType
	,CASE
		WHEN [Document Number] = [Document Description]
			THEN CASE 
					WHEN LEN([Customer PO Number]) = 0
						THEN LTRIM(RTRIM([Document Type]))
					ELSE LTRIM(RTRIM([Customer PO Number]))
				END
		ELSE LTRIM(RTRIM([Document Type])) + ' ' + LTRIM(RTRIM([Document Description]))
	END	AS [docDescription]		
		,SopHeader.CURNCYID as currencyCode
		,SopHeader.ORDOCAMT as totalAmount
		,SopHeader.[docDate] as docDate
		,SopHeader.[dueDate] as dueDate
		,COALESCE(SopHeader.ORDOCAMT - payments.[Current Trx Amount], 0) as amountPaid
		,COALESCE(payments.[Current Trx Amount],SopHeader.ORDOCAMT) as balanceDue
	from IC.dbo.SOP30200 SopHeader
	INNER JOIN MIASQS01.[CmsImport].[dbo].financialDocuments pn
		on SopHeader.SOPNUMBE = pn.sopNumber	
	INNER JOIN DYNAMICS.dbo.MC40200 AS currencyMaster 
		ON SopHeader.CURNCYID = currencyMaster.CURNCYID
	INNER JOIN IC.dbo.ReceivablesTransactions as payments
		ON SopHeader.SOPNUMBE = payments.[Document Number]
	WHERE pn.balanceDue <> COALESCE(payments.[Current Trx Amount],SopHeader.ORDOCAMT)
		AND SopHeader.SOPNUMBE <> 'INV0116745';







GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PreferredNetProcessSopDocs_ConfirmBeforeDelete]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 01/06/2016
-- Description:	Sends SOP Docs to Preferred Net
-- =============================================
CREATE PROCEDURE [dbo].[PreferredNetProcessSopDocs_ConfirmBeforeDelete] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--delete old documents
	UPDATE MIASQS01.[CmsImport].[dbo].financialDocuments
	SET toDelete = 1
	WHERE dueDate <= DATEADD(YEAR,-4,GETDATE());

	--insert all posted sop docs into the Preferred Net metadata table
	INSERT INTO MIASQS01.[CmsImport].[dbo].financialDocuments ([sopNumber]
      ,[hotelCode]
      ,[custClass]
      ,[batchName]
      ,[docType]
      ,[docDescription]
      ,[currencyCode]
      ,[totalAmount]
      ,[docDate]
      ,[dueDate]
      ,[amountPaid]
      ,[balanceDue]
      ,[printed]
      ,[emailed]
      ,toDelete)
	SELECT [sopNumber]
      ,[hotelCode]
      ,[custClass]
      ,[batchName]      
      ,[docType]
      ,[docDescription]
      ,[currencyCode]
      ,[totalAmount]
      ,[docDate]
      ,[dueDate]
      ,[amountPaid]
      ,[balanceDue]
      ,0
      ,0
      ,0
	FROM [functions].[dbo].[sopDocsForPreferredNetInsert];
	
	--update all posted sop docs with the most accurate payment info
	UPDATE pn
	SET pn.[totalAmount] = sopUpdate.totalAmount
      ,pn.[amountPaid] = sopUpdate.amountPaid
      ,pn.[balanceDue] = sopUpdate.balanceDue
	FROM MIASQS01.[CmsImport].[dbo].financialDocuments pn
	INNER JOIN [functions].[dbo].[sopDocsForPreferredNetUpdate] sopUpdate
		ON pn.sopNumber = sopUpdate.sopNumber;
		
	--insert any new hotels into the running status for preferred net
	INSERT INTO MIASQS01.[CmsImport].[dbo].[FinancialAccount] ([hotelCode]
	  ,[hotelName]
      ,[currencyCode]
      ,[totalDue]
      ,[currentBalance]
      ,[pastDue]
      ,[pastDueDays]
      ,[serviceLevel])
     SELECT gp.[hotelCode]
	  ,gp.[hotelName]
      ,gp.[currencyCode]
      ,gp.[totalDue]
      ,gp.[currentBalance]
      ,gp.[pastDue]
      ,gp.pastDueDays
      ,gp.serviceLevel
     FROM functions.dbo.pastDueAmounts gp
     LEFT OUTER JOIN MIASQS01.[CmsImport].[dbo].[FinancialAccount] pn
		on gp.hotelCode = pn.hotelCode
		and gp.currencyCode = pn.currencyCode
	WHERE pn.hotelCode IS NULL;
  
	--update each hotel's running status
	UPDATE pn
	SET pn.[totalDue] = gp.[totalDue]
		,pn.[currentBalance] = gp.[currentBalance]
		,pn.[pastDue] = gp.pastDue
		,pn.[pastDueDays] = gp.pastDueDays
        ,pn.[serviceLevel] = gp.serviceLevel
  FROM MIASQS01.[CmsImport].[dbo].[FinancialAccount] pn
  INNER JOIN functions.dbo.pastDueAmounts gp
	on pn.hotelCode = gp.hotelCode
	and pn.currencyCode = gp.currencyCode;
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
