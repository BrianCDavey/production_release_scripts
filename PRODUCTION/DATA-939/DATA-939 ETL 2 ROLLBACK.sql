USE ETL
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/6/2020 6:52:28 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SASGenderAnalysis_FinalImport_ToDimensionalWareHouse]'
GO


-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-03-29
-- Description:	This procedure is a part of crafted import process loading data from ETL db to Marketing DB
-- Prototype: EXEC [dbo].[SASGenderAnalysis_FinalImport_ToDimensionalWareHouse] 1
-- History: 2020-03-29 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SASGenderAnalysis_FinalImport_ToDimensionalWareHouse]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

		--DECLARE @QueueID int = 1

		UPDATE g
		SET	g.[Gender] = CASE WHEN sga.GenderName = 'F' THEN 'Female' WHEN sga.GenderName = 'M' THEN 'Male' ELSE 'Unknown' END,
			g.[IsSelfReported_Gender] = 0
		FROM Dimensional_Warehouse.dim.Guest g
		INNER JOIN dbo.Import_SASGenderAnalysis sga ON g.guestKey = sga.guestKey
		WHERE QueueID = @QueueID
			AND (g.IsSelfReported_Gender = 0 OR g.Gender NOT IN('Male','Female'))

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
