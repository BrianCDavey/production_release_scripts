/*
Run this script on:

        wus2-data-dp-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 7/15/2022 2:55:44 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoRegistrant_FinalImport_Core_Swoogo]'
GO


-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-07-29
-- Description:	This procedure is a part of Swoogo import process loading data from ETL db to Core DB
-- Prototype: EXEC [dbo].[SwoogoRegistrant_FinalImport_Core_Swoogo] 3
-- History: 2020-08-13 Ti Yao Initial Creation
--			2021-11-09	Brian Davey	Modified to allow processing from new location
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoRegistrant_FinalImport_Core_Swoogo]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @app varchar(255)
	SELECT @app = [Application] FROM dbo.Queue WHERE QueueID = @QueueID

	DELETE FROM Core.Swoogo.weboomAccountingRegistrations
	
	-- MAX EVENT INFORMATION ----------------------------------------------------------
	DECLARE @EventQueueID int
	SELECT @EventQueueID = MAX(QueueID) FROM dbo.Queue WHERE Application = 'SwoogoEventDetail_FromAzure'
	-----------------------------------------------------------------------------------

	-- CREATE & POPULATE #OldInvoiceDueDate -------------------------------------------
	IF OBJECT_ID('tempdb..#OldInvoiceDueDate') IS NOT NULL
	DROP TABLE #OldInvoiceDueDate;

	CREATE TABLE #OldInvoiceDueDate
	(
		financeEventCode nvarchar(100) NOT NULL,
		InvoiceDueDate nvarchar(255) NULL

		PRIMARY KEY CLUSTERED(financeEventCode)
	)

	INSERT INTO #OldInvoiceDueDate(financeEventCode,InvoiceDueDate)
	SELECT DISTINCT JSON_VALUE(EventDetailJSON,'$.c_14144') AS financeEventCode,
				MAX(JSON_VALUE(EventDetailJSON,'$.c_17292')) AS invoiceDueDate
	FROM dbo.Import_SwoogoEventDetail
	WHERE QueueID = 37487 --hard code this batch before invoice due date column is removed
		AND JSON_VALUE(EventDetailJSON,'$.id') NOT IN (27822,27823)
		AND JSON_VALUE(EventDetailJSON,'$.name') NOT LIKE '%template%'
	 GROUP BY  JSON_VALUE(EventDetailJSON,'$.c_14144')
	 -----------------------------------------------------------------------------------
	
	IF @app = 'SwoogoRegistrantDetail'
	BEGIN
		-- INSERT INTO Core.Swoogo.weboomAccountingRegistrations ---------------------------
		INSERT INTO Core.Swoogo.weboomAccountingRegistrations([QueueID],[accountCode],[billingAmount],[billingAmountCurrency],[invoiceDueDate],[serviceDate],[financeEventCode],[marketingInclusionFlag],[marketingInclusionExplanation],[registrationStatus],[confirmationNumber],[attendingGuestFullName],[eventType],[eventDate],[eventLocation],[timeLoaded],[isBad],[errorDescription],[isDeleted],[registrantType])
		SELECT DISTINCT @QueueID AS QueueID,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.c_633673') AS accountCode,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.individual_gross') AS billingAmount,
				ISNULL(NULLIF(JSON_VALUE(se.EventDetailJSON,'$.currency'),''),'USD') AS billingAmountCurrency,
				COALESCE(CASE WHEN ISDATE(JSON_VALUE(se.EventDetailJSON,'$.c_40308')) = 0 THEN NULL ELSE JSON_VALUE(se.EventDetailJSON,'$.c_40308') END,war.invoiceDueDate,old.InvoiceDueDate,CAST(getdate() AS DATE)) AS invoiceDueDate,
				JSON_VALUE(se.EventDetailJSON,'$.start_date') AS serviceDate,
				JSON_VALUE(se.EventDetailJSON,'$.c_14144') AS financeEventCode,
				CASE WHEN JSON_VALUE(sd.RegistrantDetailJSON,'$.c_669175') = '' THEN 0 ELSE 1 END AS marketingInclusionFlag,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.c_669175') AS marketingInclusionExplanation,
				CASE 
					WHEN JSON_VALUE(sd.RegistrantDetailJSON,'$.c_630198.value') IN ('Hotel - Approved','Hotel (2nd Attendee) - Approved','Hotel (3rd Attendee) - Approved') 
								AND JSON_VALUE(se.EventDetailJSON,'$.type_id.value') <> 'Complimentary Event'
					THEN 'Invoiced'
					ELSE JSON_VALUE(sd.RegistrantDetailJSON,'$.registration_status') 
				END AS registrationStatus,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.id') AS confirmationNumber,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.first_name') + ' ' + JSON_VALUE(sd.RegistrantDetailJSON,'$.last_name') AS attendingGuestFullName,
				sm.Webboom AS eventType,
				JSON_VALUE(se.EventDetailJSON,'$.start_date') AS eventDate,
				CAST(JSON_VALUE(se.EventDetailJSON,'$.name') AS nvarchar(80)) AS eventLocation,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.created_at') AS timeLoaded,
				NULL AS isBad,NULL AS errorDescription,0 AS [isDeleted],
				JSON_VALUE(sd.RegistrantDetailJSON,'$.reg_type_id.value') AS registrantType
		FROM dbo.Import_SwoogoRegistrantDetail sd
			INNER JOIN dbo.Import_SwoogoEventDetail se ON JSON_VALUE(sd.RegistrantDetailJSON,'$.event_id') = JSON_VALUE(se.EventDetailJSON,'$.id')
														AND se.QueueID = @EventQueueID
			INNER JOIN [core].[dbo].[WebboomSwoogoEventTypeMapping] sm ON JSON_VALUE(se.EventDetailJSON,'$.type_id.value') =sm.swoogo
			LEFT JOIN core.dbo.weboomAccountingRegistrations war ON war.confirmationNumber = JSON_VALUE(sd.RegistrantDetailJSON,'$.id')
			LEFT JOIN #OldInvoiceDueDate old ON old.financeEventCode = JSON_VALUE(se.EventDetailJSON,'$.c_14144')
		WHERE sd.QueueID = @QueueID
			AND JSON_VALUE(se.EventDetailJSON,'$.type_id.value') IS NOT NULL
			AND JSON_VALUE(sd.RegistrantDetailJSON,'$.event_id') NOT IN (27822,27823)
			AND CAST(JSON_VALUE(sd.RegistrantDetailJSON,'$.created_at') AS DATE) >= '2021-01-25'
			-----------------------------------------------------------------------------------
	END
	ELSE IF @app = 'SwoogoRegistrantDetail_FromAzure'
	BEGIN
		-- INSERT INTO Core.Swoogo.weboomAccountingRegistrations ---------------------------
		INSERT INTO Core.Swoogo.weboomAccountingRegistrations([QueueID],[accountCode],[billingAmount],[billingAmountCurrency],[invoiceDueDate],[serviceDate],[financeEventCode],[marketingInclusionFlag],[marketingInclusionExplanation],[registrationStatus],[confirmationNumber],[attendingGuestFullName],[eventType],[eventDate],[eventLocation],[timeLoaded],[isBad],[errorDescription],[isDeleted],[registrantType])
		SELECT DISTINCT @QueueID AS QueueID,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.c_633673') AS accountCode,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.individual_gross') AS billingAmount,
				ISNULL(NULLIF(JSON_VALUE(se.EventDetailJSON,'$.currency'),''),'USD') AS billingAmountCurrency,
				COALESCE(CASE WHEN ISDATE(JSON_VALUE(se.EventDetailJSON,'$.c_40308')) = 0 THEN NULL ELSE JSON_VALUE(se.EventDetailJSON,'$.c_40308') END,war.invoiceDueDate,old.InvoiceDueDate,CAST(getdate() AS DATE)) AS invoiceDueDate,
				JSON_VALUE(se.EventDetailJSON,'$.start_date') AS serviceDate,
				JSON_VALUE(se.EventDetailJSON,'$.c_14144') AS financeEventCode,
				CASE WHEN JSON_VALUE(sd.RegistrantDetailJSON,'$.c_669175') = '' THEN 0 ELSE 1 END AS marketingInclusionFlag,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.c_669175') AS marketingInclusionExplanation,
				CASE 
					WHEN JSON_VALUE(sd.RegistrantDetailJSON,'$.c_630198.value') IN ('Hotel - Approved','Hotel (2nd Attendee) - Approved','Hotel (3rd Attendee) - Approved') 
								AND JSON_VALUE(se.EventDetailJSON,'$.type_id.value') <> 'Complimentary Event'
					THEN 'Invoiced'
					ELSE JSON_VALUE(sd.RegistrantDetailJSON,'$.registration_status') 
				END AS registrationStatus,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.id') AS confirmationNumber,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.first_name') + ' ' + JSON_VALUE(sd.RegistrantDetailJSON,'$.last_name') AS attendingGuestFullName,
				sm.Webboom AS eventType,
				JSON_VALUE(se.EventDetailJSON,'$.start_date') AS eventDate,
				CAST(JSON_VALUE(se.EventDetailJSON,'$.name') AS nvarchar(80)) AS eventLocation,
				JSON_VALUE(sd.RegistrantDetailJSON,'$.created_at') AS timeLoaded,
				NULL AS isBad,NULL AS errorDescription,0 AS [isDeleted],
				JSON_VALUE(sd.RegistrantDetailJSON,'$.reg_type_id.value') AS registrantType
		FROM dbo.Import_SwoogoRegistrantDetail_FromAzure sd
			INNER JOIN dbo.Import_SwoogoEventDetail_FromAzure se ON JSON_VALUE(sd.RegistrantDetailJSON,'$.event_id') = JSON_VALUE(se.EventDetailJSON,'$.id')
														AND se.QueueID = @EventQueueID
			INNER JOIN [core].[dbo].[WebboomSwoogoEventTypeMapping] sm ON JSON_VALUE(se.EventDetailJSON,'$.type_id.value') =sm.swoogo
			LEFT JOIN core.dbo.weboomAccountingRegistrations war ON war.confirmationNumber = JSON_VALUE(sd.RegistrantDetailJSON,'$.id')
			LEFT JOIN #OldInvoiceDueDate old ON old.financeEventCode = JSON_VALUE(se.EventDetailJSON,'$.c_14144')
		WHERE sd.QueueID = @QueueID
			AND JSON_VALUE(se.EventDetailJSON,'$.type_id.value') IS NOT NULL
			AND JSON_VALUE(sd.RegistrantDetailJSON,'$.event_id') NOT IN (27822,27823)
			AND CAST(JSON_VALUE(sd.RegistrantDetailJSON,'$.created_at') AS DATE) >= '2021-01-25'
			-----------------------------------------------------------------------------------
	END
	
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
