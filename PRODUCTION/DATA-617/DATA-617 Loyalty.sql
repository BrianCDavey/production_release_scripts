USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/14/2019 1:22:26 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating schemas'
GO
CREATE SCHEMA [BSI]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [epsilon]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [operations]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Enrollment_Source]'
GO
CREATE TABLE [BSI].[Enrollment_Source]
(
[Enrollment_SourceID] [int] NOT NULL IDENTITY(1, 1),
[Enrollment_SourceName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Enrollment_Source] on [BSI].[Enrollment_Source]'
GO
ALTER TABLE [BSI].[Enrollment_Source] ADD CONSTRAINT [PK_BSI_Enrollment_Source] PRIMARY KEY CLUSTERED  ([Enrollment_SourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_Enrollment_SourceName] on [BSI].[Enrollment_Source]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_Enrollment_SourceName] ON [BSI].[Enrollment_Source] ([Enrollment_SourceName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[MemberProfile]'
GO
CREATE TABLE [BSI].[MemberProfile]
(
[MemberProfileID] [int] NOT NULL IDENTITY(1, 1),
[Enrollment_Date] [date] NULL,
[Enrollment_HotelID] [int] NULL,
[Enrollment_SourceID] [int] NULL,
[GuestID] [int] NOT NULL,
[MemberStatus] [tinyint] NULL,
[MemberStatus_Desc] AS (case [MemberStatus] when (0) then 'Disabled' when (1) then 'Enabled' else 'Unknown' end),
[Disabled_Date] [date] NULL,
[ProfileUpdate_Date] [date] NULL,
[Remarks] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TierID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_MemberProfile] on [BSI].[MemberProfile]'
GO
ALTER TABLE [BSI].[MemberProfile] ADD CONSTRAINT [PK_BSI_MemberProfile] PRIMARY KEY CLUSTERED  ([MemberProfileID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[EmailOptions]'
GO
CREATE TABLE [BSI].[EmailOptions]
(
[EmailOptionsID] [int] NOT NULL IDENTITY(1, 1),
[EmailOptionName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_EmailOptions] on [BSI].[EmailOptions]'
GO
ALTER TABLE [BSI].[EmailOptions] ADD CONSTRAINT [PK_BSI_EmailOptions] PRIMARY KEY CLUSTERED  ([EmailOptionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_EmailOptionName] on [BSI].[EmailOptions]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_EmailOptionName] ON [BSI].[EmailOptions] ([EmailOptionName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[MemberEmailOptions]'
GO
CREATE TABLE [BSI].[MemberEmailOptions]
(
[MemberEmailOptionsID] [int] NOT NULL IDENTITY(1, 1),
[MemberProfileID] [int] NOT NULL,
[EmailOptionsID] [int] NOT NULL,
[OptionStatus] [tinyint] NULL,
[OptionStatus_Desc] AS (case [OptionStatus] when (0) then 'Opt-Out' when (1) then 'Opt-In' else 'Unknown' end)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_MemberEmailOptions] on [BSI].[MemberEmailOptions]'
GO
ALTER TABLE [BSI].[MemberEmailOptions] ADD CONSTRAINT [PK_BSI_MemberEmailOptions] PRIMARY KEY CLUSTERED  ([MemberEmailOptionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[LoyaltyNumber]'
GO
CREATE TABLE [BSI].[LoyaltyNumber]
(
[LoyaltyNumberID] [int] NOT NULL IDENTITY(1, 1),
[LoyaltyNumberName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_LoyaltyNumber] on [BSI].[LoyaltyNumber]'
GO
ALTER TABLE [BSI].[LoyaltyNumber] ADD CONSTRAINT [PK_BSI_LoyaltyNumber] PRIMARY KEY CLUSTERED  ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_LoyaltyNumber_LoyaltyNumberName] on [BSI].[LoyaltyNumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_LoyaltyNumber_LoyaltyNumberName] ON [BSI].[LoyaltyNumber] ([LoyaltyNumberName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Tier]'
GO
CREATE TABLE [BSI].[Tier]
(
[TierID] [int] NOT NULL IDENTITY(1, 1),
[TierName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Tier] on [BSI].[Tier]'
GO
ALTER TABLE [BSI].[Tier] ADD CONSTRAINT [PK_BSI_Tier] PRIMARY KEY CLUSTERED  ([TierID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_TierName] on [BSI].[Tier]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_TierName] ON [BSI].[Tier] ([TierName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Transactions]'
GO
CREATE TABLE [BSI].[Transactions]
(
[TransactionsID] [int] NOT NULL IDENTITY(1, 1),
[TDR_Transaction_ID] [bigint] NOT NULL,
[Remarks] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Points_Earned] [decimal] (28, 2) NULL,
[Points_Redemeed] [decimal] (28, 2) NULL,
[Currency_Code] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transaction_Date] [date] NULL,
[Reward_Posting_Date] [date] NULL,
[Value_of_Redemption_USD] [decimal] (28, 2) NULL,
[TransactionSourceID] [int] NULL,
[MemberStatus] [tinyint] NULL,
[MemberStatus_DESC] AS (case [MemberStatus] when (0) then 'Disabled' when (1) then 'Enabled' when (2) then 'C' when (3) then 'W' when (4) then 'B' end),
[CampaignID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Transactions] on [BSI].[Transactions]'
GO
ALTER TABLE [BSI].[Transactions] ADD CONSTRAINT [PK_BSI_Transactions] PRIMARY KEY CLUSTERED  ([TransactionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Reservations]'
GO
CREATE TABLE [BSI].[Reservations]
(
[ReservationsID] [int] NOT NULL IDENTITY(1, 1),
[TransactionsID] [int] NOT NULL,
[Booking_ID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Hotel_Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Arrival_Date] [date] NULL,
[Departure_Date] [date] NULL,
[Booking_Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reservation_Revenue] [decimal] (28, 2) NULL,
[Hotel_Brand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Amount_Spent_USD] [decimal] (28, 2) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Reservations] on [BSI].[Reservations]'
GO
ALTER TABLE [BSI].[Reservations] ADD CONSTRAINT [PK_BSI_Reservations] PRIMARY KEY CLUSTERED  ([ReservationsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_Reservations_TransactionsID] on [BSI].[Reservations]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_Reservations_TransactionsID] ON [BSI].[Reservations] ([TransactionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[RewardActivity]'
GO
CREATE TABLE [BSI].[RewardActivity]
(
[RewardActivityID] [int] NOT NULL IDENTITY(1, 1),
[RedemptionDate] [date] NOT NULL,
[VoucherTypeID] [int] NULL,
[VoucherID] [int] NULL,
[VoucherValue] [decimal] (38, 2) NULL,
[PayableValue_USD] [decimal] (38, 2) NULL,
[VoucherCurrency] [nvarchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VoucherCurrency_Date] [date] NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_RewardActivity] on [BSI].[RewardActivity]'
GO
ALTER TABLE [BSI].[RewardActivity] ADD CONSTRAINT [PK_BSI_RewardActivity] PRIMARY KEY CLUSTERED  ([RewardActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Campaign]'
GO
CREATE TABLE [BSI].[Campaign]
(
[CampaignID] [int] NOT NULL IDENTITY(1, 1),
[CampaignName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Campaign] on [BSI].[Campaign]'
GO
ALTER TABLE [BSI].[Campaign] ADD CONSTRAINT [PK_BSI_Campaign] PRIMARY KEY CLUSTERED  ([CampaignID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_Campaign_CampaignName] on [BSI].[Campaign]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_Campaign_CampaignName] ON [BSI].[Campaign] ([CampaignName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[TransactionSource]'
GO
CREATE TABLE [BSI].[TransactionSource]
(
[TransactionSourceID] [int] NOT NULL IDENTITY(1, 1),
[TransactionSourceName] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_TransactionSource] on [BSI].[TransactionSource]'
GO
ALTER TABLE [BSI].[TransactionSource] ADD CONSTRAINT [PK_BSI_TransactionSource] PRIMARY KEY CLUSTERED  ([TransactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_TransactionSource_TransactionSourceName] on [BSI].[TransactionSource]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_TransactionSource_TransactionSourceName] ON [BSI].[TransactionSource] ([TransactionSourceName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EmailOptions]'
GO
CREATE TABLE [dbo].[EmailOptions]
(
[EmailOptionsID] [int] NOT NULL IDENTITY(1, 1),
[EmailOptionsName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_EmailOptions] on [dbo].[EmailOptions]'
GO
ALTER TABLE [dbo].[EmailOptions] ADD CONSTRAINT [PK_dbo_EmailOptions] PRIMARY KEY CLUSTERED  ([EmailOptionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MemberEmailOptions]'
GO
CREATE TABLE [dbo].[MemberEmailOptions]
(
[MemberEmailOptionsID] [int] NOT NULL IDENTITY(1, 1),
[MemberProfileID] [int] NOT NULL,
[EmailOptionsID] [int] NOT NULL,
[OptionStatus] [tinyint] NULL,
[OptionStatus_Desc] AS (case [OptionStatus] when (0) then 'Opt-Out' when (1) then 'Opt-In' else 'Unknown' end),
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_MemberEmailOptions] on [dbo].[MemberEmailOptions]'
GO
ALTER TABLE [dbo].[MemberEmailOptions] ADD CONSTRAINT [PK_dbo_MemberEmailOptions] PRIMARY KEY CLUSTERED  ([MemberEmailOptionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MemberProfile]'
GO
CREATE TABLE [dbo].[MemberProfile]
(
[MemberProfileID] [int] NOT NULL IDENTITY(1, 1),
[GuestID] [int] NOT NULL,
[TierId] [int] NULL,
[Enrollment_Date] [date] NULL,
[Enrollment_SourceID] [int] NULL,
[Enrollment_PromotionID] [int] NULL,
[Enrollment_HotelID] [int] NULL,
[MemberTypeID] [int] NULL,
[IsEmailValidated] [bit] NULL,
[TravelAgencyID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL,
[External_SourceKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberStatus] [tinyint] NULL,
[MemberStatus_Desc] AS (case [MemberStatus] when (0) then 'Disabled' when (1) then 'Enabled' else 'Unknown' end),
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_MemberProfile] on [dbo].[MemberProfile]'
GO
ALTER TABLE [dbo].[MemberProfile] ADD CONSTRAINT [PK_dbo_MemberProfile] PRIMARY KEY CLUSTERED  ([MemberProfileID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoyaltyNumber]'
GO
CREATE TABLE [dbo].[LoyaltyNumber]
(
[LoyaltyNumberID] [int] NOT NULL IDENTITY(1, 1),
[LoyaltyNumberName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_LoyaltyNumber] on [dbo].[LoyaltyNumber]'
GO
ALTER TABLE [dbo].[LoyaltyNumber] ADD CONSTRAINT [PK_dbo_LoyaltyNumber] PRIMARY KEY CLUSTERED  ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_dbo_LoyaltyNumber_LoyaltyNumberName] on [dbo].[LoyaltyNumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_dbo_LoyaltyNumber_LoyaltyNumberName] ON [dbo].[LoyaltyNumber] ([LoyaltyNumberName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PointActivity]'
GO
CREATE TABLE [dbo].[PointActivity]
(
[PointActivityID] [int] NOT NULL IDENTITY(1, 1),
[ActivityDate] [datetime] NULL,
[ActivityTypeID] [int] NULL,
[PointTypeID] [int] NULL,
[ActivityCauseSystemID] [int] NOT NULL,
[ActivityCauseID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityCauseUserID] [int] NULL,
[ActivityCauseAmount] [decimal] (38, 5) NULL,
[ActivityCauseCurrency] [nchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityCauseDate] [datetime] NULL,
[CurrencyExchangeDate] [datetime] NULL,
[CurrencyExchangeRate] [decimal] (38, 9) NULL,
[Points] [decimal] (38, 5) NULL,
[PointLiabilityUSD] [decimal] (38, 2) NULL,
[Notes] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberID] [int] NOT NULL,
[DataSourceID] [int] NOT NULL,
[Internal_SourceKey] [int] NULL,
[External_SourceKey] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_PointActivity] on [dbo].[PointActivity]'
GO
ALTER TABLE [dbo].[PointActivity] ADD CONSTRAINT [PK_dbo_PointActivity] PRIMARY KEY CLUSTERED  ([PointActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RewardActivity]'
GO
CREATE TABLE [dbo].[RewardActivity]
(
[RewardActivityID] [int] NOT NULL IDENTITY(1, 1),
[MemberRewardNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardStatusID] [int] NULL,
[CreateDate] [datetime] NULL,
[LastUpdated] [datetime] NULL,
[UserUpdatedID] [int] NULL,
[RewardTypeID] [int] NULL,
[RewardManufacturerID] [int] NULL,
[RewardID] [int] NULL,
[RewardCost] [decimal] (38, 2) NULL,
[RewardCurrency] [nchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardValueUSD] [decimal] (38, 2) NULL,
[CurrencyExchangeDate] [datetime] NULL,
[CurrencyExchangeRate] [decimal] (38, 9) NULL,
[PointValue] [decimal] (38, 5) NULL,
[Notes] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberID] [int] NOT NULL,
[DataSourceID] [int] NOT NULL,
[Internal_SourceKey] [int] NULL,
[External_SourceKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RewardActivity] on [dbo].[RewardActivity]'
GO
ALTER TABLE [dbo].[RewardActivity] ADD CONSTRAINT [PK_dbo_RewardActivity] PRIMARY KEY CLUSTERED  ([RewardActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Enrollment_Promotion]'
GO
CREATE TABLE [epsilon].[Enrollment_Promotion]
(
[Enrollment_PromotionID] [int] NOT NULL IDENTITY(1, 1),
[Enrollment_PromotionName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Enrollment_Promotion] on [epsilon].[Enrollment_Promotion]'
GO
ALTER TABLE [epsilon].[Enrollment_Promotion] ADD CONSTRAINT [PK_epsilon_Enrollment_Promotion] PRIMARY KEY CLUSTERED  ([Enrollment_PromotionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[MemberProfile]'
GO
CREATE TABLE [epsilon].[MemberProfile]
(
[MemberProfileID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NOT NULL,
[SystemProfileID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GuestID] [int] NOT NULL,
[TierId] [int] NULL,
[Enrollment_Date] [date] NULL,
[Enrollment_SourceID] [int] NULL,
[Enrollment_PromotionID] [int] NULL,
[Enrollment_HotelID] [int] NULL,
[MemberTypeID] [int] NULL,
[IsEmailValidated] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgencyID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_MemberProfile] on [epsilon].[MemberProfile]'
GO
ALTER TABLE [epsilon].[MemberProfile] ADD CONSTRAINT [PK_epsilon_MemberProfile] PRIMARY KEY CLUSTERED  ([MemberProfileID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Enrollment_Source]'
GO
CREATE TABLE [epsilon].[Enrollment_Source]
(
[Enrollment_SourceID] [int] NOT NULL IDENTITY(1, 1),
[Enrollment_SourceName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Enrollment_Source] on [epsilon].[Enrollment_Source]'
GO
ALTER TABLE [epsilon].[Enrollment_Source] ADD CONSTRAINT [PK_epsilon_Enrollment_Source] PRIMARY KEY CLUSTERED  ([Enrollment_SourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[EmailOptions]'
GO
CREATE TABLE [epsilon].[EmailOptions]
(
[EmailOptionsID] [int] NOT NULL IDENTITY(1, 1),
[EmailOptionsName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_EmailOptions] on [epsilon].[EmailOptions]'
GO
ALTER TABLE [epsilon].[EmailOptions] ADD CONSTRAINT [PK_epsilon_EmailOptions] PRIMARY KEY CLUSTERED  ([EmailOptionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[MemberEmailOptions]'
GO
CREATE TABLE [epsilon].[MemberEmailOptions]
(
[MemberEmailOptionsID] [int] NOT NULL IDENTITY(1, 1),
[MemberProfileID] [int] NOT NULL,
[EmailOptionsID] [int] NOT NULL,
[OptionStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionStatus_Desc] AS (case [OptionStatus] when 'N' then 'Opt-Out' when 'Y' then 'Opt-In' else 'Unknown' end)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_MemberEmailOptions] on [epsilon].[MemberEmailOptions]'
GO
ALTER TABLE [epsilon].[MemberEmailOptions] ADD CONSTRAINT [PK_epsilon_MemberEmailOptions] PRIMARY KEY CLUSTERED  ([MemberEmailOptionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[MemberType]'
GO
CREATE TABLE [epsilon].[MemberType]
(
[MemberTypeID] [int] NOT NULL IDENTITY(1, 1),
[MemberTypeName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_MemberType] on [epsilon].[MemberType]'
GO
ALTER TABLE [epsilon].[MemberType] ADD CONSTRAINT [PK_epsilon_MemberType] PRIMARY KEY CLUSTERED  ([MemberTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Tier]'
GO
CREATE TABLE [epsilon].[Tier]
(
[TierID] [int] NOT NULL IDENTITY(1, 1),
[TierName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Tier] on [epsilon].[Tier]'
GO
ALTER TABLE [epsilon].[Tier] ADD CONSTRAINT [PK_epsilon_Tier] PRIMARY KEY CLUSTERED  ([TierID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[TravelAgency]'
GO
CREATE TABLE [epsilon].[TravelAgency]
(
[TravelAgencyID] [int] NOT NULL IDENTITY(1, 1),
[IataID] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgencyName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_TravelAgency] on [epsilon].[TravelAgency]'
GO
ALTER TABLE [epsilon].[TravelAgency] ADD CONSTRAINT [PK_epsilon_TravelAgency] PRIMARY KEY CLUSTERED  ([TravelAgencyID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[ActivityCauseSystem]'
GO
CREATE TABLE [epsilon].[ActivityCauseSystem]
(
[ActivityCauseSystemID] [int] NOT NULL IDENTITY(1, 1),
[ActivityCauseSystemName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_ActivityCauseSystem] on [epsilon].[ActivityCauseSystem]'
GO
ALTER TABLE [epsilon].[ActivityCauseSystem] ADD CONSTRAINT [PK_epsilon_ActivityCauseSystem] PRIMARY KEY CLUSTERED  ([ActivityCauseSystemID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_ActivityCauseSystem]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_ActivityCauseSystem] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_ActivityCauseSystem]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.ActivityCauseSystem AS tgt
	USING
	(
		SELECT DISTINCT TXN_SOURCE_CODE FROM ETL.dbo.Import_EpsilonPoint 
		WHERE QueueID = @QueueID
	) AS src ON src.TXN_SOURCE_CODE = tgt.ActivityCauseSystemName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(ActivityCauseSystemName)
		VALUES(src.TXN_SOURCE_CODE)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[ActivityType]'
GO
CREATE TABLE [epsilon].[ActivityType]
(
[ActivityTypeID] [int] NOT NULL IDENTITY(1, 1),
[ActivityTypeName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_ActivityType] on [epsilon].[ActivityType]'
GO
ALTER TABLE [epsilon].[ActivityType] ADD CONSTRAINT [PK_epsilon_ActivityType] PRIMARY KEY CLUSTERED  ([ActivityTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_ActivityType]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_ActivityType] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_ActivityType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.ActivityType AS tgt
	USING
	(
		SELECT DISTINCT DISPLAY_POINT_TYPE FROM ETL.dbo.Import_EpsilonPoint 
		WHERE QueueID = @QueueID
	) AS src ON src.DISPLAY_POINT_TYPE = tgt.ActivityTypeName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(ActivityTypeName)
		VALUES(src.DISPLAY_POINT_TYPE)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[UserUpdated]'
GO
CREATE TABLE [epsilon].[UserUpdated]
(
[UserUpdatedID] [int] NOT NULL IDENTITY(1, 1),
[UserUpdatedName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_UserUpdated] on [epsilon].[UserUpdated]'
GO
ALTER TABLE [epsilon].[UserUpdated] ADD CONSTRAINT [PK_epsilon_UserUpdated] PRIMARY KEY CLUSTERED  ([UserUpdatedID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[PointType]'
GO
CREATE TABLE [epsilon].[PointType]
(
[PointTypeID] [int] NOT NULL IDENTITY(1, 1),
[PointTypeName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_PointType] on [epsilon].[PointType]'
GO
ALTER TABLE [epsilon].[PointType] ADD CONSTRAINT [PK_epsilon_PointType] PRIMARY KEY CLUSTERED  ([PointTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[PointActivity]'
GO
CREATE TABLE [epsilon].[PointActivity]
(
[PointActivityID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NOT NULL,
[SystemPointActivityID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityTypeID] [int] NULL,
[PointTypeID] [int] NULL,
[UpdateUser] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityCauseSystemID] [int] NOT NULL,
[ActivityCauseID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityCauseUserID] [int] NULL,
[ActivityCauseAmount] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityCauseCurrency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActivityCauseDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeRate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Points] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PointLiabilityUSD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_PointActivity] on [epsilon].[PointActivity]'
GO
ALTER TABLE [epsilon].[PointActivity] ADD CONSTRAINT [PK_epsilon_PointActivity] PRIMARY KEY CLUSTERED  ([PointActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[LoyaltyNumber]'
GO
CREATE TABLE [epsilon].[LoyaltyNumber]
(
[LoyaltyNumberID] [int] NOT NULL IDENTITY(1, 1),
[LoyaltyNumberName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_LoyaltyNumber] on [epsilon].[LoyaltyNumber]'
GO
ALTER TABLE [epsilon].[LoyaltyNumber] ADD CONSTRAINT [PK_epsilon_LoyaltyNumber] PRIMARY KEY CLUSTERED  ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_PointActivity]'
GO








-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  iep.ACT_POINT_ID AS [SystemPointActivityID]
		, iep.QueueID AS QueueID
		, iep.CREATE_DATE AS [ActivityDate]
		, ac.[ActivityTypeID] AS [ActivityTypeID]
		, pt.[PointTypeID] AS [PointTypeID]
		, iep.Update_User AS [UpdateUser]
		, acs.[ActivityCauseSystemID] AS [ActivityCauseSystemID]
		, COALESCE(NULLIF(iep.ACT_TRANSACTION_ID,''),NULLIF(iep.ACT_ADJUSTMENT_ID,''),NULLIF(iep.ACT_ACTIVITY_ID,''),iep.ACT_ORDER_REWARD_ID) AS [ActivityCauseID]
		, iep.[Transaction_Number] AS [TransactionNumber]
		, uu.[UserUpdatedID] AS [ActivityCauseUserID]
		, iep.TRANSACTION_NET_AMOUNT AS [ActivityCauseAmount]
		, iep.CURRENCY_CODE AS [ActivityCauseCurrency]
		, iep.TRANSACTION_DATE AS [ActivityCauseDate]
		, iep.CURRENCY_EXCHANGE_DATE AS [CurrencyExchangeDate]
		, iep.CURRENCY_EXCHANGE_RATE AS [CurrencyExchangeRate]
		, iep.NUM_POINTS AS [Points]
		, iep.USD_PTS_LIABILITY AS [PointLiabilityUSD]
		, iep.TRANS_DESCRIPTION AS [Notes]
		, ln.LoyaltyNumberID
		FROM ETL.dbo.Import_EpsilonPoint iep
		INNER JOIN epsilon.LoyaltyNumber ln ON ln.LoyaltyNumberName = iep.Card_Number
		LEFT JOIN epsilon.ActivityType ac ON iep.DISPLAY_POINT_TYPE  = ac.ActivityTypeName
		LEFT JOIN epsilon.PointType pt ON pt.PointTypeName  = iep.SHORT_DESC
		LEFT JOIN epsilon.ActivityCauseSystem acs ON acs.ActivityCauseSystemName  = iep.TXN_SOURCE_CODE
		LEFT JOIN epsilon.UserUpdated uu ON uu.UserUpdatedName = iep.CREATE_USER
		WHERE iep.QueueID = @QueueID
	) AS src ON 
	src.[SystemPointActivityID] = tgt.[SystemPointActivityID]
	AND src.[ActivityDate] = tgt.[ActivityDate]
	AND src.[ActivityTypeID] = tgt.[ActivityTypeID]
	AND src.[PointTypeID] = tgt.[PointTypeID]
	AND src.[UpdateUser] = tgt.[UpdateUser]
	AND src.[ActivityCauseSystemID] = tgt.[ActivityCauseSystemID]
	AND src.[ActivityCauseID] = tgt.[ActivityCauseID]
	AND src.[TransactionNumber] = tgt.[TransactionNumber]
	AND src.[ActivityCauseUserID] = tgt.[ActivityCauseUserID]
	AND src.[ActivityCauseAmount] = tgt.[ActivityCauseAmount]
	AND src.[ActivityCauseCurrency] = tgt.[ActivityCauseCurrency]
	AND src.[ActivityCauseDate] = tgt.[ActivityCauseDate]
	AND src.[CurrencyExchangeDate] = tgt.[CurrencyExchangeDate]
	AND src.[CurrencyExchangeRate] = tgt.[CurrencyExchangeRate]
	AND src.[Points] = tgt.[Points]
	AND src.[PointLiabilityUSD] = tgt.[PointLiabilityUSD]
	AND src.[Notes] = tgt.[Notes]
	AND src.LoyaltyNumberID = tgt.LoyaltyNumberID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(	[SystemPointActivityID]
		,QueueID
	,[ActivityDate]
	,[ActivityTypeID]
	,[PointTypeID]
	,[UpdateUser]
	,[ActivityCauseSystemID]
	,[ActivityCauseID]
	,[TransactionNumber]
	,[ActivityCauseUserID]
	,[ActivityCauseAmount]
	,[ActivityCauseCurrency]
	,[ActivityCauseDate]
	,[CurrencyExchangeDate]
	,[CurrencyExchangeRate]
	,[Points]
	,[PointLiabilityUSD]
	,[Notes]
	,LoyaltyNumberID)
		VALUES([SystemPointActivityID]
		,QueueID
	,[ActivityDate]
	,[ActivityTypeID]
	,[PointTypeID]
	,[UpdateUser]
	,[ActivityCauseSystemID]
	,[ActivityCauseID]
	,[TransactionNumber]
	,[ActivityCauseUserID]
	,[ActivityCauseAmount]
	,[ActivityCauseCurrency]
	,[ActivityCauseDate]
	,[CurrencyExchangeDate]
	,[CurrencyExchangeRate]
	,[Points]
	,[PointLiabilityUSD]
	,[Notes]
	,LoyaltyNumberID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_PointType]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_PointType] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_PointType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.PointType AS tgt
	USING
	(
		SELECT DISTINCT SHORT_DESC FROM ETL.dbo.Import_EpsilonPoint 
		WHERE QueueID = @QueueID
	) AS src ON src.SHORT_DESC = tgt.PointTypeName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(PointTypeName)
		VALUES(src.SHORT_DESC)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_UserUpdated]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_UserUpdated] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_UserUpdated]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.UserUpdated AS tgt
	USING
	(
		SELECT DISTINCT UPDATE_USER AS UPDATE_USER FROM ETL.dbo.Import_EpsilonRedemption 
		WHERE QueueID = @QueueID
		UNION
		SELECT DISTINCT CREATE_USER AS UPDATE_USER FROM ETL.dbo.Import_EpsilonPoint 
		WHERE QueueID = @QueueID
		UNION
		SELECT DISTINCT UPDATE_USER FROM ETL.dbo.Import_EpsilonReward 
		WHERE QueueID = @QueueID

	) AS src ON src.UPDATE_USER = tgt.UserUpdatedName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(UserUpdatedName)
		VALUES(src.UPDATE_USER)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_LoyaltyNumber]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_LoyaltyNumber] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================

CREATE PROCEDURE [epsilon].[Populate_LoyaltyNumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.LoyaltyNumber AS tgt
	USING
	(
		SELECT LoyaltyNumber
		FROM
		(
			SELECT DISTINCT CARD_NUMBER AS LoyaltyNumber FROM ETL.[dbo].[Import_EpsilonMember] WHERE QueueID = @QueueID
				UNION
			SELECT DISTINCT CARD_NUMBER AS LoyaltyNumber FROM ETL.[dbo].[Import_EpsilonReward] WHERE QueueID = @QueueID
				UNION
			SELECT DISTINCT CARD_NUMBER AS LoyaltyNumber FROM ETL.[dbo].[Import_EpsilonPoint] WHERE QueueID = @QueueID
				UNION
			SELECT DISTINCT CARD_NUMBER AS LoyaltyNumber FROM ETL.[dbo].[Import_EpsilonRedemption] WHERE QueueID = @QueueID
		) x
	)AS src ON src.LoyaltyNumber = tgt.LoyaltyNumberName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(LoyaltyNumberName)
		VALUES(LoyaltyNumber)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Point]'
GO










-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Point] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [epsilon].[Populate_Point]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
		-- Loyalty.epsilon.Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.epsilon.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.epsilon.Populate_LoyaltyNumber @QueueID
	---------------------------------------------------------------------------


		-- epsilon.Populate_ActivityType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_ActivityType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_ActivityType @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_PointType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_PointType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_PointType @QueueID
	--------------------------------------------------------------------------
	
	-- epsilon.Populate_ActivityCauseSystem ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_ActivityCauseSystem'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_ActivityCauseSystem @QueueID
	----------------------------------------------------------------------------


	-- epsilon.Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_UserUpdated @QueueID
	----------------------------------------------------------------------------

	-- epsilon.Populate_PointActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_PointActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_PointActivity @QueueID
	----------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[RewardType]'
GO
CREATE TABLE [epsilon].[RewardType]
(
[RewardTypeID] [int] NOT NULL IDENTITY(1, 1),
[RewardTypeName] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_RewardType] on [epsilon].[RewardType]'
GO
ALTER TABLE [epsilon].[RewardType] ADD CONSTRAINT [PK_epsilon_RewardType] PRIMARY KEY CLUSTERED  ([RewardTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[RewardStatus]'
GO
CREATE TABLE [epsilon].[RewardStatus]
(
[RewardStatusID] [int] NOT NULL IDENTITY(1, 1),
[RewardStatusName] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardStatusdescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_RewardStatus] on [epsilon].[RewardStatus]'
GO
ALTER TABLE [epsilon].[RewardStatus] ADD CONSTRAINT [PK_epsilon_RewardStatus] PRIMARY KEY CLUSTERED  ([RewardStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[RewardManufacturer]'
GO
CREATE TABLE [epsilon].[RewardManufacturer]
(
[RewardManufacturerID] [int] NOT NULL IDENTITY(1, 1),
[RewardManufacturerName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_RewardManufacturer] on [epsilon].[RewardManufacturer]'
GO
ALTER TABLE [epsilon].[RewardManufacturer] ADD CONSTRAINT [PK_epsilon_RewardManufacturer] PRIMARY KEY CLUSTERED  ([RewardManufacturerID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[RewardActivity]'
GO
CREATE TABLE [epsilon].[RewardActivity]
(
[RewardActivityID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NOT NULL,
[SystemActivityID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActOrderDetailID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActRewardCertID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberRewardNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardStatusID] [int] NULL,
[CreateDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdated] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserUpdatedID] [int] NULL,
[RewardTypeID] [int] NULL,
[RewardManufacturerID] [int] NULL,
[RewardID] [int] NULL,
[RewardCost] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardCurrency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardValueUSD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrencyExchangeRate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PointValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_RewardActivity] on [epsilon].[RewardActivity]'
GO
ALTER TABLE [epsilon].[RewardActivity] ADD CONSTRAINT [PK_epsilon_RewardActivity] PRIMARY KEY CLUSTERED  ([RewardActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Reward]'
GO
CREATE TABLE [epsilon].[Reward]
(
[RewardID] [int] NOT NULL IDENTITY(1, 1),
[RewardName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Reward] on [epsilon].[Reward]'
GO
ALTER TABLE [epsilon].[Reward] ADD CONSTRAINT [PK_epsilon_Reward] PRIMARY KEY CLUSTERED  ([RewardID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_RewardActivity]'
GO








-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_RewardActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_RewardActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.RewardActivity AS tgt
	USING
	(
		SELECT DISTINCT 
				ier.ACT_ORDER_REWARD_ID	AS SystemActivityID
				, ier.QueueID AS QueueID
				,ier.ACT_ORDER_DETAIL_ID  AS [ActOrderDetailID]
				,ier.ACT_REWARD_CERT_ID  AS [ActRewardCertID]
				, ier.CERT_NUMBER  AS [MemberRewardNumber]
				, rs.[RewardStatusID]  AS [RewardStatusID]
				, ier.CREATE_DATE  AS [CreateDate]
				, ier.UPDATE_DATE  AS [LastUpdated]
				, uu.[UserUpdatedID]  AS [UserUpdatedID]
				, rt.[RewardTypeID]  AS [RewardTypeID]
				, rm.[RewardManufacturerID]  AS [RewardManufacturerID]
				, r.[RewardID]  AS [RewardID]
				, ier.RETAIL_COST  AS [RewardCost]
				, ier.CURRENCY_CODE  AS [RewardCurrency]
				, ier.USD_REWARD_VALUE  AS [RewardValueUSD]
				, ier.CURRENCY_EXCHANGE_DATE  AS [CurrencyExchangeDate]
				, ier.CURRENCY_EXCHANGE_RATE  AS [CurrencyExchangeRate]
				, ier.NUM_POINTS  AS [PointValue]
				, ier.TRANS_DESCRIPTION  AS [Notes]
				, ln.loyaltynumberID 
		FROM ETL.dbo.Import_EpsilonReward ier
		INNER JOIN epsilon.loyaltynumber ln ON ln.loyaltynumberName = ier.Card_Number
		LEFT JOIN epsilon.RewardStatus rs ON ier.ORDER_DETAIL_STATUS  = rs.RewardStatusName
		LEFT JOIN epsilon.UserUpdated uu ON uu.UserUpdatedName  = ier.UPDATE_USER
		LEFT JOIN epsilon.RewardType rt ON rt.RewardTypeName = ier.REWARD_TYPE
		LEFT JOIN epsilon.RewardManufacturer rm ON rm.RewardManufacturerName  = ier.MANUFACTURER
		LEFT JOIN epsilon.Reward r ON r.RewardName  = ier.title
		WHERE ier.QueueID = @QueueID
	) AS src ON 
					src.[SystemActivityID]	= tgt.[SystemActivityID]
				AND src.[ActOrderDetailID]  = tgt.[ActOrderDetailID]
				AND src.[ActRewardCertID]  = tgt.[ActRewardCertID]
				AND src.[MemberRewardNumber]  = tgt.[MemberRewardNumber]
				AND src.[RewardStatusID]  = tgt.[RewardStatusID]
				AND src.[CreateDate]  = tgt.[CreateDate]
				AND src.[LastUpdated]  = tgt.[LastUpdated]
				AND src.[UserUpdatedID]  = tgt.[UserUpdatedID]
				AND src.[RewardTypeID]  = tgt.[RewardTypeID]
				AND src.[RewardManufacturerID]  = tgt.[RewardManufacturerID]
				AND src.[RewardID]  = tgt.[RewardID]
				AND src.[RewardCost]  = tgt.[RewardCost]
				AND src.[RewardCurrency]  = tgt.[RewardCurrency]
				AND src.[RewardValueUSD]  = tgt.[RewardValueUSD]
				AND src.[CurrencyExchangeDate]  = tgt.[CurrencyExchangeDate]
				AND src.[CurrencyExchangeRate]  = tgt.[CurrencyExchangeRate]
				AND src.[PointValue]  = tgt.[PointValue]
				AND src.[Notes]  = tgt.[Notes]
				AND src.loyaltynumberID  = tgt.loyaltynumberID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(	[SystemActivityID]
				,QueueID
				,[ActOrderDetailID]
				,[ActRewardCertID]
				,[MemberRewardNumber]
				,[RewardStatusID]
				,[CreateDate]
				,[LastUpdated]
				,[UserUpdatedID]
				,[RewardTypeID]
				,[RewardManufacturerID]
				,[RewardID]
				,[RewardCost]
				,[RewardCurrency]
				,[RewardValueUSD]
				,[CurrencyExchangeDate]
				,[CurrencyExchangeRate]
				,[PointValue]
				,[Notes]
				,loyaltynumberID)
		VALUES([SystemActivityID]
				,QueueID
				,[ActOrderDetailID]
				,[ActRewardCertID]
				,[MemberRewardNumber]
				,[RewardStatusID]
				,[CreateDate]
				,[LastUpdated]
				,[UserUpdatedID]
				,[RewardTypeID]
				,[RewardManufacturerID]
				,[RewardID]
				,[RewardCost]
				,[RewardCurrency]
				,[RewardValueUSD]
				,[CurrencyExchangeDate]
				,[CurrencyExchangeRate]
				,[PointValue]
				,[Notes]
				,loyaltynumberID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_RewardStatus]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_RewardStatus] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_RewardStatus]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.RewardStatus AS tgt
	USING
	(
		SELECT DISTINCT ORDER_DETAIL_STATUS FROM ETL.dbo.Import_EpsilonReward 
		WHERE QueueID = @QueueID
	) AS src ON src.ORDER_DETAIL_STATUS = tgt.RewardStatusName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(RewardStatusName)
		VALUES(src.ORDER_DETAIL_STATUS)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_RewardType]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_RewardType] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_RewardType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.RewardType AS tgt
	USING
	(
		SELECT DISTINCT Reward_Type FROM ETL.dbo.Import_EpsilonRedemption 
		WHERE QueueID = @QueueID
		UNION 
		SELECT DISTINCT REWARD_TYPE FROM ETL.dbo.Import_EpsilonReward 
		WHERE QueueID = @QueueID
	) AS src ON src.Reward_Type = tgt.RewardTypeName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(RewardTypeName)
		VALUES(src.Reward_Type)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_RewardManufacturer]'
GO






-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_RewardManufacturer] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_RewardManufacturer]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.RewardManufacturer AS tgt
	USING
	(
		SELECT DISTINCT MANUFACTURER FROM ETL.dbo.Import_EpsilonReward 
		WHERE QueueID = @QueueID
	) AS src ON src.MANUFACTURER = tgt.RewardManufacturerName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(RewardManufacturerName)
		VALUES(src.MANUFACTURER)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Reward]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Reward] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_Reward]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.Reward AS tgt
	USING
	(
		SELECT DISTINCT title FROM ETL.dbo.Import_EpsilonRedemption 
		WHERE QueueID = @QueueID
		UNION
		SELECT DISTINCT TITLE FROM ETL.dbo.Import_EpsilonReward 
		WHERE QueueID = @QueueID
	) AS src ON src.title = tgt.RewardName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(RewardName)
		VALUES(src.title)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_RewardOrderActivity]'
GO












-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_RewardOrderActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [epsilon].[Populate_RewardOrderActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	

		-- Loyalty.epsilon.Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.epsilon.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.epsilon.Populate_LoyaltyNumber @QueueID
	---------------------------------------------------------------------------


		-- epsilon.Populate_RewardStatus ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_RewardStatus'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_RewardStatus @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_UserUpdated @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_RewardType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_RewardType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_RewardType @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_RewardManufacturer ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_RewardManufacturer'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_RewardManufacturer @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_Reward ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_Reward'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_Reward @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_RewardActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_RewardActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_RewardActivity @QueueID
	---------------------------------------------------------------------------



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[VoucherType]'
GO
CREATE TABLE [BSI].[VoucherType]
(
[VoucherTypeID] [int] NOT NULL IDENTITY(1, 1),
[VoucherTypeName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_VoucherType] on [BSI].[VoucherType]'
GO
ALTER TABLE [BSI].[VoucherType] ADD CONSTRAINT [PK_BSI_VoucherType] PRIMARY KEY CLUSTERED  ([VoucherTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_VoucherType_VoucherTypeName] on [BSI].[VoucherType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_VoucherType_VoucherTypeName] ON [BSI].[VoucherType] ([VoucherTypeName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_VoucherType]'
GO

CREATE   PROCEDURE [BSI].[Populate_VoucherType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.VoucherType AS tgt
	USING
	(
		SELECT DISTINCT [Voucher_Type] FROM ETL.dbo.Import_IPHotelRedemption
			UNION
		SELECT DISTINCT [Voucher_Type] FROM Superset.BSI.Reward_Certificate_Liability_Report_Import
	) AS src ON src.[Voucher_Type] = tgt.VoucherTypeName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(VoucherTypeName)
		VALUES(src.[Voucher_Type])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Voucher]'
GO
CREATE TABLE [BSI].[Voucher]
(
[VoucherID] [int] NOT NULL IDENTITY(1, 1),
[VoucherNumber] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VoucherName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Voucher] on [BSI].[Voucher]'
GO
ALTER TABLE [BSI].[Voucher] ADD CONSTRAINT [PK_BSI_Voucher] PRIMARY KEY CLUSTERED  ([VoucherID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_Voucher_VoucherNumber] on [BSI].[Voucher]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_Voucher_VoucherNumber] ON [BSI].[Voucher] ([VoucherNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Voucher]'
GO

CREATE   PROCEDURE [BSI].[Populate_Voucher]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.Voucher AS tgt
	USING
	(
		SELECT [Voucher_Number],MAX([Voucher_Name]) AS [Voucher_Name]
		FROM ETL.dbo.Import_IPHotelRedemption
		GROUP BY [Voucher_Number]
			UNION
		SELECT [Voucher_Number],MAX([Voucher_Name]) AS [Voucher_Name]
		FROM Superset.BSI.Reward_Certificate_Liability_Report_Import
		GROUP BY [Voucher_Number]

	) AS src ON src.[Voucher_Number] = tgt.VoucherNumber
	WHEN MATCHED THEN
		UPDATE
			SET VoucherName = src.[Voucher_Name]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(VoucherNumber,VoucherName)
		VALUES(src.[Voucher_Number],[Voucher_Name])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[RedemptionActivity]'
GO
CREATE TABLE [BSI].[RedemptionActivity]
(
[RedemptionActivityID] [int] NOT NULL IDENTITY(1, 1),
[RewardActivityID] [int] NOT NULL,
[HotelID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_RedemptionActivity] on [BSI].[RedemptionActivity]'
GO
ALTER TABLE [BSI].[RedemptionActivity] ADD CONSTRAINT [PK_BSI_RedemptionActivity] PRIMARY KEY CLUSTERED  ([RedemptionActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_RedemptionActivity]'
GO

CREATE   PROCEDURE [BSI].[Populate_RedemptionActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.RedemptionActivity AS tgt
	USING
	(
		SELECT DISTINCT ra.[RewardActivityID],hh.HotelID
		FROM ETL.dbo.Import_IPHotelRedemption hd
			INNER JOIN BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = hd.Membership_Number
			INNER JOIN [BSI].[RewardActivity] ra ON ra.[RedemptionDate] = hd.[Redemption_Date]
					AND ra.[LoyaltyNumberID] = ln.LoyaltyNumberID
			LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelCode = hd.Hotel_Code
	) AS src ON src.[RewardActivityID] = tgt.[RewardActivityID]
	WHEN MATCHED THEN
		UPDATE
			SET HotelID = src.HotelID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardActivityID],HotelID)
		VALUES(src.[RewardActivityID],src.HotelID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RewardType]'
GO
CREATE TABLE [dbo].[RewardType]
(
[RewardTypeID] [int] NOT NULL IDENTITY(1, 1),
[RewardTypeName] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RewardType] on [dbo].[RewardType]'
GO
ALTER TABLE [dbo].[RewardType] ADD CONSTRAINT [PK_dbo_RewardType] PRIMARY KEY CLUSTERED  ([RewardTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_RewardType_Epsilon]'
GO


-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Populate_RewardType_Epsilon] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[BSI_Populate_RewardType_Epsilon]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.RewardType AS tgt
	USING
	(
		SELECT [RewardTypeID],[RewardTypeName] FROM [Epsilon].[RewardType]
	) AS src ON src.[RewardTypeName] = tgt.[RewardTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[RewardTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardTypeName],Epsilon_ID)
		VALUES(src.[RewardTypeName],src.[RewardTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ActivityType]'
GO
CREATE TABLE [dbo].[ActivityType]
(
[ActivityTypeID] [int] NOT NULL IDENTITY(1, 1),
[ActivityTypeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_ActivityType] on [dbo].[ActivityType]'
GO
ALTER TABLE [dbo].[ActivityType] ADD CONSTRAINT [PK_dbo_ActivityType] PRIMARY KEY CLUSTERED  ([ActivityTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_ActivityType]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_ActivityType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE INTO dbo.ActivityType AS tgt
	USING
	(
		SELECT [CampaignID],[CampaignName] FROM [BSI].[Campaign]
	) AS src ON src.[CampaignName] = tgt.[ActivityTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET [BSI_ID] = src.[CampaignID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ActivityTypeName],[BSI_ID])
		VALUES(src.[CampaignName],src.[CampaignID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ActivityCauseSystem]'
GO
CREATE TABLE [dbo].[ActivityCauseSystem]
(
[ActivityCauseSystemID] [int] NOT NULL IDENTITY(1, 1),
[ActivityCauseSystemName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_ActivityCauseSystem] on [dbo].[ActivityCauseSystem]'
GO
ALTER TABLE [dbo].[ActivityCauseSystem] ADD CONSTRAINT [PK_dbo_ActivityCauseSystem] PRIMARY KEY CLUSTERED  ([ActivityCauseSystemID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_ActivityCauseSystem]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_ActivityCauseSystem]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE INTO dbo.ActivityCauseSystem AS tgt
	USING
	(
		SELECT [TransactionSourceID],[TransactionSourceName] FROM [BSI].[TransactionSource]
	) AS src ON src.[TransactionSourceName] = tgt.[ActivityCauseSystemName]
	WHEN MATCHED THEN
		UPDATE
			SET [BSI_ID] = src.[TransactionSourceID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ActivityCauseSystemName],[BSI_ID])
		VALUES(src.[TransactionSourceName],src.[TransactionSourceID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PointType]'
GO
CREATE TABLE [dbo].[PointType]
(
[PointTypeID] [int] NOT NULL IDENTITY(1, 1),
[PointTypeName] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_PointType] on [dbo].[PointType]'
GO
ALTER TABLE [dbo].[PointType] ADD CONSTRAINT [PK_dbo_PointType] PRIMARY KEY CLUSTERED  ([PointTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_PointType]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_PointType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.PointType AS tgt
	USING
	(
		SELECT 0 AS BSI_ID,'Point Credit' AS [PointTypeName]
	) AS src ON src.[PointTypeName] = tgt.[PointTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET BSI_ID = tgt.BSI_ID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([PointTypeName],[BSI_ID])
		VALUES(src.[PointTypeName],src.[BSI_ID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DataSource]'
GO
CREATE TABLE [dbo].[DataSource]
(
[DataSourceID] [int] NOT NULL IDENTITY(1, 1),
[SourceName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_DataSource] on [dbo].[DataSource]'
GO
ALTER TABLE [dbo].[DataSource] ADD CONSTRAINT [PK_dbo_DataSource] PRIMARY KEY CLUSTERED  ([DataSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_PointActivity]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_PointActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'BSI'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT t.[Transaction_Date],actT.ActivityTypeID,pt.PointTypeID,ISNULL(t.[TransactionSourceID],'') AS [TransactionSourceID],res.[Booking_ID],res.[Reservation_Revenue],
				res.[Arrival_Date],t.[Reward_Posting_Date],
				CurrencyRates.[dbo].[convertCurrency](res.[Reservation_Revenue],'USD',t.[Currency_Code],res.[Arrival_Date]) AS [CurrencyExchangeRate],
				CASE WHEN t.[Points_Redemeed] > 0 THEN t.[Points_Redemeed] ELSE t.[Points_Earned] END AS [Points],
				t.[Value_of_Redemption_USD],t.[Remarks],ln.[LoyaltyNumberID],@ds AS DataSourceID,t.[TransactionsID],t.[TDR_Transaction_ID],
				t.[Currency_Code]
		FROM [BSI].[Transactions] t
			LEFT JOIN [BSI].[Reservations] res On res.[TransactionsID] = t.[TransactionsID]
			INNER JOIN [dbo].[PointType] pt ON pt.[BSI_ID] = 0
			INNER JOIN [dbo].[LoyaltyNumber] ln ON ln.BSI_ID = t.[LoyaltyNumberID]
			LEFT JOIN [dbo].[ActivityType] actT ON actT.BSI_ID = t.[CampaignID]
	) AS src ON src.DataSourceID = tgt.DataSourceID AND src.[TransactionsID] = tgt.[Internal_SourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [ActivityDate] = src.[Transaction_Date],
				[ActivityTypeID] = src.ActivityTypeID,
				[PointTypeID] = src.PointTypeID,
				[ActivityCauseSystemID] = src.[TransactionSourceID],
				[ActivityCauseID] = src.[Booking_ID],
				[ActivityCauseAmount] = src.[Reservation_Revenue],
				[ActivityCauseDate] = src.[Arrival_Date],
				[CurrencyExchangeDate] = src.[Reward_Posting_Date],
				[CurrencyExchangeRate] = src.[CurrencyExchangeRate],
				[Points] = src.[Points],
				[PointLiabilityUSD] = src.[Value_of_Redemption_USD],
				[Notes] = src.[Remarks],
				[LoyaltyNumberID] = src.[LoyaltyNumberID],
				[External_SourceKey] = src.DataSourceID,
				[ActivityCauseCurrency] = src.[Currency_Code]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ActivityDate],[ActivityTypeID],[PointTypeID],[ActivityCauseSystemID],[ActivityCauseID],[ActivityCauseAmount],
				[ActivityCauseDate],[CurrencyExchangeDate],
				[CurrencyExchangeRate],
				[Points],
				[PointLiabilityUSD],[Notes],[LoyaltyNumberID],[DataSourceID],[Internal_SourceKey],[External_SourceKey],
				[ActivityCauseCurrency])
		VALUES(src.[Transaction_Date],src.ActivityTypeID,src.PointTypeID,src.[TransactionSourceID],src.[Booking_ID],src.[Reservation_Revenue],
				src.[Arrival_Date],src.[Reward_Posting_Date],
				src.[CurrencyExchangeRate],
				src.[Points],
				src.[Value_of_Redemption_USD],src.[Remarks],src.[LoyaltyNumberID],src.DataSourceID,src.[TransactionsID],src.[TDR_Transaction_ID],
				src.[Currency_Code])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Enrollment_Source]'
GO

CREATE   PROCEDURE [BSI].[Populate_Enrollment_Source]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.Enrollment_Source AS tgt
	USING
	(
		SELECT DISTINCT [Enrollment_Source] FROM [Superset].[BSI].[Customer_Profile_Import] WHERE [Enrollment_Source] IS NOT NULL
	) AS src  ON src.[Enrollment_Source] = tgt.Enrollment_SourceName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(Enrollment_SourceName)
		VALUES(src.[Enrollment_Source])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_RewardType]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_RewardType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.RewardType AS tgt
	USING
	(
		SELECT [VoucherTypeID],[VoucherTypeName] FROM [BSI].[VoucherType]
	) AS src ON src.[VoucherTypeName] = tgt.[RewardTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET [BSI_ID] = src.[VoucherTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardTypeName],[BSI_ID])
		VALUES([VoucherTypeName],[VoucherTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_MemberEmailOptions]'
GO

CREATE   PROCEDURE [BSI].[Populate_MemberEmailOptions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	;WITH cte_Opt
	AS
	(
		SELECT DISTINCT g.GuestID,[e-statement],[Special_Offers],[Member_Surveys],[Partner_Offers]
		FROM [Superset].[BSI].[Customer_Profile_Import] cust
			LEFT JOIN Guests.BSI.Guest_EmailAddress ge ON ge.[emailAddress] = cust.[Email]
			LEFT JOIN Guests.BSI.vw_Location loc ON ISNULL(loc.CityName,'') = ISNULL(cust.[City],'') AND ISNULL(loc.StateName,'') = ISNULL(cust.[State],'')
												AND ISNULL(loc.CountryName,'') = ISNULL(cust.[Country],'') AND ISNULL(loc.PostalCodeName,'') = ISNULL(cust.[Zip_Postal],'')
			LEFT JOIN Guests.BSI.Guest g ON ISNULL(g.[FirstName],'') = ISNULL(cust.[First_Name],'') AND ISNULL(g.[MiddleName],'') = ISNULL(cust.[Middle_Name],'')
										AND ISNULL(g.[LastName],'') = ISNULL(cust.[Last_Name],'')
										AND ISNULL(g.[Address1],'') = ISNULL(cust.[Address],'') AND ISNULL(g.[Address2],'') = ISNULL(cust.[Address2],'')
										AND ISNULL(g.Guest_EmailAddressID,0) = ISNULL(ge.Guest_EmailAddressID,0) AND ISNULL(g.LocationID,0) = ISNULL(loc.LocationID,0)
	)
	MERGE INTO BSI.MemberEmailOptions AS tgt
	USING
	(
		SELECT DISTINCT mp.MemberProfileID,
			(SELECT EmailOptionsID FROM BSI.EmailOptions WHERE EmailOptionName = 'e-statement') AS EmailOptionsID,
			CASE opt.[e-statement] WHEN 'Opt-In' THEN 1 WHEN 'Opt-Out' THEN 0 ELSE NULL END AS OptionStatus
		FROM BSI.MemberProfile mp
			INNER JOIN cte_Opt opt ON opt.GuestID = mp.GuestID
		
		UNION ALL

		SELECT DISTINCT mp.MemberProfileID,
			(SELECT EmailOptionsID FROM BSI.EmailOptions WHERE EmailOptionName = 'special offers') AS EmailOptionsID,
			CASE opt.[Special_Offers] WHEN 'Opt-In' THEN 1 WHEN 'Opt-Out' THEN 0 ELSE NULL END AS OptionStatus
		FROM BSI.MemberProfile mp
			INNER JOIN cte_Opt opt ON opt.GuestID = mp.GuestID
		
		UNION ALL

		SELECT DISTINCT mp.MemberProfileID,
			(SELECT EmailOptionsID FROM BSI.EmailOptions WHERE EmailOptionName = 'member surveys') AS EmailOptionsID,
			CASE opt.[Special_Offers] WHEN 'Opt-In' THEN 1 WHEN 'Opt-Out' THEN 0 ELSE NULL END AS OptionStatus
		FROM BSI.MemberProfile mp
			INNER JOIN cte_Opt opt ON opt.GuestID = mp.GuestID
		
		UNION ALL

		SELECT DISTINCT mp.MemberProfileID,
			(SELECT EmailOptionsID FROM BSI.EmailOptions WHERE EmailOptionName = 'partner offers') AS EmailOptionsID,
			CASE opt.[Special_Offers] WHEN 'Opt-In' THEN 1 WHEN 'Opt-Out' THEN 0 ELSE NULL END AS OptionStatus
		FROM BSI.MemberProfile mp
			INNER JOIN cte_Opt opt ON opt.GuestID = mp.GuestID
	) AS src ON src.MemberProfileID = tgt.MemberProfileID AND src.EmailOptionsID = tgt.EmailOptionsID
	WHEN MATCHED THEN
		UPDATE
			SET OptionStatus = src.OptionStatus
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(MemberProfileID,EmailOptionsID,OptionStatus)
		VALUES(src.MemberProfileID,src.EmailOptionsID,src.OptionStatus)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Reward]'
GO
CREATE TABLE [dbo].[Reward]
(
[RewardID] [int] NOT NULL IDENTITY(1, 1),
[RewardName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Reward] on [dbo].[Reward]'
GO
ALTER TABLE [dbo].[Reward] ADD CONSTRAINT [PK_dbo_Reward] PRIMARY KEY CLUSTERED  ([RewardID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_Reward]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_Reward]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Reward AS tgt
	USING
	(
		SELECT [VoucherID],[VoucherNumber] FROM [BSI].[Voucher]
	) AS src ON src.[VoucherNumber] = tgt.[RewardName]
	WHEN MATCHED THEN
		UPDATE
			SET [BSI_ID] = src.[VoucherID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardName],[BSI_ID])
		VALUES([VoucherNumber],[VoucherID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_MemberProfile]'
GO

CREATE   PROCEDURE [BSI].[Populate_MemberProfile]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.MemberProfile AS tgt
	USING
	(
		SELECT ln.LoyaltyNumberID,cust.[Membership_Date],
			hh.HotelID,es.Enrollment_SourceID,g.GuestID,
			CASE cust.[Member_Status] WHEN 'Enabled' THEN 1 WHEN 'Disabled' THEN 0 ELSE NULL END AS MemberStatus,
			cust.[Disabled_Date],
			CASE ISDATE(cust.[Profile_Update_Date]) WHEN 1 THEN cust.[Profile_Update_Date] ELSE NULL END AS [Profile Update Date],
			cust.[Remarks],t.TierID
		FROM [Superset].[BSI].[Customer_Profile_Import] cust
			INNER JOIN BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = cust.[iPrefer_Number]
			LEFT JOIN BSI.Tier t ON t.TierName = cust.Tier
			LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelCode = cust.[SignedupHotelCode]
			LEFT JOIN BSI.Enrollment_Source es ON es.Enrollment_SourceName = cust.[SignedupHotelCode]
			LEFT JOIN Guests.BSI.Guest_EmailAddress ge ON ge.[emailAddress] = cust.[Email]
			INNER JOIN Guests.BSI.Guest g ON g.LoyaltyNumberID = ln.LoyaltyNumberID
	) AS src ON src.LoyaltyNumberID = tgt.LoyaltyNumberID
	WHEN MATCHED THEN
		UPDATE
			SET GuestID = src.GuestID,
				Enrollment_Date = src.[Membership_Date],
				Enrollment_HotelID = src.HotelID,
				Enrollment_SourceID = src.Enrollment_SourceID,
				MemberStatus = src.MemberStatus,
				Disabled_Date = src.[Disabled_Date],
				ProfileUpdate_Date = src.[Profile Update Date],
				Remarks = src.Remarks,
				TierID = src.TierID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(LoyaltyNumberID,Enrollment_Date,Enrollment_HotelID,Enrollment_SourceID,GuestID,
				MemberStatus,
				Disabled_Date,ProfileUpdate_Date,Remarks,TierID)
		VALUES(src.LoyaltyNumberID,src.[Membership_Date],src.HotelID,src.Enrollment_SourceID,src.GuestID,
			src.MemberStatus,
			src.[Disabled_Date],src.[Profile Update Date],src.[Remarks],src.TierID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_RewardActivity]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_RewardActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'BSI'

	MERGE INTO dbo.RewardActivity AS tgt
	USING
	(
		SELECT ra.[RedemptionDate],rt.[RewardTypeID],r.[RewardID],ra.[VoucherValue],ra.[PayableValue_USD],ra.[VoucherCurrency_Date],
				CurrencyRates.[dbo].[convertCurrency](ra.[VoucherValue],'USD',ra.[VoucherCurrency],ra.[VoucherCurrency_Date]) AS [CurrencyExchangeRate],
				ra.[LoyaltyNumberID],@ds AS DataSourceID,ra.[RewardActivityID],ra.[VoucherCurrency]
		FROM [BSI].[RewardActivity] ra
			LEFT JOIN [dbo].[RewardType] rt ON rt.BSI_ID = ra.[VoucherTypeID]
			LEFT JOIN [dbo].[Reward] r ON r.BSI_ID = ra.[VoucherID]
	) AS src ON src.DataSourceID = tgt.DataSourceID AND src.[RewardActivityID] = tgt.[Internal_SourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [CreateDate] = src.[RedemptionDate],
				[RewardTypeID] = src.[RewardTypeID],
				[RewardID] = src.[RewardID],
				[RewardCost] = src.[VoucherValue],
				[RewardValueUSD] = src.[PayableValue_USD],
				[CurrencyExchangeDate] = src.[VoucherCurrency_Date],
				[CurrencyExchangeRate] = src.[CurrencyExchangeRate],
				[LoyaltyNumberID] = src.[LoyaltyNumberID],
				[RewardCurrency] = src.[VoucherCurrency]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([CreateDate],[RewardTypeID],[RewardID],[RewardCost],[RewardValueUSD],[CurrencyExchangeDate],
				[CurrencyExchangeRate],
				[LoyaltyNumberID],[DataSourceID],[Internal_SourceKey],[RewardCurrency])
		VALUES(src.[RedemptionDate],src.[RewardTypeID],src.[RewardID],src.[VoucherValue],src.[PayableValue_USD],src.[VoucherCurrency_Date],
				src.[CurrencyExchangeRate],
				src.[LoyaltyNumberID],src.DataSourceID,src.[RewardActivityID],src.[VoucherCurrency])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Tier]'
GO

CREATE   PROCEDURE [BSI].[Populate_Tier]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.Tier AS tgt
	USING
	(
		SELECT DISTINCT [Tier] FROM [Superset].[BSI].[Customer_Profile_Import]
	) AS src ON src.[Tier] = tgt.TierName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(TierName)
		VALUES(src.[Tier])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RedemptionActivity]'
GO
CREATE TABLE [dbo].[RedemptionActivity]
(
[RedemptionActivityID] [int] NOT NULL IDENTITY(1, 1),
[MemberRewardNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL,
[LastUpdated] [datetime] NULL,
[UserUpdatedID] [int] NULL,
[RedeemedHotelID] [int] NULL,
[RewardTypeID] [int] NULL,
[RewardID] [int] NULL,
[RewardCost] [decimal] (20, 2) NULL,
[RewardCurrency] [nchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberID] [int] NOT NULL,
[DataSourceID] [int] NOT NULL,
[Internal_SourceKey] [int] NULL,
[External_SourceKey] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RedemptionActivity] on [dbo].[RedemptionActivity]'
GO
ALTER TABLE [dbo].[RedemptionActivity] ADD CONSTRAINT [PK_dbo_RedemptionActivity] PRIMARY KEY CLUSTERED  ([RedemptionActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_RedemptionActivity]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_RedemptionActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'BSI'

	MERGE INTO dbo.RedemptionActivity AS tgt
	USING
	(
		SELECT rew.[CreateDate],red.[HotelID],rew.[RewardTypeID],rew.[RewardID],rew.[RewardCost],rew.[LoyaltyNumberID],
				rew.DataSourceID,red.[RedemptionActivityID],rew.[RewardCurrency]
		FROM [BSI].[RedemptionActivity] red
			INNER JOIN [dbo].[RewardActivity] rew ON rew.DataSourceID = @ds AND rew.[Internal_SourceKey] = red.[RewardActivityID]
	) AS src ON src.DataSourceID = tgt.DataSourceID AND src.[RedemptionActivityID] = tgt.[Internal_SourceKey]
	WHEN MATCHED THEN
		UPDATE 
			SET [CreateDate] = src.[CreateDate],
				[RedeemedHotelID] = src.[HotelID],
				[RewardTypeID] = src.[RewardTypeID],
				[RewardID] = src.[RewardID],
				[RewardCost] = src.[RewardCost],
				[LoyaltyNumberID] = src.[LoyaltyNumberID],
				[RewardCurrency] = src.[RewardCurrency]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([CreateDate],[RedeemedHotelID],[RewardTypeID],[RewardID],[RewardCost],[LoyaltyNumberID],
				[DataSourceID],[Internal_SourceKey],[RewardCurrency])
		VALUES(src.[CreateDate],src.[HotelID],src.[RewardTypeID],src.[RewardID],src.[RewardCost],src.[LoyaltyNumberID],
				src.DataSourceID,src.[RedemptionActivityID],src.[RewardCurrency])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [operations].[Truncate_Log]'
GO
CREATE TABLE [operations].[Truncate_Log]
(
[Truncate_LogID] [int] NOT NULL IDENTITY(1, 1),
[DropFK_Script] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TruncateTbl_Script] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateFK_Script] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL CONSTRAINT [df_CreateDate] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [operations].[Truncate_List]'
GO
CREATE TABLE [operations].[Truncate_List]
(
[TableName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SchemaName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_operations_Truncate_List] on [operations].[Truncate_List]'
GO
ALTER TABLE [operations].[Truncate_List] ADD CONSTRAINT [PK_operations_Truncate_List] PRIMARY KEY CLUSTERED  ([TableName], [SchemaName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [operations].[TruncateTablesWithinTableList]'
GO

CREATE PROCEDURE [operations].[TruncateTablesWithinTableList]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @drop nvarchar(MAX) = N'',
			@truncate nvarchar(MAX) = N'',
			@create nvarchar(MAX) = N'';

	IF EXISTS(SELECT * FROM operations.Truncate_List)
	BEGIN
		-- DROP FK SCRIPT -----------------------------------------
		SELECT @drop += N'
		ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
			+ ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
		FROM sys.foreign_keys AS fk
			INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.[object_id]
			INNER JOIN sys.schemas AS cs ON ct.[schema_id] = cs.[schema_id]
			INNER JOIN operations.Truncate_List tl ON SCHEMA_ID(tl.SchemaName) = cs.schema_id AND tl.TableName = ct.name
		-----------------------------------------------------------

		-- CREATE FK SCRIPT ---------------------------------------
		SELECT @create += N'
		ALTER TABLE ' 
		   + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
		   + ' ADD CONSTRAINT ' + QUOTENAME(fk.name) 
		   + ' FOREIGN KEY (' +
		   STUFF(
					(
						SELECT ',' + QUOTENAME(c.name)
						FROM sys.columns AS c 
							INNER JOIN sys.foreign_key_columns AS fkc ON fkc.parent_column_id = c.column_id AND fkc.parent_object_id = c.[object_id]
						WHERE fkc.constraint_object_id = fk.[object_id]
						ORDER BY fkc.constraint_column_id 
						FOR XML PATH(N''),TYPE
					).value(N'.[1]',N'nvarchar(max)'),1,1,N''
				)
			+ ') REFERENCES ' + QUOTENAME(rs.name) + '.' + QUOTENAME(rt.name)
			+ '(' + 
			STUFF(
					(
						SELECT ',' + QUOTENAME(c.name)
						FROM sys.columns AS c 
							INNER JOIN sys.foreign_key_columns AS fkc ON fkc.referenced_column_id = c.column_id AND fkc.referenced_object_id = c.[object_id]
						WHERE fkc.constraint_object_id = fk.[object_id]
						ORDER BY fkc.constraint_column_id 
						FOR XML PATH(N''),TYPE
					).value(N'.[1]',N'nvarchar(max)'),1,1,N''
				) + ');'
		FROM sys.foreign_keys AS fk
			INNER JOIN sys.tables AS rt ON fk.referenced_object_id = rt.[object_id]
			INNER JOIN sys.schemas AS rs ON rt.[schema_id] = rs.[schema_id]
			INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.[object_id]
			INNER JOIN sys.schemas AS cs ON ct.[schema_id] = cs.[schema_id]
			INNER JOIN operations.Truncate_List tl ON SCHEMA_ID(tl.SchemaName) = cs.schema_id AND tl.TableName = ct.name
		WHERE rt.is_ms_shipped = 0
			AND ct.is_ms_shipped = 0
		-----------------------------------------------------------

		-- TRUNCATE TABLE SCRIPT ----------------------------------
		SELECT @truncate += N'
		TRUNCATE TABLE ' + SchemaName + '.' + QUOTENAME(TableName) + ';'
		FROM operations.Truncate_List
		-----------------------------------------------------------

		-- POPULATE LOG -------------------------------------------
		INSERT INTO operations.Truncate_Log(DropFK_Script,TruncateTbl_Script,CreateFK_Script)
		SELECT @drop,@truncate,@create
		-----------------------------------------------------------

		-- RUN TRUNCATE -------------------------------------------
		EXEC sp_executesql @drop;
		EXEC sp_executesql @truncate;
		EXEC sp_executesql @create;
		-----------------------------------------------------------
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[TruncateForReload]'
GO

CREATE PROCEDURE [BSI].[TruncateForReload]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DELETE [operations].[Truncate_List]

	INSERT INTO [operations].[Truncate_List]([SchemaName],[TableName])
	VALUES('BSI','MemberEmailOptions'),
	('BSI','MemberProfile'),
	('BSI','Enrollment_Source'),
	('BSI','Tier'),
	('BSI','Reservations'),
	('BSI','Transactions'),
	('BSI','TransactionSource'),
	('BSI','Campaign'),
	('BSI','RewardActivity'),
	('BSI','RedemptionActivity'),
	('BSI','VoucherType'),
	('BSI','Voucher'),
	('BSI','LoyaltyNumber')
	EXEC operations.TruncateTablesWithinTableList
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_TransactionSource]'
GO

CREATE   PROCEDURE [BSI].[Populate_TransactionSource]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.TransactionSource AS tgt
	USING
	(
		SELECT DISTINCT [Transaction_Source] FROM [Superset].[BSI].[TransactionDetailedReport] WHERE NULLIF([Transaction_Source],'') IS NOT NULL
	) AS src ON src.[Transaction_Source] = tgt.TransactionSourceName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(TransactionSourceName)
		VALUES([Transaction_Source])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Transactions]'
GO

CREATE   PROCEDURE [BSI].[Populate_Transactions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_TDR
	AS
	(
		SELECT [Transaction_Id],[Remarks],[Points_Earned],[Points_Redemeed],[Currency_Code],
				[Transaction_Date],[Reward_Posting_Date],[Value_of_Redemption_USD],
				[Transaction_Source],[Campaign],[iPrefer_Number],
				CASE [Member_Status] WHEN 'D' THEN 0 WHEN 'E' THEN 1 WHEN 'C' THEN 2 WHEN 'W' THEN 3 WHEN 'B' THEN 4 END AS MemberStatus
		FROM Superset.[BSI].[TransactionDetailedReport]
	)
	MERGE INTO BSI.Transactions AS tgt
	USING
	(
		SELECT tdr.[Transaction_Id],tdr.[Remarks],tdr.[Points_Earned],tdr.[Points_Redemeed],tdr.[Currency_Code],
				tdr.[Transaction_Date],tdr.[Reward_Posting_Date],tdr.[Value_of_Redemption_USD],[iPrefer_Number],
				ts.TransactionSourceID,ln.LoyaltyNumberID,tdr.MemberStatus,cam.CampaignID
		FROM cte_TDR tdr
			INNER JOIN BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = tdr.[iPrefer_Number]
			LEFT JOIN BSI.TransactionSource ts ON ts.TransactionSourceName = tdr.[Transaction_Source]
			LEFT JOIN BSI.Campaign cam ON cam.CampaignName = tdr.[Campaign]
	) AS src ON src.[Transaction_Id] = tgt.TDR_Transaction_ID
	WHEN MATCHED THEN
		UPDATE
			SET	[Remarks] = src.[Remarks],
				[Points_Earned] = src.[Points_Earned],
				[Points_Redemeed] = src.[Points_Redemeed],
				[Currency_Code] = src.[Currency_Code],
				[Transaction_Date] = src.[Transaction_Date],
				[Reward_Posting_Date] = src.[Reward_Posting_Date],
				[Value_of_Redemption_USD] = src.[Value_of_Redemption_USD],
				TransactionSourceID = src.TransactionSourceID,
				LoyaltyNumberID = src.LoyaltyNumberID,
				MemberStatus = src.MemberStatus,
				CampaignID = src.CampaignID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(TDR_Transaction_ID,[Remarks],[Points_Earned],[Points_Redemeed],[Currency_Code],[Transaction_Date],
				[Reward_Posting_Date],[Value_of_Redemption_USD],TransactionSourceID,LoyaltyNumberID,MemberStatus,CampaignID)
		VALUES([Transaction_Id],[Remarks],[Points_Earned],[Points_Redemeed],[Currency_Code],[Transaction_Date],
				[Reward_Posting_Date],[Value_of_Redemption_USD],TransactionSourceID,LoyaltyNumberID,MemberStatus,CampaignID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_RewardActivity]'
GO

CREATE   PROCEDURE [BSI].[Populate_RewardActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.RewardActivity AS tgt
	USING
	(
		SELECT DISTINCT hd.[Redemption_Date],ln.LoyaltyNumberID,vt.VoucherTypeID,v.VoucherID,hd.[Voucher_Value],
				hd.[Payable_Value_USD],hd.[Voucher_Currency],hd.[Currency_Used_Date]
		FROM ETL.dbo.Import_IPHotelRedemption hd
			INNER JOIN BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = hd.Membership_Number
			LEFT JOIN BSI.VoucherType vt ON vt.VoucherTypeName = hd.[Voucher_Type]
			LEFT JOIN BSI.Voucher v ON v.VoucherNumber = hd.[Voucher_Number]
		
		UNION

		SELECT reward.[Redemption_Date],ln.LoyaltyNumberID,vt.VoucherTypeID,v.VoucherID,reward.[Voucher_Value],
			reward.[Payable_Value_USD],reward.[Voucher_Currency],reward.[Currency_Conversion_Date] AS [Currency_Used_Date]
		FROM Superset.BSI.Reward_Certificate_Liability_Report_Import reward
			INNER JOIN BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = reward.Membership_Number
			LEFT JOIN BSI.VoucherType vt ON vt.VoucherTypeName = reward.[Voucher_Type]
			LEFT JOIN BSI.Voucher v ON v.VoucherNumber = reward.[Voucher_Number]
	) AS src ON src.VoucherID = tgt.VoucherID
	WHEN MATCHED THEN
		UPDATE
			SET RedemptionDate = src.[Redemption_Date],
				LoyaltyNumberID = src.LoyaltyNumberID,
				VoucherTypeID = src.VoucherTypeID,
				VoucherID = src.VoucherID,
				VoucherValue = src.[Voucher_Value],
				PayableValue_USD = src.[Payable_Value_USD],
				VoucherCurrency = src.[Voucher_Currency],
				VoucherCurrency_Date = src.[Currency_Used_Date]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(RedemptionDate,LoyaltyNumberID,VoucherTypeID,VoucherID,VoucherValue,
				PayableValue_USD,VoucherCurrency,VoucherCurrency_Date)
		VALUES(src.[Redemption_Date],src.LoyaltyNumberID,src.VoucherTypeID,src.VoucherID,src.[Voucher_Value],
				src.[Payable_Value_USD],src.[Voucher_Currency],src.[Currency_Used_Date])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Reservations]'
GO

CREATE   PROCEDURE [BSI].[Populate_Reservations]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.Reservations AS tgt
	USING
	(
		SELECT t.TransactionsID,tdr.[Booking_ID],tdr.[Hotel_Code],tdr.[Arrival_Date],tdr.[Departure_Date],tdr.[Booking_Source],
			tdr.[Reservation_Revenue],tdr.[Hotel_Brand],tdr.[Hotel_Name],tdr.[Amount_Spent_USD]
		FROM Superset.[BSI].[TransactionDetailedReport] tdr
			INNER JOIN BSI.Transactions t ON t.TDR_Transaction_ID = tdr.[Transaction_Id]
		WHERE NULLIF(tdr.[Booking_ID],'') IS NOT NULL
	) As src ON src.TransactionsID = tgt.TransactionsID
	WHEN MATCHED THEN
		UPDATE
			SET [Booking_ID] = src.[Booking_ID],
				[Hotel_Code] = src.[Hotel_Code],
				[Arrival_Date] = src.[Arrival_Date],
				[Departure_Date] = src.[Departure_Date],
				[Booking_Source] = src.[Booking_Source],
				[Reservation_Revenue] = src.[Reservation_Revenue],
				[Hotel_Brand] = src.[Hotel_Brand],
				[Hotel_Name] = src.[Hotel_Name],
				[Amount_Spent_USD] = src.[Amount_Spent_USD]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(TransactionsID,[Booking_ID],[Hotel_Code],[Arrival_Date],[Departure_Date],[Booking_Source],[Reservation_Revenue],[Hotel_Brand],[Hotel_Name],[Amount_Spent_USD])
		VALUES(src.TransactionsID,src.[Booking_ID],src.[Hotel_Code],src.[Arrival_Date],src.[Departure_Date],src.[Booking_Source],src.[Reservation_Revenue],src.[Hotel_Brand],src.[Hotel_Name],src.[Amount_Spent_USD])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_LoyaltyNumber]'
GO

CREATE   PROCEDURE [BSI].[Populate_LoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.LoyaltyNumber AS tgt
	USING
	(
		SELECT LoyaltyNumber
		FROM
		(
			SELECT DISTINCT [iPrefer_Number] AS LoyaltyNumber FROM Superset.[BSI].[TransactionDetailedReport]
				UNION
			SELECT DISTINCT [Membership_Number] AS LoyaltyNumber FROM ETL.dbo.Import_IPHotelRedemption
				UNION
			SELECT DISTINCT [iPrefer_Number] AS LoyaltyNumber FROM [Superset].[BSI].[Customer_Profile_Import]
				UNION
			SELECT DISTINCT [Membership_Number] FROM Superset.BSI.Reward_Certificate_Liability_Report_Import
		) x
	)AS src ON src.LoyaltyNumber = tgt.LoyaltyNumberName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(LoyaltyNumberName)
		VALUES(LoyaltyNumber)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Campaign]'
GO

CREATE   PROCEDURE [BSI].[Populate_Campaign]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO BSI.Campaign AS tgt
	USING
	(
		SELECT DISTINCT [Campaign] FROM [Superset].[BSI].[TransactionDetailedReport] WHERE NULLIF([Campaign],'') IS NOT NULL
	) AS src ON src.[Campaign] = tgt.[CampaignName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([CampaignName])
		VALUES([Campaign])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Loyalty]'
GO



CREATE   PROCEDURE [BSI].[Populate_Loyalty]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
	-- TRUNCATE BEFORE LOAD ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': TRUNCATE BEFORE LOAD'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.TruncateForReload
		EXEC Guests.BSI.TruncateForReload
	---------------------------------------------------------------------------

	-- Guests.BSI.Populate_Location -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.BSI.Populate_Location'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.BSI.Populate_Location
	---------------------------------------------------------------------------

	-- BSI.Populate_LoyaltyNumber ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_LoyaltyNumber
	---------------------------------------------------------------------------

	-- Guests.BSI.Populate_GuestInfo ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.BSI.Populate_GuestInfo'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.BSI.Populate_GuestInfo
	---------------------------------------------------------------------------

	-- BSI.Populate_Tier ------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_Tier'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_Tier
	---------------------------------------------------------------------------

	-- BSI.Populate_Enrollment_Source -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_Enrollment_Source'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_Enrollment_Source
	---------------------------------------------------------------------------

	-- BSI.Populate_MemberProfile ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_MemberProfile'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_MemberProfile
	---------------------------------------------------------------------------

	-- BSI.Populate_MemberEmailOptions ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_MemberEmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_MemberEmailOptions
	---------------------------------------------------------------------------

	-- BSI.Populate_TransactionSource -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_TransactionSource'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_TransactionSource
	---------------------------------------------------------------------------

	-- BSI.Populate_Campaign --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_Campaign'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_Campaign
	---------------------------------------------------------------------------

	-- BSI.Populate_Transactions ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_Transactions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_Transactions
	---------------------------------------------------------------------------

	-- BSI.Populate_Reservations ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_Reservations'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_Reservations
	---------------------------------------------------------------------------


	-- BSI.Populate_VoucherType ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_VoucherType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_VoucherType
	---------------------------------------------------------------------------

	-- BSI.Populate_Voucher ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_Voucher'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_Voucher
	---------------------------------------------------------------------------

	-- BSI.Populate_RewardActivity --------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_RewardActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_RewardActivity
	---------------------------------------------------------------------------

	-- BSI.Populate_RedemptionActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': BSI.Populate_RedemptionActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC BSI.Populate_RedemptionActivity
	---------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_LoyaltyNumber]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_LoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.LoyaltyNumber AS tgt
	USING
	(
		SELECT [LoyaltyNumberID],[LoyaltyNumberName] FROM BSI.LoyaltyNumber
	) AS src ON src.[LoyaltyNumberName] = tgt.[LoyaltyNumberName]
	WHEN MATCHED THEN
		UPDATE
			SET [BSI_ID] = src.[LoyaltyNumberID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([LoyaltyNumberName],[BSI_ID])
		VALUES(src.[LoyaltyNumberName],src.[LoyaltyNumberID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Tier]'
GO
CREATE TABLE [dbo].[Tier]
(
[TierID] [int] NOT NULL IDENTITY(1, 1),
[TierName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Tier] on [dbo].[Tier]'
GO
ALTER TABLE [dbo].[Tier] ADD CONSTRAINT [PK_dbo_Tier] PRIMARY KEY CLUSTERED  ([TierID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_dbo_Tier_TierName] on [dbo].[Tier]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_dbo_Tier_TierName] ON [dbo].[Tier] ([TierName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_Tier]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_Tier]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Tier AS tgt
	USING
	(
		SELECT [TierID],[TierName] FROM [BSI].[Tier]
	) AS src ON src.[TierName] = tgt.[TierName]
	WHEN MATCHED THEN
		UPDATE
			SET BSI_ID = src.[TierID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([TierName],BSI_ID)
		VALUES([TierName],TierID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Enrollment_Source]'
GO
CREATE TABLE [dbo].[Enrollment_Source]
(
[Enrollment_SourceID] [int] NOT NULL IDENTITY(1, 1),
[Enrollment_SourceName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Enrollment_Source] on [dbo].[Enrollment_Source]'
GO
ALTER TABLE [dbo].[Enrollment_Source] ADD CONSTRAINT [PK_dbo_Enrollment_Source] PRIMARY KEY CLUSTERED  ([Enrollment_SourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_dbo_Enrollment_Source_Enrollment_SourceName] on [dbo].[Enrollment_Source]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_dbo_Enrollment_Source_Enrollment_SourceName] ON [dbo].[Enrollment_Source] ([Enrollment_SourceName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_Enrollment_Source]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_Enrollment_Source]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Enrollment_Source AS tgt
	USING
	(
		SELECT [Enrollment_SourceID],[Enrollment_SourceName] FROM [BSI].[Enrollment_Source]
	) AS src ON src.[Enrollment_SourceName] = tgt.[Enrollment_SourceName]
	WHEN MATCHED THEN
		UPDATE
			SET BSI_ID = src.[Enrollment_SourceID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Enrollment_SourceName],BSI_ID)
		VALUES(src.[Enrollment_SourceName],src.[Enrollment_SourceID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_MemberProfile]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_MemberProfile]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE dbo.MemberProfile AS tgt
	USING
	(
		SELECT g.[GuestID],t.[TierID],[Enrollment_Date],es.[Enrollment_SourceID],[Enrollment_HotelID],ln.[LoyaltyNumberID],[MemberProfileID],[MemberStatus]
		FROM [BSI].[MemberProfile] mp
			INNER JOIN dbo.LoyaltyNumber ln ON ln.BSI_ID = mp.[LoyaltyNumberID]
			LEFT JOIN Guests.dbo.Guest g ON g.BSI_ID = mp.[GuestID]
			LEFT JOIN dbo.Tier t ON t.BSI_ID = mp.TierID
			LEFT JOIN [dbo].[Enrollment_Source] es ON es.BSI_ID = mp.[Enrollment_SourceID]
	) AS src ON src.[LoyaltyNumberID] = tgt.[LoyaltyNumberID]
	WHEN MATCHED THEN
		UPDATE
			SET [GuestID] = src.[GuestID],
				[TierId] = src.[TierID],
				[Enrollment_Date] = src.[Enrollment_Date],
				[Enrollment_SourceID] = src.[Enrollment_SourceID],
				[Enrollment_HotelID] = src.[Enrollment_HotelID],
				[BSI_ID] = src.[MemberProfileID],
				[MemberStatus] = src.[MemberStatus]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([GuestID],[TierId],[Enrollment_Date],[Enrollment_SourceID],[Enrollment_HotelID],[LoyaltyNumberID],[BSI_ID],[MemberStatus])
		VALUES(src.[GuestID],src.[TierID],src.[Enrollment_Date],src.[Enrollment_SourceID],src.[Enrollment_HotelID],src.[LoyaltyNumberID],src.[MemberProfileID],src.[MemberStatus])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_MemberEmailOptions]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_MemberEmailOptions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.MemberEmailOptions AS tgt
	USING
	(
		SELECT meo.[MemberEmailOptionsID],mp.[MemberProfileID],eo.[EmailOptionsID],meo.[OptionStatus]
		FROM [BSI].[MemberEmailOptions] meo
			INNER JOIN dbo.MemberProfile mp ON mp.[BSI_ID] = meo.[MemberProfileID]
			INNER JOIN BSI.EmailOptions beo ON beo.[EmailOptionsID] = meo.[EmailOptionsID]
			INNER JOIN dbo.EmailOptions eo ON eo.[EmailOptionsName] = beo.[EmailOptionName]
	) AS src ON src.[MemberProfileID] = tgt.[MemberProfileID] AND src.[EmailOptionsID] = tgt.[EmailOptionsID]
	WHEN MATCHED THEN
		UPDATE
			SET [OptionStatus] = src.[OptionStatus],
				[BSI_ID] = src.[MemberEmailOptionsID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MemberProfileID],[EmailOptionsID],[OptionStatus],[BSI_ID])
		VALUES(src.[MemberProfileID],src.[EmailOptionsID],src.[OptionStatus],src.[MemberEmailOptionsID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TruncateForReload]'
GO


CREATE PROCEDURE [dbo].[TruncateForReload]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DELETE [operations].[Truncate_List]

	INSERT INTO [operations].[Truncate_List]([SchemaName],[TableName])
	VALUES('dbo','ActivityCauseSystem'),
		('dbo','ActivityType'),
		('dbo','Enrollment_Promotion'),
		('dbo','Enrollment_Source'),
		('dbo','LoyaltyNumber'),
		('dbo','MemberEmailOptions'),
		('dbo','MemberProfile'),
		('dbo','MemberType'),
		('dbo','PointActivity'),
		('dbo','RedemptionActivity'),
		('dbo','Reward'),
		('dbo','RewardActivity'),
		('dbo','RewardManufacturer'),
		('dbo','RewardStatus'),
		('dbo','RewardType'),
		('dbo','Tier')

	EXEC operations.TruncateTablesWithinTableList
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_Loyalty]'
GO


-- ************************************************************************** --
CREATE   PROCEDURE [dbo].[BSI_Populate_Loyalty]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
	-- TRUNCATE BEFORE LOAD ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': TRUNCATE BEFORE LOAD'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.TruncateForReload
		EXEC Guests.dbo.TruncateForReload
	---------------------------------------------------------------------------

	-- Guests.BSI.Populate_Location -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.dbo.Populate_Location'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.dbo.BSI_Populate_Location
	---------------------------------------------------------------------------

	-- dbo.Populate_LoyaltyNumber ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_LoyaltyNumber
	---------------------------------------------------------------------------

	-- Guests.dbo.Populate_GuestInfo ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.dbo.Populate_GuestInfo'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.dbo.BSI_Populate_GuestInfo
	---------------------------------------------------------------------------

	-- dbo.Populate_Tier ------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Populate_Tier'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_Tier
	---------------------------------------------------------------------------

	-- dbo.Populate_Enrollment_Source -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Populate_Enrollment_Source'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_Enrollment_Source
	---------------------------------------------------------------------------

	-- dbo.Populate_MemberProfile ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Populate_MemberProfile'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_MemberProfile
	---------------------------------------------------------------------------

	-- dbo.Populate_MemberEmailOptions ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Populate_MemberEmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_MemberEmailOptions
	---------------------------------------------------------------------------

	-- dbo.BSI_Populate_ActivityType ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_ActivityType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_ActivityType
	--------------------------------------------------------------------------------

	-- dbo.BSI_Populate_ActivityCauseSystem ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_ActivityCauseSystem'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_ActivityCauseSystem
	---------------------------------------------------------------------------

	-- dbo.BSI_Populate_PointType ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_PointType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_PointType
	---------------------------------------------------------------------------

	-- dbo.BSI_Populate_PointActivity -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_PointActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_PointActivity
	---------------------------------------------------------------------------

	-- dbo.BSI_Populate_RewardType --------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_RewardType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_RewardType
	---------------------------------------------------------------------------

	-- dbo.BSI_Populate_Reward ------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_Reward'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_Reward
	----------------------------------------------------------------------------

	-- dbo.BSI_Populate_RewardActivity -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_RewardActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_RewardActivity
	--------------------------------------------------------------------------------

	-- dbo.BSI_Populate_RedemptionActivity -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BSI_Populate_RedemptionActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.BSI_Populate_RedemptionActivity
	---------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_ActivityCauseSystem]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_ActivityCauseSystem] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_ActivityCauseSystem]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.ActivityCauseSystem AS tgt
	USING
	(
		SELECT [ActivityCauseSystemID],[ActivityCauseSystemName] FROM [Epsilon].[ActivityCauseSystem]
	) AS src ON src.[ActivityCauseSystemName] = tgt.[ActivityCauseSystemName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[ActivityCauseSystemID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ActivityCauseSystemName],Epsilon_ID)
		VALUES(src.[ActivityCauseSystemName],src.[ActivityCauseSystemID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_ActivityType]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_ActivityType] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_ActivityType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.ActivityType AS tgt
	USING
	(
		SELECT [ActivityTypeID],[ActivityTypeName] FROM [Epsilon].[ActivityType]
	) AS src ON src.[ActivityTypeName] = tgt.[ActivityTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[ActivityTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ActivityTypeName],Epsilon_ID)
		VALUES(src.[ActivityTypeName],src.[ActivityTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_EmailOptions]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_EmailOptions] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_EmailOptions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.EmailOptions AS tgt
	USING
	(
		SELECT [EmailOptionsID],[EmailOptionsName] FROM [Epsilon].[EmailOptions]
	) AS src ON src.[EmailOptionsName] = tgt.[EmailOptionsName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[EmailOptionsID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([EmailOptionsName],Epsilon_ID)
		VALUES(src.[EmailOptionsName],src.[EmailOptionsID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Enrollment_Promotion]'
GO
CREATE TABLE [dbo].[Enrollment_Promotion]
(
[Enrollment_PromotionID] [int] NOT NULL IDENTITY(1, 1),
[Enrollment_PromotionName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Enrollment_Promotion] on [dbo].[Enrollment_Promotion]'
GO
ALTER TABLE [dbo].[Enrollment_Promotion] ADD CONSTRAINT [PK_dbo_Enrollment_Promotion] PRIMARY KEY CLUSTERED  ([Enrollment_PromotionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Enrollment_Promotion]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Enrollment_Promotion] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_Enrollment_Promotion]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Enrollment_Promotion AS tgt
	USING
	(
		SELECT [Enrollment_PromotionID],[Enrollment_PromotionName] FROM [Epsilon].[Enrollment_Promotion]
	) AS src ON src.[Enrollment_PromotionName] = tgt.[Enrollment_PromotionName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[Enrollment_PromotionID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Enrollment_PromotionName],Epsilon_ID)
		VALUES(src.[Enrollment_PromotionName],src.[Enrollment_PromotionID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Enrollment_Source]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Enrollment_Source] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_Enrollment_Source]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Enrollment_Source AS tgt
	USING
	(
		SELECT [Enrollment_SourceID],[Enrollment_SourceName] FROM [Epsilon].[Enrollment_Source]
	) AS src ON src.[Enrollment_SourceName] = tgt.[Enrollment_SourceName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[Enrollment_SourceID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Enrollment_SourceName],Epsilon_ID)
		VALUES(src.[Enrollment_SourceName],src.[Enrollment_SourceID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_LoyaltyNumber]'
GO


-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_LoyaltyNumber] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================

CREATE PROCEDURE [dbo].[Epsilon_Populate_LoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.LoyaltyNumber AS tgt
	USING
	(
		SELECT [LoyaltyNumberID],[LoyaltyNumberName] FROM Epsilon.LoyaltyNumber
	) AS src ON src.[LoyaltyNumberName] = tgt.[LoyaltyNumberName]
	WHEN MATCHED THEN
		UPDATE
			SET [Epsilon_ID] = src.[LoyaltyNumberID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([LoyaltyNumberName],[Epsilon_ID])
		VALUES(src.[LoyaltyNumberName],src.[LoyaltyNumberID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_MemberEmailOptions]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_MemberEmailOptions] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_MemberEmailOptions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.MemberEmailOptions AS tgt
	USING
	(
		SELECT meo.MemberEmailOptionsID ,mp.[MemberProfileID],eo.[EmailOptionsID],CASE WHEN meo.[OptionStatus] = 'Y' THEN 1 WHEN  meo.[OptionStatus] = 'N' THEN 0 ELSE NULL END AS [OptionStatus]
		FROM [Epsilon].[MemberEmailOptions] meo
			INNER JOIN dbo.MemberProfile mp ON mp.Epsilon_ID = meo.[MemberProfileID]
			INNER JOIN Epsilon.EmailOptions beo ON beo.[EmailOptionsID] = meo.[EmailOptionsID]
			INNER JOIN dbo.EmailOptions eo ON eo.[EmailOptionsName] = beo.[EmailOptionsName]
	) AS src ON src.[MemberProfileID] = tgt.[MemberProfileID] AND src.[EmailOptionsID] = tgt.[EmailOptionsID]
	WHEN MATCHED THEN
		UPDATE
			SET [OptionStatus] = src.[OptionStatus],
			Epsilon_ID = src.MemberEmailOptionsID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MemberProfileID],[EmailOptionsID],[OptionStatus],Epsilon_ID)
		VALUES(src.[MemberProfileID],src.[EmailOptionsID],src.[OptionStatus],src.MemberEmailOptionsID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TravelAgency]'
GO
CREATE TABLE [dbo].[TravelAgency]
(
[TravelAgencyID] [int] NOT NULL IDENTITY(1, 1),
[IataID] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgencyName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_TravelAgency] on [dbo].[TravelAgency]'
GO
ALTER TABLE [dbo].[TravelAgency] ADD CONSTRAINT [PK_dbo_TravelAgency] PRIMARY KEY CLUSTERED  ([TravelAgencyID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[MemberType]'
GO
CREATE TABLE [dbo].[MemberType]
(
[MemberTypeID] [int] NOT NULL IDENTITY(1, 1),
[MemberTypeName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_MemberType] on [dbo].[MemberType]'
GO
ALTER TABLE [dbo].[MemberType] ADD CONSTRAINT [PK_dbo_MemberType] PRIMARY KEY CLUSTERED  ([MemberTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_MemberProfile]'
GO







-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_MemberProfile] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_MemberProfile]
@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE dbo.MemberProfile AS tgt
	USING
	(
		SELECT mp.MemberProfileID AS [Internal_SourceKey],g.[GuestID], t.[TierId], mp.[Enrollment_Date], es.[Enrollment_SourceID], ep.[Enrollment_PromotionID]
		, [Enrollment_HotelID], mt.[MemberTypeID], 
		CASE WHEN [IsEmailValidated] = 'N' THEN 0
		WHEN [IsEmailValidated] = 'Y' THEN 1 ELSE NULL END AS [IsEmailValidated]		
		, ta.[TravelAgencyID], ln.[LoyaltyNumberID]
		, mp.SystemProfileID AS [external_SourceKey]
		,mp.QueueID
		FROM [epsilon].[MemberProfile] mp
			INNER JOIN dbo.LoyaltyNumber ln ON ln.epsilon_ID = mp.[LoyaltyNumberID]
			LEFT JOIN Guests.dbo.Guest g ON g.epsilon_ID = mp.[GuestID]
			LEFT JOIN dbo.Tier t ON t.epsilon_ID = mp.TierID
			LEFT JOIN dbo.Enrollment_Source es ON es.epsilon_ID = mp.[Enrollment_SourceID]
			LEFT JOIN dbo.Enrollment_Promotion ep ON ep.epsilon_ID = mp.Enrollment_PromotionID
			LEFT JOIN dbo.MemberType mt ON mt.epsilon_ID = mp.MemberTypeID
			LEFT JOIN dbo.TravelAgency ta ON ta.epsilon_ID = mp.TravelAgencyID
		WHERE mp.QueueID = @QueueID
	) AS src ON src.[LoyaltyNumberID] = tgt.[LoyaltyNumberID]
	WHEN MATCHED THEN
		UPDATE
			SET tgt.[GuestID] = src.[GuestID]
			, tgt.[TierId] = src.[TierId]
			, tgt.[Enrollment_Date] = src.[Enrollment_Date]
			, tgt.[Enrollment_SourceID] = src.[Enrollment_SourceID]
			, tgt.[Enrollment_PromotionID] = src.[Enrollment_PromotionID]
			, tgt.[Enrollment_HotelID] = src.[Enrollment_HotelID]
			, tgt.[MemberTypeID] = src.[MemberTypeID]
			, tgt.[IsEmailValidated] = src.[IsEmailValidated]
			, tgt.[TravelAgencyID] = src.[TravelAgencyID]
			, tgt.[External_SourceKey] = src.[External_SourceKey]
			, tgt.Epsilon_ID = src.[Internal_SourceKey]
			, tgt.QueueID = src.QueueID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [GuestID], [TierId], [Enrollment_Date], [Enrollment_SourceID], [Enrollment_PromotionID], [Enrollment_HotelID], [MemberTypeID], [IsEmailValidated], [TravelAgencyID], [LoyaltyNumberID],  [External_SourceKey],Epsilon_ID,QueueID)
		VALUES( src.[GuestID], src.[TierId], src.[Enrollment_Date], src.[Enrollment_SourceID], src.[Enrollment_PromotionID], src.[Enrollment_HotelID], src.[MemberTypeID], src.[IsEmailValidated], src.[TravelAgencyID], src.[LoyaltyNumberID], src.[External_SourceKey],src.[Internal_SourceKey],src.QueueID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_MemberType]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_MemberType] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_MemberType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.MemberType AS tgt
	USING
	(
		SELECT [MemberTypeID],[MemberTypeName] FROM [Epsilon].[MemberType]
	) AS src ON src.[MemberTypeName] = tgt.[MemberTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[MemberTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MemberTypeName],Epsilon_ID)
		VALUES(src.[MemberTypeName],src.[MemberTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_TravelAgency]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_TravelAgency] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_TravelAgency]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.TravelAgency AS tgt
	USING
	(
		SELECT [TravelAgencyID],[TravelAgencyName] FROM [Epsilon].[TravelAgency]
	) AS src ON src.[TravelAgencyName] = tgt.[TravelAgencyName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[TravelAgencyID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([TravelAgencyName],Epsilon_ID)
		VALUES(src.[TravelAgencyName],src.[TravelAgencyID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Tier]'
GO


-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Tier] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_Tier]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Tier AS tgt
	USING
	(
		SELECT [TierID],[TierName] FROM [Epsilon].[Tier]
	) AS src ON src.[TierName] = tgt.[TierName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[TierID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([TierName],Epsilon_ID)
		VALUES([TierName],TierID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_EmailOptions]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_EmailOptions] 9744
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_EmailOptions]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.EmailOptions AS tgt
	USING
	(
		SELECT DISTINCT [key] COLLATE DATABASE_DEFAULT AS [key] FROM ETL.dbo.Import_EpsilonMember
		CROSS APPLY OPENJSON(substring(json_external_data,2, len(json_external_data) - 2),N'lax $')
		 WHERE QueueID = @QueueID
	) AS src  ON src.[key] = tgt.EmailOptionsName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(EmailOptionsName)
		VALUES(src.[key])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Member]'
GO









-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of dbo import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Member] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Epsilon_Populate_Member]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

		-- Loyalty.dbo.Epsilon_Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.dbo.Epsilon_Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.dbo.Epsilon_Populate_LoyaltyNumber 
	---------------------------------------------------------------------------
	
	-- Guests.dbo.Epsilon_Populate_Location -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.dbo.Epsilon_Populate_Location'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.dbo.Epsilon_Populate_Location 
	---------------------------------------------------------------------------

	-- Guests.dbo.Epsilon_Populate_GuestInfo ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.dbo.Epsilon_Populate_GuestInfo'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.dbo.Epsilon_Populate_GuestInfo @QueueID
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_Tier ------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Tier'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Tier 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_Enrollment_Source -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Enrollment_Source'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Enrollment_Source 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_Enrollment_Promotion -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Enrollment_Promotion'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Enrollment_Promotion 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_TravelAgency -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_TravelAgency'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_TravelAgency 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberType -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_MemberType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_MemberType 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberType -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_EmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_EmailOptions 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberProfile ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_MemberProfile'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_MemberProfile @QueueID
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_MemberEmailOptions ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_MemberEmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_MemberEmailOptions 
	---------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Enrollment_Promotion]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Enrollment_Promotion] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_Enrollment_Promotion]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.Enrollment_Promotion AS tgt
	USING
	(
		SELECT DISTINCT Promo_Code FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src  ON src.Promo_Code = tgt.Enrollment_PromotionName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(Enrollment_PromotionName)
		VALUES(src.Promo_Code)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UserUpdated]'
GO
CREATE TABLE [dbo].[UserUpdated]
(
[UserUpdatedID] [int] NOT NULL IDENTITY(1, 1),
[UserUpdatedName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_UserUpdated] on [dbo].[UserUpdated]'
GO
ALTER TABLE [dbo].[UserUpdated] ADD CONSTRAINT [PK_dbo_UserUpdated] PRIMARY KEY CLUSTERED  ([UserUpdatedID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_PointActivity]'
GO















-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  
		systemPointActivityID AS [External_SourceKey]
		, [PointActivityID] AS [Internal_SourceKey]
		, CAST(REPLACE(LEFT(NULLIF([ActivityDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityDate],''),2)   AS datetime ) AS [ActivityDate]
		, ac.[ActivityTypeID]
		, pt.[PointTypeID]
		, acs.[ActivityCauseSystemID]
		, [ActivityCauseID]
		, uu.UserUpdatedID AS [ActivityCauseUserID]
		, CAST(NULLIF([ActivityCauseAmount],'') AS decimal(20,5)) AS [ActivityCauseAmount]
		, [ActivityCauseCurrency]
		, CAST(REPLACE(LEFT(NULLIF([ActivityCauseDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityCauseDate],''),2)   AS datetime ) AS [ActivityCauseDate]
		, CAST(REPLACE(LEFT(NULLIF([CurrencyExchangeDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CurrencyExchangeDate],''),2)   AS datetime ) AS [CurrencyExchangeDate]
		, CAST(NULLIF([CurrencyExchangeRate],'') AS decimal(10,9) ) AS [CurrencyExchangeRate]
		, CAST(NULLIF([Points],'') AS decimal(15,5) ) AS [Points]
		, CAST(NULLIF([PointLiabilityUSD],'') AS decimal(10,2) ) AS [PointLiabilityUSD]
		, [Notes]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		,  [QueueID]
		FROM epsilon.PointActivity pa
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = pa.LoyaltyNumberID
		LEFT JOIN dbo.ActivityType ac ON pa.ActivityTypeID  = ac.Epsilon_ID
		LEFT JOIN dbo.PointType pt ON pt.Epsilon_ID  = pa.PointTypeID
		LEFT JOIN dbo.ActivityCauseSystem acs ON acs.Epsilon_ID  = pa.ActivityCauseSystemID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = pa.ActivityCauseUserID
		--WHERE pa.QueueID = @QueueID
	) AS src ON 
	ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	AND tgt.Internal_SourceKey = src.Internal_SourceKey
	AND ISNULL(tgt.[ActivityDate],'1900-01-01')  = ISNULL(src.[ActivityDate],'1900-01-01')
	AND ISNULL(tgt.[ActivityTypeID],'') = ISNULL(src.[ActivityTypeID],'')
	AND ISNULL(tgt.[PointTypeID],'') = ISNULL(src.[PointTypeID],'')
	AND ISNULL(tgt.[ActivityCauseSystemID],'') = ISNULL(src.[ActivityCauseSystemID],'')
	AND ISNULL(tgt.[ActivityCauseID],'') = ISNULL(src.[ActivityCauseID],'')
	AND ISNULL(tgt.[ActivityCauseUserID],'') = ISNULL(src.[ActivityCauseUserID],'')
	AND ISNULL(tgt.[ActivityCauseAmount],0) = ISNULL(src.[ActivityCauseAmount],0)
	AND ISNULL(tgt.[ActivityCauseCurrency],'') = ISNULL(src.[ActivityCauseCurrency],'')
	AND ISNULL(tgt.[ActivityCauseDate],'1900-01-01') = ISNULL(src.[ActivityCauseDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeDate],'1900-01-01') = ISNULL(src.[CurrencyExchangeDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeRate],0) = ISNULL(src.[CurrencyExchangeRate],0)
	AND ISNULL(tgt.[Points],0) = ISNULL(src.[Points],0)
	AND ISNULL(tgt.[PointLiabilityUSD],0) = ISNULL(src.[PointLiabilityUSD],0)
	AND ISNULL(tgt.[Notes],'') = ISNULL(src.[Notes],'')
	AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [ActivityDate], [ActivityTypeID], [PointTypeID], [ActivityCauseSystemID], [ActivityCauseID], [ActivityCauseUserID], [ActivityCauseAmount], [ActivityCauseCurrency], [ActivityCauseDate], [CurrencyExchangeDate], [CurrencyExchangeRate], [Points], [PointLiabilityUSD], [Notes], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey],  [QueueID],[External_SourceKey]	)
		VALUES( src.[ActivityDate], src.[ActivityTypeID], src.[PointTypeID], src.[ActivityCauseSystemID], src.[ActivityCauseID], src.[ActivityCauseUserID], src.[ActivityCauseAmount], src.[ActivityCauseCurrency], src.[ActivityCauseDate], src.[CurrencyExchangeDate], src.[CurrencyExchangeRate], src.[Points], src.[PointLiabilityUSD], src.[Notes], src.[LoyaltyNumberID], src.[DataSourceID], src.[Internal_SourceKey],  src.[QueueID],src.[External_SourceKey])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Enrollment_Source]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Enrollment_Source] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_Enrollment_Source]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.Enrollment_Source AS tgt
	USING
	(
		SELECT DISTINCT Source_Code FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src  ON src.Source_Code = tgt.Enrollment_SourceName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(Enrollment_SourceName)
		VALUES(src.Source_Code)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_UserUpdated]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_UserUpdated] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_UserUpdated]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.UserUpdated AS tgt
	USING
	(
		SELECT [UserUpdatedID],[UserUpdatedName] FROM [Epsilon].[UserUpdated]
	) AS src ON src.[UserUpdatedName] = tgt.[UserUpdatedName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[UserUpdatedID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([UserUpdatedName],Epsilon_ID)
		VALUES(src.[UserUpdatedName],src.[UserUpdatedID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_MemberEmailOptions]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_MemberEmailOptions] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_MemberEmailOptions]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	;WITH cte_Opt
	AS
	(
		SELECT DISTINCT mp.memberprofileid, iem.json_external_data
		FROM ETL.dbo.Import_EpsilonMember iem
			LEFT JOIN epsilon.Tier t ON t.TierName = iem.Tier_Code
			LEFT JOIN epsilon.Enrollment_Source es ON es.Enrollment_SourceName = iem.SOURCE_CODE
			LEFT JOIN epsilon.Enrollment_Promotion ep ON ep.Enrollment_PromotionName = iem.PROMO_CODE
			LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelCode = iem.ENROLLMENT_STORE_CODE
			LEFT JOIN epsilon.MemberType mt ON mt.MemberTypeName = iem.MemberType
			LEFT JOIN epsilon.TravelAgency ta ON ta.TravelAgencyName = iem.TRAVEL_AGENCY_NAME AND ta.IATAID = iem.IATA_ID
			LEFT JOIN Guests.epsilon.Guest_EmailAddress ge ON ge.[emailAddress] = iem.[Email_Addr]
			LEFT JOIN Guests.epsilon.vw_Location loc ON ISNULL(loc.CityName,'') = ISNULL(iem.[City],'') AND ISNULL(loc.StateName,'') = ISNULL(iem.state_code,'')
												AND ISNULL(loc.CountryName,'') = ISNULL(iem.country_code,'') AND ISNULL(loc.PostalCodeName,'') = ISNULL(iem.postal_code,'')
			LEFT JOIN Guests.epsilon.Gender gen ON ISNULL(gen.gendername,'') = ISNULL(iem.gender,'')
			LEFT JOIN Guests.epsilon.Guest g ON ISNULL(g.[FirstName],'') = ISNULL(iem.[First_Name],'') AND ISNULL(g.[MiddleName],'') = ISNULL(iem.[Middle_Name],'')
										AND ISNULL(g.[LastName],'') = ISNULL(iem.[Last_Name],'')
										AND ISNULL(g.[Address1],'') = ISNULL(iem.address_line_1,'') AND ISNULL(g.[Address2],'') = ISNULL(iem.address_line_2,'')
										AND ISNULL(g.Guest_EmailAddressID,0) = ISNULL(ge.Guest_EmailAddressID,0) AND ISNULL(g.LocationID,0) = ISNULL(loc.LocationID,0)
										AND ISNULL(g.birthdate,'') = ISNULL(iem.birth_date,'') AND ISNULL(g.genderid,0) = ISNULL(gen.genderid,0)
			INNER JOIN Loyalty.epsilon.loyaltynumber ln ON iem.CARD_NUMBER = ln.loyaltynumbername
			INNER JOIN Loyalty.epsilon.MemberProfile mp ON iem.QueueID = mp.QueueID 
										AND iem.profile_id = mp.systemprofileid
										AND g.GuestID = mp.guestid
										AND t.TierID = mp.tierid
										AND iem.CLIENT_JOIN_DATE = mp.enrollment_date
										AND es.Enrollment_SourceID = mp.Enrollment_SourceID
										AND ep.Enrollment_PromotionID = mp.Enrollment_PromotionID
										AND ISNULL(hh.HotelID,'') = ISNULL(mp.Enrollment_HotelID,'')
										AND mt.MemberTypeID = mp.MemberTypeID
										AND iem.IS_EMAIL_VALIDATED = mp.IsEmailValidated
										AND ta.TravelAgencyID = mp.TravelAgencyID
										AND mp.LoyaltyNumberID = ln.loyaltynumberid
		WHERE iem.QueueID = @QueueID
	)
	MERGE INTO epsilon.MemberEmailOptions AS tgt
	USING
	(
		SELECT eo.EmailOptionsID, ie.memberprofileid, ie.[value] AS OptionStatus
		FROM 
		(
			SELECT DISTINCT memberprofileid, [key] COLLATE DATABASE_DEFAULT AS [key], [value]  COLLATE DATABASE_DEFAULT AS [value]
			FROM cte_Opt 	CROSS APPLY OPENJSON(substring(json_external_data,2, len(json_external_data) - 2),N'lax $')
		) ie
		LEFT JOIN Loyalty.[epsilon].EmailOptions eo ON ie.[key] = eo.EmailOptionsName
	) AS src ON src.MemberProfileID = tgt.MemberProfileID AND src.EmailOptionsID = tgt.EmailOptionsID
	WHEN MATCHED THEN
		UPDATE
			SET OptionStatus = src.OptionStatus
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(MemberProfileID,EmailOptionsID,OptionStatus)
		VALUES(src.MemberProfileID,src.EmailOptionsID,src.OptionStatus)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_PointType]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_PointType] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_PointType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.PointType AS tgt
	USING
	(
		SELECT [PointTypeID],[PointTypeName] FROM [Epsilon].[PointType]
	) AS src ON src.[PointTypeName] = tgt.[PointTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[PointTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([PointTypeName],Epsilon_ID)
		VALUES(src.[PointTypeName],src.[PointTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_MemberProfile]'
GO







-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_MemberProfile] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_MemberProfile]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.MemberProfile AS tgt
	USING
	(
		SELECT DISTINCT iem.QueueID,iem.profile_id,g.GuestID, t.TierID, iem.CLIENT_JOIN_DATE,es.Enrollment_SourceID, ep.Enrollment_PromotionID, hh.HotelID AS Enrollment_HotelID
		,mt.MemberTypeID, iem.IS_EMAIL_VALIDATED, ta.TravelAgencyID, ln.LoyaltyNumberID
		FROM ETL.dbo.Import_EpsilonMember iem
			INNER JOIN epsilon.LoyaltyNumber ln ON ln.LoyaltyNumberName = iem.card_number
			LEFT JOIN epsilon.Tier t ON t.TierName = iem.Tier_Code
			LEFT JOIN epsilon.Enrollment_Source es ON es.Enrollment_SourceName = iem.SOURCE_CODE
			LEFT JOIN epsilon.Enrollment_Promotion ep ON ep.Enrollment_PromotionName = iem.PROMO_CODE
			LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelCode = iem.ENROLLMENT_STORE_CODE
			LEFT JOIN epsilon.MemberType mt ON mt.MemberTypeName = iem.MemberType
			LEFT JOIN epsilon.TravelAgency ta ON ta.TravelAgencyName = iem.TRAVEL_AGENCY_NAME AND ta.IATAID = iem.IATA_ID
			LEFT JOIN Guests.epsilon.Guest_EmailAddress ge ON ge.[emailAddress] = iem.[Email_Addr]
			INNER JOIN Guests.epsilon.Guest g ON ln.LoyaltyNumberID = g.LoyaltyNumberID
		WHERE iem.QueueID = @QueueID
	) AS src ON src.LoyaltyNumberID = tgt.LoyaltyNumberID
	WHEN MATCHED THEN
		UPDATE
			SET QueueID = src.QueueID,
				guestid = src.guestid,
				SystemProfileID = src.profile_id,
				TierID = src.TierID,
				Enrollment_Date = src.CLIENT_JOIN_DATE,
				Enrollment_SourceID = src.Enrollment_SourceID,
				Enrollment_PromotionID = src.Enrollment_PromotionID,
				Enrollment_HotelID = src.Enrollment_HotelID,
				MemberTypeID = src.MemberTypeID,
				IsEmailValidated = src.IS_EMAIL_VALIDATED,
				TravelAgencyID = src.TravelAgencyID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([QueueID], [SystemProfileID],  [GuestID], [TierId], [Enrollment_Date], [Enrollment_SourceID], [Enrollment_PromotionID], [Enrollment_HotelID], [MemberTypeID], [IsEmailValidated], [TravelAgencyID],LoyaltyNumberID)
		VALUES(src.QueueID,src.profile_id,src.GuestID, src.TierID, src.CLIENT_JOIN_DATE,src.Enrollment_SourceID, src.Enrollment_PromotionID, src.Enrollment_HotelID
		,src.MemberTypeID, src.IS_EMAIL_VALIDATED, src.TravelAgencyID,src.LoyaltyNumberID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Point]'
GO














-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Point] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Epsilon_Populate_Point]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
		-- Loyalty.dbo.Epsilon_Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.dbo.Epsilon_Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.dbo.Epsilon_Populate_LoyaltyNumber 
	---------------------------------------------------------------------------


		-- dbo.Epsilon_Populate_ActivityType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_ActivityType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_ActivityType 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_PointType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_PointType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_PointType 
	--------------------------------------------------------------------------
	
	-- dbo.Epsilon_Populate_ActivityCauseSystem ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_ActivityCauseSystem'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_ActivityCauseSystem 
	----------------------------------------------------------------------------


	-- dbo.Epsilon_Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_UserUpdated 
	----------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_PointActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_PointActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_PointActivity @QueueID
	----------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_MemberType]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_MemberType] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_MemberType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.MemberType AS tgt
	USING
	(
		SELECT DISTINCT MemberType FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src  ON src.MemberType = tgt.MemberTypeName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(MemberTypeName)
		VALUES(src.MemberType)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[RedemptionActivity]'
GO
CREATE TABLE [epsilon].[RedemptionActivity]
(
[RedemptionActivityID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NOT NULL,
[SystemRedemptionID] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberRewardNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastUpdated] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserUpdatedID] [int] NULL,
[RedeemedHotelID] [int] NULL,
[RewardTypeID] [int] NULL,
[RewardID] [int] NULL,
[RewardCost] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardCurrency] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_RedemptionActivity] on [epsilon].[RedemptionActivity]'
GO
ALTER TABLE [epsilon].[RedemptionActivity] ADD CONSTRAINT [PK_epsilon_RedemptionActivity] PRIMARY KEY CLUSTERED  ([RedemptionActivityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_RedemptionActivity]'
GO











-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of dbo import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RedemptionActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_RedemptionActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.RedemptionActivity AS tgt
	USING
	(
		SELECT DISTINCT ra.[RedemptionActivityID] AS [Internal_SourceKey]
		, [MemberRewardNumber]
		,  CAST(REPLACE(LEFT(NULLIF([CreateDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CreateDate],''),2)   AS datetime )  AS [CreateDate]
		, CAST(REPLACE(LEFT(NULLIF([LastUpdated],''),18),'.',':') + ' ' + RIGHT(NULLIF([LastUpdated],''),2)   AS datetime )  AS [LastUpdated]
		, uu.[UserUpdatedID]
		, [RedeemedHotelID]
		, rt.[RewardTypeID]
		, r.[RewardID]
		, CAST(NULLIF([RewardCost],'') AS decimal(20,2) )  AS [RewardCost]
		, [RewardCurrency]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		, [QueueID]
		,[SystemRedemptionID] AS [External_SourceKey]
		FROM epsilon.[RedemptionActivity] ra
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = ra.LoyaltyNumberID
		LEFT JOIN dbo.Reward r ON r.Epsilon_ID  = ra.RewardID
		LEFT JOIN dbo.RewardType rt ON rt.Epsilon_ID  = ra.RewardTypeID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = ra.UserUpdatedID
		WHERE ra.QueueID = @QueueID
	) AS src ON 
		 ISNULL(tgt.[Internal_SourceKey],'') = ISNULL(src.[Internal_SourceKey],'')
	 AND ISNULL(tgt.[MemberRewardNumber],'') = ISNULL(src.[MemberRewardNumber],'')
	 AND ISNULL(tgt.[CreateDate],'1900-01-01') = ISNULL(src.[CreateDate],'1900-01-01')
	 AND ISNULL(tgt.[LastUpdated],'1900-01-01') = ISNULL(src.[LastUpdated],'1900-01-01')
	 AND ISNULL(tgt.[UserUpdatedID],'') = ISNULL(src.[UserUpdatedID],'')
	 AND ISNULL(tgt.[RedeemedHotelID],'') = ISNULL(src.[RedeemedHotelID],'')
	 AND ISNULL(tgt.[RewardTypeID],'') = ISNULL(src.[RewardTypeID],'')
	 AND ISNULL(tgt.[RewardID],'') = ISNULL(src.[RewardID],'')
	 AND ISNULL(tgt.[RewardCost],0) = ISNULL(src.[RewardCost],0)
	 AND ISNULL(tgt.[RewardCurrency],'') = ISNULL(src.[RewardCurrency],'')
	 AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	 AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	 AND ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [MemberRewardNumber], [CreateDate], [LastUpdated], [UserUpdatedID], [RedeemedHotelID], [RewardTypeID], [RewardID], [RewardCost], [RewardCurrency], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey], [External_SourceKey], [QueueID] )
		VALUES( src.[MemberRewardNumber], src.[CreateDate], src.[LastUpdated], src.[UserUpdatedID], src.[RedeemedHotelID], src.[RewardTypeID], src.[RewardID], src.[RewardCost], src.[RewardCurrency], src.[LoyaltyNumberID],src.[DataSourceID], src.[Internal_SourceKey], src.[External_SourceKey], src.[QueueID] )
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_TravelAgency]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_TravelAgency] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_TravelAgency]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.TravelAgency AS tgt
	USING
	(
		SELECT DISTINCT IATA_ID,Travel_Agency_Name FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src  ON src.IATA_ID = tgt.IATAID AND src.Travel_Agency_Name = tgt.TravelAgencyName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(IATAID,TravelAgencyName)
		VALUES(src.IATA_ID,src.Travel_Agency_Name)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_RewardType]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RewardType] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_RewardType]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.RewardType AS tgt
	USING
	(
		SELECT [RewardTypeID],[RewardTypeName] FROM [Epsilon].[RewardType]
	) AS src ON src.[RewardTypeName] = tgt.[RewardTypeName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[RewardTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardTypeName],Epsilon_ID)
		VALUES(src.[RewardTypeName],src.[RewardTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Tier]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Tier] 1
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_Tier]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.Tier AS tgt
	USING
	(
		SELECT DISTINCT Tier_Code FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src ON src.Tier_Code = tgt.TierName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(TierName)
		VALUES(src.Tier_Code)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Reward]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Reward] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_Reward]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.Reward AS tgt
	USING
	(
		SELECT [RewardID],[RewardName] FROM [Epsilon].[Reward]
	) AS src ON src.[RewardName] = tgt.[RewardName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[RewardID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardName],Epsilon_ID)
		VALUES(src.[RewardName],src.[RewardID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Member]'
GO






-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Member] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [epsilon].[Populate_Member]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

		-- Loyalty.epsilon.Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.epsilon.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.epsilon.Populate_LoyaltyNumber @QueueID
	---------------------------------------------------------------------------
	
	-- Guests.epsilon.Populate_Location -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.epsilon.Populate_Location'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.epsilon.Populate_Location @QueueID
	---------------------------------------------------------------------------

	-- Guests.epsilon.Populate_GuestInfo ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Guests.epsilon.Populate_GuestInfo'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Guests.epsilon.Populate_GuestInfo @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_Tier ------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_Tier'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_Tier @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_Enrollment_Source -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_Enrollment_Source'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_Enrollment_Source @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_Enrollment_Promotion -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_Enrollment_Promotion'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_Enrollment_Promotion @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_TravelAgency -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_TravelAgency'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_TravelAgency @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_MemberType -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_MemberType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_MemberType @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_MemberType -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_EmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_EmailOptions @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_MemberProfile ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_MemberProfile'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_MemberProfile @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_MemberEmailOptions ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_MemberEmailOptions'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_MemberEmailOptions @QueueID
	---------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Redemption]'
GO









-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Redemption] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Epsilon_Populate_Redemption]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
		-- Loyalty.dbo.Epsilon_Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.dbo.Epsilon_Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.dbo.Epsilon_Populate_LoyaltyNumber 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_Reward ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Reward'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Reward 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_RewardType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_RewardType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_RewardType 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_UserUpdated 
	---------------------------------------------------------------------------

	-- dbo.Epsilon_Populate_RedemptionActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_RedemptionActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_RedemptionActivity @QueueID
	---------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RewardStatus]'
GO
CREATE TABLE [dbo].[RewardStatus]
(
[RewardStatusID] [int] NOT NULL IDENTITY(1, 1),
[RewardStatusName] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RewardStatusdescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RewardStatus] on [dbo].[RewardStatus]'
GO
ALTER TABLE [dbo].[RewardStatus] ADD CONSTRAINT [PK_dbo_RewardStatus] PRIMARY KEY CLUSTERED  ([RewardStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RewardManufacturer]'
GO
CREATE TABLE [dbo].[RewardManufacturer]
(
[RewardManufacturerID] [int] NOT NULL IDENTITY(1, 1),
[RewardManufacturerName] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_RewardManufacturer] on [dbo].[RewardManufacturer]'
GO
ALTER TABLE [dbo].[RewardManufacturer] ADD CONSTRAINT [PK_dbo_RewardManufacturer] PRIMARY KEY CLUSTERED  ([RewardManufacturerID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_RewardActivity]'
GO











-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of dbo import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RewardActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_RewardActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.RewardActivity AS tgt
	USING
	(
		SELECT DISTINCT 
		ra.RewardActivityID AS Internal_SourceKey
		, [MemberRewardNumber]
		, rs.[RewardStatusID]
		, CAST(REPLACE(LEFT(NULLIF([CreateDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CreateDate],''),2)   AS datetime ) AS [CreateDate]
		, CAST(REPLACE(LEFT(NULLIF([LastUpdated],''),18),'.',':') + ' ' + RIGHT(NULLIF([LastUpdated],''),2)   AS datetime ) AS [LastUpdated]
		, uu.[UserUpdatedID]
		, rt.[RewardTypeID]
		, rm.[RewardManufacturerID]
		, r.[RewardID]
		, CAST(NULLIF([RewardCost],'') AS decimal(20,2)) AS [RewardCost]
		, [RewardCurrency]
		, CAST(NULLIF([RewardValueUSD],'') AS decimal(20,2)) AS [RewardValueUSD]
		, CAST(REPLACE(LEFT(NULLIF([CurrencyExchangeDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CurrencyExchangeDate],''),2)   AS datetime ) AS [CurrencyExchangeDate]
		, CAST(NULLIF([CurrencyExchangeRate],'') AS decimal(10,9)) AS [CurrencyExchangeRate]
		, CAST(NULLIF([PointValue],'') AS decimal(15,5)) AS [PointValue]
		, [Notes]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		, SystemActivityID AS [External_SourceKey]
		, [QueueID]
		FROM [epsilon].[RewardActivity] ra
		INNER JOIN dbo.loyaltynumber ln ON ln.Epsilon_ID = ra.loyaltynumberID
		LEFT JOIN dbo.RewardStatus rs ON rs.Epsilon_ID  = ra.RewardStatusID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID  = ra.UserUpdatedID
		LEFT JOIN dbo.RewardType rt ON rt.Epsilon_ID = ra.RewardTypeID
		LEFT JOIN dbo.RewardManufacturer rm ON rm.Epsilon_ID  = ra.RewardManufacturerID
		LEFT JOIN dbo.Reward r ON r.Epsilon_ID  = ra.RewardID
		WHERE ra.QueueID = @QueueID
	) AS src ON 
				ISNULL(tgt.Internal_SourceKey,'') = ISNULL(src.Internal_SourceKey,'')
			AND ISNULL(tgt.[MemberRewardNumber],'') = ISNULL(src.[MemberRewardNumber],'')
			AND ISNULL(tgt.[RewardStatusID],'') = ISNULL(src.[RewardStatusID],'')
			AND ISNULL(tgt.[CreateDate],'1900-01-01') = ISNULL(src.[CreateDate],'1900-01-01')
			AND ISNULL(tgt.[LastUpdated],'1900-01-01') = ISNULL(src.[LastUpdated],'1900-01-01')
			AND ISNULL(tgt.[UserUpdatedID],'') = ISNULL(src.[UserUpdatedID],'')
			AND ISNULL(tgt.[RewardTypeID],'') = ISNULL(src.[RewardTypeID],'')
			AND ISNULL(tgt.[RewardManufacturerID],'') = ISNULL(src.[RewardManufacturerID],'')
			AND ISNULL(tgt.[RewardID],'') = ISNULL(src.[RewardID],'')
			AND ISNULL(tgt.[RewardCost],0) = ISNULL(src.[RewardCost],0)
			AND ISNULL(tgt.[RewardCurrency],'') = ISNULL(src.[RewardCurrency],'')
			AND ISNULL(tgt.[RewardValueUSD],0) = ISNULL(src.[RewardValueUSD],0)
			AND ISNULL(tgt.[CurrencyExchangeDate],'1900-01-01') = ISNULL(src.[CurrencyExchangeDate],'1900-01-01')
			AND ISNULL(tgt.[CurrencyExchangeRate],0) = ISNULL(src.[CurrencyExchangeRate],0)
			AND ISNULL(tgt.[PointValue],0) = ISNULL(src.[PointValue],0)
			AND ISNULL(tgt.[Notes],'') = ISNULL(src.[Notes],'')
			AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
			AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
			AND ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([MemberRewardNumber], [RewardStatusID], [CreateDate], [LastUpdated], [UserUpdatedID], [RewardTypeID], [RewardManufacturerID], [RewardID], [RewardCost], [RewardCurrency], [RewardValueUSD], [CurrencyExchangeDate], [CurrencyExchangeRate], [PointValue], [Notes], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey], [External_SourceKey], [QueueID]	)
		VALUES(src.[MemberRewardNumber], src.[RewardStatusID], src.[CreateDate], src.[LastUpdated], src.[UserUpdatedID], src.[RewardTypeID], src.[RewardManufacturerID], src.[RewardID], src.[RewardCost], src.[RewardCurrency], src.[RewardValueUSD], src.[CurrencyExchangeDate], src.[CurrencyExchangeRate], src.[PointValue], src.[Notes], src.[LoyaltyNumberID], src.[DataSourceID], src.[Internal_SourceKey], src.[External_SourceKey], src.[QueueID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_RewardManufacturer]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RewardManufacturer] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_RewardManufacturer]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.RewardManufacturer AS tgt
	USING
	(
		SELECT [RewardManufacturerID],[RewardManufacturerName] FROM [Epsilon].[RewardManufacturer]
	) AS src ON src.[RewardManufacturerName] = tgt.[RewardManufacturerName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[RewardManufacturerID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardManufacturerName],Epsilon_ID)
		VALUES(src.[RewardManufacturerName],src.[RewardManufacturerID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_RewardStatus]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RewardStatus] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_RewardStatus]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.RewardStatus AS tgt
	USING
	(
		SELECT [RewardStatusID],[RewardStatusName] FROM [Epsilon].[RewardStatus]
	) AS src ON src.[RewardStatusName] = tgt.[RewardStatusName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[RewardStatusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([RewardStatusName],Epsilon_ID)
		VALUES(src.[RewardStatusName],src.[RewardStatusID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_RewardOrderActivity]'
GO
















-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_RewardOrderActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Epsilon_Populate_RewardOrderActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	

		-- Loyalty.dbo.Epsilon_Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.dbo.Epsilon_Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.dbo.Epsilon_Populate_LoyaltyNumber 
	---------------------------------------------------------------------------


		-- dbo.Epsilon_Populate_RewardStatus ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_RewardStatus'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_RewardStatus 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_UserUpdated 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_RewardType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_RewardType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_RewardType 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_RewardManufacturer ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_RewardManufacturer'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_RewardManufacturer 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_Reward ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_Reward'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_Reward 
	---------------------------------------------------------------------------

		-- dbo.Epsilon_Populate_RewardActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Epsilon_Populate_RewardActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.Epsilon_Populate_RewardActivity @QueueID
	---------------------------------------------------------------------------



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[TruncateForReload]'
GO


CREATE PROCEDURE [epsilon].[TruncateForReload]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DELETE [operations].[Truncate_List]

	INSERT INTO [operations].[Truncate_List]([SchemaName],[TableName])
	VALUES('epsilon','ActivityCauseSystem'),
		('epsilon','ActivityType'),
		('epsilon','Enrollment_Promotion'),
		('epsilon','Enrollment_Source'),
		('epsilon','LoyaltyNumber'),
		('epsilon','MemberEmailOptions'),
		('epsilon','MemberProfile'),
		('epsilon','MemberType'),
		('epsilon','PointActivity'),
		('epsilon','PointType'),
		('epsilon','RedemptionActivity'),
		('epsilon','Reward'),
		('epsilon','RewardActivity'),
		('epsilon','RewardManufacturer'),
		('epsilon','RewardStatus'),
		('epsilon','RewardType'),
		('epsilon','Tier'),
		('epsilon','TravelAgency'),
		('epsilon','UserUpdated'),
		('epsilon','EmailOptions')

	EXEC operations.TruncateTablesWithinTableList
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_RedemptionActivity]'
GO







-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_RedemptionActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [epsilon].[Populate_RedemptionActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.RedemptionActivity AS tgt
	USING
	(
		SELECT DISTINCT ier.ACT_REWARD_CERT_ID, ier.QueueID, ier.CERT_NUMBER, ier.[STATUS],  ier.CREATE_DATE, 
		ier.UPDATE_DATE, uu.UserUpdatedID, rt.RewardTypeID, hh.HotelID, r.RewardID, ier.RETAIL_COST, ier.Currency_code AS RewardCurrency,ln.loyaltynumberID
		FROM ETL.dbo.Import_EpsilonRedemption ier
		INNER JOIN epsilon.LoyaltyNumber ln ON ln.LoyaltyNumberName = ier.card_number
		LEFT JOIN epsilon.Reward r ON ier.title  = r.RewardName
		LEFT JOIN epsilon.RewardType rt ON rt.RewardTypeName  = ier.Reward_Type
		LEFT JOIN epsilon.UserUpdated uu ON uu.UserUpdatedName = ier.Update_User
		LEFT JOIN hotels..hotel hh ON hh.hotelcode = ier.store_code
		WHERE ier.QueueID = @QueueID
	) AS src ON 
	src.ACT_REWARD_CERT_ID = tgt.SystemRedemptionID AND
	src.CERT_NUMBER = tgt.MemberRewardNumber AND 
	src.[STATUS] = tgt.[Status] AND 
	src.CREATE_DATE = tgt.[CreateDate] AND
	src.UPDATE_DATE = tgt.[LastUpdated] AND
	src.UserUpdatedID = tgt.UserUpdatedID AND
	src.RewardTypeID = tgt.RewardTypeID AND 
	src.HotelID = tgt.[RedeemedHotelID] AND
	src.RewardID = tgt.RewardID AND 
	src.RETAIL_COST = tgt.RewardCost AND
	src.RewardCurrency = tgt.RewardCurrency AND
	src.loyaltynumberID = tgt.loyaltynumberID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([SystemRedemptionID],QueueID, [MemberRewardNumber], [Status], [CreateDate], [LastUpdated], [UserUpdatedID], [RedeemedHotelID], [RewardTypeID], [RewardID], [RewardCost], RewardCurrency,loyaltynumberID)
		VALUES(src.ACT_REWARD_CERT_ID,src.QueueID, src.CERT_NUMBER, src.[STATUS],  src.CREATE_DATE, 
		src.UPDATE_DATE, src.UserUpdatedID,src.HotelID, src.RewardTypeID,  src.RewardID, src.RETAIL_COST, src.RewardCurrency,src.loyaltynumberID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Redemption]'
GO







-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_Redemption] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
------------------------------------------------------------------------------------
CREATE PROCEDURE [epsilon].[Populate_Redemption]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)
	
		-- Loyalty.epsilon.Populate_LoyaltyNumber -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Loyalty.epsilon.Populate_LoyaltyNumber'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC Loyalty.epsilon.Populate_LoyaltyNumber @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_Reward ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_Reward'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_Reward @QueueID
	---------------------------------------------------------------------------

		-- epsilon.Populate_RewardType ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_RewardType'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_RewardType @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_UserUpdated ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_UserUpdated'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_UserUpdated @QueueID
	---------------------------------------------------------------------------

	-- epsilon.Populate_RedemptionActivity ----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': epsilon.Populate_RedemptionActivity'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC epsilon.Populate_RedemptionActivity @QueueID
	---------------------------------------------------------------------------


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DDL_EVENTS]'
GO
CREATE TABLE [dbo].[DDL_EVENTS]
(
[EventType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostTime] [datetime2] NULL,
[SPID] [int] NULL,
[ServerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatabaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchemaName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SetOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommandText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[Transactions]'
GO
ALTER TABLE [BSI].[Transactions] ADD CONSTRAINT [FK_BSI_Transactions_CampaignID] FOREIGN KEY ([CampaignID]) REFERENCES [BSI].[Campaign] ([CampaignID])
GO
ALTER TABLE [BSI].[Transactions] ADD CONSTRAINT [FK_BSI_Transactions_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [BSI].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [BSI].[Transactions] ADD CONSTRAINT [FK_BSI_Transactions_TransactionSourceID] FOREIGN KEY ([TransactionSourceID]) REFERENCES [BSI].[TransactionSource] ([TransactionSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[MemberEmailOptions]'
GO
ALTER TABLE [BSI].[MemberEmailOptions] ADD CONSTRAINT [FK_BSI_MemberEmailOptions_EmailOptionsID] FOREIGN KEY ([EmailOptionsID]) REFERENCES [BSI].[EmailOptions] ([EmailOptionsID])
GO
ALTER TABLE [BSI].[MemberEmailOptions] ADD CONSTRAINT [FK_BSI_MemberEmailOptions_MemberProfileID] FOREIGN KEY ([MemberProfileID]) REFERENCES [BSI].[MemberProfile] ([MemberProfileID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[MemberProfile]'
GO
ALTER TABLE [BSI].[MemberProfile] ADD CONSTRAINT [FK_BSI_Enrollment_SourceID] FOREIGN KEY ([Enrollment_SourceID]) REFERENCES [BSI].[Enrollment_Source] ([Enrollment_SourceID])
GO
ALTER TABLE [BSI].[MemberProfile] ADD CONSTRAINT [FK_BSI_MemberProfile_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [BSI].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [BSI].[MemberProfile] ADD CONSTRAINT [FK_BSI_MemberProfile_TierID] FOREIGN KEY ([TierID]) REFERENCES [BSI].[Tier] ([TierID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[RewardActivity]'
GO
ALTER TABLE [BSI].[RewardActivity] ADD CONSTRAINT [FK_BSI_RewardActivity_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [BSI].[LoyaltyNumber] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[Reservations]'
GO
ALTER TABLE [BSI].[Reservations] ADD CONSTRAINT [FK_BSI_Reservations_TransactionsID] FOREIGN KEY ([TransactionsID]) REFERENCES [BSI].[Transactions] ([TransactionsID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[MemberEmailOptions]'
GO
ALTER TABLE [dbo].[MemberEmailOptions] ADD CONSTRAINT [FK_dbo_MemberEmailOptions_EmailOptionsID] FOREIGN KEY ([EmailOptionsID]) REFERENCES [dbo].[EmailOptions] ([EmailOptionsID])
GO
ALTER TABLE [dbo].[MemberEmailOptions] ADD CONSTRAINT [FK_dbo_MemberEmailOptions_MemberProfileID] FOREIGN KEY ([MemberProfileID]) REFERENCES [dbo].[MemberProfile] ([MemberProfileID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[PointActivity]'
GO
ALTER TABLE [dbo].[PointActivity] ADD CONSTRAINT [FK_dbo_PointActivity_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[RewardActivity]'
GO
ALTER TABLE [dbo].[RewardActivity] ADD CONSTRAINT [FK_dbo_RewardActivity_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [epsilon].[MemberEmailOptions]'
GO
ALTER TABLE [epsilon].[MemberEmailOptions] ADD CONSTRAINT [FK_epsilon_MemberEmailOptions_EmailOptionsID] FOREIGN KEY ([EmailOptionsID]) REFERENCES [epsilon].[EmailOptions] ([EmailOptionsID])
GO
ALTER TABLE [epsilon].[MemberEmailOptions] ADD CONSTRAINT [FK_epsilon_MemberEmailOptions_MemberProfileID] FOREIGN KEY ([MemberProfileID]) REFERENCES [epsilon].[MemberProfile] ([MemberProfileID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [epsilon].[MemberProfile]'
GO
ALTER TABLE [epsilon].[MemberProfile] ADD CONSTRAINT [FK_epsilon_Enrollment_PromotionID] FOREIGN KEY ([Enrollment_PromotionID]) REFERENCES [epsilon].[Enrollment_Promotion] ([Enrollment_PromotionID])
GO
ALTER TABLE [epsilon].[MemberProfile] ADD CONSTRAINT [FK_epsilon_Enrollment_SourceID] FOREIGN KEY ([Enrollment_SourceID]) REFERENCES [epsilon].[Enrollment_Source] ([Enrollment_SourceID])
GO
ALTER TABLE [epsilon].[MemberProfile] ADD CONSTRAINT [FK_epsilon_TierID] FOREIGN KEY ([TierId]) REFERENCES [epsilon].[Tier] ([TierID])
GO
ALTER TABLE [epsilon].[MemberProfile] ADD CONSTRAINT [FK_epsilon_MemberTypeID] FOREIGN KEY ([MemberTypeID]) REFERENCES [epsilon].[MemberType] ([MemberTypeID])
GO
ALTER TABLE [epsilon].[MemberProfile] ADD CONSTRAINT [FK_epsilon_TravelAgencyID] FOREIGN KEY ([TravelAgencyID]) REFERENCES [epsilon].[TravelAgency] ([TravelAgencyID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating DDL triggers'
GO

CREATE TRIGGER [trig_ddl_all] ON DATABASE 
	FOR DDL_DATABASE_LEVEL_EVENTS 
AS 
	DECLARE @data XML;
	SET @data = EVENTDATA();
	
	INSERT INTO DDL_EVENTS(EventType,PostTime,SPID,ServerName,LoginName,UserName,DatabaseName,SchemaName
							,ObjectName,ObjectType,SetOptions,CommandText)
	VALUES
	(
		@data.value('(/EVENT_INSTANCE/EventType)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/PostTime)[1]','datetime2(7)'),
		@data.value('(/EVENT_INSTANCE/SPID)[1]','int'),
		@data.value('(/EVENT_INSTANCE/ServerName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/LoginName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/UserName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/DatabaseName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/SchemaName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/ObjectType)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/TSQLCommand/SetOptions)[1]','nvarchar(MAX)'),
		@data.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]','nvarchar(MAX)')
	);
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
