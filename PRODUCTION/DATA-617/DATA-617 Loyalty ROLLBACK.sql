USE Loyalty
GO

/*
Run this script on:

        chi-lt-00032377.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/14/2019 1:23:07 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[Transactions]'
GO
ALTER TABLE [BSI].[Transactions] DROP CONSTRAINT [FK_BSI_Transactions_CampaignID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[Transactions] DROP CONSTRAINT [FK_BSI_Transactions_LoyaltyNumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[Transactions] DROP CONSTRAINT [FK_BSI_Transactions_TransactionSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[MemberEmailOptions]'
GO
ALTER TABLE [BSI].[MemberEmailOptions] DROP CONSTRAINT [FK_BSI_MemberEmailOptions_EmailOptionsID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[MemberEmailOptions] DROP CONSTRAINT [FK_BSI_MemberEmailOptions_MemberProfileID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[MemberProfile]'
GO
ALTER TABLE [BSI].[MemberProfile] DROP CONSTRAINT [FK_BSI_Enrollment_SourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[MemberProfile] DROP CONSTRAINT [FK_BSI_MemberProfile_LoyaltyNumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[MemberProfile] DROP CONSTRAINT [FK_BSI_MemberProfile_TierID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[RewardActivity]'
GO
ALTER TABLE [BSI].[RewardActivity] DROP CONSTRAINT [FK_BSI_RewardActivity_LoyaltyNumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[Reservations]'
GO
ALTER TABLE [BSI].[Reservations] DROP CONSTRAINT [FK_BSI_Reservations_TransactionsID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[MemberEmailOptions]'
GO
ALTER TABLE [dbo].[MemberEmailOptions] DROP CONSTRAINT [FK_dbo_MemberEmailOptions_EmailOptionsID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[MemberEmailOptions] DROP CONSTRAINT [FK_dbo_MemberEmailOptions_MemberProfileID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[PointActivity]'
GO
ALTER TABLE [dbo].[PointActivity] DROP CONSTRAINT [FK_dbo_PointActivity_LoyaltyNumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[RewardActivity]'
GO
ALTER TABLE [dbo].[RewardActivity] DROP CONSTRAINT [FK_dbo_RewardActivity_LoyaltyNumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [epsilon].[MemberEmailOptions]'
GO
ALTER TABLE [epsilon].[MemberEmailOptions] DROP CONSTRAINT [FK_epsilon_MemberEmailOptions_EmailOptionsID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[MemberEmailOptions] DROP CONSTRAINT [FK_epsilon_MemberEmailOptions_MemberProfileID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [epsilon].[MemberProfile]'
GO
ALTER TABLE [epsilon].[MemberProfile] DROP CONSTRAINT [FK_epsilon_Enrollment_PromotionID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[MemberProfile] DROP CONSTRAINT [FK_epsilon_Enrollment_SourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[MemberProfile] DROP CONSTRAINT [FK_epsilon_TierID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[MemberProfile] DROP CONSTRAINT [FK_epsilon_MemberTypeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[MemberProfile] DROP CONSTRAINT [FK_epsilon_TravelAgencyID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Campaign]'
GO
ALTER TABLE [BSI].[Campaign] DROP CONSTRAINT [PK_BSI_Campaign]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[EmailOptions]'
GO
ALTER TABLE [BSI].[EmailOptions] DROP CONSTRAINT [PK_BSI_EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Enrollment_Source]'
GO
ALTER TABLE [BSI].[Enrollment_Source] DROP CONSTRAINT [PK_BSI_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[LoyaltyNumber]'
GO
ALTER TABLE [BSI].[LoyaltyNumber] DROP CONSTRAINT [PK_BSI_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[MemberEmailOptions]'
GO
ALTER TABLE [BSI].[MemberEmailOptions] DROP CONSTRAINT [PK_BSI_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[MemberProfile]'
GO
ALTER TABLE [BSI].[MemberProfile] DROP CONSTRAINT [PK_BSI_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[RedemptionActivity]'
GO
ALTER TABLE [BSI].[RedemptionActivity] DROP CONSTRAINT [PK_BSI_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Reservations]'
GO
ALTER TABLE [BSI].[Reservations] DROP CONSTRAINT [PK_BSI_Reservations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[RewardActivity]'
GO
ALTER TABLE [BSI].[RewardActivity] DROP CONSTRAINT [PK_BSI_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Tier]'
GO
ALTER TABLE [BSI].[Tier] DROP CONSTRAINT [PK_BSI_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[TransactionSource]'
GO
ALTER TABLE [BSI].[TransactionSource] DROP CONSTRAINT [PK_BSI_TransactionSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Transactions]'
GO
ALTER TABLE [BSI].[Transactions] DROP CONSTRAINT [PK_BSI_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[VoucherType]'
GO
ALTER TABLE [BSI].[VoucherType] DROP CONSTRAINT [PK_BSI_VoucherType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Voucher]'
GO
ALTER TABLE [BSI].[Voucher] DROP CONSTRAINT [PK_BSI_Voucher]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[ActivityCauseSystem]'
GO
ALTER TABLE [dbo].[ActivityCauseSystem] DROP CONSTRAINT [PK_dbo_ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[ActivityType]'
GO
ALTER TABLE [dbo].[ActivityType] DROP CONSTRAINT [PK_dbo_ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[DataSource]'
GO
ALTER TABLE [dbo].[DataSource] DROP CONSTRAINT [PK_dbo_DataSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EmailOptions]'
GO
ALTER TABLE [dbo].[EmailOptions] DROP CONSTRAINT [PK_dbo_EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Enrollment_Promotion]'
GO
ALTER TABLE [dbo].[Enrollment_Promotion] DROP CONSTRAINT [PK_dbo_Enrollment_Promotion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Enrollment_Source]'
GO
ALTER TABLE [dbo].[Enrollment_Source] DROP CONSTRAINT [PK_dbo_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[LoyaltyNumber]'
GO
ALTER TABLE [dbo].[LoyaltyNumber] DROP CONSTRAINT [PK_dbo_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[MemberEmailOptions]'
GO
ALTER TABLE [dbo].[MemberEmailOptions] DROP CONSTRAINT [PK_dbo_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[MemberProfile]'
GO
ALTER TABLE [dbo].[MemberProfile] DROP CONSTRAINT [PK_dbo_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[MemberType]'
GO
ALTER TABLE [dbo].[MemberType] DROP CONSTRAINT [PK_dbo_MemberType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[PointActivity]'
GO
ALTER TABLE [dbo].[PointActivity] DROP CONSTRAINT [PK_dbo_PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[PointType]'
GO
ALTER TABLE [dbo].[PointType] DROP CONSTRAINT [PK_dbo_PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RedemptionActivity]'
GO
ALTER TABLE [dbo].[RedemptionActivity] DROP CONSTRAINT [PK_dbo_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RewardActivity]'
GO
ALTER TABLE [dbo].[RewardActivity] DROP CONSTRAINT [PK_dbo_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RewardManufacturer]'
GO
ALTER TABLE [dbo].[RewardManufacturer] DROP CONSTRAINT [PK_dbo_RewardManufacturer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RewardStatus]'
GO
ALTER TABLE [dbo].[RewardStatus] DROP CONSTRAINT [PK_dbo_RewardStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[RewardType]'
GO
ALTER TABLE [dbo].[RewardType] DROP CONSTRAINT [PK_dbo_RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Reward]'
GO
ALTER TABLE [dbo].[Reward] DROP CONSTRAINT [PK_dbo_Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Tier]'
GO
ALTER TABLE [dbo].[Tier] DROP CONSTRAINT [PK_dbo_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[TravelAgency]'
GO
ALTER TABLE [dbo].[TravelAgency] DROP CONSTRAINT [PK_dbo_TravelAgency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UserUpdated]'
GO
ALTER TABLE [dbo].[UserUpdated] DROP CONSTRAINT [PK_dbo_UserUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[ActivityCauseSystem]'
GO
ALTER TABLE [epsilon].[ActivityCauseSystem] DROP CONSTRAINT [PK_epsilon_ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[ActivityType]'
GO
ALTER TABLE [epsilon].[ActivityType] DROP CONSTRAINT [PK_epsilon_ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[EmailOptions]'
GO
ALTER TABLE [epsilon].[EmailOptions] DROP CONSTRAINT [PK_epsilon_EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Enrollment_Promotion]'
GO
ALTER TABLE [epsilon].[Enrollment_Promotion] DROP CONSTRAINT [PK_epsilon_Enrollment_Promotion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Enrollment_Source]'
GO
ALTER TABLE [epsilon].[Enrollment_Source] DROP CONSTRAINT [PK_epsilon_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[LoyaltyNumber]'
GO
ALTER TABLE [epsilon].[LoyaltyNumber] DROP CONSTRAINT [PK_epsilon_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[MemberEmailOptions]'
GO
ALTER TABLE [epsilon].[MemberEmailOptions] DROP CONSTRAINT [PK_epsilon_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[MemberProfile]'
GO
ALTER TABLE [epsilon].[MemberProfile] DROP CONSTRAINT [PK_epsilon_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[MemberType]'
GO
ALTER TABLE [epsilon].[MemberType] DROP CONSTRAINT [PK_epsilon_MemberType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[PointActivity]'
GO
ALTER TABLE [epsilon].[PointActivity] DROP CONSTRAINT [PK_epsilon_PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[PointType]'
GO
ALTER TABLE [epsilon].[PointType] DROP CONSTRAINT [PK_epsilon_PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[RedemptionActivity]'
GO
ALTER TABLE [epsilon].[RedemptionActivity] DROP CONSTRAINT [PK_epsilon_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[RewardActivity]'
GO
ALTER TABLE [epsilon].[RewardActivity] DROP CONSTRAINT [PK_epsilon_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[RewardManufacturer]'
GO
ALTER TABLE [epsilon].[RewardManufacturer] DROP CONSTRAINT [PK_epsilon_RewardManufacturer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[RewardStatus]'
GO
ALTER TABLE [epsilon].[RewardStatus] DROP CONSTRAINT [PK_epsilon_RewardStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[RewardType]'
GO
ALTER TABLE [epsilon].[RewardType] DROP CONSTRAINT [PK_epsilon_RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Reward]'
GO
ALTER TABLE [epsilon].[Reward] DROP CONSTRAINT [PK_epsilon_Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Tier]'
GO
ALTER TABLE [epsilon].[Tier] DROP CONSTRAINT [PK_epsilon_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[TravelAgency]'
GO
ALTER TABLE [epsilon].[TravelAgency] DROP CONSTRAINT [PK_epsilon_TravelAgency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[UserUpdated]'
GO
ALTER TABLE [epsilon].[UserUpdated] DROP CONSTRAINT [PK_epsilon_UserUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [operations].[Truncate_List]'
GO
ALTER TABLE [operations].[Truncate_List] DROP CONSTRAINT [PK_operations_Truncate_List]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [operations].[Truncate_Log]'
GO
ALTER TABLE [operations].[Truncate_Log] DROP CONSTRAINT [df_CreateDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_Campaign_CampaignName] from [BSI].[Campaign]'
GO
DROP INDEX [UX_BSI_Campaign_CampaignName] ON [BSI].[Campaign]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_EmailOptionName] from [BSI].[EmailOptions]'
GO
DROP INDEX [UX_BSI_EmailOptionName] ON [BSI].[EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_Enrollment_SourceName] from [BSI].[Enrollment_Source]'
GO
DROP INDEX [UX_BSI_Enrollment_SourceName] ON [BSI].[Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_LoyaltyNumber_LoyaltyNumberName] from [BSI].[LoyaltyNumber]'
GO
DROP INDEX [UX_BSI_LoyaltyNumber_LoyaltyNumberName] ON [BSI].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_Reservations_TransactionsID] from [BSI].[Reservations]'
GO
DROP INDEX [UX_BSI_Reservations_TransactionsID] ON [BSI].[Reservations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_TierName] from [BSI].[Tier]'
GO
DROP INDEX [UX_BSI_TierName] ON [BSI].[Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_TransactionSource_TransactionSourceName] from [BSI].[TransactionSource]'
GO
DROP INDEX [UX_BSI_TransactionSource_TransactionSourceName] ON [BSI].[TransactionSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_VoucherType_VoucherTypeName] from [BSI].[VoucherType]'
GO
DROP INDEX [UX_BSI_VoucherType_VoucherTypeName] ON [BSI].[VoucherType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_Voucher_VoucherNumber] from [BSI].[Voucher]'
GO
DROP INDEX [UX_BSI_Voucher_VoucherNumber] ON [BSI].[Voucher]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_dbo_Enrollment_Source_Enrollment_SourceName] from [dbo].[Enrollment_Source]'
GO
DROP INDEX [UX_dbo_Enrollment_Source_Enrollment_SourceName] ON [dbo].[Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_dbo_LoyaltyNumber_LoyaltyNumberName] from [dbo].[LoyaltyNumber]'
GO
DROP INDEX [UX_dbo_LoyaltyNumber_LoyaltyNumberName] ON [dbo].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_dbo_Tier_TierName] from [dbo].[Tier]'
GO
DROP INDEX [UX_dbo_Tier_TierName] ON [dbo].[Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping DDL triggers'
GO
DROP TRIGGER [trig_ddl_all] ON DATABASE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DDL_EVENTS]'
GO
DROP TABLE [dbo].[DDL_EVENTS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Redemption]'
GO
DROP PROCEDURE [epsilon].[Populate_Redemption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_RedemptionActivity]'
GO
DROP PROCEDURE [epsilon].[Populate_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[TruncateForReload]'
GO
DROP PROCEDURE [epsilon].[TruncateForReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_RewardOrderActivity]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_RewardOrderActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_RewardStatus]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_RewardStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_RewardManufacturer]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_RewardManufacturer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_RewardActivity]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RewardManufacturer]'
GO
DROP TABLE [dbo].[RewardManufacturer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RewardStatus]'
GO
DROP TABLE [dbo].[RewardStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Redemption]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Redemption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Member]'
GO
DROP PROCEDURE [epsilon].[Populate_Member]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Reward]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Tier]'
GO
DROP PROCEDURE [epsilon].[Populate_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_RewardType]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_TravelAgency]'
GO
DROP PROCEDURE [epsilon].[Populate_TravelAgency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_RedemptionActivity]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[RedemptionActivity]'
GO
DROP TABLE [epsilon].[RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_MemberType]'
GO
DROP PROCEDURE [epsilon].[Populate_MemberType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Point]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Point]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_MemberProfile]'
GO
DROP PROCEDURE [epsilon].[Populate_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_PointType]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_MemberEmailOptions]'
GO
DROP PROCEDURE [epsilon].[Populate_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_UserUpdated]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_UserUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Enrollment_Source]'
GO
DROP PROCEDURE [epsilon].[Populate_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_PointActivity]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UserUpdated]'
GO
DROP TABLE [dbo].[UserUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Enrollment_Promotion]'
GO
DROP PROCEDURE [epsilon].[Populate_Enrollment_Promotion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Member]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Member]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_EmailOptions]'
GO
DROP PROCEDURE [epsilon].[Populate_EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Tier]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_TravelAgency]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_TravelAgency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_MemberType]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_MemberType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_MemberProfile]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[MemberType]'
GO
DROP TABLE [dbo].[MemberType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[TravelAgency]'
GO
DROP TABLE [dbo].[TravelAgency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_MemberEmailOptions]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_LoyaltyNumber]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Enrollment_Source]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Enrollment_Promotion]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Enrollment_Promotion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Enrollment_Promotion]'
GO
DROP TABLE [dbo].[Enrollment_Promotion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_EmailOptions]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_ActivityType]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_ActivityCauseSystem]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_Loyalty]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_Loyalty]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[TruncateForReload]'
GO
DROP PROCEDURE [dbo].[TruncateForReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_MemberEmailOptions]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_MemberProfile]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_Enrollment_Source]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Enrollment_Source]'
GO
DROP TABLE [dbo].[Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_Tier]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Tier]'
GO
DROP TABLE [dbo].[Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_LoyaltyNumber]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Loyalty]'
GO
DROP PROCEDURE [BSI].[Populate_Loyalty]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Campaign]'
GO
DROP PROCEDURE [BSI].[Populate_Campaign]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_LoyaltyNumber]'
GO
DROP PROCEDURE [BSI].[Populate_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Reservations]'
GO
DROP PROCEDURE [BSI].[Populate_Reservations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_RewardActivity]'
GO
DROP PROCEDURE [BSI].[Populate_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Transactions]'
GO
DROP PROCEDURE [BSI].[Populate_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_TransactionSource]'
GO
DROP PROCEDURE [BSI].[Populate_TransactionSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[TruncateForReload]'
GO
DROP PROCEDURE [BSI].[TruncateForReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [operations].[TruncateTablesWithinTableList]'
GO
DROP PROCEDURE [operations].[TruncateTablesWithinTableList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [operations].[Truncate_List]'
GO
DROP TABLE [operations].[Truncate_List]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [operations].[Truncate_Log]'
GO
DROP TABLE [operations].[Truncate_Log]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_RedemptionActivity]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RedemptionActivity]'
GO
DROP TABLE [dbo].[RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Tier]'
GO
DROP PROCEDURE [BSI].[Populate_Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_RewardActivity]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_MemberProfile]'
GO
DROP PROCEDURE [BSI].[Populate_MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_Reward]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Reward]'
GO
DROP TABLE [dbo].[Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_MemberEmailOptions]'
GO
DROP PROCEDURE [BSI].[Populate_MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_RewardType]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Enrollment_Source]'
GO
DROP PROCEDURE [BSI].[Populate_Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_PointActivity]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DataSource]'
GO
DROP TABLE [dbo].[DataSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_PointType]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[PointType]'
GO
DROP TABLE [dbo].[PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_ActivityCauseSystem]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[ActivityCauseSystem]'
GO
DROP TABLE [dbo].[ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_ActivityType]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[ActivityType]'
GO
DROP TABLE [dbo].[ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_RewardType_Epsilon]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_RewardType_Epsilon]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RewardType]'
GO
DROP TABLE [dbo].[RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_RedemptionActivity]'
GO
DROP PROCEDURE [BSI].[Populate_RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[RedemptionActivity]'
GO
DROP TABLE [BSI].[RedemptionActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Voucher]'
GO
DROP PROCEDURE [BSI].[Populate_Voucher]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Voucher]'
GO
DROP TABLE [BSI].[Voucher]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_VoucherType]'
GO
DROP PROCEDURE [BSI].[Populate_VoucherType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[VoucherType]'
GO
DROP TABLE [BSI].[VoucherType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_RewardOrderActivity]'
GO
DROP PROCEDURE [epsilon].[Populate_RewardOrderActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Reward]'
GO
DROP PROCEDURE [epsilon].[Populate_Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_RewardManufacturer]'
GO
DROP PROCEDURE [epsilon].[Populate_RewardManufacturer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_RewardType]'
GO
DROP PROCEDURE [epsilon].[Populate_RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_RewardStatus]'
GO
DROP PROCEDURE [epsilon].[Populate_RewardStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_RewardActivity]'
GO
DROP PROCEDURE [epsilon].[Populate_RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Reward]'
GO
DROP TABLE [epsilon].[Reward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[RewardActivity]'
GO
DROP TABLE [epsilon].[RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[RewardManufacturer]'
GO
DROP TABLE [epsilon].[RewardManufacturer]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[RewardStatus]'
GO
DROP TABLE [epsilon].[RewardStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[RewardType]'
GO
DROP TABLE [epsilon].[RewardType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Point]'
GO
DROP PROCEDURE [epsilon].[Populate_Point]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_LoyaltyNumber]'
GO
DROP PROCEDURE [epsilon].[Populate_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_UserUpdated]'
GO
DROP PROCEDURE [epsilon].[Populate_UserUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_PointType]'
GO
DROP PROCEDURE [epsilon].[Populate_PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_PointActivity]'
GO
DROP PROCEDURE [epsilon].[Populate_PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[LoyaltyNumber]'
GO
DROP TABLE [epsilon].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[PointActivity]'
GO
DROP TABLE [epsilon].[PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[PointType]'
GO
DROP TABLE [epsilon].[PointType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[UserUpdated]'
GO
DROP TABLE [epsilon].[UserUpdated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_ActivityType]'
GO
DROP PROCEDURE [epsilon].[Populate_ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[ActivityType]'
GO
DROP TABLE [epsilon].[ActivityType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_ActivityCauseSystem]'
GO
DROP PROCEDURE [epsilon].[Populate_ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[ActivityCauseSystem]'
GO
DROP TABLE [epsilon].[ActivityCauseSystem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[TravelAgency]'
GO
DROP TABLE [epsilon].[TravelAgency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Tier]'
GO
DROP TABLE [epsilon].[Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[MemberType]'
GO
DROP TABLE [epsilon].[MemberType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[MemberEmailOptions]'
GO
DROP TABLE [epsilon].[MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[EmailOptions]'
GO
DROP TABLE [epsilon].[EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Enrollment_Source]'
GO
DROP TABLE [epsilon].[Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[MemberProfile]'
GO
DROP TABLE [epsilon].[MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Enrollment_Promotion]'
GO
DROP TABLE [epsilon].[Enrollment_Promotion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RewardActivity]'
GO
DROP TABLE [dbo].[RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[PointActivity]'
GO
DROP TABLE [dbo].[PointActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[LoyaltyNumber]'
GO
DROP TABLE [dbo].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[MemberProfile]'
GO
DROP TABLE [dbo].[MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[MemberEmailOptions]'
GO
DROP TABLE [dbo].[MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EmailOptions]'
GO
DROP TABLE [dbo].[EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[TransactionSource]'
GO
DROP TABLE [BSI].[TransactionSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Campaign]'
GO
DROP TABLE [BSI].[Campaign]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[RewardActivity]'
GO
DROP TABLE [BSI].[RewardActivity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Reservations]'
GO
DROP TABLE [BSI].[Reservations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Transactions]'
GO
DROP TABLE [BSI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Tier]'
GO
DROP TABLE [BSI].[Tier]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[LoyaltyNumber]'
GO
DROP TABLE [BSI].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[MemberEmailOptions]'
GO
DROP TABLE [BSI].[MemberEmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[EmailOptions]'
GO
DROP TABLE [BSI].[EmailOptions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[MemberProfile]'
GO
DROP TABLE [BSI].[MemberProfile]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Enrollment_Source]'
GO
DROP TABLE [BSI].[Enrollment_Source]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping schemas'
GO
DROP SCHEMA [BSI]
GO
DROP SCHEMA [epsilon]
GO
DROP SCHEMA [operations]
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
