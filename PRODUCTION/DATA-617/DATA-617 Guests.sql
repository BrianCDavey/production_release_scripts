USE Guests
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Guests    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Guests

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/14/2019 1:26:22 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating schemas'
GO
CREATE SCHEMA [BSI]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [epsilon]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
CREATE SCHEMA [operations]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Location]'
GO
CREATE TABLE [BSI].[Location]
(
[LocationID] [int] NOT NULL IDENTITY(1, 1),
[CityID] [int] NULL,
[StateID] [int] NULL,
[CountryID] [int] NULL,
[PostalCodeID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Location] on [BSI].[Location]'
GO
ALTER TABLE [BSI].[Location] ADD CONSTRAINT [PK_BSI_Location] PRIMARY KEY CLUSTERED  ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_CityID_StateID_CountryID_PostalCodeID] on [BSI].[Location]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_CityID_StateID_CountryID_PostalCodeID] ON [BSI].[Location] ([CityID], [StateID], [CountryID], [PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Guest]'
GO
CREATE TABLE [BSI].[Guest]
(
[GuestID] [int] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Guest_EmailAddressID] [int] NULL,
[LocationID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Guest] on [BSI].[Guest]'
GO
ALTER TABLE [BSI].[Guest] ADD CONSTRAINT [PK_BSI_Guest] PRIMARY KEY CLUSTERED  ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[City]'
GO
CREATE TABLE [BSI].[City]
(
[CityID] [int] NOT NULL IDENTITY(1, 1),
[CityName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_City] on [BSI].[City]'
GO
ALTER TABLE [BSI].[City] ADD CONSTRAINT [PK_BSI_City] PRIMARY KEY CLUSTERED  ([CityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_City] on [BSI].[City]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_City] ON [BSI].[City] ([CityName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Country]'
GO
CREATE TABLE [BSI].[Country]
(
[CountryID] [int] NOT NULL IDENTITY(1, 1),
[CountryName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Country] on [BSI].[Country]'
GO
ALTER TABLE [BSI].[Country] ADD CONSTRAINT [PK_BSI_Country] PRIMARY KEY CLUSTERED  ([CountryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_Country] on [BSI].[Country]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_Country] ON [BSI].[Country] ([CountryName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[PostalCode]'
GO
CREATE TABLE [BSI].[PostalCode]
(
[PostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[PostalCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_PostalCode] on [BSI].[PostalCode]'
GO
ALTER TABLE [BSI].[PostalCode] ADD CONSTRAINT [PK_BSI_PostalCode] PRIMARY KEY CLUSTERED  ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_PostalCode] on [BSI].[PostalCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_PostalCode] ON [BSI].[PostalCode] ([PostalCodeName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[State]'
GO
CREATE TABLE [BSI].[State]
(
[StateID] [int] NOT NULL IDENTITY(1, 1),
[StateName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_State] on [BSI].[State]'
GO
ALTER TABLE [BSI].[State] ADD CONSTRAINT [PK_BSI_State] PRIMARY KEY CLUSTERED  ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_State] on [BSI].[State]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_State] ON [BSI].[State] ([StateName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Guest_EmailAddress]'
GO
CREATE TABLE [dbo].[Guest_EmailAddress]
(
[Guest_EmailAddressID] [int] NOT NULL IDENTITY(1, 1),
[emailAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Guest_EmailAddress] on [dbo].[Guest_EmailAddress]'
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD CONSTRAINT [PK_dbo_Guest_EmailAddress] PRIMARY KEY CLUSTERED  ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Guest]'
GO
CREATE TABLE [dbo].[Guest]
(
[GuestID] [int] NOT NULL IDENTITY(1, 1),
[customerID] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[salutation] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NULL,
[phone] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Guest_EmailAddressID] [int] NULL,
[Location_LocationHashKey] [varbinary] (8000) NULL,
[location_AddressID] [int] NULL,
[BirthDate] [date] NULL,
[GenderID] [int] NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Guest] on [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [PK_dbo_Guest] PRIMARY KEY CLUSTERED  ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Location]'
GO
CREATE TABLE [dbo].[Location]
(
[LocationID] [int] NOT NULL IDENTITY(1, 1),
[CityID] [int] NULL,
[StateID] [int] NULL,
[CountryID] [int] NULL,
[PostalCodeID] [int] NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Location] on [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [PK_dbo_Location] PRIMARY KEY CLUSTERED  ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[City]'
GO
CREATE TABLE [dbo].[City]
(
[CityID] [int] NOT NULL IDENTITY(1, 1),
[CityName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_City] on [dbo].[City]'
GO
ALTER TABLE [dbo].[City] ADD CONSTRAINT [PK_dbo_City] PRIMARY KEY CLUSTERED  ([CityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Country]'
GO
CREATE TABLE [dbo].[Country]
(
[CountryID] [int] NOT NULL IDENTITY(1, 1),
[CountryName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Country] on [dbo].[Country]'
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [PK_dbo_Country] PRIMARY KEY CLUSTERED  ([CountryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[PostalCode]'
GO
CREATE TABLE [dbo].[PostalCode]
(
[PostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[PostalCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_PostalCode] on [dbo].[PostalCode]'
GO
ALTER TABLE [dbo].[PostalCode] ADD CONSTRAINT [PK_dbo_PostalCode] PRIMARY KEY CLUSTERED  ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[State]'
GO
CREATE TABLE [dbo].[State]
(
[StateID] [int] NOT NULL IDENTITY(1, 1),
[StateName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_State] on [dbo].[State]'
GO
ALTER TABLE [dbo].[State] ADD CONSTRAINT [PK_dbo_State] PRIMARY KEY CLUSTERED  ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[City]'
GO
CREATE TABLE [epsilon].[City]
(
[CityID] [int] NOT NULL IDENTITY(1, 1),
[CityName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_City] on [epsilon].[City]'
GO
ALTER TABLE [epsilon].[City] ADD CONSTRAINT [PK_BSI_City] PRIMARY KEY CLUSTERED  ([CityID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Location]'
GO
CREATE TABLE [epsilon].[Location]
(
[LocationID] [int] NOT NULL IDENTITY(1, 1),
[CityID] [int] NULL,
[StateID] [int] NULL,
[CountryID] [int] NULL,
[PostalCodeID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Location] on [epsilon].[Location]'
GO
ALTER TABLE [epsilon].[Location] ADD CONSTRAINT [PK_epsilon_Location] PRIMARY KEY CLUSTERED  ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Country]'
GO
CREATE TABLE [epsilon].[Country]
(
[CountryID] [int] NOT NULL IDENTITY(1, 1),
[CountryName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Country] on [epsilon].[Country]'
GO
ALTER TABLE [epsilon].[Country] ADD CONSTRAINT [PK_epsilon_Country] PRIMARY KEY CLUSTERED  ([CountryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[PostalCode]'
GO
CREATE TABLE [epsilon].[PostalCode]
(
[PostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[PostalCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_PostalCode] on [epsilon].[PostalCode]'
GO
ALTER TABLE [epsilon].[PostalCode] ADD CONSTRAINT [PK_epsilon_PostalCode] PRIMARY KEY CLUSTERED  ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[State]'
GO
CREATE TABLE [epsilon].[State]
(
[StateID] [int] NOT NULL IDENTITY(1, 1),
[StateName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_State] on [epsilon].[State]'
GO
ALTER TABLE [epsilon].[State] ADD CONSTRAINT [PK_BSI_State] PRIMARY KEY CLUSTERED  ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[vw_Location]'
GO

CREATE   VIEW [BSI].[vw_Location]
AS
	SELECT loc.LocationID,cit.CityName,st.StateName,con.CountryName,pc.PostalCodeName
	FROM BSi.[Location] loc
		LEFT JOIN BSI.City cit ON cit.CityID = loc.CityID
		LEFT JOIN BSI.[State] st ON st.StateID = loc.StateID
		LEFT JOIN BSI.Country con ON con.CountryID = loc.CountryID
		LEFT JOIN BSI.PostalCode pc ON pc.PostalCodeID = loc.PostalCodeID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Guest_EmailAddress]'
GO
CREATE TABLE [BSI].[Guest_EmailAddress]
(
[Guest_EmailAddressID] [int] NOT NULL IDENTITY(1, 1),
[emailAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_BSI_Guest_EmailAddress] on [BSI].[Guest_EmailAddress]'
GO
ALTER TABLE [BSI].[Guest_EmailAddress] ADD CONSTRAINT [PK_BSI_Guest_EmailAddress] PRIMARY KEY CLUSTERED  ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_BSI_EmailAddress] on [BSI].[Guest_EmailAddress]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_BSI_EmailAddress] ON [BSI].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_GuestInfo]'
GO

CREATE   PROCEDURE [BSI].[Populate_GuestInfo]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- Populate Guest_EmailAddress -----------------------
	MERGE INTO BSI.Guest_EmailAddress AS tgt
	USING
	(
		SELECT DISTINCT Email FROM Superset.[BSI].[Customer_Profile_Import] WHERE NULLIF(Email,'') IS NOT NULL
	) AS src ON src.Email = tgt.[emailAddress]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([emailAddress])
		VALUES(src.Email)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------

	-- Populate Guest -----------------------------------
	MERGE INTO BSI.Guest AS tgt
	USING
	(
		SELECT ln.LoyaltyNumberID,cust.[First_Name],cust.[Middle_Name],cust.[Last_Name],cust.Address,cust.[Address2],ge.Guest_EmailAddressID,loc.LocationID
		FROM Superset.[BSI].[Customer_Profile_Import] cust
			INNER JOIN Loyalty.BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = cust.[iPrefer_Number]
			LEFT JOIN BSI.Guest_EmailAddress ge ON ge.emailAddress = cust.Email
			LEFT JOIN BSI.vw_Location loc ON loc.CityName = cust.City AND loc.StateName = cust.[State]
											AND loc.CountryName = cust.Country AND loc.PostalCodeName = cust.[Zip_Postal]
	) AS src ON src.LoyaltyNumberID = tgt.LoyaltyNumberID
	WHEN MATCHED THEN
		UPDATE
			SET [FirstName] = src.[First_Name],
				[MiddleName] = src.[Middle_Name],
				[LastName] = src.[Last_Name],
				[Address1] = src.[Address],
				[Address2] = src.[Address2],
				Guest_EmailAddressID = src.Guest_EmailAddressID,
				LocationID = src.LocationID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(LoyaltyNumberID,[FirstName],[MiddleName],[LastName],[Address1],[Address2],Guest_EmailAddressID,LocationID)
		VALUES(src.LoyaltyNumberID,src.[First_Name],src.[Middle_Name],src.[Last_Name],src.[Address],src.[Address2],src.Guest_EmailAddressID,src.LocationID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[Populate_Location]'
GO

CREATE   PROCEDURE [BSI].[Populate_Location]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE @print varchar(2000)

	-- City --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate City'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO BSI.City AS tgt
		USING
		(
			SELECT DISTINCT City FROM Superset.[BSI].[Customer_Profile_Import] WHERE NULLIF(City,'') IS NOT NULL
		) AS src ON src.City = tgt.CityName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityName)
			VALUES(src.City)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- State -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate State'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO BSI.[State] AS tgt
		USING
		(
			SELECT DISTINCT [State] FROM Superset.[BSI].[Customer_Profile_Import] WHERE NULLIF([State],'') IS NOT NULL
		) AS src ON src.[State] = tgt.[StateName]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(StateName)
			VALUES(src.[State])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Country -----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Country'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO BSI.Country AS tgt
		USING
		(
			SELECT DISTINCT Country FROM Superset.[BSI].[Customer_Profile_Import] WHERE NULLIF(Country,'') IS NOT NULL
		) AS src ON src.Country = tgt.CountryName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CountryName)
			VALUES(src.Country)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Postal Code -------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Postal Code'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO BSI.[PostalCode] AS tgt
		USING
		(
			SELECT DISTINCT [Zip_Postal] FROM Superset.[BSI].[Customer_Profile_Import] WHERE NULLIF([Zip_Postal],'') IS NOT NULL
		) AS src ON src.[Zip_Postal] = tgt.[PostalCodeName]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([PostalCodeName])
			VALUES(src.[Zip_Postal])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Location ----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Location'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO BSI.[Location] AS tgt
		USING
		(
			SELECT DISTINCT cit.CityID,s.StateID,con.CountryID,pos.PostalCodeID
			FROM Superset.[BSI].[Customer_Profile_Import] cus
				LEFT JOIN BSI.City cit ON cit.CityName = cus.City
				LEFT JOIN BSI.State s ON s.StateName = cus.State
				LEFT JOIN BSI.Country con ON con.CountryName = cus.Country
				LEFT JOIN BSI.PostalCode pos ON pos.PostalCodeName = cus.[Zip_Postal]
		) AS src ON src.CityID = tgt.CityID AND src.StateID = tgt.StateID AND src.CountryID = tgt.CountryID AND src.PostalCodeID = tgt.PostalCodeID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityID,StateID,CountryID,PostalCodeID)
			VALUES(src.CityID,src.StateID,src.CountryID,src.PostalCodeID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [operations].[Truncate_Log]'
GO
CREATE TABLE [operations].[Truncate_Log]
(
[Truncate_LogID] [int] NOT NULL IDENTITY(1, 1),
[DropFK_Script] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TruncateTbl_Script] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateFK_Script] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDate] [datetime] NULL CONSTRAINT [df_CreateDate] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [operations].[Truncate_List]'
GO
CREATE TABLE [operations].[Truncate_List]
(
[TableName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SchemaName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_operations_Truncate_List] on [operations].[Truncate_List]'
GO
ALTER TABLE [operations].[Truncate_List] ADD CONSTRAINT [PK_operations_Truncate_List] PRIMARY KEY CLUSTERED  ([TableName], [SchemaName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [operations].[TruncateTablesWithinTableList]'
GO

CREATE PROCEDURE [operations].[TruncateTablesWithinTableList]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @drop nvarchar(MAX) = N'',
			@truncate nvarchar(MAX) = N'',
			@create nvarchar(MAX) = N'';

	IF EXISTS(SELECT * FROM operations.Truncate_List)
	BEGIN
		-- DROP FK SCRIPT -----------------------------------------
		SELECT @drop += N'
		ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
			+ ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + ';'
		FROM sys.foreign_keys AS fk
			INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.[object_id]
			INNER JOIN sys.schemas AS cs ON ct.[schema_id] = cs.[schema_id]
			INNER JOIN operations.Truncate_List tl ON SCHEMA_ID(tl.SchemaName) = cs.schema_id AND tl.TableName = ct.name
		-----------------------------------------------------------

		-- CREATE FK SCRIPT ---------------------------------------
		SELECT @create += N'
		ALTER TABLE ' 
		   + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
		   + ' ADD CONSTRAINT ' + QUOTENAME(fk.name) 
		   + ' FOREIGN KEY (' +
		   STUFF(
					(
						SELECT ',' + QUOTENAME(c.name)
						FROM sys.columns AS c 
							INNER JOIN sys.foreign_key_columns AS fkc ON fkc.parent_column_id = c.column_id AND fkc.parent_object_id = c.[object_id]
						WHERE fkc.constraint_object_id = fk.[object_id]
						ORDER BY fkc.constraint_column_id 
						FOR XML PATH(N''),TYPE
					).value(N'.[1]',N'nvarchar(max)'),1,1,N''
				)
			+ ') REFERENCES ' + QUOTENAME(rs.name) + '.' + QUOTENAME(rt.name)
			+ '(' + 
			STUFF(
					(
						SELECT ',' + QUOTENAME(c.name)
						FROM sys.columns AS c 
							INNER JOIN sys.foreign_key_columns AS fkc ON fkc.referenced_column_id = c.column_id AND fkc.referenced_object_id = c.[object_id]
						WHERE fkc.constraint_object_id = fk.[object_id]
						ORDER BY fkc.constraint_column_id 
						FOR XML PATH(N''),TYPE
					).value(N'.[1]',N'nvarchar(max)'),1,1,N''
				) + ');'
		FROM sys.foreign_keys AS fk
			INNER JOIN sys.tables AS rt ON fk.referenced_object_id = rt.[object_id]
			INNER JOIN sys.schemas AS rs ON rt.[schema_id] = rs.[schema_id]
			INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.[object_id]
			INNER JOIN sys.schemas AS cs ON ct.[schema_id] = cs.[schema_id]
			INNER JOIN operations.Truncate_List tl ON SCHEMA_ID(tl.SchemaName) = cs.schema_id AND tl.TableName = ct.name
		WHERE rt.is_ms_shipped = 0
			AND ct.is_ms_shipped = 0
		-----------------------------------------------------------

		-- TRUNCATE TABLE SCRIPT ----------------------------------
		SELECT @truncate += N'
		TRUNCATE TABLE ' + SchemaName + '.' + QUOTENAME(TableName) + ';'
		FROM operations.Truncate_List
		-----------------------------------------------------------

		-- POPULATE LOG -------------------------------------------
		INSERT INTO operations.Truncate_Log(DropFK_Script,TruncateTbl_Script,CreateFK_Script)
		SELECT @drop,@truncate,@create
		-----------------------------------------------------------

		-- RUN TRUNCATE -------------------------------------------
		EXEC sp_executesql @drop;
		EXEC sp_executesql @truncate;
		EXEC sp_executesql @create;
		-----------------------------------------------------------
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [BSI].[TruncateForReload]'
GO

CREATE PROCEDURE [BSI].[TruncateForReload]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DELETE [operations].[Truncate_List]

	INSERT INTO [operations].[Truncate_List]([SchemaName],[TableName])
	VALUES('BSI','Guest'),
			('BSI','Guest_EmailAddress'),
			('BSI','Location'),
			('BSI','PostalCode'),
			('BSI','Country'),
			('BSI','State'),
			('BSI','City')
	EXEC operations.TruncateTablesWithinTableList
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_Location]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_Location]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- City --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate City'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.City AS tgt
		USING
		(
			SELECT DISTINCT [CityID],[CityName] FROM BSI.City
		) AS src ON src.CityName = tgt.CityName
		WHEN MATCHED THEN
			UPDATE
				SET [BSI_ID] = src.CityID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityName,[BSI_ID])
			VALUES(src.CityName,src.CityID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- State -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate State'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[State] AS tgt
		USING
		(
			SELECT DISTINCT [StateID],[StateName] FROM [BSI].[State]
		) AS src ON src.[StateName] = tgt.[StateName]
		WHEN MATCHED THEN
			UPDATE
				SET [BSI_ID] = src.[StateID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([StateName],[BSI_ID])
			VALUES(src.[StateName],src.[StateID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Country -----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Country'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.Country AS tgt
		USING
		(
			SELECT DISTINCT [CountryID],[CountryName] FROM [BSI].[Country]
		) AS src ON src.[CountryName] = tgt.CountryName
		WHEN MATCHED THEN
			UPDATE
				SET [BSI_ID] = src.[CountryID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([CountryName],[BSI_ID])
			VALUES(src.[CountryName],src.[CountryID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Postal Code -------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Postal Code'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[PostalCode] AS tgt
		USING
		(
			SELECT DISTINCT [PostalCodeID],[PostalCodeName] FROM [BSI].[PostalCode]
		) AS src ON src.[PostalCodeName] = tgt.[PostalCodeName]
		WHEN MATCHED THEN
			UPDATE
				SET [BSI_ID] = src.[PostalCodeID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([PostalCodeName],[BSI_ID])
			VALUES(src.[PostalCodeName],src.[PostalCodeID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Location ----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Location'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[Location] AS tgt
		USING
		(
			SELECT l.LocationID,cit.CityID,st.StateID,con.CountryID,pc.PostalCodeID
			FROM BSI.[Location] l
				LEFT JOIN dbo.City cit ON cit.BSI_ID = l.CityID
				LEFT JOIN dbo.State st ON st.BSI_ID = l.StateID
				LEFT JOIN dbo.Country con ON con.BSI_ID = l.CountryID
				LEFT JOIN dbo.PostalCode pc ON pc.BSI_ID = l.PostalCodeID
		) AS src ON src.CityID = tgt.CityID AND src.StateID = tgt.StateID AND src.CountryID = tgt.CountryID AND src.PostalCodeID = tgt.PostalCodeID
		WHEN MATCHED THEN
			UPDATE
				SET BSI_ID = src.LocationID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityID,StateID,CountryID,PostalCodeID,BSI_ID)
			VALUES(src.CityID,src.StateID,src.CountryID,src.PostalCodeID,src.LocationID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BSI_Populate_GuestInfo]'
GO

CREATE   PROCEDURE [dbo].[BSI_Populate_GuestInfo]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- Populate Guest_EmailAddress -----------------------
	MERGE INTO dbo.Guest_EmailAddress AS tgt
	USING
	(
		SELECT DISTINCT [Guest_EmailAddressID],[emailAddress]
		FROM [BSI].[Guest_EmailAddress]
	) AS src ON src.[emailAddress] = tgt.[emailAddress]
	WHEN MATCHED THEN
		UPDATE
			SET BSI_ID = src.[Guest_EmailAddressID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([emailAddress],[BSI_ID])
		VALUES(src.[emailAddress],src.[Guest_EmailAddressID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------

	-- Populate Guest -----------------------------------
	MERGE INTO dbo.Guest AS tgt
	USING
	(
		SELECT [FirstName],[LastName],[Address1],[Address2],loc.[LocationID],ge.[Guest_EmailAddressID],[GuestID],ln.[LoyaltyNumberID]
		FROM [BSI].[Guest] g
			INNER JOIN Loyalty.dbo.LoyaltyNumber ln ON ln.BSI_ID = g.[LoyaltyNumberID]
			LEFT JOIN dbo.[Location] loc ON loc.BSI_ID = g.LocationID
			LEFT JOIN dbo.[Guest_EmailAddress] ge ON ge.BSI_ID = g.[Guest_EmailAddressID]
	) AS src ON src.[LoyaltyNumberID] = tgt.[LoyaltyNumberID]
	WHEN MATCHED THEN
		UPDATE
			SET [FirstName] = src.[FirstName],
				[LastName] = src.[LastName],
				[Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Guest_EmailAddressID] = src.[Guest_EmailAddressID],
				[BSI_ID] = tgt.[GuestID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([FirstName],[LastName],[Address1],[Address2],[LocationID],[Guest_EmailAddressID],[BSI_ID],[LoyaltyNumberID])
		VALUES(src.[FirstName],src.[LastName],src.[Address1],src.[Address2],src.[LocationID],src.[Guest_EmailAddressID],src.[GuestID],src.[LoyaltyNumberID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TruncateForReload]'
GO


CREATE PROCEDURE [dbo].[TruncateForReload]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DELETE [operations].[Truncate_List]

	INSERT INTO [operations].[Truncate_List]([SchemaName],[TableName])
	VALUES('dbo','Guest'),
			('dbo','Guest_EmailAddress'),
			('dbo','Location'),
			('dbo','PostalCode'),
			('dbo','Country'),
			('dbo','State'),
			('dbo','City')
	EXEC operations.TruncateTablesWithinTableList
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[vw_Location]'
GO


CREATE VIEW [epsilon].[vw_Location]
AS
	SELECT loc.LocationID,cit.CityName,st.StateName,con.CountryName,pc.PostalCodeName
	FROM epsilon.[Location] loc
		LEFT JOIN epsilon.City cit ON cit.CityID = loc.CityID
		LEFT JOIN epsilon.[State] st ON st.StateID = loc.StateID
		LEFT JOIN epsilon.Country con ON con.CountryID = loc.CountryID
		LEFT JOIN epsilon.PostalCode pc ON pc.PostalCodeID = loc.PostalCodeID
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Guest_EmailAddress]'
GO
CREATE TABLE [epsilon].[Guest_EmailAddress]
(
[Guest_EmailAddressID] [int] NOT NULL IDENTITY(1, 1),
[emailAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Guest_EmailAddress] on [epsilon].[Guest_EmailAddress]'
GO
ALTER TABLE [epsilon].[Guest_EmailAddress] ADD CONSTRAINT [PK_epsilon_Guest_EmailAddress] PRIMARY KEY CLUSTERED  ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Guest]'
GO
CREATE TABLE [epsilon].[Guest]
(
[GuestID] [int] NOT NULL IDENTITY(1, 1),
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NULL,
[Guest_EmailAddressID] [int] NULL,
[Location_LocationHashKey] [varbinary] (8000) NULL,
[BirthDate] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenderID] [int] NULL,
[LoyaltyNumberID] [int] NOT NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Guest] on [epsilon].[Guest]'
GO
ALTER TABLE [epsilon].[Guest] ADD CONSTRAINT [PK_dbo_Guest] PRIMARY KEY CLUSTERED  ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Gender]'
GO
CREATE TABLE [epsilon].[Gender]
(
[GenderID] [int] NOT NULL IDENTITY(1, 1),
[GenderName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_epsilon_Gender] on [epsilon].[Gender]'
GO
ALTER TABLE [epsilon].[Gender] ADD CONSTRAINT [PK_epsilon_Gender] PRIMARY KEY CLUSTERED  ([GenderID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_GuestInfo]'
GO







-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Guest DB
-- Prototype: EXEC [epsilon].[Populate_GuestInfo] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================

CREATE PROCEDURE [epsilon].[Populate_GuestInfo]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- Populate Guest_EmailAddress -----------------------
	MERGE INTO epsilon.Guest_EmailAddress AS tgt
	USING
	(
		SELECT DISTINCT email_addr FROM ETL.dbo.[Import_EpsilonMember] WHERE QueueID = @QueueID
	) AS src ON src.email_addr = tgt.[emailAddress]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([emailAddress])
		VALUES(src.email_addr)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------

		-- Populate Gender -----------------------
	MERGE INTO epsilon.Gender AS tgt
	USING
	(
		SELECT DISTINCT Gender FROM ETL.dbo.[Import_EpsilonMember] WHERE QueueID = @QueueID
	) AS src ON src.Gender = tgt.GenderName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(GenderName)
		VALUES(src.Gender)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------

	-- Populate Guest -----------------------------------
	MERGE INTO epsilon.Guest AS tgt
	USING
	(
		SELECT DISTINCT ln.LoyaltyNumberID, iem.[First_Name],iem.[Middle_Name],iem.[Last_Name],iem.Address_line_1,iem.Address_line_2,ge.Guest_EmailAddressID,loc.LocationID,iem.birth_date,g.genderID,iem.QueueID
		FROM ETL.dbo.[Import_EpsilonMember] iem
			INNER JOIN Loyalty.epsilon.LoyaltyNumber ln ON ln.LoyaltyNumberName = iem.Card_Number
			LEFT JOIN epsilon.Guest_EmailAddress ge ON ge.emailAddress = iem.email_addr
			LEFT JOIN  epsilon.Gender g ON g.gendername = iem.gender
			LEFT JOIN epsilon.vw_Location loc ON loc.CityName = iem.City AND  loc.StateName = iem.state_code
											AND loc.CountryName = iem.Country_code AND loc.PostalCodeName = iem.postal_code
		WHERE iem.QueueID = @QueueID
	) AS src ON src.LoyaltyNumberID = tgt.LoyaltyNumberID
	WHEN MATCHED THEN
		UPDATE SET
		tgt.[FirstName] = src.[First_Name]
		,tgt.[MiddleName] = src.[Middle_Name]
		,tgt.[LastName] = src.[Last_Name]
		,tgt.[Address1] = src.Address_line_1
		,tgt.[Address2] = src.Address_line_2
		,tgt.Guest_EmailAddressID = src.Guest_EmailAddressID
		,tgt.LocationID = src.LocationID
		,tgt.birthdate = src.birth_date
		,tgt.genderID = src.genderID
		,tgt.QueueID = src.QueueID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(LoyaltyNumberID,[FirstName],[MiddleName],[LastName],[Address1],[Address2],Guest_EmailAddressID,LocationID,birthdate,genderID, QueueID)
		VALUES(src.LoyaltyNumberID, src.[First_Name],src.[Middle_Name],src.[Last_Name],src.Address_line_1,src.Address_line_2,src.Guest_EmailAddressID,src.LocationID,src.birth_date,src.genderID, QueueID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[Populate_Location]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Guest DB
-- Prototype: EXEC [epsilon].[Populate_Location] 9744
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================

CREATE PROCEDURE [epsilon].[Populate_Location]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

		-- City --------------------------------------
	MERGE INTO epsilon.City AS tgt
	USING
	(
		SELECT DISTINCT City FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src ON src.City = tgt.CityName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(CityName)
		VALUES(src.City)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	----------------------------------------------

	
	-- State -------------------------------------
	MERGE INTO epsilon.[State] AS tgt
	USING
	(
		SELECT DISTINCT state_code FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src ON src.state_code = tgt.[StateName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(StateName)
		VALUES(src.state_code)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	----------------------------------------------
	
	-- Country -----------------------------------
	MERGE INTO epsilon.Country AS tgt
	USING
	(
		SELECT DISTINCT Country_Code FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src ON src.Country_Code = tgt.CountryName
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(CountryName)
		VALUES(src.Country_Code)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	----------------------------------------------
	
	-- Postal Code -------------------------------
	MERGE INTO epsilon.[PostalCode] AS tgt
	USING
	(
		SELECT DISTINCT Postal_Code FROM ETL.dbo.Import_EpsilonMember WHERE QueueID = @QueueID
	) AS src ON src.Postal_Code = tgt.[PostalCodeName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([PostalCodeName])
		VALUES(src.Postal_Code)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	----------------------------------------------
	
	-- Location ----------------------------------
	MERGE INTO epsilon.[Location] AS tgt
	USING
	(
		SELECT DISTINCT cit.CityID,s.StateID,con.CountryID,pos.PostalCodeID
		FROM ETL.dbo.Import_EpsilonMember iem
			LEFT JOIN epsilon.City cit ON cit.CityName = iem.City
			LEFT JOIN epsilon.State s ON s.StateName = iem.state_code
			LEFT JOIN epsilon.Country con ON con.CountryName = iem.country_code
			LEFT JOIN epsilon.PostalCode pos ON pos.PostalCodeName = iem.postal_code
		WHERE iem.QueueID = @QueueID
	) AS src ON src.CityID = tgt.CityID AND src.StateID = tgt.StateID AND src.CountryID = tgt.CountryID AND src.PostalCodeID = tgt.PostalCodeID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(CityID,StateID,CountryID,PostalCodeID)
		VALUES(src.CityID,src.StateID,src.CountryID,src.PostalCodeID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	----------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [epsilon].[TruncateForReload]'
GO


CREATE PROCEDURE [epsilon].[TruncateForReload]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DELETE [operations].[Truncate_List]

	INSERT INTO [operations].[Truncate_List]([SchemaName],[TableName])
	VALUES('Epsilon','Guest'),
			('Epsilon','Guest_EmailAddress'),
			('Epsilon','Gender'),
			('Epsilon','Location'),
			('Epsilon','PostalCode'),
			('Epsilon','Country'),
			('Epsilon','State'),
			('Epsilon','City')
	EXEC operations.TruncateTablesWithinTableList
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Gender]'
GO
CREATE TABLE [dbo].[Gender]
(
[GenderID] [int] NOT NULL IDENTITY(1, 1),
[GenderName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BSI_ID] [int] NULL,
[Epsilon_ID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Gender] on [dbo].[Gender]'
GO
ALTER TABLE [dbo].[Gender] ADD CONSTRAINT [PK_dbo_Gender] PRIMARY KEY CLUSTERED  ([GenderID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_GuestInfo]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Guest DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_GuestInfo] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_GuestInfo]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- Populate Guest_EmailAddress -----------------------
	MERGE INTO dbo.Guest_EmailAddress AS tgt
	USING
	(
		SELECT DISTINCT [Guest_EmailAddressID],[emailAddress] FROM [Epsilon].[Guest_EmailAddress]
	) AS src ON src.[emailAddress] = tgt.[emailAddress]
	WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.[Guest_EmailAddressID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([emailAddress],[Epsilon_ID])
		VALUES(src.[emailAddress],src.[Guest_EmailAddressID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------

	-- Populate Gender -----------------------
	MERGE INTO dbo.Gender AS tgt
	USING
	(
		SELECT DISTINCT GenderID, GenderName FROM epsilon.Gender
	) AS src ON src.GenderName = tgt.GenderName
	WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.GenderID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(GenderName,[Epsilon_ID])
		VALUES(src.GenderName,GenderID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------


	-- Populate Guest -----------------------------------
	MERGE INTO dbo.Guest AS tgt
	USING
	(
		SELECT [FirstName],[LastName],[Address1],[Address2],loc.[LocationID],gea.[Guest_EmailAddressID],[GuestID],[LoyaltyNumberID],g.birthdate,ge.genderID, g.QueueID
		FROM [Epsilon].[Guest] g
			LEFT JOIN dbo.[Location] loc ON loc.Epsilon_ID = g.LocationID
			LEFT JOIN dbo.Gender ge ON ge.Epsilon_ID = g.GenderID
			LEFT JOIN dbo.[Guest_EmailAddress] gea ON gea.Epsilon_ID = g.[Guest_EmailAddressID]
		WHERE g.QueueID = @QueueID
	) AS src ON src.[LoyaltyNumberID] = tgt.[LoyaltyNumberID]
	WHEN MATCHED THEN
		UPDATE
			SET [FirstName] = src.[FirstName],
				[LastName] = src.[LastName],
				[Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Guest_EmailAddressID] = src.[Guest_EmailAddressID],
				[Epsilon_ID] = src.[GuestID]
				,tgt.birthdate = src.birthdate
				,tgt.genderID = src.genderID
				, tgt.QueueID = src.QueueID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([FirstName],[LastName],[Address1],[Address2],[LocationID],[Guest_EmailAddressID],[Epsilon_ID],[LoyaltyNumberID],birthdate,genderID,QueueID)
		VALUES(src.[FirstName],src.[LastName],src.[Address1],src.[Address2],src.[LocationID],src.[Guest_EmailAddressID],src.[GuestID],src.[LoyaltyNumberID],src.birthdate,src.genderID,src.QueueID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_Location]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Guest DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Location] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
CREATE PROCEDURE [dbo].[Epsilon_Populate_Location]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- City --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate City'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.City AS tgt
		USING
		(
			SELECT DISTINCT [CityID],[CityName] FROM epsilon.City
		) AS src ON src.CityName = tgt.CityName
		WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.CityID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityName,[Epsilon_ID])
			VALUES(src.CityName,src.CityID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- State -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate State'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[State] AS tgt
		USING
		(
			SELECT DISTINCT [StateID],[StateName] FROM [Epsilon].[State]
		) AS src ON src.[StateName] = tgt.[StateName]
		WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.[StateID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([StateName],[Epsilon_ID])
			VALUES(src.[StateName],src.[StateID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Country -----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Country'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.Country AS tgt
		USING
		(
			SELECT DISTINCT [CountryID],[CountryName] FROM [Epsilon].[Country]
		) AS src ON src.[CountryName] = tgt.CountryName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([CountryName],[Epsilon_ID])
			VALUES(src.[CountryName],src.[CountryID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Postal Code -------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Postal Code'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[PostalCode] AS tgt
		USING
		(
			SELECT DISTINCT [PostalCodeID],[PostalCodeName] FROM [Epsilon].[PostalCode]
		) AS src ON src.[PostalCodeName] = tgt.[PostalCodeName]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([PostalCodeName],[Epsilon_ID])
			VALUES(src.[PostalCodeName],src.[PostalCodeID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Location ----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Location'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[Location] AS tgt
		USING
		(
			SELECT l.LocationID,cit.CityID,st.StateID,con.CountryID,pc.PostalCodeID
			FROM Epsilon.[Location] l
				LEFT JOIN dbo.City cit ON cit.Epsilon_ID = l.CityID
				LEFT JOIN dbo.State st ON st.Epsilon_ID = l.StateID
				LEFT JOIN dbo.Country con ON con.Epsilon_ID = l.CountryID
				LEFT JOIN dbo.PostalCode pc ON pc.Epsilon_ID = l.PostalCodeID
		) AS src ON src.CityID = tgt.CityID AND src.StateID = tgt.StateID AND src.CountryID = tgt.CountryID AND src.PostalCodeID = tgt.PostalCodeID
		WHEN MATCHED THEN
			UPDATE
				SET Epsilon_ID = src.LocationID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityID,StateID,CountryID,PostalCodeID,Epsilon_ID)
			VALUES(src.CityID,src.StateID,src.CountryID,src.PostalCodeID,src.LocationID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DDL_EVENTS]'
GO
CREATE TABLE [dbo].[DDL_EVENTS]
(
[EventType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostTime] [datetime2] NULL,
[SPID] [int] NULL,
[ServerName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatabaseName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchemaName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SetOptions] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommandText] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[Location]'
GO
ALTER TABLE [BSI].[Location] ADD CONSTRAINT [FK_BSI_Location_City] FOREIGN KEY ([CityID]) REFERENCES [BSI].[City] ([CityID])
GO
ALTER TABLE [BSI].[Location] ADD CONSTRAINT [FK_BSI_Location_Country] FOREIGN KEY ([CountryID]) REFERENCES [BSI].[Country] ([CountryID])
GO
ALTER TABLE [BSI].[Location] ADD CONSTRAINT [FK_BSI_Location_State] FOREIGN KEY ([StateID]) REFERENCES [BSI].[State] ([StateID])
GO
ALTER TABLE [BSI].[Location] ADD CONSTRAINT [FK_BSI_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [BSI].[PostalCode] ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[Guest]'
GO
ALTER TABLE [BSI].[Guest] ADD CONSTRAINT [FK_BSI_Guest] FOREIGN KEY ([LocationID]) REFERENCES [BSI].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [BSI].[Guest_EmailAddress]'
GO
ALTER TABLE [BSI].[Guest_EmailAddress] ADD CONSTRAINT [FK_BSI_Guest_EmailAddress] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [BSI].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_City] FOREIGN KEY ([CityID]) REFERENCES [dbo].[City] ([CityID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[State] ([StateID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [dbo].[PostalCode] ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [FK_dbo_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [FK_dbo_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [dbo].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Guest_EmailAddress]'
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD CONSTRAINT [FK_dbo_Guest_EmailAddress] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [dbo].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [epsilon].[Location]'
GO
ALTER TABLE [epsilon].[Location] ADD CONSTRAINT [FK_epsilon_Location_City] FOREIGN KEY ([CityID]) REFERENCES [epsilon].[City] ([CityID])
GO
ALTER TABLE [epsilon].[Location] ADD CONSTRAINT [FK_epsilon_Location_Country] FOREIGN KEY ([CountryID]) REFERENCES [epsilon].[Country] ([CountryID])
GO
ALTER TABLE [epsilon].[Location] ADD CONSTRAINT [FK_epsilon_Location_State] FOREIGN KEY ([StateID]) REFERENCES [epsilon].[State] ([StateID])
GO
ALTER TABLE [epsilon].[Location] ADD CONSTRAINT [FK_epsilon_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [epsilon].[PostalCode] ([PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating DDL triggers'
GO

CREATE TRIGGER [trig_ddl_all] ON DATABASE 
	FOR DDL_DATABASE_LEVEL_EVENTS 
AS 
	DECLARE @data XML;
	SET @data = EVENTDATA();
	
	INSERT INTO DDL_EVENTS(EventType,PostTime,SPID,ServerName,LoginName,UserName,DatabaseName,SchemaName
							,ObjectName,ObjectType,SetOptions,CommandText)
	VALUES
	(
		@data.value('(/EVENT_INSTANCE/EventType)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/PostTime)[1]','datetime2(7)'),
		@data.value('(/EVENT_INSTANCE/SPID)[1]','int'),
		@data.value('(/EVENT_INSTANCE/ServerName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/LoginName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/UserName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/DatabaseName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/SchemaName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/ObjectType)[1]','nvarchar(255)'),
		@data.value('(/EVENT_INSTANCE/TSQLCommand/SetOptions)[1]','nvarchar(MAX)'),
		@data.value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]','nvarchar(MAX)')
	);
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
