USE Guests
GO

/*
Run this script on:

        chi-lt-00032377.Guests    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Guests

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/14/2019 1:25:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[Location]'
GO
ALTER TABLE [BSI].[Location] DROP CONSTRAINT [FK_BSI_Location_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[Location] DROP CONSTRAINT [FK_BSI_Location_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[Location] DROP CONSTRAINT [FK_BSI_Location_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[Location] DROP CONSTRAINT [FK_BSI_Location_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[Guest]'
GO
ALTER TABLE [BSI].[Guest] DROP CONSTRAINT [FK_BSI_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [BSI].[Guest_EmailAddress]'
GO
ALTER TABLE [BSI].[Guest_EmailAddress] DROP CONSTRAINT [FK_BSI_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [FK_dbo_Location_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [FK_dbo_Guest_LocationID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [FK_dbo_Guest_Guest_EmailAddressID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Guest_EmailAddress]'
GO
ALTER TABLE [dbo].[Guest_EmailAddress] DROP CONSTRAINT [FK_dbo_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [epsilon].[Location]'
GO
ALTER TABLE [epsilon].[Location] DROP CONSTRAINT [FK_epsilon_Location_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[Location] DROP CONSTRAINT [FK_epsilon_Location_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[Location] DROP CONSTRAINT [FK_epsilon_Location_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[Location] DROP CONSTRAINT [FK_epsilon_Location_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[City]'
GO
ALTER TABLE [BSI].[City] DROP CONSTRAINT [PK_BSI_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Country]'
GO
ALTER TABLE [BSI].[Country] DROP CONSTRAINT [PK_BSI_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Guest]'
GO
ALTER TABLE [BSI].[Guest] DROP CONSTRAINT [PK_BSI_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Guest_EmailAddress]'
GO
ALTER TABLE [BSI].[Guest_EmailAddress] DROP CONSTRAINT [PK_BSI_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[Location]'
GO
ALTER TABLE [BSI].[Location] DROP CONSTRAINT [PK_BSI_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[PostalCode]'
GO
ALTER TABLE [BSI].[PostalCode] DROP CONSTRAINT [PK_BSI_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [BSI].[State]'
GO
ALTER TABLE [BSI].[State] DROP CONSTRAINT [PK_BSI_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[City]'
GO
ALTER TABLE [dbo].[City] DROP CONSTRAINT [PK_dbo_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Country]'
GO
ALTER TABLE [dbo].[Country] DROP CONSTRAINT [PK_dbo_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Gender]'
GO
ALTER TABLE [dbo].[Gender] DROP CONSTRAINT [PK_dbo_Gender]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [PK_dbo_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Guest_EmailAddress]'
GO
ALTER TABLE [dbo].[Guest_EmailAddress] DROP CONSTRAINT [PK_dbo_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] DROP CONSTRAINT [PK_dbo_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[PostalCode]'
GO
ALTER TABLE [dbo].[PostalCode] DROP CONSTRAINT [PK_dbo_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[State]'
GO
ALTER TABLE [dbo].[State] DROP CONSTRAINT [PK_dbo_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[City]'
GO
ALTER TABLE [epsilon].[City] DROP CONSTRAINT [PK_BSI_City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Country]'
GO
ALTER TABLE [epsilon].[Country] DROP CONSTRAINT [PK_epsilon_Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Gender]'
GO
ALTER TABLE [epsilon].[Gender] DROP CONSTRAINT [PK_epsilon_Gender]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Guest]'
GO
ALTER TABLE [epsilon].[Guest] DROP CONSTRAINT [PK_dbo_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Guest_EmailAddress]'
GO
ALTER TABLE [epsilon].[Guest_EmailAddress] DROP CONSTRAINT [PK_epsilon_Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[Location]'
GO
ALTER TABLE [epsilon].[Location] DROP CONSTRAINT [PK_epsilon_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[PostalCode]'
GO
ALTER TABLE [epsilon].[PostalCode] DROP CONSTRAINT [PK_epsilon_PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [epsilon].[State]'
GO
ALTER TABLE [epsilon].[State] DROP CONSTRAINT [PK_BSI_State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [operations].[Truncate_List]'
GO
ALTER TABLE [operations].[Truncate_List] DROP CONSTRAINT [PK_operations_Truncate_List]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [operations].[Truncate_Log]'
GO
ALTER TABLE [operations].[Truncate_Log] DROP CONSTRAINT [df_CreateDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_City] from [BSI].[City]'
GO
DROP INDEX [UX_BSI_City] ON [BSI].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_Country] from [BSI].[Country]'
GO
DROP INDEX [UX_BSI_Country] ON [BSI].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_EmailAddress] from [BSI].[Guest_EmailAddress]'
GO
DROP INDEX [UX_BSI_EmailAddress] ON [BSI].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_CityID_StateID_CountryID_PostalCodeID] from [BSI].[Location]'
GO
DROP INDEX [UX_CityID_StateID_CountryID_PostalCodeID] ON [BSI].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_PostalCode] from [BSI].[PostalCode]'
GO
DROP INDEX [UX_BSI_PostalCode] ON [BSI].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_BSI_State] from [BSI].[State]'
GO
DROP INDEX [UX_BSI_State] ON [BSI].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping DDL triggers'
GO
DROP TRIGGER [trig_ddl_all] ON DATABASE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DDL_EVENTS]'
GO
DROP TABLE [dbo].[DDL_EVENTS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_Location]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Epsilon_Populate_GuestInfo]'
GO
DROP PROCEDURE [dbo].[Epsilon_Populate_GuestInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Gender]'
GO
DROP TABLE [dbo].[Gender]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[TruncateForReload]'
GO
DROP PROCEDURE [epsilon].[TruncateForReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_Location]'
GO
DROP PROCEDURE [epsilon].[Populate_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Populate_GuestInfo]'
GO
DROP PROCEDURE [epsilon].[Populate_GuestInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Gender]'
GO
DROP TABLE [epsilon].[Gender]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Guest]'
GO
DROP TABLE [epsilon].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Guest_EmailAddress]'
GO
DROP TABLE [epsilon].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[vw_Location]'
GO
DROP VIEW [epsilon].[vw_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[TruncateForReload]'
GO
DROP PROCEDURE [dbo].[TruncateForReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_GuestInfo]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_GuestInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[BSI_Populate_Location]'
GO
DROP PROCEDURE [dbo].[BSI_Populate_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[TruncateForReload]'
GO
DROP PROCEDURE [BSI].[TruncateForReload]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [operations].[TruncateTablesWithinTableList]'
GO
DROP PROCEDURE [operations].[TruncateTablesWithinTableList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [operations].[Truncate_List]'
GO
DROP TABLE [operations].[Truncate_List]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [operations].[Truncate_Log]'
GO
DROP TABLE [operations].[Truncate_Log]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_Location]'
GO
DROP PROCEDURE [BSI].[Populate_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Populate_GuestInfo]'
GO
DROP PROCEDURE [BSI].[Populate_GuestInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Guest_EmailAddress]'
GO
DROP TABLE [BSI].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[vw_Location]'
GO
DROP VIEW [BSI].[vw_Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[State]'
GO
DROP TABLE [epsilon].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[PostalCode]'
GO
DROP TABLE [epsilon].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Country]'
GO
DROP TABLE [epsilon].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[Location]'
GO
DROP TABLE [epsilon].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [epsilon].[City]'
GO
DROP TABLE [epsilon].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[State]'
GO
DROP TABLE [dbo].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[PostalCode]'
GO
DROP TABLE [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Country]'
GO
DROP TABLE [dbo].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[City]'
GO
DROP TABLE [dbo].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Location]'
GO
DROP TABLE [dbo].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Guest]'
GO
DROP TABLE [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Guest_EmailAddress]'
GO
DROP TABLE [dbo].[Guest_EmailAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[State]'
GO
DROP TABLE [BSI].[State]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[PostalCode]'
GO
DROP TABLE [BSI].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Country]'
GO
DROP TABLE [BSI].[Country]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[City]'
GO
DROP TABLE [BSI].[City]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Guest]'
GO
DROP TABLE [BSI].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Location]'
GO
DROP TABLE [BSI].[Location]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping schemas'
GO
DROP SCHEMA [BSI]
GO
DROP SCHEMA [epsilon]
GO
DROP SCHEMA [operations]
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
