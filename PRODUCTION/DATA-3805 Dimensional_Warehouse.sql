USE Dimensional_Warehouse
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 9/17/2024 5:32:02 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [temp].[Populate_Reservations_From_Reservations]'
GO


ALTER   PROCEDURE [temp].[Populate_Reservations_From_Reservations]
	@minDate date,
	@maxDate date,
	@OldestDate date,
	@Last_QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @rVersion varbinary(8)
	SELECT @rVersion = MAX(ISNULL(rVersion,0)) FROM fact.Reservation


	-- #CUR_DATE ------------------------------
	IF OBJECT_ID('tempdb..#CUR_DATE') IS NOT NULL
		DROP TABLE #CUR_DATE;
	CREATE TABLE #CUR_DATE
	(
		FullDate date NOT NULL,
		currencyCode nvarchar(3) NOT NULL,
		DateKey int NOT NULL,
		USD_ExchangeRate decimal(18,9) NOT NULL,
		currencyKey int NOT NULL,
		PRIMARY KEY CLUSTERED(FullDate,currencyCode)
	)

	INSERT INTO #CUR_DATE(FullDate,currencyCode,DateKey,USD_ExchangeRate,currencyKey)
	SELECT DISTINCT dd.[Full Date],cur.[Currency Code],dd.DateKey,ce.[USD Exchange Rate],cur.currencyKey
	FROM dim.DimDate dd
		INNER JOIN fact.CurrencyExchange ce ON ce.DateKey = dd.DateKey
		INNER JOIN dim.Currency cur ON cur.currencyKey = ce.currencyKey
	WHERE cur.[Currency Code] IN(SELECT DISTINCT currency FROM Reservations.dbo.TransactionDetail)
		AND dd.[Full Date] BETWEEN @minDate AND @maxDate
	--------------------------------------------

	-- POPULATE temp.Reservations --------------
	;WITH cte_Res
	(
		[Booking System],[Confirmation Number],[GDS Booked],[GDS Record Locator],
		[Confirmation Date],[Arrival Date],[Departure Date],[Cancellation Date],
		[hotelKey],[bookingSourceKey],[voiceAgentKey],[rateKey],[roomKey],[crsChainKey],
		[formOfPaymentKey],[bookingStatusKey],[partyDetailsKey],[loyaltyDetailsKey],[guestKey],
		[pseudoCityKey],[travelAgencyKey],[corporateAccountKey],[leisureAccountKey],[Lead Time],
		[Length Of Stay],[Rooms],[Nights],[Room Nights],[Original Currency],
		[Revenue Original Currency],[Travel Agency Commission Original Currency],
		[Revenue USD Confirmation Date],[Travel Agency Commission USD Confirmation Date],
		[Revenue USD Arrival Date],[Travel Agency Commission USD Arrival Date],
		[Revenue USD Departure Date],[Travel Agency Commission USD Departure Date],
		[Travel Agency Commission Percentage],
		[Count Of Bookings],[Loyalty Program Points],[PH Cost USD],[PH Revenue USD],[PH Surcharges USD],
		[PH Profit USD],[PH Brand Revenue],[Itinerary Number],[Children Count],[Party Size],
		[New Enrollment],[I Prefer Number Imputed],
		rVersion,[Internal Program]
	)
	AS
	(
		SELECT DISTINCT ISNULL(dbs.[Booking System],0),t.confirmationNumber,'Unknown','Unknown',
			confDate.DateKey,arvlDate.DateKey,depDate.DateKey,cnclDate.DateKey,
			ISNULL(dh.hotelKey,0),ISNULL(dbs.bookingSourceKey,0),ISNULL(dva.voiceAgentKey,0),ISNULL(dr.rateKey,0),ISNULL(droom.roomKey,0),ISNULL(dchain.crsChainKey,0),
			ISNULL(fop.formOfPaymentKey,0),ISNULL(dbstat.bookingStatusKey,0), ISNULL(pd.partyDetailsKey,0) ,ISNULL(dld.loyaltyDetailsKey,0),ISNULL(dg.guestKey,0),
			0 ,ISNULL(dta.travelAgencyKey,0), 0 , 0 ,ISNULL(DATEDIFF(day,confDate.[Full Date],arvlDate.[Full Date]),0),
			ISNULL(DATEDIFF(day,arvlDate.[Full Date],depDate.[Full Date]),0),td.rooms,td.nights,td.rooms * td.nights,ISNULL(CurConfDate.currencyKey,0),
			ISNULL(td.reservationRevenue,0),ISNULL(td.[commisionPercent] * td.reservationRevenue,0),
			ISNULL(CurConfDate.USD_ExchangeRate * td.reservationRevenue,0),ISNULL(CurConfDate.USD_ExchangeRate * td.[commisionPercent] * td.reservationRevenue,0),
			ISNULL(curArvlDate.USD_ExchangeRate * td.reservationRevenue,0),ISNULL(curArvlDate.USD_ExchangeRate * td.[commisionPercent] * td.reservationRevenue,0),
			ISNULL(curDepDate.USD_ExchangeRate * td.reservationRevenue,0),ISNULL(curDepDate.USD_ExchangeRate * td.[commisionPercent] * td.reservationRevenue,0),
			ISNULL(td.[commisionPercent],0),
			1,0,0,0,0,
			0,'Unknown',ISNULL(t.itineraryNumber,'Unknown'),
			td.childrenCount,td.totalGuestCount,
			CASE t.LoyaltyNumber_IsNewMember WHEN 0 THEN 'Repeat guest' WHEN 1 THEN 'New enrollment' ELSE 'Unknown' END AS [New Enrollment],
			CASE t.LoyaltyNumber_IsImputedFromEmail  WHEN 0 THEN 'Guest did not enter number' WHEN 1 THEN 'Guest entered number' ELSE 'Unknown' END AS [I Prefer Number Imputed],
			t.rVersion,
			COALESCE(aws.[program_name],ff.[program_name], 'Ambiguous') [Internal Program]
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
			LEFT JOIN dim.PartyDetail pd ON pd.sourceKey = td.TransactionDetailID
			LEFT JOIN #CUR_DATE CurConfDate ON CurConfDate.FullDate = ts.confirmationDate AND CurConfDate.currencyCode = td.currency
			LEFT JOIN #CUR_DATE curArvlDate ON curArvlDate.FullDate = td.arrivalDate AND curArvlDate.currencyCode = td.currency
			LEFT JOIN #CUR_DATE curDepDate ON curDepDate.FullDate = td.departureDate AND curDepDate.currencyCode = td.currency
			LEFT JOIN dim.DimDate confDate ON confDate.[Full Date] = ts.confirmationDate
			LEFT JOIN dim.DimDate arvlDate ON arvlDate.[Full Date] = td.arrivalDate
			LEFT JOIN dim.DimDate depDate ON depDate.[Full Date] = td.departureDate
			LEFT JOIN dim.DimDate cnclDate ON cnclDate.[Full Date] = ts.cancellationDate
			LEFT JOIN Reservations.dbo.hotel rh ON rh.HotelID = t.HotelID
			LEFT JOIN dim.Hotel dh ON dh.sourceKey = rh.Hotel_hotelID
			LEFT JOIN temp.BookingSource dbs ON dbs.CRS_sourceKey = t.CRS_BookingSourceID AND dbs.PH_sourceKey = ISNULL(t.PH_BookingSourceID,0)
			LEFT JOIN Reservations.dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
			LEFT JOIN dim.VoiceAgent dva ON dva.[Voice Agent User Name] = va.VoiceAgent

			LEFT JOIN Reservations.dbo.RateCode rtc ON rtc.RateCodeID = t.RateCodeID
			LEFT JOIN Reservations.dbo.RateCategory rc ON rc.RateCategoryID = t.RateCategoryID
			LEFT JOIN Reservations.dbo.PromoCode pc ON pc.PromoCodeID = t.PromoCodeID
			LEFT JOIN Reservations.dbo.CorporateCode cc ON cc.CorporateCodeID = t.CorporateCodeID
			LEFT JOIN dim.Rate dr ON dr.[Rate Code] = rtc.RateCode
								AND dr.[Rate Name] = rtc.RateName
								AND dr.[Rate Category] = ISNULL(rc.rateCategoryCode,'Unassigned')
								AND dr.[Promo Code] = ISNULL(pc.promotionalCode,'None')
								AND dr.[Corporate Code] = ISNULL(cc.corporationCode,'None')
								AND dr.Commissionable = CASE WHEN ISNULL(td.commisionPercent,0) > 0 THEN 1 ELSE 0 END
			LEFT JOIN Reservations.dbo.RoomType rrt ON rrt.RoomTypeID = t.RoomTypeID
			LEFT JOIN [dim].[Room] droom ON droom.[Room Name] = rrt.roomTypeName AND droom.[Room Code] = rrt.roomTypeCode AND (droom.hotelKey IS NULL OR droom.hotelKey = dh.hotelKey)
			LEFT JOIN Reservations.dbo.Chain rchain ON rchain.ChainID = t.ChainID
			LEFT JOIN dim.CRS_Chain dchain ON dchain.[CRS Chain ID] = rchain.CRS_ChainID
			LEFT JOIN dim.FormOfPayment fop ON fop.[Form of Payment Code] = ts.creditCardType
			LEFT JOIN dim.BookingStatus dbstat ON dbstat.[Booking Status Name] = ts.[status]
			LEFT JOIN Reservations.dbo.LoyaltyProgram rlp ON rlp.LoyaltyProgramID = t.LoyaltyProgramID
			LEFT JOIN dim.LoyaltyDetail dld ON dld.IsOptIn = ISNULL(td.optIn,0)
												AND dld.IsTagged = ISNULL(td.LoyaltyNumberTagged,0)
												AND dld.IsValid =	CASE td.LoyaltyNumberValidated
																		WHEN 1 THEN 1
																		WHEN 0 THEN CASE TRIM(ISNULL(ln.loyaltyNumber,'')) WHEN '' THEN NULL ELSE 0 END
																	END
												AND dld.[Loyalty Program] = ISNULL(NULLIF(TRIM(rlp.LoyaltyProgram),''),'Unknown')
												AND dld.[Loyalty Number] = 
														CASE
															WHEN NULLIF(TRIM(ln.loyaltyNumber),'') IS NOT NULL THEN
																CASE td.LoyaltyNumberValidated
																	WHEN 1 THEN 'I Prefer Number'
																	ELSE 'Other Program Number'
																END
															ELSE 'None'
														END
			LEFT JOIN [map].[guestKey_Res_GuestID] dg ON dg.GuestID = t.GuestID
			LEFT JOIN Reservations.dbo.TravelAgent rta ON rta.TravelAgentID = t.TravelAgentID
			LEFT JOIN Reservations.dbo.IATANumber ia ON ia.IATANumberID = t.IATANumberID
			LEFT JOIN (SELECT [IATA Number],MAX(travelAgencyKey) AS travelAgencyKey FROM [dim].[TravelAgency] GROUP BY [IATA Number]) dta ON dta.[IATA Number] = ia.IATANumber
			LEFT JOIN [Reservations].[dbo].[KeyClubReservations] aws on aws.reservation_id = t.confirmationNumber
			LEFT JOIN [Reservations].[dbo].[FriendsAndFamily] ff on ff.reservation_id = t.confirmationNumber
		WHERE ts.confirmationDate > @OldestDate
			AND t.QueueID <= @Last_QueueID
			--AND t.rVersion > @rVersion
	)
	INSERT INTO [temp].[Reservation]([Booking System],[Confirmation Number],[GDS Booked],[GDS Record Locator],[Confirmation Date],[Arrival Date],[Departure Date],[Cancellation Date],
			[hotelKey],[bookingSourceKey],[voiceAgentKey],[rateKey],[roomKey],[crsChainKey],[formOfPaymentKey],[bookingStatusKey],[partyDetailsKey],[loyaltyDetailsKey],[guestKey],
			[pseudoCityKey],[travelAgencyKey],[corporateAccountKey],[leisureAccountKey],[Lead Time],[Length Of Stay],[Rooms],[Nights],[Room Nights],[Original Currency],
			[Revenue Original Currency],[Travel Agency Commission Original Currency],[Revenue USD Confirmation Date],[Travel Agency Commission USD Confirmation Date],
			[Revenue USD Arrival Date],[Travel Agency Commission USD Arrival Date],[Revenue USD Departure Date],[Travel Agency Commission USD Departure Date],
			[Travel Agency Commission Percentage],[Count Of Bookings],[Loyalty Program Points],[PH Cost USD],[PH Revenue USD],[PH Surcharges USD],[PH Profit USD],
			[PH Brand Revenue],[Itinerary Number],[Children Count],[Party Size],
			[New Enrollment],[I Prefer Number Imputed],[Organic Halo],[Ad Halo],
			rVersion,[Internal Program])
	SELECT [Booking System],[Confirmation Number],[GDS Booked],[GDS Record Locator],[Confirmation Date],[Arrival Date],[Departure Date],[Cancellation Date],
		[hotelKey],[bookingSourceKey],[voiceAgentKey],[rateKey],[roomKey],[crsChainKey],[formOfPaymentKey],[bookingStatusKey],[partyDetailsKey],[loyaltyDetailsKey],[guestKey],
		[pseudoCityKey],[travelAgencyKey],[corporateAccountKey],[leisureAccountKey],[Lead Time],[Length Of Stay],[Rooms],[Nights],[Room Nights],[Original Currency],
		[Revenue Original Currency],[Travel Agency Commission Original Currency],[Revenue USD Confirmation Date],[Travel Agency Commission USD Confirmation Date],
		[Revenue USD Arrival Date],[Travel Agency Commission USD Arrival Date],[Revenue USD Departure Date],[Travel Agency Commission USD Departure Date],
		[Travel Agency Commission Percentage],[Count Of Bookings],[Loyalty Program Points],[PH Cost USD],[PH Revenue USD],[PH Surcharges USD],[PH Profit USD],
		[PH Brand Revenue],[Itinerary Number],[Children Count],[Party Size],
		[New Enrollment],[I Prefer Number Imputed],'False','False',
		rVersion,[Internal Program]
	FROM cte_Res
	--------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_LoyaltyDetail]'
GO


ALTER PROCEDURE [dbo].[Populate_LoyaltyDetail]
AS
BEGIN

	;WITH cte_LNV
	AS
	(
		SELECT 'Invalid I Prefer Number' AS [Loyalty Number Valid]
			UNION
		SELECT 'Missing I Prefer Number' AS [Loyalty Number Valid]
			UNION
		SELECT 'Valid I Prefer Number' AS [Loyalty Number Valid]
	),
	cte_LNT
	AS
	(
		SELECT 'Missing Tag' AS [Loyalty Number Tagged]
			UNION
		SELECT 'Not Tagged' AS [Loyalty Number Tagged]
			UNION
		SELECT 'Tagged' AS [Loyalty Number Tagged]
	),
	cte_LOI
	AS
	(
		SELECT 'Missing Opt-in' AS [Loyalty Opt-In]
			UNION
		SELECT 'Not Opted-In' AS [Loyalty Opt-In]
			UNION
		SELECT 'Opt-In' AS [Loyalty Opt-In]
	),
	cte_LN
	AS
	(
		SELECT 'I Prefer Number' AS [Loyalty Number]
			UNION
		SELECT 'None' AS [Loyalty Number]
			UNION
		SELECT 'Other Program Number' AS [Loyalty Number]
	),
	cte_LP
	AS
	(
		SELECT DISTINCT lp.LoyaltyProgram AS [Loyalty Program]
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		WHERE lp.LoyaltyProgram NOT IN(SELECT DISTINCT [Loyalty Program] FROM dim.LoyaltyDetail)
			AND NULLIF(lp.LoyaltyProgram,'') IS NOT NULL
	),
	cte_Valid
	AS
	(
		SELECT DISTINCT
			ISNULL(td.optIn,0) AS optIn,
			ISNULL(td.LoyaltyNumberTagged,0) AS LoyaltyNumberTagged,
			CASE td.LoyaltyNumberValidated
				WHEN 1 THEN 1
				WHEN 0 THEN CASE TRIM(ISNULL(ln.loyaltyNumber,'')) WHEN '' THEN NULL ELSE 0 END
			END AS LoyaltyNumberValidated,
			ISNULL(NULLIF(rlp.LoyaltyProgram,''),'Unknown') AS LoyaltyProgram,
			CASE
				WHEN NULLIF(ln.loyaltyNumber,'') IS NOT NULL THEN
					CASE td.LoyaltyNumberValidated
						WHEN 1 THEN 'I Prefer Number'
						ELSE
							CASE TRIM(ISNULL(loyaltyNumber,'')) WHEN '' THEN 'None' ELSE 'Other Program Number' END
					END
				ELSE 'None'
			END AS loyaltyNumber
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
			LEFT JOIN Reservations.dbo.LoyaltyProgram rlp ON rlp.LoyaltyProgramID = t.LoyaltyProgramID
	),
	cte_LoyaltyDetail
	AS
	(
		SELECT lnv.[Loyalty Number Valid],lnt.[Loyalty Number Tagged],loi.[Loyalty Opt-In],lp.[Loyalty Program],
				CASE lnv.[Loyalty Number Valid] WHEN 'Valid I Prefer Number' THEN 1 WHEN 'Invalid I Prefer Number' THEN 0 ELSE NULL END AS [IsValid],
				CASE lnt.[Loyalty Number Tagged] WHEN 'Tagged' THEN 1 WHEN 'Not Tagged' THEN 0 ELSE NULL END AS [IsTagged],
				CASE loi.[Loyalty Opt-In] WHEN 'Opt-In' THEN 1 WHEN 'Not Opted-In' THEN 0 ELSE NULL END AS [IsOptIn],
				ln.[Loyalty Number]
		FROM cte_LP AS lp
			CROSS JOIN cte_LNV AS lnv
			CROSS JOIN cte_LNT AS lnt
			CROSS JOIN cte_LOI AS loi
			CROSS JOIN cte_LN AS ln
	)
	INSERT INTO [dim].[LoyaltyDetail]([Loyalty Number Valid],[Loyalty Number Tagged],[Loyalty Opt-In],[Loyalty Program],[IsValid],[IsTagged],[IsOptIn],[Loyalty Number])
	SELECT ld.[Loyalty Number Valid],ld.[Loyalty Number Tagged],ld.[Loyalty Opt-In],ld.[Loyalty Program],ld.[IsValid],ld.[IsTagged],ld.[IsOptIn],ld.[Loyalty Number]
	FROM cte_LoyaltyDetail ld
		INNER JOIN cte_Valid valid ON ld.IsOptIn = valid.optIn
								AND ld.IsTagged = valid.LoyaltyNumberTagged
								AND ld.IsValid = valid.LoyaltyNumberValidated
								AND ld.[Loyalty Program] = valid.LoyaltyProgram
								AND ld.[Loyalty Number] = valid.loyaltyNumber
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
