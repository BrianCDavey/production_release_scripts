USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/28/2020 8:08:04 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_TransactionDetailedReport]'
GO






CREATE VIEW [dbo].[vw_TransactionDetailedReport]
AS
		SELECT p.QueueID AS [QueueID],p.PointActivityID AS [Transaction_Id],
				acs.ActivityCauseSystemName AS [Transaction_Source],p.[Notes] AS [Remarks],
				l.loyaltyNumberName AS [iPrefer_Number],
				CASE WHEN g.GuestID IS NULL THEN 'D' ELSE 'E' END AS [Member_Status],
				p.TransactionNumber AS [Booking_ID],hh.HotelCode AS [Hotel_Code],
				ISNULL(td.arrivalDate,p.[ActivityCauseDate]) AS [Arrival_Date],td.departureDate AS [Departure_Date],
				pc.PH_Channel AS [Booking_Source],actT.[ActivityTypeName] AS [Campaign],
				CASE WHEN p.Points > 0 THEN p.Points ELSE 0 END AS [Points_Earned],
				CASE WHEN p.Points < 0 THEN p.Points * -1 ELSE 0 END AS [Points_Redemeed]
				,CASE WHEN ISNULL(NULLIF(p.[ActivityCauseCurrency],''),'USD') = 'USD'  THEN CAST(p.Points AS decimal(20,5))/10 ELSE CurrencyRates.dbo.convertCurrency( CAST(p.Points AS decimal(20,5))/10,'USD',p.[ActivityCauseCurrency],CASE WHEN p.CurrencyExchangeDate = '2001-01-01 00:00:00.000' OR p.CurrencyExchangeDate IS NULL THEN p.ActivityDate ELSE p.CurrencyExchangeDate END) END AS [Reservation_Revenue],
				COALESCE(NULLIF(p.[ActivityCauseCurrency],''),'USD') AS [Currency_Code],
				p.[ActivityDate] AS [Transaction_Date],
				CASE WHEN p.CurrencyExchangeDate = '2001-01-01 00:00:00.000' OR p.CurrencyExchangeDate IS NULL THEN p.ActivityDate ELSE p.CurrencyExchangeDate END AS [Reward_Posting_Date],
				ISNULL(h.MainBrandCode,hr.MainBrandCode) AS [Hotel_Brand],ISNULL(h.HotelName,hh.HotelName) AS [Hotel_Name],
				0.00 AS [Value_of_Redemption_USD],CAST(p.Points AS decimal(20,5))/10 AS [Amount_Spent_USD],NULL AS IP_TDR_ID,
				p.IsForMigration, p.PointActivityID
		  FROM [Loyalty].[dbo].[PointActivity] p
			  LEFT JOIN loyalty.dbo.ActivityCauseSystem acs ON acs.ActivityCauseSystemID = p.ActivityCauseSystemID
			  INNER JOIN loyalty.[dbo].[LoyaltyNumber] l ON p.loyaltyNumberID = l.loyaltyNumberID
			  LEFT JOIN loyalty.[dbo].[ActivityType] actT ON actT.ActivityTypeID = p.ActivityTypeID
			  LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelID = p.ActivityCauseHotelID
			  LEFT JOIN Hotels.dbo.hotelsReporting hr ON hr.code = hh.HotelCode
			  LEFT JOIN Reservations.dbo.Transactions t ON t.confirmationNumber = p.TransactionNumber
			  LEFT JOIN Reservations.dbo.TransactionDetail td ON t.TransactionDetailID = td.TransactionDetailID
			  LEFT JOIN Reservations.dbo.Transactions tr ON tr.TransactionID = t.TransactionID
			  LEFT JOIN Reservations.dbo.PH_BookingSource b ON tr.PH_BookingSourceID = b.PH_BookingSourceID
			  LEFT JOIN Reservations.dbo.PH_Channel pc ON pc.PH_ChannelID = b.PH_ChannelID
			  LEFT JOIN Reservations.dbo.hotel ht ON ht.HotelID = t.HotelID 
			  LEFT JOIN Hotels.dbo.Hotel hre ON hre.HotelID = ht.Hotel_hotelID AND hre.HotelID = p.ActivityCauseHotelID
			  LEFT JOIN Hotels.dbo.hotelsReporting h ON h.code = hre.HotelCode
			  LEFT JOIN Guests.dbo.guest g ON g.LoyaltyNumberID = l.LoyaltyNumberID
		  WHERE  p.PointTypeID IN (1,3) -- BSI Point Credit AND Epsilon Point Credits
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[TransactionDetailedReport]'
GO
CREATE TABLE [dbo].[TransactionDetailedReport]
(
[QueueID] [int] NOT NULL,
[Transaction_Id] [bigint] NOT NULL,
[Transaction_Source] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iPrefer_Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Booking_ID] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel_Code] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Arrival_Date] [datetime] NULL,
[Departure_Date] [datetime] NULL,
[Booking_Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Campaign] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Points_Earned] [decimal] (28, 2) NULL,
[Points_Redemeed] [decimal] (28, 2) NULL,
[Reservation_Revenue] [decimal] (28, 2) NULL,
[Currency_Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transaction_Date] [datetime] NULL,
[Reward_Posting_Date] [datetime] NULL,
[Hotel_Brand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value_of_Redemption_USD] [decimal] (28, 2) NULL,
[Amount_Spent_USD] [decimal] (28, 2) NULL,
[IP_TDR_ID] [int] NOT NULL IDENTITY(1, 1),
[PointActivityID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_TransactionDetailedReport] on [dbo].[TransactionDetailedReport]'
GO
ALTER TABLE [dbo].[TransactionDetailedReport] ADD CONSTRAINT [PK_dbo_TransactionDetailedReport] PRIMARY KEY CLUSTERED  ([IP_TDR_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_TransactionDetailedReport]'
GO



CREATE PROCEDURE [dbo].[Epsilon_Populate_TransactionDetailedReport]
AS


	DECLARE @ThirtyDaysAgo datetime = DATEADD(day,-30, CAST(GETDATE() AS DATE));

	MERGE INTO dbo.TransactionDetailedReport AS tgt
	USING
	(
		SELECT [QueueID], [Transaction_Id], [Transaction_Source], [Remarks], [iPrefer_Number], [Member_Status], [Booking_ID], [Hotel_Code], [Arrival_Date], [Departure_Date], [Booking_Source], [Campaign], [Points_Earned], [Points_Redemeed], [Reservation_Revenue], [Currency_Code], [Transaction_Date], [Reward_Posting_Date], [Hotel_Brand], [Hotel_Name], [Value_of_Redemption_USD], [Amount_Spent_USD], [IP_TDR_ID], [IsForMigration], [PointActivityID]
		  FROM [dbo].[vw_TransactionDetailedReport]
		  WHERE  [Transaction_Date] >= @ThirtyDaysAgo
	) AS src ON src.PointActivityID = tgt.PointActivityID
	WHEN MATCHED THEN
		UPDATE
		SET tgt.[QueueID] = src.[QueueID],
			tgt.[Transaction_Id] = src.[Transaction_Id],
			tgt.[Transaction_Source] = src.[Transaction_Source],
			tgt.[Remarks] = src.[Remarks],
			tgt.[iPrefer_Number] = src.[iPrefer_Number],
			tgt.[Member_Status] = src.[Member_Status],
			tgt.[Booking_ID] = src.[Booking_ID],
			tgt.[Hotel_Code] = src.[Hotel_Code],
			tgt.[Arrival_Date] = src.[Arrival_Date],
			tgt.[Departure_Date] = src.[Departure_Date],
			tgt.[Booking_Source] = src.[Booking_Source],
			tgt.[Campaign] = src.[Campaign],
			tgt.[Points_Earned] = src.[Points_Earned],
			tgt.[Points_Redemeed] = src.[Points_Redemeed],
			tgt.[Reservation_Revenue] = src.[Reservation_Revenue],
			tgt.[Currency_Code] = src.[Currency_Code],
			tgt.[Transaction_Date] = src.[Transaction_Date],
			tgt.[Reward_Posting_Date] = src.[Reward_Posting_Date],
			tgt.[Hotel_Brand] = src.[Hotel_Brand],
			tgt.[Hotel_Name] = src.[Hotel_Name],
			tgt.[Value_of_Redemption_USD] = src.[Value_of_Redemption_USD],
			tgt.[Amount_Spent_USD] = src.[Amount_Spent_USD]
		WHEN NOT MATCHED BY TARGET THEN
		INSERT([QueueID], [Transaction_Id], [Transaction_Source], [Remarks], [iPrefer_Number], [Member_Status], [Booking_ID], [Hotel_Code], [Arrival_Date], [Departure_Date], [Booking_Source], [Campaign], [Points_Earned], [Points_Redemeed], [Reservation_Revenue], [Currency_Code], [Transaction_Date], [Reward_Posting_Date], [Hotel_Brand], [Hotel_Name], [Value_of_Redemption_USD], [Amount_Spent_USD], [PointActivityID])
		VALUES([QueueID], [Transaction_Id], [Transaction_Source], [Remarks], [iPrefer_Number], [Member_Status], [Booking_ID], [Hotel_Code], [Arrival_Date], [Departure_Date], [Booking_Source], [Campaign], [Points_Earned], [Points_Redemeed], [Reservation_Revenue], [Currency_Code], [Transaction_Date], [Reward_Posting_Date], [Hotel_Brand], [Hotel_Name], [Value_of_Redemption_USD], [Amount_Spent_USD], [PointActivityID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
