USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/28/2020 9:34:48 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[qs_Populate_TransactionDetailedReport]'
GO



CREATE PROCEDURE [dbo].[qs_Populate_TransactionDetailedReport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @Processed bit = 0

	IF EXISTS(SELECT TOP 1 * FROM dbo.QueueStep WHERE QueueStepStatus = 0 AND StepName = 'TransactionDetailedReport')
    BEGIN

		DECLARE @TruncateBeforeLoad bit = 0,@QueueID int = NULL,@StepID int = NULL,@application varchar(255), @QueueReservation int = NULL

		EXEC dbo.qs_ProcessTopQueueItem 'TransactionDetailedReport',@QueueID OUTPUT,@StepID OUTPUT

		SET @QueueReservation = @QueueID

		WHILE @QueueID IS NOT NULL
		BEGIN
			IF @Processed = 0
			BEGIN
				EXEC Loyalty.dbo.Epsilon_Populate_TransactionDetailedReport

				SET @Processed = 1
			END

			EXEC dbo.qs_UpdateQueueStatus @QueueID,@StepID,2,'FINISHED',-1

			EXEC dbo.qs_ProcessTopQueueItem 'TransactionDetailedReport',@QueueID OUTPUT,@StepID OUTPUT
		END
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_Populate_TransactionDetailedReport]'
GO
CREATE PROCEDURE [dbo].[Epsilon_Populate_TransactionDetailedReport]
AS
BEGIN

	DECLARE @ThirtyDaysAgo datetime = DATEADD(day,-30, CAST(GETDATE() AS DATE));

	MERGE INTO dbo.TransactionDetailedReport AS tgt
	USING
	(
		SELECT [QueueID], [Transaction_Id], [Transaction_Source], [Remarks], [iPrefer_Number], [Member_Status], [Booking_ID], [Hotel_Code], [Arrival_Date], [Departure_Date], [Booking_Source], [Campaign], [Points_Earned], [Points_Redemeed], [Reservation_Revenue], [Currency_Code], [Transaction_Date], [Reward_Posting_Date], [Hotel_Brand], [Hotel_Name], [Value_of_Redemption_USD], [Amount_Spent_USD], [IP_TDR_ID], [IsForMigration], [PointActivityID]
		  FROM [dbo].[vw_TransactionDetailedReport]
		  WHERE  [Transaction_Date] >= @ThirtyDaysAgo
	) AS src ON src.PointActivityID = tgt.PointActivityID
	WHEN MATCHED THEN
		UPDATE
		SET tgt.[QueueID] = src.[QueueID],
			tgt.[Transaction_Id] = src.[Transaction_Id],
			tgt.[Transaction_Source] = src.[Transaction_Source],
			tgt.[Remarks] = src.[Remarks],
			tgt.[iPrefer_Number] = src.[iPrefer_Number],
			tgt.[Member_Status] = src.[Member_Status],
			tgt.[Booking_ID] = src.[Booking_ID],
			tgt.[Hotel_Code] = src.[Hotel_Code],
			tgt.[Arrival_Date] = src.[Arrival_Date],
			tgt.[Departure_Date] = src.[Departure_Date],
			tgt.[Booking_Source] = src.[Booking_Source],
			tgt.[Campaign] = src.[Campaign],
			tgt.[Points_Earned] = src.[Points_Earned],
			tgt.[Points_Redemeed] = src.[Points_Redemeed],
			tgt.[Reservation_Revenue] = src.[Reservation_Revenue],
			tgt.[Currency_Code] = src.[Currency_Code],
			tgt.[Transaction_Date] = src.[Transaction_Date],
			tgt.[Reward_Posting_Date] = src.[Reward_Posting_Date],
			tgt.[Hotel_Brand] = src.[Hotel_Brand],
			tgt.[Hotel_Name] = src.[Hotel_Name],
			tgt.[Value_of_Redemption_USD] = src.[Value_of_Redemption_USD],
			tgt.[Amount_Spent_USD] = src.[Amount_Spent_USD]
		WHEN NOT MATCHED BY TARGET THEN
		INSERT([QueueID], [Transaction_Id], [Transaction_Source], [Remarks], [iPrefer_Number], [Member_Status], [Booking_ID], [Hotel_Code], [Arrival_Date], [Departure_Date], [Booking_Source], [Campaign], [Points_Earned], [Points_Redemeed], [Reservation_Revenue], [Currency_Code], [Transaction_Date], [Reward_Posting_Date], [Hotel_Brand], [Hotel_Name], [Value_of_Redemption_USD], [Amount_Spent_USD], [PointActivityID])
		VALUES([QueueID], [Transaction_Id], [Transaction_Source], [Remarks], [iPrefer_Number], [Member_Status], [Booking_ID], [Hotel_Code], [Arrival_Date], [Departure_Date], [Booking_Source], [Campaign], [Points_Earned], [Points_Redemeed], [Reservation_Revenue], [Currency_Code], [Transaction_Date], [Reward_Posting_Date], [Hotel_Brand], [Hotel_Name], [Value_of_Redemption_USD], [Amount_Spent_USD], [PointActivityID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
