USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/18/2020 10:17:53 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[mrtJoined_Reservation]'
GO










ALTER VIEW [work].[mrtJoined_Reservation]
AS

     SELECT 
			CASE WHEN bs.openHospID IS NOT NULL THEN 2 WHEN bs.synXisID IS NOT NULL THEN 1 ELSE 'Unknown' END AS transactionSourceID, 
			t.TransactionID as transactionKey,
            t.confirmationNumber,
			hh.HotelCode AS phgHotelCode,
            COALESCE(hh.openhospID, hh.synXisID) AS crsHotelID,
            hh.HotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
			CASE WHEN ch.ChainID = 22 THEN 56 ELSE ch.ChainID END AS [ChainID],--change from CRS_ChainID since open hospitality CRS_ChainID is not integer
            ch.chainName,
            ts.status AS bookingStatus,
            CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN td.billingDescription ELSE 'Unknown' END AS synxisBillingDescription,
            cha.channel AS bookingChannel,
            sec.secondarySource AS bookingSecondarySource,
            sub.subSourceCode AS bookingSubSourceCode,
            COALESCE(te.templateGroupID, 2) AS bookingTemplateGroupId,
			CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END AS bookingTemplateAbbreviation,
            aibe.ibeSourceName AS xbeTemplateName,
            acro.CRO_Code AS CROcode,
            CASE WHEN acro.CRO_Code IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            rc.rateCategoryCode AS bookingRateCategoryCode,
            rac.RateCode AS bookingRateCode,
            ISNULL(iata.IATANumber,'') AS bookingIATA,
            t.transactionTimeStamp,
            ts.confirmationDate,
            td.arrivalDate,
            td.departureDate,
            ts.cancellationDate,
            ts.cancellationNumber,
            td.nights,
            td.rooms,
            td.nights * td.rooms AS roomNights,
            td.reservationRevenue AS roomRevenueInBookingCurrency,
            td.currency AS bookingCurrencyCode,
            t.timeLoaded,
			work.[billyItemCode](CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN td.billingDescription ELSE 'Unknown' END,cha.channel,sec.secondarySource,sub.subSourceCode,CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END,croCodes.croGroupID, 123) AS [ItemCode], --hard code chainId since OH chainID is not int
			CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN ts.confirmationDate ELSE td.arrivaldate END) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            CASE WHEN bs.openHospID IS NOT NULL THEN 2 WHEN bs.synXisID IS NOT NULL THEN 1 ELSE 'Unknown' END AS CRSSourceID,
			lp.LoyaltyProgram,
			ISNULL(ln.loyaltyNumber,'') AS loyaltyNumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],
			--force tag if negative iprefer returns
			 CASE WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberValidated,0)
					WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					WHEN work.billyLoyaltyFlipFlag(t.confirmationNumber) = 1 THEN 1
					ELSE ISNULL(td.LoyaltyNumberValidated,0) END AS LoyaltyNumberValidated
				--new change
			,CASE WHEN ISNULL(td.LoyaltyNumberTagged,0) = 0 THEN 0
					 WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberTagged,0)
					 WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					 ELSE ISNULL(td.LoyaltyNumberTagged,0)
					 END as LoyaltyNumberTagged
		FROM Reservations.dbo.Transactions t WITH(NOLOCK)
		INNER JOIN Reservations.dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN Reservations.dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN Reservations.dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN Reservations.dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
		LEFT JOIN Reservations.dbo.CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
		LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
		LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
		LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
		LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
		LEFT JOIN Reservations.dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN Reservations.dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN Reservations.dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID

        LEFT JOIN work.hotelActiveBrands ON hh.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hh.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hh.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON td.currency = bookingCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = bookingCE.EXCHDATE
		LEFT JOIN Loyalty.dbo.[TransactionDetailedReport] tdr ON tdr.Booking_ID = t.confirmationNumber AND tdr.Hotel_Code = hh.HotelCode
		AND MONTH(tdr.Reward_Posting_Date) = MONTH(td.arrivalDate) AND YEAR(tdr.Reward_Posting_Date) = YEAR(td.arrivalDate) AND tdr.Reservation_Revenue <> 0 AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source IN ('Admin Portal','Hotel Portal')
		LEFT JOIN [dbo].[CROCodes] ON acro.CRO_Code = croCodes.croCode
		LEFT JOIN dbo.Templates te ON te.xbeTemplateName = aibe.ibeSourceName

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
