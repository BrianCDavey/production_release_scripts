USE [ReservationBilling]
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/12/2020 7:40:17 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Hotels]'
GO


ALTER VIEW [dbo].[Hotels]
AS
SELECT        CAST(0 AS bit) AS isTemplate, Core.dbo.hotels.crmGuid, Core.dbo.hotels.code, Core.dbo.hotels_Synxis.synxisID, Core.dbo.hotels.hotelName, 
                         Core.dbo.hotels.physicalCity, Core.dbo.hotels.state, ISO.dbo.subAreas.subAreaName, Core.dbo.hotels.country, ISO.dbo.countries.shortName, 
                         gpCustomer.CURNCYID AS currencyCode, NULL AS geographicRegionCode, Core.dbo.geographicRegions.name AS geographicRegionName, 
                         COALESCE (activeBrands.code, inactiveBrands.code) AS mainBrandCode, COALESCE (account.phg_regionalmanageridname, N'None') 
                         AS phg_regionalmanageridName, COALESCE (account.phg_areamanageridname, N'None') AS phg_areamanageridName, account.statuscodename, 
                         COALESCE (account.phg_revenueaccountmanageridname, N'None') AS phg_revenueaccountmanageridName, 
                         COALESCE (account.phg_regionaladministrationidname, N'None') AS phg_regionaladministrationidName, account.phg_invoicepastduestatus, 
                         RDuser.internalemailaddress AS phg_rdEmailAddress, Core.dbo.hotels_OpenHospitality.openHospitalityCode
FROM            Core.dbo.hotels LEFT OUTER JOIN
                         IC.dbo.RM00101 AS gpCustomer ON Core.dbo.hotels.code = gpCustomer.CUSTNMBR LEFT OUTER JOIN
                             (SELECT        Core.dbo.hotels_brands.hotelCode, MIN(subBrands.heirarchy) AS mainHeirarchy
                               FROM            Core.dbo.hotels_brands INNER JOIN
                                                         Core.dbo.brands AS subBrands ON Core.dbo.hotels_brands.brandCode = subBrands.code
                               WHERE        (Core.dbo.hotels_brands.endDatetime IS NULL) AND (Core.dbo.hotels_brands.startDatetime <= GETDATE()) OR
                                                         (GETDATE() BETWEEN Core.dbo.hotels_brands.startDatetime AND Core.dbo.hotels_brands.endDatetime)
                               GROUP BY Core.dbo.hotels_brands.hotelCode) AS hotelActiveBrands ON Core.dbo.hotels.code = hotelActiveBrands.hotelCode LEFT OUTER JOIN
                         Core.dbo.brands AS activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy LEFT OUTER JOIN
                             (SELECT        hotelCode, MIN(heirarchy) AS mainHeirarchy
                               FROM            (SELECT        hotels_brands_2.hotelCode, hotels_brands_2.brandCode, Core.dbo.brands.heirarchy
                                                         FROM            Core.dbo.hotels_brands AS hotels_brands_2 INNER JOIN
                                                                                       (SELECT        hotelCode, MAX(endDatetime) AS maxEnd
                                                                                         FROM            Core.dbo.hotels_brands AS hotels_brands_1
                                                                                         GROUP BY hotelCode) AS maxDate ON hotels_brands_2.endDatetime = maxDate.maxEnd INNER JOIN
                                                                                   Core.dbo.brands ON hotels_brands_2.brandCode = Core.dbo.brands.code
                                                         GROUP BY hotels_brands_2.hotelCode, hotels_brands_2.brandCode, Core.dbo.brands.heirarchy) AS maxBrands
                               GROUP BY hotelCode) AS hotelInactiveBrands ON Core.dbo.hotels.code = hotelInactiveBrands.hotelCode LEFT OUTER JOIN
                         Core.dbo.brands AS inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy LEFT OUTER JOIN
                         LocalCRM.dbo.Account AS account ON Core.dbo.hotels.crmGuid = account.accountid LEFT OUTER JOIN
                         LocalCRM.dbo.SystemUser AS RDuser ON account.phg_regionalmanagerid = RDuser.systemuserid LEFT OUTER JOIN
                         Core.dbo.hotels_Synxis ON Core.dbo.hotels.code = Core.dbo.hotels_Synxis.hotelCode LEFT OUTER JOIN
                         Core.dbo.hotels_OpenHospitality ON Core.dbo.hotels.code = Core.dbo.hotels_OpenHospitality.hotelCode LEFT OUTER JOIN
                         ISO.dbo.countries ON Core.dbo.hotels.country = ISO.dbo.countries.code2 LEFT OUTER JOIN
                         ISO.dbo.subAreas ON Core.dbo.hotels.country = ISO.dbo.subAreas.countryCode2 AND Core.dbo.hotels.state = ISO.dbo.subAreas.subAreaCode AND ISO.dbo.subAreas.tracked = 1 LEFT OUTER JOIN
                         Core.dbo.geographicRegions ON Core.dbo.hotels.geographicRegionCode = Core.dbo.geographicRegions.code
WHERE        (Core.dbo.hotels.code NOT LIKE '%test%') AND (Core.dbo.hotels.code <> 'BCTS4') AND (Core.dbo.hotels_Synxis.synxisID > 0) OR
                         (Core.dbo.hotels.code NOT LIKE '%test%') AND (Core.dbo.hotels.code <> 'BCTS4') AND (Core.dbo.hotels_OpenHospitality.openHospitalityCode IS NOT NULL)
						 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
