USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/10/2020 6:44:26 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[tdrJoined_Reservation]'
GO



ALTER VIEW [test].[tdrJoined_Reservation]
AS
     SELECT 
			3 as transactionSourceID, --3 = I Prefer Manual Transactions
			CAST(tdr.Transaction_Id as nvarchar(20)) as transactionKey,
            tdr.Booking_ID as confirmationNumber, 
			hotels.HotelCode AS phgHotelCode,
            NULL AS crsHotelID,
            hotels.hotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            null as chainID,
            'IPM' as chainName,
            'IPM' as bookingStatus,
            'iPrefer Manual Entry' as synxisBillingDescription,
            'iPrefer Manual Entry' AS bookingChannel,
            'iPrefer Manual Entry' AS bookingSecondarySource,
            'iPrefer Manual Entry' AS bookingSubSourceCode,
            null as bookingTemplateGroupId,
            'IPM' as bookingTemplateAbbreviation,
            'IPM' as xbeTemplateName,
            'iPrefer Manual Entry' as CROcode,
            null as bookingCroGroupID,
            'iPrefer Manual Entry' as bookingRateCategoryCode,
            'iPrefer Manual' as bookingRateCode,
            'iPrefer Manual' as bookingIATA,
            tdr.Reward_Posting_Date as transactionTimeStamp,
            CAST((SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date), (tdr.Departure_Date), (tdr.Reward_Posting_Date)) AS Fields(d)) AS DATE) as confirmationDate,
            tdr.Arrival_Date as arrivalDate,
            tdr.Departure_Date as departureDate,
            CAST(null as date) as cancellationDate,
            CAST(null as varchar) as cancellationNumber,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as nights,
            1 as rooms,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as roomNights,
            tdr.Reservation_Revenue as roomRevenueInBookingCurrency,
            tdr.Currency_Code as bookingCurrencyCode,
            null as timeLoaded,
			'IPREFERMANUAL' AS [ItemCode],
			CAST(CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END AS DATE) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            null as CRSSourceID,
			'I Prefer' as loyaltyProgram,
			tdr.iPrefer_Number as loyaltyNumber,
			'iPrefer Manual Entry' as travelAgencyName,
			1 as LoyaltyNumberValidated,
			--CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END as LoyaltyNumberTagged, --Comment out in case we need in the future
			0 AS LoyaltyNumberTagged,
			tdr.Transaction_Source, 
			tdr.Booking_Source  

     FROM Loyalty.dbo.[TransactionDetailedReport] tdr
        LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
        LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.vw_local_exchange_rates hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CAST(CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END AS DATE) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.vw_local_exchange_rates bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
			AND CAST(CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END AS DATE) = bookingCE.EXCHDATE
		--LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
		--	AND tdr.Hotel_Code = tc.Hotel_Code
		--	AND tc.DateTagged <= tdr.Reward_Posting_Date --Comment out in case we need in the future


  WHERE tdr.Reservation_Revenue <> 0
	AND tdr.Points_Earned <> 0
	AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') --remove epsilon SFTP and all BSI old point activity
	AND tdr.Hotel_Code <> 'PHG123'
	AND tdr.IsForMigration = 0
	AND tdr.Hotel_Code IS NOT NULL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
