USE Reservations
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 4/15/2020 10:33:13 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_CRS_BookingSource]'
GO
ALTER TABLE [map].[sabreAPI_CRS_BookingSource] DROP CONSTRAINT [FK_map_sabreAPI_CRS_BookingSource_BookingSourceID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_Chain]'
GO
ALTER TABLE [map].[sabreAPI_Chain] DROP CONSTRAINT [FK_map_sabreAPI_Chain_intChainID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_CorporateCode]'
GO
ALTER TABLE [map].[sabreAPI_CorporateCode] DROP CONSTRAINT [FK_map_sabreAPI_CorporateCode_CorporateCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_Guest]'
GO
ALTER TABLE [map].[sabreAPI_Guest] DROP CONSTRAINT [FK_map_sabreAPI_Guest_GuestID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_Guest_CompanyName]'
GO
ALTER TABLE [map].[sabreAPI_Guest_CompanyName] DROP CONSTRAINT [FK_map_sabreAPI_Guest_CompanyName_Guest_CompanyNameID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] DROP CONSTRAINT [FK_map_sabreAPI_IATANumber_IATANumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_LoyaltyNumber]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyNumber] DROP CONSTRAINT [FK_map_sabreAPI_LoyaltyNumber_LoyaltyNumberID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_LoyaltyProgram]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyProgram] DROP CONSTRAINT [FK_map_sabreAPI_LoyaltyProgram_LoyaltyProgramID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] DROP CONSTRAINT [FK_map_sabreAPI_PromoCode_PromoCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_RateCategory]'
GO
ALTER TABLE [map].[sabreAPI_RateCategory] DROP CONSTRAINT [FK_map_sabreAPI_RateCategory_RateCategoryID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] DROP CONSTRAINT [FK_map_sabreAPI_RateCode_RateTypeCodeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] DROP CONSTRAINT [FK_map_sabreAPI_RoomType_RoomTypeID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] DROP CONSTRAINT [FK_map_sabreAPI_TravelAgent_TravelAgentID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_VoiceAgent]'
GO
ALTER TABLE [map].[sabreAPI_VoiceAgent] DROP CONSTRAINT [FK_map_sabreAPI_VoiceAgent_UserNameID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] DROP CONSTRAINT [FK_map_sabreAPI_hotel_intHotelID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_CRS_BookingSource]'
GO
ALTER TABLE [map].[sabreAPI_CRS_BookingSource] DROP CONSTRAINT [PK_map_sabreAPI_CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_Chain]'
GO
ALTER TABLE [map].[sabreAPI_Chain] DROP CONSTRAINT [PK_map_sabreAPI_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_CorporateCode]'
GO
ALTER TABLE [map].[sabreAPI_CorporateCode] DROP CONSTRAINT [PK_map_sabreAPI_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_Guest]'
GO
ALTER TABLE [map].[sabreAPI_Guest] DROP CONSTRAINT [PK_map_sabreAPI_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_Guest_CompanyName]'
GO
ALTER TABLE [map].[sabreAPI_Guest_CompanyName] DROP CONSTRAINT [PK_map_sabreAPI_Guest_CompanyName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] DROP CONSTRAINT [PK_map_sabreAPI_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_LoyaltyNumber]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyNumber] DROP CONSTRAINT [PK_map_sabreAPI_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_LoyaltyProgram]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyProgram] DROP CONSTRAINT [PK_map_sabreAPI_LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] DROP CONSTRAINT [PK_map_sabreAPI_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_RateCategory]'
GO
ALTER TABLE [map].[sabreAPI_RateCategory] DROP CONSTRAINT [PK_map_sabreAPI_RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] DROP CONSTRAINT [PK_map_sabreAPI_RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] DROP CONSTRAINT [PK_map_sabreAPI_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] DROP CONSTRAINT [PK_map_sabreAPI_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_VoiceAgent]'
GO
ALTER TABLE [map].[sabreAPI_VoiceAgent] DROP CONSTRAINT [PK_map_sabreAPI_VoiceAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] DROP CONSTRAINT [PK_map_sabreAPI_hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_VoiceAgent]'
GO
DROP TABLE [map].[sabreAPI_VoiceAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_TravelAgent]'
GO
DROP TABLE [map].[sabreAPI_TravelAgent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_RoomType]'
GO
DROP TABLE [map].[sabreAPI_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_RateCode]'
GO
DROP TABLE [map].[sabreAPI_RateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_RateCategory]'
GO
DROP TABLE [map].[sabreAPI_RateCategory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_PromoCode]'
GO
DROP TABLE [map].[sabreAPI_PromoCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_LoyaltyProgram]'
GO
DROP TABLE [map].[sabreAPI_LoyaltyProgram]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_LoyaltyNumber]'
GO
DROP TABLE [map].[sabreAPI_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_IATANumber]'
GO
DROP TABLE [map].[sabreAPI_IATANumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_hotel]'
GO
DROP TABLE [map].[sabreAPI_hotel]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_Guest]'
GO
DROP TABLE [map].[sabreAPI_Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_Guest_CompanyName]'
GO
DROP TABLE [map].[sabreAPI_Guest_CompanyName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_CRS_BookingSource]'
GO
DROP TABLE [map].[sabreAPI_CRS_BookingSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_CorporateCode]'
GO
DROP TABLE [map].[sabreAPI_CorporateCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [map].[sabreAPI_Chain]'
GO
DROP TABLE [map].[sabreAPI_Chain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CROCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CROCode] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CROCode] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_BookingSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_BookingSource] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_BookingSource] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest_EmailAddress]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_EmailAddress] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_EmailAddress] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Location]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ibeSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ibeSource] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ibeSource] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Area]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Area] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Area] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[City]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Country]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PostalCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Region]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Region] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Region] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[State]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Chain]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Chain] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Chain] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CorporateCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateCode] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateCode] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest_CompanyName]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_CompanyName] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_CompanyName] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[hotel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[hotel] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[hotel] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IATANumber]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[IATANumber] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[IATANumber] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoyaltyNumber]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyNumber] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyNumber] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoyaltyProgram]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyProgram] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyProgram] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PromoCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PromoCode] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PromoCode] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RateCategory]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCategory] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCategory] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RateCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCode] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCode] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RoomType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RoomType] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RoomType] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TravelAgent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[VoiceAgent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VoiceAgent] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VoiceAgent] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ActionType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActionType] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActionType] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_SubSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SubSource] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SubSource] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_SecondarySource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SecondarySource] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SecondarySource] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_Channel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_Channel] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_Channel] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PMSRateTypeCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PMSRateTypeCode] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PMSRateTypeCode] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[VIP_Level]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VIP_Level] DROP
COLUMN [IsSabreAPI]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VIP_Level] DROP
COLUMN [sabreAPI_ID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping schemas'
GO
DROP SCHEMA [sabreAPI]
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
