USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 4/15/2020 10:32:20 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating schemas'
GO
CREATE SCHEMA [sabreAPI]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CROCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CROCode] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CROCode] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_BookingSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest_EmailAddress]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Location]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ibeSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ibeSource] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ibeSource] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Area]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Area] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Area] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[City]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Country]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PostalCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Region]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Region] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Region] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[State]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Chain]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Chain] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Chain] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CorporateCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateCode] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateCode] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest_CompanyName]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_CompanyName] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_CompanyName] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[hotel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[hotel] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[hotel] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IATANumber]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[IATANumber] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[IATANumber] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoyaltyNumber]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyNumber] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyNumber] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoyaltyProgram]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyProgram] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyProgram] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PromoCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PromoCode] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PromoCode] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RateCategory]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCategory] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCategory] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RateCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCode] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCode] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RoomType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RoomType] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RoomType] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TravelAgent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[VoiceAgent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VoiceAgent] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VoiceAgent] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ActionType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActionType] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActionType] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_Chain]'
GO
CREATE TABLE [map].[sabreAPI_Chain]
(
[sabreAPI_ID] [int] NOT NULL,
[intChainID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_Chain] on [map].[sabreAPI_Chain]'
GO
ALTER TABLE [map].[sabreAPI_Chain] ADD CONSTRAINT [PK_map_sabreAPI_Chain] PRIMARY KEY CLUSTERED  ([intChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_CorporateCode]'
GO
CREATE TABLE [map].[sabreAPI_CorporateCode]
(
[sabreAPI_ID] [int] NOT NULL,
[CorporateCodeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_CorporateCode] on [map].[sabreAPI_CorporateCode]'
GO
ALTER TABLE [map].[sabreAPI_CorporateCode] ADD CONSTRAINT [PK_map_sabreAPI_CorporateCode] PRIMARY KEY CLUSTERED  ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_CRS_BookingSource]'
GO
CREATE TABLE [map].[sabreAPI_CRS_BookingSource]
(
[sabreAPI_ID] [int] NOT NULL,
[BookingSourceID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_CRS_BookingSource] on [map].[sabreAPI_CRS_BookingSource]'
GO
ALTER TABLE [map].[sabreAPI_CRS_BookingSource] ADD CONSTRAINT [PK_map_sabreAPI_CRS_BookingSource] PRIMARY KEY CLUSTERED  ([sabreAPI_ID], [BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_Guest_CompanyName]'
GO
CREATE TABLE [map].[sabreAPI_Guest_CompanyName]
(
[sabreAPI_ID] [int] NOT NULL,
[Guest_CompanyNameID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_Guest_CompanyName] on [map].[sabreAPI_Guest_CompanyName]'
GO
ALTER TABLE [map].[sabreAPI_Guest_CompanyName] ADD CONSTRAINT [PK_map_sabreAPI_Guest_CompanyName] PRIMARY KEY CLUSTERED  ([Guest_CompanyNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_Guest]'
GO
CREATE TABLE [map].[sabreAPI_Guest]
(
[sabreAPI_ID] [int] NOT NULL,
[GuestID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_Guest] on [map].[sabreAPI_Guest]'
GO
ALTER TABLE [map].[sabreAPI_Guest] ADD CONSTRAINT [PK_map_sabreAPI_Guest] PRIMARY KEY CLUSTERED  ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_hotel]'
GO
CREATE TABLE [map].[sabreAPI_hotel]
(
[sabreAPI_ID] [int] NOT NULL,
[intHotelID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_hotel] on [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] ADD CONSTRAINT [PK_map_sabreAPI_hotel] PRIMARY KEY CLUSTERED  ([sabreAPI_ID], [intHotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_IATANumber]'
GO
CREATE TABLE [map].[sabreAPI_IATANumber]
(
[sabreAPI_ID] [int] NOT NULL,
[IATANumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_IATANumber] on [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] ADD CONSTRAINT [PK_map_sabreAPI_IATANumber] PRIMARY KEY CLUSTERED  ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_LoyaltyNumber]'
GO
CREATE TABLE [map].[sabreAPI_LoyaltyNumber]
(
[sabreAPI_ID] [int] NOT NULL,
[LoyaltyNumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_LoyaltyNumber] on [map].[sabreAPI_LoyaltyNumber]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyNumber] ADD CONSTRAINT [PK_map_sabreAPI_LoyaltyNumber] PRIMARY KEY CLUSTERED  ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_LoyaltyProgram]'
GO
CREATE TABLE [map].[sabreAPI_LoyaltyProgram]
(
[sabreAPI_ID] [int] NOT NULL,
[LoyaltyProgramID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_LoyaltyProgram] on [map].[sabreAPI_LoyaltyProgram]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyProgram] ADD CONSTRAINT [PK_map_sabreAPI_LoyaltyProgram] PRIMARY KEY CLUSTERED  ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_PromoCode]'
GO
CREATE TABLE [map].[sabreAPI_PromoCode]
(
[sabreAPI_ID] [int] NOT NULL,
[PromoCodeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_PromoCode] on [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] ADD CONSTRAINT [PK_map_sabreAPI_PromoCode] PRIMARY KEY CLUSTERED  ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_RateCategory]'
GO
CREATE TABLE [map].[sabreAPI_RateCategory]
(
[sabreAPI_ID] [int] NOT NULL,
[RateCategoryID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_RateCategory] on [map].[sabreAPI_RateCategory]'
GO
ALTER TABLE [map].[sabreAPI_RateCategory] ADD CONSTRAINT [PK_map_sabreAPI_RateCategory] PRIMARY KEY CLUSTERED  ([RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_RateCode]'
GO
CREATE TABLE [map].[sabreAPI_RateCode]
(
[sabreAPI_ID] [int] NOT NULL,
[RateTypeCodeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_RateCode] on [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] ADD CONSTRAINT [PK_map_sabreAPI_RateCode] PRIMARY KEY CLUSTERED  ([RateTypeCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_RoomType]'
GO
CREATE TABLE [map].[sabreAPI_RoomType]
(
[sabreAPI_ID] [int] NOT NULL,
[RoomTypeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_RoomType] on [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] ADD CONSTRAINT [PK_map_sabreAPI_RoomType] PRIMARY KEY CLUSTERED  ([sabreAPI_ID], [RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_TravelAgent]'
GO
CREATE TABLE [map].[sabreAPI_TravelAgent]
(
[sabreAPI_ID] [int] NOT NULL,
[TravelAgentID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_TravelAgent] on [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] ADD CONSTRAINT [PK_map_sabreAPI_TravelAgent] PRIMARY KEY CLUSTERED  ([sabreAPI_ID], [TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[sabreAPI_VoiceAgent]'
GO
CREATE TABLE [map].[sabreAPI_VoiceAgent]
(
[sabreAPI_ID] [int] NOT NULL,
[UserNameID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_sabreAPI_VoiceAgent] on [map].[sabreAPI_VoiceAgent]'
GO
ALTER TABLE [map].[sabreAPI_VoiceAgent] ADD CONSTRAINT [PK_map_sabreAPI_VoiceAgent] PRIMARY KEY CLUSTERED  ([UserNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_SubSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SubSource] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SubSource] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_SecondarySource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SecondarySource] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SecondarySource] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_Channel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_Channel] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_Channel] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PMSRateTypeCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PMSRateTypeCode] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PMSRateTypeCode] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[VIP_Level]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VIP_Level] ADD
[sabreAPI_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VIP_Level] ADD
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_CRS_BookingSource]'
GO
ALTER TABLE [map].[sabreAPI_CRS_BookingSource] ADD CONSTRAINT [FK_map_sabreAPI_CRS_BookingSource_BookingSourceID] FOREIGN KEY ([BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_Chain]'
GO
ALTER TABLE [map].[sabreAPI_Chain] ADD CONSTRAINT [FK_map_sabreAPI_Chain_intChainID] FOREIGN KEY ([intChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_CorporateCode]'
GO
ALTER TABLE [map].[sabreAPI_CorporateCode] ADD CONSTRAINT [FK_map_sabreAPI_CorporateCode_CorporateCodeID] FOREIGN KEY ([CorporateCodeID]) REFERENCES [dbo].[CorporateCode] ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_Guest]'
GO
ALTER TABLE [map].[sabreAPI_Guest] ADD CONSTRAINT [FK_map_sabreAPI_Guest_GuestID] FOREIGN KEY ([GuestID]) REFERENCES [dbo].[Guest] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_Guest_CompanyName]'
GO
ALTER TABLE [map].[sabreAPI_Guest_CompanyName] ADD CONSTRAINT [FK_map_sabreAPI_Guest_CompanyName_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [dbo].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] ADD CONSTRAINT [FK_map_sabreAPI_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_LoyaltyNumber]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyNumber] ADD CONSTRAINT [FK_map_sabreAPI_LoyaltyNumber_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_LoyaltyProgram]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyProgram] ADD CONSTRAINT [FK_map_sabreAPI_LoyaltyProgram_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [dbo].[LoyaltyProgram] ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] ADD CONSTRAINT [FK_map_sabreAPI_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RateCategory]'
GO
ALTER TABLE [map].[sabreAPI_RateCategory] ADD CONSTRAINT [FK_map_sabreAPI_RateCategory_RateCategoryID] FOREIGN KEY ([RateCategoryID]) REFERENCES [dbo].[RateCategory] ([RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] ADD CONSTRAINT [FK_map_sabreAPI_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] ADD CONSTRAINT [FK_map_sabreAPI_RoomType_RoomTypeID] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] ADD CONSTRAINT [FK_map_sabreAPI_TravelAgent_TravelAgentID] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_VoiceAgent]'
GO
ALTER TABLE [map].[sabreAPI_VoiceAgent] ADD CONSTRAINT [FK_map_sabreAPI_VoiceAgent_UserNameID] FOREIGN KEY ([UserNameID]) REFERENCES [dbo].[VoiceAgent] ([VoiceAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] ADD CONSTRAINT [FK_map_sabreAPI_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
