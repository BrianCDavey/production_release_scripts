/*
Run this script on:

        wus2-data-dp-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 9/17/2024 6:05:28 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [sabreAPI].[LeastRecentTransaction]'
GO
ALTER TABLE [sabreAPI].[LeastRecentTransaction] DROP CONSTRAINT [FK_SabreAPI_LeastRecentTransaction_TransactionID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [sabreAPI].[LeastRecentTransaction]'
GO
ALTER TABLE [sabreAPI].[LeastRecentTransaction] DROP CONSTRAINT [PK_SabreAPI_LeastRecentTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[PopulateLeastRecentTransactions]'
GO
DROP PROCEDURE [sabreAPI].[PopulateLeastRecentTransactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [sabreAPI].[LeastRecentTransaction]'
GO
DROP TABLE [sabreAPI].[LeastRecentTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[Transactions]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[Transactions] DROP
COLUMN [ApiTransactionID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UPDATE_CRS_BookingSourceID]'
GO

ALTER PROCEDURE [dbo].[UPDATE_CRS_BookingSourceID]
	@QueueID int = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_BS(confirmationNumber,BookingSourceID,rowNum)
	AS
	(
		SELECT t.confirmationNumber,tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp, at.actionTypeOrder)
		FROM synxis.Transactions t
			INNER JOIN synxis.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN synxis.ActionType at ON at.ActionTypeID = ts.ActionTypeID
			INNER JOIN synxis.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[synXis_CRS_BookingSource] tbs ON tbs.synXisID = bs.BookingSourceID
			INNER JOIN synxis.Channel ch ON ch.ChannelID = bs.ChannelID
		WHERE NULLIF(ch.channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM synxis.Transactions WHERE QueueID = @QueueID)

		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp, at.actionTypeOrder)
		FROM openHosp.Transactions t
			INNER JOIN openHosp.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN openHosp.ActionType at ON at.ActionTypeID = ts.ActionTypeID
			INNER JOIN openHosp.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[openHosp_CRS_BookingSource] tbs ON tbs.openHospID = bs.BookingSourceID
			INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM openHosp.Transactions WHERE QueueID = @QueueID)

		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,         
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp, at.actionTypeOrder)
		FROM pegasus.Transactions t
			INNER JOIN pegasus.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN pegasus.ActionType at ON at.ActionTypeID = ts.ActionTypeID
			INNER JOIN pegasus.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[pegasus_CRS_BookingSource] tbs ON tbs.pegasusID = bs.BookingSourceID
			INNER JOIN authority.Pegasus_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM pegasus.Transactions WHERE QueueID = @QueueID)

		UNION ALL

		SELECT t.confirmationNumber,tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp, at.actionTypeOrder)
		FROM sabreAPI.Transactions t
			INNER JOIN SabreAPI.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN SabreAPI.ActionType at ON at.ActionTypeID = ts.ActionTypeID
			INNER JOIN sabreAPI.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[sabreAPI_CRS_BookingSource] tbs ON tbs.sabreAPI_ID = bs.BookingSourceID
			INNER JOIN sabreAPI.Channel ch ON ch.ChannelID = bs.ChannelID
		WHERE NULLIF(ch.channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM sabreAPI.Transactions WHERE QueueID = @QueueID)

	)
	UPDATE t
		SET CRS_BookingSourceID = bs.BookingSourceID
	FROM dbo.Transactions t
		INNER JOIN cte_BS bs ON bs.confirmationNumber = t.confirmationNumber
	WHERE bs.rowNum = 1
		AND t.CRS_BookingSourceID != bs.BookingSourceID
END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[RawDataForLoading_Temp]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [sabreAPI].[RawDataForLoading_Temp] DROP
COLUMN [ApiTransactionID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [temp].[sabreAPI_RawDataForLoading]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [temp].[sabreAPI_RawDataForLoading] DROP
COLUMN [ApiTransactionID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[PopulateMostRecentTransactions]'
GO




ALTER PROCEDURE [sabreAPI].[PopulateMostRecentTransactions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-- CREATE & POPULATE #TRN_CONF --------------------------------
	IF OBJECT_ID('tempdb..#TRN_CONF') IS NOT NULL
		DROP TABLE #TRN_CONF;
	CREATE TABLE #TRN_CONF
	(
		TransactionID int NOT NULL,
		confirmationNumber nvarchar(20) NOT NULL,

		PRIMARY KEY CLUSTERED(TransactionID,confirmationNumber)
	)

	;WITH cte_MRT(TransactionID,confirmationNumber,rowNum)
	AS
	(
		SELECT t.TransactionID,t.confirmationNumber,
				ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp DESC, at.actionTypeOrder DESC,ln.loyaltyNumber DESC,t.TransactionID DESC)
		FROM SabreAPI.Transactions t
			INNER JOIN SabreAPI.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN SabreAPI.ActionType at ON at.ActionTypeID = ts.ActionTypeID
			LEFT JOIN SabreAPI.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		WHERE t.confirmationNumber IN(SELECT confirmationNumber FROM sabreAPI.[RawDataForLoading_Temp])
	)
	INSERT INTO #TRN_CONF(TransactionID,confirmationNumber)
	SELECT DISTINCT TransactionID,confirmationNumber
	FROM cte_MRT
	WHERE rowNum = 1
	---------------------------------------------------------------

	UPDATE mrt
		SET TransactionID = c.TransactionID
	FROM SabreAPI.MostRecentTransaction mrt
		INNER JOIN #TRN_CONF c ON c.confirmationNumber = mrt.confirmationNumber
	WHERE mrt.TransactionID != c.TransactionID

	INSERT INTO SabreAPI.[MostRecentTransaction](TransactionID,confirmationNumber)
	SELECT TransactionID,confirmationNumber
	FROM #TRN_CONF
	WHERE confirmationNumber NOT IN(SELECT DISTINCT confirmationNumber FROM SabreAPI.[MostRecentTransaction])
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[POPULATE_temp_sabreAPI_RawDataForLoading]'
GO







ALTER PROCEDURE [dbo].[POPULATE_temp_sabreAPI_RawDataForLoading]
	@QueueID int
AS
BEGIN
	DECLARE @timeLoaded datetime;
	SELECT @timeLoaded = ISNULL([ImportFinished],[ImportStarted]) FROM ETL.dbo.[Queue] WHERE QueueID = @QueueID


	-- CREATE & POPULATE #RES ---------------------------------
	IF OBJECT_ID('tempdb..#RES') IS NOT NULL
		DROP TABLE #RES;

	CREATE TABLE #RES
	(
		confirmationNumber varchar(20) NOT NULL,
		arrivalDate date NOT NULL,
		departureDate date
	)

	;WITH cte_etl
	AS
	(
		SELECT confirmationNumber,arrivalDate,departureDate
		FROM ETL.dbo.Import_SabreAPI 
		WHERE QueueID = @QueueID
			--AND [status] <> 'Cancelled'
			AND arrivalDate IS NOT NULL
	),
	cte_MRT
	AS
	(
		SELECT DISTINCT t.confirmationNumber,td.arrivalDate,td.departureDate
		FROM dbo.Transactions t
			INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		WHERE t.confirmationNumber NOT IN(SELECT confirmationNumber FROM cte_etl)

		UNION

		SELECT DISTINCT confirmationNumber,arrivalDate,departureDate
		FROM cte_etl
	)
	INSERT INTO #RES(confirmationNumber,arrivalDate,departureDate)
	SELECT confirmationNumber,arrivalDate,departureDate FROM cte_MRT
	-----------------------------------------------------------


	TRUNCATE TABLE [temp].[sabreAPI_RawDataForLoading];

	INSERT INTO [temp].[sabreAPI_RawDataForLoading]([QueueID],[chainName],[chainID],[hotelName],[hotelID],[SAPID],[hotelCode],
													[billingDescription],[transactionTimeStamp],[faxNotificationCount],[channel],
													lastModifyingChannel,[secondarySource],[subSource],[subSourceCode],[PMSRateTypeCode],[PMSRoomTypeCode],
													[marketSourceCode],[marketSegmentCode],[userName],[status],[confirmationNumber],[confirmationDate],
													[cancellationNumber],[cancellationDate],[salutation],[guestFirstName],[guestLastName],[customerID],
													[customerAddress1],[customerAddress2],[customerCity],[customerState],[customerPostalCode],
													[customerPhone],[customerCountry],[customerArea],[customerRegion],[customerCompanyName],
													[arrivalDate],[departureDate],[bookingLeadTime],[rateCategoryName],[rateCategoryCode],[rateTypeName],
													[rateTypeCode],[roomTypeName],[roomTypeCode],[nights],[averageDailyRate],[rooms],[reservationRevenue],
													[currency],[IATANumber],[travelAgencyName],[travelAgencyAddress1],[travelAgencyAddress2],
													[travelAgencyCity],[travelAgencyState],[travelAgencyPostalCode],[travelAgencyPhone],[travelAgencyFax],
													[travelAgencyCountry],[travelAgencyArea],[travelAgencyRegion],[travelAgencyEmail],[consortiaCount],
													[consortiaName],[totalPackageRevenue],[optIn],[customerEmail],[totalGuestCount],[adultCount],
													[childrenCount],[creditCardType],[actionType],[shareWith],[arrivalDOW],[departureDOW],
													[itineraryNumber],[secondaryCurrency],[secondaryCurrencyExchangeRate],[secondaryCurrencyAverageDailyRate],
													[secondaryCurrencyReservationRevenue],[secondaryCurrencyPackageRevenue],[commisionPercent],
													[membershipNumber],[corporationCode],[promotionalCode],[CROCode],[channelConnectConfirmationNumber],
													[primaryGuest],[loyaltyProgram],[loyaltyNumber],[vipLevel],[xbeTemplateName],[xbeShellName],
													[profileTypeSelection],[averageDailyRateUSD],[reservationRevenueUSD],[totalPackageRevenueUSD],
													[timeLoaded],[LoyaltyNumberTagged],[LoyaltyNumberValidated],PsuedoCity,roomCategory,GDSBookingCode,
													GDSRateAccessCode,rateTaxInclusive,taxesAmount,taxesCurrency,loyaltyCertificateNumber)
	SELECT DISTINCT @QueueID AS QueueID,
		ISNULL(cnm.SupersetChainName,'') AS chainName,
		CONVERT(int,NULLIF(cnm.SupersetChainID,'')) AS chainID,
		LEFT(ISNULL(hotelName,''), 30) AS hotelName,
		NULLIF(hotelID,'') AS hotelID,
		ISNULL(SAPID,'') AS SAPID,
		ISNULL(hotelCode,'') AS hotelCode,
		ISNULL([SabreAPI].[CalculatedBillingDescription]([sabreAPI].[CalculatedChannel](s.channel),s.secondarySource, s.subSource, s.subSourceCode,s.channelCode),'') AS billingDescription,
		CONVERT(datetime,NULLIF(REPLACE(LEFT(CASE resStatus WHEN 'Commit' THEN createDateTime ELSE lastModifyDateTime END ,19),'T',' '),'')) AS transactionTimeStamp,
		CONVERT(int,NULLIF(faxNotificationCount,'')) AS faxNotificationCount,
		--ISNULL([sabreAPI].[CalculatedChannel](s.channel),'') AS channel,
		ISNULL(s.channel,'') AS channel,
		COALESCE(s.lastModifyingChannel,s.lastModifyingChannelcode,[sabreAPI].[CalculatedChannel](s.channel),'') AS lastModifyingChannel,
		ISNULL(secondarySource,'') AS secondarySource,
		ISNULL(sm.subSource,'') AS subSource,
		ISNULL(s.subSourceCode,'') AS subSourceCode,
		ISNULL(PMSRateTypeCode,'') AS PMSRateTypeCode,
		ISNULL(PMSRoomTypeCode,'') AS PMSRoomTypeCode,
		ISNULL(marketSourceCode,'') AS marketSourceCode,
		ISNULL(marketSegmentCode,'') AS marketSegmentCode,
		ISNULL(userName,'') AS userName,
		CASE
			WHEN ISNULL(status,'') = 'Reserved' THEN 'Confirmed'
			WHEN ISNULL(status,'') = 'Cancelled' THEN 'Cancelled'
			WHEN ISNULL(status,'') = 'no-show' THEN 'Cancelled'
			ELSE 'Confirmed'
		END AS [status],
		ISNULL(s.confirmationNumber,'') AS confirmationNumber,
		CONVERT(datetime,NULLIF(confirmationDate,'')) AS confirmationDate,
		ISNULL(cancellationNumber,'') AS cancellationNumber,
		NULLIF(CONVERT(datetime,NULLIF(cancellationDate,'')),'1900-01-01') AS cancellationDate,
		ISNULL(salutation,'') AS salutation,
		ISNULL(guestFirstName,'') AS guestFirstName,
		ISNULL(guestLastName,'') AS guestLastName,
		SUBSTRING(ISNULL(customerID,''),1,12) AS customerID,
		ISNULL(customerAddress1,'') AS customerAddress1,
		ISNULL(customerAddress2,'') AS customerAddress2,
		ISNULL(customerCity,'') AS customerCity,
		ISNULL(customerState,'') AS customerState,
		ISNULL(customerPostalCode,'') AS customerPostalCode,
		ISNULL(customerPhone,'') AS customerPhone,
		ISNULL(customerCountry,'') AS customerCountry,
		ISNULL(customerArea,'') AS customerArea,
		ISNULL(customerRegion,'') AS customerRegion,
		ISNULL(customerCompanyName,'') AS customerCompanyName,
		CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')) AS arrivalDate
		,CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),'')) AS departureDate,
		DATEDIFF(day,CONVERT(datetime,NULLIF(confirmationDate,'')),CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),''))) AS bookingLeadTime,
		ISNULL(rateCategoryName,'') AS rateCategoryName,
		ISNULL(rateCategoryCode,'') AS rateCategoryCode,
		ISNULL(rateTypeName,'') AS rateTypeName,
		COALESCE(rateTypeCode,RatePlanCode,'') AS rateTypeCode,
		ISNULL(roomTypeName,'') AS roomTypeName,
		ISNULL(roomTypeCode,'') AS roomTypeCode,
		CONVERT(int,NULLIF(DATEDIFF(day,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')),CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),''))),'')) AS nights,
		CONVERT(decimal(12,2),[sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue)/CONVERT(int,NULLIF(DATEDIFF(day,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')),CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),''))),''))) AS averageDailyRate,
		CONVERT(int,NULLIF(rooms,'')) AS rooms,
		[sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue) AS reservationRevenue,
		ISNULL(currency,'') AS currency,
		ISNULL(IATANumber,'') AS IATANumber,
		ISNULL(travelAgencyName,'') AS travelAgencyName,
		ISNULL(travelAgencyAddress1,'') AS travelAgencyAddress1,
		ISNULL(travelAgencyAddress2,'') AS travelAgencyAddress2,
		ISNULL(travelAgencyCity,'') AS travelAgencyCity,
		ISNULL(travelAgencyState,'') AS travelAgencyState,
		ISNULL(travelAgencyPostalCode,'') AS travelAgencyPostalCode,
		ISNULL(travelAgencyPhone,'') AS travelAgencyPhone,
		ISNULL(travelAgencyFax,'') AS travelAgencyFax,
		ISNULL(travelAgencyCountry,'') AS travelAgencyCountry,
		ISNULL(travelAgencyArea,'') AS travelAgencyArea,
		ISNULL(travelAgencyRegion,'') AS travelAgencyRegion,
		ISNULL(travelAgencyEmail,'') AS travelAgencyEmail,
		CONVERT(int,NULLIF(consortiaCount,'')) AS consortiaCount,
		ISNULL(consortiaName,'') AS consortiaName,
		ISNULL(CONVERT(decimal(18,2),NULLIF(totalPackageRevenue,'')),0.00) AS totalPackageRevenue,
		CASE WHEN ISNULL(optIn,'') = 'Yes' THEN 'Y' WHEN ISNULL(optIn,'') = 'No' THEN 'N' ELSE 'N' END AS optIn,
		ISNULL(customerEmail,'') AS customerEmail,
		CONVERT(int,NULLIF(adultCount,'')) + ISNULL(CONVERT(int,NULLIF(childrenCount,'')),0) AS totalGuestCount,
		CONVERT(int,NULLIF(adultCount,'')) AS adultCount,
		ISNULL(CONVERT(int,NULLIF(childrenCount,'')),0) AS childrenCount,
		COALESCE(creditCardType,creditCardTypeAlternate,'') AS creditCardType,
		CASE
			WHEN ISNULL(actionType,'') = 'Commit' THEN 'N'
			WHEN ISNULL(actionType,'') = 'Modify' THEN 'M'
			WHEN ISNULL(actionType,'') = 'Cancel' THEN 'X'
			ELSE ''
		END AS actionType,
		ISNULL(shareWith,'N') AS shareWith,
		CASE DATEPART(dw,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')))
			WHEN 1 THEN 'Sun'
			WHEN 2 THEN 'Mon'
			WHEN 3 THEN 'Tue'
			WHEN 4 THEN 'Wed'
			WHEN 5 THEN 'Thu'
			WHEN 6 THEN 'Fri'
			WHEN 7 THEN 'Sat'
		END AS arrivalDOW,
		CASE DATEPART(dw,CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),'')))
			WHEN 1 THEN 'Sun'
			WHEN 2 THEN 'Mon'
			WHEN 3 THEN 'Tue'
			WHEN 4 THEN 'Wed'
			WHEN 5 THEN 'Thu'
			WHEN 6 THEN 'Fri'
			WHEN 7 THEN 'Sat' 
		END AS departureDOW,
		ISNULL(itineraryNumber,'') AS itineraryNumber,
		ISNULL(secondaryCurrency,'') AS secondaryCurrency,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyExchangeRate,'')) AS secondaryCurrencyExchangeRate,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyAverageDailyRate,'')) AS secondaryCurrencyAverageDailyRate,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyReservationRevenue,'')) AS secondaryCurrencyReservationRevenue,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyPackageRevenue,'')) AS secondaryCurrencyPackageRevenue,
		CONVERT(decimal(18,2),NULLIF(TRIM(commisionPercent),'')) AS commisionPercent,
		ISNULL(membershipNumber,'') AS membershipNumber,
		ISNULL(corporationCode,'') AS corporationCode,
		ISNULL(promotionalCode,'') AS promotionalCode,
		ISNULL(CROCode,'') AS CROCode,
		ISNULL(channelConnectConfirmationNumber,'') AS channelConnectConfirmationNumber,
		CASE ISNULL(primaryGuest,'')  WHEN 'true' THEN 'Y' WHEN 'false' THEN 'N' ELSE '' END AS primaryGuest,
		ISNULL(loyaltyProgram,'') AS loyaltyProgram,
		COALESCE(loyaltyNumber,loyaltyNumberBackUp,'') AS loyaltyNumber,
		ISNULL(vipLevel,'') AS vipLevel,
		ISNULL(s.xbeTemplateName,'') AS xbeTemplateName,
		ISNULL(xbeShellName,'') AS xbeShellName,
		ISNULL(profileTypeSelection,'') AS profileTypeSelection,
		Superset.dbo.convertCurrencyToUSD(CONVERT(decimal(12,2),[sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue)/CONVERT(int,NULLIF(DATEDIFF(day,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')),CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),''))),''))) ,currency,confirmationDate) AS averageDailyRateUSD,
		Superset.dbo.convertCurrencyToUSD([sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue),currency,confirmationDate) AS reservationRevenueUSD,
		Superset.dbo.convertCurrencyToUSD(ISNULL(totalPackageRevenue,0.00),currency,confirmationDate) AS totalPackageRevenueUSD,
		@timeLoaded AS timeLoaded,
		CASE
			WHEN ISNULL(tag1.[iPrefer Number],'') > '' THEN 1
			WHEN ISNULL(tag2.[iPrefer Number],'') > '' THEN 1
			ELSE 0
		END AS LoyaltyNumberTagged,
		ISNULL(cpi.IsMemberEnabled,0) AS LoyaltyNumberValidated,
		ISNULL(PsuedoCity,N'') AS PsuedoCity,
		ISNULL(roomCategory,N'') AS roomCategory,
		ISNULL(GDSBookingCode,N'') AS GDSBookingCode,
		ISNULL(GDSRateAccessCode,N'') AS GDSRateAccessCode,
		ISNULL(rateTaxInclusive,N'') AS rateTaxInclusive,
		ISNULL(taxesAmount,N'') AS taxesAmount,
		ISNULL(taxesCurrency,N'') AS taxesCurrency,
		ISNULL(loyaltyCertificateNumber,N'') AS loyaltyCertificateNumber
	FROM ETL.dbo.Import_SabreAPI s
		LEFT JOIN
		(
			SELECT [iPrefer_Number],Membership_Date,IsMemberEnabled
			FROM [Superset].[BSI].[Customer_Profile_Import]
		) cpi ON cpi.iPrefer_Number = s.loyaltyNumber
				AND s.arrivalDate >= cpi.Membership_Date
				AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		LEFT JOIN Superset.[BSI].[TaggedCustomers] tag1 ON tag1.[iPrefer Number] = s.loyaltyNumber
														AND tag1.Hotel_Code = s.hotelCode
														AND s.arrivalDate >= tag1.DateTagged
														AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		LEFT JOIN [ReservationBilling].[dbo].[Templates] temp ON temp.xbeTemplateName = s.xbeTemplateName
		LEFT JOIN [loyalty].[dbo].[Customer_Profile_Import] cpi2 ON cpi2.Email = s.customerEmail
		LEFT JOIN Superset.[BSI].[TaggedCustomers] tag2 ON tag2.[iPrefer Number] = cpi2.iPrefer_Number
														AND tag2.Hotel_Code = s.hotelCode
														AND s.arrivalDate >= tag2.DateTagged
														AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
														AND temp.templateGroupID = 3
		LEFT JOIN sabreAPI.ChainNameAndIDMapping cnm ON cnm.SabreDWAPIEquivalent = s.chainCode
		LEFT JOIN sabreAPI.SubsourceMapping sm ON sm.SubsourceCode = s.subSourceCode
		LEFT JOIN #RES mrt ON mrt.confirmationNumber = s.confirmationNumber
	WHERE QueueID = @QueueID
		AND primaryGuest = 'true'
		AND s.[status] IN ('Cancelled', 'Reserved','in-house','Checked Out' , 'no-show')
		AND CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')) IS NOT NULL
		AND ISDATE(NULLIF(s.confirmationDate,'')) = 1
		AND s.confirmationDate <> '0001-01-01T00:00:00'
		AND s.chainCode != 'QACHAIN'
		AND s.confirmationNumber NOT IN(SELECT confirmationNumber FROM sabreAPI.Exclude_ConfNum)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[load_Transactions]'
GO



ALTER PROCEDURE [sabreAPI].[load_Transactions]

AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_bookingSource([BookingSourceID],[channel],[secondarySource],[subSource],[subSourceCode],[CROCode],[xbeTemplateName],[xbeShellName])
	AS
	(
		SELECT bs.BookingSourceID,ch.channel,ss.secondarySource,sub.subSource,sub.subSourceCode,cro.CROCode,x.xbeTemplateName,x.xbeShellName
		FROM SabreAPI.[BookingSource] bs
			LEFT JOIN SabreAPI.[Channel] ch ON ch.ChannelID = bs.ChannelID
			LEFT JOIN SabreAPI.[SecondarySource] ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN SabreAPI.[SubSource] sub ON sub.SubSourceID = bs.SubSourceID
			LEFT JOIN SabreAPI.[CROCode] cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN SabreAPI.[xbeTemplate] x ON x.xbeTemplateID = bs.xbeTemplateID
	),cte_Transactions
	AS
	(
		SELECT DISTINCT x.QueueID,x.timeLoaded,
						x.itineraryNumber,x.confirmationNumber,x.transactionTimeStamp,x.channelConnectConfirmationNumber,
						ts.TransactionStatusID,tr.TransactionDetailID,te.TransactionsExtendedID,
						vip.VIP_LevelID,cc.CorporateCodeID,con.ConsortiaID,rom.RoomTypeID,ratc.RateCategoryID,usr.UserNameID,bil.BillingDescriptionID,
						bok.BookingSourceID,loy.LoyaltyProgramID,loyn.LoyaltyNumberID,gst.GuestID,rtc.RateTypeCodeID,pro.PromoCodeID,tra.TravelAgentID,iat.IATANumberID,
						hot.intHotelID,chn.intChainID,pms.PMSRateTypeCodeID,x.[Guest_CompanyNameID],
						lmc.ChannelID AS lastModified_ChannelID,rc.RoomCategoryID
		FROM sabreAPI.[RawDataForLoading_Temp] x
			LEFT JOIN SabreAPI.[TransactionStatus] ts ON ts.confirmationNumber = x.confirmationNumber AND ts.hashKey = x.TransactionStatus_hashKey
			LEFT JOIN SabreAPI.[TransactionDetail] tr ON tr.confirmationNumber = x.confirmationNumber AND tr.hashKey = x.TransactionDetail_hashKey
			LEFT JOIN SabreAPI.[TransactionsExtended] te ON te.confirmationNumber = x.confirmationNumber AND te.hashKey = x.TransactionsExtended_hashKey
			LEFT JOIN SabreAPI.[VIP_Level] vip ON vip.vipLevel = x.vipLevel
			LEFT JOIN SabreAPI.[CorporateCode] cc ON cc.corporationCode = x.corporationCode
			LEFT JOIN SabreAPI.[Consortia] con ON con.consortiaName = x.consortiaName
			LEFT JOIN SabreAPI.[RoomType] rom ON rom.roomTypeCode = x.roomTypeCode AND rom.PMSRoomTypeCode = x.PMSRoomTypeCode AND rom.roomTypeName = x.roomTypeName
			LEFT JOIN SabreAPI.[RoomCategory] rc ON rc.RoomCategory = x.RoomCategory 
			LEFT JOIN SabreAPI.[RateCategory] ratc ON ratc.rateCategoryCode = x.rateCategoryCode AND ratc.rateCategoryName = x.rateCategoryName
			LEFT JOIN SabreAPI.[UserName] usr ON usr.userName = x.userName
			LEFT JOIN SabreAPI.[BillingDescription] bil ON bil.billingDescription = x.billingDescription
			LEFT JOIN cte_BookingSource bok ON bok.channel = x.channel AND bok.CROCode = x.CROCode AND bok.secondarySource = x.secondarySource AND bok.subSource = x.subSource AND bok.subSourceCode = x.subSourceCode AND bok.xbeTemplateName = x.xbeTemplateName AND bok.xbeShellName = x.xbeShellName
			LEFT JOIN SabreAPI.[LoyaltyProgram] loy ON loy.loyaltyProgram = x.loyaltyProgram
			LEFT JOIN SabreAPI.[LoyaltyNumber] loyn ON loyn.loyaltyNumber = x.loyaltyNumber
			LEFT JOIN SabreAPI.[Guest] gst ON gst.Guest_EmailAddressID = x.[Guest_EmailAddressID] AND gst.hashKey = x.[Guest_hashKey]
			LEFT JOIN SabreAPI.[RateTypeCode] rtc ON rtc.RateTypeCode = x.RateTypeCode AND rtc.RateTypeName = x.RateTypeName AND rtc.GDSBookingCode = x.GDSBookingCode AND rtc.GDSRateAccessCode = x.GDSRateAccessCode AND rtc.rateTaxInclusive = x.rateTaxInclusive
			LEFT JOIN SabreAPI.[PromoCode] pro ON pro.promotionalCode = x.promotionalCode
			LEFT JOIN SabreAPI.[TravelAgent] tra ON tra.hashKey = x.TravelAgent_hashKey
			LEFT JOIN SabreAPI.[IATANumber] iat ON iat.IATANumber = x.IATANumber
			LEFT JOIN SabreAPI.[hotel] hot ON hot.HotelCode = x.HotelCode AND hot.HotelId = x.HotelId
			LEFT JOIN SabreAPI.[Chain] chn ON chn.ChainID = x.ChainID
			LEFT JOIN SabreAPI.[PMSRateTypeCode] pms ON pms.PMSRateTypeCode = x.PMSRateTypeCode
			LEFT JOIN sabreAPI.Channel lmc ON lmc.channel = x.lastModifiedChannel
	)
	INSERT INTO SabreAPI.[Transactions](subQueueID,QueueID,itineraryNumber,confirmationNumber,transactionTimeStamp,channelConnectConfirmationNumber,timeLoaded,
										TransactionStatusID,TransactionDetailID,TransactionsExtendedID,
										VIP_LevelID,CorporateCodeID,ConsortiaID,RoomTypeID,RateCategoryID,UserNameID,BillingDescriptionID,
										BookingSourceID,LoyaltyProgramID,LoyaltyNumberID,GuestID,RateTypeCodeID,PromoCodeID,TravelAgentID,IATANumberID,
										intHotelID,intChainID,PMSRateTypeCodeID,[Guest_CompanyNameID],
										LastModified_ChannelID,RoomCategoryID)
	SELECT 0,QueueID,itineraryNumber,confirmationNumber,transactionTimeStamp,channelConnectConfirmationNumber,timeLoaded,
			TransactionStatusID,TransactionDetailID,TransactionsExtendedID,
			VIP_LevelID,CorporateCodeID,ConsortiaID,RoomTypeID,RateCategoryID,UserNameID,BillingDescriptionID,
			BookingSourceID,LoyaltyProgramID,LoyaltyNumberID,GuestID,RateTypeCodeID,PromoCodeID,TravelAgentID,IATANumberID,
			intHotelID,intChainID,PMSRateTypeCodeID,[Guest_CompanyNameID],
			lastModified_ChannelID,RoomCategoryID
	FROM cte_Transactions
		EXCEPT
	SELECT subQueueID,QueueID,itineraryNumber,confirmationNumber,transactionTimeStamp,channelConnectConfirmationNumber,timeLoaded,
			TransactionStatusID,TransactionDetailID,TransactionsExtendedID,
			VIP_LevelID,CorporateCodeID,ConsortiaID,RoomTypeID,RateCategoryID,UserNameID,BillingDescriptionID,
			BookingSourceID,LoyaltyProgramID,LoyaltyNumberID,GuestID,RateTypeCodeID,PromoCodeID,TravelAgentID,IATANumberID,
			intHotelID,intChainID,PMSRateTypeCodeID,[Guest_CompanyNameID],LastModified_ChannelID, RoomCategoryID
	FROM SabreAPI.[Transactions]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[load_RawDataForLoading_Temp]'
GO






ALTER   PROCEDURE [sabreAPI].[load_RawDataForLoading_Temp]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	
	-- TABLE SHOULD HAVE BEEN CREATED IN PARENT CALL -----------
		IF OBJECT_ID('tempdb..#LOCATION') IS NULL
		BEGIN
			CREATE TABLE #LOCATION
			(
				LocationID int NOT NULL,
				City_Text nvarchar(255) NOT NULL,
				State_Text nvarchar(255) NOT NULL,
				Country_Text nvarchar(255) NOT NULL,
				PostalCode_Text nvarchar(255) NOT NULL,
				Area_Text nvarchar(255) NOT NULL,
				Region_Text nvarchar(255) NOT NULL
			)
		END
	------------------------------------------------------------
	
	TRUNCATE TABLE sabreAPI.RawDataForLoading_Temp;

	INSERT INTO sabreAPI.RawDataForLoading_Temp(QueueID,timeLoaded,itineraryNumber,transactionTimeStamp,channelConnectConfirmationNumber,
						[IATANumber],[billingDescription],
						[channel],
						lastModifiedChannel,
						[secondarySource],[subSource],[subSourceCode],[CROCode],[xbeTemplateName],[xbeShellName],[ChainID],[consortiaName],
						[corporationCode],[HotelId],[HotelName],[HotelCode],[loyaltyNumber],[loyaltyProgram],[promotionalCode],[rateCategoryName],
						[rateCategoryCode],[PMSRateTypeCode],[RateTypeName],[RateTypeCode],[roomTypeName],[roomTypeCode],[PMSRoomTypeCode],[userName],[vipLevel],
						customerID,salutation,FirstName,LastName,Address1,Address2,Guest_LocationID,
						phone,
						[Guest_CompanyNameID],[Guest_EmailAddressID],
						IsPrimaryGuest,
						confirmationNumber,nights,averageDailyRate,rooms,reservationRevenue,currency,totalPackageRevenue,
						totalGuestCount,adultCount,childrenCount,secondaryCurrency,secondaryCurrencyExchangeRate,
						secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,secondaryCurrencyPackageRevenue,commisionPercent,
						SAPID,faxNotificationCount,marketSourceCode,marketSegmentCode,shareWith,membershipNumber,consortiaCount,optIn,profileTypeSelection,
						[status],confirmationDate,cancellationNumber,cancellationDate,arrivalDate,departureDate,bookingLeadTime,
						creditCardType,
						arrivalDOW,
						departureDOW,
						ActionTypeID,
						IATANumberID,travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,TravelAgent_LocationID,
						travelAgencyPhone,travelAgencyFax,
						travelAgencyEmail,
						Location_LocationHashKey,
						[PsuedoCity],roomCategory,[GDSRateAccessCode],[GDSBookingCode],[rateTaxInclusive],
						taxAmount,taxCurrency,loyaltyCertificateNumber
						)
	SELECT DISTINCT s.QueueID,s.timeLoaded,
			s.itineraryNumber,s.transactionTimeStamp,s.channelConnectConfirmationNumber,
			s.[IATANumber],s.billingDescription,
			--[sabreAPI].[CalculatedChannel](s.channel),
			s.channel,
			--ISNULL(s.lastModifyingChannel,[sabreAPI].[CalculatedChannel](s.channel)),
			ISNULL(s.lastModifyingChannel,s.channel),
			s.secondarySource,s.subSource,s.subSourceCode,s.CROCode,s.xbeTemplateName,s.xbeShellName,s.chainID,s.consortiaName,
			s.corporationCode,s.hotelID,s.hotelName,s.hotelCode,s.loyaltyNumber,s.loyaltyProgram,s.promotionalCode,s.rateCategoryName,
			s.rateCategoryCode,s.PMSRateTypeCode,s.rateTypeName,s.rateTypeCode,s.roomTypeName,s.roomTypeCode,s.PMSRoomTypeCode,s.userName,s.vipLevel,
			s.[customerID],s.[salutation],s.[guestFirstName],s.[guestLastName],s.[customerAddress1],s.[customerAddress2],gloc.LocationID,
			s.[customerPhone],
			gcn.Guest_CompanyNameID,gea.Guest_EmailAddressID,
			CASE s.[primaryGuest] WHEN 'y' THEN 1 WHEN 'n' THEN 0 ELSE CONVERT(bit,s.[primaryGuest]) END AS IsPrimaryGuest,
			s.[confirmationNumber],CONVERT(int,s.[nights]),CONVERT(decimal(38,2),s.[averageDailyRate]),CONVERT(int,s.[rooms]),CONVERT(decimal(38,2),s.[reservationRevenue]),s.[currency],s.[totalPackageRevenue],
			s.[totalGuestCount],s.[adultCount],s.[childrenCount],s.[secondaryCurrency],s.[secondaryCurrencyExchangeRate],
			s.[secondaryCurrencyAverageDailyRate],s.[secondaryCurrencyReservationRevenue],s.[secondaryCurrencyPackageRevenue],s.[commisionPercent],
			s.[SAPID],s.[faxNotificationCount],s.[marketSourceCode],s.[marketSegmentCode],s.[shareWith],s.[membershipNumber],s.consortiaCount,s.optIn,s.profileTypeSelection,
			s.[status],s.[confirmationDate],s.[cancellationNumber],s.[cancellationDate],s.[arrivalDate],s.[departureDate],s.[bookingLeadTime],
			s.[creditCardType],
			CASE s.[arrivalDOW] WHEN 'Mon' THEN 2 WHEN 'Tue' THEN 3 WHEN 'Wed' THEN 4 WHEN 'Thu' THEN 5 WHEN 'Fri' THEN 6 WHEN 'Sat' THEN 7 WHEN 'Sun' THEN 1 END,
			CASE s.[departureDOW] WHEN 'Mon' THEN 2 WHEN 'Tue' THEN 3 WHEN 'Wed' THEN 4 WHEN 'Thu' THEN 5 WHEN 'Fri' THEN 6 WHEN 'Sat' THEN 7 WHEN 'Sun' THEN 1 END,
			at.ActionTypeID,
			i.IATANumberID,s.travelAgencyName,s.travelAgencyAddress1,s.travelAgencyAddress2,tLoc.LocationID,
			s.travelAgencyPhone,s.travelAgencyFax,
			s.travelAgencyEmail,
			HASHBYTES('MD5', UPPER(TRIM(ISNULL(customerAddress1,''))) + UPPER(TRIM(ISNULL(customerAddress2,''))) + UPPER(TRIM(ISNULL(REPLACE(customerCity,'Unknown',''),''))) + UPPER(TRIM(ISNULL(REPLACE(customerState,'Unknown',''),''))) + UPPER(TRIM(ISNULL(REPLACE(customerCountry,'Unknown',''),''))) + UPPER(TRIM(ISNULL(REPLACE(customerPostalCode,'Unknown',''),'')))),
			s.PsuedoCity,s.roomCategory,s.[GDSRateAccessCode],s.[GDSBookingCode],ISNULL(s.[rateTaxInclusive],CONVERT(bit,0)) AS [rateTaxInclusive],
			NULLIF(s.taxesAmount,''),s.taxesCurrency,s.loyaltyCertificateNumber
	FROM [temp].[sabreAPI_RawDataForLoading] s
		LEFT JOIN SabreAPI.[ActionType] at ON ISNULL(at.actionType,'NULL') = ISNULL(s.ActionType,'NULL')
		LEFT JOIN SabreAPI.[IATANumber] i ON i.IATANumber = ISNULL(s.IATANumber,'')
		LEFT JOIN #LOCATION gLoc ON gLoc.City_Text = ISNULL(s.customerCity,'NULL')
									AND gLoc.State_Text = ISNULL(s.customerState,'NULL')
									AND gLoc.Country_Text = ISNULL(s.customerCountry,'NULL')
									AND gLoc.PostalCode_Text = ISNULL(s.customerPostalCode,'NULL')
									AND gLoc.Area_Text = ISNULL(s.customerArea,'NULL')
									AND gLoc.Region_Text = ISNULL(s.customerRegion,'NULL')

		LEFT JOIN #LOCATION tLoc ON tLoc.City_Text = ISNULL(s.travelAgencyCity,'NULL')
									AND tLoc.State_Text = ISNULL(s.travelAgencyState,'NULL')
									AND tLoc.Country_Text = ISNULL(s.travelAgencyCountry,'NULL')
									AND tLoc.PostalCode_Text = ISNULL(s.travelAgencyPostalCode,'NULL')
									AND tLoc.Area_Text = ISNULL(s.travelAgencyArea,'NULL')
									AND tLoc.Region_Text = ISNULL(s.travelAgencyRegion,'NULL')
		LEFT JOIN SabreAPI.[Guest_CompanyName] gcn ON gcn.CompanyName = ISNULL(s.customerCompanyName,'')
		LEFT JOIN SabreAPI.[Guest_EmailAddress] gea ON gea.emailAddress = ISNULL(s.customerEmail,'')
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[LoadTransactions]'
GO


ALTER PROCEDURE [sabreAPI].[LoadTransactions]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	-- POPULATE [temp].[sabreAPI_RawDataForLoading] -----------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE [temp].[sabreAPI_RawDataForLoading]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC dbo.Populate_temp_sabreAPI_RawDataForLoading @QueueID
	-----------------------------------------------------------------------------------

	-- POPULATE LOCATION TABLES -------------------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE LOCATION TABLES
		-------------------------------------------------------------'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC sabreAPI.load_LocationTables @QueueID

		IF OBJECT_ID('tempdb..#LOCATION') IS NOT NULL
			DROP TABLE #LOCATION;

		CREATE TABLE #LOCATION
		(
			LocationID int NOT NULL,
			City_Text nvarchar(255) NOT NULL,
			State_Text nvarchar(255) NOT NULL,
			Country_Text nvarchar(255) NOT NULL,
			PostalCode_Text nvarchar(255) NOT NULL,
			Area_Text nvarchar(255) NOT NULL,
			Region_Text nvarchar(255) NOT NULL
		)

		INSERT INTO #LOCATION(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text,Area_Text,Region_Text)
		SELECT DISTINCT l.LocationID,ISNULL(ci.City_Text,'NULL'),ISNULL(st.State_Text,'NULL'),ISNULL(co.Country_Text,'NULL'),ISNULL(p.PostalCode_Text,'NULL'),ISNULL(a.Area_Text,'NULL'),ISNULL(r.Region_Text,'NULL')
		FROM  SabreAPI.[Location] l
			LEFT JOIN SabreAPI.[City] ci ON ci.CityID = l.CityID
			LEFT JOIN SabreAPI.[State] st ON st.StateID = l.StateID
			LEFT JOIN SabreAPI.[Country] co ON co.CountryID = l.CountryID
			LEFT JOIN SabreAPI.[PostalCode] p ON p.PostalCodeID = l.PostalCodeID
			LEFT JOIN SabreAPI.[Area] a ON a.AreaID = l.AreaID
			LEFT JOIN SabreAPI.[Region] r ON r.RegionID = l.RegionID

		SET @print = '-------------------------------------------------------------'
		RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------------

	-- ADD ACTION TYPE ----------------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.ActionType'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC sabreAPI.load_ActionType
	-----------------------------------------------------------------------------------
	

	-- SabreAPI.[Guest_CompanyName] ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Guest_CompanyName]'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC sabreAPI.load_Guest_CompanyName
	-----------------------------------------------------------------------------------

	-- SabreAPI.[Guest_EmailAddress] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Guest_EmailAddress]'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC sabreAPI.load_Guest_EmailAddress
	-----------------------------------------------------------------------------------


	-- SabreAPI.[Channel] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Channel]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_Channel
	-----------------------------------------------------------------------------------
	
	-- SabreAPI.[Chain] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Chain]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_Chain
	-----------------------------------------------------------------------------------

	-- CREATE TEMP TABLE & POPULATE ---------------------------------------------------
		 -- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[RawDataForLoading_Temp]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC SabreAPI.load_RawDataForLoading_Temp @QueueID
	-----------------------------------------------------------------------------------

	-- SabreAPI.[IATANumber] ---------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[IATANumber]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_IATANumber
	-----------------------------------------------------------------------------------

	-- SabreAPI.[TravelAgent] ---------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[TravelAgent]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_TravelAgent
	-----------------------------------------------------------------------------------

	-- SabreAPI.[BillingDescription] -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[BillingDescription]'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC sabreAPI.load_BillingDescription
	-----------------------------------------------------------------------------------


	-- SabreAPI.[SecondarySource] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[SecondarySource]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_SecondarySource
	-----------------------------------------------------------------------------------

	-- SabreAPI.[SubSource] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[SubSource]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_SubSource
	-----------------------------------------------------------------------------------

	-- SabreAPI.[CROCode] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[CROCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_CROCode
	-----------------------------------------------------------------------------------

	-- SabreAPI.[xbeTemplate] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[xbeTemplate]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_xbeTemplate
	-----------------------------------------------------------------------------------

	-- SabreAPI.[BookingSource] ------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[BookingSource]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_BookingSource
	-----------------------------------------------------------------------------------

	-- SabreAPI.[Consortia] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Consortia]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_Consortia
	-----------------------------------------------------------------------------------

	-- SabreAPI.[CoroporateCode] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[CorporateCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_CorporateCode
	-----------------------------------------------------------------------------------

	-- SabreAPI.[Guest] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Guest]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_Guest
	-----------------------------------------------------------------------------------

	-- SabreAPI.[hotel] --------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[hotel]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_Hotel
	-----------------------------------------------------------------------------------

	-- SabreAPI.[LoyaltyNumber] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[LoyaltyNumber]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_LoyaltyNumber
	-----------------------------------------------------------------------------------

	-- SabreAPI.[LoyaltyProgram] -----------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[LoyaltyProgram]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_LoyaltyProgram
	-----------------------------------------------------------------------------------

	-- SabreAPI.[PromoCode] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[PromoCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_PromoCode
	-----------------------------------------------------------------------------------

	-- SabreAPI.[RateCategory] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[RateCategory]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_RateCategory
	-----------------------------------------------------------------------------------

	-- SabreAPI.[PMSRateTypeCode] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[PMSRateTypeCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_PMSRateTypeCode
	-----------------------------------------------------------------------------------

	-- SabreAPI.[RateTypeCode] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[RateTypeCode]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_RateTypeCode
	-----------------------------------------------------------------------------------

	-- SabreAPI.[RoomType] -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[RoomType]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_RoomType
	-----------------------------------------------------------------------------------

		-- SabreAPI.[RoomCategory] -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[RoomCategory]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_RoomCategory
	-----------------------------------------------------------------------------------

	-- SabreAPI.[UserName] -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[UserName]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_UserName
	-----------------------------------------------------------------------------------

	-- SabreAPI.[VIP_Level] ----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[VIP_Level]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_VIP_Level
	-----------------------------------------------------------------------------------

	-- SabreAPI.[TransactionDetail] ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[TransactionDetail]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_TransactionDetail
	-----------------------------------------------------------------------------------

	-- SabreAPI.[TransactionsExtended] ------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[TransactionsExtended]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_TransactionsExtended
	-----------------------------------------------------------------------------------

	-- SabreAPI.[TransactionStatus] ---------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[TransactionStatus]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_TransactionStatus
	-----------------------------------------------------------------------------------

	-- SabreAPI.[Transactions] -------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': SabreAPI.[Transactions]'
		RAISERROR(@print,10,1) WITH NOWAIT
		
		EXEC sabreAPI.load_Transactions
	-----------------------------------------------------------------------------------

	-- POPULATE MRT -------------------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE SabreAPI.[MostRecentTransactions]'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC SabreAPI.[PopulateMostRecentTransactions]
	-----------------------------------------------------------------------------------

	-- PRINT STATUS --
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [sabreAPI].[RawDataForLoading]'
GO







ALTER FUNCTION [sabreAPI].[RawDataForLoading]
(
	@QueueID int = NULL
)
RETURNS TABLE 
AS
RETURN 
(

	WITH cte_MRT(confirmationNumber,arrivalDate,departureDate)
	AS
	(
		SELECT DISTINCT mrt.confirmationNumber, td.arrivalDate, td.departureDate
		FROM dbo.MostRecentTransactions mrt
		INNER JOIN dbo.Transactions t ON mrt.TransactionID = t.TransactionID
		INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		WHERE mrt.confirmationNumber NOT IN (
			SELECT confirmationNumber
			FROM ETL.dbo.Import_SabreAPI 
			WHERE 
			QueueID = @QueueID
			AND 
			status <> 'Cancelled'
			AND arrivalDate IS NOT NULL
		)
		UNION
		SELECT DISTINCT confirmationNumber, arrivalDate, departureDate
		FROM ETL.dbo.Import_SabreAPI 
		WHERE 
		QueueID = @QueueID
		AND 
		status <> 'Cancelled'
		AND arrivalDate IS NOT NULL
	)


	
	SELECT DISTINCT @QueueID AS QueueID,
		ISNULL(cnm.SupersetChainName,'') AS chainName,CONVERT(int,NULLIF(cnm.SupersetChainID,'')) AS chainID,
		LEFT(ISNULL(hotelName,''), 30) AS hotelName,
		NULLIF(hotelID,'') AS hotelID,ISNULL(SAPID,'') AS SAPID,
		ISNULL(hotelCode,'') AS hotelCode,
		--ISNULL([SabreAPI].[CalculatedBillingDescription]([sabreAPI].[CalculatedChannel](s.channel), s.secondarySource, s.subSource, s.subSourceCode,s.channelCode),'') AS billingDescription,
		ISNULL([SabreAPI].[CalculatedBillingDescription]([dbo].[Channel_Mapping_byName](s.channel), s.secondarySource, s.subSource, s.subSourceCode,s.channelCode),'') AS billingDescription,
		CONVERT(datetime,NULLIF(REPLACE(LEFT(CASE resStatus WHEN 'Commit' THEN createDateTime ELSE lastModifyDateTime END ,19),'T',' '),'')) AS transactionTimeStamp,
		CONVERT(int,NULLIF(faxNotificationCount,'')) AS faxNotificationCount,
		--ISNULL([sabreAPI].[CalculatedChannel](s.channel),'') AS channel,
		ISNULL(s.channel,'') AS channel,
		--COALESCE(s.lastModifyingChannel,[sabreAPI].[CalculatedChannel](s.channel),'') AS lastModifyingChannel,
		COALESCE(s.lastModifyingChannel,s.channel,'') AS lastModifyingChannel,
		ISNULL(secondarySource,'') AS secondarySource,
		ISNULL(sm.subSource,'') AS subSource,ISNULL(s.subSourceCode,'') AS subSourceCode,ISNULL(PMSRateTypeCode,'') AS PMSRateTypeCode,ISNULL(PMSRoomTypeCode,'') AS PMSRoomTypeCode,
		ISNULL(marketSourceCode,'') AS marketSourceCode,ISNULL(marketSegmentCode,'') AS marketSegmentCode,ISNULL(userName,'') AS userName
		,CASE WHEN ISNULL(status,'') = 'Reserved' THEN 'Confirmed'
		WHEN ISNULL(status,'') = 'Cancelled' THEN 'Cancelled'
		WHEN ISNULL(status,'') = 'no-show' THEN 'Cancelled'
		ELSE 'Confirmed'
		END AS status,
		ISNULL(s.confirmationNumber,'') AS confirmationNumber,CONVERT(datetime,NULLIF(confirmationDate,'')) AS confirmationDate,ISNULL(cancellationNumber,'') AS cancellationNumber,
		NULLIF(CONVERT(datetime,NULLIF(cancellationDate,'')),'1900-01-01') AS cancellationDate,ISNULL(salutation,'') AS salutation,ISNULL(guestFirstName,'') AS guestFirstName,
		ISNULL(guestLastName,'') AS guestLastName,SUBSTRING(ISNULL(customerID,''),1,12) AS customerID,ISNULL(customerAddress1,'') AS customerAddress1,
		ISNULL(customerAddress2,'') AS customerAddress2,ISNULL(customerCity,'') AS customerCity,ISNULL(customerState,'') AS customerState,ISNULL(customerPostalCode,'') AS customerPostalCode,
		ISNULL(customerPhone,'') AS customerPhone,ISNULL(customerCountry,'') AS customerCountry,ISNULL(customerArea,'') AS customerArea,ISNULL(customerRegion,'') AS customerRegion,
		ISNULL(customerCompanyName,'') AS customerCompanyName,
		CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')) AS arrivalDate
		,CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),'')) AS departureDate,
		DATEDIFF(day,CONVERT(datetime,NULLIF(confirmationDate,'')), CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),''))) AS bookingLeadTime
		,ISNULL(rateCategoryName,'') AS rateCategoryName,ISNULL(rateCategoryCode,'') AS rateCategoryCode,
		ISNULL(rateTypeName,'') AS rateTypeName,COALESCE(rateTypeCode,RatePlanCode,'') AS rateTypeCode,ISNULL(roomTypeName,'') AS roomTypeName,ISNULL(roomTypeCode,'') AS roomTypeCode,
		CONVERT(int,NULLIF(DATEDIFF(day,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')),CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),''))),'')) AS nights
		,CONVERT(decimal(12,2),
		[sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue)/CONVERT(int,NULLIF(DATEDIFF(day,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')),CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),''))),''))) AS averageDailyRate
		,CONVERT(int,NULLIF(rooms,'')) AS rooms,
		[sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue) AS reservationRevenue
		,ISNULL(currency,'') AS currency,ISNULL(IATANumber,'') AS IATANumber,
		ISNULL(travelAgencyName,'') AS travelAgencyName,ISNULL(travelAgencyAddress1,'') AS travelAgencyAddress1,ISNULL(travelAgencyAddress2,'') AS travelAgencyAddress2,
		ISNULL(travelAgencyCity,'') AS travelAgencyCity,ISNULL(travelAgencyState,'') AS travelAgencyState,ISNULL(travelAgencyPostalCode,'') AS travelAgencyPostalCode,
		ISNULL(travelAgencyPhone,'') AS travelAgencyPhone,ISNULL(travelAgencyFax,'') AS travelAgencyFax,ISNULL(travelAgencyCountry,'') AS travelAgencyCountry,
		ISNULL(travelAgencyArea,'') AS travelAgencyArea,ISNULL(travelAgencyRegion,'') AS travelAgencyRegion,ISNULL(travelAgencyEmail,'') AS travelAgencyEmail,
		CONVERT(int,NULLIF(consortiaCount,'')) AS consortiaCount,ISNULL(consortiaName,'') AS consortiaName,
		ISNULL(CONVERT(decimal(18,2),NULLIF(totalPackageRevenue,'')),0.00) AS totalPackageRevenue,
		CASE WHEN ISNULL(optIn,'') = 'Yes' THEN 'Y' WHEN ISNULL(optIn,'') = 'No' THEN 'N' ELSE 'N' END AS optIn
		,ISNULL(customerEmail,'') AS customerEmail,
		CONVERT(int,NULLIF(adultCount,'')) + ISNULL(CONVERT(int,NULLIF(childrenCount,'')),0) AS totalGuestCount
		,CONVERT(int,NULLIF(adultCount,'')) AS adultCount,
		ISNULL(CONVERT(int,NULLIF(childrenCount,'')),0) AS childrenCount
		,COALESCE(creditCardType,creditCardTypeAlternate,'') AS creditCardType,
		CASE WHEN ISNULL(actionType,'') = 'Commit' THEN 'N'
			WHEN ISNULL(actionType,'') = 'Modify' THEN 'M'
			WHEN ISNULL(actionType,'') = 'Cancel' THEN 'X'
			ELSE '' END AS actionType,
		ISNULL(shareWith,'N') AS shareWith,
		CASE DATEPART(dw,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')))
			WHEN 1 THEN 'Sun'
			WHEN 2 THEN 'Mon'
			WHEN 3 THEN 'Tue'
			WHEN 4 THEN 'Wed'
			WHEN 5 THEN 'Thu'
			WHEN 6 THEN 'Fri'
			WHEN 7 THEN 'Sat'
			END AS arrivalDOW,
			CASE DATEPART(dw,CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),'')))
			WHEN 1 THEN 'Sun'
			WHEN 2 THEN 'Mon'
			WHEN 3 THEN 'Tue'
			WHEN 4 THEN 'Wed'
			WHEN 5 THEN 'Thu'
			WHEN 6 THEN 'Fri'
			WHEN 7 THEN 'Sat' 
			END AS departureDOW
		,ISNULL(itineraryNumber,'') AS itineraryNumber,ISNULL(secondaryCurrency,'') AS secondaryCurrency,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyExchangeRate,'')) AS secondaryCurrencyExchangeRate,CONVERT(decimal(18,2),NULLIF(secondaryCurrencyAverageDailyRate,'')) AS secondaryCurrencyAverageDailyRate,
		CONVERT(decimal(18,2),NULLIF(secondaryCurrencyReservationRevenue,'')) AS secondaryCurrencyReservationRevenue,CONVERT(decimal(18,2),NULLIF(secondaryCurrencyPackageRevenue,'')) AS secondaryCurrencyPackageRevenue,
		CONVERT(decimal(18,2),NULLIF(TRIM(commisionPercent),'')) AS commisionPercent,ISNULL(membershipNumber,'') AS membershipNumber,ISNULL(corporationCode,'') AS corporationCode,
		ISNULL(promotionalCode,'') AS promotionalCode,ISNULL(CROCode,'') AS CROCode,ISNULL(channelConnectConfirmationNumber,'') AS channelConnectConfirmationNumber,
		CASE ISNULL(primaryGuest,'')  WHEN 'true' THEN 'Y' WHEN 'false' THEN 'N' ELSE '' END AS primaryGuest,
		ISNULL(loyaltyProgram,'') AS loyaltyProgram,ISNULL(loyaltyNumber,'') AS loyaltyNumber,ISNULL(vipLevel,'') AS vipLevel,
		ISNULL(s.xbeTemplateName,'') AS xbeTemplateName,ISNULL(xbeShellName,'') AS xbeShellName,ISNULL(profileTypeSelection,'') AS profileTypeSelection,
		Superset.dbo.convertCurrencyToUSD(CONVERT(decimal(12,2),
		[sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue)/CONVERT(int,NULLIF(DATEDIFF(day,CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')),CONVERT(datetime,NULLIF(ISNULL(s.departureDate,mrt.departureDate),''))),''))) ,currency,confirmationDate) AS averageDailyRateUSD,
		Superset.dbo.convertCurrencyToUSD([sabreAPI].[CalculatedReservationRevenue](revenueAmountBeforeTax,revenueAmountAfterTax,totalPackageRevenue),currency,confirmationDate) AS reservationRevenueUSD,
		Superset.dbo.convertCurrencyToUSD(ISNULL(totalPackageRevenue,0.00),currency,confirmationDate) AS totalPackageRevenueUSD,
		(SELECT ISNULL([ImportFinished],[ImportStarted]) FROM ETL.dbo.[Queue] WHERE QueueID = @QueueID) AS timeLoaded,
		CASE
			WHEN ISNULL(tag1.[iPrefer Number],'') > '' THEN 1
			WHEN ISNULL(tag2.[iPrefer Number],'') > '' THEN 1
			ELSE 0
		END AS LoyaltyNumberTagged
		,ISNULL(cpi.IsMemberEnabled,0) AS LoyaltyNumberValidated
		,ISNULL(PsuedoCity,N'') AS PsuedoCity,ISNULL(roomCategory,N'') AS roomCategory
		,ISNULL(GDSBookingCode,N'') AS GDSBookingCode,ISNULL(GDSRateAccessCode,N'') AS GDSRateAccessCode,ISNULL(rateTaxInclusive,N'') AS rateTaxInclusive
		,ISNULL(taxesAmount,N'') AS taxesAmount,ISNULL(taxesCurrency,N'') AS taxesCurrency, ISNULL(loyaltyCertificateNumber,N'') AS loyaltyCertificateNumber
	FROM ETL.dbo.Import_SabreAPI s
		LEFT JOIN
			(
				SELECT [iPrefer_Number],Membership_Date,IsMemberEnabled
				FROM [Superset].[BSI].[Customer_Profile_Import]
			) cpi ON cpi.iPrefer_Number = s.loyaltyNumber
				AND s.arrivalDate >= cpi.Membership_Date
				AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		LEFT JOIN Superset.[BSI].[TaggedCustomers] tag1 ON tag1.[iPrefer Number] = s.loyaltyNumber
														AND tag1.Hotel_Code = s.hotelCode
														AND s.arrivalDate >= tag1.DateTagged
														AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
		LEFT JOIN [ReservationBilling].[dbo].[Templates] temp ON temp.xbeTemplateName = s.xbeTemplateName
		LEFT JOIN [loyalty].[dbo].[Customer_Profile_Import] cpi2 ON cpi2.Email = s.customerEmail
		LEFT JOIN Superset.[BSI].[TaggedCustomers] tag2 ON tag2.[iPrefer Number] = cpi2.iPrefer_Number
														AND tag2.Hotel_Code = s.hotelCode
														AND s.arrivalDate >= tag2.DateTagged
														AND s.arrivalDate >= DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0)
														AND temp.templateGroupID = 3
		LEFT JOIN sabreAPI.ChainNameAndIDMapping cnm ON cnm.SabreDWAPIEquivalent = s.chainCode
		LEFT JOIN sabreAPI.SubsourceMapping sm ON sm.SubsourceCode = s.subSourceCode
		LEFT JOIN cte_MRT mrt ON mrt.confirmationNumber = s.confirmationNumber
	WHERE QueueID = @QueueID
		AND primaryGuest = 'true'
		AND s.[status] IN ('Cancelled', 'Reserved','in-house','Checked Out', 'no-show')
		AND CONVERT(datetime,NULLIF(ISNULL(s.arrivalDate,mrt.arrivalDate),'')) IS NOT NULL
		AND ISDATE(NULLIF(confirmationDate,'')) = 1
		AND confirmationDate <> '0001-01-01T00:00:00'
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
