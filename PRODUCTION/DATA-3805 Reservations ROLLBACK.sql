/*
Run this script on:

        wus2-data-dp-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/1/2024 6:02:21 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Upload_LoyaltyNumber]'
GO


ALTER PROCEDURE [dbo].[Upload_LoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ThirtyDaysAgo datetime = DATEADD(day,-30,GETDATE()),
			@LN_NULL int,
			@IPreferConversionDate date = '2024-05-09'

	SELECT @LN_NULL = LoyaltyNumberID FROM dbo.LoyaltyNumber WHERE LEN(loyaltyNumber) = 0


	-- CREATE #MRT_LN -------------------------------------------------------------
	IF OBJECT_ID('tempdb..#MRT_LN') IS NOT NULL
		DROP TABLE #MRT_LN;
	CREATE TABLE #MRT_LN
	(
		TransactionID int NOT NULL,
		[iPrefer_Number] nvarchar(50) NOT NULL,
		PRIMARY KEY CLUSTERED(TransactionID,[iPrefer_Number])
	)
	-------------------------------------------------------------------------------

	-- CREATE & POPULATE #CPI -----------------------------------------------------
	IF OBJECT_ID('tempdb..#MRT_CPI') IS NOT NULL
		DROP TABLE #MRT_CPI;

	CREATE TABLE #MRT_CPI
	(
		[iPrefer_Number] [nvarchar](50) NOT NULL,
		[Email] [nvarchar](255) NOT NULL,
		[Membership_Date] [date] NOT NULL,

		PRIMARY KEY CLUSTERED([Email],[Membership_Date],[iPrefer_Number])
	)

	INSERT INTO #MRT_CPI([iPrefer_Number],[Email],[Membership_Date])
	SELECT DISTINCT
		ln.LoyaltyNumberName AS [iPrefer_Number]
		,ge.emailAddress AS [Email]
		,mp.Enrollment_Date AS [Membership_Date]
	FROM Loyalty.dbo.LoyaltyNumber ln
		LEFT JOIN Guests.dbo.Guest g ON ln.LoyaltyNumberID = g.LoyaltyNumberID
		LEFT JOIN Guests.dbo.Guest_EmailAddress ge on ge.Guest_EmailAddressID = g.Guest_EmailAddressID
		LEFT JOIN Loyalty.dbo.MemberProfile mp ON ln.LoyaltyNumberID = mp.LoyaltyNumberID
	WHERE NULLIF(ge.emailAddress,'') IS NOT NULL
		AND mp.Enrollment_Date IS NOT NULL
		AND mp.[MemberStatus] = 1
	-------------------------------------------------------------------------------

	------------------------------------------------------------------------
	--add loyalty number to the reservation if it came from the I Prefer website and we can match the email address
	------------------------------------------------------------------------
	;WITH cte_MRT
	AS
	(
		SELECT t.TransactionID,ge.emailAddress,td.arrivalDate, crs.channel
		FROM [dbo].Transactions t
			INNER JOIN [dbo].TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN dbo.vw_CRS_BookingSource crs ON crs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN [dbo].Guest g ON g.GuestID = t.GuestID
			INNER JOIN [dbo].Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
			LEFT JOIN dbo.TravelAgent ta on t.travelagentid = ta.travelagentid   --  new for 6/18/24 want to exclude TA from getting points
		WHERE NULLIF(t.LoyaltyNumberID,@LN_NULL) IS NULL
			AND (ta.email IS NULL OR TRIM(ta.Email) = '')     --  new for 9/27/24
			and ge.emailAddress <>''     --  new for 6/18/24
			AND
			(
				crs.ibeSource_GroupID IN(3)
				OR
				td.arrivalDate >= @IPreferConversionDate
			)
			AND t.transactionTimeStamp >= @ThirtyDaysAgo
	)
	INSERT INTO #MRT_LN(TransactionID,iPrefer_Number)
	SELECT mrt.TransactionID,cpi.iPrefer_Number
	FROM #MRT_CPI cpi
		INNER JOIN cte_MRT mrt ON mrt.emailAddress = NULLIF(cpi.Email,'')
							AND mrt.arrivalDate >= DATEADD(day,-30,cpi.Membership_Date)
							AND mrt.arrivalDate < CAST('9999-09-09' AS date)
							and mrt.channel <> 'PMS Rez Synch' -- NEW CHANGE 5/30/24 we don't want PMS reservations included

	INSERT INTO [dbo].LoyaltyNumber(loyaltyNumber)
	SELECT DISTINCT iPrefer_Number FROM #MRT_LN
		EXCEPT
	SELECT loyaltyNumber FROM [dbo].LoyaltyNumber

	UPDATE t
		SET LoyaltyNumberID = ln.LoyaltyNumberID,
			LoyaltyNumber_IsImputedFromEmail = 1
	FROM [dbo].Transactions t
		INNER JOIN #MRT_LN x ON x.TransactionID = t.TransactionID
		INNER JOIN [dbo].LoyaltyNumber ln ON ln.loyaltyNumber = x.iPrefer_Number
	------------------------------------------------------------------------


	------------------------------------------------------------------------
	--add loyalty number to the reservation if it came from the I Prefer website and we can match the email address
	------------------------------------------------------------------------
	DELETE #MRT_LN

		-- POPULATE #MRT --------------------------------------------------------------	
		IF OBJECT_ID('tempdb..#MRT') IS NOT NULL
			DROP TABLE #MRT;

		CREATE TABLE #MRT
		(
			TransactionID int NOT NULL,
			arrivalDate date,
			customerEmail nvarchar(255)
		)

		INSERT INTO #MRT(TransactionID,arrivalDate,customerEmail)
		SELECT mrt.TransactionID,mrt.arrivalDate,mrt.customerEmail
		FROM dbo.MostRecentTransactionsReporting mrt
			LEFT JOIN Core.dbo.iPreferCheckboxHotels ip ON mrt.hotelCode = ip.hotelCode --this tells us if the hotel allows I Prefer opt-ins on their website
		WHERE mrt.transactionTimeStamp >= @ThirtyDaysAgo --reservation was touched in the last 30 days
			AND	NULLIF(mrt.loyaltynumber,N'') IS NULL --there was no loyalty number in the booking
			AND	mrt.customerEmail IS NOT NULL --there was an email address in the booking
			AND mrt.channel <> 'PMS Rez Synch' --09/26/24 Exclude PMS booking channel
			AND (mrt.travelAgencyEmail IS NULL or TRIM(mrt.travelAgencyEmail) = '') --09/26/24 Exclude TA from getting points
			AND
			(
				(
					--opted in Brand site bookings
					mrt.optIn = 1
					AND
					mrt.ChannelReportLabel = 'IBE - PHG'
					AND
					mrt.subSourceReportLabel NOT IN('HHA Call Center','HHA Call Center - iPrefer Partners on HHA.org',
												'www.historichotels.org','www.historichotels.org-cro','www.historichotels.org-flex','www.historichotels.org-gcomi','www.historichotels.org-partner','www.historichotelsworldwide.com','www.historichotelsworldwide.com-flex',
												'Active International','LuxLink','Sky Auction',
												'www.phgoffers.com-choice','www.phgoffers.com-lion','www.phgoffers.com-svc',
												'www.preferredhotelgroup.com-luxlink','www.preferredhotelgroup.com-perx','www.preferredhotelgroup.com-skyauction','www.preferredhotelgroup.com-amex')
				)
				OR
				(
					--opted in Hotel site bookings
					mrt.ChannelReportLabel = 'IBE - Hotel' 
					AND 
					mrt.optIn = 1
					AND
					ip.hotelCode IS NOT NULL
				)
				OR
				(
					--I Prefer member rate from any channel
					mrt.rateTypeCode IN (SELECT RateType FROM authority.IPreferRateType WHERE AutoEnroll=1)
				)
				OR
				(
					--I Prefer member rate from any channel this rate code was changed as of 1/1/21 and has previous reservations we don't want tagged
					mrt.ratetypecode = 'MKTMPE' and mrt.confirmationDate > '2020-12-31'
				)
				OR
				(
					--I Prefer member rate from any channel this rate code was changed as of 2/11/21 and has previous reservations we don't want tagged
					mrt.ratetypecode = 'MKTIPVIP' and mrt.confirmationDate > '2021-02-10'
				)
			)
		-------------------------------------------------------------------------------

		-- POPULATE #CPI --------------------------------------------------------------
		IF OBJECT_ID('tempdb..#CPI') IS NOT NULL
			DROP TABLE #CPI;
		CREATE TABLE #CPI
		(
			iPrefer_Number nvarchar(50) NOT NULL,
			Email nvarchar(100) NOT NULL,
			Membership_Date date NOT NULL,
			Disabled_Date date NOT NULL,

			PRIMARY KEY CLUSTERED(Email,Membership_Date,Disabled_Date)
		)

		INSERT INTO #CPI(iPrefer_Number,Email,Membership_Date,Disabled_Date)
		SELECT MAX(iPrefer_Number),Email,DATEADD(DAY,-7,Membership_Date),ISNULL(Disabled_Date,'9999-09-09')
		FROM Loyalty.dbo.Customer_Profile_Import
		WHERE NULLIF(Email,'') IS NOT NULL
			AND [MemberStatus] = 1
		GROUP BY Email,DATEADD(DAY,-7,Membership_Date),ISNULL(Disabled_Date,'9999-09-09')
		-------------------------------------------------------------------------------

		-- POPULATE #MRT_LN -----------------------------------------------------------
		INSERT INTO #MRT_LN(TransactionID,iPrefer_Number)
		SELECT mrt.TransactionID,c.iPrefer_Number
		FROM #MRT mrt
			INNER JOIN #CPI c ON c.Email = mrt.customerEmail --email address between profile and reservation matches
									AND mrt.arrivalDate >= c.Membership_Date --the member was created within 7 days of arrival
									AND mrt.arrivalDate < c.Disabled_Date --profile either is not disabled, or was not disabled at time of arrival
		-------------------------------------------------------------------------------

		-- Loyalty Number -------------------------------------------------------------
		INSERT INTO [dbo].LoyaltyNumber(loyaltyNumber)
		SELECT DISTINCT iPrefer_Number FROM #MRT_LN
			EXCEPT
		SELECT loyaltyNumber FROM [dbo].LoyaltyNumber

		UPDATE t
			SET LoyaltyNumberID = ln.LoyaltyNumberID,
				LoyaltyNumber_IsImputedFromEmail = 1
		FROM [dbo].Transactions t
			INNER JOIN #MRT_LN x ON x.TransactionID = t.TransactionID
			INNER JOIN [dbo].LoyaltyNumber ln ON ln.loyaltyNumber = x.iPrefer_Number
		-------------------------------------------------------------------------------
	------------------------------------------------------------------------


	------------------------------------------------------------------------
	--Set old I Prefer number to new I Prefer number if the guest is still using their old one
	------------------------------------------------------------------------
	DELETE #MRT_LN;

	INSERT INTO #MRT_LN(TransactionID,iPrefer_Number)
	SELECT t.TransactionID,o2n.[Membership Number]
	FROM [dbo].Transactions t
		INNER JOIN [dbo].TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		INNER JOIN dbo.vw_CRS_BookingSource crs ON crs.BookingSourceID = t.CRS_BookingSourceID
		INNER JOIN [dbo].LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		INNER JOIN [loyalty].[dbo].[IPreferMapping_OldToNew] o2n ON o2n.[Old Membership Number] = ln.loyaltyNumber
		LEFT JOIN dbo.TravelAgent ta on t.travelagentid = ta.travelagentid   --  new for 9/26/24 want to exclude TA from getting points
	WHERE t.transactionTimeStamp >= @ThirtyDaysAgo --reservation arrives on or after the first day of the current month (don't want to adjust already billed reservations)
		AND crs.channel <> 'PMS Rez Synch'
		AND (ta.Email IS NULL or TRIM(ta.Email) = '') --09/26/24 Exclude TA from getting points
		

	INSERT INTO [dbo].LoyaltyNumber(loyaltyNumber)
	SELECT DISTINCT iPrefer_Number FROM #MRT_LN
		EXCEPT
	SELECT loyaltyNumber FROM [dbo].LoyaltyNumber

	UPDATE t
		SET LoyaltyNumberID = ln.LoyaltyNumberID
	FROM [dbo].Transactions t
		INNER JOIN #MRT_LN x ON x.TransactionID = t.TransactionID
		INNER JOIN [dbo].LoyaltyNumber ln ON ln.loyaltyNumber = x.iPrefer_Number
	WHERE t.LoyaltyNumberID != ln.LoyaltyNumberID
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	--Set loyaltyNumberValidated to true if the I Prefer customer profile record was active at the time the reservation arrived with their I Prefer number
	------------------------------------------------------------------------
	UPDATE td
		SET LoyaltyNumberValidated = CASE WHEN c.iPrefer_Number IS NULL THEN 0 ELSE 1 END
	FROM [dbo].Transactions t
		INNER JOIN [dbo].TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN [dbo].LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN [loyalty].[dbo].[Customer_Profile_Import] c ON c.iPrefer_Number = NULLIF(ln.loyaltyNumber,'')
															AND td.arrivalDate >= DATEADD(DAY,-7,c.Membership_Date) --reservation arrives on or after the profile start date
															AND td.arrivalDate < ISNULL(c.Disabled_Date,'9999-09-09') --profile either is not disabled, or was not disabled at time of arrival
	WHERE t.transactionTimeStamp >= @ThirtyDaysAgo --reservation arrives on or after the first day of the current month (don't want to adjust already billed reservations)
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- SET LoyaltyNumber_IsNewMember
	------------------------------------------------------------------------
	UPDATE t
		SET LoyaltyNumber_IsNewMember = CASE
											WHEN NULLIF(ln.[LoyaltyNumberID],@LN_NULL) IS NULL THEN NULL
											ELSE
												CASE WHEN DATEDIFF(DAY,ts.confirmationDate,mp.Enrollment_Date) BETWEEN 0 AND 3 THEN 1 ELSE 0 END
										END
	FROM dbo.Transactions t
		INNER JOIN dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		INNER JOIN Loyalty.[dbo].[LoyaltyNumber] lln ON lln.[LoyaltyNumberName] = ln.loyaltyNumber
		INNER JOIN Loyalty.dbo.MemberProfile mp ON mp.LoyaltyNumberID = lln.LoyaltyNumberID
	WHERE t.LoyaltyNumber_IsNewMember IS NULL
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- SET t.LoyaltyNumber_IsImputedFromEmail = 0
	------------------------------------------------------------------------
	UPDATE t
		SET t.LoyaltyNumber_IsImputedFromEmail = 0
	FROM dbo.Transactions t
	WHERE t.LoyaltyNumber_IsImputedFromEmail IS NULL
		AND t.LoyaltyNumberID IS NOT NULL
	------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
