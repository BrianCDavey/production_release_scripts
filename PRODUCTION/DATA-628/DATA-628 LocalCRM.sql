USE LocalCRM
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.LocalCRM    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.LocalCRM

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/18/2019 1:52:40 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SyncToCore]'
GO







-- =============================================
-- Author:		Kris Scott
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[SyncToCore] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--Synch primary hotel data
	MERGE INTO Core.dbo.hotels AS tgt
	USING
		(
			SELECT
				a.accountid AS crmGuid
				,a.accountnumber AS code
				,a.name AS hotelName
				,a.address1_line1 AS physicalAddress1
				,a.address1_line2 AS physicalAddress2
				,a.phg_cityidname AS physicalCity
				,s.phg_subareacode AS state
				,c.phg_code2 AS country
				,a.address1_postalcode AS postalCode
				,geography::STPointFromText('POINT(' + CAST(a.address1_longitude AS VARCHAR(20)) + ' ' + CAST(a.address1_latitude AS VARCHAR(20)) + ')',4326) AS geoLocation
				,a.phg_yearbuilt AS yearBuilt
				,a.phg_yearlastrenovated AS yearLastRenovated
				,a.phg_floors AS floors
				,a.telephone1 AS primaryPhone
				,a.emailaddress1 AS primaryEmail
				,a.websiteurl AS primaryWebsite
				,a.phg_openingdate AS openingDate
				,a.phg_hoteltotalrooms AS totalRooms
				,a.phg_currencycode AS currencyCode
				,a.phg_competitiveset AS competitiveSet
				,a.phg_hotelregionname AS geographicRegionCode
				,a.phg_legalentityname AS legalEntity
			FROM LocalCRM.[dbo].[Account] a 
				LEFT OUTER JOIN LocalCRM.dbo.phg_country c ON a.phg_countryid = c.phg_countryid
				LEFT OUTER JOIN LocalCRM.dbo.phg_state s ON a.phg_stateprovinceid = s.phg_stateid
			WHERE a.phg_hotelaccount = 1 --is listed AS hotel account type in CRM
				AND a.accountnumber IS NOT NULL --has a hotel code in CRM
				AND a.statuscode IN (100000000,100000001,100000002,100000003) --is one of our hotel statuses
				AND a.stateCode = 0 --is in "active" state
		) AS src ON src.crmGuid = tgt.crmGuid
	WHEN MATCHED THEN
		UPDATE
			SET 
			tgt.crmGuid = src.crmGuid
			,tgt.code = src.code
			,tgt.hotelName = src.hotelName
			,tgt.physicalAddress1 = src.physicalAddress1
			,tgt.physicalAddress2 = src.physicalAddress2
			,tgt.physicalCity = src.physicalCity
			,tgt.state = src.state
			,tgt.country = src.country
			,tgt.postalCode = src.postalCode
			,tgt.geoLocation = src.geoLocation
			,tgt.yearBuilt = src.yearBuilt
			,tgt.yearLastRenovated = src.yearLastRenovated
			,tgt.floors = src.floors
			,tgt.primaryPhone = src.primaryPhone
			,tgt.primaryEmail = src.primaryEmail
			,tgt.primaryWebsite = src.primaryWebsite
			,tgt.openingDate = src.openingDate
			,tgt.totalRooms = src.totalRooms
			,tgt.currencyCode = src.currencyCode
			,tgt.competitiveSet = src.competitiveSet
			,tgt.geographicRegionCode = src.geographicRegionCode
			,tgt.legalEntity = src.legalEntity
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (crmGuid,code,hotelName,physicalAddress1,physicalAddress2,physicalCity,state,country,postalCode,geoLocation,
				yearBuilt,yearLastRenovated,floors,primaryPhone,primaryEmail,primaryWebsite,openingDate,totalRooms,currencyCode,
				competitiveSet,geographicRegionCode,legalEntity)
		VALUES (src.crmGuid,src.code,src.hotelName,src.physicalAddress1,src.physicalAddress2,src.physicalCity,src.state,src.country,src.postalCode,src.geoLocation,
				src.yearBuilt,src.yearLastRenovated,src.floors,src.primaryPhone,src.primaryEmail,src.primaryWebsite,src.openingDate,src.totalRooms,src.currencyCode,
				src.competitiveSet,src.geographicRegionCode,src.legalEntity);
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;

	EXEC  dbo.SyncToCore_DeleteHotels

	--Synch hotel attributes
	BEGIN TRY
		BEGIN TRANSACTION
    
		DELETE FROM Core.dbo.hotels_hotelAttributes;
		DELETE FROM Core.dbo.hotelAttributes_hotelAttributeCategories;
		DELETE FROM Core.dbo.hotelAttributes;
		DELETE FROM Core.dbo.hotelAttributeCategories;
    
		INSERT INTO Core.dbo.hotelAttributes (name,webValue,description)
		SELECT PHG_name,PHG_name,PHG_Description FROM LocalCRM.dbo.HotelAttributeType
    
		INSERT INTO Core.dbo.hotelAttributeCategories (name,parentCategoryName,description)
		SELECT DISTINCT phg_attributetypename,null,null FROM LocalCRM.dbo.HotelAttributeType where phg_attributetypename IS NOT NULL
    
		INSERT INTO Core.dbo.hotelAttributes_hotelAttributeCategories (hotelAttributeName,hotelAttributeCategoryName)
		SELECT PHG_name,phg_attributetypename FROM LocalCRM.dbo.HotelAttributeType where phg_attributetypename IS NOT NULL
    
		INSERT INTO Core.dbo.hotels_hotelAttributes (hotelCode,hotelAttributeName)
		SELECT hotels.code,phg_hotelattributetypename 
		FROM Core.dbo.hotels 
			INNER JOIN LocalCRM.dbo.HotelAttributes ON hotels.crmGuid = HotelAttributes.phg_accountid
		WHERE phg_hotelattributetypename IS NOT NULL

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	  IF @@TRANCOUNT > 0
		 ROLLBACK TRANSACTION

	  -- Raise an error with the details of the exception
	  DECLARE @ErrMsg nvarchar(4000),@ErrSeverity int
	  SELECT @ErrMsg = ERROR_MESSAGE(),@ErrSeverity = ERROR_SEVERITY()

	  RAISERROR(@ErrMsg,@ErrSeverity,1)
	END CATCH  


  	--Synch brand/collections/contracted programs
	MERGE INTO [Core].[dbo].[hotels_brands] AS tgt
	USING
		(
			--get all brands/collections from CRM
			SELECT DISTINCT a.accountNumber AS [hotelCode],c.phg_code AS [brandCode],hc.phg_startdate AS [startDatetime],hc.phg_enddate AS [endDatetime]
			FROM LocalCRM.dbo.phg_hotelcollection hc
				INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
				INNER JOIN LocalCRM.dbo.account a ON hc.phg_hotel = a.accountid
			WHERE a.phg_hotelaccount = 1
				AND hc.phg_startdate IS NOT NULL
				AND a.accountNumber IS NOT NULL
				AND a.statuscode IN (100000000,100000001,100000002,100000003) --is one of our hotel statuses
				AND a.stateCode = 0 --is in "active" state
				AND hc.stateCode = 0
				AND hc.statusCode = 1

			UNION
			
			--get I Prefer participation from CRM,which Core still treats AS a brand,but CRM does not
			SELECT DISTINCT a.accountNumber AS [hotelCode],'IP' AS [brandCode],cp.phg_startdate AS [startDatetime],cp.phg_enddate AS [endDatetime]
			FROM [LocalCRM].[dbo].[phg_contractedprograms] cp
				INNER JOIN LocalCRM.dbo.account a ON cp.phg_accountnameid = a.accountid
			WHERE cp.phg_startdate IS NOT NULL
				AND a.accountNumber IS NOT NULL
				AND a.statuscode IN (100000000,100000001,100000002,100000003) --is one of our hotel statuses
				AND a.stateCode = 0 --is in "active" state
				AND cp.phg_contractedprogramstypename like '%i%prefer%'
				AND cp.stateCode = 0
				AND cp.statusCode = 1
		) AS src ON tgt.hotelCode = src.hotelCode
					AND tgt.brandCode = src.brandCode
					AND tgt.startDatetime = src.startDatetime
	WHEN MATCHED THEN
		UPDATE
			SET tgt.hotelCode = src.hotelCode,
				tgt.brandCode = src.brandCode,
				tgt.startDatetime = src.startDatetime,
				tgt.endDatetime = src.endDatetime
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (hotelCode,brandCode,startDatetime)
		VALUES (src.hotelCode,src.brandCode,src.startDatetime)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE;

		  	--Synch synxis IDs
	MERGE INTO [Core].[dbo].hotels_synxis AS tgt
	USING
		(
			SELECT DISTINCT a.accountNumber AS [hotelCode],a.phg_synxiscode AS [synxisCode],a.phg_synxisid AS [synxisID]
			FROM LocalCRM.dbo.account a
			WHERE a.phg_hotelaccount = 1										--is listed AS hotel account type in CRM
				AND a.phg_synxisid IS NOT NULL									--has a synxisID
				AND a.phg_synxisid > 0											--synxisID is a positive number (old CRM did not allow nulls in that field,used negatives AS default)
				AND a.accountnumber IS NOT NULL									--has a hotel code in CRM
				AND accountNumber NOT IN ('BCTS4','PHGTEST')					--not a test hotel
				AND a.statuscode IN (100000000,100000001,100000002,100000003)	--is one of our hotel statuses
				AND a.stateCode = 0												--is in "active" state
		) AS src ON tgt.hotelCode = src.hotelCode
	WHEN MATCHED THEN
		UPDATE
			SET tgt.hotelCode = src.hotelCode,
				tgt.synxisCode = src.synxisCode,
				tgt.synxisID = src.synxisID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (hotelCode,synxisCode,synxisID)
		VALUES (src.hotelCode,src.synxisCode,src.synxisID)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE;


	MERGE INTO Core.[dbo].[hotelBookingRewards] AS tgt
	USING
		(
			SELECT a.accountnumber,br.[phg_startDate],br.[phg_endDate],br.[phg_name],br.versionNumber
			FROM LocalCRM.[dbo].[IPreferBookingReward] br
				INNER JOIN LocalCRM.[dbo].[Account] a ON a.accountid = br.phg_hotelId
				INNER JOIN
						(
							SELECT phg_hotelId,[phg_startDate],MAX(versionNumber) AS versionNumber
							FROM LocalCRM.[dbo].[IPreferBookingReward]
							WHERE [stateCode] = 0
								AND [statusCode] = 1
								AND [phg_startDate] IS NOT NULL
							GROUP BY phg_hotelId,[phg_startDate]
						) x ON x.phg_hotelId = br.phg_hotelId AND x.phg_startDate= br.phg_startDate AND x.versionNumber = br.versionNumber
			WHERE a.phg_hotelaccount = 1
				AND a.statuscode IN (100000000,100000001,100000002,100000003) --is one of our hotel statuses
				AND a.stateCode = 0 --is in "active" state
				AND a.accountnumber is not null -- add on 10/18/2019 to prevent CRM job failure.
		) AS src ON src.accountnumber = tgt.[hotelCode]
					AND src.[phg_startDate] = tgt.[startDate]
	WHEN MATCHED THEN
		UPDATE
			SET tgt.[endDate] = src.[phg_endDate],
				tgt.[bookingReward] = src.[phg_name]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([hotelCode],[startDate],[endDate],[bookingReward])
		VALUES(accountnumber,[phg_startDate],[phg_endDate],[phg_name])
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE;


	MERGE INTO core.[dbo].[brands] AS tgt
	USING
		(
			SELECT DISTINCT phg_name,phg_code,phg_heirarchy,phg_gpsiteID
			FROM [LocalCRM].[dbo].[phg_collection]
			--WHERE statecode = 0
		) AS src ON src.phg_code = tgt.[Code]
	WHEN MATCHED THEN
		UPDATE
			SET 
				[Heirarchy] = src.phg_heirarchy,
				gpsiteID = src.phg_gpsiteID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([name],[Code],[Heirarchy], gpsiteID)
		VALUES(src.phg_name,src.phg_code,src.phg_heirarchy, phg_gpsiteID);

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
