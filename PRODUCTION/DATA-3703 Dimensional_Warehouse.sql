USE Dimensional_Warehouse
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 9/24/2024 2:33:41 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Update_Reservations_CorporateAccount]'
GO

ALTER PROCEDURE [dbo].[Update_Reservations_CorporateAccount]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_CC
	AS
	(
		SELECT ca.corporateAccountKey,t.confirmationNumber
		FROM dim.CorporateAccount ca
			INNER JOIN [Reservations].[dbo].[CorporateCode] cc ON cc.corporationCode = ca.[Corporate Code]
			INNER JOIN [Reservations].[dbo].[Transactions] t ON t.CorporateCodeID = cc.CorporateCodeID
	),
	cte_RC
	AS
	(
			SELECT 
		distinct ca.corporateAccountKey , t.confirmationNumber
		FROM dim.CorporateAccount ca
		    INNER JOIN [Dimensional_Warehouse].[dim].[CorporateAccount_RateCode] carc on ca.corporateAccountKey = carc.corporateAccountKey
			INNER JOIN [Reservations].[dbo].[RateCode] rc ON rc.RateCode = carc.[Rate Code]
			INNER JOIN [Reservations].[dbo].[Transactions] t ON t.RateCodeID = rc.RateCodeID
	),
	cte_CA
	AS
	(
		SELECT corporateAccountKey,confirmationNumber
		FROM cte_CC

			UNION ALL

		SELECT corporateAccountKey,confirmationNumber
		FROM cte_RC
		WHERE confirmationNumber NOT IN(SELECT confirmationNumber FROM cte_CC)
	)
	UPDATE r
		SET corporateAccountKey = ISNULL(ca.corporateAccountKey,0)
	FROM fact.Reservation r
		LEFT JOIN cte_CA ca ON ca.confirmationNumber = r.[Confirmation Number]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
