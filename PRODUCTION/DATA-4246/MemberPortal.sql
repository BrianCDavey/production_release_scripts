USE MemberPortal
GO

/*
Run this script on:

        (local)\CHISQZ01.MemberPortal    -  This database will be modified

to synchronize it with:

        AZ-SQ-PR-01.MemberPortal

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 1/8/2025 4:02:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContent]'
GO
ALTER TABLE [dbo].[cmsContent] DROP CONSTRAINT [FK_cmsContent_cmsContentType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsContent] DROP CONSTRAINT [FK_cmsContent_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsDataType]'
GO
ALTER TABLE [dbo].[cmsDataType] DROP CONSTRAINT [FK_cmsDataType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsDocument]'
GO
ALTER TABLE [dbo].[cmsDocument] DROP CONSTRAINT [FK_cmsDocument_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMedia]'
GO
ALTER TABLE [dbo].[cmsMedia] DROP CONSTRAINT [FK_cmsMedia_cmsContent_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsMedia] DROP CONSTRAINT [FK_cmsMedia_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMember]'
GO
ALTER TABLE [dbo].[cmsMember] DROP CONSTRAINT [FK_cmsMember_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsTags]'
GO
ALTER TABLE [dbo].[cmsTags] DROP CONSTRAINT [FK_cmsTags_cmsTags]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsTask]'
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [FK_cmsTask_cmsTaskType_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [FK_cmsTask_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [FK_cmsTask_umbracoUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [FK_cmsTask_umbracoUser1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoDomains]'
GO
ALTER TABLE [dbo].[umbracoDomains] DROP CONSTRAINT [FK_umbracoDomains_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] DROP CONSTRAINT [FK_umbracoExternalLogin_umbracoUser_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoRedirectUrl]'
GO
ALTER TABLE [dbo].[umbracoRedirectUrl] DROP CONSTRAINT [FK_umbracoRedirectUrl]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMacroProperty]'
GO
ALTER TABLE [dbo].[cmsMacroProperty] DROP CONSTRAINT [FK_cmsMacroProperty_cmsMacro_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsPropertyType]'
GO
ALTER TABLE [dbo].[cmsPropertyType] DROP CONSTRAINT [FK_cmsPropertyType_cmsPropertyTypeGroup_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoAccessRule]'
GO
ALTER TABLE [dbo].[umbracoAccessRule] DROP CONSTRAINT [FK_umbracoAccessRule_umbracoAccess_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContentType]'
GO
ALTER TABLE [dbo].[cmsContentType] DROP CONSTRAINT [FK_cmsContentType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContentType2ContentType]'
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] DROP CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_child]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] DROP CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_parent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsDocumentType]'
GO
ALTER TABLE [dbo].[cmsDocumentType] DROP CONSTRAINT [FK_cmsDocumentType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMember2MemberGroup]'
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] DROP CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMemberType]'
GO
ALTER TABLE [dbo].[cmsMemberType] DROP CONSTRAINT [FK_cmsMemberType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsTemplate]'
GO
ALTER TABLE [dbo].[cmsTemplate] DROP CONSTRAINT [FK_cmsTemplate_umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserGroup]'
GO
ALTER TABLE [dbo].[umbracoUserGroup] DROP CONSTRAINT [FK_startContentId_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoUserGroup] DROP CONSTRAINT [FK_startMediaId_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoAccess]'
GO
ALTER TABLE [dbo].[umbracoAccess] DROP CONSTRAINT [FK_umbracoAccess_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoAccess] DROP CONSTRAINT [FK_umbracoAccess_umbracoNode_id1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoAccess] DROP CONSTRAINT [FK_umbracoAccess_umbracoNode_id2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] DROP CONSTRAINT [FK_umbracoNode_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoRelation]'
GO
ALTER TABLE [dbo].[umbracoRelation] DROP CONSTRAINT [FK_umbracoRelation_umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelation] DROP CONSTRAINT [FK_umbracoRelation_umbracoNode1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUser2NodeNotify]'
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify] DROP CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserGroup2NodePermission]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] DROP CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserStartNode]'
GO
ALTER TABLE [dbo].[umbracoUserStartNode] DROP CONSTRAINT [FK_umbracoUserStartNode_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportMediaRelation]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation] DROP CONSTRAINT [PK__CMSImportMediaRelation__000000000000043D]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation]'
GO
ALTER TABLE [dbo].[CMSImportRelation] DROP CONSTRAINT [PK__CMSImportRelation__000000000000042C]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledItems]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems] DROP CONSTRAINT [PK__CMSImportScheduledItems__0000000000000453]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledTask]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTask] DROP CONSTRAINT [PK__CMSImportScheduledTask__0000000000000416]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] DROP CONSTRAINT [PK__CMSImportState__00000000000003F9]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContentVersion]'
GO
ALTER TABLE [dbo].[cmsContentVersion] DROP CONSTRAINT [PK_cmsContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContentXml]'
GO
ALTER TABLE [dbo].[cmsContentXml] DROP CONSTRAINT [PK_cmsContentXml]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContent]'
GO
ALTER TABLE [dbo].[cmsContent] DROP CONSTRAINT [PK_cmsContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsDataTypePreValues]'
GO
ALTER TABLE [dbo].[cmsDataTypePreValues] DROP CONSTRAINT [PK_cmsDataTypePreValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsDataType]'
GO
ALTER TABLE [dbo].[cmsDataType] DROP CONSTRAINT [PK_cmsDataType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsDocument]'
GO
ALTER TABLE [dbo].[cmsDocument] DROP CONSTRAINT [PK_cmsDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] DROP CONSTRAINT [PK_cmsMacro]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMedia]'
GO
ALTER TABLE [dbo].[cmsMedia] DROP CONSTRAINT [PK_cmsMedia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPreviewXml]'
GO
ALTER TABLE [dbo].[cmsPreviewXml] DROP CONSTRAINT [PK_cmsContentPreviewXml]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPropertyData]'
GO
ALTER TABLE [dbo].[cmsPropertyData] DROP CONSTRAINT [PK_cmsPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPropertyTypeGroup]'
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] DROP CONSTRAINT [PK_cmsPropertyTypeGroup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsTaskType]'
GO
ALTER TABLE [dbo].[cmsTaskType] DROP CONSTRAINT [PK_cmsTaskType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsTask]'
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [PK_cmsTask]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoDomains]'
GO
ALTER TABLE [dbo].[umbracoDomains] DROP CONSTRAINT [PK_umbracoDomains]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] DROP CONSTRAINT [PK_umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoMigration]'
GO
ALTER TABLE [dbo].[umbracoMigration] DROP CONSTRAINT [PK_umbracoMigrations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] DROP CONSTRAINT [PK_structure]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportMediaRelation]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation] DROP CONSTRAINT [DF__CMSImport__ByteS__7EF6D905]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation]'
GO
ALTER TABLE [dbo].[CMSImportRelation] DROP CONSTRAINT [DF__CMSImport__Impor__00DF2177]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation]'
GO
ALTER TABLE [dbo].[CMSImportRelation] DROP CONSTRAINT [DF__CMSImport__Updat__01D345B0]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledItems]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems] DROP CONSTRAINT [DF__CMSImport__Execu__03BB8E22]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledItems]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems] DROP CONSTRAINT [DF__CMSImport__InPro__04AFB25B]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledTask]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTask] DROP CONSTRAINT [DF__CMSImport__Impor__0697FACD]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] DROP CONSTRAINT [DF__CMSImport__Paren__0880433F]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] DROP CONSTRAINT [DF__CMSImport__Impor__09746778]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContentVersion]'
GO
ALTER TABLE [dbo].[cmsContentVersion] DROP CONSTRAINT [DF__cmsConten__Versi__74794A92]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsDocument]'
GO
ALTER TABLE [dbo].[cmsDocument] DROP CONSTRAINT [DF__cmsDocume__updat__7A3223E8]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsDocument]'
GO
ALTER TABLE [dbo].[cmsDocument] DROP CONSTRAINT [DF__cmsDocume__newes__7B264821]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] DROP CONSTRAINT [DF__cmsMacro__macroU__0C50D423]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] DROP CONSTRAINT [DF__cmsMacro__macroR__0D44F85C]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] DROP CONSTRAINT [DF__cmsMacro__macroC__0E391C95]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] DROP CONSTRAINT [DF__cmsMacro__macroC__0F2D40CE]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] DROP CONSTRAINT [DF__cmsMacro__macroD__10216507]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPropertyTypeGroup]'
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] DROP CONSTRAINT [DF_cmsPropertyTypeGroup_uniqueID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsTask]'
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [DF__cmsTask__closed__251C81ED]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsTask]'
GO
ALTER TABLE [dbo].[cmsTask] DROP CONSTRAINT [DF__cmsTask__DateTim__2610A626]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] DROP CONSTRAINT [DF_umbracoExternalLogin_createDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoMigration]'
GO
ALTER TABLE [dbo].[umbracoMigration] DROP CONSTRAINT [DF_umbracoMigration_createDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsContentVersion_ContentId] from [dbo].[cmsContentVersion]'
GO
DROP INDEX [IX_cmsContentVersion_ContentId] ON [dbo].[cmsContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsContentVersion_VersionId] from [dbo].[cmsContentVersion]'
GO
DROP INDEX [IX_cmsContentVersion_VersionId] ON [dbo].[cmsContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsContent] from [dbo].[cmsContent]'
GO
DROP INDEX [IX_cmsContent] ON [dbo].[cmsContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDataType_nodeId] from [dbo].[cmsDataType]'
GO
DROP INDEX [IX_cmsDataType_nodeId] ON [dbo].[cmsDataType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDictionary_key] from [dbo].[cmsDictionary]'
GO
DROP INDEX [IX_cmsDictionary_key] ON [dbo].[cmsDictionary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDocument] from [dbo].[cmsDocument]'
GO
DROP INDEX [IX_cmsDocument] ON [dbo].[cmsDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDocument_published] from [dbo].[cmsDocument]'
GO
DROP INDEX [IX_cmsDocument_published] ON [dbo].[cmsDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDocument_newest] from [dbo].[cmsDocument]'
GO
DROP INDEX [IX_cmsDocument_newest] ON [dbo].[cmsDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsMacroPropertyAlias] from [dbo].[cmsMacro]'
GO
DROP INDEX [IX_cmsMacroPropertyAlias] ON [dbo].[cmsMacro]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsMacro_UniqueId] from [dbo].[cmsMacro]'
GO
DROP INDEX [IX_cmsMacro_UniqueId] ON [dbo].[cmsMacro]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsMedia] from [dbo].[cmsMedia]'
GO
DROP INDEX [IX_cmsMedia] ON [dbo].[cmsMedia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsPropertyData_1] from [dbo].[cmsPropertyData]'
GO
DROP INDEX [IX_cmsPropertyData_1] ON [dbo].[cmsPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsPropertyData_2] from [dbo].[cmsPropertyData]'
GO
DROP INDEX [IX_cmsPropertyData_2] ON [dbo].[cmsPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsPropertyData_3] from [dbo].[cmsPropertyData]'
GO
DROP INDEX [IX_cmsPropertyData_3] ON [dbo].[cmsPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsPropertyTypeGroupUniqueID] from [dbo].[cmsPropertyTypeGroup]'
GO
DROP INDEX [IX_cmsPropertyTypeGroupUniqueID] ON [dbo].[cmsPropertyTypeGroup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsTags] from [dbo].[cmsTags]'
GO
DROP INDEX [IX_cmsTags] ON [dbo].[cmsTags]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsTaskType_alias] from [dbo].[cmsTaskType]'
GO
DROP INDEX [IX_cmsTaskType_alias] ON [dbo].[cmsTaskType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoAccessRule] from [dbo].[umbracoAccessRule]'
GO
DROP INDEX [IX_umbracoAccessRule] ON [dbo].[umbracoAccessRule]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoMigration] from [dbo].[umbracoMigration]'
GO
DROP INDEX [IX_umbracoMigration] ON [dbo].[umbracoMigration]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_uniqueID] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_uniqueID] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNodeObjectType] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNodeObjectType] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNodeParentId] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNodeParentId] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNodePath] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNodePath] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNodeTrashed] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNodeTrashed] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoRedirectUrl] from [dbo].[umbracoRedirectUrl]'
GO
DROP INDEX [IX_umbracoRedirectUrl] ON [dbo].[umbracoRedirectUrl]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoRelationType_alias] from [dbo].[umbracoRelationType]'
GO
DROP INDEX [IX_umbracoRelationType_alias] ON [dbo].[umbracoRelationType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoRelationType_name] from [dbo].[umbracoRelationType]'
GO
DROP INDEX [IX_umbracoRelationType_name] ON [dbo].[umbracoRelationType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoMigration]'
GO
DROP TABLE [dbo].[umbracoMigration]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsPropertyData]'
GO
DROP TABLE [dbo].[cmsPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsPreviewXml]'
GO
DROP TABLE [dbo].[cmsPreviewXml]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsDataTypePreValues]'
GO
DROP TABLE [dbo].[cmsDataTypePreValues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsContentXml]'
GO
DROP TABLE [dbo].[cmsContentXml]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsContentVersion]'
GO
DROP TABLE [dbo].[cmsContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportScheduledTask]'
GO
DROP TABLE [dbo].[CMSImportScheduledTask]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportScheduledItems]'
GO
DROP TABLE [dbo].[CMSImportScheduledItems]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoDomains]'
GO
DROP TABLE [dbo].[umbracoDomains]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsTask]'
GO
DROP TABLE [dbo].[cmsTask]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsTaskType]'
GO
DROP TABLE [dbo].[cmsTaskType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsMedia]'
GO
DROP TABLE [dbo].[cmsMedia]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsDocument]'
GO
DROP TABLE [dbo].[cmsDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsDataType]'
GO
DROP TABLE [dbo].[cmsDataType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsContent]'
GO
DROP TABLE [dbo].[cmsContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoContent]'
GO
CREATE TABLE [dbo].[umbracoContent]
(
[nodeId] [int] NOT NULL,
[contentTypeId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoContent] on [dbo].[umbracoContent]'
GO
ALTER TABLE [dbo].[umbracoContent] ADD CONSTRAINT [PK_umbracoContent] PRIMARY KEY CLUSTERED ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsContentNu]'
GO
CREATE TABLE [dbo].[cmsContentNu]
(
[nodeId] [int] NOT NULL,
[published] [bit] NOT NULL,
[data] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rv] [bigint] NOT NULL,
[dataRaw] [varbinary] (max) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsContentNu] on [dbo].[cmsContentNu]'
GO
ALTER TABLE [dbo].[cmsContentNu] ADD CONSTRAINT [PK_cmsContentNu] PRIMARY KEY CLUSTERED ([nodeId], [published])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsContentType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsContentType] ADD
[variations] [int] NOT NULL CONSTRAINT [DF_cmsContentType_variations] DEFAULT ('1'),
[isElement] [bit] NOT NULL CONSTRAINT [DF_cmsContentType_isElement] DEFAULT ('0')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsTemplate]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTemplate] DROP
COLUMN [design]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoLanguage]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoLanguage] ADD
[isDefaultVariantLang] [bit] NOT NULL CONSTRAINT [DF_umbracoLanguage_isDefaultVariantLang] DEFAULT ('0'),
[mandatory] [bit] NOT NULL CONSTRAINT [DF_umbracoLanguage_mandatory] DEFAULT ('0'),
[fallbackLanguageId] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoLanguage_fallbackLanguageId] on [dbo].[umbracoLanguage]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoLanguage_fallbackLanguageId] ON [dbo].[umbracoLanguage] ([fallbackLanguageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[cmsMacro]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_cmsMacro]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[macroUseInEditor] [bit] NOT NULL CONSTRAINT [DF__cmsMacro__macroU__0C50D423] DEFAULT ('0'),
[macroRefreshRate] [int] NOT NULL CONSTRAINT [DF__cmsMacro__macroR__0D44F85C] DEFAULT ('0'),
[macroAlias] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[macroName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[macroCacheByPage] [bit] NOT NULL CONSTRAINT [DF__cmsMacro__macroC__0E391C95] DEFAULT ('1'),
[macroCachePersonalized] [bit] NOT NULL CONSTRAINT [DF__cmsMacro__macroC__0F2D40CE] DEFAULT ('0'),
[macroDontRender] [bit] NOT NULL CONSTRAINT [DF__cmsMacro__macroD__10216507] DEFAULT ('0'),
[uniqueId] [uniqueidentifier] NOT NULL,
[macroType] [int] NOT NULL,
[macroSource] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_cmsMacro] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_1_cmsMacro]([id], [macroUseInEditor], [macroRefreshRate], [macroAlias], [macroName], [macroCacheByPage], [macroCachePersonalized], [macroDontRender], [uniqueId]) SELECT [id], [macroUseInEditor], [macroRefreshRate], [macroAlias], [macroName], [macroCacheByPage], [macroCachePersonalized], [macroDontRender], [uniqueId] FROM [dbo].[cmsMacro]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_cmsMacro] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[cmsMacro]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_cmsMacro]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[cmsMacro]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_cmsMacro]', N'cmsMacro', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsMacro] on [dbo].[cmsMacro]'
GO
ALTER TABLE [dbo].[cmsMacro] ADD CONSTRAINT [PK_cmsMacro] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsMacroPropertyAlias] on [dbo].[cmsMacro]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsMacroPropertyAlias] ON [dbo].[cmsMacro] ([macroAlias])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsMacro_UniqueId] on [dbo].[cmsMacro]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsMacro_UniqueId] ON [dbo].[cmsMacro] ([uniqueId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsMember]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsMember] ADD
[securityStampToken] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[emailConfirmedDate] [datetime] NULL,
[passwordConfig] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[failedPasswordAttempts] [int] NULL,
[isLockedOut] [bit] NULL CONSTRAINT [DF_cmsMember_isLockedOut] DEFAULT ('0'),
[isApproved] [bit] NOT NULL CONSTRAINT [DF_cmsMember_isApproved] DEFAULT ('1'),
[lastLoginDate] [datetime] NULL,
[lastLockoutDate] [datetime] NULL,
[lastPasswordChangeDate] [datetime] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsPropertyType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD
[variations] [int] NOT NULL CONSTRAINT [DF_cmsPropertyType_variations] DEFAULT ('1'),
[mandatoryMessage] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[validationRegExpMessage] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[labelOnTop] [bit] NOT NULL CONSTRAINT [DF_cmsPropertyType_labelOnTop] DEFAULT ('0')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[cmsPropertyTypeGroup]'
GO
CREATE TABLE [dbo].[RG_Recovery_2_cmsPropertyTypeGroup]
(
[id] [int] NOT NULL IDENTITY(27, 1),
[contenttypeNodeId] [int] NOT NULL,
[text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[sortorder] [int] NOT NULL,
[uniqueID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_cmsPropertyTypeGroup_uniqueID] DEFAULT (newid()),
[type] [int] NOT NULL CONSTRAINT [DF_cmsPropertyTypeGroup_type] DEFAULT ('0'),
[alias] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_2_cmsPropertyTypeGroup] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_2_cmsPropertyTypeGroup]([id], [contenttypeNodeId], [text], [sortorder], [uniqueID]) SELECT [id], [contenttypeNodeId], [text], [sortorder], [uniqueID] FROM [dbo].[cmsPropertyTypeGroup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_2_cmsPropertyTypeGroup] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[cmsPropertyTypeGroup]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_2_cmsPropertyTypeGroup]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[cmsPropertyTypeGroup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_2_cmsPropertyTypeGroup]', N'cmsPropertyTypeGroup', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsPropertyTypeGroup] on [dbo].[cmsPropertyTypeGroup]'
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] ADD CONSTRAINT [PK_cmsPropertyTypeGroup] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsPropertyTypeGroupUniqueID] on [dbo].[cmsPropertyTypeGroup]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsPropertyTypeGroupUniqueID] ON [dbo].[cmsPropertyTypeGroup] ([uniqueID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoDataType]'
GO
CREATE TABLE [dbo].[umbracoDataType]
(
[nodeId] [int] NOT NULL,
[propertyEditorAlias] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[dbType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[config] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoDataType] on [dbo].[umbracoDataType]'
GO
ALTER TABLE [dbo].[umbracoDataType] ADD CONSTRAINT [PK_umbracoDataType] PRIMARY KEY CLUSTERED ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsTags]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[cmsTags].[ParentId]', N'languageId', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTags] ALTER COLUMN [tag] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTags] ALTER COLUMN [group] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsTags] on [dbo].[cmsTags]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsTags] ON [dbo].[cmsTags] ([group], [tag], [languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsTags_LanguageId] on [dbo].[cmsTags]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsTags_LanguageId] ON [dbo].[cmsTags] ([languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoUserGroup]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD
[hasAccessToAllLanguages] [bit] NOT NULL CONSTRAINT [DF_umbracoUserGroup_hasAccessToAllLanguages] DEFAULT ('True')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UFRecords]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[UFRecords] ADD
[Culture] [nvarchar] (84) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UFRecordAudit]'
GO
CREATE TABLE [dbo].[UFRecordAudit]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Record] [int] NOT NULL,
[UpdatedOn] [datetime] NOT NULL,
[UpdatedBy] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_UFRecordAudit] on [dbo].[UFRecordAudit]'
GO
ALTER TABLE [dbo].[UFRecordAudit] ADD CONSTRAINT [PK_UFRecordAudit] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Record_RecordAudit] on [dbo].[UFRecordAudit]'
GO
CREATE NONCLUSTERED INDEX [IX_Record_RecordAudit] ON [dbo].[UFRecordAudit] ([Record])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Record_UpdatedBy] on [dbo].[UFRecordAudit]'
GO
CREATE NONCLUSTERED INDEX [IX_Record_UpdatedBy] ON [dbo].[UFRecordAudit] ([UpdatedBy])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoContentSchedule]'
GO
CREATE TABLE [dbo].[umbracoContentSchedule]
(
[id] [uniqueidentifier] NOT NULL,
[nodeId] [int] NOT NULL,
[languageId] [int] NULL,
[date] [datetime] NOT NULL,
[action] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoContentSchedule] on [dbo].[umbracoContentSchedule]'
GO
ALTER TABLE [dbo].[umbracoContentSchedule] ADD CONSTRAINT [PK_umbracoContentSchedule] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoContentVersion]'
GO
CREATE TABLE [dbo].[umbracoContentVersion]
(
[id] [int] NOT NULL IDENTITY(546, 1),
[VersionDate] [datetime] NOT NULL CONSTRAINT [DF__cmsConten__Versi__74794A92] DEFAULT (getdate()),
[text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[current] [bit] NOT NULL,
[userId] [int] NULL,
[nodeId] [int] NOT NULL,
[preventCleanup] [bit] NOT NULL CONSTRAINT [DF_umbracoContentVersion_preventCleanup] DEFAULT ('0')
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoContentVersion] on [dbo].[umbracoContentVersion]'
GO
ALTER TABLE [dbo].[umbracoContentVersion] ADD CONSTRAINT [PK_umbracoContentVersion] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoContentVersion_Current] on [dbo].[umbracoContentVersion]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoContentVersion_Current] ON [dbo].[umbracoContentVersion] ([current]) INCLUDE ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoContentVersion_NodeId] on [dbo].[umbracoContentVersion]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoContentVersion_NodeId] ON [dbo].[umbracoContentVersion] ([nodeId], [current]) INCLUDE ([id], [VersionDate], [text], [userId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoContentVersionCleanupPolicy]'
GO
CREATE TABLE [dbo].[umbracoContentVersionCleanupPolicy]
(
[contentTypeId] [int] NOT NULL,
[preventCleanup] [bit] NOT NULL,
[keepAllVersionsNewerThanDays] [int] NULL,
[keepLatestVersionPerDayForDays] [int] NULL,
[updated] [datetime] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoContentVersionCleanupPolicy] on [dbo].[umbracoContentVersionCleanupPolicy]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCleanupPolicy] ADD CONSTRAINT [PK_umbracoContentVersionCleanupPolicy] PRIMARY KEY CLUSTERED ([contentTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoContentVersionCultureVariation]'
GO
CREATE TABLE [dbo].[umbracoContentVersionCultureVariation]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[versionId] [int] NOT NULL,
[languageId] [int] NOT NULL,
[name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[date] [datetime] NOT NULL,
[availableUserId] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoContentVersionCultureVariation] on [dbo].[umbracoContentVersionCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] ADD CONSTRAINT [PK_umbracoContentVersionCultureVariation] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoContentVersionCultureVariation_LanguageId] on [dbo].[umbracoContentVersionCultureVariation]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoContentVersionCultureVariation_LanguageId] ON [dbo].[umbracoContentVersionCultureVariation] ([languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoContentVersionCultureVariation_VersionId] on [dbo].[umbracoContentVersionCultureVariation]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoContentVersionCultureVariation_VersionId] ON [dbo].[umbracoContentVersionCultureVariation] ([versionId], [languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoDocument]'
GO
CREATE TABLE [dbo].[umbracoDocument]
(
[nodeId] [int] NOT NULL,
[published] [bit] NOT NULL,
[edited] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoDocument] on [dbo].[umbracoDocument]'
GO
ALTER TABLE [dbo].[umbracoDocument] ADD CONSTRAINT [PK_umbracoDocument] PRIMARY KEY CLUSTERED ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoDocument_Published] on [dbo].[umbracoDocument]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoDocument_Published] ON [dbo].[umbracoDocument] ([published])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoDocumentCultureVariation]'
GO
CREATE TABLE [dbo].[umbracoDocumentCultureVariation]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[nodeId] [int] NOT NULL,
[languageId] [int] NOT NULL,
[edited] [bit] NOT NULL,
[available] [bit] NOT NULL,
[published] [bit] NOT NULL,
[name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoDocumentCultureVariation] on [dbo].[umbracoDocumentCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] ADD CONSTRAINT [PK_umbracoDocumentCultureVariation] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoDocumentCultureVariation_LanguageId] on [dbo].[umbracoDocumentCultureVariation]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoDocumentCultureVariation_LanguageId] ON [dbo].[umbracoDocumentCultureVariation] ([languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoDocumentCultureVariation_NodeId] on [dbo].[umbracoDocumentCultureVariation]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoDocumentCultureVariation_NodeId] ON [dbo].[umbracoDocumentCultureVariation] ([nodeId], [languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoDocumentVersion]'
GO
CREATE TABLE [dbo].[umbracoDocumentVersion]
(
[id] [int] NOT NULL,
[templateId] [int] NULL,
[published] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoDocumentVersion] on [dbo].[umbracoDocumentVersion]'
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] ADD CONSTRAINT [PK_umbracoDocumentVersion] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoDomain]'
GO
CREATE TABLE [dbo].[umbracoDomain]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[domainDefaultLanguage] [int] NULL,
[domainRootStructureID] [int] NULL,
[domainName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoDomain] on [dbo].[umbracoDomain]'
GO
ALTER TABLE [dbo].[umbracoDomain] ADD CONSTRAINT [PK_umbracoDomain] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[umbracoExternalLogin]'
GO
CREATE TABLE [dbo].[RG_Recovery_3_umbracoExternalLogin]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[loginProvider] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[providerKey] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[createDate] [datetime] NOT NULL CONSTRAINT [DF_umbracoExternalLogin_createDate] DEFAULT (getdate()),
[userData] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[userOrMemberKey] [uniqueidentifier] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_3_umbracoExternalLogin] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_3_umbracoExternalLogin]([id], [loginProvider], [providerKey], [createDate]) SELECT [id], [loginProvider], [providerKey], [createDate] FROM [dbo].[umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_3_umbracoExternalLogin] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[umbracoExternalLogin]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_3_umbracoExternalLogin]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_3_umbracoExternalLogin]', N'umbracoExternalLogin', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoExternalLogin] on [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] ADD CONSTRAINT [PK_umbracoExternalLogin] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoExternalLogin_ProviderKey] on [dbo].[umbracoExternalLogin]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoExternalLogin_ProviderKey] ON [dbo].[umbracoExternalLogin] ([loginProvider], [providerKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoExternalLogin_LoginProvider] on [dbo].[umbracoExternalLogin]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoExternalLogin_LoginProvider] ON [dbo].[umbracoExternalLogin] ([loginProvider], [userOrMemberKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoExternalLoginToken]'
GO
CREATE TABLE [dbo].[umbracoExternalLoginToken]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[externalLoginId] [int] NOT NULL,
[name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[createDate] [datetime] NOT NULL CONSTRAINT [DF_umbracoExternalLoginToken_createDate] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoExternalLoginToken] on [dbo].[umbracoExternalLoginToken]'
GO
ALTER TABLE [dbo].[umbracoExternalLoginToken] ADD CONSTRAINT [PK_umbracoExternalLoginToken] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoExternalLoginToken_Name] on [dbo].[umbracoExternalLoginToken]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoExternalLoginToken_Name] ON [dbo].[umbracoExternalLoginToken] ([externalLoginId], [name])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoLog] ADD
[entityType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[parameters] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoLog] ALTER COLUMN [userId] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoMediaVersion]'
GO
CREATE TABLE [dbo].[umbracoMediaVersion]
(
[id] [int] NOT NULL,
[path] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoMediaVersion] on [dbo].[umbracoMediaVersion]'
GO
ALTER TABLE [dbo].[umbracoMediaVersion] ADD CONSTRAINT [PK_umbracoMediaVersion] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoMediaVersion] on [dbo].[umbracoMediaVersion]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoMediaVersion] ON [dbo].[umbracoMediaVersion] ([id], [path])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoPropertyData]'
GO
CREATE TABLE [dbo].[umbracoPropertyData]
(
[id] [int] NOT NULL IDENTITY(2021, 1),
[propertytypeid] [int] NOT NULL,
[versionId] [int] NULL,
[languageId] [int] NULL,
[segment] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[textValue] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[varcharValue] [nvarchar] (512) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[decimalValue] [decimal] (38, 6) NULL,
[intValue] [int] NULL,
[dateValue] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoPropertyData] on [dbo].[umbracoPropertyData]'
GO
ALTER TABLE [dbo].[umbracoPropertyData] ADD CONSTRAINT [PK_umbracoPropertyData] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoPropertyData_LanguageId] on [dbo].[umbracoPropertyData]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoPropertyData_LanguageId] ON [dbo].[umbracoPropertyData] ([languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoPropertyData_PropertyTypeId] on [dbo].[umbracoPropertyData]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoPropertyData_PropertyTypeId] ON [dbo].[umbracoPropertyData] ([propertytypeid])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoPropertyData_Segment] on [dbo].[umbracoPropertyData]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoPropertyData_Segment] ON [dbo].[umbracoPropertyData] ([segment])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoPropertyData_VersionId] on [dbo].[umbracoPropertyData]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoPropertyData_VersionId] ON [dbo].[umbracoPropertyData] ([versionId], [propertytypeid], [languageId], [segment])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoRedirectUrl]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRedirectUrl] ADD
[culture] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoRedirectUrl] on [dbo].[umbracoRedirectUrl]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoRedirectUrl] ON [dbo].[umbracoRedirectUrl] ([urlHash], [contentKey], [culture], [createDateUtc])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoRelationType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ADD
[isDependency] [bit] NOT NULL CONSTRAINT [DF_umbracoRelationType_isDependency] DEFAULT ('0')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ALTER COLUMN [parentObjectType] [uniqueidentifier] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ALTER COLUMN [childObjectType] [uniqueidentifier] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ALTER COLUMN [alias] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoRelationType_alias] on [dbo].[umbracoRelationType]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoRelationType_alias] ON [dbo].[umbracoRelationType] ([alias])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoRelationType_name] on [dbo].[umbracoRelationType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoRelationType_name] ON [dbo].[umbracoRelationType] ([name])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoUserGroup2Language]'
GO
CREATE TABLE [dbo].[umbracoUserGroup2Language]
(
[userGroupId] [int] NOT NULL,
[languageId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_userGroup2language] on [dbo].[umbracoUserGroup2Language]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Language] ADD CONSTRAINT [PK_userGroup2language] PRIMARY KEY CLUSTERED ([userGroupId], [languageId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoUserGroup2Node]'
GO
CREATE TABLE [dbo].[umbracoUserGroup2Node]
(
[userGroupId] [int] NOT NULL,
[nodeId] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoUserGroup2Node] on [dbo].[umbracoUserGroup2Node]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Node] ADD CONSTRAINT [PK_umbracoUserGroup2Node] PRIMARY KEY CLUSTERED ([userGroupId], [nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoUserGroup2Node_nodeId] on [dbo].[umbracoUserGroup2Node]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoUserGroup2Node_nodeId] ON [dbo].[umbracoUserGroup2Node] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CMSImportState]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CMSImportState] ALTER COLUMN [Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CMSImportState] ALTER COLUMN [ImportProvider] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_CMSImportState] on [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] ADD CONSTRAINT [PK_CMSImportState] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CMSImportRelation]'
GO
CREATE TABLE [dbo].[RG_Recovery_4_CMSImportRelation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UmbracoID] [uniqueidentifier] NOT NULL,
[StateId] [uniqueidentifier] NOT NULL,
[ParentStateId] [uniqueidentifier] NOT NULL,
[DefinitionAlias] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DatasourceKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImportProvider] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Updated] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CMSImportRelation]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_4_CMSImportRelation]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CMSImportRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_4_CMSImportRelation]', N'CMSImportRelation', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_CMSImportRelation] on [dbo].[CMSImportRelation]'
GO
ALTER TABLE [dbo].[CMSImportRelation] ADD CONSTRAINT [PK_CMSImportRelation] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CMSImportMediaRelation]'
GO
CREATE TABLE [dbo].[RG_Recovery_5_CMSImportMediaRelation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UmbracoMediaId] [uniqueidentifier] NOT NULL,
[SourceUrl] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ByteSize] [bigint] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CMSImportMediaRelation]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_5_CMSImportMediaRelation]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CMSImportMediaRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_5_CMSImportMediaRelation]', N'CMSImportMediaRelation', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_CMSImportMediaRelation] on [dbo].[CMSImportMediaRelation]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation] ADD CONSTRAINT [PK_CMSImportMediaRelation] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MoveDevDataToStaging]'
GO

IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UFUserSecurity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[UFUserSecurity] ADD
[ViewEntries] [bit] NOT NULL CONSTRAINT [DF_UFUserSecurity_ViewEntries] DEFAULT ('0'),
[EditEntries] [bit] NOT NULL CONSTRAINT [DF_UFUserSecurity_EditEntries] DEFAULT ('0')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_UFUserSecurity] on [dbo].[UFUserSecurity]'
GO
ALTER TABLE [dbo].[UFUserSecurity] ADD CONSTRAINT [PK_UFUserSecurity] PRIMARY KEY CLUSTERED ([User])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoServer]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[umbracoServer].[isMaster]', N'isSchedulingPublisher', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportMediaRelation_old]'
GO
CREATE TABLE [dbo].[CMSImportMediaRelation_old]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UmbracoMediaId] [int] NOT NULL,
[SourceUrl] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ByteSize] [int] NULL CONSTRAINT [DF__CMSImport__ByteS__7EF6D905] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportMediaRelation__000000000000043D] on [dbo].[CMSImportMediaRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation_old] ADD CONSTRAINT [PK__CMSImportMediaRelation__000000000000043D] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportRelation_old]'
GO
CREATE TABLE [dbo].[CMSImportRelation_old]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UmbracoID] [int] NOT NULL,
[DataSourceKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImportProvider] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__CMSImport__Impor__00DF2177] DEFAULT (NULL),
[Updated] [datetime] NULL CONSTRAINT [DF__CMSImport__Updat__01D345B0] DEFAULT (NULL),
[CustomId] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportRelation__000000000000042C] on [dbo].[CMSImportRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportRelation_old] ADD CONSTRAINT [PK__CMSImportRelation__000000000000042C] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportScheduledItems_old]'
GO
CREATE TABLE [dbo].[CMSImportScheduledItems_old]
(
[ScheduledItemId] [int] NOT NULL IDENTITY(1, 1),
[ScheduleldTaskId] [int] NOT NULL,
[ScheduledOn] [datetime] NOT NULL,
[ExecutedOn] [datetime] NULL CONSTRAINT [DF__CMSImport__Execu__03BB8E22] DEFAULT (NULL),
[InProgress] [bit] NULL CONSTRAINT [DF__CMSImport__InPro__04AFB25B] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportScheduledItems__0000000000000453] on [dbo].[CMSImportScheduledItems_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems_old] ADD CONSTRAINT [PK__CMSImportScheduledItems__0000000000000453] PRIMARY KEY CLUSTERED ([ScheduledItemId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportScheduledTaskDefinition]'
GO
CREATE TABLE [dbo].[CMSImportScheduledTaskDefinition]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserId] [int] NOT NULL,
[StateId] [uniqueidentifier] NOT NULL,
[ScheduledTaskId] [uniqueidentifier] NOT NULL,
[IsInterval] [bit] NOT NULL,
[ScheduledTaskName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NotificationAddress] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Interval] [int] NOT NULL,
[Days] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Hour] [int] NOT NULL,
[Minute] [int] NOT NULL,
[LastTimeExecuted] [datetime] NULL,
[NextRunTime] [datetime] NOT NULL,
[InProgress] [bit] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_CMSImportScheduledTaskDefinition] on [dbo].[CMSImportScheduledTaskDefinition]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTaskDefinition] ADD CONSTRAINT [PK_CMSImportScheduledTaskDefinition] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportScheduledTaskResult]'
GO
CREATE TABLE [dbo].[CMSImportScheduledTaskResult]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ScheduledTaskId] [uniqueidentifier] NOT NULL,
[Success] [bit] NOT NULL,
[Executed] [datetime] NOT NULL,
[Duration] [int] NOT NULL,
[Errors] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_CMSImportScheduledTaskResult] on [dbo].[CMSImportScheduledTaskResult]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTaskResult] ADD CONSTRAINT [PK_CMSImportScheduledTaskResult] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportScheduledTask_old]'
GO
CREATE TABLE [dbo].[CMSImportScheduledTask_old]
(
[ScheduleId] [int] NOT NULL IDENTITY(1, 1),
[ScheduleGUID] [uniqueidentifier] NOT NULL,
[ImportStateGUID] [uniqueidentifier] NOT NULL,
[ScheduledTaskName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NotifyEmailAddress] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecuteEvery] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecuteDays] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecuteHour] [int] NOT NULL,
[ExecuteMinute] [int] NOT NULL,
[ImportAsUser] [int] NULL CONSTRAINT [DF__CMSImport__Impor__0697FACD] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportScheduledTask__0000000000000416] on [dbo].[CMSImportScheduledTask_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTask_old] ADD CONSTRAINT [PK__CMSImportScheduledTask__0000000000000416] PRIMARY KEY CLUSTERED ([ScheduleId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportState_old]'
GO
CREATE TABLE [dbo].[CMSImportState_old]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UniqueIdentifier] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImportState] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Parent] [uniqueidentifier] NULL CONSTRAINT [DF__CMSImport__Paren__0880433F] DEFAULT (NULL),
[ImportProvider] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__CMSImport__Impor__09746778] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportState__00000000000003F9] on [dbo].[CMSImportState_old]'
GO
ALTER TABLE [dbo].[CMSImportState_old] ADD CONSTRAINT [PK__CMSImportState__00000000000003F9] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UFRecordWorkflowAudit]'
GO
CREATE TABLE [dbo].[UFRecordWorkflowAudit]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[RecordUniqueId] [uniqueidentifier] NOT NULL,
[WorkflowKey] [uniqueidentifier] NOT NULL,
[WorkflowName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WorkflowTypeId] [uniqueidentifier] NOT NULL,
[WorkflowTypeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecutedOn] [datetime] NOT NULL,
[ExecutionStage] [int] NULL,
[ExecutionStatus] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_UFRecordWorkflowAudit] on [dbo].[UFRecordWorkflowAudit]'
GO
ALTER TABLE [dbo].[UFRecordWorkflowAudit] ADD CONSTRAINT [PK_UFRecordWorkflowAudit] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RecordUniqueId_RecordWorkflowAudit] on [dbo].[UFRecordWorkflowAudit]'
GO
CREATE NONCLUSTERED INDEX [IX_RecordUniqueId_RecordWorkflowAudit] ON [dbo].[UFRecordWorkflowAudit] ([RecordUniqueId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_WorkflowKey_RecordWorkflowAudit] on [dbo].[UFRecordWorkflowAudit]'
GO
CREATE NONCLUSTERED INDEX [IX_WorkflowKey_RecordWorkflowAudit] ON [dbo].[UFRecordWorkflowAudit] ([WorkflowKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UFUserGroupFormSecurity]'
GO
CREATE TABLE [dbo].[UFUserGroupFormSecurity]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UserGroupId] [int] NOT NULL,
[Form] [uniqueidentifier] NOT NULL,
[HasAccess] [bit] NOT NULL,
[AllowInEditor] [bit] NOT NULL,
[SecurityType] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_UserGroupFormSecurity] on [dbo].[UFUserGroupFormSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupFormSecurity] ADD CONSTRAINT [PK_UserGroupFormSecurity] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[UFUserGroupFormSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupFormSecurity] ADD CONSTRAINT [UK_UFUserGroupFormSecurity_UserGroupId_Form] UNIQUE NONCLUSTERED ([UserGroupId], [Form])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UFUserGroupSecurity]'
GO
CREATE TABLE [dbo].[UFUserGroupSecurity]
(
[UserGroupId] [int] NOT NULL,
[ManageDataSources] [bit] NOT NULL,
[ManagePreValueSources] [bit] NOT NULL,
[ManageWorkflows] [bit] NOT NULL,
[ManageForms] [bit] NOT NULL,
[ViewEntries] [bit] NOT NULL CONSTRAINT [DF_UFUserGroupSecurity_ViewEntries] DEFAULT ('0'),
[EditEntries] [bit] NOT NULL CONSTRAINT [DF_UFUserGroupSecurity_EditEntries] DEFAULT ('0')
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_UFUserGroupSecurity] on [dbo].[UFUserGroupSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupSecurity] ADD CONSTRAINT [PK_UFUserGroupSecurity] PRIMARY KEY CLUSTERED ([UserGroupId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoCreatedPackageSchema]'
GO
CREATE TABLE [dbo].[umbracoCreatedPackageSchema]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[updateDate] [datetime] NOT NULL CONSTRAINT [DF_umbracoCreatedPackageSchema_updateDate] DEFAULT (getdate()),
[packageId] [uniqueidentifier] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoCreatedPackageSchema] on [dbo].[umbracoCreatedPackageSchema]'
GO
ALTER TABLE [dbo].[umbracoCreatedPackageSchema] ADD CONSTRAINT [PK_umbracoCreatedPackageSchema] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoCreatedPackageSchema_Name] on [dbo].[umbracoCreatedPackageSchema]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoCreatedPackageSchema_Name] ON [dbo].[umbracoCreatedPackageSchema] ([name])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoKeyValue]'
GO
CREATE TABLE [dbo].[umbracoKeyValue]
(
[key] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[updated] [datetime] NOT NULL CONSTRAINT [DF_umbracoKeyValue_updated] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoKeyValue] on [dbo].[umbracoKeyValue]'
GO
ALTER TABLE [dbo].[umbracoKeyValue] ADD CONSTRAINT [PK_umbracoKeyValue] PRIMARY KEY CLUSTERED ([key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoLogViewerQuery]'
GO
CREATE TABLE [dbo].[umbracoLogViewerQuery]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[query] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoLogViewerQuery] on [dbo].[umbracoLogViewerQuery]'
GO
ALTER TABLE [dbo].[umbracoLogViewerQuery] ADD CONSTRAINT [PK_umbracoLogViewerQuery] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LogViewerQuery_name] on [dbo].[umbracoLogViewerQuery]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_LogViewerQuery_name] ON [dbo].[umbracoLogViewerQuery] ([name])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoTwoFactorLogin]'
GO
CREATE TABLE [dbo].[umbracoTwoFactorLogin]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[userOrMemberKey] [uniqueidentifier] NOT NULL,
[providerName] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[secret] [nvarchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoTwoFactorLogin] on [dbo].[umbracoTwoFactorLogin]'
GO
ALTER TABLE [dbo].[umbracoTwoFactorLogin] ADD CONSTRAINT [PK_umbracoTwoFactorLogin] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoTwoFactorLogin_ProviderName] on [dbo].[umbracoTwoFactorLogin]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoTwoFactorLogin_ProviderName] ON [dbo].[umbracoTwoFactorLogin] ([providerName], [userOrMemberKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoTwoFactorLogin_userOrMemberKey] on [dbo].[umbracoTwoFactorLogin]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoTwoFactorLogin_userOrMemberKey] ON [dbo].[umbracoTwoFactorLogin] ([userOrMemberKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoNode] on [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] ADD CONSTRAINT [PK_umbracoNode] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_UFRecordDataBit_Key] on [dbo].[UFRecordDataBit]'
GO
CREATE NONCLUSTERED INDEX [IX_UFRecordDataBit_Key] ON [dbo].[UFRecordDataBit] ([Key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_UFRecordDataDateTime_Key] on [dbo].[UFRecordDataDateTime]'
GO
CREATE NONCLUSTERED INDEX [IX_UFRecordDataDateTime_Key] ON [dbo].[UFRecordDataDateTime] ([Key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_UFRecordDataInteger_Key] on [dbo].[UFRecordDataInteger]'
GO
CREATE NONCLUSTERED INDEX [IX_UFRecordDataInteger_Key] ON [dbo].[UFRecordDataInteger] ([Key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_UFRecordDataLongString_Key] on [dbo].[UFRecordDataLongString]'
GO
CREATE NONCLUSTERED INDEX [IX_UFRecordDataLongString_Key] ON [dbo].[UFRecordDataLongString] ([Key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[UFUserFormSecurity]'
GO
ALTER TABLE [dbo].[UFUserFormSecurity] ADD CONSTRAINT [UK_UFUserFormSecurity_User_Form] UNIQUE NONCLUSTERED ([User], [Form])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDictionary_key] on [dbo].[cmsDictionary]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsDictionary_key] ON [dbo].[cmsDictionary] ([key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDictionary_Parent] on [dbo].[cmsDictionary]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsDictionary_Parent] ON [dbo].[cmsDictionary] ([parent])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsLanguageText_languageId] on [dbo].[cmsLanguageText]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsLanguageText_languageId] ON [dbo].[cmsLanguageText] ([languageId], [UniqueId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoAccessRule] on [dbo].[umbracoAccessRule]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoAccessRule] ON [dbo].[umbracoAccessRule] ([ruleValue], [ruleType], [accessId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_Level] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNode_Level] ON [dbo].[umbracoNode] ([level], [parentID], [sortOrder], [nodeObjectType], [trashed]) INCLUDE ([nodeUser], [path], [uniqueID], [createDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_ObjectType] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNode_ObjectType] ON [dbo].[umbracoNode] ([nodeObjectType], [trashed]) INCLUDE ([uniqueID], [parentID], [level], [path], [sortOrder], [nodeUser], [text], [createDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_ParentId] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNode_ParentId] ON [dbo].[umbracoNode] ([parentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_Path] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNode_Path] ON [dbo].[umbracoNode] ([path])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_Trashed] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNode_Trashed] ON [dbo].[umbracoNode] ([trashed])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_UniqueId] on [dbo].[umbracoNode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoNode_UniqueId] ON [dbo].[umbracoNode] ([uniqueID]) INCLUDE ([parentID], [level], [path], [sortOrder], [trashed], [nodeUser], [text], [createDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoUserLogin_lastValidatedUtc] on [dbo].[umbracoUserLogin]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoUserLogin_lastValidatedUtc] ON [dbo].[umbracoUserLogin] ([lastValidatedUtc])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContentNu]'
GO
ALTER TABLE [dbo].[cmsContentNu] WITH NOCHECK  ADD CONSTRAINT [FK_cmsContentNu_umbracoContent_nodeId] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoContent] ([nodeId]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoContentVersionCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoContentVersion_id] FOREIGN KEY ([versionId]) REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoUser_id] FOREIGN KEY ([availableUserId]) REFERENCES [dbo].[umbracoUser] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoDocumentCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserGroup2Node]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Node] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoUserGroup2Node_umbracoUserGroup_id] FOREIGN KEY ([userGroupId]) REFERENCES [dbo].[umbracoUserGroup] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup2Node] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoUserGroup2Node_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMember2MemberGroup]'
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] WITH NOCHECK  ADD CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id] FOREIGN KEY ([MemberGroup]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoRelation]'
GO
ALTER TABLE [dbo].[umbracoRelation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoRelation_umbracoNode] FOREIGN KEY ([parentId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoRelation] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoRelation_umbracoNode1] FOREIGN KEY ([childId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[UFRecordAudit]'
GO
ALTER TABLE [dbo].[UFRecordAudit] ADD CONSTRAINT [FK_UFRecordAudit_UFRecords_Id] FOREIGN KEY ([Record]) REFERENCES [dbo].[UFRecords] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[UFRecordFields]'
GO
ALTER TABLE [dbo].[UFRecordFields] ADD CONSTRAINT [FK_UFRecordFields_UFRecords_Record] FOREIGN KEY ([Record]) REFERENCES [dbo].[UFRecords] ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContentTypeAllowedContentType]'
GO
ALTER TABLE [dbo].[cmsContentTypeAllowedContentType] ADD CONSTRAINT [FK_cmsContentTypeAllowedContentType_cmsContentType] FOREIGN KEY ([Id]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
ALTER TABLE [dbo].[cmsContentTypeAllowedContentType] ADD CONSTRAINT [FK_cmsContentTypeAllowedContentType_cmsContentType1] FOREIGN KEY ([AllowedId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsDocumentType]'
GO
ALTER TABLE [dbo].[cmsDocumentType] ADD CONSTRAINT [FK_cmsDocumentType_cmsContentType_nodeId] FOREIGN KEY ([contentTypeNodeId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
ALTER TABLE [dbo].[cmsDocumentType] ADD CONSTRAINT [FK_cmsDocumentType_cmsTemplate_nodeId] FOREIGN KEY ([templateNodeId]) REFERENCES [dbo].[cmsTemplate] ([nodeId])
GO
ALTER TABLE [dbo].[cmsDocumentType] ADD CONSTRAINT [FK_cmsDocumentType_umbracoNode_id] FOREIGN KEY ([contentTypeNodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsLanguageText]'
GO
ALTER TABLE [dbo].[cmsLanguageText] ADD CONSTRAINT [FK_cmsLanguageText_cmsDictionary_id] FOREIGN KEY ([UniqueId]) REFERENCES [dbo].[cmsDictionary] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMemberType]'
GO
ALTER TABLE [dbo].[cmsMemberType] ADD CONSTRAINT [FK_cmsMemberType_cmsContentType_nodeId] FOREIGN KEY ([NodeId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
ALTER TABLE [dbo].[cmsMemberType] ADD CONSTRAINT [FK_cmsMemberType_umbracoNode_id] FOREIGN KEY ([NodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMember]'
GO
ALTER TABLE [dbo].[cmsMember] ADD CONSTRAINT [FK_cmsMember_umbracoContent_nodeId] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsPropertyTypeGroup]'
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] ADD CONSTRAINT [FK_cmsPropertyTypeGroup_cmsContentType_nodeId] FOREIGN KEY ([contenttypeNodeId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsPropertyType]'
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD CONSTRAINT [FK_cmsPropertyType_cmsPropertyTypeGroup_id] FOREIGN KEY ([propertyTypeGroupId]) REFERENCES [dbo].[cmsPropertyTypeGroup] ([id])
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD CONSTRAINT [FK_cmsPropertyType_cmsContentType_nodeId] FOREIGN KEY ([contentTypeId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
ALTER TABLE [dbo].[cmsPropertyType] ADD CONSTRAINT [FK_cmsPropertyType_umbracoDataType_nodeId] FOREIGN KEY ([dataTypeId]) REFERENCES [dbo].[umbracoDataType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsTagRelationship]'
GO
ALTER TABLE [dbo].[cmsTagRelationship] ADD CONSTRAINT [FK_cmsTagRelationship_cmsContent] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsTags]'
GO
ALTER TABLE [dbo].[cmsTags] ADD CONSTRAINT [FK_cmsTags_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoContentSchedule]'
GO
ALTER TABLE [dbo].[umbracoContentSchedule] ADD CONSTRAINT [FK_umbracoContentSchedule_umbracoContent_nodeId] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
ALTER TABLE [dbo].[umbracoContentSchedule] ADD CONSTRAINT [FK_umbracoContentSchedule_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoContentVersionCleanupPolicy]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCleanupPolicy] ADD CONSTRAINT [FK_umbracoContentVersionCleanupPolicy_cmsContentType_nodeId] FOREIGN KEY ([contentTypeId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoDocumentVersion]'
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] ADD CONSTRAINT [FK_umbracoDocumentVersion_umbracoContentVersion_id] FOREIGN KEY ([id]) REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] ADD CONSTRAINT [FK_umbracoDocumentVersion_cmsTemplate_nodeId] FOREIGN KEY ([templateId]) REFERENCES [dbo].[cmsTemplate] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoMediaVersion]'
GO
ALTER TABLE [dbo].[umbracoMediaVersion] ADD CONSTRAINT [FK_umbracoMediaVersion_umbracoContentVersion_id] FOREIGN KEY ([id]) REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoPropertyData]'
GO
ALTER TABLE [dbo].[umbracoPropertyData] ADD CONSTRAINT [FK_umbracoPropertyData_umbracoContentVersion_id] FOREIGN KEY ([versionId]) REFERENCES [dbo].[umbracoContentVersion] ([id])
GO
ALTER TABLE [dbo].[umbracoPropertyData] ADD CONSTRAINT [FK_umbracoPropertyData_cmsPropertyType_id] FOREIGN KEY ([propertytypeid]) REFERENCES [dbo].[cmsPropertyType] ([id])
GO
ALTER TABLE [dbo].[umbracoPropertyData] ADD CONSTRAINT [FK_umbracoPropertyData_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoContentVersion]'
GO
ALTER TABLE [dbo].[umbracoContentVersion] ADD CONSTRAINT [FK_umbracoContentVersion_umbracoUser_id] FOREIGN KEY ([userId]) REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoContentVersion] ADD CONSTRAINT [FK_umbracoContentVersion_umbracoContent_nodeId] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoDocument]'
GO
ALTER TABLE [dbo].[umbracoDocument] ADD CONSTRAINT [FK_umbracoDocument_umbracoContent_nodeId] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoContent] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoContent]'
GO
ALTER TABLE [dbo].[umbracoContent] ADD CONSTRAINT [FK_umbracoContent_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoContent] ADD CONSTRAINT [FK_umbracoContent_cmsContentType_NodeId] FOREIGN KEY ([contentTypeId]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoDataType]'
GO
ALTER TABLE [dbo].[umbracoDataType] ADD CONSTRAINT [FK_umbracoDataType_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoDomain]'
GO
ALTER TABLE [dbo].[umbracoDomain] ADD CONSTRAINT [FK_umbracoDomain_umbracoNode_id] FOREIGN KEY ([domainRootStructureID]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoExternalLoginToken]'
GO
ALTER TABLE [dbo].[umbracoExternalLoginToken] ADD CONSTRAINT [FK_umbracoExternalLoginToken_umbracoExternalLogin_id] FOREIGN KEY ([externalLoginId]) REFERENCES [dbo].[umbracoExternalLogin] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoLanguage]'
GO
ALTER TABLE [dbo].[umbracoLanguage] ADD CONSTRAINT [FK_umbracoLanguage_umbracoLanguage_id] FOREIGN KEY ([fallbackLanguageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoLog]'
GO
ALTER TABLE [dbo].[umbracoLog] ADD CONSTRAINT [FK_umbracoLog_umbracoUser_id] FOREIGN KEY ([userId]) REFERENCES [dbo].[umbracoUser] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] ADD CONSTRAINT [FK_umbracoNode_umbracoUser_id] FOREIGN KEY ([nodeUser]) REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[umbracoNode] ADD CONSTRAINT [FK_umbracoNode_umbracoNode_id] FOREIGN KEY ([parentID]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoRedirectUrl]'
GO
ALTER TABLE [dbo].[umbracoRedirectUrl] ADD CONSTRAINT [FK_umbracoRedirectUrl_umbracoNode_uniqueID] FOREIGN KEY ([contentKey]) REFERENCES [dbo].[umbracoNode] ([uniqueID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserGroup2Language]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Language] ADD CONSTRAINT [FK_umbracoUserGroup2Language_umbracoUserGroup_id] FOREIGN KEY ([userGroupId]) REFERENCES [dbo].[umbracoUserGroup] ([id]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[umbracoUserGroup2Language] ADD CONSTRAINT [FK_umbracoUserGroup2Language_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id]) ON DELETE CASCADE
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMacroProperty]'
GO
ALTER TABLE [dbo].[cmsMacroProperty] ADD CONSTRAINT [FK_cmsMacroProperty_cmsMacro_id] FOREIGN KEY ([macro]) REFERENCES [dbo].[cmsMacro] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoAccessRule]'
GO
ALTER TABLE [dbo].[umbracoAccessRule] ADD CONSTRAINT [FK_umbracoAccessRule_umbracoAccess_id] FOREIGN KEY ([accessId]) REFERENCES [dbo].[umbracoAccess] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContentType]'
GO
ALTER TABLE [dbo].[cmsContentType] ADD CONSTRAINT [FK_cmsContentType_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContentType2ContentType]'
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] ADD CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_child] FOREIGN KEY ([childContentTypeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] ADD CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_parent] FOREIGN KEY ([parentContentTypeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsTemplate]'
GO
ALTER TABLE [dbo].[cmsTemplate] ADD CONSTRAINT [FK_cmsTemplate_umbracoNode] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserGroup]'
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD CONSTRAINT [FK_startContentId_umbracoNode_id] FOREIGN KEY ([startContentId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD CONSTRAINT [FK_startMediaId_umbracoNode_id] FOREIGN KEY ([startMediaId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoAccess]'
GO
ALTER TABLE [dbo].[umbracoAccess] ADD CONSTRAINT [FK_umbracoAccess_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] ADD CONSTRAINT [FK_umbracoAccess_umbracoNode_id1] FOREIGN KEY ([loginNodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] ADD CONSTRAINT [FK_umbracoAccess_umbracoNode_id2] FOREIGN KEY ([noAccessNodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUser2NodeNotify]'
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify] ADD CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserGroup2NodePermission]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] ADD CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserStartNode]'
GO
ALTER TABLE [dbo].[umbracoUserStartNode] ADD CONSTRAINT [FK_umbracoUserStartNode_umbracoNode_id] FOREIGN KEY ([startNode]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[cmsMember2MemberGroup]'
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] NOCHECK CONSTRAINT [FK_cmsMember2MemberGroup_cmsMember_nodeId]
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] CHECK CONSTRAINT [FK_cmsMember2MemberGroup_cmsMember_nodeId]
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] NOCHECK CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id]
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] CHECK CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[umbracoRelation]'
GO
ALTER TABLE [dbo].[umbracoRelation] NOCHECK CONSTRAINT [FK_umbracoRelation_umbracoNode]
GO
ALTER TABLE [dbo].[umbracoRelation] CHECK CONSTRAINT [FK_umbracoRelation_umbracoNode]
GO
ALTER TABLE [dbo].[umbracoRelation] NOCHECK CONSTRAINT [FK_umbracoRelation_umbracoNode1]
GO
ALTER TABLE [dbo].[umbracoRelation] CHECK CONSTRAINT [FK_umbracoRelation_umbracoNode1]
GO
ALTER TABLE [dbo].[umbracoRelation] NOCHECK CONSTRAINT [FK_umbracoRelation_umbracoRelationType_id]
GO
ALTER TABLE [dbo].[umbracoRelation] CHECK CONSTRAINT [FK_umbracoRelation_umbracoRelationType_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[umbracoUserGroup2NodePermission]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] WITH CHECK CHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] WITH CHECK CHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoUserGroup_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
