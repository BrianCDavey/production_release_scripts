/*
Run this script on:

        AZ-SQ-PR-01.CmsImport    -  This database will be modified

to synchronize it with:

        (local)\CHISQZ01.CmsImport

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 1/8/2025 4:13:03 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Cities]'
GO
ALTER PROCEDURE [dbo].[mpid_Cities]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.PHG_city)
	BEGIN
		TRUNCATE TABLE dbo.Cities;

		DECLARE @CityData TABLE (TableName nvarchar(6),
								PHG_cityId uniqueidentifier,
								PHG_name nvarchar(100),
								PHG_Country nvarchar(100),
								PHG_StateOrProvince nvarchar(100),
								PHG_City nvarchar(100),
								isDisabled bit);

		INSERT INTO @CityData(TableName,PHG_cityId,PHG_name,PHG_Country,PHG_StateOrProvince,PHG_City,isDisabled)
		SELECT MIN(TableName) AS TableName,PHG_cityId,PHG_name,PHG_Country,PHG_StateOrProvince,PHG_City,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,PHG_cityId,PHG_name,phg_countryidname AS PHG_Country,phg_stateorprovincename AS PHG_StateOrProvince,PHG_name AS PHG_City,0 AS isDisabled
				FROM LocalCRM.dbo.PHG_city
					UNION
				SELECT 'TARGET' AS TableName,
						CAST(cmsContentXml.xml as xml).value('(/City/cityGuid)[1]','uniqueidentifier') AS PHG_cityId,
						CAST(cmsContentXml.xml as xml).value('(/City/@nodeName)[1]','nvarchar(MAX)') AS PHG_name,
						CAST(cmsContentXml.xml as xml).value('(/City/cityCountry)[1]','nvarchar(MAX)') AS PHG_Country,
						CAST(cmsContentXml.xml as xml).value('(/City/cityState)[1]','nvarchar(MAX)') AS PHG_StateOrProvince,
						CAST(cmsContentXml.xml as xml).value('(/City/cityCity)[1]','nvarchar(MAX)') AS PHG_City,
						CAST(cmsContentXml.xml as xml).value('(/City/disabled)[1]','BIT') AS isDisabled
				FROM MemberPortal.dbo.cmsContentXml
				WHERE CAST(cmsContentXml.xml as xml).value('(/City/@nodeType)[1]','nvarchar(MAX)') = 12052
			) unioned
		GROUP BY PHG_cityId,PHG_name,PHG_Country,PHG_StateOrProvince,PHG_City,isDisabled
		HAVING COUNT(*) = 1;


		INSERT INTO dbo.Cities(cityGuid,Name,cityCountry,cityState,cityCity,[disabled])
		SELECT PHG_cityId,PHG_name,PHG_Country,PHG_StateOrProvince,PHG_City,0
		FROM @CityData
		WHERE TableName = 'SOURCE';


		INSERT INTO dbo.Cities(cityGuid,Name,cityCountry,cityState,cityCity,[disabled])
		SELECT PHG_cityId,PHG_name,PHG_Country,PHG_StateOrProvince,PHG_City,1
		FROM @CityData T
			INNER JOIN MemberPortal.dbo.cmsContentXml ON CAST(cmsContentXml.xml as xml).value('(/City/@nodeType)[1]','nvarchar(MAX)') = 12052
														AND CAST(cmsContentXml.xml as xml).value('(/City/cityGuid)[1]','uniqueidentifier') = T.PHG_cityId
														AND CAST(cmsContentXml.xml as xml).value('(/City/disabled)[1]','BIT') = 0
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS(SELECT 1 FROM @CityData S WHERE S.TableName = 'SOURCE' AND T.PHG_cityId = S.PHG_cityId);
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_ConsortiaSales]'
GO

ALTER PROCEDURE [dbo].[mpid_ConsortiaSales]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.ConsortiaAccounts;
		DECLARE @ConsortiaData TABLE(TableName nvarchar(6),
										nodeName nvarchar(100),
										crmAccountName nvarchar(100),
										consortiaAccountCrmGUID uniqueidentifier,
										crmAccountStreetAddressLine1 nvarchar(100),
										crmAccountStreetAddressLine2 nvarchar(100),
										crmAccountCity nvarchar(100),
										crmAccountState nvarchar(100),
										crmAccountCountry nvarchar(100),
										crmAccountPostalCode nvarchar(100),
										crmAccountPhoneNumber nvarchar(100),
										crmAccountEmailAddress nvarchar(100),
										crmAccountWebSite nvarchar(100),
										crmAccountManagedAccount bit,
										consortiaAccountIataGlobalAccountManager nvarchar(100),
										accountIndustry nvarchar(100),
										isDisabled bit);
		INSERT INTO @ConsortiaData(TableName,nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
									crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
									consortiaAccountIataGlobalAccountManager,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,LTRIM(RTRIM(account.Name)) AS nodeName,LTRIM(RTRIM(account.Name)) AS crmAccountName,UPPER(account.AccountID) AS consortiaAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,ISNULL(Address1_City, '') AS crmAccountCity,
					ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,
					ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,ISNULL(WebSiteURL, '') AS crmAccountWebSite,
					CASE account.phg_consortiasalesstatus WHEN 100000002 THEN 1 ELSE 0 END AS crmAccountManagedAccount,
					'umb://document/' + REPLACE(CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS consortiaAccountIataGlobalAccountManager,
					'umb://document/' + REPLACE(CAST(umbracoIndustry.xml as xml).value('(/Industry/@key)[1]','nvarchar(MAX)'), '-', '') AS accountIndustry,
					0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.phg_consortiasalesstatus,ISNULL(phg_showinmemberportalconsortia, 0) AS PHG_ShowinMemberPortalConsortia,
								phg_iataglobalaccountmanagerid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_consortiasalesstatus IN (100000000,100000002)
							AND Account.phg_consortiasales = 1
							AND Account.phg_showinmemberportalconsortia = 1
					  ) account
					LEFT JOIN MemberPortal.dbo.cmsContentXml as umbracoIataGlobalAccountManager ON CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																										AND CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_iataglobalaccountmanagerid
					LEFT JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																						AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
					UNION
				SELECT 'TARGET' AS TableName,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/consortiaAccountCrmGuid)[1]','uniqueidentifier') AS consortiaAccountCrmGuid,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountManagedAccount)[1]','BIT') AS crmAccountManagedAccount,
					cmsContentXml.accountManager AS consortiaAccountIataGlobalAccountManager,
					cmsContentXml.accountIndustry AS accountIndustry,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/disabled)[1]','bit') AS isDisabled
				FROM (
						SELECT xml, CAST(cx.xml as xml).value('(/ConsortiaAccount/consortiaAccountIataGlobalAccountManager)[1]','nvarchar(100)') accountManager, CAST(cx.xml as xml).value('(/ConsortiaAccount/crmAccountIndustry)[1]','nvarchar(100)') accountIndustry
						FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
						WHERE ct.alias = 'ConsortiaAccount'
					) cmsContentXml
				WHERE CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/disabled)[1]','bit') =0
			  ) unioned
		GROUP BY nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,consortiaAccountIataGlobalAccountManager,
				accountIndustry,isDisabled
		HAVING COUNT(*) = 1;
		--SELECT * FROM @ConsortiaData ORDER BY nodeName, TableName
		INSERT INTO CmsImport.dbo.ConsortiaAccounts(Name,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
											crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
											consortiaAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,0,accountIndustry
		FROM @ConsortiaData
		WHERE TableName = 'SOURCE';
		INSERT INTO CmsImport.dbo.ConsortiaAccounts(Name,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
											crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
											consortiaAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,1,accountIndustry
		FROM @ConsortiaData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @ConsortiaData S WHERE S.TableName = 'SOURCE' AND T.consortiaAccountCrmGUID = S.consortiaAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_CorporateAccounts]'
GO

ALTER PROCEDURE [dbo].[mpid_CorporateAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.CorporateAccounts;

		DECLARE @CorporateAccountData TABLE(TableName nvarchar(6),
											nodeName nvarchar(100),
											crmAccountName nvarchar(100),
											corporateAccountCrmGUID uniqueidentifier,
											crmAccountStreetAddressLine1 nvarchar(100),
											crmAccountStreetAddressLine2 nvarchar(100),
											crmAccountCity nvarchar(100),
											crmAccountState nvarchar(100),
											crmAccountCountry nvarchar(100),
											crmAccountPostalCode nvarchar(100),
											crmAccountPhoneNumber nvarchar(100),
											crmAccountEmailAddress nvarchar(100),
											crmAccountWebSite nvarchar(200),
											crmAccountSalesStatus nvarchar(25),
											corporateAccountGlobalTeamLead nvarchar(100),
											corporateAccountRegionalTeamLeadAmericas nvarchar(100),
											corporateAccountRegionalTeamLeadASPAC nvarchar(100),
											corporateAccountRegionalTeamLeadEMEA nvarchar(100),
											accountIndustry nvarchar(100),
											isDisabled bit);

		INSERT INTO @CorporateAccountData(TableName,nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,
											crmAccountCity,crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
											crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,
											corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,
				crmAccountCity,crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,
				corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,account.Name AS nodeName,account.Name AS crmAccountName,UPPER(account.AccountID) AS corporateAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,
					ISNULL(Address1_City, '') AS crmAccountCity,ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,
					ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,
					ISNULL(WebSiteURL, '') AS crmAccountWebSite,ISNULL(COALESCE(phg_corporatesalesstatusname, 'UNKNOWN'), '') AS crmAccountSalesStatus,
					'umb://document/' + REPLACE(CAST(umbracoLeadGlobal.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS corporateAccountGlobalTeamLead,
					'umb://document/' + REPLACE(CAST(umbracoLeadAmericas.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS corporateAccountRegionalTeamLeadAmericas,
					'umb://document/' + REPLACE(CAST(umbracoLeadASPAC.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS corporateAccountRegionalTeamLeadASPAC,
					'umb://document/' + REPLACE(CAST(umbracoLeadEMEA.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS corporateAccountRegionalTeamLeadEMEA,
					'umb://document/' + REPLACE(CAST(umbracoIndustry.xml as xml).value('(/Industry/@key)[1]','nvarchar(MAX)'), '-', '') AS accountIndustry,0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.PHG_CorporateSalesStatus,account.PHG_CorporateSalesStatusName,phg_corporateglobalaccountmanagerid,
								phg_corporatermamericasid,phg_corporatermaspacid,phg_corporatermemeaid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND account.phg_corporateaccount = 1
							AND account.PHG_ShowinMemberPortalCorporateSales = 1
					  ) account
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadGlobal ON CAST(umbracoLeadGlobal.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																							AND CAST(umbracoLeadGlobal.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporateglobalaccountmanagerid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadAmericas ON CAST(umbracoLeadAmericas.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																							AND CAST(umbracoLeadAmericas.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporatermamericasid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadASPAC ON CAST(umbracoLeadASPAC.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145 
																							AND CAST(umbracoLeadASPAC.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporatermaspacid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadEMEA ON CAST(umbracoLeadEMEA.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																							AND CAST(umbracoLeadEMEA.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporatermemeaid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																							AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
						UNION

				 SELECT 'TARGET' AS TableName,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/corporateAccountCrmGuid)[1]','uniqueidentifier') AS corporateAccountCrmGuid,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountSalesStatus)[1]','nvarchar(MAX)'), '') AS crmAccountSalesStatus,
						cmsContentXml.globalTeamLead AS corporateAccountGlobalTeamLead,
						cmsContentXml.regionalTeamLeadAmericas AS corporateAccountRegionalTeamLeadAmericas,
						cmsContentXml.regionalTeamLeadASPAC AS corporateAccountRegionalTeamLeadASPAC,
						cmsContentXml.regionalTeamLeadEMEA AS corporateAccountRegionalTeamLeadEMEA,
						cmsContentXml.accountIndustry AS accountIndustry,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/disabled)[1]','bit') AS isDisabled
				 FROM (
						SELECT xml,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountGlobalTeamLead)[1]','nvarchar(100)') globalTeamLead,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountRegionalTeamLeadAmericas)[1]','nvarchar(100)') regionalTeamLeadAmericas,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountRegionalTeamLeadASPAC)[1]','nvarchar(100)') regionalTeamLeadASPAC,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountRegionalTeamLeadEMEA)[1]','nvarchar(100)') regionalTeamLeadEMEA,
							CAST(cx.xml as xml).value('(/corporateAccount/crmAccountIndustry)[1]','nvarchar(100)') accountIndustry
						FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
						WHERE ct.alias = 'corporateAccount'
					) cmsContentXml
				) unioned
		GROUP BY nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountSalesStatus,corporateAccountGlobalTeamLead,
				corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @CorporateAccountData ORDER BY nodeName,TableName


		INSERT INTO CmsImport.dbo.CorporateAccounts(Name,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
													crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
													crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
													corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
				corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,0,accountIndustry
		FROM @CorporateAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.CorporateAccounts(Name,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
													crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
													crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
													corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
				corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,1,accountIndustry
		FROM @CorporateAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @CorporateAccountData S WHERE S.TableName = 'SOURCE' AND T.corporateAccountCrmGUID = S.corporateAccountCrmGUID );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_DirectoryMembers]'
GO


ALTER PROCEDURE [dbo].[mpid_DirectoryMembers]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @Hotels TABLE(hotelId int, hotelUdi nvarchar(max),hotelName nvarchar(1000));

	INSERT INTO @Hotels(hotelId, hotelUdi,hotelName)
	SELECT umbracoNode.id AS hotelId, 'umb://document/' + REPLACE(CAST(umbracoNode.uniqueID AS VARCHAR(36)), '-', '') AS hotelUdi,
			CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelName)[1]','nvarchar(MAX)') AS hotelName
	FROM MemberPortal.dbo.cmsContentXml JOIN MemberPortal.dbo.umbracoNode ON cmsContentXml.nodeId = umbracoNode.id
	WHERE CAST(cmsContentXml.xml as xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
		AND CAST(cmsContentXml.xml as xml).value('(/Hotel/disabled)[1]','int') = 0
		
		
	DECLARE @Members TABLE(nodeID int,
							firstName nvarchar(1000),
							lastName nvarchar(1000),
							title nvarchar(1000),
							defaultHotelId nvarchar(1000),
							defaultHotelName nvarchar(1000),
							hotels nvarchar(max)
						   );


	INSERT INTO @Members(nodeID,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels)
	SELECT CAST(HotelMemberXML.xml AS XML).value('(/HotelMember/@id)[1]', 'int') AS nodeID,
			CAST(HotelMemberXML.xml as xml).value('(/HotelMember/firstName)[1]', 'nvarchar(MAX)') AS firstName,
			CAST(HotelMemberXML.xml as xml).value('(/HotelMember/lastName)[1]', 'nvarchar(MAX)') AS lastName,
			CAST(HotelMemberXML.xml as xml).value('(/HotelMember/title)[1]', 'nvarchar(MAX)') AS title,
			H.hotelId AS defaultHotelId,
			H.hotelName AS defaultHotelName,
			CASE WHEN H.hotelId IS NOT NULL THEN CAST(HotelMemberXML.xml as xml).value('(/HotelMember/hotels)[1]', 'nvarchar(MAX)') ELSE NULL END AS hotels
	FROM MemberPortal.dbo.cmsMember
		INNER JOIN MemberPortal.dbo.cmsContentXml HotelMemberXML ON cmsMember.nodeId = CAST(HotelMemberXML.xml AS xml).value('(/HotelMember/@id)[1]','int')
																AND CAST(HotelMemberXML.xml AS xml).value('(/HotelMember/@nodeType)[1]','nvarchar(MAX)') = 1109
																AND CAST(HotelMemberXML.xml AS xml).value('(/HotelMember/umbracoMemberApproved)[1]','bit') = 1
		LEFT JOIN @Hotels H ON CAST(HotelMemberXML.xml AS xml).value('(/HotelMember/defaultHotel)[1]','nvarchar(MAX)') = H.hotelUdi
	WHERE CAST(HotelMemberXML.xml as xml).value('(/HotelMember/firstName)[1]', 'nvarchar(MAX)') IS NOT NULL
		OR CAST(HotelMemberXML.xml as xml).value('(/HotelMember/lastName)[1]', 'nvarchar(MAX)') IS NOT NULL;


	DECLARE @MemberCount int;
	SELECT @MemberCount = COUNT(*) FROM @Members


	IF @MemberCount > 0
	BEGIN

		TRUNCATE TABLE dbo.DirectoryMembers;

		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.firstName,M.lastName,M.title,M.defaultHotelName) AS id,'firstname' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY firstName,lastName,title,defaultHotelName



		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.lastName,M.firstName,M.title,M.defaultHotelName) AS id,'lastname' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY lastName,firstName,title,defaultHotelName



		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.title,M.firstName,M.lastName,M.defaultHotelName) AS id,'title' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY title,firstName,lastName,defaultHotelName



		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.defaultHotelName,M.firstName,M.lastName,M.title) AS id,'hotel' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY defaultHotelName,firstName,lastName,title
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_GroupAccounts]'
GO

ALTER PROCEDURE [dbo].[mpid_GroupAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN

		TRUNCATE TABLE CmsImport.dbo.GroupAccounts;

		DECLARE @GroupAccountData TABLE(TableName nvarchar(6),
										nodeName nvarchar(100),
										crmAccountName nvarchar(100),
										groupAccountCrmGUID uniqueidentifier,
										crmAccountStreetAddressLine1 nvarchar(100),
										crmAccountStreetAddressLine2 nvarchar(100),
										crmAccountCity nvarchar(100),
										crmAccountState nvarchar(100),
										crmAccountCountry nvarchar(100),
										crmAccountPostalCode nvarchar(100),
										crmAccountPhoneNumber nvarchar(100),
										crmAccountEmailAddress nvarchar(100),
										crmAccountWebSite nvarchar(100),
										crmAccountManagedAccount bit,
										groupAccountSalesDirector nvarchar(100),
										accountIndustry nvarchar(100),
										isDisabled bit);

		INSERT INTO @GroupAccountData(TableName,nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
										crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
										crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,account.Name AS nodeName,account.Name AS crmAccountName,UPPER(account.AccountID) AS groupAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,
					ISNULL(Address1_City, '') AS crmAccountCity,ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,
					ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,
					ISNULL(WebSiteURL, '') AS crmAccountWebSite,CASE account.phg_groupsalesstatus WHEN 100000002 THEN 'True' ELSE 'False' END AS crmAccountManagedAccount,
					'umb://document/' + REPLACE(CAST(umbracoGroupSales.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS groupAccountSalesDirector,
					'umb://document/' + REPLACE(CAST(umbracoIndustry.xml as xml).value('(/Industry/@key)[1]','nvarchar(MAX)'), '-', '') AS accountIndustry,
					0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,
							Telephone1,EMailAddress1,WebSiteURL,account.phg_groupsalesstatus,phg_groupsalesdirectorid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_groupsalesstatus IN (100000001,100000002)
							AND account.phg_groupaccount = 1
							AND Account.phg_showinmemberportalgroupsales = 1
					  ) account
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoGroupSales ON CAST(umbracoGroupSales.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																						AND CAST(umbracoGroupSales.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_groupsalesdirectorid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																						AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
					UNION

					SELECT 'TARGET' AS TableName,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/groupAccountCrmGuid)[1]','uniqueidentifier') AS groupAccountCrmGUID,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountManagedAccount)[1]','BIT') AS crmAccountManagedAccount,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/groupAccountSalesDirector)[1]','nvarchar(MAX)') AS groupAccountSalesDirector,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountIndustry)[1]','nvarchar(MAX)') AS accountIndustry,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/disabled)[1]','bit') AS isDisabled
					FROM MemberPortal.dbo.cmsContentXml
					WHERE CAST(cmsContentXml.xml as xml).value('(/GroupAccount/@nodeType)[1]','nvarchar(MAX)') = 17836
			 ) unioned
		GROUP BY nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @GroupAccountData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.GroupAccounts(Name,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
										crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
										groupAccountSalesDirector,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				groupAccountSalesDirector,0,accountIndustry
		FROM @GroupAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.GroupAccounts(Name,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
										crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
										groupAccountSalesDirector,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				groupAccountSalesDirector,1,accountIndustry
		FROM @GroupAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @GroupAccountData S WHERE S.TableName = 'SOURCE' AND T.groupAccountCrmGUID = S.groupAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Hotels]'
GO


ALTER PROCEDURE [dbo].[mpid_Hotels]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE dbo.Hotels;

		--get all hotel ids and all collections they are in
		DECLARE @HotelCodes TABLE(Accountid uniqueidentifier,CollectionCode nvarchar(100));

		INSERT intO @HotelCodes(Accountid,CollectionCode)
		SELECT DISTINCT hc.phg_hotel AS Accountid,c.phg_code AS CollectionCode
		FROM LocalCRM.dbo.phg_hotelcollection hc
			INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
		WHERE GETDATE() BETWEEN ISNULL(hc.phg_startdate, '1900-1-1') AND ISNULL(hc.phg_enddate, '2100-12-31');


		--combine the collections into one field per hotel
		DECLARE @GroupedHotelCodes TABLE(Accountid uniqueidentifier,CollectionCodes nvarchar(100));

		INSERT intO @GroupedHotelCodes(Accountid,CollectionCodes)
		SELECT DISTINCT Codes.Accountid,
				CAST((SELECT H.CollectionCode + ' ' FROM @HotelCodes H WHERE H.Accountid = Codes.Accountid ORDER BY H.CollectionCode FOR XML PATH('')) AS nvarchar(MAX)) AS hotels
		FROM @HotelCodes AS Codes;


		--get all contacts from Umbraco
		DECLARE @umbracoContacts TABLE(nodeId nvarchar(100),crmContactGuid uniqueidentifier);

		INSERT intO @umbracoContacts (nodeId,crmContactGuid)
		SELECT 'umb://document/' + REPLACE(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') as nodeID,CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier')
		FROM MemberPortal.dbo.cmsContentXml
		WHERE CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
			AND CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/disabled)[1]','bit') = 0;


		 --get all hotels from Umbraco
		 DECLARE @umbracoHotels TABLE(hotelCrmGuid uniqueidentifier,
										hotelCode nvarchar(100),
										nodeName nvarchar(100),
										hotelName nvarchar(100),
										hotelCity nvarchar(100),
										hotelState nvarchar(100),
										hotelCountry nvarchar(100),
										hotelRegion nvarchar(100),
										hotelIprefer bit,
										hotelAccountDirector nvarchar(100),
										hotelMarketingContact nvarchar(100),
										hotelRegionalDirector nvarchar(100),
										hotelAreaManagingDirector nvarchar(100),
										hotelRevenueAccountManager nvarchar(100),
										hotelAccountsReceivableContact nvarchar(100),
										hotelIPreferEngagementSpecialist nvarchar(100),
										hotelExecutiveVicePresident nvarchar(100),
										hotelEventContact nvarchar(100),
										contractedCurrency nvarchar(3),
										isDisabled bit,
										hotelCollectionCodes nvarchar(100),
										hotelMainCollection nvarchar(50),
										hotelRateGainId nvarchar(50),
										hotelHidePayment bit,
										hotelRegionalAdministration nvarchar(100));

		INSERT intO @umbracoHotels(hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,hotelMarketingContact,
									hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
									hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCrmGuid)[1]','uniqueidentifier') AS hotelCrmGuid,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCode)[1]','nvarchar(MAX)') AS hotelCode,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelName)[1]','nvarchar(MAX)') AS hotelName,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCity)[1]','nvarchar(MAX)'), '') AS hotelCity,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelState)[1]','nvarchar(MAX)'), '') AS hotelState,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCountry)[1]','nvarchar(MAX)'), '') AS hotelCountry,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelRegion)[1]','nvarchar(MAX)'), '') AS hotelRegion,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelIprefer)[1]','bit') AS hotelIprefer,
				nhad.uniqueID AS hotelAccountDirector,
				nhmc.uniqueID AS hotelMarketingContact,
				nhrd.uniqueID AS hotelRegionalDirector,
				nhamd.uniqueID AS hotelAreaManagingDirector,
				nhram.uniqueID AS hotelRevenueAccountManager,
				nharc.uniqueID AS hotelAccountsReceivableContact,
				nhipes.uniqueID AS hotelIPreferEngagementSpecialist,
				nhevp.uniqueID AS hotelExecutiveVicePresident,
				nhec.uniqueID AS hotelEventContact,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/contractedCurrency)[1]','nvarchar(MAX)'), '') AS contractedCurrency,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/disabled)[1]','bit') AS isDisabled,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCollectionCodes)[1]','nvarchar(MAX)'), '') AS hotelCollectionCodes,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelMainCollection)[1]','nvarchar(MAX)'), '') AS hotelMainCollection,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelRateGainID)[1]','nvarchar(MAX)'), '') AS hotelRateGainId,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelHidePayment)[1]','bit'), 1) AS hotelHidePayment,
				nhra.uniqueID AS hotelRegionalAdministration
		FROM (
				SELECT xml,
				CAST(cx.xml as xml).value('(/Hotel/hotelAccountDirector)[1]','nvarchar(100)') hotelAccountDirector,
				CAST(cx.xml as xml).value('(/Hotel/hotelMarketingContact)[1]','nvarchar(100)') hotelMarketingContact,
				CAST(cx.xml as xml).value('(/Hotel/hotelRegionalDirector)[1]','nvarchar(100)') hotelRegionalDirector,
				CAST(cx.xml as xml).value('(/Hotel/hotelAreaManagingDirector)[1]','nvarchar(100)') hotelAreaManagingDirector,
				CAST(cx.xml as xml).value('(/Hotel/hotelRevenueAccountManager)[1]','nvarchar(100)') hotelRevenueAccountManager,
				CAST(cx.xml as xml).value('(/Hotel/hotelAccountsReceivableContact)[1]','nvarchar(100)') hotelAccountsReceivableContact,
				CAST(cx.xml as xml).value('(/Hotel/hotelIPreferEngagementSpecialist)[1]','nvarchar(100)') hotelIPreferEngagementSpecialist,
				CAST(cx.xml as xml).value('(/Hotel/hotelExecutiveVicePresident)[1]','nvarchar(100)') hotelExecutiveVicePresident,
				CAST(cx.xml as xml).value('(/Hotel/hotelEventContact)[1]','nvarchar(100)') hotelEventContact,
				CAST(cx.xml as xml).value('(/Hotel/hotelRegionalAdministration)[1]','nvarchar(100)') hotelRegionalAdministration
				FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
				WHERE ct.alias = 'Hotel'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelAccountDirector)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelMarketingContact)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelRegionalDirector)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelAreaManagingDirector)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelRevenueAccountManager)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelAccountsReceivableContact)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelIPreferEngagementSpecialist)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelExecutiveVicePresident)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelEventContact)[1]','nvarchar(100)') like 'umb%'
				AND CAST(cx.xml as xml).value('(/Hotel/hotelRegionalAdministration)[1]','nvarchar(100)') like 'umb%'
			) cmsContentXml
			LEFT JOIN MemberPortal.dbo.umbracoNode nhad ON nhad.uniqueID = CAST(SUBSTRING(hotelAccountDirector, 16, 8) + '-' + SUBSTRING(hotelAccountDirector, 24, 4) + '-' + SUBSTRING(hotelAccountDirector, 28, 4) + '-' + SUBSTRING(hotelAccountDirector, 32, 4) + '-' + SUBSTRING(hotelAccountDirector, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhmc ON nhmc.uniqueID = CAST(SUBSTRING(hotelMarketingContact, 16, 8) + '-' + SUBSTRING(hotelMarketingContact, 24, 4) + '-' + SUBSTRING(hotelMarketingContact, 28, 4) + '-' + SUBSTRING(hotelMarketingContact, 32, 4) + '-' + SUBSTRING(hotelMarketingContact, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhrd ON nhrd.uniqueID = CAST(SUBSTRING(hotelRegionalDirector, 16, 8) + '-' + SUBSTRING(hotelRegionalDirector, 24, 4) + '-' + SUBSTRING(hotelRegionalDirector, 28, 4) + '-' + SUBSTRING(hotelRegionalDirector, 32, 4) + '-' + SUBSTRING(hotelRegionalDirector, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhamd ON nhamd.uniqueID = CAST(SUBSTRING(hotelAreaManagingDirector, 16, 8) + '-' + SUBSTRING(hotelAreaManagingDirector, 24, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 28, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 32, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhram ON nhram.uniqueID = CAST(SUBSTRING(hotelRevenueAccountManager, 16, 8) + '-' + SUBSTRING(hotelRevenueAccountManager, 24, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 28, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 32, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nharc ON nharc.uniqueID = CAST(SUBSTRING(hotelAccountsReceivableContact, 16, 8) + '-' + SUBSTRING(hotelAccountsReceivableContact, 24, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 28, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 32, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhipes ON nhipes.uniqueID = CAST(SUBSTRING(hotelIPreferEngagementSpecialist, 16, 8) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 24, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 28, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 32, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhevp ON nhevp.uniqueID = CAST(SUBSTRING(hotelExecutiveVicePresident, 16, 8) + '-' + SUBSTRING(hotelExecutiveVicePresident, 24, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 28, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 32, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhec ON nhec.uniqueID = CAST(SUBSTRING(hotelEventContact, 16, 8) + '-' + SUBSTRING(hotelEventContact, 24, 4) + '-' + SUBSTRING(hotelEventContact, 28, 4) + '-' + SUBSTRING(hotelEventContact, 32, 4) + '-' + SUBSTRING(hotelEventContact, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhra ON nhra.uniqueID = CAST(SUBSTRING(hotelRegionalAdministration, 16, 8) + '-' + SUBSTRING(hotelRegionalAdministration, 24, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 28, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 32, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 36, 12) AS uniqueidentifier)
		;



		--combine hotels from Umbraco and hotels from CRM into one table, with "table name" showing which system it came from
		--SOURCE table = CRM, TARGET table = Umbraco
		DECLARE @HotelData TABLE(TableName nvarchar(6),
								hotelCrmGuid uniqueidentifier,
								hotelCode nvarchar(100),
								nodeName nvarchar(100),
								hotelName nvarchar(100),
								hotelCity nvarchar(100),
								hotelState nvarchar(100),
								hotelCountry nvarchar(100),
								hotelRegion nvarchar(100),
								hotelIprefer bit,
								hotelAccountDirector nvarchar(100),
								hotelMarketingContact nvarchar(100),
								hotelRegionalDirector nvarchar(100),
								hotelAreaManagingDirector nvarchar(100),
								hotelRevenueAccountManager nvarchar(100),
								hotelAccountsReceivableContact nvarchar(100),
								hotelIPreferEngagementSpecialist nvarchar(100),
								hotelExecutiveVicePresident nvarchar(100),
								hotelEventContact nvarchar(100),
								contractedCurrency nvarchar(3),
								isDisabled bit,
								hotelCollectionCodes nvarchar(100),
								hotelMainCollection nvarchar(50),
								hotelRateGainId nvarchar(50),
								hotelRegionalAdministration nvarchar(100));

		INSERT INTO @HotelData(TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
								hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration)
		SELECT MIN(TableName) AS TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,MIN(hotelState),hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
				hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
				hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration
		FROM (SELECT DISTINCT 'SOURCE' AS TableName,hotelsReporting.crmGuid AS hotelCrmGuid,hotelsReporting.code AS hotelCode,hotelsReporting.hotelName AS nodeName,
						hotelsReporting.hotelName AS hotelName,ISNULL(hotelsReporting.physicalCity, '') AS hotelCity,ISNULL(subAreas.subAreaName, '') AS hotelState,
						ISNULL(countries.shortName, '') AS hotelCountry,ISNULL(hotelsReporting.geographicRegionName, '') AS hotelRegion,
						ISNULL(iPrefer.ContractFound, 0) AS hotelIprefer,hotelAccountDirector.nodeId AS hotelAccountDirector,hotelMarketingContact.nodeId AS hotelMarketingContact,
						hotelRegionalDirector.nodeId AS hotelRegionalDirector,hotelAreaManagingDirector.nodeId AS hotelAreaManagingDirector,
						hotelRevenueAccountManager.nodeId AS hotelRevenueAccountManager,hotelAccountsReceivableContact.nodeId AS hotelAccountsReceivableContact,
						hotelIPreferEngagementSpecialist.nodeId AS hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident.nodeId AS hotelExecutiveVicePresident,
						hotelEventContact.nodeId AS hotelEventContact,ISNULL(FinancialAccounts.currencyCode, '') AS contractedCurrency,0 AS isDisabled,
						ISNULL(BrandCodes.CollectionCodes, '') AS hotelCollectionCodes,ISNULL(mainBrandCode + ',' + brands.name, '') AS hotelMainCollection,
						ISNULL(phg_rategainid, '') AS hotelRateGainId,hotelRegionalAdministration.nodeId AS hotelRegionalAdministration
				FROM Core.dbo.hotelsReporting
					INNER JOIN (SELECT DISTINCT Account.accountid,Account.phg_accountdirectorid,Account.phg_marketingcontactid,Account.phg_regionalmanagerid,
										Account.phg_areamanagerid,Account.phg_revenueaccountmanagerid,Account.phg_accountsreceivablecontactid,Account.phg_ipreferexecutiveadminid,
										Account.phg_executivevicepresidentid,Account.phg_rategainid,Account.phg_regionaladministrationid
								FROM LocalCRM.dbo.Account
									INNER JOIN LocalCRM.dbo.phg_hotelcollection hc ON Account.accountid = hc.phg_hotel
									INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
								WHERE Account.statuscodename IN('Member Hotel','Renewal Hotel')
									OR Account.PHG_MemberPortalImportOverride = 0
								) Account ON hotelsReporting.crmGuid = Account.accountID
					LEFT JOIN ISO.dbo.subAreas ON subAreas.countryCode2 = hotelsReporting.country AND subAreas.subAreaCode = hotelsReporting.state
					LEFT JOIN ISO.dbo.countries ON hotelsReporting.country = countries.code2
					LEFT JOIN (SELECT DISTINCT Account.accountid AS AccountId,1 AS ContractFound
								FROM (SELECT DISTINCT hc.phg_hotel AS Accountid
										FROM LocalCRM.dbo.Account
											INNER JOIN LocalCRM.dbo.phg_hotelcollection hc ON Account.accountid = hc.phg_hotel
											INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
										WHERE Account.statuscodename IN('Member Hotel','Renewal Hotel')
											OR Account.PHG_MemberPortalImportOverride = 0
									  ) Account
									INNER JOIN LocalCRM.dbo.phg_contractedprograms ON Account.accountId = phg_contractedprograms.phg_accountnameid
																	AND phg_contractedprograms.phg_contractedprogramstypename LIKE '%i%Prefer%'
																	AND GETDATE() BETWEEN ISNULL(phg_contractedprograms.phg_startdate, '1900-1-1')
																	AND ISNULL(phg_contractedprograms.phg_enddate, '2100-12-31')
								) iPrefer ON Account.accountId = iPrefer.AccountId
					LEFT JOIN @GroupedHotelCodes AS BrandCodes ON hotelsReporting.crmGuid = BrandCodes.Accountid
					LEFT JOIN Core.dbo.brands ON hotelsReporting.mainBrandCode = brands.code
					LEFT JOIN @umbracoContacts as hotelAccountDirector ON hotelAccountDirector.crmContactGuid = Account.phg_accountdirectorid
					LEFT JOIN @umbracoContacts as hotelMarketingContact ON hotelMarketingContact.crmContactGuid = Account.phg_marketingcontactid
					LEFT JOIN @umbracoContacts as hotelRegionalDirector ON hotelRegionalDirector.crmContactGuid = Account.phg_regionalmanagerid
					LEFT JOIN @umbracoContacts as hotelAreaManagingDirector ON hotelAreaManagingDirector.crmContactGuid = Account.phg_areamanagerid
					LEFT JOIN @umbracoContacts as hotelRevenueAccountManager ON hotelRevenueAccountManager.crmContactGuid = Account.phg_revenueaccountmanagerid
					LEFT JOIN @umbracoContacts as hotelAccountsReceivableContact ON hotelAccountsReceivableContact.crmContactGuid = Account.phg_accountsreceivablecontactid
					LEFT JOIN @umbracoContacts as hotelExecutiveVicePresident ON hotelExecutiveVicePresident.crmContactGuid = Account.phg_executivevicepresidentid
					LEFT JOIN @umbracoContacts as hotelIPreferEngagementSpecialist ON hotelIPreferEngagementSpecialist.crmContactGuid = Account.phg_ipreferexecutiveadminid
					LEFT JOIN @umbracoContacts as hotelRegionalAdministration ON hotelRegionalAdministration.crmContactGuid = Account.phg_regionaladministrationid
					LEFT JOIN @umbracoContacts as hotelEventContact ON hotelEventContact.crmContactGuid = '570C90EF-8AA2-E611-80F9-FC15B4282DF4'
					LEFT JOIN dbo.FinancialAccounts ON hotelsReporting.code = FinancialAccounts.hotelCode
						UNION
				SELECT 'TARGET' AS TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
						hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
						hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,
						hotelMainCollection,hotelRateGainId,hotelRegionalAdministration
				FROM @umbracoHotels
			) unioned
		GROUP BY hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity
		--,hotelState
		,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,hotelMarketingContact,
				hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
				hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration,isDisabled
		--HAVING COUNT(*) = 1  <<Bruce Okallau Mar 30th 2020 I cannot understand why this would be there except perhaps to remove duplicates but unfortunately it ends up filtering out other valid hotels. Removing this for now but keeping it in the comments so that we can revisit the decision in the future. I'm also taking the minimum of the hotel state as some irregularities in ISO codes can lead to duplicate entries if we do not.
		;


		INSERT INTO dbo.Hotels(Name,hotelCrmGuid,hotelDisabled,hotelCode,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
								hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,
								hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT	S.hotelName,S.hotelCrmGuid,0,S.hotelCode,S.hotelName,S.hotelCity,S.hotelState,S.hotelCountry,S.hotelRegion,S.hotelIprefer,S.hotelAccountDirector,
				S.hotelMarketingContact,S.hotelRegionalDirector,S.hotelAreaManagingDirector,S.hotelRevenueAccountManager,S.hotelAccountsReceivableContact,
				S.hotelIPreferEngagementSpecialist,S.hotelExecutiveVicePresident,S.hotelEventContact,S.contractedCurrency,S.hotelCollectionCodes,S.hotelMainCollection,
				S.hotelRateGainId,COALESCE(U.hotelHidePayment, 1),S.hotelRegionalAdministration
		FROM @HotelData S
			LEFT JOIN @umbracoHotels U ON S.hotelCrmGuid = U.hotelCrmGuid
		WHERE TableName = 'SOURCE'


		INSERT INTO dbo.Hotels(Name,hotelCrmGuid,hotelDisabled,hotelCode,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
								hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,
								hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT T.hotelName,T.hotelCrmGuid,1,T.hotelCode,T.hotelName,T.hotelCity,T.hotelState,T.hotelCountry,T.hotelRegion,T.hotelIprefer,T.hotelAccountDirector,
				T.hotelMarketingContact,T.hotelRegionalDirector,T.hotelAreaManagingDirector,T.hotelRevenueAccountManager,T.hotelAccountsReceivableContact,
				T.hotelIPreferEngagementSpecialist,T.hotelExecutiveVicePresident,T.hotelEventContact,T.contractedCurrency,T.hotelCollectionCodes,
				T.hotelMainCollection,T.hotelRateGainId,1,T.hotelRegionalAdministration
		FROM @HotelData T
			INNER JOIN @umbracoHotels U ON U.hotelCrmGuid = T.hotelCrmGuid AND U.isDisabled = 0
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS ( SELECT 1 FROM @HotelData S WHERE S.TableName = 'SOURCE' AND T.hotelCrmGuid = S.hotelCrmGuid );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Industries]'
GO
ALTER PROCEDURE [dbo].[mpid_Industries]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	TRUNCATE TABLE dbo.Industries;

	DECLARE @IndustryData TABLE(TableName nvarchar(6),industryId int,industryName nvarchar(100),isDisabled bit);

	INSERT INTO @IndustryData(TableName,industryId,industryName,isDisabled)
	SELECT MIN(TableName) AS TableName,industryId,industryName,isDisabled
	FROM (SELECT DISTINCT 'SOURCE' AS TableName,Account.industrycode AS industryId,Account.industrycodename AS industryName,0 AS isDisabled
			FROM LocalCRM.dbo.Account
			WHERE Account.industrycode IS NOT NULL
				UNION
			SELECT 'TARGET' AS TableName,
					CAST(cmsContentXml.xml as xml).value('(/Industry/industryId)[1]','int') AS industryId,
					CAST(cmsContentXml.xml as xml).value('(/Industry/industryName)[1]','NVARCHAR(MAX)') AS industryName,
					CAST(cmsContentXml.xml as xml).value('(/Industry/disabled)[1]','BIT') AS isDisabled
			FROM MemberPortal.dbo.cmsContentXml
			WHERE CAST(cmsContentXml.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
		  ) AS unioned
	GROUP BY industryId,industryName,isDisabled
	HAVING COUNT(*) = 1;


	INSERT INTO dbo.Industries(industryId,industryName,[disabled])
	SELECT industryId,industryName,0
	FROM @IndustryData
	WHERE TableName = 'SOURCE';


	INSERT INTO dbo.Industries(industryId,industryName,[disabled])
	SELECT industryId,industryName,1
	FROM @IndustryData T
		INNER JOIN MemberPortal.dbo.cmsContentXml ON CAST(cmsContentXml.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
													AND CAST(cmsContentXml.xml as xml).value('(/Industry/industryId)[1]','int') = T.industryId
													AND CAST(cmsContentXml.xml as xml).value('(/Industry/disabled)[1]','BIT') = 0
	WHERE T.TableName = 'TARGET'
		AND NOT EXISTS ( SELECT 1 FROM @IndustryData S WHERE S.TableName = 'SOURCE' AND T.industryId = S.industryId );
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_LeisureAccounts]'
GO

ALTER PROCEDURE [dbo].[mpid_LeisureAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.LeisureAccounts;

		DECLARE @LeisureAccountData TABLE(TableName nvarchar(6),
											nodeName nvarchar(100),
											crmAccountName nvarchar(100),
											leisureAccountCrmGUID uniqueidentifier,
											crmAccountStreetAddressLine1 nvarchar(100),
											crmAccountStreetAddressLine2 nvarchar(100),
											crmAccountCity nvarchar(100),
											crmAccountState nvarchar(100),
											crmAccountCountry nvarchar(100),
											crmAccountPostalCode nvarchar(100),
											crmAccountPhoneNumber nvarchar(100),
											crmAccountEmailAddress nvarchar(100),
											crmAccountWebSite nvarchar(100),
											crmAccountManagedAccount bit,
											leisureAccountIataGlobalAccountManager nvarchar(100),
											accountIndustry nvarchar(100),
											isDisabled bit);

		INSERT INTO @LeisureAccountData(TableName,nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
										crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
										crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountManagedAccount,leisureIataGlobalAccountManager,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,account.Name AS nodeName,account.Name AS crmAccountName,UPPER(account.AccountID) AS leisureAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,
					ISNULL(Address1_City, '') AS crmAccountCity,ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,
					ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,
					ISNULL(WebSiteURL, '') AS crmAccountWebSite,CASE account.phg_leisuresalesstatus WHEN 100000002 THEN 1 ELSE 0 END AS crmAccountManagedAccount,
					'umb://document/' + REPLACE(CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/@key)[1]','nvarchar(MAX)'), '-', '') AS leisureIataGlobalAccountManager,
					'umb://document/' + REPLACE(CAST(umbracoIndustry.xml as xml).value('(/Industry/@key)[1]','nvarchar(MAX)'), '-', '') AS accountIndustry,
					0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.phg_leisuresalesstatus,phg_iataglobalaccountmanagerid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_leisuresalesstatus IN (100000001,100000002)
							AND account.phg_leisuresales = 1
							AND account.phg_showinmemberportalleisure = 1
					 ) account
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIataGlobalAccountManager ON CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																										AND CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_iataglobalaccountmanagerid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																						AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
					UNION

					SELECT 'TARGET' AS TableName,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/leisureAccountCrmGuid)[1]','uniqueidentifier') AS leisureAccountCrmGuid,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountManagedAccount)[1]','BIT') AS crmAccountManagedAccount,
						cmsContentXml.globalAccountManager AS leisureIataGlobalAccountManager,
						cmsContentXml.accountIndustry AS accountIndustry,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/disabled)[1]','bit') AS isDisabled
					FROM (
						SELECT xml,
							CAST(cx.xml as xml).value('(/LeisureAccount/leisureAccountIataGlobalAccountManager)[1]','nvarchar(100)') globalAccountManager,
							CAST(cx.xml as xml).value('(/LeisureAccount/crmAccountIndustry)[1]','nvarchar(100)') accountIndustry
						FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
						WHERE ct.alias = 'LeisureAccount'
					) cmsContentXml
			  ) unioned
		GROUP BY nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureIataGlobalAccountManager,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @LeisureAccountData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.LeisureAccounts(Name,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState, crmAccountCountry, crmAccountPostalCode, crmAccountPhoneNumber,
										crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,0,accountIndustry
		FROM @LeisureAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.LeisureAccounts(Name,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState, crmAccountCountry, crmAccountPostalCode, crmAccountPhoneNumber,
										crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,1,accountIndustry
		FROM @LeisureAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @LeisureAccountData S WHERE S.TableName = 'SOURCE' AND T.leisureAccountCrmGUID = S.leisureAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_PHR_Members]'
GO


ALTER PROCEDURE [dbo].[mpid_PHR_Members]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.SystemUser WHERE domainname IS NOT NULL AND IsDisabled = 0)
	BEGIN
		TRUNCATE TABLE dbo.PhrMembers_Hotels
		TRUNCATE TABLE dbo.PhrMembers_Inactive


		--  Retrieve PHR Members who are using the demo hotel
		DECLARE @TestHotelId nvarchar(MAX),
				@TestHotelIdXml nvarchar(MAX);

		SELECT @TestHotelId = 'umb://document/' + LOWER(REPLACE(CAST(cmsContentXml.xml AS XML).value('(/Hotel/@key)[1]', 'nvarchar(MAX)'), '-', ''))
		FROM MemberPortal.dbo.cmsContentXml
		WHERE CAST(cmsContentXml.xml as xml).value('(/Hotel/@nodeType)[1]','nvarchar(MAX)') = 4729
			AND CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelName)[1]','nvarchar(MAX)') = 'PHG Test Hotel'

		SELECT	@TestHotelIdXml = '%<i>' + @TestHotelId + '</i>%'


		DECLARE @PhgTestHotelUsers TABLE(emailAddress nvarchar(100),TestHotelId nvarchar(MAX))

		INSERT INTO @PhgTestHotelUsers(emailAddress,TestHotelId)
		SELECT email,@TestHotelId
		FROM (SELECT CAST(cmsContentXml.xml as xml).value('(/PHRMember/@id)[1]','nvarchar(MAX)') AS id,
					CAST(cmsContentXml.xml as xml).value('(/PHRMember/@loginName)[1]','nvarchar(MAX)') AS email,
					CONVERT(XML, '<i>' + REPLACE(CAST(cmsContentXml.xml as xml).value('(/PHRMember/hotels)[1]','nvarchar(MAX)'), ',', '</i><i>') + '</i>').query('.') AS hotelXML
				FROM MemberPortal.dbo.cmsContentXml
				WHERE CAST(cmsContentXml.xml as xml).value('(/PHRMember/@nodeType)[1]','nvarchar(MAX)') = 10458
					AND CAST(cmsContentXml.xml as xml).value('(/PHRMember/hotels)[1]','nvarchar(MAX)') IS NOT NULL
			  ) xmlData
		WHERE CAST(xmldata.hotelxml as nvarchar(max)) like @TestHotelIdXml


		-- Grab users that should have all hotels
		DECLARE @PhgAllHotelUsers TABLE(emailAddress nvarchar(100))

		INSERT INTO @PhgAllHotelUsers(emailAddress)
		SELECT emailAddress FROM dbo.PhrMembers_AllHotels

		UPDATE dbo.PhrMembers
			SET updateMember = 0


		DECLARE @hotels TABLE(emailAddress nvarchar(100),HotelIDs nvarchar(MAX))
	
		INSERT INTO @hotels(emailAddress,HotelIDs)
		SELECT emailAddress, HotelNodeID
		FROM (
				SELECT DISTINCT mh.emailAddress, h.HotelNodeID
				FROM CrmMemberHotel mh
					JOIN
					(
						SELECT cpd.dataNvarchar AS hotelCode, 'umb://document/' + LOWER(REPLACE(CAST(n.uniqueID AS nvarchar(MAX)), '-', '')) AS HotelNodeID
						FROM
							MemberPortal.dbo.umbracoNode n
							JOIN MemberPortal.dbo.cmsContent c ON n.id = c.nodeId
							JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
							JOIN MemberPortal.dbo.cmsDocument d ON n.id = d.nodeId
							JOIN MemberPortal.dbo.cmsPropertyData cpd ON d.versionId = cpd.versionId
							JOIN MemberPortal.dbo.cmsPropertyType cpt ON cpd.propertytypeid = cpt.id
							JOIN MemberPortal.dbo.cmsPropertyData dpd ON d.versionId = dpd.versionId
							JOIN MemberPortal.dbo.cmsPropertyType dpt ON dpd.propertytypeid = dpt.id
						WHERE
							ct.alias = 'Hotel'
							AND d.published = 1
							AND cpt.Alias = 'hotelCode'
							AND dpt.Alias = 'disabled'
							AND dpd.dataInt = 0
					) h ON mh.hotelCode = h.hotelCode

				UNION

				SELECT emailAddress, 'umb://document/' + LOWER(REPLACE(CAST(HotelXML.uniqueID AS nvarchar(MAX)), '-', '')) AS HotelNodeID
				FROM @PhgAllHotelUsers,
					(
						SELECT n.uniqueID
						FROM
							MemberPortal.dbo.umbracoNode n
							JOIN MemberPortal.dbo.cmsContent c ON n.id = c.nodeId
							JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
							JOIN MemberPortal.dbo.cmsDocument d ON n.id = d.nodeId
							JOIN MemberPortal.dbo.cmsPropertyData pd ON d.versionId = pd.versionId
							JOIN MemberPortal.dbo.cmsPropertyType pt ON pd.propertytypeid = pt.id
						WHERE
							ct.alias = 'Hotel'
							AND d.published = 1
							AND pt.Alias = 'disabled'
							AND pd.dataInt = 0
					) HotelXML

				UNION

				SELECT DISTINCT D.emailAddress,D.TestHotelId AS HotelNodeID
				FROM @PhgTestHotelUsers D
			) AS unioned



		INSERT INTO dbo.PhrMembers_Hotels(emailAddress,defaultHotel,hotels)
		SELECT DISTINCT H1.emailAddress,
					CAST((SELECT TOP 1 dh.HotelIDs FROM @hotels dh WHERE dh.emailAddress = H1.emailAddress) AS nvarchar(100)) AS defaultHotel,
					CAST((SELECT H2.HotelIDs + ',' FROM @hotels H2 WHERE H2.emailAddress = H1.emailAddress FOR XML PATH('')) AS nvarchar(MAX)) AS hotels
		FROM @hotels H1



		UPDATE dbo.PhrMembers_Hotels
			SET hotels = LEFT(hotels,LEN(hotels) - 1)



		DECLARE @PhrMembersData TABLE(TableName nvarchar(6),
										firstName nvarchar(100),
										lastName nvarchar(100),
										title nvarchar(100),
										email nvarchar(100),
										defaultHotel nvarchar(100),
										hotels nvarchar(MAX)
									 );

		INSERT INTO @PhrMembersData(TableName,firstName,lastName,title,email,defaultHotel,hotels)
		SELECT MIN(TableName) AS TableName,firstName,lastName,title,email,defaultHotel,hotels
		FROM (SELECT 'SOURCE' AS TableName,FirstName AS firstName,LastName AS lastName,Title AS title,domainname AS email,H.defaultHotel,H.hotels
				FROM LocalCRM.dbo.SystemUser
					LEFT JOIN dbo.PhrMembers_Hotels H ON SystemUser.domainname = H.emailAddress COLLATE SQL_Latin1_General_CP1_CI_AS
				WHERE domainname IS NOT NULL
					AND IsDisabled = 0

				UNION

				SELECT 'TARGET' AS TableName,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/firstName)[1]','nvarchar(MAX)') AS firstName,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/lastName)[1]','nvarchar(MAX)') AS lastName,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/title)[1]','nvarchar(MAX)') AS title,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/@loginName)[1]','nvarchar(MAX)') AS email,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/defaultHotel)[1]','nvarchar(MAX)') AS defaultHotel,
						CAST(cmsXML.xml AS xml).value('(/PHRMember/hotels)[1]','nvarchar(MAX)') AS hotels
				FROM MemberPortal.dbo.cmsContentXml cmsXML
					INNER JOIN MemberPortal.dbo.cmsMember ON CAST(cmsXML.xml AS xml).value('(/PHRMember/@id)[1]', 'nvarchar(MAX)') = cmsMember.nodeId
				WHERE CAST(cmsXML.xml AS xml).value('(/PHRMember/@nodeType)[1]','nvarchar(MAX)') = 10458
					AND CAST(cmsXML.xml AS xml).value('(/PHRMember/umbracoMemberApproved)[1]','nvarchar(MAX)') = 1
			  ) AS unioned
		GROUP BY firstName,lastName,title,email,defaultHotel,hotels
		--HAVING COUNT(*) = 1



		UPDATE dbo.PhrMembers
			SET firstName = P.firstName,
				lastName = P.lastName,
				title = P.title,
				updateMember = 1
		FROM dbo.PhrMembers
			INNER JOIN @PhrMembersData P ON PhrMembers.email = P.email
			INNER JOIN MemberPortal.dbo.cmsMember ON p.email = cmsMember.LoginName
			INNER JOIN MemberPortal.dbo.cmsContentXml cmsXML ON cmsMember.nodeId = CAST(cmsXML.xml AS xml).value('(/PHRMember/@id)[1]', 'nvarchar(MAX)')
															AND CAST(cmsXML.xml AS xml).value('(/PHRMember/@nodeType)[1]','nvarchar(MAX)') = 10458
															AND	CAST(cmsXML.xml AS xml).value('(/PHRMember/umbracoMemberApproved)[1]','nvarchar(MAX)') = 1
		WHERE P.TableName = 'SOURCE'



		INSERT INTO dbo.PhrMembers(firstName,lastName,title,email,updateMember,sendEmail)
		SELECT firstName,lastName,title,email,1,1
		FROM @PhrMembersData P
		WHERE P.TableName = 'SOURCE'
			AND P.email NOT IN(SELECT N.email FROM dbo.PhrMembers AS N)

		UPDATE p
			SET p.updateMember = 1
		FROM dbo.PhrMembers p
			LEFT OUTER JOIN MemberPortal.dbo.cmsMember c ON p.email = c.Email
		WHERE c.nodeId IS NULL

		INSERT INTO dbo.PhrMembers_Inactive(email)
		SELECT T.email
		FROM @PhrMembersData T
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS ( SELECT 1 FROM @PhrMembersData S WHERE S.TableName = 'SOURCE' AND S.email = T.email );
		
	
		UPDATE dbo.PhrMembers
			SET	updateMember = 0
		WHERE email IN(SELECT emailAddress FROM dbo.PhrMembers_ExcludeMembers)
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Set_QI_onPhgEmployees]'
GO

ALTER PROCEDURE [dbo].[mpid_Set_QI_onPhgEmployees]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.SystemUser WHERE IsDisabled = 0)
	BEGIN
		TRUNCATE TABLE dbo.PhgEmployees;

		DECLARE @EmployeeData TABLE(TableName nvarchar(6),
									crmContactGuid uniqueidentifier,
									nodeName nvarchar(100),
									crmContactFullname nvarchar(100),
									crmContactFirstName nvarchar(100),
									crmContactLastName nvarchar(100),
									crmContactJobTitle nvarchar(100),
									crmContactPhoneNumber nvarchar(100),
									crmContactFaxNumber nvarchar(100),
									crmContactEmailAddress nvarchar(100),
									phgEmployeeCity nvarchar(100),
									phgEmployeeState nvarchar(100),
									isDisabled bit);

		INSERT INTO @EmployeeData(TableName,crmContactGuid,nodeName,crmContactFullname,crmContactFirstName,crmContactLastName,
									crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,crmContactEmailAddress,phgEmployeeCity,phgEmployeeState,isDisabled)
		SELECT MIN(TableName) AS TableName,crmContactGuid,nodeName,crmContactFullname,crmContactFirstName,crmContactLastName,
				crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,crmContactEmailAddress,phgEmployeeCity,phgEmployeeState,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,SystemUserId AS crmContactGuid,FullName AS nodeName,FullName AS crmContactFullname,FirstName AS crmContactFirstName,LastName AS crmContactLastName,
					Title AS crmContactJobTitle,Address1_Telephone1 AS crmContactPhoneNumber,Address1_Fax AS crmContactFaxNumber,DomainName AS crmContactEmailAddress,
					Address1_City AS phgEmployeeCity,Address1_StateOrProvince AS phgEmployeeState,0 AS isDisabled
				FROM LocalCRM.dbo.SystemUser
				WHERE IsDisabled = 0
					UNION
				SELECT 'TARGET' AS TableName,
						CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') AS crmContactGuid,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/@nodeName)[1]','nvarchar(MAX)'), '') AS nodeName,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactFullName)[1]','nvarchar(MAX)'), '') AS crmContactFullname,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactFirstName)[1]','nvarchar(MAX)'), '') AS crmContactFirstName,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactLastName)[1]','nvarchar(MAX)'), '') AS crmContactLastName,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactJobTitle)[1]','nvarchar(MAX)'), '') AS crmContactJobTitle,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmContactPhoneNumber,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactFaxNumber)[1]','nvarchar(MAX)'), '') AS crmContactFaxNumber,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactEmailAddress)[1]','nvarchar(MAX)'), '') AS crmContactEmailAddress,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/phgEmployeeCity)[1]','nvarchar(MAX)'), '') AS phgEmployeeCity,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/phgEmployeeState)[1]','nvarchar(MAX)'), '') AS phgEmployeeState,
						CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/disabled)[1]','BIT') AS isDisabled
				FROM MemberPortal.dbo.cmsContentXml
				WHERE CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
			   ) unioned
		GROUP BY crmContactGuid,nodeName,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,
					crmContactEmailAddress,phgEmployeeCity,phgEmployeeState,isDisabled
		HAVING COUNT(*) = 1;


		INSERT INTO dbo.PhgEmployees(Name,crmContactGuid,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,
												crmContactFaxNumber,crmContactEmailAddress,crmContactDisabled,phgEmployeeCity,phgEmployeeState)
		SELECT crmContactFullname,crmContactGuid,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,
				crmContactFaxNumber,crmContactEmailAddress,0,phgEmployeeCity,phgEmployeeState
		FROM @EmployeeData
		WHERE TableName = 'SOURCE';


		INSERT INTO dbo.PhgEmployees(Name,crmContactGuid,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,
										crmContactEmailAddress,crmContactDisabled,phgEmployeeCity,phgEmployeeState)
		SELECT crmContactFullname,crmContactGuid,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,
				crmContactEmailAddress,1,phgEmployeeCity,phgEmployeeState
		FROM @EmployeeData T
			INNER JOIN MemberPortal.dbo.cmsContentXml ON CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
														AND CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = T.crmContactGuid
														AND CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/disabled)[1]','BIT') = 0
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS (SELECT 1 FROM @EmployeeData S WHERE S.TableName = 'SOURCE' AND T.crmContactGuid = S.crmContactGuid );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
