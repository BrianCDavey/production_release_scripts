USE CmsImport
GO

/*
Run this script on:

        (local)\CHISQZ01.CmsImport    -  This database will be modified

to synchronize it with:

        AZ-SQ-PR-01.CmsImport

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 1/8/2025 4:13:59 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Cities]'
GO


ALTER PROCEDURE [dbo].[mpid_Cities]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.PHG_city)
	BEGIN
		TRUNCATE TABLE dbo.Cities;

		------
		-- Step 1: Retrieve denormalized City data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoCities TABLE (nodeId int
											  , nodeName nvarchar(max)
											  , propertyTypeAlias nvarchar(max)
											  , intValue int
											  , varcharValue nvarchar(max))
		INSERT INTO @normalizedUmbracoCities
		SELECT n.id AS nodeId
			 , n.[text] AS nodeName
			 , pt.[Alias] AS propertyTypeAlias
			 , pd.intValue AS intValue
			 , pd.varcharValue AS varcharvalue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] = 'City'
		AND d.published = 1
		AND cv.[current] = 1
		AND n.trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoCities TABLE (nodeId int
												, nodeName nvarchar(MAX)
												, cityGuid uniqueidentifier
												, cityCountry nvarchar(MAX)
												, cityState nvarchar(MAX)
												, cityCity nvarchar(MAX)
												, [disabled] bit)

		INSERT INTO @denormalizedUmbracoCities
		SELECT nodeId
			 , nodeName
			 , cityGuid
			 , cityCountry
			 , cityState
			 , cityCity
			 , (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoCities n2
				WHERE n2.propertyTypeAlias = 'disabled'
				AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				 , n.nodeName AS nodeName
				 , n.propertyTypeAlias AS propertyTypeAlias
				 , n.intValue AS intValue
				 , n.varcharValue AS varcharValue
			FROM @normalizedUmbracoCities n
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([cityGuid], [cityCountry], [cityState], [cityCity])
		) AS PivotTable
		WHERE COALESCE(cityGuid, cityCountry, cityState, cityCity) IS NOT NULL


		------
		-- Step 2: Merge CRM cities with Umbraco Cities and store in Cities
		------


		DECLARE @CityData TABLE (TableName nvarchar(6),
								 PHG_cityId uniqueidentifier,
								 PHG_name nvarchar(100),
								 PHG_Country nvarchar(100),
								 PHG_StateOrProvince nvarchar(100),
								 PHG_City nvarchar(100),
								 isDisabled bit);

		INSERT INTO @CityData(TableName,PHG_cityId,PHG_name,PHG_Country,PHG_StateOrProvince,PHG_City,isDisabled)
		SELECT MIN(TableName) AS TableName
			 , PHG_cityId
			 , PHG_name
			 , PHG_Country
			 , PHG_StateOrProvince
			 , PHG_City,isDisabled
		FROM (SELECT 'SOURCE' AS TableName
				, PHG_cityId
				, PHG_name
				, phg_countryidname AS PHG_Country
				, phg_stateorprovincename AS PHG_StateOrProvince
				, PHG_name AS PHG_City
				, 0 AS isDisabled
				FROM LocalCRM.dbo.PHG_city
				UNION
				SELECT 'TARGET' AS TableName,
						umb.cityGuid AS PHG_cityId,
						umb.nodeName AS PHG_name,
						umb.cityCountry AS PHG_Country,
						umb.cityState AS PHG_StateOrProvince,
						umb.cityCity AS PHG_City,
						umb.[disabled] AS isDisabled
				FROM @denormalizedUmbracoCities umb
			) unioned
		GROUP BY PHG_cityId, PHG_name, PHG_Country, PHG_StateOrProvince, PHG_City, isDisabled
		HAVING COUNT(*) = 1;


		INSERT INTO dbo.Cities(cityGuid,Name,cityCountry,cityState,cityCity,[disabled])
		SELECT PHG_cityId
			, PHG_name
			, PHG_Country
			, PHG_StateOrProvince
			, PHG_City
			, 0
		FROM @CityData
		WHERE TableName = 'SOURCE';


		INSERT INTO dbo.Cities(cityGuid,Name,cityCountry,cityState,cityCity,[disabled])
		SELECT PHG_cityId
			, PHG_name
			, PHG_Country
			, PHG_StateOrProvince
			, PHG_City
			, 1
		FROM @CityData T
		INNER JOIN @denormalizedUmbracoCities umb
			ON umb.cityGuid = T.PHG_cityId
		WHERE T.TableName = 'TARGET'
		AND umb.[disabled] = 0
		AND NOT EXISTS(SELECT 1
						FROM @CityData S
						WHERE S.TableName = 'SOURCE'
						AND T.PHG_cityId = S.PHG_cityId);

	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_ConsortiaSales]'
GO


ALTER PROCEDURE [dbo].[mpid_ConsortiaSales]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.ConsortiaAccounts;


		------
		-- Step 1: Retrieve denormalized phgEmployee member data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoData TABLE (nodeId int
											, uniqueID uniqueidentifier
											, nodeName nvarchar(max)
											, contentTypeAlias nvarchar(max)
											, propertyTypeAlias nvarchar(max)
											, intValue int
											, varcharValue nvarchar(max))
		INSERT INTO @normalizedUmbracoData
		SELECT n.id AS nodeId
			 , n.uniqueID AS uniqueID
			 , n.[text] AS nodeName
			 , ct.alias AS contentTypeAlias
			 , pt.[Alias] AS propertyTypeAlias
			 , pd.intValue AS intValue
			 , pd.varcharValue AS varcharvalue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] IN ('PhgEmployee', 'Industry', 'ConsortiaAccount')
		  AND d.published = 1
		  AND cv.[current] = 1
		  AND n.trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoConsortia TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, consortiaAccountCrmGuid uniqueidentifier
													, crmAccountName nvarchar(MAX)
													, crmAccountStreetAddressLine1 nvarchar(MAX)
													, crmAccountStreetAddressLine2 nvarchar(MAX)
													, crmAccountCity nvarchar(MAX)
													, crmAccountState nvarchar(MAX)
													, crmAccountCountry nvarchar(MAX)
													, crmAccountPostalCode nvarchar(MAX)
													, crmAccountPhoneNumber nvarchar(MAX)
													, crmAccountEmailAddress nvarchar(MAX)
													, crmAccountWebSite nvarchar(MAX)
													, consortiaAccountIataGlobalAccountManager nvarchar(100)
													, crmAccountIndustry nvarchar(100)
													, crmAccountManagedAccount bit
													, [disabled] bit)

		INSERT INTO @denormalizedUmbracoConsortia
		SELECT nodeId
			, nodeName
			, consortiaAccountCrmGuid
			, crmAccountName
			, crmAccountStreetAddressLine1
			, crmAccountStreetAddressLine2
			, crmAccountCity
			, crmAccountState
			, crmAccountCountry
			, crmAccountPostalCode
			, crmAccountPhoneNumber
			, crmAccountEmailAddress
			, crmAccountWebSite
			, consortiaAccountIataGlobalAccountManager
			, crmAccountIndustry
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'ConsortiaAccount'
				  AND n2.propertyTypeAlias = 'crmAccountManagedAccount'
				  AND n2.nodeId = PivotTable.nodeId) AS [crmAccountManagedAccount]
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'ConsortiaAccount'
			      AND n2.propertyTypeAlias = 'disabled'
				  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'ConsortiaAccount'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN
			 ([consortiaAccountCrmGuid], [crmAccountName], [crmAccountStreetAddressLine1], [crmAccountStreetAddressLine2]
			, [crmAccountCity], [crmAccountState], [crmAccountCountry], [crmAccountPostalCode]
			, [crmAccountPhoneNumber], [crmAccountEmailAddress], [crmAccountWebSite]
			, [consortiaAccountIataGlobalAccountManager], [crmAccountIndustry])
		) AS PivotTable
		WHERE COALESCE(consortiaAccountCrmGuid, crmAccountName) IS NOT NULL


		DECLARE @denormalizedUmbracoIndustries TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, industryKey uniqueidentifier
													, industryId int
													, industryName nvarchar(MAX))

		INSERT INTO @denormalizedUmbracoIndustries
		SELECT nodeId
			 , nodeName
			 , uniqueID AS industryKey
			 , industryId
			 , industryName
		FROM (
			SELECT n.nodeId AS nodeId
				 , n.nodeName AS nodeName
				 , n.uniqueID AS uniqueID
				 , n.propertyTypeAlias AS propertyTypeAlias
				 , n.intValue AS intValue
				 , n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'Industry'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([industryId], [industryName])
		) AS PivotTable
		WHERE COALESCE(industryId, industryName) IS NOT NULL


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoPhgEmployee TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, employeeKey uniqueidentifier
													, crmContactGuid uniqueidentifier)

		INSERT INTO @denormalizedUmbracoPhgEmployee
		SELECT nodeId
			, nodeName
			, uniqueID as employeeKey
			, crmContactGuid
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.uniqueID AS uniqueID
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'PhgEmployee'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([crmContactGuid])
		) AS PivotTable
		WHERE crmContactGuid IS NOT NULL

		------
		-- Step 2: Merge CRM data with Umbraco members and store in ConsortiaAccounts
		------

		DECLARE @ConsortiaData TABLE(TableName nvarchar(6),
										nodeName nvarchar(100),
										crmAccountName nvarchar(100),
										consortiaAccountCrmGUID uniqueidentifier,
										crmAccountStreetAddressLine1 nvarchar(100),
										crmAccountStreetAddressLine2 nvarchar(100),
										crmAccountCity nvarchar(100),
										crmAccountState nvarchar(100),
										crmAccountCountry nvarchar(100),
										crmAccountPostalCode nvarchar(100),
										crmAccountPhoneNumber nvarchar(100),
										crmAccountEmailAddress nvarchar(100),
										crmAccountWebSite nvarchar(100),
										crmAccountManagedAccount bit,
										consortiaAccountIataGlobalAccountManager nvarchar(100),
										accountIndustry nvarchar(100),
										isDisabled bit);

		INSERT INTO @ConsortiaData(TableName,nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
									crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
									consortiaAccountIataGlobalAccountManager,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName
			  ,nodeName
			  ,crmAccountName
			  ,consortiaAccountCrmGUID
			  ,crmAccountStreetAddressLine1
			  ,crmAccountStreetAddressLine2
			  ,crmAccountCity
			  ,crmAccountState
			  ,crmAccountCountry
			  ,crmAccountPostalCode
			  ,crmAccountPhoneNumber
			  ,crmAccountEmailAddress
			  ,crmAccountWebSite
			  ,crmAccountManagedAccount
			  ,consortiaAccountIataGlobalAccountManager
			  ,accountIndustry
			  ,isDisabled
		FROM (SELECT 'SOURCE' AS TableName
					,LTRIM(RTRIM(account.[Name])) AS nodeName
					,LTRIM(RTRIM(account.[Name])) AS crmAccountName
					,UPPER(account.AccountID) AS consortiaAccountCrmGUID
					,ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1
					,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2
					,ISNULL(Address1_City, '') AS crmAccountCity
					,ISNULL(Address1_StateOrProvince, '') AS crmAccountState
					,ISNULL(Address1_Country, '') AS crmAccountCountry
					,ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode
					,ISNULL(Telephone1, '') AS crmAccountPhoneNumber
					,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress
					,ISNULL(WebSiteURL, '') AS crmAccountWebSite
					,CASE account.phg_consortiasalesstatus WHEN 100000002 THEN 1 ELSE 0 END AS crmAccountManagedAccount
					,'umb://document/' + REPLACE(umbracoIataGlobalAccountManager.employeeKey, '-', '') AS consortiaAccountIataGlobalAccountManager
					,'umb://document/' + REPLACE(umbracoIndustry.industryKey, '-', '') AS accountIndustry
					,0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.phg_consortiasalesstatus,ISNULL(phg_showinmemberportalconsortia, 0) AS PHG_ShowinMemberPortalConsortia,
								phg_iataglobalaccountmanagerid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_consortiasalesstatus IN (100000000,100000002)
							AND Account.phg_consortiasales = 1
							AND Account.phg_showinmemberportalconsortia = 1
					  ) account
					LEFT JOIN @denormalizedUmbracoPhgEmployee as umbracoIataGlobalAccountManager
						ON umbracoIataGlobalAccountManager.crmContactGuid = account.phg_iataglobalaccountmanagerid
					LEFT JOIN @denormalizedUmbracoIndustries as umbracoIndustry
						ON umbracoIndustry.industryId = account.IndustryCode
					UNION
				SELECT 'TARGET' AS TableName
					  ,umb.nodeName AS nodeName
					  ,umb.crmAccountName AS crmAccountName
					  ,umb.consortiaAccountCrmGuid AS consortiaAccountCrmGuid
					  ,ISNULL(umb.crmAccountStreetAddressLine1, '') AS crmAccountStreetAddressLine1
					  ,ISNULL(umb.crmAccountStreetAddressLine2, '') AS crmAccountStreetAddressLine2
					  ,ISNULL(umb.crmAccountCity, '') AS crmAccountCity
					  ,ISNULL(umb.crmAccountState, '') AS crmAccountState
					  ,ISNULL(umb.crmAccountCountry, '') AS crmAccountCountry
					  ,ISNULL(umb.crmAccountPostalCode, '') AS crmAccountPostalCode
					  ,ISNULL(umb.crmAccountPhoneNumber, '') AS crmAccountPhoneNumber
					  ,ISNULL(umb.crmAccountEmailAddress, '') AS crmAccountEmailAddress
					  ,ISNULL(umb.crmAccountWebSite, '') AS crmAccountWebSite
					  ,umb.crmAccountManagedAccount AS crmAccountManagedAccount
					  ,umb.consortiaAccountIataGlobalAccountManager AS consortiaAccountIataGlobalAccountManager
					  ,umb.crmAccountIndustry AS accountIndustry
					  ,umb.[disabled] AS isDisabled
				FROM @denormalizedUmbracoConsortia umb
				WHERE umb.[disabled] = 0
			  ) unioned
		GROUP BY nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,consortiaAccountIataGlobalAccountManager,
				accountIndustry,isDisabled
		HAVING COUNT(*) = 1;

		--SELECT * FROM @ConsortiaData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.ConsortiaAccounts(Name,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
											crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
											consortiaAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,0,accountIndustry
		FROM @ConsortiaData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.ConsortiaAccounts(Name,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
											crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
											consortiaAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,1,accountIndustry
		FROM @ConsortiaData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @ConsortiaData S WHERE S.TableName = 'SOURCE' AND T.consortiaAccountCrmGUID = S.consortiaAccountCrmGUID );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_CorporateAccounts]'
GO



ALTER PROCEDURE [dbo].[mpid_CorporateAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.CorporateAccounts;

		------
		-- Step 1: Retrieve denormalized data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoData TABLE (nodeId int
											, uniqueID uniqueidentifier
											, nodeName nvarchar(max)
											, contentTypeAlias nvarchar(max)
											, propertyTypeAlias nvarchar(max)
											, intValue int
											, varcharValue nvarchar(max))
		INSERT INTO @normalizedUmbracoData
		SELECT n.id AS nodeId
			 , n.uniqueID AS uniqueID
			 , n.[text] AS nodeName
			 , ct.alias AS contentTypeAlias
			 , pt.[Alias] AS propertyTypeAlias
			 , pd.intValue AS intValue
			 , pd.varcharValue AS varcharvalue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] IN ('PhgEmployee', 'Industry', 'corporateAccount')
		  AND d.published = 1
		  AND cv.[current] = 1
		  AND n.trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoCorporate TABLE ( nodeId int
													, nodeName nvarchar(MAX)
													, corporateAccountCrmGuid uniqueidentifier
													, crmAccountName nvarchar(MAX)
													, crmAccountStreetAddressLine1 nvarchar(MAX)
													, crmAccountStreetAddressLine2 nvarchar(MAX)
													, crmAccountCity nvarchar(MAX)
													, crmAccountState nvarchar(MAX)
													, crmAccountCountry nvarchar(MAX)
													, crmAccountPostalCode nvarchar(MAX)
													, crmAccountPhoneNumber nvarchar(MAX)
													, crmAccountEmailAddress nvarchar(MAX)
													, crmAccountWebSite nvarchar(MAX)
													, crmAccountSalesStatus nvarchar(MAX)
													, corporateAccountGlobalTeamLead nvarchar(100)
													, corporateAccountRegionalTeamLeadAmericas nvarchar(100)
													, corporateAccountRegionalTeamLeadASPAC nvarchar(100)
													, corporateAccountRegionalTeamLeadEMEA nvarchar(100)
													, crmAccountIndustry nvarchar(100)
													, [disabled] bit)

		INSERT INTO @denormalizedUmbracoCorporate
		SELECT nodeId
			, nodeName
			, corporateAccountCrmGuid
			, crmAccountName
			, crmAccountStreetAddressLine1
			, crmAccountStreetAddressLine2
			, crmAccountCity
			, crmAccountState
			, crmAccountCountry
			, crmAccountPostalCode
			, crmAccountPhoneNumber
			, crmAccountEmailAddress
			, crmAccountWebSite
			, crmAccountSalesStatus
			, corporateAccountGlobalTeamLead
			, corporateAccountRegionalTeamLeadAmericas
			, corporateAccountRegionalTeamLeadASPAC
			, corporateAccountRegionalTeamLeadEMEA
			, crmAccountIndustry
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'corporateAccount'
			      AND n2.propertyTypeAlias = 'disabled'
				  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'corporateAccount'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN
			 ([corporateAccountCrmGuid], [crmAccountName], [crmAccountStreetAddressLine1], [crmAccountStreetAddressLine2]
			, [crmAccountCity], [crmAccountState], [crmAccountCountry], [crmAccountPostalCode]
			, [crmAccountPhoneNumber], [crmAccountEmailAddress], [crmAccountWebSite], [crmAccountSalesStatus]
			, [corporateAccountGlobalTeamLead], [corporateAccountRegionalTeamLeadAmericas]
			, [corporateAccountRegionalTeamLeadASPAC], [corporateAccountRegionalTeamLeadEMEA], [crmAccountIndustry])
		) AS PivotTable
		WHERE COALESCE(corporateAccountCrmGuid, crmAccountName) IS NOT NULL AND corporateAccountCrmGuid <> ''


		DECLARE @denormalizedUmbracoIndustries TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, industryKey uniqueidentifier
													, industryId int
													, industryName nvarchar(MAX))

		INSERT INTO @denormalizedUmbracoIndustries
		SELECT nodeId
			 , nodeName
			 , uniqueID AS industryKey
			 , industryId
			 , industryName
		FROM (
			SELECT n.nodeId AS nodeId
				 , n.nodeName AS nodeName
				 , n.uniqueID AS uniqueID
				 , n.propertyTypeAlias AS propertyTypeAlias
				 , n.intValue AS intValue
				 , n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'Industry'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([industryId], [industryName])
		) AS PivotTable
		WHERE COALESCE(industryId, industryName) IS NOT NULL


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoPhgEmployee TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, employeeKey uniqueidentifier
													, crmContactGuid uniqueidentifier)

		INSERT INTO @denormalizedUmbracoPhgEmployee
		SELECT nodeId
			, nodeName
			, uniqueID as employeeKey
			, crmContactGuid
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.uniqueID AS uniqueID
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'PhgEmployee'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([crmContactGuid])
		) AS PivotTable
		WHERE crmContactGuid IS NOT NULL

		------
		-- Step 2: Merge CRM data with Umbraco members and store in CorporateAccounts
		------

		DECLARE @CorporateAccountData TABLE(TableName nvarchar(6),
											nodeName nvarchar(100),
											crmAccountName nvarchar(100),
											corporateAccountCrmGUID uniqueidentifier,
											crmAccountStreetAddressLine1 nvarchar(100),
											crmAccountStreetAddressLine2 nvarchar(100),
											crmAccountCity nvarchar(100),
											crmAccountState nvarchar(100),
											crmAccountCountry nvarchar(100),
											crmAccountPostalCode nvarchar(100),
											crmAccountPhoneNumber nvarchar(100),
											crmAccountEmailAddress nvarchar(100),
											crmAccountWebSite nvarchar(200),
											crmAccountSalesStatus nvarchar(25),
											corporateAccountGlobalTeamLead nvarchar(100),
											corporateAccountRegionalTeamLeadAmericas nvarchar(100),
											corporateAccountRegionalTeamLeadASPAC nvarchar(100),
											corporateAccountRegionalTeamLeadEMEA nvarchar(100),
											accountIndustry nvarchar(100),
											isDisabled bit);

		INSERT INTO @CorporateAccountData(TableName,nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,
											crmAccountCity,crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
											crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,
											corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,
				crmAccountCity,crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,
				corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName
					,account.[Name] AS nodeName
					,account.[Name] AS crmAccountName
					,UPPER(account.AccountID) AS corporateAccountCrmGUID
					,ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1
					,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2
					,ISNULL(Address1_City, '') AS crmAccountCity
					,ISNULL(Address1_StateOrProvince, '') AS crmAccountState
					,ISNULL(Address1_Country, '') AS crmAccountCountry
					,ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode
					,ISNULL(Telephone1, '') AS crmAccountPhoneNumber
					,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress
					,ISNULL(WebSiteURL, '') AS crmAccountWebSite
					,ISNULL(COALESCE(phg_corporatesalesstatusname, 'UNKNOWN'), '') AS crmAccountSalesStatus
					,'umb://document/' + REPLACE(umbracoLeadGlobal.employeeKey, '-', '') AS corporateAccountGlobalTeamLead
					,'umb://document/' + REPLACE(umbracoLeadAmericas.employeeKey, '-', '') AS corporateAccountRegionalTeamLeadAmericas
					,'umb://document/' + REPLACE(umbracoLeadASPAC.employeeKey, '-', '') AS corporateAccountRegionalTeamLeadASPAC
					,'umb://document/' + REPLACE(umbracoLeadEMEA.employeeKey, '-', '') AS corporateAccountRegionalTeamLeadEMEA
					,'umb://document/' + REPLACE(umbracoIndustry.industryKey, '-', '') AS accountIndustry
					,0 AS isDisabled
				FROM (SELECT DISTINCT [Name]
									 ,account.AccountID
									 ,Address1_Line1
									 ,Address1_Line2
									 ,Address1_City
									 ,Address1_StateOrProvince
									 ,Address1_Country
									 ,Address1_PostalCode
									 ,Telephone1
									 ,EMailAddress1
									 ,WebSiteURL
									 ,account.PHG_CorporateSalesStatus
									 ,account.PHG_CorporateSalesStatusName
									 ,phg_corporateglobalaccountmanagerid
									 ,phg_corporatermamericasid
									 ,phg_corporatermaspacid
									 ,phg_corporatermemeaid
									 ,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND account.phg_corporateaccount = 1
							AND account.PHG_ShowinMemberPortalCorporateSales = 1) account
				LEFT OUTER JOIN @denormalizedUmbracoPhgEmployee as umbracoLeadGlobal
					ON umbracoLeadGlobal.crmContactGuid = account.phg_corporateglobalaccountmanagerid
				LEFT OUTER JOIN @denormalizedUmbracoPhgEmployee as umbracoLeadAmericas
					ON umbracoLeadAmericas.crmContactGuid = account.phg_corporatermamericasid
				LEFT OUTER JOIN @denormalizedUmbracoPhgEmployee as umbracoLeadASPAC
					ON umbracoLeadASPAC.crmContactGuid = account.phg_corporatermaspacid
				LEFT OUTER JOIN @denormalizedUmbracoPhgEmployee as umbracoLeadEMEA
					ON umbracoLeadEMEA.crmContactGuid = account.phg_corporatermemeaid
				LEFT OUTER JOIN @denormalizedUmbracoIndustries as umbracoIndustry
					ON umbracoIndustry.industryId = account.IndustryCode

				UNION

				SELECT 'TARGET' AS TableName
					  ,umb.nodeName AS nodeName
					  ,umb.crmAccountName AS crmAccountName
					  ,umb.corporateAccountCrmGuid AS corporateAccountCrmGuid
					  ,ISNULL(umb.crmAccountStreetAddressLine1, '') AS crmAccountStreetAddressLine1
					  ,ISNULL(umb.crmAccountStreetAddressLine2, '') AS crmAccountStreetAddressLine2
					  ,ISNULL(umb.crmAccountCity, '') AS crmAccountCity
					  ,ISNULL(umb.crmAccountState, '') AS crmAccountState
					  ,ISNULL(umb.crmAccountCountry, '') AS crmAccountCountry
					  ,ISNULL(umb.crmAccountPostalCode, '') AS crmAccountPostalCode
					  ,ISNULL(umb.crmAccountPhoneNumber, '') AS crmAccountPhoneNumber
					  ,ISNULL(umb.crmAccountEmailAddress, '') AS crmAccountEmailAddress
					  ,ISNULL(umb.crmAccountWebSite, '') AS crmAccountWebSite
					  ,ISNULL(umb.crmAccountSalesStatus, '') AS crmAccountSalesStatus
					  ,umb.corporateAccountGlobalTeamLead AS corporateAccountGlobalTeamLead
					  ,umb.corporateAccountRegionalTeamLeadAmericas AS corporateAccountRegionalTeamLeadAmericas
					  ,umb.corporateAccountRegionalTeamLeadASPAC AS corporateAccountRegionalTeamLeadASPAC
					  ,umb.corporateAccountRegionalTeamLeadEMEA AS corporateAccountRegionalTeamLeadEMEA
					  ,umb.crmAccountIndustry AS accountIndustry
					  ,umb.[disabled] AS isDisabled
				 FROM @denormalizedUmbracoCorporate umb
				) unioned
		GROUP BY nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountSalesStatus,corporateAccountGlobalTeamLead,
				corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @CorporateAccountData ORDER BY nodeName,TableName


		INSERT INTO CmsImport.dbo.CorporateAccounts(Name,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
													crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
													crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
													corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
				corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,0,accountIndustry
		FROM @CorporateAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.CorporateAccounts(Name,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
													crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
													crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
													corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
				corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,1,accountIndustry
		FROM @CorporateAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @CorporateAccountData S WHERE S.TableName = 'SOURCE' AND T.corporateAccountCrmGUID = S.corporateAccountCrmGUID );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_DirectoryMembers]'
GO



ALTER PROCEDURE [dbo].[mpid_DirectoryMembers]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	------
	-- Step 1: Retrieve denormalized data from Umbraco
	------

	-- Store normalized Umbraco property data
	DECLARE @normalizedUmbracoData TABLE (nodeId int
										, uniqueID uniqueidentifier
										, nodeName nvarchar(max)
										, contentTypeAlias nvarchar(max)
										, propertyTypeAlias nvarchar(max)
										, intValue int
										, varcharValue nvarchar(max))
	INSERT INTO @normalizedUmbracoData
	SELECT n.id AS nodeId
			, n.uniqueID AS uniqueID
			, n.[text] AS nodeName
			, ct.alias AS contentTypeAlias
			, pt.[Alias] AS propertyTypeAlias
			, pd.intValue AS intValue
			, pd.varcharValue AS varcharvalue
	FROM MemberPortal.dbo.umbracoNode n
	LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
		ON cv.nodeId = n.id
	LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
		ON cv.id = dv.id
	INNER JOIN MemberPortal.dbo.umbracoContent c
		ON c.nodeId = n.id
	INNER JOIN MemberPortal.dbo.cmsContentType ct
		ON ct.nodeId = c.contentTypeId
	LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
		ON d.nodeId = c.nodeId
	LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
		ON pd.versionId = cv.id
	INNER JOIN MemberPortal.dbo.cmsPropertyType pt
		ON pt.id = pd.propertytypeid
	WHERE ct.[alias] IN ('Hotel', 'HotelMember')
		AND (d.published IS NULL OR d.published = 1)
		AND cv.[current] = 1
		AND n.trashed = 0
	ORDER BY n.id DESC, pt.[Alias], versionDate DESC


	-- Store denormalize the Umbraco property data
	DECLARE @denormalizedUmbracoHotel TABLE ( nodeId int
											, hotelKey uniqueidentifier
											, nodeName nvarchar(MAX)
											, hotelName nvarchar(MAX)
											, hotelCode nvarchar(MAX)
											, [disabled] bit)

	INSERT INTO @denormalizedUmbracoHotel
	SELECT nodeId
		, uniqueID as hotelKey
		, nodeName
		, hotelName
		, hotelCode
		, (SELECT n2.intValue AS intValue
			FROM @normalizedUmbracoData n2
			WHERE n2.contentTypeAlias = 'Hotel'
			    AND n2.propertyTypeAlias = 'disabled'
				AND n2.nodeId = PivotTable.nodeId) AS [disabled]
	FROM (
		SELECT n.nodeId AS nodeId
			,n.uniqueID AS uniqueID
			,n.nodeName AS nodeName
			,n.propertyTypeAlias AS propertyTypeAlias
			,n.intValue AS intValue
			,n.varcharValue AS varcharValue
		FROM @normalizedUmbracoData n
		WHERE n.contentTypeAlias = 'Hotel'
	) AS SourceTable
	PIVOT (
		MAX(varcharValue) FOR propertyTypeAlias IN ([hotelName], [hotelCode])
	) AS PivotTable
	WHERE COALESCE(hotelName, hotelCode) IS NOT NULL


	DECLARE @denormalizedUmbracoHotelMember TABLE ( nodeId int
												, memberKey uniqueidentifier
												, nodeName nvarchar(MAX)
												, loginName nvarchar(MAX)
												, defaultHotel nvarchar(MAX)
												, hotels nvarchar(MAX)
												, firstName nvarchar(MAX)
												, lastName nvarchar(MAX)
												, title nvarchar(MAX)
												, umbracoMemberApproved bit)

	INSERT INTO @denormalizedUmbracoHotelMember
	SELECT nodeId
		, uniqueID as memberKey
		, nodeName
		, loginName
		, defaultHotel
		, hotels
		, firstName
		, lastName
		, title
		, umbracoMemberApproved
	FROM (
		SELECT n.nodeId AS nodeId
			,n.uniqueID AS uniqueID
			,n.nodeName AS nodeName
			,m.loginName AS loginName
			,m.isApproved AS umbracoMemberApproved
			,n.propertyTypeAlias AS propertyTypeAlias
			,n.intValue AS intValue
			,n.varcharValue AS varcharValue
		FROM @normalizedUmbracoData n
		INNER JOIN MemberPortal.dbo.cmsMember m
			ON m.nodeId = n.nodeId
		WHERE n.contentTypeAlias = 'PHRMember'
	) AS SourceTable
	PIVOT (
		MAX(varcharValue) FOR propertyTypeAlias IN
			([defaultHotel], [hotels], [firstName], [lastName], [title])
	) AS PivotTable
	WHERE COALESCE([defaultHotel], [hotels], [firstName], [lastName], [title]) IS NOT NULL

	------
	-- Step 2: Merge umbraco Hotel Member data with associated Hotels and store in DirectoryMembers
	------


	DECLARE @Hotels TABLE(hotelId int, hotelUdi nvarchar(max),hotelName nvarchar(1000));

	INSERT INTO @Hotels(hotelId, hotelUdi,hotelName)
	SELECT umb.nodeId AS hotelId
		 , 'umb://document/' + REPLACE(umb.hotelKey, '-', '') AS hotelUdi
		 , umb.hotelName AS hotelName
	FROM @denormalizedUmbracoHotel umb
	WHERE umb.[disabled] = 0
		
		
	DECLARE @Members TABLE(nodeID int,
							firstName nvarchar(1000),
							lastName nvarchar(1000),
							title nvarchar(1000),
							defaultHotelId nvarchar(1000),
							defaultHotelName nvarchar(1000),
							hotels nvarchar(max)
						   );


	INSERT INTO @Members(nodeID,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels)
	SELECT umb.nodeId AS nodeID
		  ,umb.firstName AS firstName
		  ,umb.lastName AS lastName
		  ,umb.title AS title
		  ,H.hotelId AS defaultHotelId
		  ,H.hotelName AS defaultHotelName
		  ,CASE WHEN H.hotelId IS NOT NULL THEN umb.hotels ELSE NULL END AS hotels
	FROM @denormalizedUmbracoHotelMember umb
	LEFT OUTER JOIN @Hotels H
		ON umb.defaultHotel = H.hotelUdi
	WHERE umb.umbracoMemberApproved = 1
	  AND (umb.firstName IS NOT NULL OR umb.lastName IS NOT NULL);


	DECLARE @MemberCount int;
	SELECT @MemberCount = COUNT(*) FROM @Members


	IF @MemberCount > 0
	BEGIN

		TRUNCATE TABLE dbo.DirectoryMembers;

		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.firstName,M.lastName,M.title,M.defaultHotelName) AS id,'firstname' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY firstName,lastName,title,defaultHotelName



		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.lastName,M.firstName,M.title,M.defaultHotelName) AS id,'lastname' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY lastName,firstName,title,defaultHotelName



		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.title,M.firstName,M.lastName,M.defaultHotelName) AS id,'title' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY title,firstName,lastName,defaultHotelName



		INSERT INTO dbo.DirectoryMembers(id,sortedBy,nodeId,firstName,lastName,title,defaultHotelId,defaultHotelName,hotels,totalRecords)
		SELECT ROW_NUMBER() OVER(ORDER BY M.defaultHotelName,M.firstName,M.lastName,M.title) AS id,'hotel' AS sortedBy,M.nodeID,M.firstName,M.lastName,M.title,
				M.defaultHotelId,M.defaultHotelName,M.hotels,@MemberCount AS totalRecords
		FROM @Members M
		ORDER BY defaultHotelName,firstName,lastName,title
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_GroupAccounts]'
GO


ALTER PROCEDURE [dbo].[mpid_GroupAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN

		TRUNCATE TABLE CmsImport.dbo.GroupAccounts;


		------
		-- Step 1: Retrieve denormalized data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoData TABLE (nodeId int
											, uniqueID uniqueidentifier
											, nodeName nvarchar(max)
											, contentTypeAlias nvarchar(max)
											, propertyTypeAlias nvarchar(max)
											, intValue int
											, varcharValue nvarchar(max))
		INSERT INTO @normalizedUmbracoData
		SELECT n.id AS nodeId
			 , n.uniqueID AS uniqueID
			 , n.[text] AS nodeName
			 , ct.alias AS contentTypeAlias
			 , pt.[Alias] AS propertyTypeAlias
			 , pd.intValue AS intValue
			 , pd.varcharValue AS varcharvalue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] IN ('PhgEmployee', 'Industry', 'GroupAccount')
		  AND d.published = 1
		  AND cv.[current] = 1
		  AND n.trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoGroupAccount TABLE ( nodeId int
													, nodeName nvarchar(MAX)
													, groupAccountCrmGuid uniqueidentifier
													, crmAccountName nvarchar(MAX)
													, crmAccountStreetAddressLine1 nvarchar(MAX)
													, crmAccountStreetAddressLine2 nvarchar(MAX)
													, crmAccountCity nvarchar(MAX)
													, crmAccountState nvarchar(MAX)
													, crmAccountCountry nvarchar(MAX)
													, crmAccountPostalCode nvarchar(MAX)
													, crmAccountPhoneNumber nvarchar(MAX)
													, crmAccountEmailAddress nvarchar(MAX)
													, crmAccountWebSite nvarchar(MAX)
													, groupAccountSalesDirector nvarchar(MAX)
													, crmAccountIndustry nvarchar(100)
													, crmAccountManagedAccount bit
													, [disabled] bit)

		INSERT INTO @denormalizedUmbracoGroupAccount
		SELECT nodeId
			, nodeName
			, groupAccountCrmGuid
			, crmAccountName
			, crmAccountStreetAddressLine1
			, crmAccountStreetAddressLine2
			, crmAccountCity
			, crmAccountState
			, crmAccountCountry
			, crmAccountPostalCode
			, crmAccountPhoneNumber
			, crmAccountEmailAddress
			, crmAccountWebSite
			, groupAccountSalesDirector
			, crmAccountIndustry
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'GroupAccount'
			      AND n2.propertyTypeAlias = 'crmAccountManagedAccount'
				  AND n2.nodeId = PivotTable.nodeId) AS [crmAccountManagedAccount]
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'GroupAccount'
			      AND n2.propertyTypeAlias = 'disabled'
				  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'GroupAccount'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN
			 ([groupAccountCrmGuid], [crmAccountName], [crmAccountStreetAddressLine1], [crmAccountStreetAddressLine2]
			, [crmAccountCity], [crmAccountState], [crmAccountCountry], [crmAccountPostalCode]
			, [crmAccountPhoneNumber], [crmAccountEmailAddress], [crmAccountWebSite], [groupAccountSalesDirector]
			, [crmAccountIndustry])
		) AS PivotTable
		WHERE COALESCE(groupAccountCrmGuid, crmAccountName) IS NOT NULL


		DECLARE @denormalizedUmbracoIndustries TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, industryKey uniqueidentifier
													, industryId int
													, industryName nvarchar(MAX))

		INSERT INTO @denormalizedUmbracoIndustries
		SELECT nodeId
			 , nodeName
			 , uniqueID AS industryKey
			 , industryId
			 , industryName
		FROM (
			SELECT n.nodeId AS nodeId
				 , n.nodeName AS nodeName
				 , n.uniqueID AS uniqueID
				 , n.propertyTypeAlias AS propertyTypeAlias
				 , n.intValue AS intValue
				 , n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'Industry'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([industryId], [industryName])
		) AS PivotTable
		WHERE COALESCE(industryId, industryName) IS NOT NULL


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoPhgEmployee TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, employeeKey uniqueidentifier
													, crmContactGuid uniqueidentifier)

		INSERT INTO @denormalizedUmbracoPhgEmployee
		SELECT nodeId
			, nodeName
			, uniqueID as employeeKey
			, crmContactGuid
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.uniqueID AS uniqueID
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'PhgEmployee'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([crmContactGuid])
		) AS PivotTable
		WHERE crmContactGuid IS NOT NULL

		------
		-- Step 2: Merge CRM data with Umbraco members and store in GroupAccounts
		------


		DECLARE @GroupAccountData TABLE(TableName nvarchar(6),
										nodeName nvarchar(100),
										crmAccountName nvarchar(100),
										groupAccountCrmGUID uniqueidentifier,
										crmAccountStreetAddressLine1 nvarchar(100),
										crmAccountStreetAddressLine2 nvarchar(100),
										crmAccountCity nvarchar(100),
										crmAccountState nvarchar(100),
										crmAccountCountry nvarchar(100),
										crmAccountPostalCode nvarchar(100),
										crmAccountPhoneNumber nvarchar(100),
										crmAccountEmailAddress nvarchar(100),
										crmAccountWebSite nvarchar(100),
										crmAccountManagedAccount bit,
										groupAccountSalesDirector nvarchar(100),
										accountIndustry nvarchar(100),
										isDisabled bit);

		INSERT INTO @GroupAccountData(TableName,nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
										crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
										crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName
					,account.[Name] AS nodeName
					,account.[Name] AS crmAccountName
					,UPPER(account.AccountID) AS groupAccountCrmGUID
					,ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1
					,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2
					,ISNULL(Address1_City, '') AS crmAccountCity
					,ISNULL(Address1_StateOrProvince, '') AS crmAccountState
					,ISNULL(Address1_Country, '') AS crmAccountCountry
					,ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode
					,ISNULL(Telephone1, '') AS crmAccountPhoneNumber
					,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress
					,ISNULL(WebSiteURL, '') AS crmAccountWebSite
					,CASE account.phg_groupsalesstatus WHEN 100000002 THEN 'True' ELSE 'False' END AS crmAccountManagedAccount
					,'umb://document/' + REPLACE(umbracoGroupSales.employeeKey, '-', '') AS groupAccountSalesDirector
					,'umb://document/' + REPLACE(umbracoIndustry.industryKey, '-', '') AS accountIndustry
					,0 AS isDisabled
				FROM (SELECT DISTINCT [Name]
									 ,account.AccountID
									 ,Address1_Line1
									 ,Address1_Line2
									 ,Address1_City
									 ,Address1_StateOrProvince
									 ,Address1_Country
									 ,Address1_PostalCode
									 ,Telephone1
									 ,EMailAddress1
									 ,WebSiteURL
									 ,account.phg_groupsalesstatus
									 ,phg_groupsalesdirectorid
									 ,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_groupsalesstatus IN (100000001,100000002)
							AND account.phg_groupaccount = 1
							AND Account.phg_showinmemberportalgroupsales = 1) account
				LEFT OUTER JOIN @denormalizedUmbracoPhgEmployee as umbracoGroupSales
					ON umbracoGroupSales.crmContactGuid = account.phg_groupsalesdirectorid
				LEFT OUTER JOIN @denormalizedUmbracoIndustries as umbracoIndustry
					ON umbracoIndustry.industryId = account.IndustryCode

				UNION

				SELECT 'TARGET' AS TableName
					  ,umb.nodeName AS nodeName
					  ,umb.crmAccountName AS crmAccountName
					  ,umb.groupAccountCrmGuid AS groupAccountCrmGUID
					  ,ISNULL(umb.crmAccountStreetAddressLine1, '') AS crmAccountStreetAddressLine1
					  ,ISNULL(umb.crmAccountStreetAddressLine2, '') AS crmAccountStreetAddressLine2
					  ,ISNULL(umb.crmAccountCity, '') AS crmAccountCity
					  ,ISNULL(umb.crmAccountState, '') AS crmAccountState
					  ,ISNULL(umb.crmAccountCountry, '') AS crmAccountCountry
					  ,ISNULL(umb.crmAccountPostalCode, '') AS crmAccountPostalCode
					  ,ISNULL(umb.crmAccountPhoneNumber, '') AS crmAccountPhoneNumber
					  ,ISNULL(umb.crmAccountEmailAddress, '') AS crmAccountEmailAddress
					  ,ISNULL(umb.crmAccountWebSite, '') AS crmAccountWebSite
					  ,umb.crmAccountManagedAccount AS crmAccountManagedAccount
					  ,umb.groupAccountSalesDirector AS groupAccountSalesDirector
					  ,umb.crmAccountIndustry AS accountIndustry
					  ,umb.[disabled] AS isDisabled
				FROM @denormalizedUmbracoGroupAccount umb
			 ) unioned
		GROUP BY nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @GroupAccountData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.GroupAccounts(Name,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
										crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
										groupAccountSalesDirector,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				groupAccountSalesDirector,0,accountIndustry
		FROM @GroupAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.GroupAccounts(Name,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
										crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
										groupAccountSalesDirector,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				groupAccountSalesDirector,1,accountIndustry
		FROM @GroupAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @GroupAccountData S WHERE S.TableName = 'SOURCE' AND T.groupAccountCrmGUID = S.groupAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Hotels]'
GO


ALTER PROCEDURE [dbo].[mpid_Hotels]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE dbo.Hotels;



	------
	-- Step 1: Retrieve denormalized data from Umbraco
	------

	-- Store normalized Umbraco property data
	DECLARE @normalizedUmbracoData TABLE (nodeId int
										, uniqueID uniqueidentifier
										, nodeName nvarchar(max)
										, contentTypeAlias nvarchar(max)
										, propertyTypeAlias nvarchar(max)
										, intValue int
										, varcharValue nvarchar(max))
	INSERT INTO @normalizedUmbracoData
	SELECT n.id AS nodeId
		 , n.uniqueID AS uniqueID
		 , n.[text] AS nodeName
		 , ct.alias AS contentTypeAlias
		 , pt.[Alias] AS propertyTypeAlias
		 , pd.intValue AS intValue
		 , pd.varcharValue AS varcharvalue
	FROM MemberPortal.dbo.umbracoNode n
	LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
		ON cv.nodeId = n.id
	LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
		ON cv.id = dv.id
	INNER JOIN MemberPortal.dbo.umbracoContent c
		ON c.nodeId = n.id
	INNER JOIN MemberPortal.dbo.cmsContentType ct
		ON ct.nodeId = c.contentTypeId
	LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
		ON d.nodeId = c.nodeId
	LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
		ON pd.versionId = cv.id
	INNER JOIN MemberPortal.dbo.cmsPropertyType pt
		ON pt.id = pd.propertytypeid
	WHERE ct.[alias] IN ('Hotel', 'PhgEmployee')
	  AND d.published = 1
	  AND cv.[current] = 1
	  AND n.trashed = 0
	ORDER BY n.id DESC, pt.[Alias], versionDate DESC


	-- Store denormalize the Umbraco property data
	DECLARE @denormalizedUmbracoHotels TABLE (nodeId int
											, nodeName nvarchar(MAX)
											, hotelCrmGuid uniqueidentifier
											, hotelCode nvarchar(MAX)
											, hotelName nvarchar(MAX)
											, hotelCity nvarchar(MAX)
											, hotelState nvarchar(MAX)
											, hotelCountry nvarchar(MAX)
											, hotelRegion nvarchar(MAX)
											, hotelContractedCurrency nvarchar(MAX)
											, hotelCollectionCodes nvarchar(MAX)
											, hotelMainCollection nvarchar(MAX)
											, hotelRateGainID nvarchar(MAX)
											, hotelAccountDirector nvarchar(100)
											, hotelMarketingContact nvarchar(100)
											, hotelRegionalDirector nvarchar(100)
											, hotelAreaManagingDirector nvarchar(100)
										    , hotelRevenueAccountManager nvarchar(100)
										    , hotelAccountsReceivableContact nvarchar(100)
										    , hotelIPreferEngagementSpecialist nvarchar(100)
										    , hotelExecutiveVicePresident nvarchar(100)
										    , hotelEventContact nvarchar(100)
										    , hotelRegionalAdministration nvarchar(100)
											, hotelHidePayment bit
											, hotelIprefer bit
											, [disabled] bit)

	INSERT INTO @denormalizedUmbracoHotels
	SELECT nodeId
		 , nodeName
		 , hotelCrmGuid
		 , hotelCode
		 , hotelName
		 , hotelCity
		 , hotelState
		 , hotelCountry
		 , hotelRegion
		 , contractedCurrency
		 , hotelCollectionCodes
		 , hotelMainCollection
		 , hotelRateGainID
		 , hotelAccountDirector
		 , hotelMarketingContact
		 , hotelRegionalDirector
		 , hotelAreaManagingDirector
		 , hotelRevenueAccountManager
		 , hotelAccountsReceivableContact
		 , hotelIPreferEngagementSpecialist
		 , hotelExecutiveVicePresident
		 , hotelEventContact
		 , hotelRegionalAdministration
		 , (SELECT n2.intValue AS intValue
			FROM @normalizedUmbracoData n2
			WHERE n2.contentTypeAlias = 'Hotel'
			  AND n2.propertyTypeAlias = 'hotelIprefer'
			  AND n2.nodeId = PivotTable.nodeId) AS [hotelIprefer]
		 , (SELECT n2.intValue AS intValue
			FROM @normalizedUmbracoData n2
			WHERE n2.contentTypeAlias = 'Hotel'
			  AND n2.propertyTypeAlias = 'hotelHidePayment'
			  AND n2.nodeId = PivotTable.nodeId) AS [hotelHidePayment]
		 , (SELECT n2.intValue AS intValue
			FROM @normalizedUmbracoData n2
			WHERE n2.contentTypeAlias = 'Hotel'
			  AND n2.propertyTypeAlias = 'disabled'
			  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
	FROM (
		SELECT n.nodeId AS nodeId
			 , n.nodeName AS nodeName
			 , n.propertyTypeAlias AS propertyTypeAlias
			 , n.intValue AS intValue
			 , n.varcharValue AS varcharValue
		FROM @normalizedUmbracoData n
		WHERE n.contentTypeAlias = 'Hotel'
	) AS SourceTable
	PIVOT (
		MAX(varcharValue) FOR propertyTypeAlias IN ([hotelCrmGuid], [hotelCode], [hotelName], [hotelCity], [hotelState], [hotelCountry]
		    , [hotelRegion], [contractedCurrency], [hotelCollectionCodes], [hotelMainCollection], [hotelRateGainID]
			, [hotelAccountDirector], [hotelMarketingContact], [hotelRegionalDirector], [hotelAreaManagingDirector]
			, [hotelRevenueAccountManager], [hotelAccountsReceivableContact], [hotelIPreferEngagementSpecialist]
			, [hotelExecutiveVicePresident], [hotelEventContact], [hotelRegionalAdministration])
	) AS PivotTable
	WHERE COALESCE(hotelCrmGuid, hotelCode, hotelName) IS NOT NULL



	DECLARE @denormalizedUmbracoPhgEmployee TABLE (nodeId int
												 , employeeKey uniqueidentifier
											     , nodeName nvarchar(MAX)
											     , crmContactGuid uniqueidentifier
											     , [disabled] bit)

	INSERT INTO @denormalizedUmbracoPhgEmployee
	SELECT nodeId
		 , uniqueID AS employeeKey
		 , nodeName
		 , crmContactGuid
		 , (SELECT n2.intValue AS intValue
			FROM @normalizedUmbracoData n2
			WHERE n2.contentTypeAlias = 'PhgEmployee'
			  AND n2.propertyTypeAlias = 'disabled'
			  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
	FROM (
		SELECT n.nodeId AS nodeId
		     , n.uniqueID AS uniqueID
			 , n.nodeName AS nodeName
			 , n.propertyTypeAlias AS propertyTypeAlias
			 , n.intValue AS intValue
			 , n.varcharValue AS varcharValue
		FROM @normalizedUmbracoData n
		WHERE n.contentTypeAlias = 'PhgEmployee'
	) AS SourceTable
	PIVOT (
		MAX(varcharValue) FOR propertyTypeAlias IN ([crmContactGuid], [industryName])
	) AS PivotTable
	WHERE COALESCE(crmContactGuid, industryName) IS NOT NULL


	------
	-- Step 2: Merge CRM data with ISO and Umbraco data and store in Hotels
	------


		--get all hotel ids and all collections they are in
		DECLARE @HotelCodes TABLE(Accountid uniqueidentifier
								, CollectionCode nvarchar(100));

		INSERT INTO @HotelCodes(Accountid,CollectionCode)
		SELECT DISTINCT hc.phg_hotel AS Accountid
					  , c.phg_code AS CollectionCode
		FROM LocalCRM.dbo.phg_hotelcollection hc
		INNER JOIN LocalCRM.dbo.phg_collection c
			ON hc.phg_collection = c.phg_collectionid
		WHERE GETDATE() BETWEEN ISNULL(hc.phg_startdate, '1900-1-1') AND ISNULL(hc.phg_enddate, '2100-12-31');


		--combine the collections into one field per hotel
		DECLARE @GroupedHotelCodes TABLE(Accountid uniqueidentifier
										,CollectionCodes nvarchar(100));

		INSERT INTO @GroupedHotelCodes(Accountid, CollectionCodes)
		SELECT DISTINCT Codes.Accountid
				      , CAST((SELECT H.CollectionCode + ' '
					          FROM @HotelCodes H
							  WHERE H.Accountid = Codes.Accountid
							  ORDER BY H.CollectionCode FOR XML PATH('')) AS nvarchar(MAX)) AS hotels
		FROM @HotelCodes AS Codes;


		--get all contacts from Umbraco
		DECLARE @umbracoContacts TABLE(nodeId nvarchar(100)
									 , crmContactGuid uniqueidentifier);

		INSERT INTO @umbracoContacts (nodeId, crmContactGuid)
		SELECT 'umb://document/' + REPLACE(umb.employeeKey, '-', '') as nodeID
			 , umb.crmContactGuid
		FROM @denormalizedUmbracoPhgEmployee umb
		WHERE umb.[disabled] = 0;


		 --get all hotels from Umbraco
		 DECLARE @umbracoHotels TABLE(hotelCrmGuid uniqueidentifier,
										hotelCode nvarchar(100),
										nodeName nvarchar(100),
										hotelName nvarchar(100),
										hotelCity nvarchar(100),
										hotelState nvarchar(100),
										hotelCountry nvarchar(100),
										hotelRegion nvarchar(100),
										hotelIprefer bit,
										hotelAccountDirector nvarchar(100),
										hotelMarketingContact nvarchar(100),
										hotelRegionalDirector nvarchar(100),
										hotelAreaManagingDirector nvarchar(100),
										hotelRevenueAccountManager nvarchar(100),
										hotelAccountsReceivableContact nvarchar(100),
										hotelIPreferEngagementSpecialist nvarchar(100),
										hotelExecutiveVicePresident nvarchar(100),
										hotelEventContact nvarchar(100),
										contractedCurrency nvarchar(3),
										isDisabled bit,
										hotelCollectionCodes nvarchar(100),
										hotelMainCollection nvarchar(50),
										hotelRateGainId nvarchar(50),
										hotelHidePayment bit,
										hotelRegionalAdministration nvarchar(100));

		INSERT INTO @umbracoHotels(hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,hotelMarketingContact,
									hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
									hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT umb.hotelCrmGuid AS hotelCrmGuid
			 , umb.hotelCode AS hotelCode
			 , umb.nodeName AS nodeName
			 , umb.hotelName AS hotelName
			 , ISNULL(umb.hotelCity, '') AS hotelCity
			 , ISNULL(umb.hotelState, '') AS hotelState
			 , ISNULL(umb.hotelCountry, '') AS hotelCountry
			 , ISNULL(umb.hotelRegion, '') AS hotelRegion
			 , umb.hotelIprefer AS hotelIprefer
			 , nhad.uniqueID AS hotelAccountDirector
			 , nhmc.uniqueID AS hotelMarketingContact
			 , nhrd.uniqueID AS hotelRegionalDirector
			 , nhamd.uniqueID AS hotelAreaManagingDirector
			 , nhram.uniqueID AS hotelRevenueAccountManager
			 , nharc.uniqueID AS hotelAccountsReceivableContact
			 , nhipes.uniqueID AS hotelIPreferEngagementSpecialist
			 , nhevp.uniqueID AS hotelExecutiveVicePresident
			 , nhec.uniqueID AS hotelEventContact
			 , ISNULL(umb.hotelContractedCurrency, '') AS contractedCurrency
			 , umb.[disabled] AS isDisabled
			 , ISNULL(umb.hotelCollectionCodes, '') AS hotelCollectionCodes
			 , ISNULL(umb.hotelMainCollection, '') AS hotelMainCollection
			 , ISNULL(umb.hotelRateGainID, '') AS hotelRateGainId
			 , ISNULL(umb.hotelHidePayment, 1) AS hotelHidePayment
			 , nhra.uniqueID AS hotelRegionalAdministration
		FROM @denormalizedUmbracoHotels as umb
		LEFT JOIN MemberPortal.dbo.umbracoNode nhad ON nhad.uniqueID = CAST(SUBSTRING(hotelAccountDirector, 16, 8) + '-' + SUBSTRING(hotelAccountDirector, 24, 4) + '-' + SUBSTRING(hotelAccountDirector, 28, 4) + '-' + SUBSTRING(hotelAccountDirector, 32, 4) + '-' + SUBSTRING(hotelAccountDirector, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhmc ON nhmc.uniqueID = CAST(SUBSTRING(hotelMarketingContact, 16, 8) + '-' + SUBSTRING(hotelMarketingContact, 24, 4) + '-' + SUBSTRING(hotelMarketingContact, 28, 4) + '-' + SUBSTRING(hotelMarketingContact, 32, 4) + '-' + SUBSTRING(hotelMarketingContact, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhrd ON nhrd.uniqueID = CAST(SUBSTRING(hotelRegionalDirector, 16, 8) + '-' + SUBSTRING(hotelRegionalDirector, 24, 4) + '-' + SUBSTRING(hotelRegionalDirector, 28, 4) + '-' + SUBSTRING(hotelRegionalDirector, 32, 4) + '-' + SUBSTRING(hotelRegionalDirector, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhamd ON nhamd.uniqueID = CAST(SUBSTRING(hotelAreaManagingDirector, 16, 8) + '-' + SUBSTRING(hotelAreaManagingDirector, 24, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 28, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 32, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhram ON nhram.uniqueID = CAST(SUBSTRING(hotelRevenueAccountManager, 16, 8) + '-' + SUBSTRING(hotelRevenueAccountManager, 24, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 28, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 32, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nharc ON nharc.uniqueID = CAST(SUBSTRING(hotelAccountsReceivableContact, 16, 8) + '-' + SUBSTRING(hotelAccountsReceivableContact, 24, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 28, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 32, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhipes ON nhipes.uniqueID = CAST(SUBSTRING(hotelIPreferEngagementSpecialist, 16, 8) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 24, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 28, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 32, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhevp ON nhevp.uniqueID = CAST(SUBSTRING(hotelExecutiveVicePresident, 16, 8) + '-' + SUBSTRING(hotelExecutiveVicePresident, 24, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 28, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 32, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhec ON nhec.uniqueID = CAST(SUBSTRING(hotelEventContact, 16, 8) + '-' + SUBSTRING(hotelEventContact, 24, 4) + '-' + SUBSTRING(hotelEventContact, 28, 4) + '-' + SUBSTRING(hotelEventContact, 32, 4) + '-' + SUBSTRING(hotelEventContact, 36, 12) AS uniqueidentifier)
		LEFT JOIN MemberPortal.dbo.umbracoNode nhra ON nhra.uniqueID = CAST(SUBSTRING(hotelRegionalAdministration, 16, 8) + '-' + SUBSTRING(hotelRegionalAdministration, 24, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 28, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 32, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 36, 12) AS uniqueidentifier)
		;



		--combine hotels from Umbraco and hotels from CRM into one table, with "table name" showing which system it came from
		--SOURCE table = CRM, TARGET table = Umbraco
		DECLARE @HotelData TABLE(TableName nvarchar(6),
								hotelCrmGuid uniqueidentifier,
								hotelCode nvarchar(100),
								nodeName nvarchar(100),
								hotelName nvarchar(100),
								hotelCity nvarchar(100),
								hotelState nvarchar(100),
								hotelCountry nvarchar(100),
								hotelRegion nvarchar(100),
								hotelIprefer bit,
								hotelAccountDirector nvarchar(100),
								hotelMarketingContact nvarchar(100),
								hotelRegionalDirector nvarchar(100),
								hotelAreaManagingDirector nvarchar(100),
								hotelRevenueAccountManager nvarchar(100),
								hotelAccountsReceivableContact nvarchar(100),
								hotelIPreferEngagementSpecialist nvarchar(100),
								hotelExecutiveVicePresident nvarchar(100),
								hotelEventContact nvarchar(100),
								contractedCurrency nvarchar(3),
								isDisabled bit,
								hotelCollectionCodes nvarchar(100),
								hotelMainCollection nvarchar(50),
								hotelRateGainId nvarchar(50),
								hotelRegionalAdministration nvarchar(100));

		INSERT INTO @HotelData(TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
								hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration)
		SELECT MIN(TableName) AS TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,MIN(hotelState),hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
				hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
				hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration
		FROM (SELECT DISTINCT 'SOURCE' AS TableName,hotelsReporting.crmGuid AS hotelCrmGuid,hotelsReporting.code AS hotelCode,hotelsReporting.hotelName AS nodeName,
						hotelsReporting.hotelName AS hotelName,ISNULL(hotelsReporting.physicalCity, '') AS hotelCity,ISNULL(subAreas.subAreaName, '') AS hotelState,
						ISNULL(countries.shortName, '') AS hotelCountry,ISNULL(hotelsReporting.geographicRegionName, '') AS hotelRegion,
						ISNULL(iPrefer.ContractFound, 0) AS hotelIprefer,hotelAccountDirector.nodeId AS hotelAccountDirector,hotelMarketingContact.nodeId AS hotelMarketingContact,
						hotelRegionalDirector.nodeId AS hotelRegionalDirector,hotelAreaManagingDirector.nodeId AS hotelAreaManagingDirector,
						hotelRevenueAccountManager.nodeId AS hotelRevenueAccountManager,hotelAccountsReceivableContact.nodeId AS hotelAccountsReceivableContact,
						hotelIPreferEngagementSpecialist.nodeId AS hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident.nodeId AS hotelExecutiveVicePresident,
						hotelEventContact.nodeId AS hotelEventContact,ISNULL(FinancialAccounts.currencyCode, '') AS contractedCurrency,0 AS isDisabled,
						ISNULL(BrandCodes.CollectionCodes, '') AS hotelCollectionCodes,ISNULL(mainBrandCode + ',' + brands.name, '') AS hotelMainCollection,
						ISNULL(phg_rategainid, '') AS hotelRateGainId,hotelRegionalAdministration.nodeId AS hotelRegionalAdministration
				FROM Core.dbo.hotelsReporting
					INNER JOIN (SELECT DISTINCT Account.accountid,Account.phg_accountdirectorid,Account.phg_marketingcontactid,Account.phg_regionalmanagerid,
										Account.phg_areamanagerid,Account.phg_revenueaccountmanagerid,Account.phg_accountsreceivablecontactid,Account.phg_ipreferexecutiveadminid,
										Account.phg_executivevicepresidentid,Account.phg_rategainid,Account.phg_regionaladministrationid
								FROM LocalCRM.dbo.Account
									INNER JOIN LocalCRM.dbo.phg_hotelcollection hc ON Account.accountid = hc.phg_hotel
									INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
								WHERE Account.statuscodename IN('Member Hotel','Renewal Hotel')
									OR Account.PHG_MemberPortalImportOverride = 0
								) Account ON hotelsReporting.crmGuid = Account.accountID
					LEFT JOIN ISO.dbo.subAreas ON subAreas.countryCode2 = hotelsReporting.country AND subAreas.subAreaCode = hotelsReporting.state
					LEFT JOIN ISO.dbo.countries ON hotelsReporting.country = countries.code2
					LEFT JOIN (SELECT DISTINCT Account.accountid AS AccountId,1 AS ContractFound
								FROM (SELECT DISTINCT hc.phg_hotel AS Accountid
										FROM LocalCRM.dbo.Account
											INNER JOIN LocalCRM.dbo.phg_hotelcollection hc ON Account.accountid = hc.phg_hotel
											INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
										WHERE Account.statuscodename IN('Member Hotel','Renewal Hotel')
											OR Account.PHG_MemberPortalImportOverride = 0
									  ) Account
									INNER JOIN LocalCRM.dbo.phg_contractedprograms ON Account.accountId = phg_contractedprograms.phg_accountnameid
																	AND phg_contractedprograms.phg_contractedprogramstypename LIKE '%i%Prefer%'
																	AND GETDATE() BETWEEN ISNULL(phg_contractedprograms.phg_startdate, '1900-1-1')
																	AND ISNULL(phg_contractedprograms.phg_enddate, '2100-12-31')
								) iPrefer ON Account.accountId = iPrefer.AccountId
					LEFT JOIN @GroupedHotelCodes AS BrandCodes ON hotelsReporting.crmGuid = BrandCodes.Accountid
					LEFT JOIN Core.dbo.brands ON hotelsReporting.mainBrandCode = brands.code
					LEFT JOIN @umbracoContacts as hotelAccountDirector ON hotelAccountDirector.crmContactGuid = Account.phg_accountdirectorid
					LEFT JOIN @umbracoContacts as hotelMarketingContact ON hotelMarketingContact.crmContactGuid = Account.phg_marketingcontactid
					LEFT JOIN @umbracoContacts as hotelRegionalDirector ON hotelRegionalDirector.crmContactGuid = Account.phg_regionalmanagerid
					LEFT JOIN @umbracoContacts as hotelAreaManagingDirector ON hotelAreaManagingDirector.crmContactGuid = Account.phg_areamanagerid
					LEFT JOIN @umbracoContacts as hotelRevenueAccountManager ON hotelRevenueAccountManager.crmContactGuid = Account.phg_revenueaccountmanagerid
					LEFT JOIN @umbracoContacts as hotelAccountsReceivableContact ON hotelAccountsReceivableContact.crmContactGuid = Account.phg_accountsreceivablecontactid
					LEFT JOIN @umbracoContacts as hotelExecutiveVicePresident ON hotelExecutiveVicePresident.crmContactGuid = Account.phg_executivevicepresidentid
					LEFT JOIN @umbracoContacts as hotelIPreferEngagementSpecialist ON hotelIPreferEngagementSpecialist.crmContactGuid = Account.phg_ipreferexecutiveadminid
					LEFT JOIN @umbracoContacts as hotelRegionalAdministration ON hotelRegionalAdministration.crmContactGuid = Account.phg_regionaladministrationid
					LEFT JOIN @umbracoContacts as hotelEventContact ON hotelEventContact.crmContactGuid = '570C90EF-8AA2-E611-80F9-FC15B4282DF4'
					LEFT JOIN dbo.FinancialAccounts ON hotelsReporting.code = FinancialAccounts.hotelCode
						UNION
				SELECT 'TARGET' AS TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
						hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
						hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,
						hotelMainCollection,hotelRateGainId,hotelRegionalAdministration
				FROM @umbracoHotels
			) unioned
		GROUP BY hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity
		--,hotelState
		,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,hotelMarketingContact,
				hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
				hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration,isDisabled
		--HAVING COUNT(*) = 1  <<Bruce Okallau Mar 30th 2020 I cannot understand why this would be there except perhaps to remove duplicates but unfortunately it ends up filtering out other valid hotels. Removing this for now but keeping it in the comments so that we can revisit the decision in the future. I'm also taking the minimum of the hotel state as some irregularities in ISO codes can lead to duplicate entries if we do not.
		;


		INSERT INTO dbo.Hotels(Name,hotelCrmGuid,hotelDisabled,hotelCode,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
								hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,
								hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT	S.hotelName,S.hotelCrmGuid,0,S.hotelCode,S.hotelName,S.hotelCity,S.hotelState,S.hotelCountry,S.hotelRegion,S.hotelIprefer,S.hotelAccountDirector,
				S.hotelMarketingContact,S.hotelRegionalDirector,S.hotelAreaManagingDirector,S.hotelRevenueAccountManager,S.hotelAccountsReceivableContact,
				S.hotelIPreferEngagementSpecialist,S.hotelExecutiveVicePresident,S.hotelEventContact,S.contractedCurrency,S.hotelCollectionCodes,S.hotelMainCollection,
				S.hotelRateGainId,COALESCE(U.hotelHidePayment, 1),S.hotelRegionalAdministration
		FROM @HotelData S
			LEFT JOIN @umbracoHotels U ON S.hotelCrmGuid = U.hotelCrmGuid
		WHERE TableName = 'SOURCE'


		INSERT INTO dbo.Hotels(Name,hotelCrmGuid,hotelDisabled,hotelCode,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
								hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,
								hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT T.hotelName,T.hotelCrmGuid,1,T.hotelCode,T.hotelName,T.hotelCity,T.hotelState,T.hotelCountry,T.hotelRegion,T.hotelIprefer,T.hotelAccountDirector,
				T.hotelMarketingContact,T.hotelRegionalDirector,T.hotelAreaManagingDirector,T.hotelRevenueAccountManager,T.hotelAccountsReceivableContact,
				T.hotelIPreferEngagementSpecialist,T.hotelExecutiveVicePresident,T.hotelEventContact,T.contractedCurrency,T.hotelCollectionCodes,
				T.hotelMainCollection,T.hotelRateGainId,1,T.hotelRegionalAdministration
		FROM @HotelData T
			INNER JOIN @umbracoHotels U ON U.hotelCrmGuid = T.hotelCrmGuid AND U.isDisabled = 0
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS ( SELECT 1 FROM @HotelData S WHERE S.TableName = 'SOURCE' AND T.hotelCrmGuid = S.hotelCrmGuid );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Industries]'
GO



ALTER PROCEDURE [dbo].[mpid_Industries]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	TRUNCATE TABLE dbo.Industries;

	------
	-- Step 1: Retrieve denormalized Industry data from Umbraco
	------

	-- Store normalized Umbraco property data
	DECLARE @normalizedUmbracoIndustries TABLE (nodeId int
												, nodeName nvarchar(max)
												, propertyTypeAlias nvarchar(max)
												, intValue int
												, varcharValue nvarchar(max))
	INSERT INTO @normalizedUmbracoIndustries
	SELECT n.id AS nodeId
			, n.[text] AS nodeName
			, pt.[Alias] AS propertyTypeAlias
			, pd.intValue AS intValue
			, pd.varcharValue AS varcharvalue
	FROM MemberPortal.dbo.umbracoNode n
	LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
		ON cv.nodeId = n.id
	LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
		ON cv.id = dv.id
	INNER JOIN MemberPortal.dbo.umbracoContent c
		ON c.nodeId = n.id
	INNER JOIN MemberPortal.dbo.cmsContentType ct
		ON ct.nodeId = c.contentTypeId
	LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
		ON d.nodeId = c.nodeId
	LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
		ON pd.versionId = cv.id
	INNER JOIN MemberPortal.dbo.cmsPropertyType pt
		ON pt.id = pd.propertytypeid
	WHERE ct.[alias] = 'Industry'
	AND d.published = 1
	AND cv.[current] = 1
	AND n.trashed = 0
	ORDER BY n.id DESC, pt.[Alias], versionDate DESC


	-- Store denormalize the Umbraco property data
	DECLARE @denormalizedUmbracoIndustries TABLE (nodeId int
												, nodeName nvarchar(MAX)
												, industryId int
												, industryName nvarchar(MAX)
												, [disabled] bit)

	INSERT INTO @denormalizedUmbracoIndustries
	SELECT nodeId
		 , nodeName
		 , industryId
		 , industryName
		 , (SELECT n2.intValue AS intValue
			FROM @normalizedUmbracoIndustries n2
			WHERE n2.propertyTypeAlias = 'disabled'
			  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
	FROM (
		SELECT n.nodeId AS nodeId
			 , n.nodeName AS nodeName
			 , n.propertyTypeAlias AS propertyTypeAlias
			 , n.intValue AS intValue
			 , n.varcharValue AS varcharValue
		FROM @normalizedUmbracoIndustries n
	) AS SourceTable
	PIVOT (
		MAX(varcharValue) FOR propertyTypeAlias IN ([industryId], [industryName])
	) AS PivotTable
	WHERE COALESCE(industryId, industryName) IS NOT NULL


	------
	-- Step 2: Merge CRM industries with Umbraco Industries and store in Industries table
	------


	DECLARE @IndustryData TABLE(TableName nvarchar(6),industryId int,industryName nvarchar(100),isDisabled bit);

	INSERT INTO @IndustryData(TableName,industryId,industryName,isDisabled)
	SELECT MIN(TableName) AS TableName
		 , industryId
		 , industryName
		 , isDisabled
	FROM (SELECT DISTINCT 'SOURCE' AS TableName
						, Account.industrycode AS industryId
						, Account.industrycodename AS industryName
						, 0 AS isDisabled
		  FROM LocalCRM.dbo.Account
		  WHERE Account.industrycode IS NOT NULL
		  UNION
		  SELECT 'TARGET' AS TableName
				,umb.industryId AS industryId
				,umb.industryName AS industryName
				,umb.[disabled] AS isDisabled
		  FROM @denormalizedUmbracoIndustries umb
		  ) AS unioned
	GROUP BY industryId, industryName, isDisabled
	HAVING COUNT(*) = 1;


	--INSERT INTO dbo.Industries(industryId,industryName,[disabled])
	SELECT industryId
		 , industryName
		 , 0
	FROM @IndustryData
	WHERE TableName = 'SOURCE';


	--INSERT INTO dbo.Industries(industryId,industryName,[disabled])
	SELECT T.industryId
		 , T.industryName
		 , 1
	FROM @IndustryData T
	INNER JOIN @denormalizedUmbracoIndustries umb
		ON umb.industryId = T.industryId
	WHERE T.TableName = 'TARGET'
	  AND umb.[disabled] = 0
	  AND NOT EXISTS ( SELECT 1 FROM @IndustryData S WHERE S.TableName = 'SOURCE' AND T.industryId = S.industryId );
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_LeisureAccounts]'
GO



ALTER PROCEDURE [dbo].[mpid_LeisureAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.LeisureAccounts;


		------
		-- Step 1: Retrieve denormalized data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoData TABLE (nodeId int
											, uniqueID uniqueidentifier
											, nodeName nvarchar(max)
											, contentTypeAlias nvarchar(max)
											, propertyTypeAlias nvarchar(max)
											, intValue int
											, varcharValue nvarchar(max))
		INSERT INTO @normalizedUmbracoData
		SELECT n.id AS nodeId
			 , n.uniqueID AS uniqueID
			 , n.[text] AS nodeName
			 , ct.alias AS contentTypeAlias
			 , pt.[Alias] AS propertyTypeAlias
			 , pd.intValue AS intValue
			 , pd.varcharValue AS varcharvalue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] IN ('PhgEmployee', 'Industry', 'LeisureAccount')
		  AND d.published = 1
		  AND cv.[current] = 1
		  AND n.trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoLeisureAccount TABLE ( nodeId int
														, nodeName nvarchar(MAX)
														, leisureAccountCrmGuid uniqueidentifier
														, crmAccountName nvarchar(MAX)
														, crmAccountStreetAddressLine1 nvarchar(MAX)
														, crmAccountStreetAddressLine2 nvarchar(MAX)
														, crmAccountCity nvarchar(MAX)
														, crmAccountState nvarchar(MAX)
														, crmAccountCountry nvarchar(MAX)
														, crmAccountPostalCode nvarchar(MAX)
														, crmAccountPhoneNumber nvarchar(MAX)
														, crmAccountEmailAddress nvarchar(MAX)
														, crmAccountWebSite nvarchar(MAX)
														, leisureAccountIataGlobalAccountManager nvarchar(MAX)
														, crmAccountIndustry nvarchar(100)
														, crmAccountManagedAccount bit
														, [disabled] bit)

		INSERT INTO @denormalizedUmbracoLeisureAccount
		SELECT nodeId
			, nodeName
			, leisureAccountCrmGuid
			, crmAccountName
			, crmAccountStreetAddressLine1
			, crmAccountStreetAddressLine2
			, crmAccountCity
			, crmAccountState
			, crmAccountCountry
			, crmAccountPostalCode
			, crmAccountPhoneNumber
			, crmAccountEmailAddress
			, crmAccountWebSite
			, leisureAccountIataGlobalAccountManager
			, crmAccountIndustry
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'LeisureAccount'
			      AND n2.propertyTypeAlias = 'crmAccountManagedAccount'
				  AND n2.nodeId = PivotTable.nodeId) AS [crmAccountManagedAccount]
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'LeisureAccount'
			      AND n2.propertyTypeAlias = 'disabled'
				  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'LeisureAccount'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN
			 ([leisureAccountCrmGuid], [crmAccountName], [crmAccountStreetAddressLine1], [crmAccountStreetAddressLine2]
			, [crmAccountCity], [crmAccountState], [crmAccountCountry], [crmAccountPostalCode]
			, [crmAccountPhoneNumber], [crmAccountEmailAddress], [crmAccountWebSite]
			, [leisureAccountIataGlobalAccountManager], [crmAccountIndustry])
		) AS PivotTable
		WHERE COALESCE(leisureAccountCrmGuid, crmAccountName) IS NOT NULL AND leisureAccountCrmGuid <> ''


		DECLARE @denormalizedUmbracoIndustries TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, industryKey uniqueidentifier
													, industryId int
													, industryName nvarchar(MAX))

		INSERT INTO @denormalizedUmbracoIndustries
		SELECT nodeId
			 , nodeName
			 , uniqueID AS industryKey
			 , industryId
			 , industryName
		FROM (
			SELECT n.nodeId AS nodeId
				 , n.nodeName AS nodeName
				 , n.uniqueID AS uniqueID
				 , n.propertyTypeAlias AS propertyTypeAlias
				 , n.intValue AS intValue
				 , n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'Industry'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([industryId], [industryName])
		) AS PivotTable
		WHERE COALESCE(industryId, industryName) IS NOT NULL


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoPhgEmployee TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, employeeKey uniqueidentifier
													, crmContactGuid uniqueidentifier)

		INSERT INTO @denormalizedUmbracoPhgEmployee
		SELECT nodeId
			, nodeName
			, uniqueID as employeeKey
			, crmContactGuid
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.uniqueID AS uniqueID
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'PhgEmployee'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([crmContactGuid])
		) AS PivotTable
		WHERE crmContactGuid IS NOT NULL

		------
		-- Step 2: Merge CRM data with Umbraco members and store in LeisureAccounts
		------


		DECLARE @LeisureAccountData TABLE(TableName nvarchar(6),
											nodeName nvarchar(100),
											crmAccountName nvarchar(100),
											leisureAccountCrmGUID uniqueidentifier,
											crmAccountStreetAddressLine1 nvarchar(100),
											crmAccountStreetAddressLine2 nvarchar(100),
											crmAccountCity nvarchar(100),
											crmAccountState nvarchar(100),
											crmAccountCountry nvarchar(100),
											crmAccountPostalCode nvarchar(100),
											crmAccountPhoneNumber nvarchar(100),
											crmAccountEmailAddress nvarchar(100),
											crmAccountWebSite nvarchar(100),
											crmAccountManagedAccount bit,
											leisureAccountIataGlobalAccountManager nvarchar(100),
											accountIndustry nvarchar(100),
											isDisabled bit);

		INSERT INTO @LeisureAccountData(TableName,nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
										crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
										crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountManagedAccount,leisureIataGlobalAccountManager,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName
					,account.[Name] AS nodeName
					,account.[Name] AS crmAccountName
					,UPPER(account.AccountID) AS leisureAccountCrmGUID
					,ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1
					,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2
					,ISNULL(Address1_City, '') AS crmAccountCity
					,ISNULL(Address1_StateOrProvince, '') AS crmAccountState
					,ISNULL(Address1_Country, '') AS crmAccountCountry
					,ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode
					,ISNULL(Telephone1, '') AS crmAccountPhoneNumber
					,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress
					,ISNULL(WebSiteURL, '') AS crmAccountWebSite
					,CASE account.phg_leisuresalesstatus WHEN 100000002 THEN 1 ELSE 0 END AS crmAccountManagedAccount
					,'umb://document/' + REPLACE(umbracoIataGlobalAccountManager.employeeKey, '-', '') AS leisureIataGlobalAccountManager
					,'umb://document/' + REPLACE(umbracoIndustry.industryKey, '-', '') AS accountIndustry
					,0 AS isDisabled
				FROM (SELECT DISTINCT [Name]
									 ,account.AccountID
									 ,Address1_Line1
									 ,Address1_Line2
									 ,Address1_City
									 ,Address1_StateOrProvince
									 ,Address1_Country
									 ,Address1_PostalCode
									 ,Telephone1
									 ,EMailAddress1
									 ,WebSiteURL
									 ,account.phg_leisuresalesstatus
									 ,phg_iataglobalaccountmanagerid
									 ,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_leisuresalesstatus IN (100000001,100000002)
							AND account.phg_leisuresales = 1
							AND account.phg_showinmemberportalleisure = 1) account
				LEFT OUTER JOIN @denormalizedUmbracoPhgEmployee as umbracoIataGlobalAccountManager
					ON umbracoIataGlobalAccountManager.crmContactGuid = account.phg_iataglobalaccountmanagerid
				LEFT OUTER JOIN @denormalizedUmbracoIndustries as umbracoIndustry
					ON umbracoIndustry.industryId = account.IndustryCode

				UNION

				SELECT 'TARGET' AS TableName
					  ,umb.nodeName AS nodeName
					  ,umb.crmAccountName AS crmAccountName
					  ,umb.leisureAccountCrmGuid AS leisureAccountCrmGuid
					  ,ISNULL(umb.crmAccountStreetAddressLine1, '') AS crmAccountStreetAddressLine1
					  ,ISNULL(umb.crmAccountStreetAddressLine2, '') AS crmAccountStreetAddressLine2
					  ,ISNULL(umb.crmAccountCity, '') AS crmAccountCity
					  ,ISNULL(umb.crmAccountState, '') AS crmAccountState
					  ,ISNULL(umb.crmAccountCountry, '') AS crmAccountCountry
					  ,ISNULL(umb.crmAccountPostalCode, '') AS crmAccountPostalCode
					  ,ISNULL(umb.crmAccountPhoneNumber, '') AS crmAccountPhoneNumber
					  ,ISNULL(umb.crmAccountEmailAddress, '') AS crmAccountEmailAddress
					  ,ISNULL(umb.crmAccountWebSite, '') AS crmAccountWebSite
					  ,umb.crmAccountManagedAccount AS crmAccountManagedAccount
					  ,umb.leisureAccountIataGlobalAccountManager AS leisureIataGlobalAccountManager
					  ,umb.crmAccountIndustry AS accountIndustry
					  ,umb.[disabled] AS isDisabled
				FROM @denormalizedUmbracoLeisureAccount umb
			  ) unioned
		GROUP BY nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureIataGlobalAccountManager,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @LeisureAccountData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.LeisureAccounts(Name,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState, crmAccountCountry, crmAccountPostalCode, crmAccountPhoneNumber,
										crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,0,accountIndustry
		FROM @LeisureAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.LeisureAccounts(Name,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState, crmAccountCountry, crmAccountPostalCode, crmAccountPhoneNumber,
										crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,1,accountIndustry
		FROM @LeisureAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @LeisureAccountData S WHERE S.TableName = 'SOURCE' AND T.leisureAccountCrmGUID = S.leisureAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_PHR_Members]'
GO



ALTER PROCEDURE [dbo].[mpid_PHR_Members]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.SystemUser WHERE domainname IS NOT NULL AND IsDisabled = 0)
	BEGIN
		TRUNCATE TABLE dbo.PhrMembers_Hotels
		TRUNCATE TABLE dbo.PhrMembers_Inactive


		------
		-- Step 1: Retrieve denormalized data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoData TABLE (nodeId int
											, uniqueID uniqueidentifier
											, nodeName nvarchar(max)
											, contentTypeAlias nvarchar(max)
											, propertyTypeAlias nvarchar(max)
											, intValue int
											, varcharValue nvarchar(max)
											, textValue ntext)
		INSERT INTO @normalizedUmbracoData
		SELECT n.id AS nodeId
			 , n.uniqueID AS uniqueID
			 , n.[text] AS nodeName
			 , ct.alias AS contentTypeAlias
			 , pt.[Alias] AS propertyTypeAlias
			 , pd.intValue AS intValue
			 , pd.varcharValue AS varcharvalue
			 , pd.textValue AS textValue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] IN ('Hotel', 'PHRMember')
		  AND (d.published IS NULL OR d.published = 1)
		  AND cv.[current] = 1
		  AND trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoHotel TABLE ( nodeId int
												, hotelKey uniqueidentifier
												, nodeName nvarchar(MAX)
												, hotelName nvarchar(MAX)
												, hotelCode nvarchar(MAX)
												, [disabled] bit)

		INSERT INTO @denormalizedUmbracoHotel
		SELECT nodeId
			, uniqueID as hotelKey
			, nodeName
			, hotelName
			, hotelCode
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoData n2
				WHERE n2.contentTypeAlias = 'Hotel'
			      AND n2.propertyTypeAlias = 'disabled'
				  AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				,n.uniqueID AS uniqueID
				,n.nodeName AS nodeName
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			WHERE n.contentTypeAlias = 'Hotel'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN ([hotelName], [hotelCode])
		) AS PivotTable
		WHERE COALESCE(hotelName, hotelCode) IS NOT NULL


		DECLARE @denormalizedUmbracoPhrMember TABLE ( nodeId int
													, memberKey uniqueidentifier
													, nodeName nvarchar(MAX)
													, loginName nvarchar(MAX)
													, defaultHotel nvarchar(MAX)
													, hotels nvarchar(MAX)
													, firstName nvarchar(MAX)
													, lastName nvarchar(MAX)
													, title nvarchar(MAX)
													, umbracoMemberApproved bit)

		INSERT INTO @denormalizedUmbracoPhrMember
		SELECT nodeId
			, uniqueID as memberKey
			, nodeName
			, loginName
			, (SELECT n2.textValue AS textValue
			   FROM @normalizedUmbracoData n2
			   WHERE n2.contentTypeAlias = 'PHRMember'
				 AND n2.propertyTypeAlias = 'defaultHotel'
				 AND n2.nodeId = PivotTable.nodeId) AS defaultHotel
			, (SELECT n2.textValue AS textValue
			   FROM @normalizedUmbracoData n2
			   WHERE n2.contentTypeAlias = 'PHRMember'
				 AND n2.propertyTypeAlias = 'hotels'
				 AND n2.nodeId = PivotTable.nodeId) AS hotels
			, firstName
			, lastName
			, title
			, umbracoMemberApproved
		FROM (
			SELECT n.nodeId AS nodeId
				,n.uniqueID AS uniqueID
				,n.nodeName AS nodeName
				,m.loginName AS loginName
				,m.isApproved AS umbracoMemberApproved
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoData n
			INNER JOIN MemberPortal.dbo.cmsMember m
				ON m.nodeId = n.nodeId
			WHERE n.contentTypeAlias = 'PHRMember'
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN
			 ([firstName], [lastName], [title])
		) AS PivotTable
		WHERE COALESCE([firstName], [lastName], [title]) IS NOT NULL

		------
		-- Step 2: Update PhrMembers, PhrMembers_Hotels, and PhrMembers_Inactive
		------


		--  Retrieve PHR Members who are using the demo hotel
		DECLARE @TestHotelId nvarchar(MAX),
				@TestHotelIdXml nvarchar(MAX);

		SELECT @TestHotelId = 'umb://document/' + LOWER(REPLACE(umbHotel.hotelKey, '-', ''))
		FROM @denormalizedUmbracoHotel umbHotel
		WHERE umbHotel.hotelName = 'PHG Test Hotel'

		SELECT @TestHotelIdXml = '%<i>' + @TestHotelId + '</i>%'


		DECLARE @PhgTestHotelUsers TABLE(emailAddress nvarchar(100), TestHotelId nvarchar(MAX))

		INSERT INTO @PhgTestHotelUsers(emailAddress,TestHotelId)
		SELECT umbPhrMember.loginName AS email
			  ,@TestHotelId
		FROM @denormalizedUmbracoPhrMember umbPhrMember
		WHERE umbPhrMember.hotels IS NOT NULL
		  AND umbPhrMember.hotels LIKE @TestHotelIdXml


		-- Grab users that should have all hotels
		DECLARE @PhgAllHotelUsers TABLE(emailAddress nvarchar(100))

		INSERT INTO @PhgAllHotelUsers(emailAddress)
		SELECT emailAddress
		FROM dbo.PhrMembers_AllHotels

		UPDATE dbo.PhrMembers
			SET updateMember = 0


		DECLARE @hotels TABLE(emailAddress nvarchar(100),HotelIDs nvarchar(MAX))
	
		INSERT INTO @hotels(emailAddress,HotelIDs)
		SELECT emailAddress, HotelNodeID
		FROM (
				SELECT DISTINCT mh.emailAddress
							  , 'umb://document/' + LOWER(REPLACE(umbHotel.hotelKey, '-', '')) AS HotelNodeID
				FROM CrmMemberHotel mh
				INNER JOIN @denormalizedUmbracoHotel umbHotel
					ON umbHotel.hotelCode = mh.hotelCode
				WHERE umbHotel.[disabled] = 0

				UNION

				SELECT emailAddress
					 , 'umb://document/' + LOWER(REPLACE(enabledHotels.hotelKey, '-', '')) AS HotelNodeID
				FROM @PhgAllHotelUsers,
					(
						SELECT umbHotel.hotelKey
						FROM @denormalizedUmbracoHotel umbHotel
						WHERE umbHotel.[disabled] = 0
					) enabledHotels

				UNION

				SELECT DISTINCT D.emailAddress
							  , D.TestHotelId AS HotelNodeID
				FROM @PhgTestHotelUsers D
			) AS unioned



		INSERT INTO dbo.PhrMembers_Hotels(emailAddress,defaultHotel,hotels)
		SELECT DISTINCT H1.emailAddress,
					CAST((SELECT TOP 1 dh.HotelIDs FROM @hotels dh WHERE dh.emailAddress = H1.emailAddress) AS nvarchar(100)) AS defaultHotel,
					CAST((SELECT H2.HotelIDs + ',' FROM @hotels H2 WHERE H2.emailAddress = H1.emailAddress FOR XML PATH('')) AS nvarchar(MAX)) AS hotels
		FROM @hotels H1



		UPDATE dbo.PhrMembers_Hotels
		SET hotels = LEFT(hotels,LEN(hotels) - 1)



		DECLARE @PhrMembersData TABLE(TableName nvarchar(6),
										firstName nvarchar(100),
										lastName nvarchar(100),
										title nvarchar(100),
										email nvarchar(100),
										defaultHotel nvarchar(100),
										hotels nvarchar(MAX)
									 );

		INSERT INTO @PhrMembersData(TableName,firstName,lastName,title,email,defaultHotel,hotels)
		SELECT MIN(TableName) AS TableName
			  ,firstName
			  ,lastName
			  ,title
			  ,email
			  ,defaultHotel
			  ,hotels
		FROM (SELECT 'SOURCE' AS TableName,FirstName AS firstName,LastName AS lastName,Title AS title,domainname AS email,H.defaultHotel,H.hotels
			  FROM LocalCRM.dbo.SystemUser
			  LEFT JOIN dbo.PhrMembers_Hotels H
				ON SystemUser.domainname = H.emailAddress COLLATE SQL_Latin1_General_CP1_CI_AS
			  WHERE domainname IS NOT NULL
				AND IsDisabled = 0

			  UNION

			  SELECT 'TARGET' AS TableName
					,umbPhrMember.firstName AS firstName
					,umbPhrMember.lastName AS lastName
					,umbPhrMember.title AS title
					,umbPhrMember.loginName AS email
					,umbPhrMember.defaultHotel AS defaultHotel
					,umbPhrMember.hotels AS hotels
			  FROM @denormalizedUmbracoPhrMember umbPhrMember
			  INNER JOIN MemberPortal.dbo.cmsMember
				ON umbPhrMember.nodeId = cmsMember.nodeId
			  WHERE umbPhrMember.umbracoMemberApproved = 1
			  ) AS unioned
		GROUP BY firstName,lastName,title,email,defaultHotel,hotels
		--HAVING COUNT(*) = 1



		UPDATE dbo.PhrMembers
		SET firstName = P.firstName,
			lastName = P.lastName,
			title = P.title,
			updateMember = 1
		FROM dbo.PhrMembers
		INNER JOIN @PhrMembersData P ON PhrMembers.email = P.email
		INNER JOIN MemberPortal.dbo.cmsMember ON p.email = cmsMember.LoginName
		INNER JOIN @denormalizedUmbracoPhrMember umbPhrMember ON cmsMember.nodeId = umbPhrMember.nodeId
		WHERE P.TableName = 'SOURCE'
		  AND umbPhrMember.umbracoMemberApproved = 1



		INSERT INTO dbo.PhrMembers(firstName,lastName,title,email,updateMember,sendEmail)
		SELECT firstName,lastName,title,email,1,1
		FROM @PhrMembersData P
		WHERE P.TableName = 'SOURCE'
		  AND P.email NOT IN(SELECT N.email FROM dbo.PhrMembers AS N)

		UPDATE p
		SET p.updateMember = 1
		FROM dbo.PhrMembers p
		LEFT OUTER JOIN MemberPortal.dbo.cmsMember c ON p.email = c.Email
		WHERE c.nodeId IS NULL

		INSERT INTO dbo.PhrMembers_Inactive(email)
		SELECT T.email
		FROM @PhrMembersData T
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS (SELECT 1
							FROM @PhrMembersData S
							WHERE S.TableName = 'SOURCE'
							  AND S.email = T.email );
		
	
		UPDATE dbo.PhrMembers
		SET	updateMember = 0
		WHERE email IN (SELECT emailAddress
						FROM dbo.PhrMembers_ExcludeMembers)
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_Set_QI_onPhgEmployees]'
GO


ALTER PROCEDURE [dbo].[mpid_Set_QI_onPhgEmployees]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.SystemUser WHERE IsDisabled = 0)
	BEGIN
		TRUNCATE TABLE dbo.PhgEmployees;

		------
		-- Step 1: Retrieve denormalized phgEmployee member data from Umbraco
		------

		-- Store normalized Umbraco property data
		DECLARE @normalizedUmbracoPhgEmployee TABLE (nodeId int
													, nodeName nvarchar(max)
													, propertyTypeAlias nvarchar(max)
													, intValue int
													, varcharValue nvarchar(max))
		INSERT INTO @normalizedUmbracoPhgEmployee
		SELECT n.id AS nodeId
			, n.[text] AS nodeName
			, pt.[Alias] AS propertyTypeAlias
			, pd.intValue AS intValue
			, pd.varcharValue AS varcharvalue
		FROM MemberPortal.dbo.umbracoNode n
		LEFT OUTER JOIN MemberPortal.dbo.umbracoContentVersion cv
			ON cv.nodeId = n.id
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocumentVersion dv
			ON cv.id = dv.id
		INNER JOIN MemberPortal.dbo.umbracoContent c
			ON c.nodeId = n.id
		INNER JOIN MemberPortal.dbo.cmsContentType ct
			ON ct.nodeId = c.contentTypeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoDocument d
			ON d.nodeId = c.nodeId
		LEFT OUTER JOIN MemberPortal.dbo.umbracoPropertyData pd
			ON pd.versionId = cv.id
		INNER JOIN MemberPortal.dbo.cmsPropertyType pt
			ON pt.id = pd.propertytypeid
		WHERE ct.[alias] = 'PhgEmployee'
		AND d.published = 1
		AND cv.[current] = 1
		AND trashed = 0
		ORDER BY n.id DESC, pt.[Alias], versionDate DESC


		-- Store denormalize the Umbraco property data
		DECLARE @denormalizedUmbracoPhgEmployee TABLE (nodeId int
													, nodeName nvarchar(MAX)
													, crmContactGuid uniqueidentifier
													, crmContactFullName nvarchar(MAX)
													, crmContactFirstName nvarchar(MAX)
													, crmContactLastName nvarchar(MAX)
													, crmContactJobTitle nvarchar(MAX)
													, crmContactPhoneNumber nvarchar(MAX)
													, crmContactFaxNumber nvarchar(MAX)
													, crmContactEmailAddress nvarchar(MAX)
													, phgEmployeeCity nvarchar(MAX)
													, phgEmployeeState nvarchar(MAX)
													, [disabled] bit)

		INSERT INTO @denormalizedUmbracoPhgEmployee
		SELECT nodeId
			, nodeName
			, crmContactGuid
			, crmContactFullName
			, crmContactFirstName
			, crmContactLastName
			, crmContactJobTitle
			, crmContactPhoneNumber
			, crmContactFaxNumber
			, crmContactEmailAddress
			, phgEmployeeCity
			, phgEmployeeState
			, (SELECT n2.intValue AS intValue
				FROM @normalizedUmbracoPhgEmployee n2
				WHERE n2.propertyTypeAlias = 'disabled'
				AND n2.nodeId = PivotTable.nodeId) AS [disabled]
		FROM (
			SELECT n.nodeId AS nodeId
				,n.nodeName AS nodeName
				,n.propertyTypeAlias AS propertyTypeAlias
				,n.intValue AS intValue
				,n.varcharValue AS varcharValue
			FROM @normalizedUmbracoPhgEmployee n
		) AS SourceTable
		PIVOT (
			MAX(varcharValue) FOR propertyTypeAlias IN
			([crmContactGuid], [crmContactFullName], [crmContactFirstName], [crmContactLastName]
		, [crmContactJobTitle], [crmContactPhoneNumber], [crmContactFaxNumber], [crmContactEmailAddress]
		, [phgEmployeeCity], [phgEmployeeState])
		) AS PivotTable
		WHERE COALESCE(crmContactGuid, crmContactFullName, crmContactFirstName, crmContactLastName, crmContactPhoneNumber, crmContactEmailAddress) IS NOT NULL


		------
		-- Step 2: Merge CRM members with Umbraco members and store in PhgEmployees
		------

		DECLARE @EmployeeData TABLE(TableName nvarchar(6),
									crmContactGuid uniqueidentifier,
									nodeName nvarchar(100),
									crmContactFullname nvarchar(100),
									crmContactFirstName nvarchar(100),
									crmContactLastName nvarchar(100),
									crmContactJobTitle nvarchar(100),
									crmContactPhoneNumber nvarchar(100),
									crmContactFaxNumber nvarchar(100),
									crmContactEmailAddress nvarchar(100),
									phgEmployeeCity nvarchar(100),
									phgEmployeeState nvarchar(100),
									isDisabled bit);

		INSERT INTO @EmployeeData(TableName,crmContactGuid,nodeName,crmContactFullname,crmContactFirstName,crmContactLastName,
									crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,crmContactEmailAddress,phgEmployeeCity,phgEmployeeState,isDisabled)
		SELECT MIN(TableName) AS TableName
			, crmContactGuid
			, nodeName
			, crmContactFullname
			, crmContactFirstName
			, crmContactLastName
			, crmContactJobTitle
			, crmContactPhoneNumber
			, crmContactFaxNumber
			, crmContactEmailAddress
			, phgEmployeeCity
			, phgEmployeeState
			, isDisabled
		FROM (SELECT 'SOURCE' AS TableName
				, SystemUserId AS crmContactGuid
				, FullName AS nodeName
				, FullName AS crmContactFullname
				, FirstName AS crmContactFirstName
				, LastName AS crmContactLastName
				, Title AS crmContactJobTitle
				, Address1_Telephone1 AS crmContactPhoneNumber
				, Address1_Fax AS crmContactFaxNumber
				, DomainName AS crmContactEmailAddress
				, Address1_City AS phgEmployeeCity
				, Address1_StateOrProvince AS phgEmployeeState
				, 0 AS isDisabled
			FROM LocalCRM.dbo.SystemUser
			WHERE IsDisabled = 0
			UNION
			SELECT 'TARGET' AS TableName
				, umb.crmContactGuid AS crmContactGuid
				, ISNULL(umb.nodeName, '') AS nodeName
				, ISNULL(umb.crmContactFullName, '') AS crmContactFullname
				, ISNULL(umb.crmContactFirstName, '') AS crmContactFirstName
				, ISNULL(umb.crmContactLastName, '') AS crmContactLastName
				, ISNULL(umb.crmContactJobTitle, '') AS crmContactJobTitle
				, ISNULL(umb.crmContactPhoneNumber, '') AS crmContactPhoneNumber
				, ISNULL(umb.crmContactFaxNumber, '') AS crmContactFaxNumber
				, ISNULL(umb.crmContactEmailAddress, '') AS crmContactEmailAddress
				, ISNULL(umb.phgEmployeeCity, '') AS phgEmployeeCity
				, ISNULL(umb.phgEmployeeState, '') AS phgEmployeeState
				, umb.[disabled] AS isDisabled
			FROM @denormalizedUmbracoPhgEmployee umb) unioned
		GROUP BY crmContactGuid
			, nodeName
			, crmContactFullname
			, crmContactFirstName
			, crmContactLastName
			, crmContactJobTitle
			, crmContactPhoneNumber
			, crmContactFaxNumber
			, crmContactEmailAddress
			, phgEmployeeCity
			, phgEmployeeState
			, isDisabled
		HAVING COUNT(*) = 1;


		INSERT INTO dbo.PhgEmployees ([Name],crmContactGuid,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,
												crmContactFaxNumber,crmContactEmailAddress,crmContactDisabled,phgEmployeeCity,phgEmployeeState)
		SELECT crmContactFullname
			, crmContactGuid
			, crmContactFullname
			, crmContactFirstName
			, crmContactLastName
			, crmContactJobTitle
			, crmContactPhoneNumber
			, crmContactFaxNumber
			, crmContactEmailAddress
			, 0
			, phgEmployeeCity
			, phgEmployeeState
		FROM @EmployeeData
		WHERE TableName = 'SOURCE';


		INSERT INTO dbo.PhgEmployees([Name],crmContactGuid,crmContactFullname,crmContactFirstName,crmContactLastName,crmContactJobTitle,crmContactPhoneNumber,crmContactFaxNumber,
										crmContactEmailAddress,crmContactDisabled,phgEmployeeCity,phgEmployeeState)
		SELECT umb.crmContactFullname
			, umb.crmContactGuid
			, umb.crmContactFullname
			, umb.crmContactFirstName
			, umb.crmContactLastName
			, umb.crmContactJobTitle
			, umb.crmContactPhoneNumber
			, umb.crmContactFaxNumber
			, umb.crmContactEmailAddress
			, 1
			, umb.phgEmployeeCity
			, umb.phgEmployeeState
		FROM @EmployeeData T
		INNER JOIN @denormalizedUmbracoPhgEmployee umb
			ON umb.crmContactGuid = T.crmContactGuid
		AND umb.[disabled] = 0
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS (SELECT 1
							FROM @EmployeeData S
							WHERE S.TableName = 'SOURCE'
							AND T.crmContactGuid = S.crmContactGuid);

	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
