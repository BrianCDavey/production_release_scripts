/*
Run this script on:

        AZ-SQ-PR-01.MemberPortal    -  This database will be modified

to synchronize it with:

        (local)\CHISQZ01.MemberPortal

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 1/8/2025 4:03:48 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[UFRecordAudit]'
GO
ALTER TABLE [dbo].[UFRecordAudit] DROP CONSTRAINT [FK_UFRecordAudit_UFRecords_Id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[UFRecordFields]'
GO
ALTER TABLE [dbo].[UFRecordFields] DROP CONSTRAINT [FK_UFRecordFields_UFRecords_Record]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContentNu]'
GO
ALTER TABLE [dbo].[cmsContentNu] DROP CONSTRAINT [FK_cmsContentNu_umbracoContent_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContentTypeAllowedContentType]'
GO
ALTER TABLE [dbo].[cmsContentTypeAllowedContentType] DROP CONSTRAINT [FK_cmsContentTypeAllowedContentType_cmsContentType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsContentTypeAllowedContentType] DROP CONSTRAINT [FK_cmsContentTypeAllowedContentType_cmsContentType1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsDocumentType]'
GO
ALTER TABLE [dbo].[cmsDocumentType] DROP CONSTRAINT [FK_cmsDocumentType_cmsContentType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsDocumentType] DROP CONSTRAINT [FK_cmsDocumentType_cmsTemplate_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsDocumentType] DROP CONSTRAINT [FK_cmsDocumentType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsLanguageText]'
GO
ALTER TABLE [dbo].[cmsLanguageText] DROP CONSTRAINT [FK_cmsLanguageText_cmsDictionary_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsLanguageText] DROP CONSTRAINT [FK_cmsLanguageText_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMemberType]'
GO
ALTER TABLE [dbo].[cmsMemberType] DROP CONSTRAINT [FK_cmsMemberType_cmsContentType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsMemberType] DROP CONSTRAINT [FK_cmsMemberType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMember]'
GO
ALTER TABLE [dbo].[cmsMember] DROP CONSTRAINT [FK_cmsMember_umbracoContent_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsPropertyTypeGroup]'
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] DROP CONSTRAINT [FK_cmsPropertyTypeGroup_cmsContentType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsPropertyType]'
GO
ALTER TABLE [dbo].[cmsPropertyType] DROP CONSTRAINT [FK_cmsPropertyType_cmsContentType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsPropertyType] DROP CONSTRAINT [FK_cmsPropertyType_umbracoDataType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsTagRelationship]'
GO
ALTER TABLE [dbo].[cmsTagRelationship] DROP CONSTRAINT [FK_cmsTagRelationship_cmsContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsTags]'
GO
ALTER TABLE [dbo].[cmsTags] DROP CONSTRAINT [FK_cmsTags_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsTemplate]'
GO
ALTER TABLE [dbo].[cmsTemplate] DROP CONSTRAINT [FK_cmsTemplate_umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoContentSchedule]'
GO
ALTER TABLE [dbo].[umbracoContentSchedule] DROP CONSTRAINT [FK_umbracoContentSchedule_umbracoContent_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoContentSchedule] DROP CONSTRAINT [FK_umbracoContentSchedule_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoContentVersionCleanupPolicy]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCleanupPolicy] DROP CONSTRAINT [FK_umbracoContentVersionCleanupPolicy_cmsContentType_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoContentVersionCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] DROP CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoContentVersion_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] DROP CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] DROP CONSTRAINT [FK_umbracoContentVersionCultureVariation_umbracoUser_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoContentVersion]'
GO
ALTER TABLE [dbo].[umbracoContentVersion] DROP CONSTRAINT [FK_umbracoContentVersion_umbracoContent_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoContentVersion] DROP CONSTRAINT [FK_umbracoContentVersion_umbracoUser_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoContent]'
GO
ALTER TABLE [dbo].[umbracoContent] DROP CONSTRAINT [FK_umbracoContent_cmsContentType_NodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoContent] DROP CONSTRAINT [FK_umbracoContent_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoDataType]'
GO
ALTER TABLE [dbo].[umbracoDataType] DROP CONSTRAINT [FK_umbracoDataType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoDocumentCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] DROP CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] DROP CONSTRAINT [FK_umbracoDocumentCultureVariation_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoDocumentVersion]'
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] DROP CONSTRAINT [FK_umbracoDocumentVersion_cmsTemplate_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] DROP CONSTRAINT [FK_umbracoDocumentVersion_umbracoContentVersion_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoDocument]'
GO
ALTER TABLE [dbo].[umbracoDocument] DROP CONSTRAINT [FK_umbracoDocument_umbracoContent_nodeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoDomain]'
GO
ALTER TABLE [dbo].[umbracoDomain] DROP CONSTRAINT [FK_umbracoDomain_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoExternalLoginToken]'
GO
ALTER TABLE [dbo].[umbracoExternalLoginToken] DROP CONSTRAINT [FK_umbracoExternalLoginToken_umbracoExternalLogin_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoLanguage]'
GO
ALTER TABLE [dbo].[umbracoLanguage] DROP CONSTRAINT [FK_umbracoLanguage_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoLog]'
GO
ALTER TABLE [dbo].[umbracoLog] DROP CONSTRAINT [FK_umbracoLog_umbracoUser_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoMediaVersion]'
GO
ALTER TABLE [dbo].[umbracoMediaVersion] DROP CONSTRAINT [FK_umbracoMediaVersion_umbracoContentVersion_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] DROP CONSTRAINT [FK_umbracoNode_umbracoUser_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoNode] DROP CONSTRAINT [FK_umbracoNode_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoPropertyData]'
GO
ALTER TABLE [dbo].[umbracoPropertyData] DROP CONSTRAINT [FK_umbracoPropertyData_cmsPropertyType_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoPropertyData] DROP CONSTRAINT [FK_umbracoPropertyData_umbracoContentVersion_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoPropertyData] DROP CONSTRAINT [FK_umbracoPropertyData_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoRedirectUrl]'
GO
ALTER TABLE [dbo].[umbracoRedirectUrl] DROP CONSTRAINT [FK_umbracoRedirectUrl_umbracoNode_uniqueID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserGroup2Language]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Language] DROP CONSTRAINT [FK_umbracoUserGroup2Language_umbracoLanguage_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoUserGroup2Language] DROP CONSTRAINT [FK_umbracoUserGroup2Language_umbracoUserGroup_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserGroup2Node]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Node] DROP CONSTRAINT [FK_umbracoUserGroup2Node_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoUserGroup2Node] DROP CONSTRAINT [FK_umbracoUserGroup2Node_umbracoUserGroup_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoAccessRule]'
GO
ALTER TABLE [dbo].[umbracoAccessRule] DROP CONSTRAINT [FK_umbracoAccessRule_umbracoAccess_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContentType]'
GO
ALTER TABLE [dbo].[cmsContentType] DROP CONSTRAINT [FK_cmsContentType_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsContentType2ContentType]'
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] DROP CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_child]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] DROP CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_parent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[cmsMember2MemberGroup]'
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] DROP CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserGroup]'
GO
ALTER TABLE [dbo].[umbracoUserGroup] DROP CONSTRAINT [FK_startContentId_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoUserGroup] DROP CONSTRAINT [FK_startMediaId_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoAccess]'
GO
ALTER TABLE [dbo].[umbracoAccess] DROP CONSTRAINT [FK_umbracoAccess_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoAccess] DROP CONSTRAINT [FK_umbracoAccess_umbracoNode_id1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoAccess] DROP CONSTRAINT [FK_umbracoAccess_umbracoNode_id2]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoRelation]'
GO
ALTER TABLE [dbo].[umbracoRelation] DROP CONSTRAINT [FK_umbracoRelation_umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelation] DROP CONSTRAINT [FK_umbracoRelation_umbracoNode1]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUser2NodeNotify]'
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify] DROP CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserGroup2NodePermission]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] DROP CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[umbracoUserStartNode]'
GO
ALTER TABLE [dbo].[umbracoUserStartNode] DROP CONSTRAINT [FK_umbracoUserStartNode_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportMediaRelation]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation] DROP CONSTRAINT [PK_CMSImportMediaRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportMediaRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation_old] DROP CONSTRAINT [PK__CMSImportMediaRelation__000000000000043D]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation]'
GO
ALTER TABLE [dbo].[CMSImportRelation] DROP CONSTRAINT [PK_CMSImportRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportRelation_old] DROP CONSTRAINT [PK__CMSImportRelation__000000000000042C]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledItems_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems_old] DROP CONSTRAINT [PK__CMSImportScheduledItems__0000000000000453]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledTaskDefinition]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTaskDefinition] DROP CONSTRAINT [PK_CMSImportScheduledTaskDefinition]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledTaskResult]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTaskResult] DROP CONSTRAINT [PK_CMSImportScheduledTaskResult]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledTask_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTask_old] DROP CONSTRAINT [PK__CMSImportScheduledTask__0000000000000416]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] DROP CONSTRAINT [PK_CMSImportState]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState_old]'
GO
ALTER TABLE [dbo].[CMSImportState_old] DROP CONSTRAINT [PK__CMSImportState__00000000000003F9]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFRecordAudit]'
GO
ALTER TABLE [dbo].[UFRecordAudit] DROP CONSTRAINT [PK_UFRecordAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFRecordWorkflowAudit]'
GO
ALTER TABLE [dbo].[UFRecordWorkflowAudit] DROP CONSTRAINT [PK_UFRecordWorkflowAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserFormSecurity]'
GO
ALTER TABLE [dbo].[UFUserFormSecurity] DROP CONSTRAINT [UK_UFUserFormSecurity_User_Form]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserGroupFormSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupFormSecurity] DROP CONSTRAINT [PK_UserGroupFormSecurity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserGroupFormSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupFormSecurity] DROP CONSTRAINT [UK_UFUserGroupFormSecurity_UserGroupId_Form]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserGroupSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupSecurity] DROP CONSTRAINT [PK_UFUserGroupSecurity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserSecurity]'
GO
ALTER TABLE [dbo].[UFUserSecurity] DROP CONSTRAINT [PK_UFUserSecurity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContentNu]'
GO
ALTER TABLE [dbo].[cmsContentNu] DROP CONSTRAINT [PK_cmsContentNu]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsTemplate]'
GO
ALTER TABLE [dbo].[cmsTemplate] DROP CONSTRAINT [PK_cmsTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContentSchedule]'
GO
ALTER TABLE [dbo].[umbracoContentSchedule] DROP CONSTRAINT [PK_umbracoContentSchedule]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContentVersionCleanupPolicy]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCleanupPolicy] DROP CONSTRAINT [PK_umbracoContentVersionCleanupPolicy]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContentVersionCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoContentVersionCultureVariation] DROP CONSTRAINT [PK_umbracoContentVersionCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContentVersion]'
GO
ALTER TABLE [dbo].[umbracoContentVersion] DROP CONSTRAINT [PK_umbracoContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContent]'
GO
ALTER TABLE [dbo].[umbracoContent] DROP CONSTRAINT [PK_umbracoContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoCreatedPackageSchema]'
GO
ALTER TABLE [dbo].[umbracoCreatedPackageSchema] DROP CONSTRAINT [PK_umbracoCreatedPackageSchema]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoDataType]'
GO
ALTER TABLE [dbo].[umbracoDataType] DROP CONSTRAINT [PK_umbracoDataType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoDocumentCultureVariation]'
GO
ALTER TABLE [dbo].[umbracoDocumentCultureVariation] DROP CONSTRAINT [PK_umbracoDocumentCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoDocumentVersion]'
GO
ALTER TABLE [dbo].[umbracoDocumentVersion] DROP CONSTRAINT [PK_umbracoDocumentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoDocument]'
GO
ALTER TABLE [dbo].[umbracoDocument] DROP CONSTRAINT [PK_umbracoDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoDomain]'
GO
ALTER TABLE [dbo].[umbracoDomain] DROP CONSTRAINT [PK_umbracoDomain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoExternalLoginToken]'
GO
ALTER TABLE [dbo].[umbracoExternalLoginToken] DROP CONSTRAINT [PK_umbracoExternalLoginToken]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] DROP CONSTRAINT [PK_umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoKeyValue]'
GO
ALTER TABLE [dbo].[umbracoKeyValue] DROP CONSTRAINT [PK_umbracoKeyValue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoLogViewerQuery]'
GO
ALTER TABLE [dbo].[umbracoLogViewerQuery] DROP CONSTRAINT [PK_umbracoLogViewerQuery]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoMediaVersion]'
GO
ALTER TABLE [dbo].[umbracoMediaVersion] DROP CONSTRAINT [PK_umbracoMediaVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] DROP CONSTRAINT [PK_umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoPropertyData]'
GO
ALTER TABLE [dbo].[umbracoPropertyData] DROP CONSTRAINT [PK_umbracoPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoTwoFactorLogin]'
GO
ALTER TABLE [dbo].[umbracoTwoFactorLogin] DROP CONSTRAINT [PK_umbracoTwoFactorLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoUserGroup2Language]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Language] DROP CONSTRAINT [PK_userGroup2language]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoUserGroup2Node]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2Node] DROP CONSTRAINT [PK_umbracoUserGroup2Node]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportMediaRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation_old] DROP CONSTRAINT [DF__CMSImport__ByteS__7EF6D905]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportRelation_old] DROP CONSTRAINT [DF__CMSImport__Impor__00DF2177]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportRelation_old]'
GO
ALTER TABLE [dbo].[CMSImportRelation_old] DROP CONSTRAINT [DF__CMSImport__Updat__01D345B0]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledItems_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems_old] DROP CONSTRAINT [DF__CMSImport__Execu__03BB8E22]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledItems_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems_old] DROP CONSTRAINT [DF__CMSImport__InPro__04AFB25B]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportScheduledTask_old]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTask_old] DROP CONSTRAINT [DF__CMSImport__Impor__0697FACD]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState_old]'
GO
ALTER TABLE [dbo].[CMSImportState_old] DROP CONSTRAINT [DF__CMSImport__Paren__0880433F]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[CMSImportState_old]'
GO
ALTER TABLE [dbo].[CMSImportState_old] DROP CONSTRAINT [DF__CMSImport__Impor__09746778]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserGroupSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupSecurity] DROP CONSTRAINT [DF_UFUserGroupSecurity_ViewEntries]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserGroupSecurity]'
GO
ALTER TABLE [dbo].[UFUserGroupSecurity] DROP CONSTRAINT [DF_UFUserGroupSecurity_EditEntries]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserSecurity]'
GO
ALTER TABLE [dbo].[UFUserSecurity] DROP CONSTRAINT [DF_UFUserSecurity_ViewEntries]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[UFUserSecurity]'
GO
ALTER TABLE [dbo].[UFUserSecurity] DROP CONSTRAINT [DF_UFUserSecurity_EditEntries]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContentType]'
GO
ALTER TABLE [dbo].[cmsContentType] DROP CONSTRAINT [DF_cmsContentType_variations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsContentType]'
GO
ALTER TABLE [dbo].[cmsContentType] DROP CONSTRAINT [DF_cmsContentType_isElement]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMember]'
GO
ALTER TABLE [dbo].[cmsMember] DROP CONSTRAINT [DF_cmsMember_isLockedOut]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsMember]'
GO
ALTER TABLE [dbo].[cmsMember] DROP CONSTRAINT [DF_cmsMember_isApproved]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPropertyTypeGroup]'
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] DROP CONSTRAINT [DF_cmsPropertyTypeGroup_type]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPropertyType]'
GO
ALTER TABLE [dbo].[cmsPropertyType] DROP CONSTRAINT [DF_cmsPropertyType_variations]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[cmsPropertyType]'
GO
ALTER TABLE [dbo].[cmsPropertyType] DROP CONSTRAINT [DF_cmsPropertyType_labelOnTop]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContentVersion]'
GO
ALTER TABLE [dbo].[umbracoContentVersion] DROP CONSTRAINT [DF__cmsConten__Versi__74794A92]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoContentVersion]'
GO
ALTER TABLE [dbo].[umbracoContentVersion] DROP CONSTRAINT [DF_umbracoContentVersion_preventCleanup]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoCreatedPackageSchema]'
GO
ALTER TABLE [dbo].[umbracoCreatedPackageSchema] DROP CONSTRAINT [DF_umbracoCreatedPackageSchema_updateDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoExternalLoginToken]'
GO
ALTER TABLE [dbo].[umbracoExternalLoginToken] DROP CONSTRAINT [DF_umbracoExternalLoginToken_createDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] DROP CONSTRAINT [DF_umbracoExternalLogin_createDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoKeyValue]'
GO
ALTER TABLE [dbo].[umbracoKeyValue] DROP CONSTRAINT [DF_umbracoKeyValue_updated]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoLanguage]'
GO
ALTER TABLE [dbo].[umbracoLanguage] DROP CONSTRAINT [DF_umbracoLanguage_isDefaultVariantLang]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoLanguage]'
GO
ALTER TABLE [dbo].[umbracoLanguage] DROP CONSTRAINT [DF_umbracoLanguage_mandatory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoRelationType]'
GO
ALTER TABLE [dbo].[umbracoRelationType] DROP CONSTRAINT [DF_umbracoRelationType_isDependency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[umbracoUserGroup]'
GO
ALTER TABLE [dbo].[umbracoUserGroup] DROP CONSTRAINT [DF_umbracoUserGroup_hasAccessToAllLanguages]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Record_RecordAudit] from [dbo].[UFRecordAudit]'
GO
DROP INDEX [IX_Record_RecordAudit] ON [dbo].[UFRecordAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Record_UpdatedBy] from [dbo].[UFRecordAudit]'
GO
DROP INDEX [IX_Record_UpdatedBy] ON [dbo].[UFRecordAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_UFRecordDataBit_Key] from [dbo].[UFRecordDataBit]'
GO
DROP INDEX [IX_UFRecordDataBit_Key] ON [dbo].[UFRecordDataBit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_UFRecordDataDateTime_Key] from [dbo].[UFRecordDataDateTime]'
GO
DROP INDEX [IX_UFRecordDataDateTime_Key] ON [dbo].[UFRecordDataDateTime]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_UFRecordDataInteger_Key] from [dbo].[UFRecordDataInteger]'
GO
DROP INDEX [IX_UFRecordDataInteger_Key] ON [dbo].[UFRecordDataInteger]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_UFRecordDataLongString_Key] from [dbo].[UFRecordDataLongString]'
GO
DROP INDEX [IX_UFRecordDataLongString_Key] ON [dbo].[UFRecordDataLongString]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_RecordUniqueId_RecordWorkflowAudit] from [dbo].[UFRecordWorkflowAudit]'
GO
DROP INDEX [IX_RecordUniqueId_RecordWorkflowAudit] ON [dbo].[UFRecordWorkflowAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_WorkflowKey_RecordWorkflowAudit] from [dbo].[UFRecordWorkflowAudit]'
GO
DROP INDEX [IX_WorkflowKey_RecordWorkflowAudit] ON [dbo].[UFRecordWorkflowAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDictionary_key] from [dbo].[cmsDictionary]'
GO
DROP INDEX [IX_cmsDictionary_key] ON [dbo].[cmsDictionary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsDictionary_Parent] from [dbo].[cmsDictionary]'
GO
DROP INDEX [IX_cmsDictionary_Parent] ON [dbo].[cmsDictionary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsLanguageText_languageId] from [dbo].[cmsLanguageText]'
GO
DROP INDEX [IX_cmsLanguageText_languageId] ON [dbo].[cmsLanguageText]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsTags] from [dbo].[cmsTags]'
GO
DROP INDEX [IX_cmsTags] ON [dbo].[cmsTags]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsTags_LanguageId] from [dbo].[cmsTags]'
GO
DROP INDEX [IX_cmsTags_LanguageId] ON [dbo].[cmsTags]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_cmsTemplate_nodeId] from [dbo].[cmsTemplate]'
GO
DROP INDEX [IX_cmsTemplate_nodeId] ON [dbo].[cmsTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoAccessRule] from [dbo].[umbracoAccessRule]'
GO
DROP INDEX [IX_umbracoAccessRule] ON [dbo].[umbracoAccessRule]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoContentVersionCultureVariation_VersionId] from [dbo].[umbracoContentVersionCultureVariation]'
GO
DROP INDEX [IX_umbracoContentVersionCultureVariation_VersionId] ON [dbo].[umbracoContentVersionCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoContentVersionCultureVariation_LanguageId] from [dbo].[umbracoContentVersionCultureVariation]'
GO
DROP INDEX [IX_umbracoContentVersionCultureVariation_LanguageId] ON [dbo].[umbracoContentVersionCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoContentVersion_NodeId] from [dbo].[umbracoContentVersion]'
GO
DROP INDEX [IX_umbracoContentVersion_NodeId] ON [dbo].[umbracoContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoContentVersion_Current] from [dbo].[umbracoContentVersion]'
GO
DROP INDEX [IX_umbracoContentVersion_Current] ON [dbo].[umbracoContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoCreatedPackageSchema_Name] from [dbo].[umbracoCreatedPackageSchema]'
GO
DROP INDEX [IX_umbracoCreatedPackageSchema_Name] ON [dbo].[umbracoCreatedPackageSchema]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoDocumentCultureVariation_NodeId] from [dbo].[umbracoDocumentCultureVariation]'
GO
DROP INDEX [IX_umbracoDocumentCultureVariation_NodeId] ON [dbo].[umbracoDocumentCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoDocumentCultureVariation_LanguageId] from [dbo].[umbracoDocumentCultureVariation]'
GO
DROP INDEX [IX_umbracoDocumentCultureVariation_LanguageId] ON [dbo].[umbracoDocumentCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoDocument_Published] from [dbo].[umbracoDocument]'
GO
DROP INDEX [IX_umbracoDocument_Published] ON [dbo].[umbracoDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoExternalLoginToken_Name] from [dbo].[umbracoExternalLoginToken]'
GO
DROP INDEX [IX_umbracoExternalLoginToken_Name] ON [dbo].[umbracoExternalLoginToken]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoExternalLogin_ProviderKey] from [dbo].[umbracoExternalLogin]'
GO
DROP INDEX [IX_umbracoExternalLogin_ProviderKey] ON [dbo].[umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoExternalLogin_LoginProvider] from [dbo].[umbracoExternalLogin]'
GO
DROP INDEX [IX_umbracoExternalLogin_LoginProvider] ON [dbo].[umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoLanguage_fallbackLanguageId] from [dbo].[umbracoLanguage]'
GO
DROP INDEX [IX_umbracoLanguage_fallbackLanguageId] ON [dbo].[umbracoLanguage]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LogViewerQuery_name] from [dbo].[umbracoLogViewerQuery]'
GO
DROP INDEX [IX_LogViewerQuery_name] ON [dbo].[umbracoLogViewerQuery]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoMediaVersion] from [dbo].[umbracoMediaVersion]'
GO
DROP INDEX [IX_umbracoMediaVersion] ON [dbo].[umbracoMediaVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_UniqueId] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_UniqueId] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_Level] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_Level] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_ObjectType] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_ObjectType] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_ParentId] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_ParentId] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_Path] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_Path] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoNode_Trashed] from [dbo].[umbracoNode]'
GO
DROP INDEX [IX_umbracoNode_Trashed] ON [dbo].[umbracoNode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoPropertyData_PropertyTypeId] from [dbo].[umbracoPropertyData]'
GO
DROP INDEX [IX_umbracoPropertyData_PropertyTypeId] ON [dbo].[umbracoPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoPropertyData_VersionId] from [dbo].[umbracoPropertyData]'
GO
DROP INDEX [IX_umbracoPropertyData_VersionId] ON [dbo].[umbracoPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoPropertyData_LanguageId] from [dbo].[umbracoPropertyData]'
GO
DROP INDEX [IX_umbracoPropertyData_LanguageId] ON [dbo].[umbracoPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoPropertyData_Segment] from [dbo].[umbracoPropertyData]'
GO
DROP INDEX [IX_umbracoPropertyData_Segment] ON [dbo].[umbracoPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoRedirectUrl] from [dbo].[umbracoRedirectUrl]'
GO
DROP INDEX [IX_umbracoRedirectUrl] ON [dbo].[umbracoRedirectUrl]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoRelationType_alias] from [dbo].[umbracoRelationType]'
GO
DROP INDEX [IX_umbracoRelationType_alias] ON [dbo].[umbracoRelationType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoRelationType_name] from [dbo].[umbracoRelationType]'
GO
DROP INDEX [IX_umbracoRelationType_name] ON [dbo].[umbracoRelationType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoTwoFactorLogin_ProviderName] from [dbo].[umbracoTwoFactorLogin]'
GO
DROP INDEX [IX_umbracoTwoFactorLogin_ProviderName] ON [dbo].[umbracoTwoFactorLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoTwoFactorLogin_userOrMemberKey] from [dbo].[umbracoTwoFactorLogin]'
GO
DROP INDEX [IX_umbracoTwoFactorLogin_userOrMemberKey] ON [dbo].[umbracoTwoFactorLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoUserGroup2Node_nodeId] from [dbo].[umbracoUserGroup2Node]'
GO
DROP INDEX [IX_umbracoUserGroup2Node_nodeId] ON [dbo].[umbracoUserGroup2Node]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_umbracoUserLogin_lastValidatedUtc] from [dbo].[umbracoUserLogin]'
GO
DROP INDEX [IX_umbracoUserLogin_lastValidatedUtc] ON [dbo].[umbracoUserLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoTwoFactorLogin]'
GO
DROP TABLE [dbo].[umbracoTwoFactorLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoLogViewerQuery]'
GO
DROP TABLE [dbo].[umbracoLogViewerQuery]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoKeyValue]'
GO
DROP TABLE [dbo].[umbracoKeyValue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoCreatedPackageSchema]'
GO
DROP TABLE [dbo].[umbracoCreatedPackageSchema]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UFUserGroupSecurity]'
GO
DROP TABLE [dbo].[UFUserGroupSecurity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UFUserGroupFormSecurity]'
GO
DROP TABLE [dbo].[UFUserGroupFormSecurity]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UFRecordWorkflowAudit]'
GO
DROP TABLE [dbo].[UFRecordWorkflowAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportState_old]'
GO
DROP TABLE [dbo].[CMSImportState_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportScheduledTask_old]'
GO
DROP TABLE [dbo].[CMSImportScheduledTask_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportScheduledTaskResult]'
GO
DROP TABLE [dbo].[CMSImportScheduledTaskResult]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportScheduledTaskDefinition]'
GO
DROP TABLE [dbo].[CMSImportScheduledTaskDefinition]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportScheduledItems_old]'
GO
DROP TABLE [dbo].[CMSImportScheduledItems_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportRelation_old]'
GO
DROP TABLE [dbo].[CMSImportRelation_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CMSImportMediaRelation_old]'
GO
DROP TABLE [dbo].[CMSImportMediaRelation_old]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoUserGroup2Node]'
GO
DROP TABLE [dbo].[umbracoUserGroup2Node]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoUserGroup2Language]'
GO
DROP TABLE [dbo].[umbracoUserGroup2Language]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoPropertyData]'
GO
DROP TABLE [dbo].[umbracoPropertyData]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoMediaVersion]'
GO
DROP TABLE [dbo].[umbracoMediaVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoExternalLoginToken]'
GO
DROP TABLE [dbo].[umbracoExternalLoginToken]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoDomain]'
GO
DROP TABLE [dbo].[umbracoDomain]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoDocumentVersion]'
GO
DROP TABLE [dbo].[umbracoDocumentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoDocumentCultureVariation]'
GO
DROP TABLE [dbo].[umbracoDocumentCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoDocument]'
GO
DROP TABLE [dbo].[umbracoDocument]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoContentVersionCultureVariation]'
GO
DROP TABLE [dbo].[umbracoContentVersionCultureVariation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoContentVersionCleanupPolicy]'
GO
DROP TABLE [dbo].[umbracoContentVersionCleanupPolicy]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoContentVersion]'
GO
DROP TABLE [dbo].[umbracoContentVersion]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoContentSchedule]'
GO
DROP TABLE [dbo].[umbracoContentSchedule]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UFRecordAudit]'
GO
DROP TABLE [dbo].[UFRecordAudit]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoDataType]'
GO
DROP TABLE [dbo].[umbracoDataType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[cmsContentNu]'
GO
DROP TABLE [dbo].[cmsContentNu]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[umbracoContent]'
GO
DROP TABLE [dbo].[umbracoContent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsContentType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsContentType] DROP
COLUMN [variations],
COLUMN [isElement]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsContent]'
GO
CREATE TABLE [dbo].[cmsContent]
(
[pk] [int] NOT NULL IDENTITY(174, 1),
[nodeId] [int] NOT NULL,
[contentType] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsContent] on [dbo].[cmsContent]'
GO
ALTER TABLE [dbo].[cmsContent] ADD CONSTRAINT [PK_cmsContent] PRIMARY KEY CLUSTERED ([pk])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsContent] on [dbo].[cmsContent]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsContent] ON [dbo].[cmsContent] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsDataType]'
GO
CREATE TABLE [dbo].[cmsDataType]
(
[pk] [int] NOT NULL IDENTITY(33, 1),
[nodeId] [int] NOT NULL,
[propertyEditorAlias] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[dbType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsDataType] on [dbo].[cmsDataType]'
GO
ALTER TABLE [dbo].[cmsDataType] ADD CONSTRAINT [PK_cmsDataType] PRIMARY KEY CLUSTERED ([pk])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDataType_nodeId] on [dbo].[cmsDataType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsDataType_nodeId] ON [dbo].[cmsDataType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsDocument]'
GO
CREATE TABLE [dbo].[cmsDocument]
(
[nodeId] [int] NOT NULL,
[published] [bit] NOT NULL,
[documentUser] [int] NOT NULL,
[versionId] [uniqueidentifier] NOT NULL,
[text] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[releaseDate] [datetime] NULL,
[expireDate] [datetime] NULL,
[updateDate] [datetime] NOT NULL CONSTRAINT [DF__cmsDocume__updat__7A3223E8] DEFAULT (getdate()),
[templateId] [int] NULL,
[newest] [bit] NOT NULL CONSTRAINT [DF__cmsDocume__newes__7B264821] DEFAULT ('0')
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsDocument] on [dbo].[cmsDocument]'
GO
ALTER TABLE [dbo].[cmsDocument] ADD CONSTRAINT [PK_cmsDocument] PRIMARY KEY CLUSTERED ([versionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDocument_newest] on [dbo].[cmsDocument]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsDocument_newest] ON [dbo].[cmsDocument] ([newest])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDocument] on [dbo].[cmsDocument]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsDocument] ON [dbo].[cmsDocument] ([nodeId], [versionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDocument_published] on [dbo].[cmsDocument]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsDocument_published] ON [dbo].[cmsDocument] ([published])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoLanguage]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoLanguage] DROP
COLUMN [isDefaultVariantLang],
COLUMN [mandatory],
COLUMN [fallbackLanguageId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsMacro]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsMacro] ADD
[macroScriptType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[macroScriptAssembly] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[macroXSLT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[macroPython] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsMacro] DROP
COLUMN [macroType],
COLUMN [macroSource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsMedia]'
GO
CREATE TABLE [dbo].[cmsMedia]
(
[nodeId] [int] NOT NULL,
[versionId] [uniqueidentifier] NOT NULL,
[mediaPath] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsMedia] on [dbo].[cmsMedia]'
GO
ALTER TABLE [dbo].[cmsMedia] ADD CONSTRAINT [PK_cmsMedia] PRIMARY KEY CLUSTERED ([versionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsMedia] on [dbo].[cmsMedia]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsMedia] ON [dbo].[cmsMedia] ([nodeId], [versionId], [mediaPath])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsMember]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsMember] DROP
COLUMN [securityStampToken],
COLUMN [emailConfirmedDate],
COLUMN [passwordConfig],
COLUMN [failedPasswordAttempts],
COLUMN [isLockedOut],
COLUMN [isApproved],
COLUMN [lastLoginDate],
COLUMN [lastLockoutDate],
COLUMN [lastPasswordChangeDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsPropertyTypeGroup]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsPropertyTypeGroup] DROP
COLUMN [type],
COLUMN [alias]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsPropertyType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsPropertyType] DROP
COLUMN [variations],
COLUMN [mandatoryMessage],
COLUMN [validationRegExpMessage],
COLUMN [labelOnTop]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[cmsTags]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[cmsTags].[languageId]', N'ParentId', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTags] ALTER COLUMN [tag] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[cmsTags] ALTER COLUMN [group] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsTags] on [dbo].[cmsTags]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsTags] ON [dbo].[cmsTags] ([tag], [group])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsTaskType]'
GO
CREATE TABLE [dbo].[cmsTaskType]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[alias] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsTaskType] on [dbo].[cmsTaskType]'
GO
ALTER TABLE [dbo].[cmsTaskType] ADD CONSTRAINT [PK_cmsTaskType] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsTaskType_alias] on [dbo].[cmsTaskType]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsTaskType_alias] ON [dbo].[cmsTaskType] ([alias])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsTask]'
GO
CREATE TABLE [dbo].[cmsTask]
(
[closed] [bit] NOT NULL CONSTRAINT [DF__cmsTask__closed__251C81ED] DEFAULT ('0'),
[id] [int] NOT NULL IDENTITY(1, 1),
[taskTypeId] [int] NOT NULL,
[nodeId] [int] NOT NULL,
[parentUserId] [int] NOT NULL,
[userId] [int] NOT NULL,
[DateTime] [datetime] NOT NULL CONSTRAINT [DF__cmsTask__DateTim__2610A626] DEFAULT (getdate()),
[Comment] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsTask] on [dbo].[cmsTask]'
GO
ALTER TABLE [dbo].[cmsTask] ADD CONSTRAINT [PK_cmsTask] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[cmsTemplate]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_cmsTemplate]
(
[pk] [int] NOT NULL IDENTITY(11, 1),
[nodeId] [int] NOT NULL,
[alias] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[design] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_cmsTemplate] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_1_cmsTemplate]([pk], [nodeId], [alias]) SELECT [pk], [nodeId], [alias] FROM [dbo].[cmsTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_cmsTemplate] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[cmsTemplate]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_cmsTemplate]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[cmsTemplate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_cmsTemplate]', N'cmsTemplate', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsTemplate] on [dbo].[cmsTemplate]'
GO
ALTER TABLE [dbo].[cmsTemplate] ADD CONSTRAINT [PK_cmsTemplate] PRIMARY KEY CLUSTERED ([pk])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsTemplate_nodeId] on [dbo].[cmsTemplate]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsTemplate_nodeId] ON [dbo].[cmsTemplate] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoUserGroup]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoUserGroup] DROP
COLUMN [hasAccessToAllLanguages]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoDomains]'
GO
CREATE TABLE [dbo].[umbracoDomains]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[domainDefaultLanguage] [int] NULL,
[domainRootStructureID] [int] NULL,
[domainName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoDomains] on [dbo].[umbracoDomains]'
GO
ALTER TABLE [dbo].[umbracoDomains] ADD CONSTRAINT [PK_umbracoDomains] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[umbracoExternalLogin]'
GO
CREATE TABLE [dbo].[RG_Recovery_2_umbracoExternalLogin]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[userId] [int] NOT NULL,
[loginProvider] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[providerKey] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[createDate] [datetime] NOT NULL CONSTRAINT [DF_umbracoExternalLogin_createDate] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_2_umbracoExternalLogin] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_2_umbracoExternalLogin]([id], [loginProvider], [providerKey], [createDate]) SELECT [id], [loginProvider], [providerKey], [createDate] FROM [dbo].[umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_2_umbracoExternalLogin] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[umbracoExternalLogin]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_2_umbracoExternalLogin]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[umbracoExternalLogin]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_2_umbracoExternalLogin]', N'umbracoExternalLogin', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoExternalLogin] on [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] ADD CONSTRAINT [PK_umbracoExternalLogin] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoRedirectUrl]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRedirectUrl] DROP
COLUMN [culture]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoRedirectUrl] on [dbo].[umbracoRedirectUrl]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoRedirectUrl] ON [dbo].[umbracoRedirectUrl] ([urlHash], [contentKey], [createDateUtc])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoRelationType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] DROP
COLUMN [isDependency]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ALTER COLUMN [parentObjectType] [uniqueidentifier] NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ALTER COLUMN [childObjectType] [uniqueidentifier] NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoRelationType] ALTER COLUMN [alias] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoRelationType_alias] on [dbo].[umbracoRelationType]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoRelationType_alias] ON [dbo].[umbracoRelationType] ([alias])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoRelationType_name] on [dbo].[umbracoRelationType]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoRelationType_name] ON [dbo].[umbracoRelationType] ([name])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CMSImportState]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CMSImportState] ALTER COLUMN [Name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CMSImportState] ALTER COLUMN [ImportProvider] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] ADD CONSTRAINT [DF__CMSImport__Paren__0880433F] DEFAULT (NULL) FOR [Parent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CMSImportState] ADD CONSTRAINT [DF__CMSImport__Impor__09746778] DEFAULT (NULL) FOR [ImportProvider]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportState__00000000000003F9] on [dbo].[CMSImportState]'
GO
ALTER TABLE [dbo].[CMSImportState] ADD CONSTRAINT [PK__CMSImportState__00000000000003F9] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UFRecords]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[UFRecords] DROP
COLUMN [Culture]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UFUserSecurity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[UFUserSecurity] DROP
COLUMN [ViewEntries],
COLUMN [EditEntries]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoLog] DROP
COLUMN [entityType],
COLUMN [parameters]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[umbracoLog] ALTER COLUMN [userId] [int] NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[umbracoServer]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[umbracoServer].[isSchedulingPublisher]', N'isMaster', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportScheduledItems]'
GO
CREATE TABLE [dbo].[CMSImportScheduledItems]
(
[ScheduledItemId] [int] NOT NULL IDENTITY(1, 1),
[ScheduleldTaskId] [int] NOT NULL,
[ScheduledOn] [datetime] NOT NULL,
[ExecutedOn] [datetime] NULL CONSTRAINT [DF__CMSImport__Execu__03BB8E22] DEFAULT (NULL),
[InProgress] [bit] NULL CONSTRAINT [DF__CMSImport__InPro__04AFB25B] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportScheduledItems__0000000000000453] on [dbo].[CMSImportScheduledItems]'
GO
ALTER TABLE [dbo].[CMSImportScheduledItems] ADD CONSTRAINT [PK__CMSImportScheduledItems__0000000000000453] PRIMARY KEY CLUSTERED ([ScheduledItemId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CMSImportScheduledTask]'
GO
CREATE TABLE [dbo].[CMSImportScheduledTask]
(
[ScheduleId] [int] NOT NULL IDENTITY(1, 1),
[ScheduleGUID] [uniqueidentifier] NOT NULL,
[ImportStateGUID] [uniqueidentifier] NOT NULL,
[ScheduledTaskName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NotifyEmailAddress] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecuteEvery] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecuteDays] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExecuteHour] [int] NOT NULL,
[ExecuteMinute] [int] NOT NULL,
[ImportAsUser] [int] NULL CONSTRAINT [DF__CMSImport__Impor__0697FACD] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportScheduledTask__0000000000000416] on [dbo].[CMSImportScheduledTask]'
GO
ALTER TABLE [dbo].[CMSImportScheduledTask] ADD CONSTRAINT [PK__CMSImportScheduledTask__0000000000000416] PRIMARY KEY CLUSTERED ([ScheduleId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsContentVersion]'
GO
CREATE TABLE [dbo].[cmsContentVersion]
(
[id] [int] NOT NULL IDENTITY(546, 1),
[ContentId] [int] NOT NULL,
[VersionId] [uniqueidentifier] NOT NULL,
[VersionDate] [datetime] NOT NULL CONSTRAINT [DF__cmsConten__Versi__74794A92] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsContentVersion] on [dbo].[cmsContentVersion]'
GO
ALTER TABLE [dbo].[cmsContentVersion] ADD CONSTRAINT [PK_cmsContentVersion] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsContentVersion_ContentId] on [dbo].[cmsContentVersion]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsContentVersion_ContentId] ON [dbo].[cmsContentVersion] ([ContentId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsContentVersion_VersionId] on [dbo].[cmsContentVersion]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsContentVersion_VersionId] ON [dbo].[cmsContentVersion] ([VersionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsContentXml]'
GO
CREATE TABLE [dbo].[cmsContentXml]
(
[nodeId] [int] NOT NULL,
[xml] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsContentXml] on [dbo].[cmsContentXml]'
GO
ALTER TABLE [dbo].[cmsContentXml] ADD CONSTRAINT [PK_cmsContentXml] PRIMARY KEY CLUSTERED ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsDataTypePreValues]'
GO
CREATE TABLE [dbo].[cmsDataTypePreValues]
(
[id] [int] NOT NULL IDENTITY(17, 1),
[datatypeNodeId] [int] NOT NULL,
[value] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sortorder] [int] NOT NULL,
[alias] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsDataTypePreValues] on [dbo].[cmsDataTypePreValues]'
GO
ALTER TABLE [dbo].[cmsDataTypePreValues] ADD CONSTRAINT [PK_cmsDataTypePreValues] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsPreviewXml]'
GO
CREATE TABLE [dbo].[cmsPreviewXml]
(
[nodeId] [int] NOT NULL,
[versionId] [uniqueidentifier] NOT NULL,
[timestamp] [datetime] NOT NULL,
[xml] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsContentPreviewXml] on [dbo].[cmsPreviewXml]'
GO
ALTER TABLE [dbo].[cmsPreviewXml] ADD CONSTRAINT [PK_cmsContentPreviewXml] PRIMARY KEY CLUSTERED ([nodeId], [versionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[cmsPropertyData]'
GO
CREATE TABLE [dbo].[cmsPropertyData]
(
[id] [int] NOT NULL IDENTITY(2021, 1),
[contentNodeId] [int] NOT NULL,
[versionId] [uniqueidentifier] NULL,
[propertytypeid] [int] NOT NULL,
[dataInt] [int] NULL,
[dataDate] [datetime] NULL,
[dataNvarchar] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dataNtext] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dataDecimal] [decimal] (20, 9) NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_cmsPropertyData] on [dbo].[cmsPropertyData]'
GO
ALTER TABLE [dbo].[cmsPropertyData] ADD CONSTRAINT [PK_cmsPropertyData] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsPropertyData_1] on [dbo].[cmsPropertyData]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_cmsPropertyData_1] ON [dbo].[cmsPropertyData] ([contentNodeId], [versionId], [propertytypeid])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsPropertyData_3] on [dbo].[cmsPropertyData]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsPropertyData_3] ON [dbo].[cmsPropertyData] ([propertytypeid])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsPropertyData_2] on [dbo].[cmsPropertyData]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsPropertyData_2] ON [dbo].[cmsPropertyData] ([versionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[umbracoMigration]'
GO
CREATE TABLE [dbo].[umbracoMigration]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[version] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[createDate] [datetime] NOT NULL CONSTRAINT [DF_umbracoMigration_createDate] DEFAULT (getdate())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_umbracoMigrations] on [dbo].[umbracoMigration]'
GO
ALTER TABLE [dbo].[umbracoMigration] ADD CONSTRAINT [PK_umbracoMigrations] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoMigration] on [dbo].[umbracoMigration]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoMigration] ON [dbo].[umbracoMigration] ([name], [version])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CMSImportMediaRelation]'
GO
CREATE TABLE [dbo].[RG_Recovery_3_CMSImportMediaRelation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UmbracoMediaId] [int] NOT NULL,
[SourceUrl] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ByteSize] [int] NULL CONSTRAINT [DF__CMSImport__ByteS__7EF6D905] DEFAULT (NULL)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_3_CMSImportMediaRelation] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_3_CMSImportMediaRelation]([Id], [UmbracoMediaId], [SourceUrl], [ByteSize]) SELECT [Id], [UmbracoMediaId], [SourceUrl], CAST([ByteSize] AS [int]) FROM [dbo].[CMSImportMediaRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_3_CMSImportMediaRelation] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CMSImportMediaRelation]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_3_CMSImportMediaRelation]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CMSImportMediaRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_3_CMSImportMediaRelation]', N'CMSImportMediaRelation', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportMediaRelation__000000000000043D] on [dbo].[CMSImportMediaRelation]'
GO
ALTER TABLE [dbo].[CMSImportMediaRelation] ADD CONSTRAINT [PK__CMSImportMediaRelation__000000000000043D] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[CMSImportRelation]'
GO
CREATE TABLE [dbo].[RG_Recovery_4_CMSImportRelation]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UmbracoID] [int] NOT NULL,
[DataSourceKey] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImportProvider] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__CMSImport__Impor__00DF2177] DEFAULT (NULL),
[Updated] [datetime] NULL CONSTRAINT [DF__CMSImport__Updat__01D345B0] DEFAULT (NULL),
[CustomId] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_4_CMSImportRelation] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_4_CMSImportRelation]([Id], [UmbracoID], [DataSourceKey], [ImportProvider], [Updated]) SELECT [Id], [UmbracoID], [DatasourceKey], [ImportProvider], [Updated] FROM [dbo].[CMSImportRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_4_CMSImportRelation] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[CMSImportRelation]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_4_CMSImportRelation]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[CMSImportRelation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_4_CMSImportRelation]', N'CMSImportRelation', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__CMSImportRelation__000000000000042C] on [dbo].[CMSImportRelation]'
GO
ALTER TABLE [dbo].[CMSImportRelation] ADD CONSTRAINT [PK__CMSImportRelation__000000000000042C] PRIMARY KEY CLUSTERED ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MoveDevDataToStaging]'
GO
ALTER PROCEDURE [dbo].[MoveDevDataToStaging]
AS
BEGIN
	SET NOCOUNT ON;

	/*
	-- exec dbo.MoveDevDataToStaging

	set identity_insert MemberPortal.dbo.CMSImportMediaRelation ON

	truncate table MemberPortal.dbo.CMSImportMediaRelation
	insert into MemberPortal.dbo.CMSImportMediaRelation
		(	Id ,UmbracoMediaId , SourceUrl, ByteSize )
	select	Id ,UmbracoMediaId , SourceUrl, ByteSize
	from	miasqs01.MemberPortal.dbo.CMSImportMediaRelation

	set identity_insert MemberPortal.dbo.CMSImportMediaRelation OFF



	set identity_insert MemberPortal.dbo.CMSImportRelation ON

	truncate table MemberPortal.dbo.CMSImportRelation
	insert into MemberPortal.dbo.CMSImportRelation
		(	Id, UmbracoID, DataSourceKey, ImportProvider, Updated )
	select	Id, UmbracoID, DataSourceKey, ImportProvider, Updated
	from	miasqs01.MemberPortal.dbo.CMSImportRelation

	set identity_insert MemberPortal.dbo.CMSImportRelation OFF



	set identity_insert MemberPortal.dbo.CMSImportScheduledItems ON

	truncate table MemberPortal.dbo.CMSImportScheduledItems
	insert into MemberPortal.dbo.CMSImportScheduledItems
		(	ScheduledItemId, ScheduleldTaskId, ScheduledOn, ExecutedOn, InProgress )
	select	ScheduledItemId, ScheduleldTaskId, ScheduledOn, ExecutedOn, InProgress 
	from	miasqs01.MemberPortal.dbo.CMSImportScheduledItems

	set identity_insert MemberPortal.dbo.CMSImportScheduledItems OFF



	set identity_insert MemberPortal.dbo.CMSImportScheduledTask ON

	truncate table MemberPortal.dbo.CMSImportScheduledTask
	insert into MemberPortal.dbo.CMSImportScheduledTask
		(	ScheduleId, ScheduleGUID, ImportStateGUID, ScheduledTaskName, NotifyEmailAddress, ExecuteEvery, ExecuteDays, ExecuteHour, ExecuteMinute, ImportAsUser )
	select	ScheduleId, ScheduleGUID, ImportStateGUID, ScheduledTaskName, NotifyEmailAddress, ExecuteEvery, ExecuteDays, ExecuteHour, ExecuteMinute, ImportAsUser
	from	miasqs01.MemberPortal.dbo.CMSImportScheduledTask

	set identity_insert MemberPortal.dbo.CMSImportScheduledTask OFF



	set identity_insert MemberPortal.dbo.CMSImportState ON

	truncate table MemberPortal.dbo.CMSImportState
	insert into MemberPortal.dbo.CMSImportState
		(	Id, UniqueIdentifier, Name, ImportState, Parent, ImportProvider )
	select	Id, UniqueIdentifier, Name, ImportState, Parent, ImportProvider
	from	miasqs01.MemberPortal.dbo.CMSImportState

	set identity_insert MemberPortal.dbo.CMSImportState OFF

	*/
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_structure] on [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] ADD CONSTRAINT [PK_structure] PRIMARY KEY CLUSTERED ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_cmsDictionary_key] on [dbo].[cmsDictionary]'
GO
CREATE NONCLUSTERED INDEX [IX_cmsDictionary_key] ON [dbo].[cmsDictionary] ([key])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoAccessRule] on [dbo].[umbracoAccessRule]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoAccessRule] ON [dbo].[umbracoAccessRule] ([accessId], [ruleValue], [ruleType])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNodeObjectType] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNodeObjectType] ON [dbo].[umbracoNode] ([nodeObjectType])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNodeParentId] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNodeParentId] ON [dbo].[umbracoNode] ([parentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNodePath] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNodePath] ON [dbo].[umbracoNode] ([path])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNodeTrashed] on [dbo].[umbracoNode]'
GO
CREATE NONCLUSTERED INDEX [IX_umbracoNodeTrashed] ON [dbo].[umbracoNode] ([trashed])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_umbracoNode_uniqueID] on [dbo].[umbracoNode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_umbracoNode_uniqueID] ON [dbo].[umbracoNode] ([uniqueID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMedia]'
GO
ALTER TABLE [dbo].[cmsMedia] WITH NOCHECK  ADD CONSTRAINT [FK_cmsMedia_cmsContent_nodeId] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[cmsContent] ([nodeId])
GO
ALTER TABLE [dbo].[cmsMedia] WITH NOCHECK  ADD CONSTRAINT [FK_cmsMedia_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserGroup2NodePermission]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] WITH NOCHECK  ADD CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContent]'
GO
ALTER TABLE [dbo].[cmsContent] ADD CONSTRAINT [FK_cmsContent_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsContent] ADD CONSTRAINT [FK_cmsContent_cmsContentType_nodeId] FOREIGN KEY ([contentType]) REFERENCES [dbo].[cmsContentType] ([nodeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsDataType]'
GO
ALTER TABLE [dbo].[cmsDataType] ADD CONSTRAINT [FK_cmsDataType_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsDocument]'
GO
ALTER TABLE [dbo].[cmsDocument] ADD CONSTRAINT [FK_cmsDocument_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMember]'
GO
ALTER TABLE [dbo].[cmsMember] ADD CONSTRAINT [FK_cmsMember_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsTags]'
GO
ALTER TABLE [dbo].[cmsTags] ADD CONSTRAINT [FK_cmsTags_cmsTags] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[cmsTags] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsTask]'
GO
ALTER TABLE [dbo].[cmsTask] ADD CONSTRAINT [FK_cmsTask_cmsTaskType_id] FOREIGN KEY ([taskTypeId]) REFERENCES [dbo].[cmsTaskType] ([id])
GO
ALTER TABLE [dbo].[cmsTask] ADD CONSTRAINT [FK_cmsTask_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsTask] ADD CONSTRAINT [FK_cmsTask_umbracoUser] FOREIGN KEY ([parentUserId]) REFERENCES [dbo].[umbracoUser] ([id])
GO
ALTER TABLE [dbo].[cmsTask] ADD CONSTRAINT [FK_cmsTask_umbracoUser1] FOREIGN KEY ([userId]) REFERENCES [dbo].[umbracoUser] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsTemplate]'
GO
ALTER TABLE [dbo].[cmsTemplate] ADD CONSTRAINT [FK_cmsTemplate_umbracoNode] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoDomains]'
GO
ALTER TABLE [dbo].[umbracoDomains] ADD CONSTRAINT [FK_umbracoDomains_umbracoNode_id] FOREIGN KEY ([domainRootStructureID]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoExternalLogin]'
GO
ALTER TABLE [dbo].[umbracoExternalLogin] ADD CONSTRAINT [FK_umbracoExternalLogin_umbracoUser_id] FOREIGN KEY ([userId]) REFERENCES [dbo].[umbracoUser] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoRedirectUrl]'
GO
ALTER TABLE [dbo].[umbracoRedirectUrl] ADD CONSTRAINT [FK_umbracoRedirectUrl] FOREIGN KEY ([contentKey]) REFERENCES [dbo].[umbracoNode] ([uniqueID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsLanguageText]'
GO
ALTER TABLE [dbo].[cmsLanguageText] ADD CONSTRAINT [FK_cmsLanguageText_umbracoLanguage_id] FOREIGN KEY ([languageId]) REFERENCES [dbo].[umbracoLanguage] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoAccessRule]'
GO
ALTER TABLE [dbo].[umbracoAccessRule] ADD CONSTRAINT [FK_umbracoAccessRule_umbracoAccess_id] FOREIGN KEY ([accessId]) REFERENCES [dbo].[umbracoAccess] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoNode]'
GO
ALTER TABLE [dbo].[umbracoNode] ADD CONSTRAINT [FK_umbracoNode_umbracoNode_id] FOREIGN KEY ([parentID]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContentType]'
GO
ALTER TABLE [dbo].[cmsContentType] ADD CONSTRAINT [FK_cmsContentType_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsContentType2ContentType]'
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] ADD CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_child] FOREIGN KEY ([childContentTypeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[cmsContentType2ContentType] ADD CONSTRAINT [FK_cmsContentType2ContentType_umbracoNode_parent] FOREIGN KEY ([parentContentTypeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsDocumentType]'
GO
ALTER TABLE [dbo].[cmsDocumentType] ADD CONSTRAINT [FK_cmsDocumentType_umbracoNode_id] FOREIGN KEY ([contentTypeNodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMember2MemberGroup]'
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] ADD CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id] FOREIGN KEY ([MemberGroup]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[cmsMemberType]'
GO
ALTER TABLE [dbo].[cmsMemberType] ADD CONSTRAINT [FK_cmsMemberType_umbracoNode_id] FOREIGN KEY ([NodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserGroup]'
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD CONSTRAINT [FK_startContentId_umbracoNode_id] FOREIGN KEY ([startContentId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoUserGroup] ADD CONSTRAINT [FK_startMediaId_umbracoNode_id] FOREIGN KEY ([startMediaId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoAccess]'
GO
ALTER TABLE [dbo].[umbracoAccess] ADD CONSTRAINT [FK_umbracoAccess_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] ADD CONSTRAINT [FK_umbracoAccess_umbracoNode_id1] FOREIGN KEY ([loginNodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoAccess] ADD CONSTRAINT [FK_umbracoAccess_umbracoNode_id2] FOREIGN KEY ([noAccessNodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoRelation]'
GO
ALTER TABLE [dbo].[umbracoRelation] ADD CONSTRAINT [FK_umbracoRelation_umbracoNode] FOREIGN KEY ([parentId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
ALTER TABLE [dbo].[umbracoRelation] ADD CONSTRAINT [FK_umbracoRelation_umbracoNode1] FOREIGN KEY ([childId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUser2NodeNotify]'
GO
ALTER TABLE [dbo].[umbracoUser2NodeNotify] ADD CONSTRAINT [FK_umbracoUser2NodeNotify_umbracoNode_id] FOREIGN KEY ([nodeId]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[umbracoUserStartNode]'
GO
ALTER TABLE [dbo].[umbracoUserStartNode] ADD CONSTRAINT [FK_umbracoUserStartNode_umbracoNode_id] FOREIGN KEY ([startNode]) REFERENCES [dbo].[umbracoNode] ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[cmsMember2MemberGroup]'
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] WITH CHECK CHECK CONSTRAINT [FK_cmsMember2MemberGroup_cmsMember_nodeId]
GO
ALTER TABLE [dbo].[cmsMember2MemberGroup] WITH CHECK CHECK CONSTRAINT [FK_cmsMember2MemberGroup_umbracoNode_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[umbracoRelation]'
GO
ALTER TABLE [dbo].[umbracoRelation] WITH CHECK CHECK CONSTRAINT [FK_umbracoRelation_umbracoNode]
GO
ALTER TABLE [dbo].[umbracoRelation] WITH CHECK CHECK CONSTRAINT [FK_umbracoRelation_umbracoNode1]
GO
ALTER TABLE [dbo].[umbracoRelation] WITH CHECK CHECK CONSTRAINT [FK_umbracoRelation_umbracoRelationType_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[umbracoUserGroup2NodePermission]'
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] NOCHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] CHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoNode_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] NOCHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoUserGroup_id]
GO
ALTER TABLE [dbo].[umbracoUserGroup2NodePermission] CHECK CONSTRAINT [FK_umbracoUserGroup2NodePermission_umbracoUserGroup_id]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
