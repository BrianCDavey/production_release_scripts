USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 3/30/2020 11:19:11 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[BillyPointAwardErrors]'
GO
CREATE TABLE [dbo].[BillyPointAwardErrors]
(
[errorMessage] [nvarchar] (90) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[synxisID] [int] NULL,
[openHospitalityID] [int] NULL,
[confirmationNumber] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[chargeDescription] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[channel] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[secondarySource] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[subSourceCode] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CROCode] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[templateAbbreviation] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[xbeTemplateName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateCategoryCode] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[rateTypeCode] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[arrivalDate] [date] NULL,
[clauseID] [int] NULL,
[HandbackImportStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HandbackProcessErrors] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [work].[LoadBillyPointAwardErrorsTable]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 03/16/2020
-- Description:	
-- EXEC [work].[LoadBillyPointAwardErrorsTable] 19646
-- =============================================
CREATE PROCEDURE [work].[LoadBillyPointAwardErrorsTable]
	@RunID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
--DECLARE @RunID int = 19646
declare	@startDate DATE, 
		@endDate DATE,
		@maxCalcDate DATE;

--Delete Temp table if exist
IF OBJECT_ID('tempdb..#mrtWithoutIpreferPointAward') IS NOT NULL
DROP TABLE #mrtWithoutIpreferPointAward;
		
-- get current run dates
select	@startDate = startdate, 
		@endDate = enddate,
		@maxCalcDate = RunDate
from	[work].[Run]
WHERE runID = @runID


  CREATE TABLE #mrtWithoutIpreferPointAward
  (
	[hotelCode] nvarchar(50)
    ,[hotelName] nvarchar(250)
    ,[synxisID] nvarchar(50)
    ,[confirmationNumber] nvarchar(max)
	,classificationID int
    ,clauseID int
    ,[channel] nvarchar(max)
    ,[secondarySource] nvarchar(max)
    ,[subSourceCode] nvarchar(max)
    ,[CROCode] nvarchar(max)
    ,[templateAbbreviation] nvarchar(max)
    ,[xbeTemplateName] nvarchar(max)
    ,[rateCategoryCode] nvarchar(max)
    ,[rateTypeCode] nvarchar(max)
    ,[arrivalDate] date
    ,[OpenHospitalityID] nvarchar(50)
	,ExportFlag int
	,HandbackFlag int
	,PointFlag int
	,HandbackImportStatus nvarchar(255)
	,HandbackProcessErrors nvarchar(255)
  )

insert into #mrtWithoutIpreferPointAward
	( hotelCode, hotelName, synxisID, openHospitalityID, confirmationNumber, clauseID, channel, secondarySource, subSourceCode, CROCode
	, templateAbbreviation, xbeTemplateName, rateCategoryCode, rateTypeCode, arrivalDate,
	ExportFlag, HandbackFlag, PointFlag, HandbackImportStatus, HandbackProcessErrors)
   select DISTINCT
	cd.hotelCode as [hotelCode]
    ,hr.HotelName as [hotelName]
    ,hh.synXisID as [synxisID]
	,hh.openhospID as [OpenHospitalityID]
    ,cd.confirmationNumber as [confirmationNumber]
    ,brule.clauseID
    ,cha.channel as [channel]
    ,sec.secondarySource as [secondarySource]
    ,sub.subSourceCode as [subSourceCode]
    ,acro.CRO_Code as [CROCode]
    ,ISNULL(pt.[siteAbbreviation], 'HOTEL') as [templateAbbreviation]
    ,aibe.ibeSourceName as [xbeTemplateName]
    ,rc.rateCategoryCode as [rateCategoryCode]
    ,rac.RateCode as [rateTypeCode]
    ,td.arrivalDate as [arrivalDate]
	, CASE WHEN eeptl.EpsilonPointTransactionLogID IS NULL THEN 0 ELSE 1 END AS ExportFlag
	, CASE WHEN eepth.Import_EpsilonExportPointTransactionHandBackID IS NULL THEN 0 ELSE 1 END AS HandbackFlag
	, CASE WHEN tdr.IP_TDR_ID IS NULL THEN 0 ELSE 1 END AS PointFlag
	,eepth.ImportStatus AS HandbackImportStatus
	,eepth.ProcessErrors AS HandbackProcessErrors
FROM  dbo.Charges cd 
	LEFT JOIN Reservations.dbo.Transactions t ON t.TransactionID = cd.transactionKey 
	INNER JOIN Reservations.dbo.TransactionStatus ts  ON ts.TransactionStatusID = t.TransactionStatusID
	INNER JOIN Reservations.dbo.TransactionDetail td  ON td.TransactionDetailID = t.TransactionDetailID
	LEFT JOIN Reservations.dbo.Chain ch  ON ch.ChainID = t.ChainID
	LEFT JOIN Reservations.dbo.hotel ht  ON ht.HotelID = t.HotelID
	LEFT JOIN Hotels.dbo.Hotel hh  ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
	LEFT JOIN Reservations.dbo.CRS_BookingSource bs  ON bs.BookingSourceID = t.CRS_BookingSourceID
	LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
	LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
	LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
	LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
	LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
	LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
	LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
	LEFT JOIN Reservations.dbo.RateCategory rc  ON rc.RateCategoryID = t.RateCategoryID
	LEFT JOIN Reservations.dbo.IATANumber iata  ON iata.IATANumberID = t.IATANumberID
	LEFT JOIN Reservations.dbo.TravelAgent ta  ON ta.TravelAgentID = t.TravelAgentID
	LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
	LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
	LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
	LEFT JOIN Reservations.synxis.transactions tr ON tr.TransactionID = t.sourceKey AND t.DataSourceID IN(SELECT DataSourceID FROM Reservations.authority.DataSource WHERE SourceName = 'SynXis')
	LEFT JOIN Reservations.synxis.BillingDescription bd ON tr.BillingDescriptionID = bd.BillingDescriptionID

	LEFT OUTER JOIN [dbo].[Templates] pt ON aibe.ibeSourceName = pt.[xbeTemplateName]
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hr ON cd.hotelCode = hr.code
	LEFT OUTER JOIN Loyalty.dbo.TransactionDetailedReport tdr ON tdr.Booking_ID = cd.confirmationNumber  AND tdr.Hotel_Code = cd.hotelCode
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hrs ON hh.synXisID = hrs.synxisID
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hro ON hh.openhospID = hro.openHospitalityCode
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON COALESCE(hro.code, hrs.code) = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON td.currency = cur.CURNCYID		AND td.arrivalDate = cur.EXCHDATE
	LEFT OUTER JOIN ETL.dbo.EpsilonPointTransactionLog eeptl ON eeptl.confirmationnumber = t.confirmationnumber
	LEFT OUTER JOIN ETL.dbo.Import_EpsilonExportPointTransactionHandBack eepth 
		ON CASE WHEN LEN(eepth.TRANSACTION_NUMBER) < 20 THEN eepth.TRANSACTION_NUMBER ELSE LEFT(eepth.TRANSACTION_NUMBER, CHARINDEX('-',eepth.TRANSACTION_NUMBER)-1) END  = cd.confirmationNumber 
			AND eepth.TRANS_LOCATION = cd.hotelCode AND eepth.QueueID >= 10008
where 
 cd.arrivalDate BETWEEN @startDate AND @endDate
AND cd.billableDate < DATEADD(day,-3, @maxCalcDate)
AND cd.transactionSourceID IN (1,2)
AND cd.classificationID = 5
AND tdr.Transaction_Id IS NULL

TRUNCATE TABLE dbo.BillyPointAwardErrors

insert into dbo.BillyPointAwardErrors
	(errorMessage, hotelCode, hotelName, synxisID, openHospitalityID, confirmationNumber, clauseID, channel, secondarySource, subSourceCode, CROCode, templateAbbreviation, xbeTemplateName, rateCategoryCode, rateTypeCode, arrivalDate, HandbackImportStatus, HandbackProcessErrors)
   select DISTINCT
   'I Prefer Point Not Sent' as [errorMessage]
   	,[hotelCode]
    ,[hotelName]
    ,[synxisID]
	,[OpenHospitalityID]
    ,[confirmationNumber]
    ,[clauseID]
    ,[channel]
    ,[secondarySource]
    ,[subSourceCode]
    ,[CROCode]
    ,[templateAbbreviation]
    ,[xbeTemplateName]
    ,[rateCategoryCode]
    ,[rateTypeCode]
    ,[arrivalDate]
	,HandbackImportStatus
	,HandbackProcessErrors
FROM #mrtWithoutIpreferPointAward
WHERE ExportFlag = 0

UNION ALL

select DISTINCT
   'I Prefer Point Not in HandBack ' as [errorMessage]
   	,[hotelCode]
    ,[hotelName]
    ,[synxisID]
	,[OpenHospitalityID]
    ,[confirmationNumber]
    ,[clauseID]
    ,[channel]
    ,[secondarySource]
    ,[subSourceCode]
    ,[CROCode]
    ,[templateAbbreviation]
    ,[xbeTemplateName]
    ,[rateCategoryCode]
    ,[rateTypeCode]
    ,[arrivalDate]
	,HandbackImportStatus
	,HandbackProcessErrors
FROM #mrtWithoutIpreferPointAward
WHERE ExportFlag = 1 AND HandbackFlag = 0

UNION ALL

select DISTINCT
   'I Prefer Point Error Out in Handback' as [errorMessage]
   	,[hotelCode]
    ,[hotelName]
    ,[synxisID]
	,[OpenHospitalityID]
    ,[confirmationNumber]
    ,[clauseID]
    ,[channel]
    ,[secondarySource]
    ,[subSourceCode]
    ,[CROCode]
    ,[templateAbbreviation]
    ,[xbeTemplateName]
    ,[rateCategoryCode]
    ,[rateTypeCode]
    ,[arrivalDate]
	,HandbackImportStatus
	,HandbackProcessErrors
FROM #mrtWithoutIpreferPointAward
WHERE ExportFlag = 1 AND HandbackFlag = 1 AND HandbackProcessErrors <> ''

UNION ALL

select DISTINCT
   'I Prefer Point Not In TransactionDetailReport' as [errorMessage]
   	,[hotelCode]
    ,[hotelName]
    ,[synxisID]
	,[OpenHospitalityID]
    ,[confirmationNumber]
    ,[clauseID]
    ,[channel]
    ,[secondarySource]
    ,[subSourceCode]
    ,[CROCode]
    ,[templateAbbreviation]
    ,[xbeTemplateName]
    ,[rateCategoryCode]
    ,[rateTypeCode]
    ,[arrivalDate]
	,HandbackImportStatus
	,HandbackProcessErrors
FROM #mrtWithoutIpreferPointAward
WHERE ExportFlag = 1 AND HandbackFlag = 1 AND HandbackProcessErrors = '' AND HandbackImportStatus = 'success'
AND PointFlag = 0

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[LoadBillyErrorTable]'
GO


-- =============================================
-- Author:		Kris Scott
-- Create date: 04/29/2016
-- Description:	
-- History: 2019-06-03 Ti Yao Swith to Reservations DB version
-- EXEC [work].[LoadBillyErrorTable] 19646
-- =============================================
ALTER PROCEDURE [work].[LoadBillyErrorTable]
	@RunID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
--DECLARE @RunID int = 19646
declare	@startDate DATE, 
		@endDate DATE,
		@maxCalcDate DATE,
		@maxInvoiceDate DATE,
		@firstDayOfInvoiceMonth DATE;

--Delete Temp table if exist
IF OBJECT_ID('tempdb..#mrtWithoutCharges') IS NOT NULL
DROP TABLE #mrtWithoutCharges;

IF OBJECT_ID('tempdb..#mrtWithoutCharges') IS NOT NULL
DROP TABLE #nonBillable;
		
-- get current run dates
select	@startDate = startdate, 
		@endDate = enddate,
		@maxCalcDate = RunDate
from	[work].[Run]
WHERE runID = @runID

-- get last invoice date run dates
select	@maxInvoiceDate = MAX(invoiceDate)
from	dbo.Charges

SELECT @firstDayOfInvoiceMonth = DATEADD(month, DATEDIFF(month, 0, @maxInvoiceDate), 0)

  SELECT
	c.transactionSourceID, c.transactionKey
  INTO #nonBillable
  FROM dbo.Charges as c
  WHERE c.billableDate BETWEEN @startDate AND @endDate
  AND c.[classificationID] = 4
  
  CREATE TABLE #mrtWithoutCharges
  (
	[hotelCode] nvarchar(50)
    ,[hotelName] nvarchar(250)
    ,[synxisID] nvarchar(50)
    ,[confirmationNumber] nvarchar(max)
	,classificationID int
    ,clauseID int
    ,[channel] nvarchar(max)
    ,[secondarySource] nvarchar(max)
    ,[subSourceCode] nvarchar(max)
    ,[CROCode] nvarchar(max)
    ,[templateAbbreviation] nvarchar(max)
    ,[xbeTemplateName] nvarchar(max)
    ,[rateCategoryCode] nvarchar(max)
    ,[rateTypeCode] nvarchar(max)
    ,[arrivalDate] date
    ,[OpenHospitalityID] nvarchar(50)
    ,currency nvarchar(5)
	,bookingChargesCount int
	,commissionChargesCount int
	,iPreferChargesCount int
	,phHotelCode nvarchar(50)
	,gpHotelCode nvarchar(50)
	,gpCurrency nvarchar(5)
	,resCurrency nvarchar(5)
  )

INSERT into	#mrtWithoutCharges
   select DISTINCT
	COALESCE(hrs.code, hro.code,hh.HotelCode) as [hotelCode]
    ,hh.HotelName as [hotelName]
    ,hh.synXisID as [synxisID]
    ,t.confirmationNumber as [confirmationNumber]
	,cd.classificationID
    ,brule.clauseID
    ,cha.channel as [channel]
    ,sec.secondarySource as [secondarySource]
    ,sub.subSourceCode as [subSourceCode]
    ,acro.CRO_Code as [CROCode]
    ,ISNULL(pt.[siteAbbreviation], 'HOTEL') as [templateAbbreviation]
    ,aibe.ibeSourceName as [xbeTemplateName]
    ,rc.rateCategoryCode as [rateCategoryCode]
    ,rac.RateCode as [rateTypeCode]
    ,td.arrivalDate as [arrivalDate]
    ,hh.openhospID as [OpenHospitalityID]
    ,td.currency
	,bc.bookingChargesCount
	,cc.commissionChargesCount
	,ipc.iPreferChargesCount
	,COALESCE(hrs.code, hro.code, NULL) as phHotelCode
	,gpc.custnmbr as gpHotelCode
	,gpc.CURNCYID as gpCurrency
	,cur.CURNCYID as resCurrency
FROM Reservations.dbo.Transactions t 
	INNER JOIN Reservations.dbo.TransactionStatus ts  ON ts.TransactionStatusID = t.TransactionStatusID
	INNER JOIN Reservations.dbo.TransactionDetail td  ON td.TransactionDetailID = t.TransactionDetailID
	LEFT JOIN Reservations.dbo.Chain ch  ON ch.ChainID = t.ChainID
	LEFT JOIN Reservations.dbo.hotel ht  ON ht.HotelID = t.HotelID
	LEFT JOIN Hotels.dbo.Hotel hh  ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
	LEFT JOIN Reservations.dbo.CRS_BookingSource bs  ON bs.BookingSourceID = t.CRS_BookingSourceID
	LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
	LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
	LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
	LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
	LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
	LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
	LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
	LEFT JOIN Reservations.dbo.RateCategory rc  ON rc.RateCategoryID = t.RateCategoryID
	LEFT JOIN Reservations.dbo.IATANumber iata  ON iata.IATANumberID = t.IATANumberID
	LEFT JOIN Reservations.dbo.TravelAgent ta  ON ta.TravelAgentID = t.TravelAgentID
	LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
	LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
	LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
	LEFT JOIN Reservations.synxis.transactions tr ON tr.TransactionID = t.sourceKey AND t.DataSourceID IN(SELECT DataSourceID FROM Reservations.authority.DataSource WHERE SourceName = 'SynXis')
	LEFT JOIN Reservations.synxis.BillingDescription bd ON tr.BillingDescriptionID = bd.BillingDescriptionID

	LEFT OUTER JOIN [dbo].[Templates] pt ON aibe.ibeSourceName = pt.[xbeTemplateName]
	LEFT OUTER JOIN dbo.Charges cd on t.TransactionID = cd.transactionKey AND cd.transactionSourceID IN (1,2)
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on t.TransactionID = bc.transactionKey AND bc.transactionSourceID IN (1,2)
	LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on t.TransactionID = cc.transactionKey AND cc.transactionSourceID IN (1,2)	
	LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON t.TransactionID = ipc.transactionKey AND ipc.transactionSourceID IN (1,2)	
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hrs ON hh.synXisID = hrs.synxisID
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hro ON hh.openhospID = hro.openHospitalityCode
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON COALESCE(hro.code, hrs.code) = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON td.currency = cur.CURNCYID		AND td.arrivalDate = cur.EXCHDATE
	LEFT OUTER JOIN #nonBillable nb	on t.TransactionID = nb.transactionKey AND nb.transactionSourceID IN (1,2)
	LEFT OUTER JOIN dbo.Clauses cl ON COALESCE(hrs.code, hro.code,hh.HotelCode) = cl.hotelCode AND cl.clauseName LIKE '%Any Booking%'
where (cd.hotelCode IS NULL OR (bookingChargesCount IS NULL OR bookingChargesCount <> 1) OR (commissionChargesCount > 1) OR cd.chargeValueInUSD IS NULL OR cd.chargeValueInHotelCurrency IS NULL)
and td.arrivalDate BETWEEN @firstDayOfInvoiceMonth AND @endDate
AND t.timeLoaded < DATEADD(day,-2,@maxCalcDate)
AND ts.status <> 'Cancelled'
AND nb.transactionKey IS NULL --only want to see bookings not handled by non-billable charges
AND cl.clauseID IS NULL

INSERT into	#mrtWithoutCharges
   select 
	tdr.Hotel_Code as [hotelCode]
    ,hr.hotelName as [hotelName]
    ,'' as [synxisID]
    ,tdr.Booking_ID as [confirmationNumber]
	,cd.classificationID
    ,brule.clauseID
    ,'iPrefer Manual Entry' as [channel]
    ,tdr.Transaction_Source as [secondarySource]
    ,tdr.Remarks as [subSourceCode]
    ,'' as [CROCode]
    ,'' as [templateAbbreviation]
    ,'' as [xbeTemplateName]
    ,'' as [rateCategoryCode]
    ,'' as [rateTypeCode]
    ,tdr.Reward_Posting_Date as [arrivalDate]
    ,'' as [OpenHospitalityID]
    ,tdr.Currency_Code
	,bc.bookingChargesCount
	,cc.commissionChargesCount
	,ipc.iPreferChargesCount
	,hr.code as phHotelCode
	,gpc.custnmbr as gpHotelCode
	,gpc.CURNCYID as gpCurrency
	,cur.CURNCYID as resCurrency
from Loyalty.dbo.TransactionDetailedReport tdr
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hr ON tdr.Hotel_Code = hr.code
	LEFT OUTER JOIN dbo.Charges cd on tdr.Transaction_Id = cd.transactionKey AND cd.transactionSourceID = 3 --iprefer manual points
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on tdr.Transaction_Id = bc.transactionKey AND bc.transactionSourceID = 3
	LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on tdr.Transaction_Id = cc.transactionKey AND cc.transactionSourceID = 3	
	LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON tdr.Transaction_Id = ipc.transactionKey AND ipc.transactionSourceID = 3	
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON hr.code = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON tdr.Currency_Code = cur.CURNCYID AND tdr.Reward_Posting_Date = cur.EXCHDATE
	LEFT OUTER JOIN #nonBillable nb	on tdr.Transaction_Id = nb.transactionKey AND nb.transactionSourceID = 3 --iprefer manual points
where (cd.hotelCode IS NULL OR (ipc.iPreferChargesCount IS NULL OR ipc.iPreferChargesCount  <> 1))
and tdr.Reward_Posting_Date BETWEEN @startDate AND @endDate
AND tdr.Reward_Posting_Date < DATEADD(day,-2,@maxCalcDate)
AND tdr.Reservation_Revenue <> 0
AND tdr.Points_Earned <> 0
AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal')
AND tdr.Hotel_Code <> 'PHG123'
AND tdr.Hotel_Code IS NOT NULL
AND nb.transactionKey IS NULL --only want to see bookings not handled by non-billable charges;
AND tdr.Booking_ID <> ''

	--	empty the billy error table
	truncate table dbo.BillyCalcErrors

	-- reinsert new error records into the table
	insert into dbo.BillyCalcErrors
	(errorMessage, hotelCode, hotelName, synxisID, openHospitalityID, confirmationNumber, clauseID, channel, secondarySource, subSourceCode, CROCode, templateAbbreviation, xbeTemplateName, rateCategoryCode, rateTypeCode, arrivalDate)


SELECT 'Unable to map hotel to CRM' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NULL

UNION ALL

SELECT 'No hotel found in GP' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NULL

UNION ALL

SELECT 'No currency found on GP customer card' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NULL

UNION ALL

SELECT 'No currency exchange rate found in GP for ' + currency as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND mrt.arrivalDate < GETDATE()
AND resCurrency IS NULL 

UNION ALL

SELECT DISTINCT 'Multiple booking charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND mrt.bookingChargesCount > 1
AND mrt.classificationID = 1

UNION ALL

SELECT DISTINCT 'No booking or I Prefer charge found' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (
	(mrt.bookingChargesCount = 0 OR mrt.bookingChargesCount IS NULL)
	AND (mrt.ipreferChargesCount = 0 OR mrt.ipreferChargesCount IS NULL)
	)

UNION ALL

SELECT DISTINCT 'Multiple commission charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.commissionChargesCount > 1)
AND mrt.classificationID = 2


UNION ALL


SELECT DISTINCT 'Multiple iPrefer charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.iPreferChargesCount > 1)
AND mrt.classificationID = 5


UNION ALL

SELECT 'Unknown error' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.bookingChargesCount = 1)
--AND (mrt.surchargeCount IS NULL OR mrt.surchargeCount <= 1)
AND (mrt.commissionChargesCount IS NULL OR mrt.commissionChargesCount <= 1)
;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[runCalculator]'
GO




ALTER PROCEDURE [work].[runCalculator]
	@runType bit = 1, --0=test, 1=production
	@startDate date = NULL,
	@endDate date = NULL,
	@hotelCode nvarchar(20) = NULL,
	@confirmationNumber nvarchar(255) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': start'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	
	-- DELETE MRT ---------------------------------------------------------------
	EXEC [work].[Delete_MRT]

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [work].[Delete_MRT] Finished'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------


	DECLARE @RunID int;
	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunType,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),@runType,0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': work.Run insert'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-2,MIN(sub.billableDate)),
				@endDate = DATEADD(DAY,2,MAX(sub.billableDate)),
				@hotelCode = MAX(sub.phgHotelCode)
		
		FROM (		
			SELECT mrt.phgHotelCode, CONVERT(date,CASE WHEN mrt.arrivalDate >= GETDATE() THEN mrt.confirmationDate ELSE mrt.arrivaldate END) as billableDate
			FROM work.mrtJoined_Reservation mrt
			WHERE mrt.confirmationNumber = @confirmationNumber
				UNION 
			SELECT tdr.phgHotelCode, CASE WHEN tdr.arrivalDate >= GETDATE() THEN tdr.transactionTimeStamp ELSE tdr.arrivalDate END as billableDate
			FROM work.tdrJoined_Reservation tdr
			WHERE tdr.confirmationNumber = @confirmationNumber
			) as sub
	END

	IF(@startDate IS NULL OR @endDate IS NULL)
	BEGIN
		IF @startDate IS NULL
			BEGIN
			IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
			BEGIN
				SET @startDate = DATEADD (mm,-1,GETDATE())
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
			ELSE
			BEGIN
				SET @startDate = GETDATE()
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
		END

		IF @endDate IS NULL OR @endDate < @startDate
		BEGIN
			SET @endDate = DATEADD(YEAR,2,@startDate)
		END
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': set up parameter'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- UPDATE RUN PARAMETERS ----------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 1,
	startDate = @startDate,
	endDate = @endDate
	WHERE runID = @RunID;
	-----------------------------------------------------------------------------
	
	-- POPULATE [MrtForCalculation] ---------------------------------------------
	SET @print = '	@startDate=' + CONVERT(varchar(100),@startDate,120) + ', @endDate=' + CONVERT(varchar(100),@endDate,120) + ', @RunID=' + CONVERT(varchar(20), @RunID) + ', @hotelCode=' + ISNULL(@hotelCode,'') + ', @confirmationNumber=' + ISNULL(@confirmationNumber,'')
	RAISERROR(@print,10,1) WITH NOWAIT

	IF @runType = 0
	BEGIN
		EXEC [work].[Populate_ExchangeRates] @runType, @startDate, @endDate
		EXEC [test].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber
	END
	ELSE
		EXEC [work].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE [MrtForCalculation] complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- POPULATE MrtForCalc_Clauses ----------------------------------------------
	IF @runType = 0
		EXEC test.Populate_MrtForCalc_Clauses @RunID
	ELSE
		EXEC work.Populate_MrtForCalc_Clauses @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE work.Populate_MrtForCalc_Clauses complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- Eliminate excluded clauses------------------------------------------------
	IF @runType = 0
		EXEC test.handleExclusions @RunID
	ELSE
		EXEC work.handleExclusions @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': handleExclusions complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- CALCULATE CHARGES --------------------------------------------------------
	IF @runType = 0
	BEGIN
		-- RUN standard calculation ---------------------------------------------
		EXEC test.Calculate_StandardCharges @RunID

		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Calculate_StandardCharges complete'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT
		-------------------------------------------------------------------------

		-- Calculate thresholds -------------------------------------------------
		EXEC test.Calculate_ThresholdCharges @RunID

		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Calculate_ThresholdCharges complete'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT
		-------------------------------------------------------------------------
	END
	ELSE
	BEGIN
		DELETE c
		FROM dbo.Charges c
		WHERE billableDate BETWEEN @startDate AND @endDate
		AND hotelCode = ISNULL(@hotelCode, hotelCode)
		AND confirmationNumber = ISNULL(@confirmationNumber, confirmationNumber)
		AND c.sopNumber IS NULL
		AND transactionSourceID = 3 --delete iprefer charge for recalculating summary manual point credit
									--This is temparory solution to avoid big change on Calculate_StandardCharges proc. Will fix this bug later.

		EXEC work.Calculate_Charges @RunID
	END
	-----------------------------------------------------------------------------

	-- Subtract Booking fees from Commission fees--------------------------------
	EXEC work.handleCommissions @RunID, @RunType

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': handleCommissions complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- move billableDate for old historical arrival --------------------------------
	IF @runType = 1
	BEGIN
		EXEC [work].[Move_Charges_BillableDate] @RunID
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Move_Charges_BillableDate complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	
	IF @runType = 1
	BEGIN
		EXEC work.LoadBillyErrorTable @RunID
		EXEC work.LoadBillyPointAwardErrorsTable @RunID
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': LoadBillyErrorTable complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT

	-- SET RUN TO COMPLETED -------------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 2 
	WHERE runID = @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': execution complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT

	RETURN @runID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
