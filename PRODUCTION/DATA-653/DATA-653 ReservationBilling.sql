USE [ReservationBilling]
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/3/2020 7:53:28 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating schemas'
GO
CREATE SCHEMA [loyalty]
AUTHORIZATION [dbo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[billyLoyaltyFlipFlagNoSupersetCore]'
GO






-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-05-16
-- Description:	This function is used in new billy to calculate loyalty flip flag for Iprefer charge SUM in USD less than 0
-- This function is called by view [work].[mrtJoined]
-- Prototype: SELECT [work].[billyLoyaltyFlipFlagNoSupersetCore]('27198SB100351' ,'APCCI', 9, 2018)
-- =============================================
ALTER FUNCTION [work].[billyLoyaltyFlipFlagNoSupersetCore]
(
	-- Add the parameters for the function here
	@confirmationNumber nvarchar(255),
	@phgHotelCode nvarchar(20),
	@MONtransactionTimeStamp int,
	@YEARtransactionTimeStamp int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit = 0, --default to no return for flip flag
			@counttdr int = 0

	-- Add the T-SQL statements to compute the return value here
	SELECT @counttdr = COUNT(1)
	FROM Loyalty.dbo.[TransactionDetailedReport] tdr
		LEFT JOIN work.[local_exchange_rates] bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = bookingCE.EXCHDATE
	WHERE tdr.Transaction_Source = 'Admin Portal' AND tdr.Booking_Source != 'PMSBooking'
	AND tdr.Booking_ID = @confirmationNumber
	AND tdr.Hotel_Code = @phgHotelCode
	AND MONTH(tdr.Reward_Posting_Date) = @MONtransactionTimeStamp
	AND YEAR(tdr.Reward_Posting_Date) = @YEARtransactionTimeStamp
	AND tdr.Reservation_Revenue <> 0
	AND tdr.Points_Earned <> 0
	GROUP BY tdr.Booking_ID, tdr.Hotel_Code, MONTH(tdr.Reward_Posting_Date), YEAR(tdr.Reward_Posting_Date)
	,tdr.Transaction_Source, tdr.Booking_Source
	HAVING SUM(ROUND((tdr.Reservation_Revenue/bookingCE.XCHGRATE) * 1.00,2)) <= 0.01

	IF(@counttdr > 0)
	SET @Result = 1


	-- Return the result of the function
	RETURN @Result

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[postResIntegration_UpdateCD]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-03-20
-- Description:	REwrite of original form Kris Scott
--				connect sop numbers to billy calculations after the smart connect integration
-- =============================================
ALTER procedure [dbo].[postResIntegration_UpdateCD] 
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;


	select	SOPNUMBE, CUSTNMBR, DOCID, DOCDATE, BACHNUMB 
	into	#SOP10100	
	from	chisqp01.IC.dbo.SOP10100
	where	DOCID = 'RESINV'
	and		BACHNUMB like 'RES%'
	AND		docdate = @invoiceDate
	

	UPDATE	CD
	SET		CD.sopNumber = gp.SOPNUMBE,
			CD.invoiceDate = gp.DOCDATE
	FROM	#SOP10100	as gp
	join	ReservationBilling.dbo.Charges CD with (nolock)
		on	CD.hotelCode = gp.CUSTNMBR
		and CD.billableDate between @startDate and @endDate
		and gp.BACHNUMB like 'RES%'
	where	CD.sopNumber is null
	

	drop table #SOP10100
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[postResIntegration_UpdateCD_TEST]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-03-20
-- Description:	REwrite of original form Kris Scott
--				connect sop numbers to billy calculations after the smart connect integration
-- =============================================
ALTER procedure [dbo].[postResIntegration_UpdateCD_TEST] 
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;


	select	SOPNUMBE, CUSTNMBR, DOCID, DOCDATE, BACHNUMB 
	into	#SOP10100	
	from	ICT.dbo.SOP10100
	where	DOCID = 'RESINV'
	and		BACHNUMB like 'RES%'
	AND		docdate = @invoiceDate
	

	UPDATE	CD
	SET		CD.sopNumber = gp.SOPNUMBE,
			CD.invoiceDate = gp.DOCDATE
	FROM	#SOP10100	as gp
	join	ReservationBilling.dbo.Charges CD with (nolock)
		on	CD.hotelCode = gp.CUSTNMBR
		and CD.billableDate between @startDate and @endDate
		and gp.BACHNUMB like 'RES%'
	where	CD.sopNumber is null
			
	drop table #SOP10100
	
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Populate_MrtForCalculation_ByDate]'
GO




ALTER PROCEDURE [test].[Populate_MrtForCalculation_ByDate]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_mrt
	AS
	(
		SELECT confirmationNumber FROM Reservations.dbo.mostrecenttransactions WHERE arrivalDate BETWEEN @startDate AND @endDate
	),
	cte_loyaltyflipflag
	AS
	(
		SELECT confirmationNumber,phgHotelCode,SUMroomRevenueInUSD,MONtransactionTimeStamp,YEARtransactionTimeStamp,LoyaltyFlipFlag
		FROM [test].[iPreferInversion] 
		WHERE (MINtransactionTimeStamp BETWEEN @startDate AND @endDate
			OR MAXtransactionTimeStamp BETWEEN @startDate AND @endDate)
	)
	INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],mrtj.[gpSiteID],mrtj.[chainID],
			mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],mrtj.[bookingChannel],mrtj.[bookingSecondarySource],
			mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],
			mrtj.[bookingCroGroupID],mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],mrtj.[nights],mrtj.[rooms],
			mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],
			mrtj.exchangeDate,mrtj.hotelCurrencyCode,mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],
			--new change
			CASE
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				WHEN LoyaltyFlipFlag = 1 THEN 1
				ELSE mrtj.LoyaltyNumberValidated
			END AS LoyaltyNumberValidated,
			--new change
			CASE
				WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				ELSE mrtj.LoyaltyNumberTagged
			END as LoyaltyNumberTagged,
			mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.confirmationNumber
	FROM test.mrtJoined mrtj
		LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode
							AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber
							AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
							AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
	WHERE mrtj.arrivalDate BETWEEN @startDate AND @endDate
	
	UNION
	
	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],tdr.[gpSiteID],tdr.[chainID],
			tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],tdr.[bookingChannel],tdr.[bookingSecondarySource],
			tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],
			tdr.[bookingCroGroupID],tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],tdr.[nights],tdr.[rooms],
			tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,
			tdr.hotelCurrencyCode,tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,tdr.loyaltyProgram,
			tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,tdr.transactionTimestamp,tdr.transactionSourceID,
			tdr.transactionKey
	FROM test.tdrJoined tdr
		INNER JOIN
		(    
			SELECT tdrJ.confirmationNumber,tdrJ.phgHotelCode,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp
			FROM test.tdrJoined tdrJ
				LEFT JOIN cte_mrt mrt ON mrt.confirmationNumber = tdrJ.confirmationNumber
			WHERE
			(
				tdrJ.Transaction_Source = 'Hotel Portal'
				OR
				(
					tdrJ.Transaction_Source = 'Admin Portal'
					AND
					(tdrJ.Booking_Source = 'PMSBooking' OR mrt.confirmationNumber IS NULL)
				)
			)
			GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
			HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0
		) MP ON tdr.phgHotelCode = MP.phgHotelCode
					AND tdr.confirmationNumber = MP.confirmationNumber
					AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
					AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
	WHERE tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Populate_MrtForCalculation_ByHotelCode]'
GO




ALTER PROCEDURE [test].[Populate_MrtForCalculation_ByHotelCode]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int,
	@hotelCode nvarchar(20) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_mrt
	AS
	(
		SELECT confirmationNumber FROM Reservations.dbo.mostrecenttransactions WHERE arrivalDate BETWEEN @startDate AND @endDate
	),
	cte_loyaltyflipflag
	AS
	(
		SELECT confirmationNumber,phgHotelCode,SUMroomRevenueInUSD,MONtransactionTimeStamp,YEARtransactionTimeStamp,LoyaltyFlipFlag
		FROM [test].[iPreferInversion] 
		WHERE (MINtransactionTimeStamp BETWEEN @startDate AND @endDate
			OR MAXtransactionTimeStamp BETWEEN @startDate AND @endDate)
			AND phgHotelCode = @hotelCode
	)
	INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],mrtj.[gpSiteID],mrtj.[chainID],
			mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],mrtj.[bookingChannel],mrtj.[bookingSecondarySource],
			mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],
			mrtj.[bookingCroGroupID],mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],mrtj.[nights],mrtj.[rooms],
			mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],
			mrtj.exchangeDate,mrtj.hotelCurrencyCode,mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],
			--new change
			CASE
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				WHEN LoyaltyFlipFlag = 1 THEN 1
				ELSE mrtj.LoyaltyNumberValidated
			END AS LoyaltyNumberValidated,
			--new change
			CASE
				WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				ELSE mrtj.LoyaltyNumberTagged
			END as LoyaltyNumberTagged,
			mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.confirmationNumber
	FROM test.mrtJoined mrtj
		LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode
							AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber
							AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
							AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
	WHERE mrtj.phgHotelCode = @hotelCode
		AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
	
	UNION
	
	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],tdr.[gpSiteID],tdr.[chainID],
			tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],tdr.[bookingChannel],tdr.[bookingSecondarySource],
			tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],
			tdr.[bookingCroGroupID],tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],tdr.[nights],tdr.[rooms],
			tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,
			tdr.hotelCurrencyCode,tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,tdr.loyaltyProgram,
			tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,tdr.transactionTimestamp,tdr.transactionSourceID,
			tdr.transactionKey
	FROM test.tdrJoined tdr
		INNER JOIN
		(    
			SELECT tdrJ.confirmationNumber,tdrJ.phgHotelCode,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp
			FROM test.tdrJoined tdrJ
				LEFT JOIN cte_mrt mrt ON mrt.confirmationNumber = tdrJ.confirmationNumber
			WHERE
			(
				tdrJ.Transaction_Source = 'Hotel Portal'
				OR
				(
					tdrJ.Transaction_Source = 'Admin Portal'
					AND
					(tdrJ.Booking_Source = 'PMSBooking' OR mrt.confirmationNumber IS NULL)
				)
			)
			GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
			HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0
		) MP ON tdr.phgHotelCode = MP.phgHotelCode
					AND tdr.confirmationNumber = MP.confirmationNumber
					AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
					AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
	WHERE tdr.phgHotelCode = @hotelCode
		AND tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Populate_MrtForCalculation_ByConfNum]'
GO




ALTER PROCEDURE [test].[Populate_MrtForCalculation_ByConfNum]
	@RunID int,
	@confirmationNumber nvarchar(255) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_mrt
	AS
	(
		SELECT confirmationNumber FROM  Reservations.dbo.mostrecenttransactions WHERE confirmationNumber = @confirmationNumber
	),
	cte_loyaltyflipflag
	AS
	(
		SELECT confirmationNumber,phgHotelCode,SUMroomRevenueInUSD,MONtransactionTimeStamp,YEARtransactionTimeStamp,LoyaltyFlipFlag
		FROM [test].[iPreferInversion] 
		WHERE confirmationNumber = @confirmationnumber
	)
	INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],mrtj.[gpSiteID],mrtj.[chainID],
			mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],mrtj.[bookingChannel],mrtj.[bookingSecondarySource],
			mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],
			mrtj.[bookingCroGroupID],mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],mrtj.[nights],mrtj.[rooms],
			mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],
			mrtj.exchangeDate,mrtj.hotelCurrencyCode,mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],
			--new change
			CASE
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				WHEN LoyaltyFlipFlag = 1 THEN 1
				ELSE mrtj.LoyaltyNumberValidated
			END AS LoyaltyNumberValidated,
			--new change
			CASE
				WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				ELSE mrtj.LoyaltyNumberTagged
			END as LoyaltyNumberTagged,
			mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.confirmationNumber
	FROM test.mrtJoined mrtj
		LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode 
							AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
							AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
	WHERE mrtj.confirmationNumber = @confirmationNumber

	UNION

	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],tdr.[gpSiteID],tdr.[chainID],
			tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],tdr.[bookingChannel],tdr.[bookingSecondarySource],
			tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],
			tdr.[bookingCroGroupID],tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],tdr.[nights],tdr.[rooms],
			tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],
			tdr.exchangeDate,tdr.hotelCurrencyCode,tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,
			tdr.loyaltyProgram,tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,tdr.transactionTimestamp,
			tdr.transactionSourceID,tdr.transactionKey
	FROM test.tdrJoined tdr
		INNER JOIN
		(    
			SELECT tdrJ.confirmationNumber,tdrJ.phgHotelCode,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp
			FROM test.tdrJoined tdrJ
				LEFT JOIN cte_mrt mrt ON mrt.confirmationNumber = tdrJ.confirmationNumber
			WHERE
			(
				tdrJ.Transaction_Source = 'Hotel Portal'
				OR
				(
					tdrJ.Transaction_Source = 'Admin Portal'
					AND
					(tdrJ.Booking_Source = 'PMSBooking' OR mrt.confirmationNumber IS NULL)
				)
			)
			GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
			HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0
		) MP ON tdr.phgHotelCode = MP.phgHotelCode 
				AND tdr.confirmationNumber = MP.confirmationNumber
				AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
				AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
	WHERE tdr.confirmationNumber = @confirmationNumber
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[tdrJoined_Reservation]'
GO










ALTER VIEW [test].[tdrJoined_Reservation]
AS
     SELECT 
			3 as transactionSourceID, --3 = I Prefer Manual Transactions
			CAST(tdr.Transaction_Id as nvarchar(20)) as transactionKey,
            tdr.Booking_ID as confirmationNumber, 
			hotels.HotelCode AS phgHotelCode,
            NULL AS crsHotelID,
            hotels.hotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            null as chainID,
            'IPM' as chainName,
            'IPM' as bookingStatus,
            'iPrefer Manual Entry' as synxisBillingDescription,
            'iPrefer Manual Entry' AS bookingChannel,
            'iPrefer Manual Entry' AS bookingSecondarySource,
            'iPrefer Manual Entry' AS bookingSubSourceCode,
            null as bookingTemplateGroupId,
            'IPM' as bookingTemplateAbbreviation,
            'IPM' as xbeTemplateName,
            'iPrefer Manual Entry' as CROcode,
            null as bookingCroGroupID,
            'iPrefer Manual Entry' as bookingRateCategoryCode,
            'iPrefer Manual' as bookingRateCode,
            'iPrefer Manual' as bookingIATA,
            tdr.Reward_Posting_Date as transactionTimeStamp,
            (SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date), (tdr.Departure_Date), (tdr.Reward_Posting_Date)) AS Fields(d)) as confirmationDate,
            tdr.Arrival_Date as arrivalDate,
            tdr.Departure_Date as departureDate,
            CAST(null as date) as cancellationDate,
            CAST(null as varchar) as cancellationNumber,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as nights,
            1 as rooms,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as roomNights,
            tdr.Reservation_Revenue as roomRevenueInBookingCurrency,
            tdr.Currency_Code as bookingCurrencyCode,
            null as timeLoaded,
			'IPREFERMANUAL' AS [ItemCode],
			CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            null as CRSSourceID,
			'I Prefer' as loyaltyProgram,
			tdr.iPrefer_Number as loyaltyNumber,
			'iPrefer Manual Entry' as travelAgencyName,
			1 as LoyaltyNumberValidated,
			CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END as LoyaltyNumberTagged,
			tdr.Transaction_Source, 
			tdr.Booking_Source  

     FROM Loyalty.dbo.[TransactionDetailedReport] tdr
        LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
        LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.vw_local_exchange_rates hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.vw_local_exchange_rates bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = bookingCE.EXCHDATE
		LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
			AND tdr.Hotel_Code = tc.Hotel_Code
			AND tc.DateTagged <= tdr.Reward_Posting_Date


  WHERE tdr.Reservation_Revenue <> 0
	AND tdr.Points_Earned <> 0
	AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') --remove epsilon SFTP and all BSI old point activity
	AND tdr.Hotel_Code <> 'PHG123'
	AND tdr.Remarks NOT LIKE '%migration%' 
	AND tdr.Hotel_Code IS NOT NULL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[mrtJoined_Reservation]'
GO


ALTER VIEW [test].[mrtJoined_Reservation]
AS 
     SELECT 
			1 AS transactionSourceID, -- hardcode 1 use transactionID for the difference between openhospitality and sabre
			t.TransactionID as transactionKey,
            t.confirmationNumber,
			hh.HotelCode AS phgHotelCode,
            COALESCE(hh.openhospID, hh.synXisID) AS crsHotelID,
            hh.HotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
			CASE WHEN ch.ChainID = 22 THEN 56 ELSE ch.ChainID END AS [ChainID],--change from CRS_ChainID since open hospitality CRS_ChainID is not integer
            ch.chainName,
            ts.status AS bookingStatus,
            CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN bd.billingDescription ELSE 'Unknown' END AS synxisBillingDescription,
            cha.channel AS bookingChannel,
            sec.secondarySource AS bookingSecondarySource,
            sub.subSourceCode AS bookingSubSourceCode,
            COALESCE(te.templateGroupID, 2) AS bookingTemplateGroupId,
			CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END AS bookingTemplateAbbreviation,
            aibe.ibeSourceName AS xbeTemplateName,
            acro.CRO_Code AS CROcode,
            CASE WHEN acro.CRO_Code IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            rc.rateCategoryCode AS bookingRateCategoryCode,
            rac.RateCode AS bookingRateCode,
            ISNULL(iata.IATANumber,'') AS bookingIATA,
            t.transactionTimeStamp,
            ts.confirmationDate,
            td.arrivalDate,
            td.departureDate,
            ts.cancellationDate,
            ts.cancellationNumber,
            td.nights,
            td.rooms,
            td.nights * td.rooms AS roomNights,
            td.reservationRevenue AS roomRevenueInBookingCurrency,
            td.currency AS bookingCurrencyCode,
            t.timeLoaded,
			work.[billyItemCode](CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN bd.billingDescription ELSE 'Unknown' END,cha.channel,sec.secondarySource,sub.subSourceCode,CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END,croCodes.croGroupID, 123) AS [ItemCode], --hard code chainId since OH chainID is not int
			CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN ts.confirmationDate ELSE td.arrivaldate END) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            CASE WHEN bs.openHospID IS NOT NULL THEN 2 WHEN bs.synXisID IS NOT NULL THEN 1 ELSE 'Unknown' END AS CRSSourceID,
			lp.LoyaltyProgram,
			ISNULL(ln.loyaltyNumber,'') AS loyaltyNumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],
			--force tag if negative iprefer returns
			 CASE WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberValidated,0)
					WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					WHEN work.billyLoyaltyFlipFlag(t.confirmationNumber) = 1 THEN 1
					ELSE ISNULL(td.LoyaltyNumberValidated,0) END AS LoyaltyNumberValidated
				--new change
			,CASE WHEN ISNULL(td.LoyaltyNumberTagged,0) = 0 THEN 0
					 WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberTagged,0)
					 WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					 ELSE ISNULL(td.LoyaltyNumberTagged,0)
					 END as LoyaltyNumberTagged
		FROM Reservations.dbo.Transactions t WITH(NOLOCK)
		INNER JOIN Reservations.dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN Reservations.dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN Reservations.dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN Reservations.dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
		LEFT JOIN Reservations.dbo.CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
		LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
		LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
		LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
		LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
		LEFT JOIN Reservations.dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN Reservations.dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN Reservations.dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
		LEFT JOIN Reservations.synxis.transactions tr ON tr.TransactionID = t.sourceKey AND t.DataSourceID IN(SELECT DataSourceID FROM Reservations.authority.DataSource WHERE SourceName = 'SynXis')
		LEFT JOIN Reservations.synxis.BillingDescription bd ON tr.BillingDescriptionID = bd.BillingDescriptionID

        LEFT JOIN work.hotelActiveBrands ON hh.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hh.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hh.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[vw_local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[vw_local_exchange_rates] bookingCE ON td.currency = bookingCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = bookingCE.EXCHDATE
		LEFT JOIN Loyalty.dbo.[TransactionDetailedReport] tdr ON tdr.Booking_ID = t.confirmationNumber AND tdr.Hotel_Code = hh.HotelCode
		AND MONTH(tdr.Reward_Posting_Date) = MONTH(td.arrivalDate) AND YEAR(tdr.Reward_Posting_Date) = YEAR(td.arrivalDate) AND tdr.Reservation_Revenue <> 0 AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source IN ('Admin Portal','Hotel Portal')
		LEFT JOIN [dbo].[CROCodes] ON acro.CRO_Code = croCodes.croCode
		LEFT JOIN dbo.Templates te ON te.xbeTemplateName = aibe.ibeSourceName
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Populate_MrtForCalculation_ByDate_Reservation]'
GO






ALTER PROCEDURE [test].[Populate_MrtForCalculation_ByDate_Reservation]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_mrt
	AS
	(
		SELECT confirmationNumber FROM Reservations.dbo.mostrecenttransactions WHERE arrivalDate BETWEEN @startDate AND @endDate
	),
	cte_loyaltyflipflag
	AS
	(
		SELECT confirmationNumber,phgHotelCode,SUMroomRevenueInUSD,MONtransactionTimeStamp,YEARtransactionTimeStamp,LoyaltyFlipFlag
		FROM [test].[iPreferInversion_Reservation] 
		WHERE (MINtransactionTimeStamp BETWEEN @startDate AND @endDate
			OR MAXtransactionTimeStamp BETWEEN @startDate AND @endDate)
	)
	INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],mrtj.[gpSiteID],mrtj.[chainID],
			mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],mrtj.[bookingChannel],mrtj.[bookingSecondarySource],
			mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],
			mrtj.[bookingCroGroupID],mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],mrtj.[nights],mrtj.[rooms],
			mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],
			mrtj.exchangeDate,mrtj.hotelCurrencyCode,mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],
			--new change
			CASE
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				WHEN LoyaltyFlipFlag = 1 THEN 1
				ELSE mrtj.LoyaltyNumberValidated
			END AS LoyaltyNumberValidated,
			--new change
			CASE
				WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				ELSE mrtj.LoyaltyNumberTagged
			END as LoyaltyNumberTagged,
			mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.transactionKey
	FROM test.mrtJoined_Reservation mrtj
		LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode
							AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber
							AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
							AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
	WHERE mrtj.arrivalDate BETWEEN @startDate AND @endDate
	
	UNION
	
	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],tdr.[gpSiteID],tdr.[chainID],
			tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],tdr.[bookingChannel],tdr.[bookingSecondarySource],
			tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],
			tdr.[bookingCroGroupID],tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],tdr.[nights],tdr.[rooms],
			tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,
			tdr.hotelCurrencyCode,tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,tdr.loyaltyProgram,
			tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,tdr.transactionTimestamp,tdr.transactionSourceID,
			tdr.transactionKey
	FROM test.tdrJoined_Reservation tdr
		INNER JOIN
		(    
			SELECT tdrJ.confirmationNumber,tdrJ.phgHotelCode,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp
			FROM test.tdrJoined_Reservation tdrJ
				LEFT JOIN cte_mrt mrt ON mrt.confirmationNumber = tdrJ.confirmationNumber
			WHERE
			(
				tdrJ.Transaction_Source = 'Hotel Portal'
				OR
				(
					tdrJ.Transaction_Source = 'Admin Portal'
					AND
					(tdrJ.Booking_Source = 'PMSBooking' OR mrt.confirmationNumber IS NULL)
				)
			)
			GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
			HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0.01
		) MP ON tdr.phgHotelCode = MP.phgHotelCode
					AND tdr.confirmationNumber = MP.confirmationNumber
					AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
					AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
	WHERE tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Populate_MrtForCalculation_ByHotelCode_Reservation]'
GO






ALTER PROCEDURE [test].[Populate_MrtForCalculation_ByHotelCode_Reservation]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int,
	@hotelCode nvarchar(20) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_mrt
	AS
	(
		SELECT confirmationNumber FROM Reservations.dbo.mostrecenttransactions WHERE arrivalDate BETWEEN @startDate AND @endDate
	),
	cte_loyaltyflipflag
	AS
	(
		SELECT confirmationNumber,phgHotelCode,SUMroomRevenueInUSD,MONtransactionTimeStamp,YEARtransactionTimeStamp,LoyaltyFlipFlag
		FROM [test].[iPreferInversion_Reservation] 
		WHERE (MINtransactionTimeStamp BETWEEN @startDate AND @endDate
			OR MAXtransactionTimeStamp BETWEEN @startDate AND @endDate)
			AND phgHotelCode = @hotelCode
	)
	INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],mrtj.[gpSiteID],mrtj.[chainID],
			mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],mrtj.[bookingChannel],mrtj.[bookingSecondarySource],
			mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],
			mrtj.[bookingCroGroupID],mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],mrtj.[nights],mrtj.[rooms],
			mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],
			mrtj.exchangeDate,mrtj.hotelCurrencyCode,mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],
			--new change
			CASE
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				WHEN LoyaltyFlipFlag = 1 THEN 1
				ELSE mrtj.LoyaltyNumberValidated
			END AS LoyaltyNumberValidated,
			--new change
			CASE
				WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
				WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				ELSE mrtj.LoyaltyNumberTagged
			END as LoyaltyNumberTagged,
			mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.transactionKey
	FROM test.mrtJoined_Reservation mrtj
		LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode
							AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber
							AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
							AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
	WHERE mrtj.phgHotelCode = @hotelCode
		AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
	
	UNION
	
	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],tdr.[gpSiteID],tdr.[chainID],
			tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],tdr.[bookingChannel],tdr.[bookingSecondarySource],
			tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],
			tdr.[bookingCroGroupID],tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],tdr.[nights],tdr.[rooms],
			tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,
			tdr.hotelCurrencyCode,tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,tdr.loyaltyProgram,
			tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,tdr.transactionTimestamp,tdr.transactionSourceID,
			tdr.transactionKey
	FROM test.tdrJoined_Reservation tdr
		INNER JOIN
		(    
			SELECT tdrJ.confirmationNumber,tdrJ.phgHotelCode,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp
			FROM test.tdrJoined_Reservation tdrJ
				LEFT JOIN cte_mrt mrt ON mrt.confirmationNumber = tdrJ.confirmationNumber
			WHERE
			(
				tdrJ.Transaction_Source = 'Hotel Portal'
				OR
				(
					tdrJ.Transaction_Source = 'Admin Portal'
					AND
					(tdrJ.Booking_Source = 'PMSBooking' OR mrt.confirmationNumber IS NULL)
				)
			)
			GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
			HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0
		) MP ON tdr.phgHotelCode = MP.phgHotelCode
					AND tdr.confirmationNumber = MP.confirmationNumber
					AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
					AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
	WHERE tdr.phgHotelCode = @hotelCode
		AND tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[BillyControlTotals]'
GO


-- =============================================
-- Author:		Kris Scott
-- Create date: 04/03/2019
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[BillyControlTotals] 
	-- Add the parameters for the stored procedure here
	@year int = 0, 
	@month int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	DECLARE @startDate DATE
	DECLARE @endDate DATE 

	SET @startDate = CAST('01/01/' + CAST(@Year - 2 AS CHAR(4)) AS DATE);
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(CAST(@Month AS CHAR(2)) + '/01/' + CAST(@Year AS CHAR(4)) AS DATE)));

;WITH mrt AS (
	SELECT
			YEAR(mrt.arrivalDate) as arrivalYear,
			MONTH(mrt.arrivalDate) as arrivalMonth,
			MRT.channel,
			MRT.hotelCode,
			COUNT(DISTINCT MRT.confirmationNumber) AS bookings
			, SUM(MRT.reservationRevenueUSD) as roomRevenueUSD
	FROM Reservations.dbo.mostrecenttransactionsreporting MRT 
	WHERE MRT.arrivalDate BETWEEN @startDate AND @endDate
		  AND
		  MRT.status <> 'Cancelled'
		  AND
		  MRT.channel <> 'PMS Rez Synch'
	GROUP BY YEAR(mrt.arrivalDate),
			MONTH(mrt.arrivalDate),
			MRT.channel,
			MRT.hotelCode
),
billy AS (
SELECT
			YEAR(mrt.arrivalDate) as arrivalYear,
			MONTH(mrt.arrivalDate) as arrivalMonth,
			MRT.channel,
			C.hotelCode,
			COUNT(DISTINCT C.confirmationNumber) AS bookingsWithCharges
	, SUM(C.chargeValueInUSD) as chargesInUSD
	FROM ReservationBilling.dbo.Charges C 
	JOIN Reservations.dbo.mostrecenttransactions MRT 
		ON MRT.confirmationNumber = C.confirmationNumber 
	WHERE MRT.arrivalDate BETWEEN @startDate AND @endDate
	AND C.classificationID NOT IN (4)
	GROUP BY YEAR(mrt.arrivalDate),
			MONTH(mrt.arrivalDate),
			MRT.channel,
			C.hotelCode
)
SELECT
			mrt.arrivalYear,
			mrt.arrivalMonth,
			mrt.channel,
			mrt.hotelCode,
			mrt.bookings,		
			billy.bookingsWithCharges,
			mrt.roomRevenueUSD,
			billy.chargesInUSD
	FROM MRT LEFT JOIN BILLY 
		ON MRT.arrivalYear = billy.arrivalYear
		AND MRT.arrivalMonth = billy.arrivalMonth
		AND MRT.channel = billy.channel
		AND MRT.hotelCode = billy.hotelCode
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Populate_MrtForCalculation_ORIGINAL]'
GO






ALTER PROCEDURE [test].[Populate_MrtForCalculation_ORIGINAL]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int,
	@hotelCode nvarchar(20) = NULL,
	@confirmationNumber nvarchar(255) = NULL

AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--TRUNCATE TABLE [test].[MrtForCalculation];

	IF(@confirmationNumber IS NOT NULL) -- for performance purpose, separate known and unknown confirmation number
	BEGIN
		--;WITH cte_invoiced
		--AS (
		--	SELECT transactionSourceID,transactionKey,sopNumber 
		--	FROM dbo.Charges
		--	WHERE hotelCode = ISNULL(@hotelCode,hotelCode) --either we're running all hotels, or we're just getting a specific hotel
		--	AND confirmationNumber = ISNULL(@confirmationNumber,confirmationNumber) --either we're running all bookings, or just getting a specific conf#
		--	AND sopNumber IS NOT NULL
		--),
		WITH cte_mrt
		AS (
			SELECT confirmationNumber FROM  Reservations.dbo.mostrecenttransactions 
			WHERE 
			confirmationNumber = @confirmationNumber
		)
		, cte_loyaltyflipflag
		AS (
			SELECT 
			  confirmationNumber
			  ,phgHotelCode
			  ,SUMroomRevenueInUSD
			  ,MONtransactionTimeStamp
			  ,YEARtransactionTimeStamp
			  ,LoyaltyFlipFlag
			 FROM [test].[iPreferInversion] 
			 WHERE confirmationNumber = @confirmationnumber
		)
		INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
		SELECT
			@RunID 
			,mrtj.[confirmationNumber]
			,mrtj.[phgHotelCode]
			,mrtj.[crsHotelID]
			,mrtj.[hotelName]
			,mrtj.[mainBrandCode]
			,mrtj.[gpSiteID]
			,mrtj.[chainID]
			,mrtj.[chainName]
			,mrtj.[bookingStatus]
			,mrtj.[synxisBillingDescription]
			,mrtj.[bookingChannel]
			,mrtj.[bookingSecondarySource]
			,mrtj.[bookingSubSourceCode]
			,mrtj.[bookingTemplateGroupID]
			,mrtj.[bookingTemplateAbbreviation]
			,mrtj.[xbeTemplateName]
			,mrtj.[CROcode]
			,mrtj.[bookingCroGroupID]
			,mrtj.[bookingRateCategoryCode]
			,mrtj.[bookingRateCode]
			,mrtj.[bookingIATA]
			,mrtj.[transactionTimeStamp]
			,mrtj.[confirmationDate]
			,mrtj.[arrivalDate]
			,mrtj.[departureDate]
			,mrtj.[cancellationDate]
			,mrtj.[cancellationNumber]
			,mrtj.[nights]
			,mrtj.[rooms]
			,mrtj.[roomNights]
			,mrtj.[roomRevenueInBookingCurrency]
			,mrtj.[bookingCurrencyCode]
			,mrtj.[timeLoaded]
			,mrtj.[CRSSourceID]
			,mrtj.[ItemCode]
			,mrtj.exchangeDate
			,mrtj.hotelCurrencyCode
			,mrtj.hotelCurrencyDecimalPlaces
			,mrtj.hotelCurrencyExchangeRate
			,mrtj.bookingCurrencyExchangeRate
			,mrtj.loyaltyProgram
			,mrtj.loyaltyNumber
			,mrtj.[travelAgencyName]
			--,mrtj.LoyaltyNumberValidated
			--,mrtj.LoyaltyNumberTagged
				--new change
			, CASE WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
					WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
					WHEN LoyaltyFlipFlag = 1 THEN 1
					ELSE mrtj.LoyaltyNumberValidated END AS LoyaltyNumberValidated
				--new change
			,CASE WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
					 WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
					 WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
					 ELSE mrtj.LoyaltyNumberTagged
					 END as LoyaltyNumberTagged
			,mrtj.arrivalDate
			,mrtj.CRSSourceID
			,mrtj.confirmationNumber
		FROM test.mrtJoined mrtj
			--LEFT JOIN cte_invoiced inv ON inv.transactionKey = mrtj.transactionKey 
			--	AND inv.transactionSourceID =  mrtj.transactionSourceID
			LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode 
				AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
				AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
		WHERE mrtj.phgHotelCode = ISNULL(@hotelCode,mrtj.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
			AND mrtj.confirmationNumber = @confirmationNumber --either we're running all bookings, or just getting a specific conf#
			--AND mrtj.arrivalDate BETWEEN @startDate AND @endDate --remove because it calculate confirmation date for mrt
			--AND (@runType = 0 --test run
			--	OR inv.transactionKey IS NULL) --not previously invoiced
	UNION
		SELECT
			@RunID 
			,tdr.[confirmationNumber]
			,tdr.[phgHotelCode]
			,tdr.[crsHotelID]
			,tdr.[hotelName]
			,tdr.[mainBrandCode]
			,tdr.[gpSiteID]
			,tdr.[chainID]
			,tdr.[chainName]
			,tdr.[bookingStatus]
			,tdr.[synxisBillingDescription]
			,tdr.[bookingChannel]
			,tdr.[bookingSecondarySource]
			,tdr.[bookingSubSourceCode]
			,tdr.[bookingTemplateGroupID]
			,tdr.[bookingTemplateAbbreviation]
			,tdr.[xbeTemplateName]
			,tdr.[CROcode]
			,tdr.[bookingCroGroupID]
			,tdr.[bookingRateCategoryCode]
			,tdr.[bookingRateCode]
			,tdr.[bookingIATA]
			,tdr.[transactionTimeStamp]
			,tdr.[confirmationDate]
			,tdr.[arrivalDate]
			,tdr.[departureDate]
			,tdr.[cancellationDate]
			,tdr.[cancellationNumber]
			,tdr.[nights]
			,tdr.[rooms]
			,tdr.[roomNights]
			,tdr.[roomRevenueInBookingCurrency]
			,tdr.[bookingCurrencyCode]
			,tdr.[timeLoaded]
			,tdr.[CRSSourceID]
			,tdr.[ItemCode]
			,tdr.exchangeDate
			,tdr.hotelCurrencyCode
			,tdr.hotelCurrencyDecimalPlaces
			,tdr.hotelCurrencyExchangeRate
			,tdr.bookingCurrencyExchangeRate
			,tdr.loyaltyProgram
			,tdr.loyaltyNumber
			,tdr.[travelAgencyName]
			,tdr.LoyaltyNumberValidated
			,tdr.LoyaltyNumberTagged
			,tdr.transactionTimestamp
			,tdr.transactionSourceID
			,tdr.transactionKey
	  FROM test.tdrJoined tdr
			INNER JOIN (    
				 SELECT 
				tdrJ.confirmationNumber
				,tdrJ.phgHotelCode
				,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp
				,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp

				FROM test.tdrJoined tdrJ
				LEFT JOIN cte_mrt mrt
				ON mrt.confirmationNumber = tdrJ.confirmationNumber

				WHERE ( tdrJ.Transaction_Source = 'Hotel Portal' OR 
				(tdrJ.Transaction_Source = 'Admin Portal' AND (tdrJ.Booking_Source = 'PMSBooking' OR mrt.confirmationNumber IS NULL))
					)
				GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode
					, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
				HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0
	
		) MP ON tdr.phgHotelCode = MP.phgHotelCode 
				AND tdr.confirmationNumber = MP.confirmationNumber AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
				AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
			--LEFT JOIN cte_invoiced inv ON inv.transactionKey = tdr.transactionKey
			--	AND inv.transactionSourceID =  tdr.transactionSourceID
	  WHERE tdr.phgHotelCode = ISNULL(@hotelCode,tdr.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
		AND tdr.confirmationNumber = ISNULL(@confirmationNumber,tdr.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
		--AND tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival --remove because it calculate arrival date for tdr
		--AND (@runType = 0 --test run
		--	OR inv.transactionKey IS NULL) --not previously invoiced
	END
	ELSE --confirmationNumber unknown
	BEGIN
		--;WITH cte_invoiced
	--AS (
	--	SELECT transactionSourceID,transactionKey,sopNumber 
	--	FROM dbo.Charges
	--	WHERE hotelCode = ISNULL(@hotelCode,hotelCode) --either we're running all hotels, or we're just getting a specific hotel
	--	AND confirmationNumber = ISNULL(@confirmationNumber,confirmationNumber) --either we're running all bookings, or just getting a specific conf#
	--	AND sopNumber IS NOT NULL
	--),
	WITH cte_mrt
	AS (
		SELECT confirmationNumber FROM  Reservations.dbo.mostrecenttransactions 
		WHERE arrivalDate BETWEEN @startDate AND @endDate
	)
	, cte_loyaltyflipflag
	AS (
		SELECT 
		  confirmationNumber
		  ,phgHotelCode
		  ,SUMroomRevenueInUSD
		  ,MONtransactionTimeStamp
		  ,YEARtransactionTimeStamp
		  ,LoyaltyFlipFlag
		 FROM [test].[iPreferInversion] 
		 WHERE (MINtransactionTimeStamp BETWEEN @startDate AND @endDate
		 OR MAXtransactionTimeStamp BETWEEN @startDate AND @endDate)
		 AND phgHotelCode = ISNULL(@hotelCode,phgHotelCode)
	)
	INSERT INTO [test].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)

	SELECT
		@RunID 
		,mrtj.[confirmationNumber]
		,mrtj.[phgHotelCode]
		,mrtj.[crsHotelID]
		,mrtj.[hotelName]
		,mrtj.[mainBrandCode]
		,mrtj.[gpSiteID]
		,mrtj.[chainID]
		,mrtj.[chainName]
		,mrtj.[bookingStatus]
		,mrtj.[synxisBillingDescription]
		,mrtj.[bookingChannel]
		,mrtj.[bookingSecondarySource]
		,mrtj.[bookingSubSourceCode]
		,mrtj.[bookingTemplateGroupID]
		,mrtj.[bookingTemplateAbbreviation]
		,mrtj.[xbeTemplateName]
		,mrtj.[CROcode]
		,mrtj.[bookingCroGroupID]
		,mrtj.[bookingRateCategoryCode]
		,mrtj.[bookingRateCode]
		,mrtj.[bookingIATA]
		,mrtj.[transactionTimeStamp]
		,mrtj.[confirmationDate]
		,mrtj.[arrivalDate]
		,mrtj.[departureDate]
		,mrtj.[cancellationDate]
		,mrtj.[cancellationNumber]
		,mrtj.[nights]
		,mrtj.[rooms]
		,mrtj.[roomNights]
		,mrtj.[roomRevenueInBookingCurrency]
		,mrtj.[bookingCurrencyCode]
		,mrtj.[timeLoaded]
		,mrtj.[CRSSourceID]
		,mrtj.[ItemCode]
		,mrtj.exchangeDate
		,mrtj.hotelCurrencyCode
		,mrtj.hotelCurrencyDecimalPlaces
		,mrtj.hotelCurrencyExchangeRate
		,mrtj.bookingCurrencyExchangeRate
		,mrtj.loyaltyProgram
		,mrtj.loyaltyNumber
		,mrtj.[travelAgencyName]
		--,mrtj.LoyaltyNumberValidated
		--,mrtj.LoyaltyNumberTagged
			--new change
		, CASE WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberValidated
				WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				WHEN LoyaltyFlipFlag = 1 THEN 1
				ELSE mrtj.LoyaltyNumberValidated END AS LoyaltyNumberValidated
			--new change
		,CASE WHEN mrtj.LoyaltyNumberTagged = 0 THEN 0
				 WHEN tdrGrouped.confirmationNumber IS NULL THEN mrtj.LoyaltyNumberTagged
				 WHEN tdrGrouped.SUMroomRevenueInUSD <= 0 THEN 0
				 ELSE mrtj.LoyaltyNumberTagged
				 END as LoyaltyNumberTagged
		,mrtj.arrivalDate
		,mrtj.CRSSourceID
		,mrtj.confirmationNumber
	FROM test.mrtJoined mrtj
		--LEFT JOIN cte_invoiced inv ON inv.transactionKey = mrtj.transactionKey 
		--	AND inv.transactionSourceID =  mrtj.transactionSourceID
		LEFT JOIN cte_loyaltyflipflag tdrGrouped ON mrtj.[phgHotelCode] = tdrGrouped.phgHotelCode 
			AND mrtj.confirmationNumber = tdrGrouped.confirmationNumber AND tdrGrouped.MONtransactionTimeStamp = MONTH(mrtj.arrivalDate)
			AND tdrGrouped.YEARtransactionTimeStamp = YEAR(mrtj.arrivalDate)
	WHERE mrtj.phgHotelCode = ISNULL(@hotelCode,mrtj.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
		--AND mrtj.confirmationNumber = @confirmationNumber --either we're running all bookings, or just getting a specific conf#
		AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
		--AND (@runType = 0 --test run
		--	OR inv.transactionKey IS NULL) --not previously invoiced
UNION
	SELECT
		@RunID 
		,tdr.[confirmationNumber]
		,tdr.[phgHotelCode]
		,tdr.[crsHotelID]
		,tdr.[hotelName]
		,tdr.[mainBrandCode]
		,tdr.[gpSiteID]
		,tdr.[chainID]
		,tdr.[chainName]
		,tdr.[bookingStatus]
		,tdr.[synxisBillingDescription]
		,tdr.[bookingChannel]
		,tdr.[bookingSecondarySource]
		,tdr.[bookingSubSourceCode]
		,tdr.[bookingTemplateGroupID]
		,tdr.[bookingTemplateAbbreviation]
		,tdr.[xbeTemplateName]
		,tdr.[CROcode]
		,tdr.[bookingCroGroupID]
		,tdr.[bookingRateCategoryCode]
		,tdr.[bookingRateCode]
		,tdr.[bookingIATA]
		,tdr.[transactionTimeStamp]
		,tdr.[confirmationDate]
		,tdr.[arrivalDate]
		,tdr.[departureDate]
		,tdr.[cancellationDate]
		,tdr.[cancellationNumber]
		,tdr.[nights]
		,tdr.[rooms]
		,tdr.[roomNights]
		,tdr.[roomRevenueInBookingCurrency]
		,tdr.[bookingCurrencyCode]
		,tdr.[timeLoaded]
		,tdr.[CRSSourceID]
		,tdr.[ItemCode]
		,tdr.exchangeDate
		,tdr.hotelCurrencyCode
		,tdr.hotelCurrencyDecimalPlaces
		,tdr.hotelCurrencyExchangeRate
		,tdr.bookingCurrencyExchangeRate
		,tdr.loyaltyProgram
		,tdr.loyaltyNumber
		,tdr.[travelAgencyName]
		,tdr.LoyaltyNumberValidated
		,tdr.LoyaltyNumberTagged
		,tdr.transactionTimestamp
		,tdr.transactionSourceID
		,tdr.transactionKey
  FROM test.tdrJoined tdr
		INNER JOIN (    
		 SELECT 
		tdrJ.confirmationNumber
		,tdrJ.phgHotelCode
		,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp
		,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp

		FROM test.tdrJoined tdrJ
		LEFT JOIN cte_mrt mrt
		ON mrt.confirmationNumber = tdrJ.confirmationNumber

  WHERE ( tdrJ.Transaction_Source = 'Hotel Portal' 
		OR (tdrJ.Transaction_Source = 'Admin Portal' AND 
		(tdrJ.Booking_Source = 'PMSBooking'
		OR mrt.confirmationNumber IS NULL))
		)
	GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode
		, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
	HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0
	
	) MP ON tdr.phgHotelCode = MP.phgHotelCode 
			AND tdr.confirmationNumber = MP.confirmationNumber AND MP.MONtransactionTimeStamp = MONTH(tdr.transactionTimeStamp)
			AND MP.YEARtransactionTimeStamp = YEAR(tdr.transactionTimeStamp)
		--LEFT JOIN cte_invoiced inv ON inv.transactionKey = tdr.transactionKey
		--	AND inv.transactionSourceID =  tdr.transactionSourceID
  WHERE tdr.phgHotelCode = ISNULL(@hotelCode,tdr.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
	AND tdr.confirmationNumber = ISNULL(@confirmationNumber,tdr.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
	AND tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
	--AND (@runType = 0 --test run
	--	OR inv.transactionKey IS NULL) --not previously invoiced
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[iPreferInversion]'
GO







ALTER VIEW [work].[iPreferInversion]
AS
     SELECT 
	  tdrJ.confirmationNumber
      ,tdrJ.phgHotelCode
	  ,SUM(ROUND((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00,2)) AS SUMroomRevenueInUSD
	  ,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp
	  ,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp
	  ,[work].[billyLoyaltyFlipFlag](tdrJ.confirmationNumber) AS LoyaltyFlipFlag
     FROM work.tdrJoined tdrJ
        LEFT JOIN Reservations.dbo.mostrecenttransactions mrt
		ON mrt.confirmationNumber = tdrJ.confirmationNumber
		
	GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
	,tdrJ.Transaction_Source, tdrJ.Booking_Source

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[ManualPoint]'
GO








ALTER VIEW [work].[ManualPoint]
AS
	
     SELECT 
      tdrJ.confirmationNumber
      ,tdrJ.phgHotelCode
	  ,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp
	  ,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp

     FROM work.tdrJoined tdrJ
        LEFT JOIN Reservations.dbo.mostrecenttransactions mrt
		ON mrt.confirmationNumber = tdrJ.confirmationNumber

  WHERE ( tdrJ.Transaction_Source = 'Hotel Portal' 
		OR (tdrJ.Transaction_Source = 'Admin Portal' AND 
		(tdrJ.Booking_Source = 'PMSBooking'
		OR mrt.confirmationNumber IS NULL))
		)
	GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode
		, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
	HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0.01

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [loyalty].[Credit_Memos]'
GO
CREATE TABLE [loyalty].[Credit_Memos]
(
[Redemption_Date] [datetime] NULL,
[Hotel_Code] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Membership_Number] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Number] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Value] [numeric] (18, 2) NULL,
[Voucher_Currency] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payable_Value] [numeric] (18, 2) NULL,
[Create_Date] [datetime] NOT NULL,
[SOPNumber] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Invoice_Date] [date] NULL,
[JENumber] [int] NULL,
[Currency_Conversion_Date] [date] NULL,
[id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_Credit_Memos] on [loyalty].[Credit_Memos]'
GO
ALTER TABLE [loyalty].[Credit_Memos] ADD CONSTRAINT [PK_Credit_Memos] PRIMARY KEY NONCLUSTERED  ([id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [loyalty].[Credit_Memo_Errors]'
GO
CREATE TABLE [loyalty].[Credit_Memo_Errors]
(
[Redemption_Date] [datetime] NULL,
[Hotel_Code] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Membership_Number] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Type] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Number] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Voucher_Value] [numeric] (18, 2) NULL,
[Voucher_Currency] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Payable_Value_USD] [numeric] (18, 2) NULL,
[filename] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importdate] [datetime] NULL,
[id] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[Error_Message] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [loyalty].[Calculate_Credit_Memos]'
GO

CREATE	procedure [loyalty].[Calculate_Credit_Memos]
AS
BEGIN


declare	@startDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --first day of previous month
		,@endDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) --last day of previous month


/*	It is possible for the same Voucher Number to appear in multiple Epsilon Redemption files. 
	We want to end up with only one record per voucher number. 
	Add handling to achieve the following:

		1. If the voucher number does not previously exist in our data, add it
		2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
		3. If the voucher number DOES exist in our data and DOES have a SOP Number, check all fields of the new record against the existing.
			3a. If the new record matches the old record, ignore the new record
			3b. If the new record has a change in any field from the old record, it needs to be reported as an error to the Finance team somehow
*/


--		1. If the voucher number does not previously exist in our data, add it
select	i.voucher_number, MAX(i.id) mostrecent_id
into	#insert_new
from	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import i
	left outer join loyalty.Credit_Memos cm
		on i.Voucher_Number = cm.Voucher_Number
where	i.importdate >= @startDate
	and i.Redemption_Date <= @enddate
	and cm.Voucher_Number IS NULL
group by i.Voucher_Number


--		2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
select	i.voucher_number , MAX(i.id) mostrecent_id
into	#update_exists -- select * 
from	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import i
	left outer join loyalty.Credit_Memos cm
		on i.Voucher_Number = cm.Voucher_Number
where	i.importdate >= @startDate
	and i.Redemption_Date <= @enddate
	and cm.SOPNumber IS NULL
group by i.Voucher_Number


--		3. If the voucher number DOES exist in our data and DOES have a SOP Number
--			check all fields of the new record against the existing.
--		3a. If the new record matches the old record, ignore the new record
select	Imported.voucher_number
into	#ignore
from	loyalty.Credit_Memos CM
join	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import Imported
 on		CM.Voucher_Number = Imported.Voucher_Number
where	Imported.importdate >= @startDate
and		Imported.Redemption_Date <= @enddate
and		CM.Redemption_Date = Imported.Redemption_Date
and		CM.Hotel_Code = Imported.Hotel_Code 
and		CM.Membership_Number = Imported.Membership_Number
and		CM.Voucher_Value = Imported.Voucher_Value 
and		CM.Voucher_Currency = Imported.Voucher_Currency 
and		CM.Payable_Value = Imported.Payable_Value_USD 
and		CM.SOPNumber is not null

--		3. If the voucher number DOES exist in our data and DOES have a SOP Number
--			check all fields of the new record against the existing.
--		3b. If the new record has a change in any field from the old record
--			it needs to be reported as an error to the Finance team somehow
select	Imported.voucher_number
, 'Voucher_Number already exists with an SOPNumer, but with different data.' as ErrorMessage
into	#error
from	Loyalty.Credit_Memos CM
join	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import Imported
 on		CM.Voucher_Number = Imported.Voucher_Number
where	Imported.importdate >= @startDate
and		Imported.Redemption_Date <= @enddate
and		CM.SOPNumber is not null
and (	CM.Redemption_Date <> Imported.Redemption_Date
or		CM.Hotel_Code <> Imported.Hotel_Code 
or		CM.Membership_Number <> Imported.Membership_Number 
or		CM.Voucher_Value <> Imported.Voucher_Value 
or		CM.Voucher_Currency <> Imported.Voucher_Currency 
or		CM.Payable_Value <> Imported.Payable_Value_USD 
	)


insert	into	loyalty.Credit_Memos
	(	Redemption_Date, Hotel_Code, Membership_Number, Voucher_Number, 
		Voucher_Value, Voucher_Currency, Payable_Value, 
		Create_Date, SOPNumber, Invoice_Date, Currency_Conversion_Date
	)
select	Redemption_Date, Hotel_Code, Membership_Number, i.Voucher_Number, 
		Voucher_Value, Voucher_Currency, Payable_Value_USD, 
		GETDATE() as Create_Date, null as SOPNumber, null as Invoice_Date, Currency_Conversion_Date
from	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import BSI
join	#insert_new i
	on	BSI.Voucher_Number = i.Voucher_Number
	and	BSI.id = i.mostrecent_id

update	CM
set		CM.Redemption_Date = Imported.Redemption_Date, 
		CM.Hotel_Code = Imported.Hotel_Code , 
		CM.Membership_Number = Imported.Membership_Number , 
		CM.Voucher_Value = Imported.Voucher_Value , 
		CM.Voucher_Currency = Imported.Voucher_Currency , 
		CM.Payable_Value = Imported.Payable_Value_USD , 
		CM.Currency_Conversion_Date = Imported.Currency_Conversion_Date, 
		CM.Create_Date = GetDate()
from	Loyalty.Credit_Memos CM
join	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import Imported
	on	CM.Voucher_Number = Imported.Voucher_Number
join	#update_exists u
	on	CM.Voucher_Number = u.Voucher_Number
	and	Imported.id = u.mostrecent_id


-- empty table for the current run
truncate table loyalty.[Credit_Memo_Errors]

INSERT INTO Loyalty.[Credit_Memo_Errors]
	(	[Redemption_Date],[Hotel_Code],[Hotel_Name],[Membership_Number],[Member_Name],[Voucher_Type]
		,[Voucher_Number],[Voucher_Name],[Voucher_Value],[Voucher_Currency],[Payable_Value_USD]
		,[filename],[importdate],[Error_Message]
	)
select	imported.Redemption_Date, imported.Hotel_Code, imported.Hotel_Name, imported.Membership_Number, 
		imported.Member_Name, imported.Voucher_Type, imported.Voucher_Number, imported.Voucher_Name,
		imported.Voucher_Value, imported.Voucher_Currency, imported.Payable_Value_USD, 
		imported.filename, imported.importdate, e.ErrorMessage
from 	#error e
join	Loyalty.dbo.[Account_Statement_Hotel_Redemption_Import] imported
	on	e.Voucher_Number = imported.Voucher_Number
           

drop table #insert_new
drop table #update_exists
drop table #error
drop table #ignore

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [work].[tdrJoinedManual_Reservation]'
GO


CREATE VIEW [work].[tdrJoinedManual_Reservation]
AS
     SELECT 
			3 as transactionSourceID, --3 = I Prefer Manual Transactions
			tdr.Transaction_Id as transactionKey,
            tdr.Booking_ID as confirmationNumber, 
			hotels.HotelCode AS phgHotelCode,
            NULL AS crsHotelID,
            hotels.hotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            null as chainID,
            'IPM' as chainName,
            'IPM' as bookingStatus,
            'iPrefer Manual Entry' as synxisBillingDescription,
            'iPrefer Manual Entry' AS bookingChannel,
            'iPrefer Manual Entry' AS bookingSecondarySource,
            'iPrefer Manual Entry' AS bookingSubSourceCode,
            null as bookingTemplateGroupId,
            'IPM' as bookingTemplateAbbreviation,
            'IPM' as xbeTemplateName,
            'iPrefer Manual Entry' as CROcode,
            null as bookingCroGroupID,
            'iPrefer Manual Entry' as bookingRateCategoryCode,
            'iPrefer Manual' as bookingRateCode,
            'iPrefer Manual' as bookingIATA,
            tdr.Reward_Posting_Date as transactionTimeStamp,
            (SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date), (tdr.Departure_Date), (tdr.Reward_Posting_Date)) AS Fields(d)) as confirmationDate,
            tdr.Arrival_Date as arrivalDate,
            tdr.Departure_Date as departureDate,
            CAST(null as date) as cancellationDate,
            CAST(null as varchar) as cancellationNumber,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as nights,
            1 as rooms,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as roomNights,
            tdr.Reservation_Revenue as roomRevenueInBookingCurrency,
            tdr.Currency_Code as bookingCurrencyCode,
            null as timeLoaded,
			'IPREFERMANUAL' AS [ItemCode],
			CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            null as CRSSourceID,
			'I Prefer' as loyaltyProgram,
			tdr.iPrefer_Number as loyaltyNumber,
			'iPrefer Manual Entry' as travelAgencyName,
			1 as LoyaltyNumberValidated,
			CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END as LoyaltyNumberTagged,
			tdr.Transaction_Source, 
			tdr.Booking_Source  

     FROM Loyalty.dbo.[TransactionDetailedReport] tdr
        LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
        LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = bookingCE.EXCHDATE
		LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
			AND tdr.Hotel_Code = tc.Hotel_Code
			AND tc.DateTagged <= tdr.Reward_Posting_Date
		LEFT JOIN Reservations.dbo.MostRecentTransactions mrt ON tdr.Booking_ID = mrt.confirmationNumber  --only when point can be linked to Reservation
  WHERE tdr.Reservation_Revenue <> 0
	AND tdr.Points_Earned <> 0
	AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') --remove epsilon SFTP and all BSI old point activity
	AND tdr.Hotel_Code <> 'PHG123'
	AND tdr.Remarks NOT LIKE '%migration%' 
	AND tdr.Hotel_Code IS NOT NULL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[LoadBillyErrorTable]'
GO



-- =============================================
-- Author:		Kris Scott
-- Create date: 04/29/2016
-- Description:	
-- History: 2019-06-03 Ti Yao Swith to Reservations DB version
-- EXEC [work].[LoadBillyErrorTable] 19646
-- =============================================
ALTER PROCEDURE [work].[LoadBillyErrorTable]
	@RunID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
--DECLARE @RunID int = 19646
declare	@startDate DATE, 
		@endDate DATE,
		@maxCalcDate DATE;
		
-- get current run dates
select	@startDate = startdate, 
		@endDate = enddate,
		@maxCalcDate = RunDate
from	[work].[Run]
WHERE runID = @runID

  SELECT
	c.transactionSourceID, c.transactionKey
  INTO #nonBillable
  FROM dbo.Charges as c
  WHERE c.billableDate BETWEEN @startDate AND @endDate
  AND c.[classificationID] = 4
  
  CREATE TABLE #mrtWithoutCharges
  (
	[hotelCode] nvarchar(50)
    ,[hotelName] nvarchar(250)
    ,[synxisID] nvarchar(50)
    ,[confirmationNumber] nvarchar(max)
	,classificationID int
    ,clauseID int
    ,[channel] nvarchar(max)
    ,[secondarySource] nvarchar(max)
    ,[subSourceCode] nvarchar(max)
    ,[CROCode] nvarchar(max)
    ,[templateAbbreviation] nvarchar(max)
    ,[xbeTemplateName] nvarchar(max)
    ,[rateCategoryCode] nvarchar(max)
    ,[rateTypeCode] nvarchar(max)
    ,[arrivalDate] date
    ,[OpenHospitalityID] nvarchar(50)
    ,currency nvarchar(5)
	,bookingChargesCount int
	,commissionChargesCount int
	,iPreferChargesCount int
	,phHotelCode nvarchar(50)
	,gpHotelCode nvarchar(50)
	,gpCurrency nvarchar(5)
	,resCurrency nvarchar(5)
  )

INSERT into	#mrtWithoutCharges
   select 
	COALESCE(hrs.code, hro.code,hh.HotelCode) as [hotelCode]
    ,hh.HotelName as [hotelName]
    ,hh.synXisID as [synxisID]
    ,t.confirmationNumber as [confirmationNumber]
	,cd.classificationID
    ,brule.clauseID
    ,cha.channel as [channel]
    ,sec.secondarySource as [secondarySource]
    ,sub.subSourceCode as [subSourceCode]
    ,acro.CRO_Code as [CROCode]
    ,ISNULL(pt.[siteAbbreviation], 'HOTEL') as [templateAbbreviation]
    ,aibe.ibeSourceName as [xbeTemplateName]
    ,rc.rateCategoryCode as [rateCategoryCode]
    ,rac.RateCode as [rateTypeCode]
    ,td.arrivalDate as [arrivalDate]
    ,hh.openhospID as [OpenHospitalityID]
    ,td.currency
	,bc.bookingChargesCount
	,cc.commissionChargesCount
	,ipc.iPreferChargesCount
	,COALESCE(hrs.code, hro.code, NULL) as phHotelCode
	,gpc.custnmbr as gpHotelCode
	,gpc.CURNCYID as gpCurrency
	,cur.CURNCYID as resCurrency
FROM Reservations.dbo.Transactions t 
	INNER JOIN Reservations.dbo.TransactionStatus ts  ON ts.TransactionStatusID = t.TransactionStatusID
	INNER JOIN Reservations.dbo.TransactionDetail td  ON td.TransactionDetailID = t.TransactionDetailID
	LEFT JOIN Reservations.dbo.Chain ch  ON ch.ChainID = t.ChainID
	LEFT JOIN Reservations.dbo.hotel ht  ON ht.HotelID = t.HotelID
	LEFT JOIN Hotels.dbo.Hotel hh  ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
	LEFT JOIN Reservations.dbo.CRS_BookingSource bs  ON bs.BookingSourceID = t.CRS_BookingSourceID
	LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
	LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
	LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
	LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
	LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
	LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
	LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
	LEFT JOIN Reservations.dbo.RateCategory rc  ON rc.RateCategoryID = t.RateCategoryID
	LEFT JOIN Reservations.dbo.IATANumber iata  ON iata.IATANumberID = t.IATANumberID
	LEFT JOIN Reservations.dbo.TravelAgent ta  ON ta.TravelAgentID = t.TravelAgentID
	LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
	LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
	LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
	LEFT JOIN Reservations.synxis.transactions tr ON tr.TransactionID = t.sourceKey AND t.DataSourceID IN(SELECT DataSourceID FROM Reservations.authority.DataSource WHERE SourceName = 'SynXis')
	LEFT JOIN Reservations.synxis.BillingDescription bd ON tr.BillingDescriptionID = bd.BillingDescriptionID

	LEFT OUTER JOIN [dbo].[Templates] pt ON aibe.ibeSourceName = pt.[xbeTemplateName]
	LEFT OUTER JOIN dbo.Charges cd on t.TransactionID = cd.transactionKey AND cd.transactionSourceID IN (1,2)
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on t.TransactionID = bc.transactionKey AND bc.transactionSourceID IN (1,2)
	LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on t.TransactionID = cc.transactionKey AND cc.transactionSourceID IN (1,2)	
	LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON t.TransactionID = ipc.transactionKey AND ipc.transactionSourceID IN (1,2)	
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hrs ON hh.synXisID = hrs.synxisID
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hro ON hh.openhospID = hro.openHospitalityCode
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON COALESCE(hro.code, hrs.code) = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON td.currency = cur.CURNCYID		AND td.arrivalDate = cur.EXCHDATE
	LEFT OUTER JOIN #nonBillable nb	on t.TransactionID = nb.transactionKey AND nb.transactionSourceID IN (1,2)
where (cd.hotelCode IS NULL OR (bookingChargesCount IS NULL OR bookingChargesCount <> 1) OR (commissionChargesCount > 1))
and td.arrivalDate BETWEEN @startDate AND @endDate
AND t.timeLoaded < DATEADD(day,-2,@maxCalcDate)
AND ts.status <> 'Cancelled'
AND nb.transactionKey IS NULL; --only want to see bookings not handled by non-billable charges


INSERT into	#mrtWithoutCharges
   select 
	tdr.Hotel_Code as [hotelCode]
    ,hr.hotelName as [hotelName]
    ,'' as [synxisID]
    ,tdr.Booking_ID as [confirmationNumber]
	,cd.classificationID
    ,brule.clauseID
    ,'iPrefer Manual Entry' as [channel]
    ,tdr.Transaction_Source as [secondarySource]
    ,tdr.Remarks as [subSourceCode]
    ,'' as [CROCode]
    ,'' as [templateAbbreviation]
    ,'' as [xbeTemplateName]
    ,'' as [rateCategoryCode]
    ,'' as [rateTypeCode]
    ,tdr.Reward_Posting_Date as [arrivalDate]
    ,'' as [OpenHospitalityID]
    ,tdr.Currency_Code
	,bc.bookingChargesCount
	,cc.commissionChargesCount
	,ipc.iPreferChargesCount
	,hr.code as phHotelCode
	,gpc.custnmbr as gpHotelCode
	,gpc.CURNCYID as gpCurrency
	,cur.CURNCYID as resCurrency
from Loyalty.dbo.TransactionDetailedReport tdr
	LEFT OUTER JOIN Hotels.dbo.hotelsReporting hr ON tdr.Hotel_Code = hr.code
	LEFT OUTER JOIN dbo.Charges cd on tdr.Transaction_Id = cd.transactionKey AND cd.transactionSourceID = 3 --iprefer manual points
	LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
	LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on tdr.Transaction_Id = bc.transactionKey AND bc.transactionSourceID = 3
	LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on tdr.Transaction_Id = cc.transactionKey AND cc.transactionSourceID = 3	
	LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON tdr.Transaction_Id = ipc.transactionKey AND ipc.transactionSourceID = 3	
	LEFT OUTER JOIN IC.dbo.RM00101 gpc ON hr.code = gpc.custnmbr
	LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON tdr.Currency_Code = cur.CURNCYID AND tdr.Reward_Posting_Date = cur.EXCHDATE
	LEFT OUTER JOIN #nonBillable nb	on tdr.Transaction_Id = nb.transactionKey AND nb.transactionSourceID = 3 --iprefer manual points
where (cd.hotelCode IS NULL OR (ipc.iPreferChargesCount IS NULL OR ipc.iPreferChargesCount  <> 1))
and tdr.Reward_Posting_Date BETWEEN @startDate AND @endDate
AND tdr.Reward_Posting_Date < DATEADD(day,-2,@maxCalcDate)
AND tdr.Reservation_Revenue <> 0
AND tdr.Points_Earned <> 0
AND tdr.Transaction_Source IN ('Admin Portal'
	,'Hotel Portal'
	)
AND nb.transactionKey IS NULL --only want to see bookings not handled by non-billable charges;



	--	empty the billy error table
	truncate table dbo.BillyCalcErrors

	-- reinsert new error records into the table
	insert into dbo.BillyCalcErrors
	(errorMessage, hotelCode, hotelName, synxisID, openHospitalityID, confirmationNumber, clauseID, channel, secondarySource, subSourceCode, CROCode, templateAbbreviation, xbeTemplateName, rateCategoryCode, rateTypeCode, arrivalDate)


SELECT 'Unable to map hotel to CRM' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NULL

UNION ALL

SELECT 'No hotel found in GP' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NULL

UNION ALL

SELECT 'No currency found on GP customer card' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NULL

UNION ALL

SELECT 'No currency exchange rate found in GP for ' + currency as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND mrt.arrivalDate < GETDATE()
AND resCurrency IS NULL 

UNION ALL

SELECT DISTINCT 'Multiple booking charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND mrt.bookingChargesCount > 1
AND mrt.classificationID = 1

UNION ALL

SELECT DISTINCT 'No booking or I Prefer charge found' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (
	(mrt.bookingChargesCount = 0 OR mrt.bookingChargesCount IS NULL)
	AND (mrt.ipreferChargesCount = 0 OR mrt.ipreferChargesCount IS NULL)
	)

UNION ALL

SELECT DISTINCT 'Multiple commission charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.commissionChargesCount > 1)
AND mrt.classificationID = 2


UNION ALL


SELECT DISTINCT 'Multiple iPrefer charges' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.iPreferChargesCount > 1)
AND mrt.classificationID = 5


UNION ALL

SELECT 'Unknown error' as [errorMessage],
		[hotelCode]
      ,[hotelName]
      ,[synxisID]
	  ,OpenHospitalityID
      ,[confirmationNumber]
      ,[clauseID]
      ,[channel]
      ,[secondarySource]
      ,[subSourceCode]
      ,[CROCode]
      ,[templateAbbreviation]
      ,[xbeTemplateName]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[arrivalDate]
FROM #mrtWithoutCharges mrt
WHERE phHotelCode IS NOT NULL
AND gpHotelCode IS NOT NULL
AND gpCurrency IS NOT NULL
AND (mrt.bookingChargesCount = 1)
--AND (mrt.surchargeCount IS NULL OR mrt.surchargeCount <= 1)
AND (mrt.commissionChargesCount IS NULL OR mrt.commissionChargesCount <= 1)
;

DROP TABLE #mrtWithoutCharges;
DROP TABLE #nonBillable;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[tdrJoined_Reservation]'
GO

ALTER VIEW [work].[tdrJoined_Reservation]
AS
     SELECT 
			3 as transactionSourceID, --3 = I Prefer Manual Transactions
			tdr.Transaction_Id as transactionKey,
            tdr.Booking_ID as confirmationNumber, 
			hotels.HotelCode AS phgHotelCode,
            NULL AS crsHotelID,
            hotels.hotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            null as chainID,
            'IPM' as chainName,
            'IPM' as bookingStatus,
            'iPrefer Manual Entry' as synxisBillingDescription,
            'iPrefer Manual Entry' AS bookingChannel,
            'iPrefer Manual Entry' AS bookingSecondarySource,
            'iPrefer Manual Entry' AS bookingSubSourceCode,
            null as bookingTemplateGroupId,
            'IPM' as bookingTemplateAbbreviation,
            'IPM' as xbeTemplateName,
            'iPrefer Manual Entry' as CROcode,
            null as bookingCroGroupID,
            'iPrefer Manual Entry' as bookingRateCategoryCode,
            'iPrefer Manual' as bookingRateCode,
            'iPrefer Manual' as bookingIATA,
            tdr.Reward_Posting_Date as transactionTimeStamp,
            (SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date), (tdr.Departure_Date), (tdr.Reward_Posting_Date)) AS Fields(d)) as confirmationDate,
            tdr.Arrival_Date as arrivalDate,
            tdr.Departure_Date as departureDate,
            CAST(null as date) as cancellationDate,
            CAST(null as varchar) as cancellationNumber,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as nights,
            1 as rooms,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as roomNights,
            tdr.Reservation_Revenue as roomRevenueInBookingCurrency,
            tdr.Currency_Code as bookingCurrencyCode,
            null as timeLoaded,
			'IPREFERMANUAL' AS [ItemCode],
			CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            null as CRSSourceID,
			'I Prefer' as loyaltyProgram,
			tdr.iPrefer_Number as loyaltyNumber,
			'iPrefer Manual Entry' as travelAgencyName,
			1 as LoyaltyNumberValidated,
			CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END as LoyaltyNumberTagged,
			tdr.Transaction_Source, 
			tdr.Booking_Source  

     FROM Loyalty.dbo.[TransactionDetailedReport] tdr
        LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
        LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = bookingCE.EXCHDATE
		LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
			AND tdr.Hotel_Code = tc.Hotel_Code
			AND tc.DateTagged <= tdr.Reward_Posting_Date
		INNER JOIN Reservations.dbo.MostRecentTransactions mrt ON tdr.Booking_ID = mrt.confirmationNumber  --only when point can be linked to Reservation

  WHERE tdr.Reservation_Revenue <> 0
	AND tdr.Points_Earned <> 0
	AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') --remove epsilon SFTP and all BSI old point activity
	AND tdr.Hotel_Code <> 'PHG123'
	AND tdr.Remarks NOT LIKE '%migration%' 
	AND tdr.Hotel_Code IS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[ManualPoint_Reservation]'
GO



ALTER VIEW [work].[ManualPoint_Reservation]
AS
	
     SELECT 
      tdrJ.loyaltyNumber + tdrJ.phgHotelCode + CAST(YEAR(tdrJ.confirmationdate) AS varchar(4)) + '-' +  CAST(MONTH(tdrJ.confirmationdate) AS varchar(2))  AS confirmationNumber
	  ,tdrJ.loyaltyNumber
      ,tdrJ.phgHotelCode
	  ,MONTH(tdrJ.confirmationdate) AS MONtransactionTimeStamp
	  ,YEAR(tdrJ.confirmationdate) AS YEARtransactionTimeStamp
	  ,SUM(tdrJ.roomRevenueInBookingCurrency* 1.00/tdrJ.bookingCurrencyExchangeRate ) AS roomRevenueInHotelCurrency
	  , MIN(tdrJ.confirmationdate) AS confirmationdate
	  , MIN(tdrJ.transactionKey) AS transactionKey
     FROM work.tdrJoinedManual_Reservation tdrJ
        LEFT JOIN Reservations.dbo.transactions mrt
		ON mrt.confirmationNumber = tdrJ.confirmationNumber

  WHERE ( tdrJ.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') AND mrt.confirmationNumber IS NULL)
	GROUP BY tdrJ.loyaltyNumber, tdrJ.phgHotelCode
		, MONTH(tdrJ.confirmationdate), YEAR(tdrJ.confirmationdate)
	HAVING SUM(tdrJ.roomRevenueInBookingCurrency* 1.00/tdrJ.bookingCurrencyExchangeRate ) > 0.1

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[mrtJoined_Reservation]'
GO









ALTER VIEW [work].[mrtJoined_Reservation]
AS

     SELECT 
			1 AS transactionSourceID, -- hardcode 1 use transactionID for the difference between openhospitality and sabre
			t.TransactionID as transactionKey,
            t.confirmationNumber,
			hh.HotelCode AS phgHotelCode,
            COALESCE(hh.openhospID, hh.synXisID) AS crsHotelID,
            hh.HotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
			CASE WHEN ch.ChainID = 22 THEN 56 ELSE ch.ChainID END AS [ChainID],--change from CRS_ChainID since open hospitality CRS_ChainID is not integer
            ch.chainName,
            ts.status AS bookingStatus,
            CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN td.billingDescription ELSE 'Unknown' END AS synxisBillingDescription,
            cha.channel AS bookingChannel,
            sec.secondarySource AS bookingSecondarySource,
            sub.subSourceCode AS bookingSubSourceCode,
            COALESCE(te.templateGroupID, 2) AS bookingTemplateGroupId,
			CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END AS bookingTemplateAbbreviation,
            aibe.ibeSourceName AS xbeTemplateName,
            acro.CRO_Code AS CROcode,
            CASE WHEN acro.CRO_Code IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            rc.rateCategoryCode AS bookingRateCategoryCode,
            rac.RateCode AS bookingRateCode,
            ISNULL(iata.IATANumber,'') AS bookingIATA,
            t.transactionTimeStamp,
            ts.confirmationDate,
            td.arrivalDate,
            td.departureDate,
            ts.cancellationDate,
            ts.cancellationNumber,
            td.nights,
            td.rooms,
            td.nights * td.rooms AS roomNights,
            td.reservationRevenue AS roomRevenueInBookingCurrency,
            td.currency AS bookingCurrencyCode,
            t.timeLoaded,
			work.[billyItemCode](CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN td.billingDescription ELSE 'Unknown' END,cha.channel,sec.secondarySource,sub.subSourceCode,CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END,croCodes.croGroupID, 123) AS [ItemCode], --hard code chainId since OH chainID is not int
			CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN ts.confirmationDate ELSE td.arrivaldate END) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            CASE WHEN bs.openHospID IS NOT NULL THEN 2 WHEN bs.synXisID IS NOT NULL THEN 1 ELSE 'Unknown' END AS CRSSourceID,
			lp.LoyaltyProgram,
			ISNULL(ln.loyaltyNumber,'') AS loyaltyNumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],
			--force tag if negative iprefer returns
			 CASE WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberValidated,0)
					WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					WHEN work.billyLoyaltyFlipFlag(t.confirmationNumber) = 1 THEN 1
					ELSE ISNULL(td.LoyaltyNumberValidated,0) END AS LoyaltyNumberValidated
				--new change
			,CASE WHEN ISNULL(td.LoyaltyNumberTagged,0) = 0 THEN 0
					 WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberTagged,0)
					 WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					 ELSE ISNULL(td.LoyaltyNumberTagged,0)
					 END as LoyaltyNumberTagged
		FROM Reservations.dbo.Transactions t WITH(NOLOCK)
		INNER JOIN Reservations.dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN Reservations.dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN Reservations.dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN Reservations.dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
		LEFT JOIN Reservations.dbo.CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
		LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
		LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
		LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
		LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
		LEFT JOIN Reservations.dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN Reservations.dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN Reservations.dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID

        LEFT JOIN work.hotelActiveBrands ON hh.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hh.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hh.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON td.currency = bookingCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = bookingCE.EXCHDATE
		LEFT JOIN Loyalty.dbo.[TransactionDetailedReport] tdr ON tdr.Booking_ID = t.confirmationNumber AND tdr.Hotel_Code = hh.HotelCode
		AND MONTH(tdr.Reward_Posting_Date) = MONTH(td.arrivalDate) AND YEAR(tdr.Reward_Posting_Date) = YEAR(td.arrivalDate) AND tdr.Reservation_Revenue <> 0 AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source IN ('Admin Portal','Hotel Portal')
		LEFT JOIN [dbo].[CROCodes] ON acro.CRO_Code = croCodes.croCode
		LEFT JOIN dbo.Templates te ON te.xbeTemplateName = aibe.ibeSourceName

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Populate_MrtForCalculation_ByDate_Reservation]'
GO











ALTER PROCEDURE [work].[Populate_MrtForCalculation_ByDate_Reservation]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	;WITH cte_Invoiced
	AS
	(
		SELECT DISTINCT transactionSourceID,transactionKey,sopNumber, confirmationNumber -- Adding for manual point award without reservation 
		FROM dbo.Charges
		WHERE sopNumber IS NOT NULL
	)
	INSERT INTO [work].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],
			mrtj.[gpSiteID],mrtj.[chainID],mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],
			mrtj.[bookingChannel],mrtj.[bookingSecondarySource],mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],
			mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],mrtj.[bookingCroGroupID],
			mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],
			mrtj.[nights],mrtj.[rooms],mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],
			mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],mrtj.exchangeDate,mrtj.hotelCurrencyCode,
			mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],mrtj.LoyaltyNumberValidated,
			mrtj.LoyaltyNumberTagged,mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.transactionKey
	FROM work.mrtJoined_Reservation mrtj
		LEFT JOIN cte_Invoiced inv ON inv.transactionKey = mrtj.transactionKey AND inv.transactionSourceID =  mrtj.transactionSourceID
	WHERE mrtj.arrivalDate BETWEEN @startDate AND @endDate
		AND inv.transactionKey IS NULL
	
	UNION ALL

	--Manual Point award with Superset Reservation information	
	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],
			tdr.[gpSiteID],tdr.[chainID],tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],
			tdr.[bookingChannel],tdr.[bookingSecondarySource],tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],
			tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],tdr.[bookingCroGroupID],
			tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],
			tdr.[nights],tdr.[rooms],tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],
			tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,tdr.hotelCurrencyCode,
			tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,
			tdr.loyaltyProgram,tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,
			tdr.transactionTimestamp,tdr.transactionSourceID,tdr.transactionKey
	FROM work.tdrJoined_Reservation tdr
		LEFT JOIN cte_Invoiced inv ON inv.transactionKey = tdr.transactionKey AND inv.transactionSourceID =  tdr.transactionSourceID
  WHERE tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
	AND inv.transactionKey IS NULL --not previously invoiced

	UNION ALL

	--Manual Point award without Superset Reservation information	
				
	SELECT DISTINCT @RunID,MP.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],
			tdr.[gpSiteID],tdr.[chainID],tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],
			tdr.[bookingChannel],tdr.[bookingSecondarySource],tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],
			tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],tdr.[bookingCroGroupID],
			tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],MP.[confirmationDate] AS [transactionTimeStamp],
			mp.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],
			tdr.[nights],tdr.[rooms],tdr.[roomNights],mp.roomRevenueInHotelCurrency AS [roomRevenueInBookingCurrency],tdr.hotelCurrencyCode AS [bookingCurrencyCode],
			tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,tdr.hotelCurrencyCode,
			tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,
			tdr.loyaltyProgram,tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,
			tdr.transactionTimestamp,tdr.transactionSourceID,mp.transactionKey
	FROM work.tdrJoinedMaual_Reservation tdr
		INNER JOIN work.ManualPoint_Reservation MP ON tdr.phgHotelCode = MP.phgHotelCode 
			AND tdr.loyaltyNumber = MP.loyaltyNumber AND MP.MONtransactionTimeStamp = MONTH(tdr.confirmationdate)
			AND MP.YEARtransactionTimeStamp = YEAR(tdr.confirmationdate)
		LEFT JOIN cte_Invoiced inv ON inv.transactionKey = tdr.transactionKey AND inv.transactionSourceID =  tdr.transactionSourceID AND mp.confirmationNumber = inv.confirmationNumber
  WHERE tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
	AND inv.transactionKey IS NULL --not previously invoiced

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Reservations]'
GO


ALTER VIEW [dbo].[Reservations]
AS

SELECT 
mrt.confirmationNumber
, COALESCE (synxisHotels.code, OHHotels.code) AS hotelCode
, COALESCE (synxisHotels.synxisID, OHHotels.openHospitalityCode) AS SynxisID
, COALESCE (synxisHotels.hotelName, OHHotels.hotelName) AS hotelName
, COALESCE (activeBrands.code, inactiveBrands.code) AS mainBrandCode
, COALESCE (activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID
, mrt.chainID
, mrt.chainName
, mrt.status AS bookingStatus
, mrt.billingDescription AS synxisBillingDescription
, mrt.channel AS bookingChannel
, mrt.secondarySource AS bookingSecondarySource
, mrt.subSourceCode AS bookingSubSourceCode
, COALESCE (dbo.[Templates].[templateID], 2) AS bookingTemplateOptionId
, COALESCE (dbo.[Templates].siteAbbreviation, N'HOTEL') AS bookingTemplateAbbreviation
, mrt.xbeTemplateName
, mrt.CROCode
, CASE WHEN mrt.CROCode IS NULL THEN 0 
	ELSE COALESCE (croCodes.[croID], 2) 
	END AS bookingCroOptionID
, crog.[croGroupName] as CroOption
, mrt.rateCategoryCode AS bookingRateCategoryCode
, mrt.rateTypeCode AS bookingRateCode
, mrt.IATANumber AS bookingIATA
, mrt.transactionTimeStamp
, mrt.confirmationDate
, mrt.arrivalDate
, mrt.departureDate
, mrt.cancellationDate
, mrt.cancellationNumber
, mrt.nights
, mrt.rooms
, mrt.nights * mrt.rooms AS roomNights
, mrt.reservationRevenue AS roomRevenueInBookedCurrency
, mrt.currency AS currencyCodeBooked
, mrt.timeLoaded
, mrt.CRSSourceID
, mrt.loyaltyNumber AS LoyaltyNumber
, CASE WHEN mrt.LoyaltyNumberTagged = 1 THEN 'Tagged' 
	WHEN mrt.loyaltyNumber IS NULL OR mrt.loyaltyNumber = '' THEN 'None' 
	WHEN mrt.[LoyaltyNumberValidated] = 0 THEN 'Invalid' 
	WHEN mrt.[LoyaltyNumberValidated] = 1 THEN 'Valid' 
	ELSE 'None' 
	END AS LoyaltyStatus
FROM            Reservations.dbo.mostrecenttransactions AS mrt LEFT OUTER JOIN
                         Core.dbo.hotelsReporting AS synxisHotels ON synxisHotels.synxisID = mrt.hotelID LEFT OUTER JOIN
                         Core.dbo.hotelsReporting AS OHHotels ON OHHotels.openHospitalityCode = mrt.OpenHospitalityID LEFT OUTER JOIN
                         WORK.hotelActiveBrands ON COALESCE (synxisHotels.code, OHHotels.code) = WORK .hotelActiveBrands.hotelCode LEFT OUTER JOIN
                         Core.dbo.brands AS activeBrands ON WORK .hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy LEFT OUTER JOIN
                         WORK.hotelInactiveBrands ON COALESCE (synxisHotels.code, OHHotels.code) = WORK .hotelInactiveBrands.hotelCode LEFT OUTER JOIN
                         Core.dbo.brands AS inactiveBrands ON WORK .hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy LEFT OUTER JOIN
                         dbo.croCodes ON mrt.CROCode = dbo.croCodes.croCode LEFT OUTER JOIN
                         dbo.Templates ON mrt.xbeTemplateName = dbo.[Templates].xbeTemplateName LEFT OUTER JOIN
						 dbo.[CROGroups] crog ON crog.[croGroupID] = croCodes.[croGroupID]

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[ManualPoint]'
GO







ALTER VIEW [test].[ManualPoint]
AS
	
     SELECT 
      tdrJ.confirmationNumber
      ,tdrJ.phgHotelCode
	  ,MONTH(tdrJ.transactionTimeStamp) AS MONtransactionTimeStamp
	  ,YEAR(tdrJ.transactionTimeStamp) AS YEARtransactionTimeStamp

     FROM test.tdrJoined tdrJ
        LEFT JOIN Reservations.dbo.mostrecenttransactions mrt
		ON mrt.confirmationNumber = tdrJ.confirmationNumber

  WHERE ( tdrJ.Transaction_Source = 'Hotel Portal' 
		OR (tdrJ.Transaction_Source = 'Admin Portal' AND 
		(tdrJ.Booking_Source = 'PMSBooking'
		OR mrt.confirmationNumber IS NULL))
		)
	GROUP BY tdrJ.confirmationNumber, tdrJ.phgHotelCode
		, MONTH(tdrJ.transactionTimeStamp), YEAR(tdrJ.transactionTimeStamp)
	HAVING SUM((tdrJ.roomRevenueInBookingCurrency/tdrJ.bookingCurrencyExchangeRate) * 1.00) > 0.01
-- This view is not used in billy since left join mrt is time costly without filter.
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [loyalty].[HotelRedemptionRules]'
GO
CREATE TABLE [loyalty].[HotelRedemptionRules]
(
[HotelRedemptionRulesID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[HotelID] [int] NULL,
[ReimbursementPercentage] [decimal] (4, 3) NULL,
[StartDate] [date] NULL,
[EndDate] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_HotelRedemptionRules] on [loyalty].[HotelRedemptionRules]'
GO
ALTER TABLE [loyalty].[HotelRedemptionRules] ADD CONSTRAINT [PK_HotelRedemptionRules] PRIMARY KEY NONCLUSTERED  ([HotelRedemptionRulesID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[iPreferPointAwards]'
GO



ALTER VIEW [dbo].[iPreferPointAwards]
AS
SELECT        ISNULL(CAST((row_number() OVER (ORDER BY Booking_ID)) AS int), 0) AS EDMXID, Booking_ID, iPrefer_Number, Member_Status, Reward_Posting_Date AS pointAwardDate, Booking_Source, Transaction_Source, Remarks, Campaign, Points_Earned, 
                         Reservation_Revenue, Currency_Code
FROM            Loyalty.dbo.TransactionDetailedReport
WHERE        (Booking_ID IS NOT NULL) AND (Booking_ID <> '')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
