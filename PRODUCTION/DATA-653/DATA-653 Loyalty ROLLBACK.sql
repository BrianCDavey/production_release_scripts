USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/2/2020 12:46:29 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Customer_Profile_Import]'
GO
DROP VIEW [dbo].[Customer_Profile_Import]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[BSI_Populate_RewardActivity]'
GO

ALTER   PROCEDURE [dbo].[BSI_Populate_RewardActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'BSI'

	MERGE INTO dbo.RewardActivity AS tgt
	USING
	(
		SELECT ra.[RedemptionDate],rt.[RewardTypeID],r.[RewardID],ra.[VoucherValue],ra.[PayableValue_USD],ra.[VoucherCurrency_Date],
				CurrencyRates.[dbo].[convertCurrency](ra.[VoucherValue],'USD',ra.[VoucherCurrency],ra.[VoucherCurrency_Date]) AS [CurrencyExchangeRate],
				ra.[LoyaltyNumberID],@ds AS DataSourceID,ra.[RewardActivityID],ra.[VoucherCurrency]
		FROM [BSI].[RewardActivity] ra
			LEFT JOIN [dbo].[RewardType] rt ON rt.BSI_ID = ra.[VoucherTypeID]
			LEFT JOIN [dbo].[Reward] r ON r.BSI_ID = ra.[VoucherID]
	) AS src ON src.DataSourceID = tgt.DataSourceID AND src.[RewardActivityID] = tgt.[Internal_SourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [CreateDate] = src.[RedemptionDate],
				[RewardTypeID] = src.[RewardTypeID],
				[RewardID] = src.[RewardID],
				[RewardCost] = src.[VoucherValue],
				[RewardValueUSD] = src.[PayableValue_USD],
				[CurrencyExchangeDate] = src.[VoucherCurrency_Date],
				[CurrencyExchangeRate] = src.[CurrencyExchangeRate],
				[LoyaltyNumberID] = src.[LoyaltyNumberID],
				[RewardCurrency] = src.[VoucherCurrency]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([CreateDate],[RewardTypeID],[RewardID],[RewardCost],[RewardValueUSD],[CurrencyExchangeDate],
				[CurrencyExchangeRate],
				[LoyaltyNumberID],[DataSourceID],[Internal_SourceKey],[RewardCurrency])
		VALUES(src.[RedemptionDate],src.[RewardTypeID],src.[RewardID],src.[VoucherValue],src.[PayableValue_USD],src.[VoucherCurrency_Date],
				src.[CurrencyExchangeRate],
				src.[LoyaltyNumberID],src.DataSourceID,src.[RewardActivityID],src.[VoucherCurrency])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransactionDetailedReport]'
GO
ALTER VIEW [dbo].[TransactionDetailedReport]
AS

SELECT 
p.QueueID AS [QueueID]	
, p.[External_SourceKey] AS [Transaction_Id]
, acs.ActivityCauseSystemName AS [Transaction_Source]
, p.[Notes] AS [Remarks]	
, l.loyaltyNumberName AS [iPrefer_Number]
, CASE WHEN g.GuestID IS NULL THEN 'D' ELSE 'E' END AS [Member_Status] 
, p.TransactionNumber AS [Booking_ID]	 
, t.HotelCode AS [Hotel_Code]	
, p.[ActivityCauseDate] AS [Arrival_Date]	
, t.departureDate AS [Departure_Date]	
, pc.PH_Channel AS [Booking_Source]	
, actT.[ActivityTypeName] AS [Campaign] 
, CASE WHEN p.Points > 0 THEN p.Points ELSE 0 END AS [Points_Earned] 
, CASE WHEN p.Points < 0 THEN p.Points * -1 ELSE 0 END AS [Points_Redemeed]
, p.ActivityCauseAmount AS [Reservation_Revenue] 
, p.[ActivityCauseCurrency] AS [Currency_Code] 
, p.[ActivityDate] AS [Transaction_Date] 
, p.CurrencyExchangeDate AS [Reward_Posting_Date]	
, h.MainBrandCode AS [Hotel_Brand] 
, h.HotelName AS [Hotel_Name]	
, [PointLiabilityUSD] AS [Value_of_Redemption_USD]	
, Reservations.[dbo].[convertCurrencyToUSD](ActivityCauseAmount, p.[ActivityCauseCurrency], [ActivityCauseDate]) AS [Amount_Spent_USD]	
, NULL AS IP_TDR_ID  
  FROM [Loyalty].[dbo].[PointActivity] p
  LEFT JOIN loyalty.dbo.ActivityCauseSystem acs ON acs.ActivityCauseSystemID = p.ActivityCauseSystemID
  INNER JOIN loyalty.[dbo].[LoyaltyNumber] l ON p.loyaltyNumberID = l.loyaltyNumberID
  LEFT JOIN loyalty.[dbo].[ActivityType] actT ON actT.ActivityTypeID = p.ActivityTypeID
  LEFT JOIN Reservations.dbo.MostRecentTransactions t ON t.confirmationNumber = p.TransactionNumber
  LEFT JOIN Reservations.dbo.Transactions tr ON tr.TransactionID = t.TransactionID
  LEFT JOIN Reservations.dbo.PH_BookingSource b ON tr.PH_BookingSourceID = b.PH_BookingSourceID
  LEFT JOIN Reservations.dbo.PH_Channel pc ON pc.PH_ChannelID = b.PH_ChannelID
  LEFT JOIN Hotels.dbo.hotelsReporting h ON h.code = t.HotelCode
  LEFT JOIN Guests.dbo.guest g ON g.LoyaltyNumberID = l.LoyaltyNumberID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
