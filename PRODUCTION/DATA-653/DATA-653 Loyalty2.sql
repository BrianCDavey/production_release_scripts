USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/3/2020 2:18:10 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransactionDetailedReport]'
GO







ALTER VIEW [dbo].[TransactionDetailedReport]
AS

SELECT 
p.QueueID AS [QueueID]	
, p.PointActivityID AS [Transaction_Id]
, acs.ActivityCauseSystemName AS [Transaction_Source]
, p.[Notes] AS [Remarks]	
, l.loyaltyNumberName AS [iPrefer_Number]
, CASE WHEN g.GuestID IS NULL THEN 'D' ELSE 'E' END AS [Member_Status] 
, p.TransactionNumber AS [Booking_ID]	 
, ISNULL(t.HotelCode,hh.HotelCode) AS [Hotel_Code]	
, ISNULL(t.arrivalDate,p.[ActivityCauseDate]) AS [Arrival_Date]	
, t.departureDate AS [Departure_Date]	
, pc.PH_Channel AS [Booking_Source]	
, actT.[ActivityTypeName] AS [Campaign] 
, CASE WHEN p.Points > 0 THEN p.Points ELSE 0 END AS [Points_Earned] 
, CASE WHEN p.Points < 0 THEN p.Points * -1 ELSE 0 END AS [Points_Redemeed]
, CASE WHEN ISNULL(NULLIF(p.[ActivityCauseCurrency],''),'USD') = 'USD'  THEN CAST(p.Points AS decimal(20,5))/10 ELSE CurrencyRates.dbo.convertCurrency( CAST(p.Points AS decimal(20,5))/10,'USD',p.[ActivityCauseCurrency], CASE WHEN p.CurrencyExchangeDate = '2001-01-01 00:00:00.000' OR p.CurrencyExchangeDate IS NULL THEN p.ActivityDate ELSE p.CurrencyExchangeDate END) END AS [Reservation_Revenue] 
, COALESCE(NULLIF(p.[ActivityCauseCurrency],''),'USD') AS [Currency_Code] 
, p.[ActivityDate] AS [Transaction_Date] 
, CASE WHEN p.CurrencyExchangeDate = '2001-01-01 00:00:00.000' OR p.CurrencyExchangeDate IS NULL THEN p.ActivityDate ELSE p.CurrencyExchangeDate END AS [Reward_Posting_Date]	
, ISNULL(h.MainBrandCode,hr.MainBrandCode) AS [Hotel_Brand] 
, ISNULL(h.HotelName,hh.HotelName) AS [Hotel_Name]	
, 0.00 AS [Value_of_Redemption_USD]	
, CAST(p.Points AS decimal(20,5))/10 AS [Amount_Spent_USD]	
, NULL AS IP_TDR_ID  
  FROM [Loyalty].[dbo].[PointActivity] p
  LEFT JOIN loyalty.dbo.ActivityCauseSystem acs ON acs.ActivityCauseSystemID = p.ActivityCauseSystemID
  INNER JOIN loyalty.[dbo].[LoyaltyNumber] l ON p.loyaltyNumberID = l.loyaltyNumberID
  LEFT JOIN loyalty.[dbo].[ActivityType] actT ON actT.ActivityTypeID = p.ActivityTypeID
  LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelID = p.ActivityCauseHotelID
  LEFT JOIN Hotels.dbo.hotelsReporting hr ON hr.code = hh.HotelCode
  LEFT JOIN Reservations.dbo.MostRecentTransactions t ON t.confirmationNumber = p.TransactionNumber
  LEFT JOIN Reservations.dbo.Transactions tr ON tr.TransactionID = t.TransactionID
  LEFT JOIN Reservations.dbo.PH_BookingSource b ON tr.PH_BookingSourceID = b.PH_BookingSourceID
  LEFT JOIN Reservations.dbo.PH_Channel pc ON pc.PH_ChannelID = b.PH_ChannelID
  LEFT JOIN Hotels.dbo.hotelsReporting h ON h.code = t.HotelCode
  LEFT JOIN Guests.dbo.guest g ON g.LoyaltyNumberID = l.LoyaltyNumberID
  WHERE  p.PointTypeID IN (1,3) -- BSI Point Credit AND Epsilon Point Credits

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Account_Statement_Hotel_Redemption_Import]'
GO


CREATE VIEW [dbo].[Account_Statement_Hotel_Redemption_Import]
AS

SELECT 
		ra.CreateDate AS [Redemption_Date]
      ,h.HotelCode AS [Hotel_Code]
      ,h.HotelName AS [Hotel_Name]
      ,l.LoyaltyNumberName AS [Membership_Number]
      ,g.FirstName + ', ' + LastName AS [Member_Name]
      ,rt.RewardTypeName AS [Voucher_Type]
      ,ISNULL(ra.MemberRewardNumber,r.RewardName) AS [Voucher_Number]
      ,ra.RewardCurrency + CAST(ra.RewardCost AS nvarchar(20)) + ' ' + rt.RewardTypeName AS [Voucher_Name]
      ,ra.RewardCost AS [Voucher_Value]
      ,ra.RewardCurrency AS [Voucher_Currency]
      ,ISNULL(hr.ReimbursementPercentage,0.85) * RewardCost AS [Payable_Value_USD]
	  --,0.85 * RewardCost AS [Payable_Value_USD] --hardcode for tesing since
      ,q.FilePath AS [filename]
      ,q.ImportFinished AS [importdate]
      ,ra.RedemptionActivityID AS [id]
      ,CAST(ra.CreateDate AS DATE) AS [Currency_Conversion_Date]
FROM dbo.RedemptionActivity ra
LEFT JOIN dbo.Reward r ON ra.RewardID = r.RewardID
LEFT JOIN Hotels.dbo.Hotel h ON ra.RedeemedHotelID = HotelID
LEFT JOIN dbo.LoyaltyNumber l ON ra.LoyaltyNumberID = l.LoyaltyNumberID
LEFT JOIN Guests.dbo.Guest g ON ra.LoyaltyNumberID = g.LoyaltyNumberID
LEFT JOIN dbo.RewardType rt ON ra.RewardTypeID = rt.RewardTypeID
LEFT JOIN ETL.dbo.Queue q ON ra.QueueID = q.QueueID
LEFT JOIN ReservationBilling.loyalty.HotelRedemptionRules hr ON hr.HotelID = ra.RedeemedHotelID AND ra.CreateDate BETWEEN hr.StartDate AND hr.EndDate

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
