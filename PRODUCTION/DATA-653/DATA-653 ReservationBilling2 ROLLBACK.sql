USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/3/2020 2:19:52 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[ManualPoint_Reservation]'
GO



ALTER VIEW [work].[ManualPoint_Reservation]
AS
	
     SELECT 
      tdrJ.loyaltyNumber + tdrJ.phgHotelCode + CAST(YEAR(tdrJ.confirmationdate) AS varchar(4)) + '-' +  CAST(MONTH(tdrJ.confirmationdate) AS varchar(2))  AS confirmationNumber
	  ,tdrJ.loyaltyNumber
      ,tdrJ.phgHotelCode
	  ,MONTH(tdrJ.confirmationdate) AS MONtransactionTimeStamp
	  ,YEAR(tdrJ.confirmationdate) AS YEARtransactionTimeStamp
	  ,SUM(tdrJ.roomRevenueInBookingCurrency* 1.00/tdrJ.bookingCurrencyExchangeRate ) AS roomRevenueInHotelCurrency
	  , MIN(tdrJ.confirmationdate) AS confirmationdate
	  , MIN(tdrJ.transactionKey) AS transactionKey
     FROM work.tdrJoinedManual_Reservation tdrJ
        LEFT JOIN Reservations.dbo.transactions mrt
		ON mrt.confirmationNumber = tdrJ.confirmationNumber

  WHERE ( tdrJ.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') AND mrt.confirmationNumber IS NULL)
	GROUP BY tdrJ.loyaltyNumber, tdrJ.phgHotelCode
		, MONTH(tdrJ.confirmationdate), YEAR(tdrJ.confirmationdate)
	HAVING SUM(tdrJ.roomRevenueInBookingCurrency* 1.00/tdrJ.bookingCurrencyExchangeRate ) > 0.1

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Populate_MrtForCalculation_ByDate_Reservation]'
GO











ALTER PROCEDURE [work].[Populate_MrtForCalculation_ByDate_Reservation]
	@startDate date = NULL,
	@endDate date = NULL,
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	SET NOCOUNT ON;

	;WITH cte_Invoiced
	AS
	(
		SELECT DISTINCT transactionSourceID,transactionKey,sopNumber, confirmationNumber -- Adding for manual point award without reservation 
		FROM dbo.Charges
		WHERE sopNumber IS NOT NULL
	)
	INSERT INTO [work].[MrtForCalculation](runID, confirmationNumber, phgHotelCode, crsHotelID, hotelName, mainBrandCode, gpSiteID, chainID, chainName, bookingStatus, synxisBillingDescription, bookingChannel, bookingSecondarySource, bookingSubSourceCode, bookingTemplateGroupID, bookingTemplateAbbreviation, xbeTemplateName, CROcode, bookingCroGroupID, bookingRateCategoryCode, bookingRateCode, bookingIATA, transactionTimestamp, confirmationDate, arrivalDate, departureDate, cancellationDate, cancellationNumber, nights, rooms, roomNights, roomRevenueInBookingCurrency, bookingCurrencyCode, timeLoaded, CRSSourceID, ItemCode, exchangeDate, hotelCurrencyCode, hotelCurrencyDecimalPlaces, hotelCurrencyExchangeRate, bookingCurrencyExchangeRate, loyaltyProgram, loyaltyNumber, travelAgencyName, LoyaltyNumberValidated, LoyaltyNumberTagged, billableDate, transactionSourceID, transactionKey)
	SELECT @RunID,mrtj.[confirmationNumber],mrtj.[phgHotelCode],mrtj.[crsHotelID],mrtj.[hotelName],mrtj.[mainBrandCode],
			mrtj.[gpSiteID],mrtj.[chainID],mrtj.[chainName],mrtj.[bookingStatus],mrtj.[synxisBillingDescription],
			mrtj.[bookingChannel],mrtj.[bookingSecondarySource],mrtj.[bookingSubSourceCode],mrtj.[bookingTemplateGroupID],
			mrtj.[bookingTemplateAbbreviation],mrtj.[xbeTemplateName],mrtj.[CROcode],mrtj.[bookingCroGroupID],
			mrtj.[bookingRateCategoryCode],mrtj.[bookingRateCode],mrtj.[bookingIATA],mrtj.[transactionTimeStamp],
			mrtj.[confirmationDate],mrtj.[arrivalDate],mrtj.[departureDate],mrtj.[cancellationDate],mrtj.[cancellationNumber],
			mrtj.[nights],mrtj.[rooms],mrtj.[roomNights],mrtj.[roomRevenueInBookingCurrency],mrtj.[bookingCurrencyCode],
			mrtj.[timeLoaded],mrtj.[CRSSourceID],mrtj.[ItemCode],mrtj.exchangeDate,mrtj.hotelCurrencyCode,
			mrtj.hotelCurrencyDecimalPlaces,mrtj.hotelCurrencyExchangeRate,mrtj.bookingCurrencyExchangeRate,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],mrtj.LoyaltyNumberValidated,
			mrtj.LoyaltyNumberTagged,mrtj.arrivalDate,mrtj.CRSSourceID,mrtj.transactionKey
	FROM work.mrtJoined_Reservation mrtj
		LEFT JOIN cte_Invoiced inv ON inv.transactionKey = mrtj.transactionKey AND inv.transactionSourceID =  mrtj.transactionSourceID
	WHERE mrtj.arrivalDate BETWEEN @startDate AND @endDate
		AND inv.transactionKey IS NULL
	
	UNION ALL

	--Manual Point award with Superset Reservation information	
	SELECT @RunID,tdr.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],
			tdr.[gpSiteID],tdr.[chainID],tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],
			tdr.[bookingChannel],tdr.[bookingSecondarySource],tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],
			tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],tdr.[bookingCroGroupID],
			tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],tdr.[transactionTimeStamp],
			tdr.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],
			tdr.[nights],tdr.[rooms],tdr.[roomNights],tdr.[roomRevenueInBookingCurrency],tdr.[bookingCurrencyCode],
			tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,tdr.hotelCurrencyCode,
			tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,
			tdr.loyaltyProgram,tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,
			tdr.transactionTimestamp,tdr.transactionSourceID,tdr.transactionKey
	FROM work.tdrJoined_Reservation tdr
		LEFT JOIN cte_Invoiced inv ON inv.transactionKey = tdr.transactionKey AND inv.transactionSourceID =  tdr.transactionSourceID
  WHERE tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
	AND inv.transactionKey IS NULL --not previously invoiced

	UNION ALL

	--Manual Point award without Superset Reservation information	
				
	SELECT DISTINCT @RunID,MP.[confirmationNumber],tdr.[phgHotelCode],tdr.[crsHotelID],tdr.[hotelName],tdr.[mainBrandCode],
			tdr.[gpSiteID],tdr.[chainID],tdr.[chainName],tdr.[bookingStatus],tdr.[synxisBillingDescription],
			tdr.[bookingChannel],tdr.[bookingSecondarySource],tdr.[bookingSubSourceCode],tdr.[bookingTemplateGroupID],
			tdr.[bookingTemplateAbbreviation],tdr.[xbeTemplateName],tdr.[CROcode],tdr.[bookingCroGroupID],
			tdr.[bookingRateCategoryCode],tdr.[bookingRateCode],tdr.[bookingIATA],MP.[confirmationDate] AS [transactionTimeStamp],
			mp.[confirmationDate],tdr.[arrivalDate],tdr.[departureDate],tdr.[cancellationDate],tdr.[cancellationNumber],
			tdr.[nights],tdr.[rooms],tdr.[roomNights],mp.roomRevenueInHotelCurrency AS [roomRevenueInBookingCurrency],tdr.hotelCurrencyCode AS [bookingCurrencyCode],
			tdr.[timeLoaded],tdr.[CRSSourceID],tdr.[ItemCode],tdr.exchangeDate,tdr.hotelCurrencyCode,
			tdr.hotelCurrencyDecimalPlaces,tdr.hotelCurrencyExchangeRate,tdr.bookingCurrencyExchangeRate,
			tdr.loyaltyProgram,tdr.loyaltyNumber,tdr.[travelAgencyName],tdr.LoyaltyNumberValidated,tdr.LoyaltyNumberTagged,
			tdr.transactionTimestamp,tdr.transactionSourceID,mp.transactionKey
	FROM work.tdrJoinedMaual_Reservation tdr
		INNER JOIN work.ManualPoint_Reservation MP ON tdr.phgHotelCode = MP.phgHotelCode 
			AND tdr.loyaltyNumber = MP.loyaltyNumber AND MP.MONtransactionTimeStamp = MONTH(tdr.confirmationdate)
			AND MP.YEARtransactionTimeStamp = YEAR(tdr.confirmationdate)
		LEFT JOIN cte_Invoiced inv ON inv.transactionKey = tdr.transactionKey AND inv.transactionSourceID =  tdr.transactionSourceID AND mp.confirmationNumber = inv.confirmationNumber
  WHERE tdr.transactionTimestamp BETWEEN @startDate AND @endDate --I Prefer manual transactions are billed by reward date, not arrival
	AND inv.transactionKey IS NULL --not previously invoiced

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
