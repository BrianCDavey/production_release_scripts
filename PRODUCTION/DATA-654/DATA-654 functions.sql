/*
Run this script on:

        CHISQP01.functions    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\LINKED_DB.functions

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/22/2020 11:03:34 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[preCreditMemoIntegration]'
GO

ALTER PROCEDURE [dbo].[preCreditMemoIntegration] 
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,@startdate)) --the end of the start month

--get the hotel production totals for invoice header information
DECLARE @hotelPreferences as table
	(	hotelCode nvarchar(10), currencyCode char(3), DECPLCUR int --, PYMTRMID char(21), DSCPCTAM int
	)

INSERT	INTO @hotelPreferences
SELECT	HS.hotelCode,
		customerMaster.CURNCYID,
		currencyMaster.DECPLCUR

FROM	ReservationBilling.loyalty.Credit_Memos CM

JOIN	Core.dbo.hotels_Synxis HS
	ON	CM.hotel_code = HS.hotelcode

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	HS.hotelCode = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC40200 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID

WHERE	CM.Create_Date >= @startDate
AND		CM.Redemption_Date <= @enddate
AND		ISNULL(CM.Payable_Value, 0) <> 0.00
		
GROUP BY HS.hotelCode, customerMaster.CURNCYID, currencyMaster.DECPLCUR --,customerMaster.PYMTRMID --,paymentTerms.DSCPCTAM
ORDER BY HS.hotelCode


--wipe out the single use table for integration
truncate table dbo.CreditMemoIntegration

--fill the single use reservation integration table with the details of this particular integration
INSERT INTO dbo.CreditMemoIntegration
	(	[CustomerCode] ,[BrandCode] ,[InvoiceDate] ,[ItemCode] ,[CurrencyCode] ,[gpSiteId] ,
		[AccountNumber] , [EntryType], [Source] , [Quantity] , TotalValue, BatchID , [poNum]
	)
select	CM.hotel_code , -- [customerCode]
		H.mainbrandcode , --[brandCode]
		@invoiceDate , --[invoiceDate]
		'IPREFER_CM' , --[itemCode]  ** hard coded for the time being, but could change in the future
		HP.currencyCode , --[currencyCode]
		B.[gpSiteId] , -- from Brands table, based on hotel main brand code ??
		'GL.00100 AcctNumbers' ,
		'EntryType ',
		'Source' , 
		COUNT(CM.Voucher_Number) , -- [quantity]
		CAST(SUM(ROUND(CM.Payable_Value * CASE HP.currencyCode WHEN 'USD' THEN 1.00 ELSE CurrencyMaster.XCHGRATE END, HP.DECPLCUR-1)) as DECIMAL(12,2)) , --as CreditAmount_HotelCurrency ,
		'IPREFERCM' + CAST(YEAR(@invoiceDate) as nvarchar(4)) + RIGHT(('0' + CAST(MONTH(@invoiceDate) as nvarchar(2))), 2),
		'IPREFERREDEMPTION'  -- ,[poNum]

from	@hotelPreferences HP
join	ReservationBilling.dbo.[Credit_Memos] CM 
	on	HP.hotelcode = CM.Hotel_Code

join	Core.dbo.HotelsReporting H with (nolock)
	on	CM.hotel_code = H.code

join	Core.dbo.Brands B with (nolock)
	on	H.mainbrandcode = B.code

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	H.Code = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC00100 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID
	AND	CM.Currency_Conversion_Date between currencyMaster.EXCHDATE AND currencyMaster.EXPNDATE
	AND	EXGTBLID like '%AVG%'

WHERE	CM.Create_Date >= @startDate
AND		CM.Redemption_Date <= @enddate
AND		CM.sopNumber is null

GROUP BY CM.hotel_code, H.mainbrandcode , B.[gpSiteId], HP.DECPLCUR, HP.currencyCode

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[preJournalEntryIntegration]'
GO

-- =============================================
-- Author:		Charles Wahl
-- Create date: 2013-12-04
-- Description: Charles Z wanted me to get the Journal Entry integration up and running.  This is a temporary solution until Tory finishes his part.
--				Will need to possibly update the mapping once preJournalEntryIntegration is done.
-- =============================================
ALTER PROCEDURE [dbo].[preJournalEntryIntegration]
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,@startdate)) --the end of the start month
	
    DECLARE @hotelPreferences as table
	(	hotelCode nvarchar(10), currencyCode char(3), DECPLCUR int
	)
INSERT	INTO @hotelPreferences
SELECT	HS.hotelCode,
		customerMaster.CURNCYID,
		currencyMaster.DECPLCUR
		
FROM	ReservationBilling.loyalty.Credit_Memos CM

JOIN	Core.dbo.hotels_Synxis HS
	ON	CM.hotel_code = HS.hotelcode

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	HS.hotelCode = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC40200 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID

WHERE	CM.Create_Date >= @startDate
AND		CM.Redemption_Date <= @enddate
AND		ISNULL(CM.Payable_Value, 0) <> 0.00	
		
GROUP BY HS.hotelCode, customerMaster.CURNCYID, currencyMaster.DECPLCUR --,customerMaster.PYMTRMID --,paymentTerms.DSCPCTAM
ORDER BY HS.hotelCode


--wipe out the single use table for reservation integration
truncate table dbo.JournalEntryIntegration


/*

select * from dbo.JournalEntryAccountNumbers


--01-000-0000-23030
select	ltrim(rtrim(ACTNUMBR_1)) + '-' + ltrim(rtrim(ACTNUMBR_2)) + '-' + ltrim(rtrim(ACTNUMBR_3)) + '-' + ltrim(rtrim(ACTNUMBR_4)) 'acctnumber', ACTDESCR
from ic.dbo.GL00100
where	ltrim(rtrim(ACTNUMBR_1)) + '-' + ltrim(rtrim(ACTNUMBR_2)) + '-' + ltrim(rtrim(ACTNUMBR_3)) + '-' + ltrim(rtrim(ACTNUMBR_4)) in (
'01-100-3250-60030',
'01-000-0000-23030',
'01-000-0000-23030',
'01-100-3250-30032',
'01-000-0000-23030',
'01-000-0000-23035',
'01-000-0000-23035',
'01-000-0000-23035',
'01-000-0000-11000',
'01-100-3250-30031')

*/




--fill the single use reservation integration table with the details of this particular integration
INSERT INTO dbo.JournalEntryIntegration
	(	[InvoiceDate] ,[ItemCode] ,[CurrencyCode] ,
		[AccountNumber] , [TransactionType], [EntryType], DistributionReference, SequenceNumber,
		[Source], DebitAmount, CreditAmount, [batchID] , headerref, AccountID
	)
select	@endDate, --[invoiceDate] ***transactionDate***
		'Credit Memo' , --[itemCode] ***dont' need*** ** hard coded for the time being, but could change in the future
		HP.currencyCode, --[currencyCode]
		JEAN.accountNumber ,
		0, --  transactionType -- 0 = Standard
		JEAN.entryType,
		JEAN.distributionReference,
		ROW_NUMBER() OVER (ORDER BY JEAN.accountNumber), -- SequenceNumber
		'Redeemed' , -- Source
		
		--Voucher Value is in Voucher Currency
		--Payable Value is in USD
		--Entry needs to be in Hotel Contracted Currency
		
		CASE JEAN.entryType
			WHEN 'Debit'
				THEN ROUND(((CM.Voucher_Value / voucherCurrExch.XCHGRATE)  - CM.Payable_Value) * hotelCurrExch.XCHGRATE,HP.DECPLCUR-1)
			ELSE 0
		END as DebitAmount ,
		CASE JEAN.entryType
			WHEN 'Credit'
				THEN ROUND(((CM.Voucher_Value / voucherCurrExch.XCHGRATE) - CM.Payable_Value) * hotelCurrExch.XCHGRATE,HP.DECPLCUR-1)
			ELSE 0
		END as CreditAmount ,
		'IPREFERCM' + CAST(YEAR(@invoiceDate) as nvarchar(4)) + RIGHT(('0' + CAST(MONTH(@invoiceDate) as nvarchar(2))), 2),
		'IPREFERCM', -- headerRef
		JEAN.id

-- select * 
from	@hotelPreferences HP
join ReservationBilling.loyalty.Credit_Memos as CM
	on	HP.hotelcode = CM.Hotel_Code
LEFT JOIN dbo.JournalEntryAccountNumbers JEAN
	ON JEAN.source = 'Redeemed'
join	Core.dbo.HotelsReporting H with (nolock)
	on	CM.hotel_code = H.code

join	Core.dbo.Brands B with (nolock)
	on	H.mainbrandcode = B.code

JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	H.Code = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC00100 AS hotelCurrExch
	ON	customerMaster.CURNCYID = hotelCurrExch.CURNCYID
	AND	CM.Redemption_Date between hotelCurrExch.EXCHDATE AND hotelCurrExch.EXPNDATE
	AND	hotelCurrExch.EXGTBLID like '%AVG%'
	
JOIN	DYNAMICS.dbo.MC00100 AS voucherCurrExch
	ON	CM.Voucher_Currency = voucherCurrExch.CURNCYID
	AND	CM.Redemption_Date between voucherCurrExch.EXCHDATE AND voucherCurrExch.EXPNDATE
	AND	voucherCurrExch.EXGTBLID like '%AVG%'	

WHERE	CM.Create_Date >= @startDate
AND		CM.Redemption_Date <= @enddate
AND		CM.sopNumber is NOT null 
AND CM.jeNumber IS NULL

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[preResIntegration]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-03-19
-- Description:	Load table functions.dbo.reservationIntegration from job RES for reservationbilling
-- =============================================

ALTER PROCEDURE [dbo].[preResIntegration] 
	-- Add the parameters for the stored procedure here
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--these declare/sets are just a short cut for simulating parameter values for troubleshooting the statements.
--DECLARE @invoiceDate as Date, @startDate as Date, @endDate as Date
--SET @invoiceDate = CAST(CONVERT(char(2),MONTH(GETDATE())) + '/10/' + CONVERT(char(4),YEAR(GETDATE())) as datetime)
--SET @startDate = DATEADD(MONTH,-1,CAST(CONVERT(char(2),MONTH(GETDATE())) + '/01/' + CONVERT(char(4),YEAR(GETDATE())) as datetime))
--SET @endDate = DATEADD(DAY,-1,CAST(CONVERT(char(2),MONTH(GETDATE())) + '/01/' + CONVERT(char(4),YEAR(GETDATE())) as datetime))


select	* 
into	#CD
from	ReservationBilling.dbo.Charges
where	billableDate between @startDate and @endDate
AND (classificationID NOT IN (4,5) --remove non billable charge because of NULL itemcode
	OR  (classificationID = 5 AND ChargeValueInUSD <> 0.00))

create index ix_CD_ConfirmNum on #CD (confirmationnumber)
		

--get the hotel production totals for invoice header information
DECLARE	@hotelTotals as table
	(	hotelCode nvarchar(10), bookings int, roomNights int, revenue decimal(18,2), currencyCode char(3), 
		DECPLCUR int, PYMTRMID char(21), DSCPCTAM int, mainBrand nvarchar(10)
	)

INSERT	INTO @hotelTotals
SELECT	hr.code,
		COUNT(*) as bookings, 
		SUM(billy.roomNights) as roomNights, 
		CAST(ROUND(SUM(billy.roomRevenueInHotelCurrency),MAX(DECPLCUR)-1) as Decimal(10,2)) as revenue,
		customerMaster.CURNCYID,
		currencyMaster.DECPLCUR,
		customerMaster.PYMTRMID,
		paymentTerms.DSCPCTAM,
		hr.mainBrandCode
FROM	(	select	distinct hotelCode, confirmationNumber, billableDate, roomRevenueInHotelCurrency, roomNights
		from	#CD
		WHERE	billableDate between @startDate and @endDate 
		AND itemCode <> 'PMS_I' AND itemcode <> 'IPREFERMANUAL_I' AND itemcode <> 'PMS_B' AND itemcode <> 'IPREFERMANUAL_B' AND classificationid <> 5) as billy 

JOIN	Core.dbo.hotelsReporting hr 
	ON	billy.hotelCode = hr.code
		
JOIN	IC.dbo.RM00101 AS customerMaster 
	ON	hr.code = customerMaster.CUSTNMBR

JOIN	DYNAMICS.dbo.MC40200 AS currencyMaster 
	ON	customerMaster.CURNCYID = currencyMaster.CURNCYID
		
LEFT OUTER JOIN IC.dbo.SY03300 AS paymentTerms ON customerMaster.PYMTRMID = paymentTerms.PYMTRMID
		
WHERE	billy.billableDate between @startDate and @endDate


GROUP BY hr.code, customerMaster.CURNCYID, currencyMaster.DECPLCUR, customerMaster.PYMTRMID, paymentTerms.DSCPCTAM, hr.mainBrandCode
ORDER BY hr.code

--wipe out the single use table for reservation integration
DELETE FROM functions.dbo.reservationIntegration

--fill the single use reservation integration table with the details of this particular integration
INSERT INTO functions.dbo.reservationIntegration
	(	[customerCode] ,[brandCode] ,[invoiceDate] ,[itemCode] ,[currencyCode] ,[gpSiteId] ,[totalRoomNights] ,
		[totalRevenue] ,[totalBookings] ,[quantity] ,[pricePer] ,[totalPrice] ,[autoCreditAmount] ,[batchID],[poNum]
	)
SELECT	CD.[hotelCode]
		,CD.collectionCode
		,@invoiceDate as invoiceDate
		,CD.[itemCode]
		,[hotelCurrencyCode]
		,CD.gpSiteID        
		,MAX(totals.[roomNights]) as totalRoomNights
		,MAX(totals.[revenue]) as totalRevenue
		,MAX(totals.bookings) as totalBookings
		,COUNT(DISTINCT CD.confirmationNumber) as quantity
		,CASE (totals.DECPLCUR-1)
				WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) as DECIMAL(12,2))) as DECIMAL(12,2))
				ELSE CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) as DECIMAL(12,2)) 
		END as pricePer
		,CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2)) as totalPrice
		,ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2)) * 
			((CASE itemCodeMaster.USCATVLS_1
				WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM
				ELSE 0
				END as decimal(8,3))
			ELSE 0
		END)/10000),totals.DECPLCUR-1) as autoCreditAmount
		, ('RES ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + ' STD') as batchID
		,('ARRIVALS ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2)) as poNum

FROM	#CD CD

LEFT OUTER JOIN IC.dbo.IV00101 AS itemCodeMaster 
	ON	CD.itemCode = itemCodeMaster.ITEMNMBR

LEFT OUTER JOIN @hotelTotals as totals 
	on	CD.hotelCode = totals.hotelCode
	
WHERE	CD.billableDate between @startDate and @endDate
AND totals.mainBrand NOT IN ('HE','HW')
AND		CD.sopNumber is null -- new charges only
AND CD.classificationID <> 5 --remove iprefer charge
GROUP BY CD.[hotelCode], CD.collectionCode, CD.gpSiteID , CD.[itemCode] ,[hotelCurrencyCode] ,
		itemCodeMaster.USCATVLS_1 ,totals.PYMTRMID ,totals.DSCPCTAM ,totals.DECPLCUR
ORDER BY CD.[hotelCode], itemCode


--fill the single use reservation integration table with the details of this particular integration
INSERT INTO functions.dbo.reservationIntegration
	(	[customerCode] ,[brandCode] ,[invoiceDate] ,[itemCode] ,[currencyCode] ,[gpSiteId] ,[totalRoomNights] ,
		[totalRevenue] ,[totalBookings] ,[quantity] ,[pricePer] ,[totalPrice] ,[autoCreditAmount] ,[batchID],[poNum]
	)
SELECT	CD.[hotelCode]
		,CD.collectionCode
		,@invoiceDate as invoiceDate
		,CD.[itemCode]
		,[hotelCurrencyCode]
		,CD.gpSiteID        
		,MAX(totals.[roomNights]) as totalRoomNights
		,MAX(totals.[revenue]) as totalRevenue
		,MAX(totals.bookings) as totalBookings
		,COUNT(DISTINCT CD.confirmationNumber) as quantity
		,CASE (totals.DECPLCUR-1)
				WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) as DECIMAL(12,2))) as DECIMAL(12,2))
				ELSE CAST(ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2))/COUNT(DISTINCT [confirmationNumber]),totals.DECPLCUR-1) as DECIMAL(12,2)) 
		END as pricePer
		,CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2)) as totalPrice
		,ROUND(CAST(ROUND(SUM([chargeValueInHotelCurrency]),totals.DECPLCUR-1) as DECIMAL(12,2)) * 
			((CASE itemCodeMaster.USCATVLS_1
				WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM
				ELSE 0
				END as decimal(8,3))
			ELSE 0
		END)/10000),totals.DECPLCUR-1) as autoCreditAmount
		, ('RES' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + 'HESTD') as batchID
		,('ARRIVALS ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2)) as poNum

FROM	#CD CD

LEFT OUTER JOIN IC.dbo.IV00101 AS itemCodeMaster 
	ON	CD.itemCode = itemCodeMaster.ITEMNMBR

LEFT OUTER JOIN @hotelTotals as totals 
	on	CD.hotelCode = totals.hotelCode
	
WHERE	CD.billableDate between @startDate and @endDate
AND totals.mainBrand IN ('HE','HW')
AND		CD.sopNumber is null -- new charges only
AND CD.classificationID <> 5 --remove iprefer charge
GROUP BY CD.[hotelCode], CD.collectionCode, CD.gpSiteID , CD.[itemCode] ,[hotelCurrencyCode] ,
		itemCodeMaster.USCATVLS_1 ,totals.PYMTRMID ,totals.DSCPCTAM ,totals.DECPLCUR
ORDER BY CD.[hotelCode], itemCode


-- get iprefer charge data

SELECT [hotelCode], collectionCode, [hotelCurrencyCode], gpSiteID, COUNT(confirmationNumber) AS Qty, SUM([chargeValueInHotelCurrency]) AS Billable_Amount_Hotel_Currency, billableDate AS billing_Date, itemcode 
into	#IPC_TEMP
FROM #CD
WHERE classificationID = 5 AND ChargeValueInUSD <> 0.00 AND sopNumber is null
GROUP BY [hotelCode], collectionCode, [hotelCurrencyCode], gpSiteID,itemcode,billableDate

SELECT hotel_code, mainbrandcode, Hotel_Currency, gpSiteId, SUM(Qty) AS Qty, SUM(Billable_Amount_Hotel_Currency) AS Billable_Amount_Hotel_Currency, MAX(billing_Date) AS billing_Date, itemcode
INTO #IPC
FROM #IPC_TEMP
GROUP BY hotel_code, mainbrandcode, Hotel_Currency, gpSiteId, itemcode


--  now new insert the new iPrefer line item 
insert into functions.dbo.reservationIntegration
	(	[customerCode],[brandCode],[invoiceDate],[itemCode],[currencyCode],[gpSiteId],[totalRoomNights],
		[totalRevenue] ,[totalBookings],[quantity],[pricePer],[totalPrice],[autoCreditAmount],[batchID],[poNum]
	)
select	IPC.hotel_code , -- [customerCode]
		IPC.mainbrandcode , --[brandCode]
		@invoiceDate , --[invoiceDate]
		IPC.itemcode,
		IPC.Hotel_Currency , --[currencyCode]
		IPC.gpSiteId , 
		0 as totalRoomNights , 
		0 as totalRevenue , 
		0 as totalBookings, 
		IPC.Qty,
		CASE (totals.DECPLCUR-1)
				WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency), IsNull(totals.DECPLCUR-1, 2) ) as DECIMAL(12,2))/ [IPC].[Qty], IsNull(totals.DECPLCUR-1, 2)) as DECIMAL(12,2))) as DECIMAL(12,2))
				ELSE CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency), IsNull(totals.DECPLCUR-1, 2)) as DECIMAL(12,2))/ [IPC].[Qty] , IsNull(totals.DECPLCUR-1, 2) ) as DECIMAL(12,2)) 
		END as pricePer , --  [pricePer]
		CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency), Isnull(totals.DECPLCUR-1, 2) ) as DECIMAL(12,2)) as totalPrice
		--,ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),totals.DECPLCUR-1) as DECIMAL(12,2)) * 
		--((CASE itemCodeMaster.USCATVLS_1
		--	WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM ELSE 0 END as decimal(8,3))
		--	ELSE 0
		--END)/10000),totals.DECPLCUR-1) as autoCreditAmount -- autoCreditAmount
		, 0.00 as autoCreditAmount
		, ('RES ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + ' IPR') as batchID
		, ('ARRIVALS ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2)) as poNum
from	#IPC IPC

LEFT OUTER JOIN @hotelTotals as totals 
	on	IPC.hotel_Code = totals.hotelCode

WHERE	IPC.billing_Date between @startDate and @endDate
AND (totals.mainBrand NOT IN ('HE','HW') OR totals.mainBrand IS NULL)
--AND		IPC.sopNumber is null

GROUP BY IPC.hotel_code, IPC.mainbrandcode, IPC.Hotel_Currency, IPC.Qty,
		totals.PYMTRMID ,totals.DSCPCTAM ,totals.DECPLCUR, IPC.gpSiteId,IPC.itemcode
ORDER BY IPC.hotel_code 


--  now new insert the new iPrefer line item 
insert into functions.dbo.reservationIntegration
	(	[customerCode],[brandCode],[invoiceDate],[itemCode],[currencyCode],[gpSiteId],[totalRoomNights],
		[totalRevenue] ,[totalBookings],[quantity],[pricePer],[totalPrice],[autoCreditAmount],[batchID],[poNum]
	)
select	IPC.hotel_code , -- [customerCode]
		IPC.mainbrandcode , --[brandCode]
		@invoiceDate , --[invoiceDate]
		IPC.itemcode,
		IPC.Hotel_Currency , --[currencyCode]
		IPC.gpSiteId , 
		0 as totalRoomNights , 
		0 as totalRevenue , 
		0 as totalBookings, 
		IPC.Qty,
		CASE (totals.DECPLCUR-1)
				WHEN 0 THEN CAST(FLOOR(CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency), IsNull(totals.DECPLCUR-1, 2) ) as DECIMAL(12,2))/ [IPC].[Qty], IsNull(totals.DECPLCUR-1, 2)) as DECIMAL(12,2))) as DECIMAL(12,2))
				ELSE CAST(ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency), IsNull(totals.DECPLCUR-1, 2)) as DECIMAL(12,2))/ [IPC].[Qty] , IsNull(totals.DECPLCUR-1, 2) ) as DECIMAL(12,2)) 
		END as pricePer , --  [pricePer]
		CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency), Isnull(totals.DECPLCUR-1, 2) ) as DECIMAL(12,2)) as totalPrice
		--,ROUND(CAST(ROUND(SUM(IPC.Billable_Amount_Hotel_Currency),totals.DECPLCUR-1) as DECIMAL(12,2)) * 
		--((CASE itemCodeMaster.USCATVLS_1
		--	WHEN 'COMM' THEN CAST(CASE WHEN RTRIM(totals.PYMTRMID) like '%AC' THEN totals.DSCPCTAM ELSE 0 END as decimal(8,3))
		--	ELSE 0
		--END)/10000),totals.DECPLCUR-1) as autoCreditAmount -- autoCreditAmount
		, 0.00 as autoCreditAmount
		, ('RES' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + 'HEIPR') as batchID
		, ('ARRIVALS ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2)) as poNum

from	#IPC IPC

LEFT OUTER JOIN @hotelTotals as totals 
	on	IPC.hotel_Code = totals.hotelCode

WHERE	IPC.billing_Date between @startDate and @endDate
AND totals.mainBrand IN ('HE','HW')
--AND		IPC.sopNumber is null

GROUP BY IPC.hotel_code, IPC.mainbrandcode, IPC.Hotel_Currency, IPC.Qty,
		totals.PYMTRMID ,totals.DSCPCTAM ,totals.DECPLCUR, IPC.gpSiteId,IPC.itemcode
ORDER BY IPC.hotel_code 

-- only iprefer charges
update	ri
set		batchID = 'RES ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + ' IPO' 
from	functions.dbo.reservationIntegration ri
where customercode in 
	(	select	distinct customercode 
		from	functions.dbo.reservationIntegration
		where	itemCode = 'IPREFER'
				OR itemcode LIKE '%[_]I'
	)
	AND customercode NOT IN  
	(	select	CustomerCode 
		from	functions.dbo.reservationIntegration
		group by customerCode
		having COUNT(distinct batchID) > 1
	)	
	
update	ri
set		batchID = 'RES ' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + ' IPR' 
from functions.dbo.reservationIntegration as ri
inner join @hotelTotals as totals
on ri.customerCode = totals.hotelCode
and totals.mainBrand NOT IN ('HE','HW')
where	customercode in 
	(	select	distinct customercode 
		from	functions.dbo.reservationIntegration
		where	itemCode = 'IPREFER'
				OR itemcode LIKE '%[_]I'
	)
	
update	ri
set		batchID = 'RES' + CAST(YEAR(@startDate) as nvarchar(4)) + '-' + RIGHT(('0' + CAST(MONTH(@startDate) as nvarchar(2))), 2) + 'HEIPR' 
from functions.dbo.reservationIntegration as ri
inner join @hotelTotals as totals
on ri.customerCode = totals.hotelCode
and totals.mainBrand IN ('HE','HW')
where	customercode in 
	(	select	distinct customercode 
		from	functions.dbo.reservationIntegration
		where	itemCode = 'IPREFER'
				OR itemcode LIKE '%[_]I'
	)	

--delete any invoices that would have no charges
DELETE FROM [functions].[dbo].[reservationIntegration]
WHERE customerCode IN (
SELECT customerCode
  FROM [functions].[dbo].[reservationIntegration]
  GROUP BY customerCode
  HAVING SUM(totalPrice) = 0)

END



GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[postJournalEntryIntegration]'
GO

-- =============================================
-- Author:		Charles Wahl
-- Create date: 2013-12-04
-- Description: Charles Z wanted me to get the Journal Entry integration up and running.  This is a temporary solution until Tory finishes his part.
--				Will need to possibly update the mapping once preJournalEntryIntegration is done.
-- =============================================
ALTER procedure [dbo].[postJournalEntryIntegration]
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,@startdate)) --the end of the start month
	
	UPDATE	CM
	SET		CM.jeNumber = j.JRNENTRY
	FROM	ReservationBilling.loyalty.Credit_Memos CM with (nolock)
	INNER JOIN IC.dbo.RM00101 r
			ON CM.hotel_code = r.CUSTNMBR
	INNER JOIN IC.dbo.GL10000 j
			ON r.CURNCYID = j.CURNCYID
			AND BACHNUMB = ('IPREFERCM' + CAST(YEAR(@invoiceDate) as nvarchar(4)) + RIGHT(('0' + CAST(MONTH(@invoiceDate) as nvarchar(2))), 2))
	WHERE	CM.Create_Date >= @startDate
	AND		CM.Redemption_Date <= @enddate
			AND CM.SOPNUMBER IS NOT NULL
			AND CM.jeNumber IS NULL;

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
