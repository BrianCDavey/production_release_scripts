USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/22/2020 10:11:58 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [loyalty].[Calculate_Credit_Memos]'
GO

ALTER	procedure [loyalty].[Calculate_Credit_Memos]
AS
BEGIN


declare	@startDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --first day of previous month
		,@endDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) --last day of previous month


/*	It is possible for the same Voucher Number to appear in multiple Epsilon Redemption files. 
	We want to end up with only one record per voucher number. 
	Add handling to achieve the following:

		1. If the voucher number does not previously exist in our data, add it
		2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
		3. If the voucher number DOES exist in our data and DOES have a SOP Number, check all fields of the new record against the existing.
			3a. If the new record matches the old record, ignore the new record
			3b. If the new record has a change in any field from the old record, it needs to be reported as an error to the Finance team somehow
*/


--		1. If the voucher number does not previously exist in our data, add it
select	i.voucher_number, MAX(i.id) mostrecent_id
into	#insert_new
from	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import i
	left outer join loyalty.Credit_Memos cm
		on i.Voucher_Number = cm.Voucher_Number
where	i.importdate >= @startDate
	and i.Redemption_Date <= @enddate
	and cm.Voucher_Number IS NULL
group by i.Voucher_Number


--		2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
select	i.voucher_number , MAX(i.id) mostrecent_id
into	#update_exists -- select * 
from	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import i
	left outer join loyalty.Credit_Memos cm
		on i.Voucher_Number = cm.Voucher_Number
where	i.importdate >= @startDate
	and i.Redemption_Date <= @enddate
	and cm.SOPNumber IS NULL
group by i.Voucher_Number


--		3. If the voucher number DOES exist in our data and DOES have a SOP Number
--			check all fields of the new record against the existing.
--		3a. If the new record matches the old record, ignore the new record
select	Imported.voucher_number
into	#ignore
from	loyalty.Credit_Memos CM
join	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import Imported
 on		CM.Voucher_Number = Imported.Voucher_Number
where	Imported.importdate >= @startDate
and		Imported.Redemption_Date <= @enddate
and		CM.Redemption_Date = Imported.Redemption_Date
and		CM.Hotel_Code = Imported.Hotel_Code 
and		CM.Membership_Number = Imported.Membership_Number
and		CM.Voucher_Value = Imported.Voucher_Value 
and		CM.Voucher_Currency = Imported.Voucher_Currency 
and		CM.Payable_Value = Imported.Payable_Value_USD 
and		CM.SOPNumber is not null

--		3. If the voucher number DOES exist in our data and DOES have a SOP Number
--			check all fields of the new record against the existing.
--		3b. If the new record has a change in any field from the old record
--			it needs to be reported as an error to the Finance team somehow
select	Imported.voucher_number
, 'Voucher_Number already exists with an SOPNumer, but with different data.' as ErrorMessage
into	#error
from	Loyalty.Credit_Memos CM
join	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import Imported
 on		CM.Voucher_Number = Imported.Voucher_Number
where	Imported.importdate >= @startDate
and		Imported.Redemption_Date <= @enddate
and		CM.SOPNumber is not null
and (	CM.Redemption_Date <> Imported.Redemption_Date
or		CM.Hotel_Code <> Imported.Hotel_Code 
or		CM.Membership_Number <> Imported.Membership_Number 
or		CM.Voucher_Value <> Imported.Voucher_Value 
or		CM.Voucher_Currency <> Imported.Voucher_Currency 
or		CM.Payable_Value <> Imported.Payable_Value_USD 
	)


insert	into	loyalty.Credit_Memos
	(	Redemption_Date, Hotel_Code, Membership_Number, Voucher_Number, 
		Voucher_Value, Voucher_Currency, Payable_Value, 
		Create_Date, SOPNumber, Invoice_Date, Currency_Conversion_Date
	)
select	Redemption_Date, Hotel_Code, Membership_Number, i.Voucher_Number, 
		Voucher_Value, Voucher_Currency, Payable_Value_USD, 
		GETDATE() as Create_Date, null as SOPNumber, null as Invoice_Date, Currency_Conversion_Date
from	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import BSI
join	#insert_new i
	on	BSI.Voucher_Number = i.Voucher_Number
	and	BSI.id = i.mostrecent_id

update	CM
set		CM.Redemption_Date = Imported.Redemption_Date, 
		CM.Hotel_Code = Imported.Hotel_Code , 
		CM.Membership_Number = Imported.Membership_Number , 
		CM.Voucher_Value = Imported.Voucher_Value , 
		CM.Voucher_Currency = Imported.Voucher_Currency , 
		CM.Payable_Value = Imported.Payable_Value_USD , 
		CM.Currency_Conversion_Date = Imported.Currency_Conversion_Date, 
		CM.Create_Date = GetDate()
from	Loyalty.Credit_Memos CM
join	Loyalty.dbo.Account_Statement_Hotel_Redemption_Import Imported
	on	CM.Voucher_Number = Imported.Voucher_Number
join	#update_exists u
	on	CM.Voucher_Number = u.Voucher_Number
	and	Imported.id = u.mostrecent_id


-- empty table for the current run
truncate table loyalty.[Credit_Memo_Errors]

INSERT INTO Loyalty.[Credit_Memo_Errors]
	(	[Redemption_Date],[Hotel_Code],[Hotel_Name],[Membership_Number],[Member_Name],[Voucher_Type]
		,[Voucher_Number],[Voucher_Name],[Voucher_Value],[Voucher_Currency],[Payable_Value_USD]
		,[filename],[importdate],[Error_Message]
	)
select	imported.Redemption_Date, imported.Hotel_Code, imported.Hotel_Name, imported.Membership_Number, 
		imported.Member_Name, imported.Voucher_Type, imported.Voucher_Number, imported.Voucher_Name,
		imported.Voucher_Value, imported.Voucher_Currency, imported.Payable_Value_USD, 
		imported.filename, imported.importdate, e.ErrorMessage
from 	#error e
join	Loyalty.dbo.[Account_Statement_Hotel_Redemption_Import] imported
	on	e.Voucher_Number = imported.Voucher_Number
           

drop table #insert_new
drop table #update_exists
drop table #error
drop table #ignore

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SourceCodes]'
GO
ALTER VIEW [dbo].[SourceCodes]
AS
SELECT         ISNULL(CAST((row_number() OVER (ORDER BY primaryChannelDescription )) AS int), 0) 
AS EDMXID, primaryChannelDescription, primaryChannelCode, secondaryChannelDescription, secondaryChannelCode, subChannelDescription, 
                         subChannelCode
FROM            Superset.dbo.sourceCodes AS sourceCodes_1
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
