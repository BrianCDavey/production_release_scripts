USE Reservations
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/14/2020 11:11:58 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_DailyPointTransactionsExport]'
GO

ALTER PROCEDURE [dbo].[Epsilon_DailyPointTransactionsExport]
	@QueueID int,
	@AsOfDay date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	--CREATE & POPULATE #EPLT ------------------------------------------------------------
	IF OBJECT_ID('tempdb..#EPTL') IS NOT NULL
		DROP TABLE #EPTL;

	CREATE TABLE #EPTL
	(
		QueueID int NOT NULL,
		ConfirmationNumber nvarchar(100) NOT NULL,
		[DataSourceID] int NOT NULL,
		[status] nvarchar(10),

		PRIMARY KEY CLUSTERED(QueueID,ConfirmationNumber,[DataSourceID])
	)

	INSERT INTO #EPTL(QueueID,ConfirmationNumber,DataSourceID,[status])
	SELECT erl.QueueID,erl.ConfirmationNumber,erl.[DataSourceID],erl.[status]
	FROM ETL.dbo.EpsilonPointTransactionLog erl
		INNER JOIN 
		(
			SELECT MAX(EpsilonPointTransactionLogID) AS EpsilonPointTransactionLogID,DataSourceID,confirmationNumber
			FROM ETL.dbo.EpsilonPointTransactionLog 
			WHERE QueueID < @QueueID
			GROUP BY DataSourceID,confirmationNumber
		) FindMax ON erl.EpsilonPointTransactionLogID = FindMax.EpsilonPointTransactionLogID
	--------------------------------------------------------------------------------------
	
	-- POPULATE ETL.[dbo].[EpsilonPointTransactionLog] -----------------------------------
	INSERT INTO ETL.[dbo].[EpsilonPointTransactionLog]([QueueID],[ConfirmationNumber],[DataSourceID],[TransactionID],[SourceKey],[transactionTimeStamp],
														[actionType],[DateSent],[HotelCode],[ArrivalDate],[DepartureDate],[status])
	SELECT DISTINCT @QueueID,t.[ConfirmationNumber],t.[DataSourceID],t.[TransactionID],t.[SourceKey],ISNULL(t.[transactionTimeStamp],
					ts.confirmationDate),act.[actionType],GETDATE(),hh.hotelCode,td.arrivalDate,td.[DepartureDate],ts.[status]
	FROM dbo.Transactions t
		INNER JOIN dbo.TransactionDetail td ON t.TransactionDetailID = td.TransactionDetailID
		INNER JOIN dbo.TransactionStatus ts ON t.TransactionStatusID = ts.TransactionStatusID
		INNER JOIN dbo.ActionType act ON ts.ActionTypeID = act.ActionTypeID
		INNER JOIN dbo.hotel h ON t.HotelID = h.HotelID
		INNER JOIN Hotels.dbo.hotel hh ON h.Hotel_hotelID = hh.HotelID
		INNER JOIN dbo.RateCode rc ON t.RateCodeID = rc.RateCodeID
		LEFT JOIN dbo.PromoCode promo ON t.PromoCodeID = promo.PromoCodeID
		LEFT JOIN dbo.vw_CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN ReservationBilling.dbo.Charges C ON c.confirmationNumber = t.confirmationNumber AND c.transactionSourceID = t.DataSourceID AND c.sopNumber IS NOT NULL --do not send something that is billed already.
		LEFT JOIN ReservationBilling.dbo.Charges ch ON t.confirmationNumber = ch.confirmationNumber AND ch.classificationID = 5 AND ch.sopNumber IS NULL AND ch.transactionSourceID <> 3--filter Billy calculated for iprefer charge. Remove manual point award.
		--checking for the reservation in general
		LEFT JOIN #EPTL AS latestSent ON t.DataSourceID = latestSent.DataSourceID AND t.confirmationNumber = latestSent.confirmationNumber
		LEFT JOIN ETL.dbo.EpsilonPointTransactionLog erl ON t.TransactionID = erl.TransactionID AND t.sourceKey = erl.SourceKey
	WHERE td.arrivalDate <= @AsOfDay  --The arrivaldate is today or earlier 
		AND td.arrivalDate >= '2019-12-01' --previous arrival would have been billed
		AND c.chargeID IS NULL --if a SOP Number exists on a charge table. Once we have billed the point transaction,we should never send a modify or cancel to Epsilon.
		AND
		(
			ch.chargeID IS NOT NULL
			OR
			(
				ts.[status] = 'Cancelled'
				AND
				td.LoyaltyNumberValidated = 1
			)
		) -- Billy calculated with Iprefer + Void Reservation with Iprefer
		AND erl.transactionID IS NULL --do not send the same transaction again
		AND
		(
			(
				ts.[status] = 'Cancelled'
				AND
				(
					latestSent.[status] <> 'Cancelled'
					AND
					latestSent.[status] IS NOT NULL 
				)
			)
			OR ts.[status] <> 'Cancelled'
		) --remove NX AND cancel only reservation
	--------------------------------------------------------------------------------------

	-- CREATE & POPULATE #RD -------------------------------------------------------------
	IF OBJECT_ID('tempdb..#RD') IS NOT NULL
		DROP TABLE #RD;

	CREATE TABLE #RD
	(
		billableDate date,
		confirmationNumber nvarchar(100) NOT NULL,

		PRIMARY KEY CLUSTERED(confirmationNumber)
	)

	INSERT INTO #RD(billableDate,confirmationNumber)
  	SELECT MAX(billableDate) AS billableDate,confirmationNumber
	FROM ReservationBilling.dbo.Charges ch 
	WHERE ch.classificationID = 5 AND ch.sopNumber IS NULL AND ch.transactionSourceID <> 3--filter Billy calculated for iprefer charge. Remove manual point award.
	GROUP BY confirmationNumber
	--------------------------------------------------------------------------------------

	-- CREATE & POPULATE #RD -------------------------------------------------------------
	IF OBJECT_ID('tempdb..#JED') IS NOT NULL
		DROP TABLE #JED;
	CREATE TABLE #JED
	(
		confirmationNumber nvarchar(100) NOT NULL,
		DataSourceID int NOT NULL,
		itineraryNumber nvarchar(18),
		HotelID int,
		
		PRIMARY KEY CLUSTERED(confirmationNumber)
	)

	INSERT INTO #JED(confirmationNumber,DataSourceID,itineraryNumber,HotelID)
	SELECT t.confirmationNumber,t.DataSourceID,t.itineraryNumber,t.HotelID
	FROM dbo.Transactions t
		INNER JOIN ETL.dbo.[EpsilonPointTransactionLog] eptl ON eptl.TransactionID = t.TransactionID
	WHERE eptl.QueueID = @QueueID
	--------------------------------------------------------------------------------------

	-- POPULATE #JED_2 -------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#JED_2') IS NOT NULL
		DROP TABLE #JED_2;
	CREATE TABLE #JED_2
	(
		confirmationNumber nvarchar(100) NOT NULL,
		JED_2 nvarchar(MAX)
		
		PRIMARY KEY CLUSTERED(confirmationNumber)
	)

	;WITH iopt
	AS
	(
	  SELECT hptd.HotelID,
			MIN(ISNULL(hptd.[StartDate],'1900-01-01')) AS startDate,
			MAX(ISNULL(hptd.[EndDate],'9999-09-09')) AS endDate
	  FROM [Hotels].[dbo].[Hotel_ProTools_Detail] hptd
		INNER JOIN Hotels.dbo.ProTools_Detail ptd ON ptd.ProTools_DetailID = hptd.ProTools_DetailID
	  WHERE ptd.ProToolsID IN(SELECT ProToolsID FROM Hotels.dbo.ProTools WHERE ProToolsName = 'I Prefer Opt-In') --14
	  GROUP BY hptd.HotelID
	)
	INSERT INTO #JED_2(confirmationNumber,JED_2)
	SELECT confirmationNumber,
		(
			SELECT
				ISNULL(t2.TransactionID,'') AS PhgTransactionId
				,ISNULL(t2.confirmationNumber,'') AS ConfirmationNumber
				,ISNULL(hh.SynXisID,hh.OpenHospID) AS CrsHotelCode
				,ISNULL(xe.toUSD,0) AS UsdExchangeRate
				,ISNULL(xe.rateDate,'9999-09-09') AS UsdExchangeDate
				,CASE 
					WHEN d.SourceName = 'SynXis' THEN 'Synxis' 
					WHEN d.SourceName = 'Open Hospitality' THEN 'Open Hospitality' 
					ELSE 'ERROR WITH CRS MAPPING' 
					END AS CrsSource
				,ISNULL(pc.PH_Channel,'') AS BookingChannel
				,ISNULL(pbs.PH_ChannelID,0) AS BookingChannelID
				,ISNULL(rc.RateCode,'') AS RateCode
				,ISNULL(rc.RateName,'') AS RateName
				,ISNULL(rcat.rateCategoryCode,'') AS RateCategory
				,ISNULL(corp.corporationCode,'') AS CorpCode
				,ISNULL(rt.roomTypeCode,'') AS RoomCode
				,ISNULL(rt.roomTypeName,'') AS RoomName
				,'' AS RoomCategory
				,ISNULL(td.rooms,0) AS RoomCount
				,ISNULL(td.totalGuestCount,0) AS GuestCount
				,ISNULL(td.childrenCount,0) AS ChildCount
				,ISNULL(t.itineraryNumber,0) AS IteneraryNumber
				,ISNULL(i.IATANumber,'') AS IataNumber
				,'' AS PsuedoCityCode
				,ISNULL(ta.Name,'') AS TravelAgencyName
				,'' AS TravelAgentFirstName
				,'' AS TravelAgentLastName
				,LOWER(ISNULL(ta.Email,'')) AS TravelAgentEmailAddress
				,ISNULL(ts.creditCardType,'') AS CreditCardType
				,ISNULL(ts.cancellationNumber,'') AS CancellationNumber
				,ISNULL(ts.cancellationDate,'') AS CancellationDate
				,ISNULL(g.FirstName,'') AS GuestFirstName
				,ISNULL(g.LastName,'') AS GuestLastName
				,LOWER(ISNULL(ge.emailAddress,'')) AS GuestEmail
				,CASE WHEN iopt.HotelID IS NULL THEN '' ELSE td.optIn END AS LoyaltyOptIn
				,CASE WHEN iopt.HotelID IS NULL THEN td.optIn ELSE '' END AS MarketingOptIn
				,ISNULL(rd.billableDate,'') AS ReportDate
				,td.reservationRevenue AS OriginalReservationRevenue
				,td.currency AS OriginalReservationCurrencyCode
			FROM dbo.Transactions t2
				LEFT JOIN dbo.TransactionDetail td ON t2.TransactionDetailID = td.TransactionDetailID
				LEFT JOIN dbo.TransactionStatus ts ON t2.TransactionStatusID = ts.TransactionStatusID
				LEFT JOIN dbo.hotel h ON t2.HotelID = h.HotelID
				LEFT JOIN Hotels.dbo.Hotel hh ON h.Hotel_hotelID = hh.HotelID
				LEFT JOIN CurrencyRates.dbo.dailyRates xe ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
															AND td.currency = xe.code
				LEFT JOIN dbo.PH_BookingSource pbs ON t2.PH_BookingSourceID = pbs.PH_BookingSourceID
				LEFT JOIN dbo.PH_Channel pc ON pbs.PH_ChannelID = pc.PH_ChannelID
				LEFT JOIN dbo.RateCode rc ON t2.RateCodeID = rc.RateCodeID						
				LEFT JOIN dbo.RateCategory rcat ON t2.RateCategoryID = rcat.RateCategoryID
				LEFT JOIN dbo.CorporateCode corp ON t2.CorporateCodeID = corp.CorporateCodeID
				LEFT JOIN dbo.RoomType rt ON t2.RoomTypeID = rt.RoomTypeID
				LEFT JOIN dbo.IATANumber i ON t2.IATANumberID = i.IATANumberID
				LEFT JOIN dbo.TravelAgent ta ON t2.TravelAgentID = ta.TravelAgentID
				LEFT JOIN dbo.Guest g ON t2.GuestID = g.GuestID
				LEFT JOIN dbo.Guest_EmailAddress ge ON g.Guest_EmailAddressID = ge.Guest_EmailAddressID
				LEFT JOIN iopt ON h.Hotel_hotelID = iopt.hotelId AND ts.confirmationDate BETWEEN iopt.startDate AND iopt.endDate
				LEFT JOIN authority.DataSource d ON d.DataSourceID = t.DataSourceID
				LEFT JOIN #RD rd ON t2.confirmationNumber = rd.confirmationNumber
			WHERE t.confirmationNumber = t2.confirmationNumber
			FOR JSON PATH,WITHOUT_ARRAY_WRAPPER
		) AS JED_2
	FROM #JED t
	--------------------------------------------------------------------------------------

	-- POPULATE #JED_3 -------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#JED_3') IS NOT NULL
		DROP TABLE #JED_3;
	CREATE TABLE #JED_3
	(
		confirmationNumber nvarchar(100) NOT NULL,
		JED_3 nvarchar(MAX)
		
		PRIMARY KEY CLUSTERED(confirmationNumber)
	)

	INSERT INTO #JED_3(confirmationNumber,JED_3)
	SELECT confirmationNumber,
		(
			SELECT *
			FROM
			(
				SELECT ISNULL(tag.id,0) AS TravelAgencyGroupID,ISNULL(tag.name,'') AS TravelAgencyGroupName
				FROM dbo.Transactions t3
					INNER JOIN dbo.TransactionStatus ts ON t3.TransactionStatusID = ts.TransactionStatusID
					INNER JOIN dbo.IATANumber i ON t3.IATANumberID = i.IATANumberID
					INNER JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag ON i.IATANumber = taitag.travelAgentId
																				AND ts.confirmationDate BETWEEN taitag.startDate
																				AND taitag.endDate
					INNER JOIN Core.dbo.travelAgentGroups tag ON taitag.travelAgentGroupID = tag.id
				WHERE t.confirmationNumber = t3.confirmationNumber
					
				UNION ALL
					
				SELECT 0 AS TravelAgencyGroupID,'' AS TravelAgencyGroupName
				WHERE NOT EXISTS
						(
							SELECT 1
							FROM dbo.Transactions t3
								INNER JOIN dbo.TransactionStatus ts ON t3.TransactionStatusID = ts.TransactionStatusID
								INNER JOIN dbo.IATANumber i ON t3.IATANumberID = i.IATANumberID
								INNER JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag ON i.IATANumber = taitag.travelAgentId
																							AND ts.confirmationDate BETWEEN taitag.startDate
																							AND taitag.endDate
								INNER JOIN Core.dbo.travelAgentGroups tag ON taitag.travelAgentGroupID = tag.id
							WHERE t.confirmationNumber = t3.confirmationNumber
						)
			) sub 
			FOR JSON PATH,ROOT('TravelAgencyGroups')
		) AS JED_3
	FROM #JED t
	--------------------------------------------------------------------------------------

	-- POPULATE #JED_4 -------------------------------------------------------------------
	IF OBJECT_ID('tempdb..#JED_4') IS NOT NULL
		DROP TABLE #JED_4;
	CREATE TABLE #JED_4
	(
		confirmationNumber nvarchar(100) NOT NULL,
		JED_4 nvarchar(MAX)
		
		PRIMARY KEY CLUSTERED(confirmationNumber)
	)

	INSERT INTO #JED_4(confirmationNumber,JED_4)
	SELECT confirmationNumber,
		(
			SELECT *
			FROM
			(
				SELECT DISTINCT ISNULL(cg.criteriaGroupID,0) AS BookingSourceID,ISNULL(cg.criteriaGroupName,'') AS BookingSourceName
				FROM dbo.Transactions t4
					INNER JOIN dbo.TransactionDetail td ON t4.TransactionDetailID = td.TransactionDetailID
					INNER JOIN dbo.CRS_BookingSource cbs ON t4.CRS_BookingSourceID = cbs.BookingSourceID
					INNER JOIN dbo.CRS_Channel cc ON cbs.ChannelID = cc.ChannelID
					INNER JOIN dbo.CRS_SecondarySource c2s ON cbs.SecondarySourceID = c2s.SecondarySourceID
					INNER JOIN dbo.CRS_SubSource css ON cbs.SubSourceID = css.SubSourceID
					LEFT JOIN dbo.ibeSource ibs ON cbs.ibeSourceNameID = ibs.ibeSourceID --change left join
					LEFT JOIN authority.ibeSource aibs ON ibs.auth_ibeSourceID = aibs.ibeSourceID --change left join
					LEFT JOIN ReservationBilling.dbo.Templates rbt ON aibs.ibeSourceName = rbt.xbeTemplateName --change left join
					LEFT JOIN dbo.CROCode cro ON cbs.CROCodeID = cro.CROCodeID --change left join
					LEFT JOIN authority.CRO_Code acro ON cro.auth_CRO_CodeID = acro.CRO_CodeID --change left join
					LEFT JOIN ReservationBilling.dbo.CROCodes rbc ON acro.CRO_Code = rbc.croCode --change left join
					INNER JOIN [ReservationBilling].[dbo].[Charges] ch ON t4.confirmationNumber = ch.confirmationNumber AND ch.classificationID = 5
					INNER JOIN ReservationBilling.dbo.Clauses cl ON ch.clauseID = cl.clauseID
					INNER JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg ON cl.clauseID = cicg.clauseID
					INNER JOIN ReservationBilling.dbo.CriteriaGroups cg ON cicg.criteriaGroupID = cg.criteriaGroupID
				WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5

				UNION ALL

				SELECT 0 AS BookingSourceID,'' AS BookingSourceName
				WHERE NOT EXISTS
						(
							SELECT 1
							FROM dbo.Transactions t4
								INNER JOIN dbo.TransactionDetail td ON t4.TransactionDetailID = td.TransactionDetailID
								INNER JOIN dbo.CRS_BookingSource cbs ON t4.CRS_BookingSourceID = cbs.BookingSourceID
								INNER JOIN dbo.CRS_Channel cc ON cbs.ChannelID = cc.ChannelID
								INNER JOIN dbo.CRS_SecondarySource c2s ON cbs.SecondarySourceID = c2s.SecondarySourceID
								INNER JOIN dbo.CRS_SubSource css ON cbs.SubSourceID = css.SubSourceID
								LEFT JOIN dbo.ibeSource ibs ON cbs.ibeSourceNameID = ibs.ibeSourceID --change left join
								LEFT JOIN authority.ibeSource aibs ON ibs.auth_ibeSourceID = aibs.ibeSourceID --change left join
								LEFT JOIN ReservationBilling.dbo.Templates rbt ON aibs.ibeSourceName = rbt.xbeTemplateName --change left join
								LEFT JOIN dbo.CROCode cro ON cbs.CROCodeID = cro.CROCodeID --change left join
								LEFT JOIN authority.CRO_Code acro ON cro.auth_CRO_CodeID = acro.CRO_CodeID --change left join
								LEFT JOIN ReservationBilling.dbo.CROCodes rbc ON acro.CRO_Code = rbc.croCode --change left join
								INNER JOIN [ReservationBilling].[dbo].[Charges] ch ON t4.confirmationNumber = ch.confirmationNumber AND ch.classificationID = 5
								INNER JOIN ReservationBilling.dbo.Clauses cl ON ch.clauseID = cl.clauseID
								INNER JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg ON cl.clauseID = cicg.clauseID
								INNER JOIN ReservationBilling.dbo.CriteriaGroups cg ON cicg.criteriaGroupID = cg.criteriaGroupID
							WHERE t.confirmationNumber = t4.confirmationNumber
								AND ch.classificationID = 5
						)
			) sub
			FOR JSON PATH,ROOT('BookingSources')
		) AS JED_4
	FROM #JED t
	--------------------------------------------------------------------------------------

	-- POPULATE ETL.dbo.EpsilonExportPointTransaction ------------------------------------
	;WITH jed
	AS
	(
		SELECT 
			t.confirmationNumber,
			REPLACE
			(
				CONCAT_WS
				(
					'ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
					(SELECT JED_2 FROM #JED_2 j2 WHERE j2.confirmationNumber = t.confirmationNumber),
					(SELECT JED_3 FROM #JED_3 j3 WHERE j3.confirmationNumber = t.confirmationNumber),
					(SELECT JED_4 FROM #JED_4 j4 WHERE j4.confirmationNumber = t.confirmationNumber)
				),
				'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',','
			)  AS jsonExternalData
		FROM #JED t
	)
	INSERT INTO ETL.dbo.EpsilonExportPointTransaction(QueueID,[01],[TRANSACTION_NUMBER],[ACT_TRANSACTION_ID],[PROFILE_ID],[CARD_NUMBER],[TRANSACTION_TYPE_CODE],
														[TRANS_DESCRIPTION],[TRANSACTION_DATE],[END_DATE],[TRANSACTION_NET_AMOUNT],[ELIGIBLE_REVENUE],
														[TAX_AMOUNT],[POST_SALES_ADJUSTMNT_AMT],[SHIPPING_HANDLING],[STATUS],[DEVICE_ID],[DEVICE_USERID],
														[AUTHORIZATION_CODE],[ORIGINAL_TRANSACTION_NUMBER],[SKIP_MOMENT_ENGINE_IND],[CURRENCY_CODE],
														[ACCT_SRC_CODE],[SRC_ACCOUNT_NUM],[ACTIVITY_DATE],[CLIENT_FILE_ID],[CLIENT_FILE_REC_NUM],[BRAND_ORG_CODE],
														[ASSOCIATE_NUM],[GROSS_AMOUNT],[NET_AMT],[PROG_CURR_GROSS_AMT],[TXN_SEQ_NUM],[TXN_SOURCE_CODE],
														[TXN_SUBTYPE_CODE],[TIER],[TRANS_LOCATION],[blank1],[PROGRAM_CODE],[EXCHANGE_RATE_ID],[GRATUITY],
														[SRC_CUSTOMER_ID],[DISC_AMT],[TXN_CHANNEL_CODE],[TXN_BUSINESS_DATE],[PROMO_CODE],[BUYER_TYPE_CODE],
														[RMB_METHOD_CODE],[RMB_VENDOR_CODE],[RMB_DATE],[blank2],[PROG_CURR_NET_AMT],[PROG_CURR_TAX_AMT],
														[PROG_CURR_POST_SALES_ADJ_AMT],[PROG_CURR_SHIPPING_HANDLING],[PROG_CURR_ELIG_REVENUE],
														[PROG_CURR_GRATUITY],[TXN_TYPE_CODE],[ORIGINAL_STORE_CODE],[ORIGINAL_TRANSACTION_DATE],
														[ORIGINAL_TRANSACTION_END_DATE],[SUSPEND_REASON],[SUSPEND_TRANSACTION],[JSON_EXTERNAL_DATA],
														[POSTING_KEY_CODE],[POSTING_KEY_VALUE])
	SELECT
		@QueueID,
		'01' AS [01],
		t.confirmationNumber AS TRANSACTION_NUMBER,
		NULL AS ACT_TRANSACTION_ID,
		NULL AS PROFILE_ID,
		UPPER(l.loyaltyNumber) AS CARD_NUMBER,
		CASE WHEN ts.[status] = 'Cancelled'  THEN 'VD' ELSE 'PR' END AS TRANSACTION_TYPE_CODE,
		'Transaction' AS TRANS_DESCRIPTION,
		td.arrivalDate AS TRANSACTION_DATE,
		td.departureDate AS END_DATE,
		[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS TRANSACTION_NET_AMOUNT, --USD?
		NULL AS ELIGIBLE_REVENUE,
		NULL AS TAX_AMOUNT,
		NULL AS POST_SALES_ADJUSTMNT_AMT,
		NULL AS SHIPPING_HANDLING,
		NULL AS STATUS,
		NULL AS DEVICE_ID,
		NULL AS DEVICE_USERID,
		NULL AS AUTHORIZATION_CODE,
		CASE WHEN ts.[status] = 'Cancelled' THEN t.confirmationNumber ELSE NULL END AS ORIGINAL_TRANSACTION_NUMBER,
		'False' AS SKIP_MOMENT_ENGINE_IND,
		'USD' AS CURRENCY_CODE,
		'ALMACCT' AS ACCT_SRC_CODE,
		g.customerID AS SRC_ACCOUNT_NUM,
		CASE WHEN eptl.transactionTimeStamp IS NULL THEN ts.confirmationDate ELSE eptl.transactionTimeStamp END AS ACTIVITY_DATE,
		t.QueueID AS CLIENT_FILE_ID,
		NULL AS CLIENT_FILE_REC_NUM,
		'ALM_BRAND' AS BRAND_ORG_CODE,
		NULL AS ASSOCIATE_NUM,
		[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS GROSS_AMOUNT,
		[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS NET_AMT,
		NULL AS PROG_CURR_GROSS_AMT,
		NULL AS TXN_SEQ_NUM,
		'PHG File' AS TXN_SOURCE_CODE,
		CASE WHEN d.SourceName = 'SynXis' THEN 'SYNXIS' WHEN d.SourceName = 'Open Hospitality' THEN 'OPENH' ELSE '' END AS TXN_SUBTYPE_CODE,
		NULL AS TIER,
		hh.HotelCode AS TRANS_LOCATION,
		NULL AS [blank1],--intentionally left blank per spec
		'PHG' AS PROGRAM_CODE,
		NULL AS EXCHANGE_RATE_ID,
		NULL AS GRATUITY,
		NULL AS SRC_CUSTOMER_ID,
		NULL AS DISC_AMT,
		NULL AS TXN_CHANNEL_CODE,
		ts.confirmationDate AS TXN_BUSINESS_DATE,
		promo.promotionalCode AS PROMO_CODE,
		NULL AS BUYER_TYPE_CODE,
		NULL AS RMB_METHOD_CODE,
		NULL AS RMB_VENDOR_CODE,
		NULL AS RMB_DATE,
		NULL AS [blank2],--intentionally left blank per spec
		NULL AS PROG_CURR_NET_AMT,
		NULL AS PROG_CURR_TAX_AMT,
		NULL AS PROG_CURR_POST_SALES_ADJ_AMT,
		NULL AS PROG_CURR_SHIPPING_HANDLING,
		NULL AS PROG_CURR_ELIG_REVENUE,
		NULL AS PROG_CURR_GRATUITY,
		NULL AS TXN_TYPE_CODE,
		CASE WHEN ts.[status] = 'Cancelled' THEN hh.hotelCode ELSE NULL END AS ORIGINAL_STORE_CODE,
		CASE WHEN ts.[status] = 'Cancelled' THEN td.arrivalDate ELSE NULL END AS ORIGINAL_TRANSACTION_DATE,
		CASE WHEN ts.[status] = 'Cancelled' THEN td.departureDate ELSE NULL END AS ORIGINAL_TRANSACTION_END_DATE,
		NULL AS SUSPEND_REASON,
		NULL AS SUSPEND_TRANSACTION,
		REPLACE(REPLACE(jed.jsonExternalData,CHAR(10),''),CHAR(13),'') AS JSON_EXTERNAL_DATA,
		NULL AS POSTING_KEY_CODE,
		NULL AS POSTING_KEY_VALUE
	FROM dbo.Transactions t
		INNER JOIN dbo.TransactionDetail td ON t.TransactionDetailID = td.TransactionDetailID
		INNER JOIN dbo.TransactionStatus ts ON t.TransactionStatusID = ts.TransactionStatusID
		INNER JOIN dbo.ActionType act ON ts.ActionTypeID = act.ActionTypeID
		LEFT JOIN dbo.LoyaltyNumber l ON t.LoyaltyNumberID = l.LoyaltyNumberID
		INNER JOIN CurrencyRates.dbo.dailyRates xe ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
													AND td.currency = xe.code
		INNER JOIN dbo.hotel h ON t.HotelID = h.HotelID
		INNER JOIN Hotels.dbo.hotel hh ON h.Hotel_hotelID = hh.HotelID
		INNER JOIN dbo.PH_BookingSource pbs ON t.PH_BookingSourceID = pbs.PH_BookingSourceID
		INNER JOIN dbo.PH_Channel pc ON pbs.PH_ChannelID = pc.PH_ChannelID
		INNER JOIN dbo.PH_SecondaryChannel psc ON pbs.PH_SecondaryChannelID = psc.PH_SecondaryChannelID
		INNER JOIN dbo.RateCode rc ON t.RateCodeID = rc.RateCodeID
		LEFT JOIN dbo.PromoCode promo ON t.PromoCodeID = promo.PromoCodeID
		LEFT JOIN jed ON t.confirmationNumber = jed.confirmationNumber
		LEFT JOIN authority.DataSource d ON d.DataSourceID = t.DataSourceID
		LEFT JOIN dbo.Guest g ON t.GuestID = g.GuestID
		INNER JOIN ETL.dbo.[EpsilonPointTransactionLog] eptl ON eptl.TransactionID = t.TransactionID
	WHERE eptl.QueueID = @QueueID AND ts.[status] <> 'Cancelled'  --Remove VD. VD will be generated later
	--------------------------------------------------------------------------------------

	-- UPDATE TRANSACTION_NUMBER TO ABC123-PR-YYYY-MM-DDTHH:MM:SS ------------------------
	UPDATE ETL.dbo.EpsilonExportPointTransaction
		SET TRANSACTION_NUMBER = [TRANSACTION_NUMBER]+ '-' +[TRANSACTION_TYPE_CODE] + '-'+ CAST(CAST([ACTIVITY_DATE] AS date) AS nvarchar(10) ) + 'T' + CAST(CAST([ACTIVITY_DATE] AS time) AS nvarchar(8))
	WHERE QueueID = @QueueID
	--------------------------------------------------------------------------------------

	-- POPULATE ETL.dbo.EpsilonExportPointTransaction ------------------------------------
	INSERT INTO ETL.dbo.EpsilonExportPointTransaction(QueueID,[01],[TRANSACTION_NUMBER],[ACT_TRANSACTION_ID],[PROFILE_ID],[CARD_NUMBER],[TRANSACTION_TYPE_CODE],
														[TRANS_DESCRIPTION],[TRANSACTION_DATE],[END_DATE],[TRANSACTION_NET_AMOUNT],[ELIGIBLE_REVENUE],
														[TAX_AMOUNT],[POST_SALES_ADJUSTMNT_AMT],[SHIPPING_HANDLING],[STATUS],[DEVICE_ID],[DEVICE_USERID],
														[AUTHORIZATION_CODE],[ORIGINAL_TRANSACTION_NUMBER],[SKIP_MOMENT_ENGINE_IND],[CURRENCY_CODE],
														[ACCT_SRC_CODE],[SRC_ACCOUNT_NUM],[ACTIVITY_DATE],[CLIENT_FILE_ID],[CLIENT_FILE_REC_NUM],[BRAND_ORG_CODE],
														[ASSOCIATE_NUM],[GROSS_AMOUNT],[NET_AMT],[PROG_CURR_GROSS_AMT],[TXN_SEQ_NUM],[TXN_SOURCE_CODE],
														[TXN_SUBTYPE_CODE],[TIER],[TRANS_LOCATION],[blank1],[PROGRAM_CODE],[EXCHANGE_RATE_ID],[GRATUITY],
														[SRC_CUSTOMER_ID],[DISC_AMT],[TXN_CHANNEL_CODE],[TXN_BUSINESS_DATE],[PROMO_CODE],[BUYER_TYPE_CODE],
														[RMB_METHOD_CODE],[RMB_VENDOR_CODE],[RMB_DATE],[blank2],[PROG_CURR_NET_AMT],[PROG_CURR_TAX_AMT],
														[PROG_CURR_POST_SALES_ADJ_AMT],[PROG_CURR_SHIPPING_HANDLING],[PROG_CURR_ELIG_REVENUE],
														[PROG_CURR_GRATUITY],[TXN_TYPE_CODE],[ORIGINAL_STORE_CODE],[ORIGINAL_TRANSACTION_DATE],
														[ORIGINAL_TRANSACTION_END_DATE],[SUSPEND_REASON],[SUSPEND_TRANSACTION],[JSON_EXTERNAL_DATA],
														[POSTING_KEY_CODE],[POSTING_KEY_VALUE])
	SELECT
		@QueueID,
		[01],
		CASE WHEN LEN([TRANSACTION_NUMBER]) < 21 THEN [TRANSACTION_NUMBER]+ '-VD-'+ CAST(CAST([ACTIVITY_DATE] AS date) AS nvarchar(10) ) + 'T' + CAST(CAST([ACTIVITY_DATE] AS time) AS nvarchar(8) )
		ELSE REPLACE([TRANSACTION_NUMBER],'-PR-','-VD-') END AS [TRANSACTION_NUMBER],
		[ACT_TRANSACTION_ID],
		[PROFILE_ID],
		[CARD_NUMBER],
		'VD' AS [TRANSACTION_TYPE_CODE],
		[TRANS_DESCRIPTION],
		[TRANSACTION_DATE],
		[END_DATE],
		[TRANSACTION_NET_AMOUNT],
		[ELIGIBLE_REVENUE],
		[TAX_AMOUNT],
		[POST_SALES_ADJUSTMNT_AMT],
		[SHIPPING_HANDLING],
		big.[STATUS],
		[DEVICE_ID],
		[DEVICE_USERID],
		[AUTHORIZATION_CODE],
		[TRANSACTION_NUMBER] AS [ORIGINAL_TRANSACTION_NUMBER],
		[SKIP_MOMENT_ENGINE_IND],
		[CURRENCY_CODE],
		[ACCT_SRC_CODE],
		[SRC_ACCOUNT_NUM],
		[ACTIVITY_DATE],
		[CLIENT_FILE_ID],
		[CLIENT_FILE_REC_NUM],
		[BRAND_ORG_CODE],
		[ASSOCIATE_NUM],
		[GROSS_AMOUNT],
		[NET_AMT],
		[PROG_CURR_GROSS_AMT],
		[TXN_SEQ_NUM],
		[TXN_SOURCE_CODE],
		[TXN_SUBTYPE_CODE],
		[TIER],
		[TRANS_LOCATION],
		[blank1],
		[PROGRAM_CODE],
		[EXCHANGE_RATE_ID],
		[GRATUITY],
		[SRC_CUSTOMER_ID],
		[DISC_AMT],
		[TXN_CHANNEL_CODE],
		[TXN_BUSINESS_DATE],
		[PROMO_CODE],
		[BUYER_TYPE_CODE],
		[RMB_METHOD_CODE],
		[RMB_VENDOR_CODE],
		[RMB_DATE],
		[blank2],
		[PROG_CURR_NET_AMT],
		[PROG_CURR_TAX_AMT],
		[PROG_CURR_POST_SALES_ADJ_AMT],
		[PROG_CURR_SHIPPING_HANDLING],
		[PROG_CURR_ELIG_REVENUE],
		[PROG_CURR_GRATUITY],
		[TXN_TYPE_CODE],
		TRANS_LOCATION AS [ORIGINAL_STORE_CODE],
		[TRANSACTION_DATE] AS [ORIGINAL_TRANSACTION_DATE],
		END_DATE AS [ORIGINAL_TRANSACTION_END_DATE],
		[SUSPEND_REASON],
		[SUSPEND_TRANSACTION],
		[JSON_EXTERNAL_DATA],
		[POSTING_KEY_CODE],
		[POSTING_KEY_VALUE]
	FROM ETL.dbo.EpsilonExportPointTransaction big
		INNER JOIN #EPTL AS small ON CASE
										WHEN LEN(big.[TRANSACTION_NUMBER]) <= 20 THEN big.[TRANSACTION_NUMBER]
										ELSE LEFT(big.[TRANSACTION_NUMBER],CHARINDEX('-',big.[TRANSACTION_NUMBER])-1)
									END = small.ConfirmationNumber
									AND big.QueueID = small.QueueID
									AND TRANSACTION_TYPE_CODE = 'PR'
									AND (CASE WHEN big.TXN_SUBTYPE_CODE = 'SynXis' THEN 1 ELSE 2 END  = small.[DataSourceID])
		INNER JOIN ETL.dbo.EpsilonPointTransactionLog eptl ON eptl. QueueID = @QueueID
															AND eptl.Confirmationnumber = CASE
																							WHEN LEN(big.[TRANSACTION_NUMBER])<=20   THEN big.[TRANSACTION_NUMBER]
																							ELSE LEFT(big.[TRANSACTION_NUMBER],CHARINDEX('-',big.[TRANSACTION_NUMBER])-1)
																						END
															AND (CASE WHEN big.TXN_SUBTYPE_CODE = 'SynXis' THEN 1 ELSE 2 END  = eptl.[DataSourceID])
	--------------------------------------------------------------------------------------		

	-- UPDATE ETL.dbo.EpsilonExportPointTransaction --------------------------------------
	UPDATE ept
		SET ept.CARD_NUMBER = UPPER(c.LoyaltyNumber)
	FROM ETL.dbo.EpsilonExportPointTransaction ept
		INNER JOIN ReservationBilling.dbo.Charges C ON CASE
															WHEN LEN(ept.[TRANSACTION_NUMBER])<=20 THEN ept.[TRANSACTION_NUMBER]
															ELSE LEFT(ept.[TRANSACTION_NUMBER],CHARINDEX('-',ept.[TRANSACTION_NUMBER])-1)
														END  = c.ConfirmationNumber
														AND c.classificationID = 5
	WHERE (ept.CARD_NUMBER IS NULL OR ept.CARD_NUMBER = '')
		AND ept.QueueID = @QueueID
	--------------------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
