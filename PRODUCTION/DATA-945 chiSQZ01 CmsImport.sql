USE CmsImport
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[mpid_Hotels]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE dbo.Hotels;

		--get all hotel ids and all collections they are in
		DECLARE @HotelCodes TABLE(Accountid uniqueidentifier,CollectionCode nvarchar(100));

		INSERT intO @HotelCodes(Accountid,CollectionCode)
		SELECT DISTINCT hc.phg_hotel AS Accountid,c.phg_code AS CollectionCode
		FROM LocalCRM.dbo.phg_hotelcollection hc
			INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
		WHERE GETDATE() BETWEEN ISNULL(hc.phg_startdate, '1900-1-1') AND ISNULL(hc.phg_enddate, '2100-12-31');


		--combine the collections into one field per hotel
		DECLARE @GroupedHotelCodes TABLE(Accountid uniqueidentifier,CollectionCodes nvarchar(100));

		INSERT intO @GroupedHotelCodes(Accountid,CollectionCodes)
		SELECT DISTINCT Codes.Accountid,
				CAST((SELECT H.CollectionCode + ' ' FROM @HotelCodes H WHERE H.Accountid = Codes.Accountid ORDER BY H.CollectionCode FOR XML PATH('')) AS nvarchar(MAX)) AS hotels
		FROM @HotelCodes AS Codes;


		--get all contacts from Umbraco
		DECLARE @umbracoContacts TABLE(nodeId int,crmContactGuid uniqueidentifier);

		INSERT intO @umbracoContacts (nodeId,crmContactGuid)
		SELECT nodeId,CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier')
		FROM MemberPortal.dbo.cmsContentXml
		WHERE CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
			AND CAST(cmsContentXml.xml as xml).value('(/PhgEmployee/disabled)[1]','bit') = 0;


		 --get all hotels from Umbraco
		 DECLARE @umbracoHotels TABLE(hotelCrmGuid uniqueidentifier,
										hotelCode nvarchar(100),
										nodeName nvarchar(100),
										hotelName nvarchar(100),
										hotelCity nvarchar(100),
										hotelState nvarchar(100),
										hotelCountry nvarchar(100),
										hotelRegion nvarchar(100),
										hotelIprefer bit,
										hotelAccountDirector int,
										hotelMarketingContact int,
										hotelRegionalDirector int,
										hotelAreaManagingDirector int,
										hotelRevenueAccountManager int,
										hotelAccountsReceivableContact int,
										hotelIPreferEngagementSpecialist int,
										hotelExecutiveVicePresident int,
										hotelEventContact int,
										contractedCurrency nvarchar(3),
										isDisabled bit,
										hotelCollectionCodes nvarchar(100),
										hotelMainCollection nvarchar(50),
										hotelRateGainId nvarchar(50),
										hotelHidePayment bit,
										hotelRegionalAdministration int);

		INSERT intO @umbracoHotels(hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,hotelMarketingContact,
									hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
									hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCrmGuid)[1]','uniqueidentifier') AS hotelCrmGuid,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCode)[1]','nvarchar(MAX)') AS hotelCode,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelName)[1]','nvarchar(MAX)') AS hotelName,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCity)[1]','nvarchar(MAX)'), '') AS hotelCity,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelState)[1]','nvarchar(MAX)'), '') AS hotelState,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCountry)[1]','nvarchar(MAX)'), '') AS hotelCountry,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelRegion)[1]','nvarchar(MAX)'), '') AS hotelRegion,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelIprefer)[1]','bit') AS hotelIprefer,
				nhad.id AS hotelAccountDirector,
				nhmc.id AS hotelMarketingContact,
				nhrd.id AS hotelRegionalDirector,
				nhamd.id AS hotelAreaManagingDirector,
				nhram.id AS hotelRevenueAccountManager,
				nharc.id AS hotelAccountsReceivableContact,
				nhipes.id AS hotelIPreferEngagementSpecialist,
				nhevp.id AS hotelExecutiveVicePresident,
				nhec.id AS hotelEventContact,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/contractedCurrency)[1]','nvarchar(MAX)'), '') AS contractedCurrency,
				CAST(cmsContentXml.xml as xml).value('(/Hotel/disabled)[1]','bit') AS isDisabled,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelCollectionCodes)[1]','nvarchar(MAX)'), '') AS hotelCollectionCodes,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelMainCollection)[1]','nvarchar(MAX)'), '') AS hotelMainCollection,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelRateGainID)[1]','nvarchar(MAX)'), '') AS hotelRateGainId,
				ISNULL(CAST(cmsContentXml.xml as xml).value('(/Hotel/hotelHidePayment)[1]','bit'), 1) AS hotelHidePayment,
				nhra.id AS hotelRegionalAdministration
		FROM (
				SELECT xml,
				CAST(cx.xml as xml).value('(/Hotel/hotelAccountDirector)[1]','nvarchar(100)') hotelAccountDirector,
				CAST(cx.xml as xml).value('(/Hotel/hotelMarketingContact)[1]','nvarchar(100)') hotelMarketingContact,
				CAST(cx.xml as xml).value('(/Hotel/hotelRegionalDirector)[1]','nvarchar(100)') hotelRegionalDirector,
				CAST(cx.xml as xml).value('(/Hotel/hotelAreaManagingDirector)[1]','nvarchar(100)') hotelAreaManagingDirector,
				CAST(cx.xml as xml).value('(/Hotel/hotelRevenueAccountManager)[1]','nvarchar(100)') hotelRevenueAccountManager,
				CAST(cx.xml as xml).value('(/Hotel/hotelAccountsReceivableContact)[1]','nvarchar(100)') hotelAccountsReceivableContact,
				CAST(cx.xml as xml).value('(/Hotel/hotelIPreferEngagementSpecialist)[1]','nvarchar(100)') hotelIPreferEngagementSpecialist,
				CAST(cx.xml as xml).value('(/Hotel/hotelExecutiveVicePresident)[1]','nvarchar(100)') hotelExecutiveVicePresident,
				CAST(cx.xml as xml).value('(/Hotel/hotelEventContact)[1]','nvarchar(100)') hotelEventContact,
				CAST(cx.xml as xml).value('(/Hotel/hotelRegionalAdministration)[1]','nvarchar(100)') hotelRegionalAdministration
				FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
				WHERE ct.alias = 'Hotel'
			) cmsContentXml
			LEFT JOIN MemberPortal.dbo.umbracoNode nhad ON nhad.uniqueID = CAST(SUBSTRING(hotelAccountDirector, 16, 8) + '-' + SUBSTRING(hotelAccountDirector, 24, 4) + '-' + SUBSTRING(hotelAccountDirector, 28, 4) + '-' + SUBSTRING(hotelAccountDirector, 32, 4) + '-' + SUBSTRING(hotelAccountDirector, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhmc ON nhmc.uniqueID = CAST(SUBSTRING(hotelMarketingContact, 16, 8) + '-' + SUBSTRING(hotelMarketingContact, 24, 4) + '-' + SUBSTRING(hotelMarketingContact, 28, 4) + '-' + SUBSTRING(hotelMarketingContact, 32, 4) + '-' + SUBSTRING(hotelMarketingContact, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhrd ON nhrd.uniqueID = CAST(SUBSTRING(hotelRegionalDirector, 16, 8) + '-' + SUBSTRING(hotelRegionalDirector, 24, 4) + '-' + SUBSTRING(hotelRegionalDirector, 28, 4) + '-' + SUBSTRING(hotelRegionalDirector, 32, 4) + '-' + SUBSTRING(hotelRegionalDirector, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhamd ON nhamd.uniqueID = CAST(SUBSTRING(hotelAreaManagingDirector, 16, 8) + '-' + SUBSTRING(hotelAreaManagingDirector, 24, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 28, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 32, 4) + '-' + SUBSTRING(hotelAreaManagingDirector, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhram ON nhram.uniqueID = CAST(SUBSTRING(hotelRevenueAccountManager, 16, 8) + '-' + SUBSTRING(hotelRevenueAccountManager, 24, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 28, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 32, 4) + '-' + SUBSTRING(hotelRevenueAccountManager, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nharc ON nharc.uniqueID = CAST(SUBSTRING(hotelAccountsReceivableContact, 16, 8) + '-' + SUBSTRING(hotelAccountsReceivableContact, 24, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 28, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 32, 4) + '-' + SUBSTRING(hotelAccountsReceivableContact, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhipes ON nhipes.uniqueID = CAST(SUBSTRING(hotelIPreferEngagementSpecialist, 16, 8) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 24, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 28, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 32, 4) + '-' + SUBSTRING(hotelIPreferEngagementSpecialist, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhevp ON nhevp.uniqueID = CAST(SUBSTRING(hotelExecutiveVicePresident, 16, 8) + '-' + SUBSTRING(hotelExecutiveVicePresident, 24, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 28, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 32, 4) + '-' + SUBSTRING(hotelExecutiveVicePresident, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhec ON nhec.uniqueID = CAST(SUBSTRING(hotelEventContact, 16, 8) + '-' + SUBSTRING(hotelEventContact, 24, 4) + '-' + SUBSTRING(hotelEventContact, 28, 4) + '-' + SUBSTRING(hotelEventContact, 32, 4) + '-' + SUBSTRING(hotelEventContact, 36, 12) AS uniqueidentifier)
			LEFT JOIN MemberPortal.dbo.umbracoNode nhra ON nhra.uniqueID = CAST(SUBSTRING(hotelRegionalAdministration, 16, 8) + '-' + SUBSTRING(hotelRegionalAdministration, 24, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 28, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 32, 4) + '-' + SUBSTRING(hotelRegionalAdministration, 36, 12) AS uniqueidentifier)
		;



		--combine hotels from Umbraco and hotels from CRM into one table, with "table name" showing which system it came from
		--SOURCE table = CRM, TARGET table = Umbraco
		DECLARE @HotelData TABLE(TableName nvarchar(6),
								hotelCrmGuid uniqueidentifier,
								hotelCode nvarchar(100),
								nodeName nvarchar(100),
								hotelName nvarchar(100),
								hotelCity nvarchar(100),
								hotelState nvarchar(100),
								hotelCountry nvarchar(100),
								hotelRegion nvarchar(100),
								hotelIprefer bit,
								hotelAccountDirector int,
								hotelMarketingContact int,
								hotelRegionalDirector int,
								hotelAreaManagingDirector int,
								hotelRevenueAccountManager int,
								hotelAccountsReceivableContact int,
								hotelIPreferEngagementSpecialist int,
								hotelExecutiveVicePresident int,
								hotelEventContact int,
								contractedCurrency nvarchar(3),
								isDisabled bit,
								hotelCollectionCodes nvarchar(100),
								hotelMainCollection nvarchar(50),
								hotelRateGainId nvarchar(50),
								hotelRegionalAdministration int);

		INSERT INTO @HotelData(TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
								hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration)
		SELECT MIN(TableName) AS TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,MIN(hotelState),hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
				hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
				hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration
		FROM (SELECT DISTINCT 'SOURCE' AS TableName,hotelsReporting.crmGuid AS hotelCrmGuid,hotelsReporting.code AS hotelCode,hotelsReporting.hotelName AS nodeName,
						hotelsReporting.hotelName AS hotelName,ISNULL(hotelsReporting.physicalCity, '') AS hotelCity,ISNULL(subAreas.subAreaName, '') AS hotelState,
						ISNULL(countries.shortName, '') AS hotelCountry,ISNULL(hotelsReporting.geographicRegionName, '') AS hotelRegion,
						ISNULL(iPrefer.ContractFound, 0) AS hotelIprefer,hotelAccountDirector.nodeId AS hotelAccountDirector,hotelMarketingContact.nodeId AS hotelMarketingContact,
						hotelRegionalDirector.nodeId AS hotelRegionalDirector,hotelAreaManagingDirector.nodeId AS hotelAreaManagingDirector,
						hotelRevenueAccountManager.nodeId AS hotelRevenueAccountManager,hotelAccountsReceivableContact.nodeId AS hotelAccountsReceivableContact,
						hotelIPreferEngagementSpecialist.nodeId AS hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident.nodeId AS hotelExecutiveVicePresident,
						hotelEventContact.nodeId AS hotelEventContact,ISNULL(FinancialAccounts.currencyCode, '') AS contractedCurrency,0 AS isDisabled,
						ISNULL(BrandCodes.CollectionCodes, '') AS hotelCollectionCodes,ISNULL(mainBrandCode + ',' + brands.name, '') AS hotelMainCollection,
						ISNULL(phg_rategainid, '') AS hotelRateGainId,hotelRegionalAdministration.nodeId AS hotelRegionalAdministration
				FROM Core.dbo.hotelsReporting
					INNER JOIN (SELECT DISTINCT Account.accountid,Account.phg_accountdirectorid,Account.phg_marketingcontactid,Account.phg_regionalmanagerid,
										Account.phg_areamanagerid,Account.phg_revenueaccountmanagerid,Account.phg_accountsreceivablecontactid,Account.phg_ipreferexecutiveadminid,
										Account.phg_executivevicepresidentid,Account.phg_rategainid,Account.phg_regionaladministrationid
								FROM LocalCRM.dbo.Account
									INNER JOIN LocalCRM.dbo.phg_hotelcollection hc ON Account.accountid = hc.phg_hotel
									INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
								WHERE Account.statuscodename IN('Member Hotel','Renewal Hotel')
									OR Account.PHG_MemberPortalImportOverride = 0
								) Account ON hotelsReporting.crmGuid = Account.accountID
					LEFT JOIN ISO.dbo.subAreas ON subAreas.countryCode2 = hotelsReporting.country AND subAreas.subAreaCode = hotelsReporting.state
					LEFT JOIN ISO.dbo.countries ON hotelsReporting.country = countries.code2
					LEFT JOIN (SELECT DISTINCT Account.accountid AS AccountId,1 AS ContractFound
								FROM (SELECT DISTINCT hc.phg_hotel AS Accountid
										FROM LocalCRM.dbo.Account
											INNER JOIN LocalCRM.dbo.phg_hotelcollection hc ON Account.accountid = hc.phg_hotel
											INNER JOIN LocalCRM.dbo.phg_collection c ON hc.phg_collection = c.phg_collectionid
										WHERE Account.statuscodename IN('Member Hotel','Renewal Hotel')
											OR Account.PHG_MemberPortalImportOverride = 0
									  ) Account
									INNER JOIN LocalCRM.dbo.phg_contractedprograms ON Account.accountId = phg_contractedprograms.phg_accountnameid
																	AND phg_contractedprograms.phg_contractedprogramstypename LIKE '%i%Prefer%'
																	AND GETDATE() BETWEEN ISNULL(phg_contractedprograms.phg_startdate, '1900-1-1')
																	AND ISNULL(phg_contractedprograms.phg_enddate, '2100-12-31')
								) iPrefer ON Account.accountId = iPrefer.AccountId
					LEFT JOIN @GroupedHotelCodes AS BrandCodes ON hotelsReporting.crmGuid = BrandCodes.Accountid
					LEFT JOIN Core.dbo.brands ON hotelsReporting.mainBrandCode = brands.code
					LEFT JOIN @umbracoContacts as hotelAccountDirector ON hotelAccountDirector.crmContactGuid = Account.phg_accountdirectorid
					LEFT JOIN @umbracoContacts as hotelMarketingContact ON hotelMarketingContact.crmContactGuid = Account.phg_marketingcontactid
					LEFT JOIN @umbracoContacts as hotelRegionalDirector ON hotelRegionalDirector.crmContactGuid = Account.phg_regionalmanagerid
					LEFT JOIN @umbracoContacts as hotelAreaManagingDirector ON hotelAreaManagingDirector.crmContactGuid = Account.phg_areamanagerid
					LEFT JOIN @umbracoContacts as hotelRevenueAccountManager ON hotelRevenueAccountManager.crmContactGuid = Account.phg_revenueaccountmanagerid
					LEFT JOIN @umbracoContacts as hotelAccountsReceivableContact ON hotelAccountsReceivableContact.crmContactGuid = Account.phg_accountsreceivablecontactid
					LEFT JOIN @umbracoContacts as hotelExecutiveVicePresident ON hotelExecutiveVicePresident.crmContactGuid = Account.phg_executivevicepresidentid
					LEFT JOIN @umbracoContacts as hotelIPreferEngagementSpecialist ON hotelIPreferEngagementSpecialist.crmContactGuid = Account.phg_ipreferexecutiveadminid
					LEFT JOIN @umbracoContacts as hotelRegionalAdministration ON hotelRegionalAdministration.crmContactGuid = Account.phg_regionaladministrationid
					LEFT JOIN @umbracoContacts as hotelEventContact ON hotelEventContact.crmContactGuid = '570C90EF-8AA2-E611-80F9-FC15B4282DF4'
					LEFT JOIN dbo.FinancialAccounts ON hotelsReporting.code = FinancialAccounts.hotelCode
						UNION
				SELECT 'TARGET' AS TableName,hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
						hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
						hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,isDisabled,hotelCollectionCodes,
						hotelMainCollection,hotelRateGainId,hotelRegionalAdministration
				FROM @umbracoHotels
			) unioned
		GROUP BY hotelCrmGuid,hotelCode,nodeName,hotelName,hotelCity
		--,hotelState
		,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,hotelMarketingContact,
				hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,hotelIPreferEngagementSpecialist,
				hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,hotelMainCollection,hotelRateGainId,hotelRegionalAdministration,isDisabled
		--HAVING COUNT(*) = 1  <<Bruce Okallau Mar 30th 2020 I cannot understand why this would be there except perhaps to remove duplicates but unfortunately it ends up filtering out other valid hotels. Removing this for now but keeping it in the comments so that we can revisit the decision in the future. I'm also taking the minimum of the hotel state as some irregularities in ISO codes can lead to duplicate entries if we do not.
		;


		INSERT INTO dbo.Hotels(Name,hotelCrmGuid,hotelDisabled,hotelCode,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
								hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,
								hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT	S.hotelName,S.hotelCrmGuid,0,S.hotelCode,S.hotelName,S.hotelCity,S.hotelState,S.hotelCountry,S.hotelRegion,S.hotelIprefer,S.hotelAccountDirector,
				S.hotelMarketingContact,S.hotelRegionalDirector,S.hotelAreaManagingDirector,S.hotelRevenueAccountManager,S.hotelAccountsReceivableContact,
				S.hotelIPreferEngagementSpecialist,S.hotelExecutiveVicePresident,S.hotelEventContact,S.contractedCurrency,S.hotelCollectionCodes,S.hotelMainCollection,
				S.hotelRateGainId,COALESCE(U.hotelHidePayment, 1),S.hotelRegionalAdministration
		FROM @HotelData S
			LEFT JOIN @umbracoHotels U ON S.hotelCrmGuid = U.hotelCrmGuid
		WHERE TableName = 'SOURCE'


		INSERT INTO dbo.Hotels(Name,hotelCrmGuid,hotelDisabled,hotelCode,hotelName,hotelCity,hotelState,hotelCountry,hotelRegion,hotelIprefer,hotelAccountDirector,
								hotelMarketingContact,hotelRegionalDirector,hotelAreaManagingDirector,hotelRevenueAccountManager,hotelAccountsReceivableContact,
								hotelIPreferEngagementSpecialist,hotelExecutiveVicePresident,hotelEventContact,contractedCurrency,hotelCollectionCodes,
								hotelMainCollection,hotelRateGainId,hotelHidePayment,hotelRegionalAdministration)
		SELECT T.hotelName,T.hotelCrmGuid,1,T.hotelCode,T.hotelName,T.hotelCity,T.hotelState,T.hotelCountry,T.hotelRegion,T.hotelIprefer,T.hotelAccountDirector,
				T.hotelMarketingContact,T.hotelRegionalDirector,T.hotelAreaManagingDirector,T.hotelRevenueAccountManager,T.hotelAccountsReceivableContact,
				T.hotelIPreferEngagementSpecialist,T.hotelExecutiveVicePresident,T.hotelEventContact,T.contractedCurrency,T.hotelCollectionCodes,
				T.hotelMainCollection,T.hotelRateGainId,1,T.hotelRegionalAdministration
		FROM @HotelData T
			INNER JOIN @umbracoHotels U ON U.hotelCrmGuid = T.hotelCrmGuid AND U.isDisabled = 0
		WHERE T.TableName = 'TARGET'
			AND NOT EXISTS ( SELECT 1 FROM @HotelData S WHERE S.TableName = 'SOURCE' AND T.hotelCrmGuid = S.hotelCrmGuid );
	END
END
GO
