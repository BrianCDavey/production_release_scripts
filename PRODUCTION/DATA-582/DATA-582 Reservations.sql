USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-ST-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/14/2019 10:28:48 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[BillyReservationsCharges_ReservationDetails]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [rpt].[BillyReservationsCharges_ReservationDetails]
	-- Add the parameters for the stored procedure here
	@confirmationNumber nvarchar (20)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 MRT.confirmationNumber,
		MRT.status,
		MRT.hotelID,
		HR.code,
		HR.hotelName,
		MRT.chainID,
		MRT.chainName,
		MRT.confirmationDate,
		MRT.arrivalDate,
		MRT.departureDate,
		MRT.cancellationDate,
		MRT.cancellationNumber,
		MRT.channel,
		MRT.secondarySource,
		MRT.subSource,
		MRT.xbeTemplateName,
		TemplateGroups.templateGroupName AS templateGroup,
		MRT.croCode,
		CROGroups.croGroupName AS croGroup,
		MRT.rateCategoryCode,
		MRT.rateCategoryName,
		MRT.rateTypeCode,
		MRT.rateTypeName,
		MRT.iataNumber,
		MRT.travelAgencyName,
		MRT.loyaltyNumber,
		CASE 
			WHEN mrt.LoyaltyNumberTagged = 1 THEN 'Tagged' 
			WHEN mrt.loyaltyNumber IS NULL OR mrt.loyaltyNumber = '' THEN 'None' 
			WHEN mrt.[LoyaltyNumberValidated] = 0 THEN 'Invalid' 
			WHEN mrt.[LoyaltyNumberValidated] = 1 THEN 'Valid' ELSE 'None' 
			END AS LoyaltyNumberStatus,
		MRT.nights,
		MRT.rooms,
		MRT.nights * MRT.rooms AS RoomNights,
		MRT.reservationRevenue,
		MRT.currency,
		[roomRevenueInHotelCurrency] AS BillableCurrency,
		[hotelCurrencyCode] AS BillableCurrencyCode,
		Charges.roomRevenueInUSD  reservationRevenueUSD
		
FROM	Reservations.dbo.mostrecenttransactions MRT
		LEFT JOIN
		Hotels.dbo.hotelsReporting HR
		ON MRT.hotelID = HR.synxisID
			OR MRT.openHospitalityID = HR.openHospitalityCode
		LEFT JOIN
		Superset.BSI.Customer_Profile_Import CPI
		ON MRT.loyaltyNumber = CPI.iPrefer_Number
		LEFT JOIN
		ReservationBilling.dbo.Templates
		ON MRT.xbeTemplateName = Templates.xbeTemplateName
		LEFT JOIN
		ReservationBilling.dbo.TemplateGroups
		ON Templates.templateGroupID = TemplateGroups.templateGroupID
		LEFT JOIN
		ReservationBilling.dbo.CROCodes
		ON MRT.croCode = CROCodes.croCode
		LEFT JOIN
		ReservationBilling.dbo.CROGroups
		ON CROCodes.croGroupID = CROGroups.croGroupID
		LEFT JOIN ReservationBilling.dbo.Charges ON Charges.[confirmationNumber] = MRT.confirmationNumber
WHERE	MRT.confirmationNumber = @confirmationNumber
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[BillyReservationsCharges_Results]'
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [rpt].[BillyReservationsCharges_Results]
	-- Add the parameters for the stored procedure here
	@confirmationNumber nvarchar (20)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	Charges.clauseName,
		Classifications.classificationName,
		ThresholdTypes.thresholdTypeName AS ThresholdType,
		CASE
			WHEN Charges.chargeValueInHotelCurrency > BillingRules.thresholdMinimum THEN 'True'
			ELSE 'False'
		END AS MinThresholdMet,
		BillingRules.startDate,
		BillingRules.endDate,
		BillingRules.confirmationDate,
		BillingRules.afterConfirmation,
		BillingRules.perReservationPercentage,
		BillingRules.perReservationFlatFee,
		BillingRules.perRoomNightFlatFee,
		Charges.[hotelCurrencyCode] AS BillableCurrencyCode,
		[chargeValueInHotelCurrency] AS TotalChargeValue,
		Charges.itemCode,
		Charges.invoiceDate,
		Charges.sopNumber

FROM	ReservationBilling.dbo.Charges
		LEFT JOIN
		ReservationBilling.dbo.Classifications
		ON Charges.classificationID = Classifications.classificationID
		LEFT JOIN
		ReservationBilling.dbo.BillingRules
		ON Charges.billingRuleID = BillingRules.billingRuleID
		LEFT JOIN
		ReservationBilling.dbo.ThresholdRules
		ON BillingRules.clauseID = ThresholdRules.clauseID
		LEFT JOIN
		ReservationBilling.dbo.ThresholdTypes
		ON ThresholdRules.thresholdTypeID = ThresholdTypes.thresholdTypeID
  WHERE	confirmationNumber = @confirmationNumber
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
