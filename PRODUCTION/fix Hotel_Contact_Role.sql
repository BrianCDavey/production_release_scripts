USE Hotels
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Hotels    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Hotels

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/29/2019 7:43:39 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Hotel_Contact_Role]'
GO

ALTER PROCEDURE [dbo].[Populate_Hotel_Contact_Role]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_GUID
	AS
	(
		SELECT DISTINCT a.accountid,ct.contactid,cr.connectionroleid,CONVERT(date,con.phg_startdate) AS StartDate,CONVERT(date,con.phg_enddate) AS EndDate
		FROM LocalCRM.dbo.Account a
			INNER JOIN LocalCRM.dbo.Connection con ON con.record1id = a.accountid
			INNER JOIN LocalCRM.dbo.Contact ct ON ct.contactid = con.record2id
			INNER JOIN [LocalCRM].[dbo].[ConnectionRole] cr ON cr.connectionroleid = con.record2roleid
															AND cr.connectionroleid = con.record1roleid
		WHERE a.phg_hotelaccount = 1
	)
	MERGE INTO [dbo].[Hotel_Contact_Role] AS tgt
	USING
	(
		SELECT DISTINCT h.HotelID,con.ContactID,r.RoleID,c.StartDate,c.EndDate
		FROM cte_GUID c
			INNER JOIN dbo.Hotel h ON h.CRM_HotelID = c.accountid
			INNER JOIN dbo.Contact con ON con.CRM_ContactID = c.contactid
			INNER JOIN dbo.[Role] r ON r.CRM_RoleID = c.connectionroleid
	) AS src ON src.HotelID = tgt.[HotelID] AND src.ContactID = tgt.[ContactID] AND src.RoleID = tgt.[RoleID]
				AND ISNULL(src.StartDate,'9999-09-09') = ISNULL(tgt.StartDate,'9999-09-09') AND ISNULL(src.EndDate,'9999-09-09') = ISNULL(tgt.EndDate,'9999-09-09')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([HotelID],[ContactID],[RoleID],StartDate,EndDate)
		VALUES(src.HotelID,src.ContactID,src.RoleID,src.StartDate,src.EndDate)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HotelID_ContactID_RoleID_StartDate_EndDate] on [dbo].[Hotel_Contact_Role]'
GO
CREATE NONCLUSTERED INDEX [IX_HotelID_ContactID_RoleID_StartDate_EndDate] ON [dbo].[Hotel_Contact_Role] ([HotelID], [ContactID], [RoleID], [StartDate], [EndDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
