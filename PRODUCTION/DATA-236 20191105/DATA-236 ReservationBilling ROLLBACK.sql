USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/5/2019 11:58:50 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_clauseID_IC_criteriaGroupID_endDate_startDate] from [dbo].[Clauses_ExcludeCriteriaGroups]'
GO
DROP INDEX [IX_clauseID_IC_criteriaGroupID_endDate_startDate] ON [dbo].[Clauses_ExcludeCriteriaGroups]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_runID_IC_Others] from [work].[MrtForCalculation]'
GO
DROP INDEX [IX_runID_IC_Others] ON [work].[MrtForCalculation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_EXCHDATE] from [work].[local_exchange_rates]'
GO
DROP INDEX [IX_EXCHDATE] ON [work].[local_exchange_rates]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [work].[Calculate_Charges]'
GO
DROP PROCEDURE [work].[Calculate_Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [work].[Delete_MRT]'
GO
DROP PROCEDURE [work].[Delete_MRT]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[mrtJoined_Reservation]'
GO







ALTER VIEW [work].[mrtJoined_Reservation]
AS

     SELECT 
			1 AS transactionSourceID, -- hardcode 1 use transactionID for the difference between openhospitality and sabre
			t.TransactionID as transactionKey,
            t.confirmationNumber,
			hh.HotelCode AS phgHotelCode,
            COALESCE(hh.openhospID, hh.synXisID) AS crsHotelID,
            hh.HotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
			CASE WHEN ch.ChainID = 22 THEN 56 ELSE ch.ChainID END AS [ChainID],--change from CRS_ChainID since open hospitality CRS_ChainID is not integer
            ch.chainName,
            ts.status AS bookingStatus,
            CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN bd.billingDescription ELSE 'Unknown' END AS synxisBillingDescription,
            cha.channel AS bookingChannel,
            sec.secondarySource AS bookingSecondarySource,
            sub.subSourceCode AS bookingSubSourceCode,
            COALESCE(te.templateGroupID, 2) AS bookingTemplateGroupId,
			CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END AS bookingTemplateAbbreviation,
            aibe.ibeSourceName AS xbeTemplateName,
            acro.CRO_Code AS CROcode,
            CASE WHEN acro.CRO_Code IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            rc.rateCategoryCode AS bookingRateCategoryCode,
            rac.RateCode AS bookingRateCode,
            ISNULL(iata.IATANumber,'') AS bookingIATA,
            t.transactionTimeStamp,
            ts.confirmationDate,
            td.arrivalDate,
            td.departureDate,
            ts.cancellationDate,
            ts.cancellationNumber,
            td.nights,
            td.rooms,
            td.nights * td.rooms AS roomNights,
            td.reservationRevenue AS roomRevenueInBookingCurrency,
            td.currency AS bookingCurrencyCode,
            t.timeLoaded,
			work.[billyItemCode](CASE WHEN bs.openHospID IS NOT NULL THEN 'Open Hospitality' WHEN bs.synXisID IS NOT NULL THEN bd.billingDescription ELSE 'Unknown' END,cha.channel,sec.secondarySource,sub.subSourceCode,CASE WHEN sec.secondarySource = 'iPrefer APP' THEN 'IPREFERAPP' ELSE COALESCE(te.siteAbbreviation, 'HOTEL') END,croCodes.croGroupID, 123) AS [ItemCode], --hard code chainId since OH chainID is not int
			CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN ts.confirmationDate ELSE td.arrivaldate END) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            CASE WHEN bs.openHospID IS NOT NULL THEN 2 WHEN bs.synXisID IS NOT NULL THEN 1 ELSE 'Unknown' END AS CRSSourceID,
			lp.LoyaltyProgram,
			ISNULL(ln.loyaltyNumber,'') AS loyaltyNumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],
			--force tag if negative iprefer returns
			 CASE WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberValidated,0)
					WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					WHEN work.billyLoyaltyFlipFlag(t.confirmationNumber) = 1 THEN 1
					ELSE ISNULL(td.LoyaltyNumberValidated,0) END AS LoyaltyNumberValidated
				--new change
			,CASE WHEN ISNULL(td.LoyaltyNumberTagged,0) = 0 THEN 0
					 WHEN tdr.Booking_ID IS NULL THEN ISNULL(td.LoyaltyNumberTagged,0)
					 WHEN [work].[billyLoyaltyFlipFlagNoSupersetCore](t.confirmationNumber,hh.HotelCode, MONTH(td.arrivalDate), YEAR(td.arrivalDate)) = 1 THEN 0
					 ELSE ISNULL(td.LoyaltyNumberTagged,0)
					 END as LoyaltyNumberTagged
		FROM Reservations.dbo.Transactions t WITH(NOLOCK)
		INNER JOIN Reservations.dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN Reservations.dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN Reservations.dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN Reservations.dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID AND hh.HotelCode NOT IN ('PHGTEST','BCTS4') 
		LEFT JOIN Reservations.dbo.CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN Reservations.dbo.CRS_Channel cha ON cha.ChannelID = bs.ChannelID
		LEFT JOIN Reservations.dbo.CRS_SecondarySource sec ON sec.SecondarySourceID = bs.SecondarySourceID
		LEFT JOIN Reservations.dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		LEFT JOIN Reservations.dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
		LEFT JOIN Reservations.authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
		LEFT JOIN Reservations.dbo.ibeSource ibe ON ibe.ibeSourceID = bs.ibeSourceNameID
		LEFT JOIN Reservations.authority.ibeSource aibe ON aibe.ibeSourceID = ibe.auth_ibeSourceID
		LEFT JOIN Reservations.dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN Reservations.dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN Reservations.dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN Reservations.dbo.LoyaltyProgram lp ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
		LEFT JOIN Reservations.synxis.transactions tr ON tr.TransactionID = t.sourceKey AND t.DataSourceID IN(SELECT DataSourceID FROM Reservations.authority.DataSource WHERE SourceName = 'SynXis')
		LEFT JOIN Reservations.synxis.BillingDescription bd ON tr.BillingDescriptionID = bd.BillingDescriptionID

        LEFT JOIN work.hotelActiveBrands ON hh.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hh.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hh.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON td.currency = bookingCE.CURNCYID 
			AND CONVERT(date,CASE WHEN td.arrivalDate >= GETDATE() THEN confirmationDate ELSE td.arrivaldate END) = bookingCE.EXCHDATE
		LEFT JOIN [Superset].[BSI].[TransactionDetailedReport] tdr ON tdr.Booking_ID = t.confirmationNumber AND tdr.Hotel_Code = hh.HotelCode
		AND MONTH(tdr.Reward_Posting_Date) = MONTH(td.arrivalDate) AND YEAR(tdr.Reward_Posting_Date) = YEAR(td.arrivalDate) AND tdr.Reservation_Revenue <> 0 AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source IN ('Admin Portal','Hotel Portal')
		LEFT JOIN [dbo].[CROCodes] ON acro.CRO_Code = croCodes.croCode
		LEFT JOIN dbo.Templates te ON te.xbeTemplateName = aibe.ibeSourceName

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Calculate_ThresholdCharges]'
GO




ALTER PROCEDURE [work].[Calculate_ThresholdCharges]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @startDate date,@endDate date
	
	SELECT	@startDate = startDate,
			@endDate = endDate
	FROM work.Run
	WHERE RunID = @RunID
	
	-- CREATE #THRESHOLD TEMP TABLE --------------------------------------------
	IF OBJECT_ID('tempdb..#THRESHOLD') IS NOT NULL
		DROP TABLE #THRESHOLD
	CREATE TABLE #THRESHOLD
	(
		billableDate date,
		transactionSourceID int,
		transactionKey nvarchar(20),
		hotelCode nvarchar(10),
		billingRuleID int,
		thresholdMinimum decimal(18,2),
		thresholdTypeID int,
		clauseID int,
		sumRoomNights int,
		countConfNum int,
		sumRoomRevenue decimal(18,2)
	)
	----------------------------------------------------------------------------

	DECLARE @billingRuleID int,
			@thresholdMinimum decimal(18,2),
			@LastPeriod date,
			@EndOfPeriod date,
			@IsGlobal bit,
			@hotelCode nvarchar(10),
			@thresholdTypeID int,
			@clauseID int
	
	-- LOOP THROUGH EACH BILLING PERIOD WITHIN DATE RANGE ----------------------
	DECLARE curPeriod CURSOR FAST_FORWARD LOCAL FOR
		SELECT lp.LastPeriod,lp.EndOfPeriod,tr.[global],cl.hotelCode,tr.thresholdTypeID,tr.clauseID
		FROM dbo.fnc_Threshold_LastPeriod(@startDate,@endDate,@RunID) lp
			INNER JOIN dbo.ThresholdRules tr ON tr.clauseID = lp.clauseID
			INNER JOIN dbo.Clauses cl ON cl.clauseID = tr.clauseID
			INNER JOIN (SELECT DISTINCT ClauseID FROM work.MrtForCalc_Clauses WHERE runID = @RunID) mrtc ON mrtc.ClauseID = cl.clauseID

		OPEN curPeriod
		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		;WITH cte_charges(clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,confCounter,[roomRevenueInHotelCurrency])
		AS
		(
			SELECT @clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,
				MAX(roomNights),1,MAX([roomRevenueInHotelCurrency])
			FROM dbo.Charges
			WHERE billableDate BETWEEN @LastPeriod AND @EndOfPeriod
				AND clauseID = CASE @IsGlobal WHEN 0 THEN @clauseID ELSE clauseID END
				--AND hotelCode = CASE @IsGlobal WHEN 0 THEN hotelCode ELSE @hotelCode END
				AND hotelCode = @hotelCode
				AND (@IsGlobal = 0 OR (@IsGlobal = 1 AND transactionSourceID != 3)) --iPrefer manual transactions don't count in global counts, only if the threshold is just for manual points
			GROUP BY billableDate,transactionSourceID,transactionKey,hotelCode
		),
		cte_Threshold
		AS
		(
			SELECT billableDate,transactionSourceID,transactionKey,hotelCode,clauseID,
					SUM(roomNights) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomNights,
					SUM(confCounter) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS countConfNum,
					SUM([roomRevenueInHotelCurrency]) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomRevenue
			FROM cte_charges
		)
		INSERT INTO #THRESHOLD(billableDate,transactionSourceID,transactionKey,hotelCode,billingRuleID,thresholdMinimum,thresholdTypeID,clauseID,
								sumRoomNights,countConfNum,sumRoomRevenue)
		SELECT DISTINCT billableDate,transactionSourceID,transactionKey,hotelCode,br.billingRuleID,br.thresholdMinimum,@thresholdTypeID,@clauseID,
						sumRoomNights,countConfNum,sumRoomRevenue
		FROM cte_Threshold tr
			INNER JOIN dbo.BillingRules br ON br.[clauseID] = tr.[clauseID] AND tr.billableDate BETWEEN br.startDate AND br.endDate
		WHERE sumRoomNights >= CASE @thresholdTypeID WHEN 3 THEN br.thresholdMinimum ELSE sumRoomNights END
			AND countConfNum >= CASE @thresholdTypeID WHEN 2 THEN br.thresholdMinimum ELSE countConfNum END
			AND sumRoomRevenue >= CASE @thresholdTypeID WHEN 1 THEN br.thresholdMinimum ELSE sumRoomRevenue END
			AND br.startDate <= @endDate AND br.endDate >= @startDate

		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID
	END
	DEALLOCATE curPeriod
	----------------------------------------------------------------------------


		-- DELETE ALL CHARGES THAT HAVE THE SAME ClauseID BUT != BillingRuleID -----
		;WITH cte_charges
		AS
		(
			SELECT DISTINCT t.transactionSourceID,t.transactionKey,t.billingRuleID,t.clauseID
			FROM dbo.Charges c
				INNER JOIN #THRESHOLD t ON t.transactionSourceID = c.transactionSourceID
					AND t.transactionKey = c.transactionKey
					AND t.clauseID = c.clauseID
				INNER JOIN (SELECT transactionSourceID,transactionKey,clauseID,MAX(thresholdMinimum) AS thresholdMinimum
							FROM #THRESHOLD
							GROUP BY transactionSourceID,transactionKey,clauseID
						   ) x ON x.transactionSourceID = t.transactionSourceID
					AND x.transactionKey = t.transactionKey 
					AND x.clauseID = t.clauseID 
					AND x.thresholdMinimum = t.thresholdMinimum
			WHERE c.billableDate BETWEEN @startDate AND @endDate
		)
		DELETE c
		FROM dbo.Charges c
			INNER JOIN cte_charges cte ON cte.transactionSourceID = c.transactionSourceID
				AND cte.transactionKey = c.transactionKey
				AND cte.clauseID = c.clauseID
		WHERE c.billingRuleID != cte.billingRuleID
			AND c.sopNumber IS NULL
			AND c.runID = @RunID
		----------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Calculate_StandardCharges]'
GO







ALTER PROCEDURE [work].[Calculate_StandardCharges]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE TEMP TABLE ---------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES;

	CREATE TABLE #CHARGES
	(
		[clauseID] [int] NOT NULL,
		[billingRuleID] [int] NOT NULL,
		[classificationID] [int] NOT NULL,
		[transactionSourceID] [int] NOT NULL,
		[transactionKey] [nvarchar](20) NOT NULL,
		[confirmationNumber] [nvarchar](255) NOT NULL,
		[hotelCode] [nvarchar](10) NOT NULL,
		[collectionCode] [nvarchar](6) NULL,
		[clauseName] [nvarchar](250) NULL,
		[billableDate] [date] NULL,
		[arrivalDate] [date] NULL,
		[roomNights] [int] NULL,
		[hotelCurrencyCode] [char](3) NULL,
		[exchangeDate] [date] NULL,
		[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
		[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
		[chargeValueInUSD] [decimal](38, 2) NULL,
		[roomRevenueInUSD] [decimal](38, 2) NULL,
		[itemCode] [nvarchar](50) NULL,
		[gpSiteID] [char](2) NULL,
		[dateCalculated] [datetime] NULL,
		[sopNumber] [char](21) NULL,
		[invoiceDate] [date] NULL,
		[loyaltyNumber] [nvarchar](50) NULL,
		[LoyaltyNumberValidated] [bit] NULL,
		[LoyaltyNumberTagged] [bit] NULL		
	)
	------------------------------------------------------------------------------------

	;WITH cte_clauses AS (
		SELECT DISTINCT runID, transactionSourceID, transactionKey, clauseID, clauseName, criteriaID, RateCategoryCode, RateCode, TravelAgentGroupID
		FROM work.MrtForCalc_Clauses
		WHERE runID = @RunID
	)
	INSERT INTO #CHARGES(clauseID,billingRuleID,classificationID,transactionSourceID,transactionKey,confirmationNumber,hotelCode,collectionCode,
							clauseName,billableDate,arrivalDate,roomNights,hotelCurrencyCode,exchangeDate,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate, loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged)
	SELECT clauses.clauseID,br.billingRuleID,br.classificationID,mrtC.transactionSourceID,mrtC.transactionKey,mrtC.confirmationNumber,mrtC.phgHotelCode,mrtC.mainBrandCode AS collectionCode,
			clauses.ClauseName,mrtC.billableDate,mrtC.arrivalDate,mrtC.roomNights,mrtC.hotelCurrencyCode,mrtc.exchangeDate,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) *  mrtC.hotelCurrencyExchangeRate, mrtC.hotelCurrencyDecimalPlaces-1
			) AS chargeValueInHotelCurrency,
			ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * mrtC.hotelCurrencyExchangeRate,mrtC.hotelCurrencyDecimalPlaces-1) AS roomRevenueInHotelCurrency,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * 	((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) * 1.00, 2
		) AS chargeValueInUSD,
		ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * 1.00, 2) AS roomRevenueInUSD,
		COALESCE
		(c.itemCodeOverride,CASE cl.classificationName
								WHEN 'Booking' THEN
								--if any of the non-source based criteria fields are anything other than the wildcard, this charge is a 'special booking'
									CASE 
										WHEN (clauses.RateCategoryCode <> '*' OR clauses.RateCode <> '*' OR clauses.TravelAgentGroupID <> 0)
										THEN mrtC.ItemCode + '_SB'
        								ELSE mrtC.ItemCode + '_B'
									END
								WHEN 'Commission' THEN mrtC.ItemCode + '_C'
								WHEN 'Surcharge' THEN mrtC.ItemCode + '_S'
								WHEN 'Non-Billable' THEN NULL
								WHEN 'I Prefer' THEN mrtC.ItemCode + '_I'
								ELSE NULL
							END
		) AS ItemCode,
		COALESCE(c.gpSiteOverride,mrtC.gpSiteID) AS gpSiteID,
		GETDATE(),NULL,NULL,
		loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged
	FROM BillingRules br
		INNER JOIN cte_clauses as clauses ON clauses.ClauseID = br.clauseID
		INNER JOIN Classifications cl ON cl.classificationID = br.classificationID
		INNER JOIN work.MrtForCalculation mrtC ON mrtC.transactionSourceID = clauses.transactionSourceID
			AND mrtc.transactionKey = clauses.transactionKey
			AND mrtc.runID = clauses.runID
		INNER JOIN work.[local_exchange_rates] brCE 
		ON br.currencyCode = brCE.CURNCYID 
			AND mrtC.exchangeDate = brCE.EXCHDATE
		INNER JOIN dbo.Criteria c ON c.criteriaID = clauses.CriteriaID
		LEFT JOIN dbo.ThresholdRules tr ON tr.clauseID = clauses.ClauseID
	WHERE clauses.runID = @RunID
		AND (
				mrtC.bookingStatus != 'Cancelled'
				OR
				(mrtC.bookingStatus = 'Cancelled' AND br.refundable = 0)
			)
		AND (
				(br.afterConfirmation = 1 AND mrtC.confirmationDate >= br.confirmationDate)
				OR
				(br.afterConfirmation = 0 AND mrtC.confirmationDate <= br.confirmationDate)
			)
		AND mrtC.billableDate BETWEEN br.startDate AND br.endDate
	------------------------------------------------------------------------------------


		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE c
		FROM dbo.Charges c
			INNER JOIN work.MrtForCalculation m
				ON c.transactionSourceID = m.transactionSourceID
				AND c.transactionKey = m.transactionKey
		WHERE m.runID = @RunID
		AND c.sopNumber IS NULL
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO dbo.Charges(runID,clauseID,billingRuleID,classificationID,transactionSourceID,transactionKey,confirmationNumber,hotelCode,collectionCode,
							clauseName,billableDate,arrivalDate,roomNights,hotelCurrencyCode,exchangeDate,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate,loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged)
		SELECT @RunID,clauseID,billingRuleID,classificationID,transactionSourceID,transactionKey,confirmationNumber,hotelCode,collectionCode,
							clauseName,billableDate,arrivalDate,roomNights,hotelCurrencyCode,exchangeDate,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate,loyaltyNumber, LoyaltyNumberValidated, LoyaltyNumberTagged
		FROM #CHARGES
		------------------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
