USE ETL
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 5/5/2020 9:45:59 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Queue]'
GO
ALTER TABLE [dbo].[Queue] DROP CONSTRAINT [DF_Queue_CreateDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Queue]'
GO
ALTER TABLE [dbo].[Queue] DROP CONSTRAINT [DF_dbo_Queue_AlertAcknowledged]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_Application_CreateDate] from [dbo].[Queue]'
GO
DROP INDEX [IX_QueueID_IC_Application_CreateDate] ON [dbo].[Queue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_ImportStarted_ImportFinished] from [dbo].[Queue]'
GO
DROP INDEX [IX_QueueID_IC_ImportStarted_ImportFinished] ON [dbo].[Queue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Queue]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_Queue]
(
[QueueID] [int] NOT NULL IDENTITY(1, 1),
[Application] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FilePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_Queue_CreateDate] DEFAULT (getdate()),
[QueueStatus] [tinyint] NOT NULL,
[QueueStatus_Desc] AS (case [QueueStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' when (3) then 'Error' when (4) then 'Resolved' else 'Error' end) PERSISTED NOT NULL,
[ImportStarted] [datetime] NULL,
[ImportFinished] [datetime] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportCount] [int] NULL,
[AlertAcknowledged] [bit] NULL CONSTRAINT [DF_dbo_Queue_AlertAcknowledged] DEFAULT ((0)),
[EmbeddedFileDate] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_Queue] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_1_Queue]([QueueID], [Application], [FilePath], [CreateDate], [QueueStatus], [ImportStarted], [ImportFinished], [Message], [ImportCount], [AlertAcknowledged], [EmbeddedFileDate]) SELECT [QueueID], [Application], [FilePath], [CreateDate], [QueueStatus], [ImportStarted], [ImportFinished], [Message], [ImportCount], [AlertAcknowledged], [EmbeddedFileDate] FROM [dbo].[Queue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_Queue] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Queue]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_Queue]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Queue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_Queue]', N'Queue', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_Application_CreateDate] on [dbo].[Queue]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_Application_CreateDate] ON [dbo].[Queue] ([QueueID]) INCLUDE ([Application], [CreateDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_ImportStarted_ImportFinished] on [dbo].[Queue]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_ImportStarted_ImportFinished] ON [dbo].[Queue] ([QueueID]) INCLUDE ([ImportFinished], [ImportStarted])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ETL_ApplicationImportStatus_Logging]'
GO


ALTER PROCEDURE [dbo].[ETL_ApplicationImportStatus_Logging]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-- UPDATE ETL STATUS --------------------------------------
	;WITH cte_Queue
	AS
	(
		SELECT q.[Application],q.FilePath,q.CreateDate,q.QueueStatus_Desc,q.[Message],q.ImportCount
		FROM dbo.[Queue] q
			INNER JOIN (SELECT [Application],MAX(QueueID) AS QueueID FROM dbo.[Queue] GROUP BY [Application]) maxQ ON maxQ.QueueID = q.QueueID
		
		UNION ALL

		SELECT 'IPExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM IPExport_LOG
		WHERE IPExport_LOG_ID IN(SELECT MAX(IPExport_LOG_ID) FROM IPExport_LOG)

		UNION ALL

		SELECT 'SabreExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM SabreExport_LOG
		WHERE SabreExport_LOG_ID IN(SELECT MAX(SabreExport_LOG_ID) FROM SabreExport_LOG)

		UNION ALL

		SELECT 'WebbMason' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM WebbMason_LOG
		WHERE WebbMason_LOG_ID IN(SELECT MAX(WebbMason_LOG_ID) FROM WebbMason_LOG)
	)
	UPDATE a
		SET ETL_LastRunDate = c.CreateDate
	FROM ETL.dbo.ETL_Applications a
		INNER JOIN cte_Queue c ON c.[Application] = a.[Application] AND (c.CreateDate > a.ETL_LastRunDate OR a.ETL_LastRunDate IS NULL)
	WHERE a.IsActive = 1
	-----------------------------------------------------------

	-- REPORT ETL STATUS --------------------------------------
	;WITH cte_Queue
	AS
	(
		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM [vw_SabreReconcile_ImportStatus]
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'SabreReconcile')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_Adara_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'Adara')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_CurrencyRate_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'CurrencyRate')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_Hyperdisk_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'Hyperdisk')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_iHamms_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'iHamms')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_IPCustProf_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPCustProf')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,LogStatus_Desc,CONVERT(nvarchar(MAX),[Message]) AS [Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_IPExport_ImportStatus
		WHERE IPExport_LOG_ID IN(SELECT MAX(IPExport_LOG_ID) FROM IPExport_LOG)

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPHotelPoint_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPHotelPointBilling')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPHotelRedemption_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPHotelRedemption')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPTaggedCustomers_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPTaggedCustomers')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPTransDetailRpt_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPTransDetailRpt')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_LuxLink_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'LuxLink')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_OpenHosp_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'OpenHosp')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_Sabre_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'Sabre')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,LogStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_SabreExport_ImportStatus
		WHERE SabreExport_LOG_ID IN(SELECT MAX(SabreExport_LOG_ID) FROM SabreExport_LOG)

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_SabreIATA_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'SabreIATA')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_custDirect_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'custDirect')
		
		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportCurrency_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportCurrency')
		
		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportHotel_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportHotel')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportPointTransaction_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportPointTransaction')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportReservation_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportReservation')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackCurrency_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackCurrency')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackHotel_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackHotel')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackPointTransaction_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackPointTransaction')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackReservation_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackReservation')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportMember_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportMember')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportPoint_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportPoint')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportRedemption_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportRedemption')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportReward_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportReward')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_CurrencyImporttoGP_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'CurrencyImporttoGP')
	)
	INSERT INTO [dbo].[ETL_ApplicationImportStatus_LOG]([Application],[LastImportDate],[FileUploadStatus],
														[LastImportStatus],[LastImportMessage],
														[LastQueueCount],[LastImportCount],[LastFinalCount],[FilePath],AlertAcknowledged)
	SELECT a.Application,c.CreateDate,CASE WHEN a.ETL_NextRunDate >= GETDATE() THEN 'ON TIME' ELSE 'LATE' END AS [File Upload Status],
			c.QueueStatus_Desc AS [Last Import Status],c.[Message] AS [Last Import Message],
			c.QueueCount AS [Last Queue Count],c.ImportCount AS [Last Import Count],c.FinalCount AS [Last FinalCount Count],c.FilePath,c.AlertAcknowledged
	FROM dbo.ETL_Applications a
		INNER JOIN cte_Queue c ON c.[Application] = a.[Application]
	WHERE a.IsActive = 1
	-----------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[qs_Populate_Reservations_Warehouse]'
GO


ALTER PROCEDURE [dbo].[qs_Populate_Reservations_Warehouse]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 * FROM dbo.QueueStep WHERE QueueStepStatus = 0 AND StepName = 'Reservations')
    BEGIN

		DECLARE @TruncateBeforeLoad bit = 0,@QueueID int = NULL,@StepID int = NULL,@application varchar(255), @QueueReservation int = NULL

		EXEC dbo.qs_ProcessTopQueueItem 'Reservations',@QueueID OUTPUT,@StepID OUTPUT

		SET @QueueReservation = @QueueID

		WHILE @QueueID IS NOT NULL
		BEGIN
			EXEC Reservations.dbo.Populate_Normalized_dbo @TruncateBeforeLoad,@QueueID,@StepID

			SET @QueueID = NULL

			EXEC dbo.qs_ProcessTopQueueItem 'Reservations',@QueueID OUTPUT,@StepID OUTPUT
		END

		EXEC [dbo].[Sabre_RunReport];

		SELECT @application = Application FROM ETL.dbo.Queue WHERE QueueID = @QueueReservation

		IF(@application = 'Sabre')
		BEGIN
			EXEC [ETL].[dbo].[EpsilonExport_LoadQueue] 'EpsilonExportReservation'
		END
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
