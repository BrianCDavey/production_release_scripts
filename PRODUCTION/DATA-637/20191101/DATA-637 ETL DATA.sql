USE ETL
GO

/*
Run this script on:

CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/1/2019 9:46:57 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Add rows to [dbo].[ServerFilePath]')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'FTP', 'sftp.preferredhotelgroup.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'PASSWORD', '0p$R0ck$')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'USERNAME', 'phg_itdevelopment')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'FTP', 'sftp.preferredhotelgroup.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'PASSWORD', '0p$R0ck$')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'USERNAME', 'phg_itdevelopment')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'DOWNLOAD', 'G:\Epsilon\EpsilonExportPointTransaction\')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'FTP', 'sftp.preferredhotelgroup.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'PASSWORD', '0p$R0ck$')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportPointTransaction', 'USERNAME', 'phg_itdevelopment')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'DOWNLOAD', 'G:\Epsilon\EpsilonExportReservation\')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'FTP', 'sftp.preferredhotelgroup.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'PASSWORD', '0p$R0ck$')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportReservation', 'USERNAME', 'phg_itdevelopment')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackCurrency', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackHotel', 'FILE PATTERN', 'C[_]PHG[_]STORE[_]BULK%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%.csv')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackHotel', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackPointTransaction', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonHandbackReservation', 'DOWNLOAD', 'G:\Epsilon\EpsilonHandbackReservation')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportMember', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportPoint', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportRedemption', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportReward', 'DOWNLOAD', 'G:\Epsilon\EpsilonImportReward')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonImportReward', 'FTP DIR', '/home/phg_epsilon/uploads')
PRINT(N'Operation applied to 28 rows out of 28')
COMMIT TRANSACTION
GO
