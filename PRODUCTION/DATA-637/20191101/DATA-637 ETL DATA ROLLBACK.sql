USE ETL
GO

/*
Run this script on:

CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/1/2019 9:48:17 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Delete rows from [dbo].[ServerFilePath]')
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'FTP' AND [FilePath] = 'sftp.preferredhotelgroup.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'PASSWORD' AND [FilePath] = '0p$R0ck$'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'USERNAME' AND [FilePath] = 'phg_itdevelopment'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'FTP' AND [FilePath] = 'sftp.preferredhotelgroup.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'PASSWORD' AND [FilePath] = '0p$R0ck$'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'USERNAME' AND [FilePath] = 'phg_itdevelopment'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\Epsilon\EpsilonExportPointTransaction\'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'FTP' AND [FilePath] = 'sftp.preferredhotelgroup.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'PASSWORD' AND [FilePath] = '0p$R0ck$'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportPointTransaction' AND [Section] = 'USERNAME' AND [FilePath] = 'phg_itdevelopment'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\Epsilon\EpsilonExportReservation\'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'FTP' AND [FilePath] = 'sftp.preferredhotelgroup.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'PASSWORD' AND [FilePath] = '0p$R0ck$'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportReservation' AND [Section] = 'USERNAME' AND [FilePath] = 'phg_itdevelopment'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackCurrency' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackHotel' AND [Section] = 'FILE PATTERN' AND [FilePath] = 'C[_]PHG[_]STORE[_]BULK%[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]%.csv'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackHotel' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackPointTransaction' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonHandbackReservation' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\Epsilon\EpsilonHandbackReservation'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportMember' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportPoint' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportRedemption' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportReward' AND [Section] = 'DOWNLOAD' AND [FilePath] = 'G:\Epsilon\EpsilonImportReward'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonImportReward' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
PRINT(N'Operation applied to 28 rows out of 28')
COMMIT TRANSACTION
GO
