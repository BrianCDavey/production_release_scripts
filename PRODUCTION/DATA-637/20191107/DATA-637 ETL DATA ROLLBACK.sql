USE ETL
GO

/*
Run this script on:

CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/7/2019 9:06:40 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Delete 13 rows from [dbo].[ETL_Applications]')
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'DailyStar'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonExportCurrency'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonExportHotel'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonExportPointTransaction'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonExportReservation'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonHandbackCurrency'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonHandbackHotel'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonHandbackPointTransaction'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonHandbackReservation'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonImportMember'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonImportPoint'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonImportRedemption'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'EpsilonImportReward'

PRINT(N'Delete and re-insert rows in [dbo].[ETL_Applications] due to identity row modification')
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'CurrencyRate'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_ABC'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_AMEX'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_BCD'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_CCRA'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_CWT'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_FCM'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_HRG'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_RADIUS'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_THOR'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_TRAVEL_TRANSPORT'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_ULTRAMAR'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IPDeletedMembers'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'SabreDaily'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'SabreReconcile'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-10-07 10:55:53.020' WHERE [Application] = 'Adara'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2018-10-18 01:15:24.953' WHERE [Application] = 'CurrencyImport'
UPDATE [dbo].[ETL_Applications] SET [ETL_FrequencyType]=3, [ETL_StartDate]='2018-02-26 17:39:20.350' WHERE [Application] = 'CustDirect'
UPDATE [dbo].[ETL_Applications] SET [IsActive]=0, [ETL_LastRunDate]='2018-05-08 14:51:23.743' WHERE [Application] = 'Hyperdisk'
UPDATE [dbo].[ETL_Applications] SET [IsActive]=0, [ETL_LastRunDate]='2019-03-14 08:21:02.867' WHERE [Application] = 'iHamms'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-07 03:07:04.033', [CountThresholdPCT]=0.001 WHERE [Application] = 'IPCustProf'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-07 02:31:33.563' WHERE [Application] = 'IPExport'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-05 13:33:23.353' WHERE [Application] = 'IPHotelPointBilling'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-05 13:35:46.123' WHERE [Application] = 'IPHotelRedemption'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-07 03:30:03.773' WHERE [Application] = 'IPTaggedCustomers'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-07 03:05:03.407' WHERE [Application] = 'IPTransDetailRpt'
UPDATE [dbo].[ETL_Applications] SET [IsActive]=0, [ETL_LastRunDate]='2018-08-20 12:25:01.893' WHERE [Application] = 'LuxLink'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-07 07:30:02.183' WHERE [Application] = 'OpenHosp'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-07 06:30:02.080' WHERE [Application] = 'Sabre'
UPDATE [dbo].[ETL_Applications] SET [ETL_StartDate]='2017-11-30 11:18:09.850', [ETL_LastRunDate]='2019-11-07 06:30:05.517' WHERE [Application] = 'SabreExport'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2019-11-04 09:14:01.907' WHERE [Application] = 'SabreIATA'
SET IDENTITY_INSERT [dbo].[ETL_Applications] ON
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('CurrencyRate',34,N'Currency Rate Import',1,NULL,1,1,'2018-08-22 10:48:50.720','2019-11-07 01:15:00.413',0,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_ABC',23,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-09-25 03:00:00.443',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_AMEX',24,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-09-20 03:00:00.950',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_BCD',25,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-08 03:00:02.167',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_CCRA',26,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-24 03:00:01.620',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_CWT',27,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-19 03:00:01.457',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_FCM',28,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-09 03:00:02.603',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_HRG',29,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2018-09-05 03:00:05.087',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_RADIUS',30,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-19 03:00:02.270',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_THOR',31,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-08 03:00:04.400',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_TRAVEL_TRANSPORT',32,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227','2019-10-08 03:00:04.853',NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_ULTRAMAR',33,N'IATA Import',1,NULL,1,3,'2018-07-11 12:06:46.227',NULL,NULL,1)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IPDeletedMembers',35,N'IPrefer Deleted Members',1,NULL,1,3,'2018-08-01 00:00:00.000','2019-11-01 02:00:02.840',NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('SabreDaily',36,N'Sabre Daily FTP Import',1,NULL,1,1,'2018-12-17 17:05:54.513','2019-11-07 08:26:01.910',0.8,NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('SabreReconcile',22,N'Sabre Reconcile Import',1,NULL,1,3,'2018-05-06 23:59:59.000','2019-11-02 00:00:03.123',1,0)
SET IDENTITY_INSERT [dbo].[ETL_Applications] OFF
PRINT(N'Operation applied to 15 rows out of 31')

PRINT(N'Add 1 row to [dbo].[ETL_Applications]')
SET IDENTITY_INSERT [dbo].[ETL_Applications] ON
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('IPCustProf_Daily', 19, N'IPrefer Customer Profile Import', 0, NULL, 1, 1, '2017-09-11 10:00:00.000', NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[ETL_Applications] OFF
COMMIT TRANSACTION
GO
