USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/7/2019 9:00:24 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonImportReward_ImportStatus]'
GO


--EpsilonImportReward FTP Import
CREATE VIEW [dbo].[vw_EpsilonImportReward_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonReward] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonImportReward'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonImportRedemption_ImportStatus]'
GO


--EpsilonImportRedemption FTP Import
CREATE VIEW [dbo].[vw_EpsilonImportRedemption_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonRedemption] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonImportRedemption'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonImportPoint_ImportStatus]'
GO


--EpsilonImportPoint FTP Import
CREATE VIEW [dbo].[vw_EpsilonImportPoint_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonPoint] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonImportPoint'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonImportMember_ImportStatus]'
GO


--EpsilonImportMember FTP Import
CREATE VIEW [dbo].[vw_EpsilonImportMember_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonMember] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonImportMember'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonHandbackReservation_ImportStatus]'
GO


--EpsilonHandbackReservation FTP Import
CREATE VIEW [dbo].[vw_EpsilonHandbackReservation_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonExportReservationHandBack] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonHandbackReservation'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonHandbackPointTransaction_ImportStatus]'
GO


--EpsilonHandbackPointTransaction FTP Import
CREATE VIEW [dbo].[vw_EpsilonHandbackPointTransaction_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonExportPointTransactionHandBack] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonHandbackPointTransaction'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonHandbackHotel_ImportStatus]'
GO


--EpsilonHandbackHotel FTP Import
CREATE VIEW [dbo].[vw_EpsilonHandbackHotel_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonExportHotelHandBack] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonHandbackHotel'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonHandbackCurrency_ImportStatus]'
GO


--EpsilonHandbackCurrency FTP Import
CREATE VIEW [dbo].[vw_EpsilonHandbackCurrency_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].[Import_EpsilonExportCurrencyHandBack] WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonHandbackCurrency'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonExportReservation_ImportStatus]'
GO


--EpsilonExportReservation FTP Import
CREATE VIEW [dbo].[vw_EpsilonExportReservation_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].EpsilonExportReservation WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonExportReservation'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonExportPointTransaction_ImportStatus]'
GO


--EpsilonExportPointTransaction FTP Import
CREATE VIEW [dbo].[vw_EpsilonExportPointTransaction_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].EpsilonExportPointTransaction WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonExportPointTransaction'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonExportHotel_ImportStatus]'
GO


--EpsilonExportHotel FTP Import
CREATE VIEW [dbo].[vw_EpsilonExportHotel_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].EpsilonExportHotel WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonExportHotel'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[vw_EpsilonExportCurrency_ImportStatus]'
GO
CREATE VIEW [dbo].[vw_EpsilonExportCurrency_ImportStatus]
AS
	SELECT q.QueueID,[Application],FilePath,CreateDate,QueueStatus_Desc,[Message],ImportCount AS QueueCount,
			(SELECT COUNT(*) FROM ETL.[dbo].EpsilonExportCurrency WHERE QueueID = q.QueueID) AS ImportCount,
			NULL AS FinalCount,
			AlertAcknowledged
	FROM dbo.[Queue] q
	WHERE [Application] = 'EpsilonExportCurrency'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ETL_ApplicationImportStatus_Logging]'
GO


ALTER PROCEDURE [dbo].[ETL_ApplicationImportStatus_Logging]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-- UPDATE ETL STATUS --------------------------------------
	;WITH cte_Queue
	AS
	(
		SELECT q.[Application],q.FilePath,q.CreateDate,q.QueueStatus_Desc,q.[Message],q.ImportCount
		FROM dbo.[Queue] q
			INNER JOIN (SELECT [Application],MAX(QueueID) AS QueueID FROM dbo.[Queue] GROUP BY [Application]) maxQ ON maxQ.QueueID = q.QueueID
		
		UNION ALL

		SELECT 'IPExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM IPExport_LOG
		WHERE IPExport_LOG_ID IN(SELECT MAX(IPExport_LOG_ID) FROM IPExport_LOG)

		UNION ALL

		SELECT 'SabreExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM SabreExport_LOG
		WHERE SabreExport_LOG_ID IN(SELECT MAX(SabreExport_LOG_ID) FROM SabreExport_LOG)
	)
	UPDATE a
		SET ETL_LastRunDate = c.CreateDate
	FROM ETL.dbo.ETL_Applications a
		INNER JOIN cte_Queue c ON c.[Application] = a.[Application] AND (c.CreateDate > a.ETL_LastRunDate OR a.ETL_LastRunDate IS NULL)
	WHERE a.IsActive = 1
	-----------------------------------------------------------

	-- REPORT ETL STATUS --------------------------------------
	;WITH cte_Queue
	AS
	(
		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM [vw_SabreReconcile_ImportStatus]
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'SabreReconcile')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_Adara_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'Adara')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_CurrencyRate_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'CurrencyRate')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_Hyperdisk_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'Hyperdisk')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_iHamms_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'iHamms')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_IPCustProf_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPCustProf')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,LogStatus_Desc,CONVERT(nvarchar(MAX),[Message]) AS [Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM vw_IPExport_ImportStatus
		WHERE IPExport_LOG_ID IN(SELECT MAX(IPExport_LOG_ID) FROM IPExport_LOG)

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPHotelPoint_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPHotelPointBilling')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPHotelRedemption_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPHotelRedemption')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPTaggedCustomers_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPTaggedCustomers')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_IPTransDetailRpt_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'IPTransDetailRpt')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_LuxLink_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'LuxLink')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_OpenHosp_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'OpenHosp')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_Sabre_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'Sabre')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,LogStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_SabreExport_ImportStatus
		WHERE SabreExport_LOG_ID IN(SELECT MAX(SabreExport_LOG_ID) FROM SabreExport_LOG)

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_SabreIATA_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'SabreIATA')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_custDirect_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'custDirect')
		
		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportCurrency_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportCurrency')
		
		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportHotel_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportHotel')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportPointTransaction_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportPointTransaction')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonExportReservation_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonExportReservation')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackCurrency_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackCurrency')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackHotel_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackHotel')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackPointTransaction_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackPointTransaction')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonHandbackReservation_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonHandbackReservation')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportMember_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportMember')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportPoint_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportPoint')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportRedemption_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportRedemption')

		UNION ALL

		SELECT [Application],FilePath,CreateDate,QueueStatus_Desc,[Message],QueueCount,ImportCount,FinalCount,AlertAcknowledged
		FROM dbo.vw_EpsilonImportReward_ImportStatus
		WHERE QueueID IN(SELECT MAX(QueueID) FROM [Queue] WHERE [Application] = 'EpsilonImportReward')
	)
	INSERT INTO [dbo].[ETL_ApplicationImportStatus_LOG]([Application],[LastImportDate],[FileUploadStatus],
														[LastImportStatus],[LastImportMessage],
														[LastQueueCount],[LastImportCount],[LastFinalCount],[FilePath],AlertAcknowledged)
	SELECT a.Application,c.CreateDate,CASE WHEN a.ETL_NextRunDate >= GETDATE() THEN 'ON TIME' ELSE 'LATE' END AS [File Upload Status],
			c.QueueStatus_Desc AS [Last Import Status],c.[Message] AS [Last Import Message],
			c.QueueCount AS [Last Queue Count],c.ImportCount AS [Last Import Count],c.FinalCount AS [Last FinalCount Count],c.FilePath,c.AlertAcknowledged
	FROM dbo.ETL_Applications a
		INNER JOIN cte_Queue c ON c.[Application] = a.[Application]
	WHERE a.IsActive = 1
	-----------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
