USE ETL
GO

/*
Run this script on:

CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/7/2019 9:05:32 AM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Delete 1 row from [dbo].[ETL_Applications]')
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IPCustProf_Daily'

PRINT(N'Delete and re-insert rows in [dbo].[ETL_Applications] due to identity row modification')
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'CurrencyRate'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_ABC'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_AMEX'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_BCD'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_CCRA'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_CWT'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_FCM'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_HRG'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_RADIUS'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_THOR'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_TRAVEL_TRANSPORT'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IATA_ULTRAMAR'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'IPDeletedMembers'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'SabreDaily'
DELETE FROM [dbo].[ETL_Applications] WHERE [Application] = 'SabreReconcile'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-01 10:30:23.990' WHERE [Application] = 'Adara'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-28 01:15:03.147' WHERE [Application] = 'CurrencyImport'
UPDATE [dbo].[ETL_Applications] SET [ETL_FrequencyType]=1, [ETL_StartDate]='2018-01-23 13:16:09.800' WHERE [Application] = 'CustDirect'
UPDATE [dbo].[ETL_Applications] SET [IsActive]=1, [ETL_LastRunDate]='2017-11-03 12:00:00.153' WHERE [Application] = 'Hyperdisk'
UPDATE [dbo].[ETL_Applications] SET [IsActive]=1, [ETL_LastRunDate]='2017-11-14 11:29:59.823' WHERE [Application] = 'iHamms'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-27 04:20:18.013', [CountThresholdPCT]=1 WHERE [Application] = 'IPCustProf'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-28 02:32:05.233' WHERE [Application] = 'IPExport'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-06 16:39:15.173' WHERE [Application] = 'IPHotelPointBilling'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-06 12:37:03.190' WHERE [Application] = 'IPHotelRedemption'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2018-01-11 10:16:54.940' WHERE [Application] = 'IPTaggedCustomers'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2018-01-09 13:16:17.687' WHERE [Application] = 'IPTransDetailRpt'
UPDATE [dbo].[ETL_Applications] SET [IsActive]=1, [ETL_LastRunDate]='2017-11-27 11:25:02.170' WHERE [Application] = 'LuxLink'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-28 07:30:02.283' WHERE [Application] = 'OpenHosp'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-28 06:30:46.730' WHERE [Application] = 'Sabre'
UPDATE [dbo].[ETL_Applications] SET [ETL_StartDate]='2017-11-30 10:09:56.140', [ETL_LastRunDate]=NULL WHERE [Application] = 'SabreExport'
UPDATE [dbo].[ETL_Applications] SET [ETL_LastRunDate]='2017-11-02 15:30:03.397' WHERE [Application] = 'SabreIATA'
SET IDENTITY_INSERT [dbo].[ETL_Applications] ON
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('CurrencyRate',2033,N'Currency Rate Import',1,NULL,1,1,'2018-08-22 09:48:41.863',NULL,0,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_ABC',2022,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_AMEX',2023,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_BCD',2024,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_CCRA',2025,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_CWT',2026,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_FCM',2027,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_HRG',2028,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_RADIUS',2029,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_THOR',2030,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_TRAVEL_TRANSPORT',2031,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IATA_ULTRAMAR',2032,N'IATA Import',1,NULL,1,3,'2018-07-02 16:52:31.680',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('IPDeletedMembers',2034,N'IPrefer Deleted Members',1,NULL,1,3,'2018-08-01 00:00:00.000',NULL,NULL,0)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('SabreDaily',3033,N'Sabre Daily FTP Import',1,NULL,1,1,'2018-12-17 16:05:31.900',NULL,0.8,NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application],[ETL_ID],[JobName],[IsActive],[DocumentationPath],[ETL_FrequencyInterval],[ETL_FrequencyType],[ETL_StartDate],[ETL_LastRunDate],[CountThresholdPCT],[EmbeddedFileDateOrderSensitive]) VALUES ('SabreReconcile',1022,N'Sabre Reconcile Import',1,NULL,1,3,'2018-05-06 23:59:59.000',NULL,1,0)
SET IDENTITY_INSERT [dbo].[ETL_Applications] OFF
PRINT(N'Operation applied to 15 rows out of 31')

PRINT(N'Add 13 rows to [dbo].[ETL_Applications]')
SET IDENTITY_INSERT [dbo].[ETL_Applications] ON
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('DailyStar', 23, N'Daily Star', 1, NULL, 1, 2, '2018-04-29 00:00:00.000', NULL, NULL, 0)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonExportCurrency', 3034, N'Epsilon Export Currency', 1, NULL, 1, 1, NULL, '2019-10-28 14:45:54.903', NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonExportHotel', 3035, N'Epsilon Export Hotel', 1, NULL, 1, 1, NULL, '2019-10-28 15:01:27.030', NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonExportPointTransaction', 3036, N'Epsilon Export Point Transaction', 1, NULL, 1, 1, NULL, '2019-10-28 14:49:27.587', NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonExportReservation', 3037, N'Epsilon Export Reservation', 1, NULL, 1, 1, NULL, '2019-10-28 14:45:39.570', NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonHandbackCurrency', 3038, N'Epsilon Export Handback', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonHandbackHotel', 3039, N'Epsilon Export Handback', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonHandbackPointTransaction', 3040, N'Epsilon Export Handback', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonHandbackReservation', 3041, N'Epsilon Export Handback', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonImportMember', 3042, N'EpsilonExportCurrency', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonImportPoint', 3043, N'EpsilonExportCurrency', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonImportRedemption', 3044, N'EpsilonExportCurrency', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
INSERT INTO [dbo].[ETL_Applications] ([Application], [ETL_ID], [JobName], [IsActive], [DocumentationPath], [ETL_FrequencyInterval], [ETL_FrequencyType], [ETL_StartDate], [ETL_LastRunDate], [CountThresholdPCT], [EmbeddedFileDateOrderSensitive]) VALUES ('EpsilonImportReward', 3045, N'EpsilonExportCurrency', 0, NULL, 1, 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ETL_Applications] OFF
COMMIT TRANSACTION
GO
