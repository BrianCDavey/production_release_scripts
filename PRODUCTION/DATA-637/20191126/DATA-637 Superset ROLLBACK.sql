USE Superset
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/26/2019 1:39:13 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport03]'
GO





-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport03]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
'03' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, CAST(ROW_NUMBER()OVER(ORDER BY Iprefer_Number) AS varchar(200)) + 'test@test.com' AS EmailAddress
--, Email AS EmailAddress
, NULL AS EmailId
, 'True' AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, Import_Date AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, CAST(ROW_NUMBER()OVER(ORDER BY Iprefer_Number) AS varchar(200)) + 'test@test.com' AS SrcEmailAddr
--, Email AS SrcEmailAddr
, NULL AS ContactPointId
, UPPER(IPrefer_Number) AS InteractionId
, NULL AS EmailType
, NULL AS SpamStatus
, NULL AS EmailFormatPref
, 'ALMACCT' AS AccountSourceCode
, UPPER(IPrefer_Number) AS SourceAccountNumber
, 'ALM_BRAND' AS BrandOrgCode
, NULL AS JsonExternalData
FROM [Superset].[BSI].[All_Member_Report_Import]
UNION ALL

	SELECT
'03' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, CAST(ROW_NUMBER()OVER(ORDER BY cpi.Iprefer_Number) AS varchar(200)) + 'test@test.com' AS EmailAddress
--, Email AS EmailAddress
, NULL AS EmailId
, 'True' AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, cpi.Import_Date AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, CAST(ROW_NUMBER()OVER(ORDER BY cpi.Iprefer_Number) AS varchar(200)) + 'test@test.com' AS SrcEmailAddr
--, Email AS SrcEmailAddr
, NULL AS ContactPointId
, UPPER(cpi.IPrefer_Number) AS InteractionId
, NULL AS EmailType
, NULL AS SpamStatus
, NULL AS EmailFormatPref
, 'ALMACCT' AS AccountSourceCode
, UPPER(cpi.IPrefer_Number) AS SourceAccountNumber
, 'ALM_BRAND' AS BrandOrgCode
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import] cpi
LEFT JOIN Superset.BSI.All_Member_Report_Import amri
ON cpi.iPrefer_Number = amri.iPrefer_Number
WHERE amri.iPrefer_Number IS NULL
AND cpi.iPrefer_Number IN (SELECT iPrefer_Number FROM Superset.BSI.TransactionDetailedReport)
;


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
