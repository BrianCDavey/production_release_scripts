USE ETL
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/29/2019 1:56:02 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Import_EpsilonExportCurrencyHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportCurrencyHandBack] DROP CONSTRAINT [PK_dbo_Import_EpsilonExportCurrencyHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Import_EpsilonExportHotelHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportHotelHandBack] DROP CONSTRAINT [PK_dbo_Import_EpsilonExportHotelHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Import_EpsilonExportPointTransactionHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportPointTransactionHandBack] DROP CONSTRAINT [PK_dbo_Import_EpsilonExportPointTransactionHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Import_EpsilonExportReservationHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportReservationHandBack] DROP CONSTRAINT [PK_dbo_Import_EpsilonExportReservationHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonExportReservationHandBack]'
GO
DROP TABLE [dbo].[Import_EpsilonExportReservationHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonExportPointTransactionHandBack]'
GO
DROP TABLE [dbo].[Import_EpsilonExportPointTransactionHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonExportHotelHandBack]'
GO
DROP TABLE [dbo].[Import_EpsilonExportHotelHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonExportCurrencyHandBack]'
GO
DROP TABLE [dbo].[Import_EpsilonExportCurrencyHandBack]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[qs_Populate_Reservations_Warehouse]'
GO

ALTER PROCEDURE [dbo].[qs_Populate_Reservations_Warehouse]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 * FROM dbo.QueueStep WHERE QueueStepStatus = 0)
    BEGIN

		DECLARE @TruncateBeforeLoad bit = 0,@QueueID int = NULL,@StepID int = NULL,@application varchar(255)

		EXEC dbo.qs_ProcessTopQueueItem @QueueID OUTPUT,@StepID OUTPUT

		WHILE @QueueID IS NOT NULL
		BEGIN
			EXEC Reservations.dbo.Populate_Normalized_dbo @TruncateBeforeLoad,@QueueID,@StepID

			SET @QueueID = NULL

			EXEC dbo.qs_ProcessTopQueueItem @QueueID OUTPUT,@StepID OUTPUT
		END

		SELECT @application = Application FROM ETL..Queue WHERE QueueID = @QueueID

		IF(@application = 'Sabre')
		BEGIN
			EXEC [ETL].[dbo].[EpsilonExport_LoadQueue] 'EpsilonExportReservation'
		END
/*
		WHILE 1 = 1
		BEGIN
			IF EXISTS
				(
					SELECT *
					FROM msdb.dbo.sysjobs j
						INNER JOIN msdb.dbo.sysjobactivity ja ON ja.job_id = j.job_id
					WHERE j.name = N'Populate Dimensional Warehouse'
						AND ja.run_requested_date IS NOT NULL
						AND ja.stop_execution_date IS NOT NULL
				)
			BEGIN
				EXEC msdb.dbo.sp_start_job @job_name = N'Populate Dimensional Warehouse'

				BREAK;
			END
			ELSE
			BEGIN
				WAITFOR DELAY '00:01';
			END
		END
*/
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
