USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/29/2019 1:57:02 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[qs_Populate_Reservations_Warehouse]'
GO


ALTER PROCEDURE [dbo].[qs_Populate_Reservations_Warehouse]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 * FROM dbo.QueueStep WHERE QueueStepStatus = 0)
    BEGIN

		DECLARE @TruncateBeforeLoad bit = 0,@QueueID int = NULL,@StepID int = NULL,@application varchar(255), @QueueReservation int = NULL

		EXEC dbo.qs_ProcessTopQueueItem @QueueID OUTPUT,@StepID OUTPUT

		SET @QueueReservation = @QueueID

		WHILE @QueueID IS NOT NULL
		BEGIN
			EXEC Reservations.dbo.Populate_Normalized_dbo @TruncateBeforeLoad,@QueueID,@StepID

			SET @QueueID = NULL

			EXEC dbo.qs_ProcessTopQueueItem @QueueID OUTPUT,@StepID OUTPUT
		END

		SELECT @application = Application FROM ETL..Queue WHERE QueueID = @QueueReservation

		IF(@application = 'Sabre')
		BEGIN
			EXEC [ETL].[dbo].[EpsilonExport_LoadQueue] 'EpsilonExportReservation'
		END
/*
		WHILE 1 = 1
		BEGIN
			IF EXISTS
				(
					SELECT *
					FROM msdb.dbo.sysjobs j
						INNER JOIN msdb.dbo.sysjobactivity ja ON ja.job_id = j.job_id
					WHERE j.name = N'Populate Dimensional Warehouse'
						AND ja.run_requested_date IS NOT NULL
						AND ja.stop_execution_date IS NOT NULL
				)
			BEGIN
				EXEC msdb.dbo.sp_start_job @job_name = N'Populate Dimensional Warehouse'

				BREAK;
			END
			ELSE
			BEGIN
				WAITFOR DELAY '00:01';
			END
		END
*/
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonExportCurrencyHandBack]'
GO
CREATE TABLE [dbo].[Import_EpsilonExportCurrencyHandBack]
(
[Import_EpsilonExportCurrencyHandBackID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NULL,
[BaseCurrencyCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetCurrencyCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rate] [decimal] (18, 9) NOT NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EndDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JsonExternalData] [nvarchar] (71) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ERROR_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ERROR_DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Import_EpsilonExportCurrencyHandBack] on [dbo].[Import_EpsilonExportCurrencyHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportCurrencyHandBack] ADD CONSTRAINT [PK_dbo_Import_EpsilonExportCurrencyHandBack] PRIMARY KEY CLUSTERED  ([Import_EpsilonExportCurrencyHandBackID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonExportHotelHandBack]'
GO
CREATE TABLE [dbo].[Import_EpsilonExportHotelHandBack]
(
[Import_EpsilonExportHotelHandBackID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NULL,
[StoreCode] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChainPriority] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChainName] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FranchiseCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentStoreCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine1] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine2] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Urbanization] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gaid] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionLevel] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreType] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerFirstName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerMiddleInitial] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerLastName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerFullName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerEmailAddr] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DMA] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DivCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreOpenDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreCloseDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemodelDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HierarchyCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrandOrgCode] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClientFileId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientFileRecordNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JsonExternalData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeofenceRadius] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreConfiguration] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreCompFlag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreTelexNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreFaxNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompStartDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompEndDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreSizeBand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorDescription] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CorrelationId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Import_EpsilonExportHotelHandBack] on [dbo].[Import_EpsilonExportHotelHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportHotelHandBack] ADD CONSTRAINT [PK_dbo_Import_EpsilonExportHotelHandBack] PRIMARY KEY CLUSTERED  ([Import_EpsilonExportHotelHandBackID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonExportPointTransactionHandBack]'
GO
CREATE TABLE [dbo].[Import_EpsilonExportPointTransactionHandBack]
(
[Import_EpsilonExportPointTransactionHandBackID] [int] NOT NULL IDENTITY(1, 1),
[01] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileRecordNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessErrors] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AuditErrors] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportStatus] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_TRANSACTION_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROFILE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARD_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANS_DESCRIPTION] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[END_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_NET_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ELIGIBLE_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POST_SALES_ADJUSTMNT_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEVICE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEVICE_USERID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AUTHORIZATION_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_TRANSACTION_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SKIP_MOMENT_ENGINE_IND] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_CODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACCT_SRC_CODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SRC_ACCOUNT_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIVITY_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_FILE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_FILE_REC_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRAND_ORG_CODE] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ASSOCIATE_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GROSS_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NET_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_GROSS_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_SEQ_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_SOURCE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_SUBTYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANS_LOCATION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[blank1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROGRAM_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EXCHANGE_RATE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SRC_CUSTOMER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISC_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_CHANNEL_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_BUSINESS_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROMO_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUYER_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMB_METHOD_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMB_VENDOR_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMB_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[blank2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_NET_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_TAX_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_POST_SALES_ADJ_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_ELIG_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_STORE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_TRANSACTION_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_TRANSACTION_END_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUSPEND_REASON] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUSPEND_TRANSACTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JSON_EXTERNAL_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTING_KEY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTING_KEY_VALUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QueueID] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Import_EpsilonExportPointTransactionHandBack] on [dbo].[Import_EpsilonExportPointTransactionHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportPointTransactionHandBack] ADD CONSTRAINT [PK_dbo_Import_EpsilonExportPointTransactionHandBack] PRIMARY KEY CLUSTERED  ([Import_EpsilonExportPointTransactionHandBackID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonExportReservationHandBack]'
GO
CREATE TABLE [dbo].[Import_EpsilonExportReservationHandBack]
(
[Import_EpsilonExportReservationHandBackID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NULL,
[01] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESERVATION_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACT_RESERVATION_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROFILE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARD_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_TYPE_CODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RESERVATION_DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ARRIVAL_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DEPARTURE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESERVATION_REVENUE] [decimal] (18, 2) NULL,
[ELIGIBLE_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TAX_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POST_SALES_ADJUSTMNT_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEVICE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DEVICE_USERID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AUTHORIZATION_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_RESERVATION_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SKIP_MOMENT_ENGINE_IND] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CURRENCY_CODE] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACCT_SRC_CODE] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SRC_ACCOUNT_NUM] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIVITY_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_FILE_ID] [int] NOT NULL,
[CLIENT_FILE_REC_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BRAND_ORG_CODE] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ASSOCIATE_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GROSS_AMOUNT] [decimal] (18, 2) NULL,
[NET_AMT] [decimal] (18, 2) NULL,
[PROG_CURR_GROSS_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_SEQ_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_SOURCE_CODE] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TXN_SUBTYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANS_LOCATION] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[blank1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROGRAM_CODE] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EXCHANGE_RATE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SRC_CUSTOMER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISC_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RESERVATION_CHANNEL_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BOOKING_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROMO_CODE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUYER_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMB_METHOD_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMB_VENDOR_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RMB_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[blank2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_RES_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_TAX_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_POST_SALES_ADJ_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_ELIG_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROG_CURR_GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_STORE_CODE] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_RESERVATION_ARRIVAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_RESERVATION_DEPARTURE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUSPEND_REASON] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SUSPEND_RESERVATION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTING_KEY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTING_KEY_VALUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JSON_EXTERNAL_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRS_SOURCE] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CARD_NUMBER_SRC] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GUEST_FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUEST_LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUEST_EMAIL_ADDRESS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ERROR_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ERROR_DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Import_EpsilonExportReservationHandBack] on [dbo].[Import_EpsilonExportReservationHandBack]'
GO
ALTER TABLE [dbo].[Import_EpsilonExportReservationHandBack] ADD CONSTRAINT [PK_dbo_Import_EpsilonExportReservationHandBack] PRIMARY KEY CLUSTERED  ([Import_EpsilonExportReservationHandBackID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
