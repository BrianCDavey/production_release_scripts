USE Superset
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/19/2019 2:39:58 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [BSI].[Cancelled_Certificates_Staging]'
GO
DROP TABLE [BSI].[Cancelled_Certificates_Staging]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [BSI].[LogQueryStatusReport_Staging]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [BSI].[LogQueryStatusReport_Staging] ALTER COLUMN [Query Text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_InquiriesExport]'
GO



-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-09
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_InquiriesExport]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT
[Member Number] AS CardNumber
,[Reference Number] AS CaseId
,'Member' AS Category
,L.Reason AS Reason
,CAST([Date of Query] AS DATE) AS CreateDate
,CAST(NULLIF([Date of Query Closed],'') AS DATE) AS UpdateDate
,'False' AS Critical
,[Query Logged By] AS CreateUser
,REPLACE(REPLACE([Query Text],CHAR(10),''),CHAR(13),'') AS Comments
FROM [Superset].BSI.[LogQueryStatusReport_Staging] S
LEFT JOIN Superset.BSI.LoyaltyPrimeSubEpsilonReasonMapping L 
ON S.[Type of sub query] = L.[Type of sub query]


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport01]'
GO











-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport01]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

;WITH firstActivity AS (
	SELECT ln.loyaltyNumber, MIN(ts.confirmationDate) as minDate
	FROM Reservations.dbo.Transactions t
	JOIN Reservations.dbo.TransactionStatus ts
		ON t.TransactionStatusID = ts.TransactionStatusID
	JOIN Reservations.dbo.LoyaltyNumber ln
		ON t.LoyaltyNumberID = ln.LoyaltyNumberID
	GROUP BY ln.loyaltyNumber
)
,jed AS (
		SELECT 
			cpi.iprefer_number
			,REPLACE(
				--CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
					(SELECT * FROM 
							(
								SELECT
									ISNULL(NULLIF(TRIM(mtms.[Member Type]),''),'STDMEMBER') AS MemberType
									,[Current_Points_Balance] AS PointBalance
									,CAST(GETDATE() AS DATETIME) AS TierDate
									,CASE WHEN mtms.[Source vs Promotion] = 'Source'
									THEN mtms.[Promo Code]
									ELSE '' END AS SubSourceCode
									,CAST(ActivityDate AS DATETIME) AS LastActivityDate
									,'' AS CampaignCode
									,CASE WHEN mtms.[Source vs Promotion] = 'promotion'
									THEN mtms.[Promo Code]
									ELSE '' END AS PromoCode
									,LEFT(Hotlisted,1) AS Hotlisted
									,CAST(DATEADD(YEAR,1,GETDATE()) AS DATETIME) AS TierExpire
									,'TRUE' AS BypassScriptingInd
								FROM [Superset].[BSI].[All_Member_Report_Import] cpi2
								LEFT JOIN [Superset].[BSI].[Member_Type_Matching_Staging] mtms ON cpi2.PromoCode = mtms.[Promo Code]
								WHERE cpi.iprefer_number = cpi2.iprefer_number
					
								UNION ALL
					
								SELECT '' AS MemberType
									,'' AS PointBalance
									,'' AS TierDate
									,'' AS SubSourceCode
									,'' AS LastActivityDate
									,'' AS CampaignCode
									,'' AS PromoCode
									,'' AS Hotlisted
									,'' AS TierExpire
									,'TRUE' AS BypassScriptingInd
								WHERE NOT EXISTS (SELECT 1
									FROM [Superset].[BSI].[All_Member_Report_Import] cpi2
									WHERE cpi.iprefer_number = cpi2.iprefer_number
								)
							) as sub 
							FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
						)
			--)
			,'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',') as jsonExternalData
					FROM [Superset].[BSI].[All_Member_Report_Import] cpi

	)
SELECT 
	'01' AS RecordNumber,
	NULL AS ProfileId,
	cpi.Membership_Date AS JoinDate,
	NULL AS CompanyName,
	BirthDate AS BirthDate,
	NULL AS Gender,
	NULL AS MaritalStatus,
	NULL AS LanguageCode,
	'Marketo' AS GlobalOptSource,
	NULL AS GlobalOptDate,
	NULL AS GlobalOptOut,
	NULL AS Suffix,
	cpi.Last_Name AS LastName,
	NULLIF(LEFT(TRIM(cpi.middle_name),1),'') AS MiddleInit,
	cpi.First_Name AS FirstName,
	NULL AS Prefix,
	cpi.iPrefer_Number AS ClientAccountId,
	CASE cpi.Member_Status WHEN 'DISABLED' THEN 'I' ELSE 'A' END AS [Status],
	'ALMACCT' AS AccountSourceCode,
	cpi.iPrefer_Number AS SourceAccountNumber,
	ISNULL(cpi.Disabled_Date,cpi.Import_Date) AS ActivityDate,
	NULL AS CreateFileId,
	NULL AS CreateRecordNumber,
	NULL AS SrcPrefix,
	cpi.First_Name AS SrcFirstName,
	NULLIF(TRIM(cpi.Middle_Name),'') AS SrcMiddleInit,
	cpi.Last_Name AS SrcLastName,
	NULL AS SrcSuffix,
	NULL AS SrcGender,
	NULL AS SrcUnparsedName,
	NULL AS SrcFirmName,
	'ALM_BRAND' AS BrandOrgCode,
	cpi.iPrefer_Number AS InteractionId,
	NULL AS PrefChannelCode,
	NULL AS Title,
	cou.code3 AS CountryCode,
	NULL AS LanguageReqInd,
	NULL AS DwellingCode,
	NULL AS SalutationId,
	NULL AS TitleCode,
	NULL AS MaturitySuffix,
	NULLIF(TRIM(cpi.Middle_Name),'') AS MiddleName,
	fa.minDate AS OrigActivityDate,
	NULL AS ProfessionalSuffix,
	NULL AS OwnershipCode,
	NULL AS StmtCycle,
	NULL AS ClientLastUpDateDate,
	cpi.iPrefer_Number AS CardNumber,
	cpi.Disabled_Date AS EndDate,
	NULL AS TierReasonCode,
	UPPER(cpi.Tier) AS TierCode,
	--'BATCH' AS EnrollChannelCode,
	ISNULL(ecm.[Epsilon Channel Code],'BATCH') AS EnrollChannelCode,
	NULL AS StatusChangeReason,
	NULL AS EnrollmentStatus,
	--'MIGRATION' AS SourceCode,
	ISNULL(esm.[Enrollment Source Code],'MIGRATION') AS SourceCode,
	NULL AS ProductName,
	'PHG' AS ProgramCode,
	NULL AS ProductLevelCode,
	NULL AS VulGarInd,
	NULL AS ErrorCode,
	NULL AS ErrorDescription,
	NULL AS CorrelationId,
	jed.jsonExternalData AS JsonExternalData,
	NULLIF(TRIM(cpi.SignedupHotelCode),'') AS EnrollmentStoreCode,
	h.HotelName AS EnrollmentStoreName

FROM [Superset].[BSI].[All_Member_Report_Import] cpi
LEFT JOIN firstActivity fa
	ON cpi.iPrefer_Number = fa.loyaltyNumber
LEFT JOIN jed
	ON cpi.iPrefer_Number = jed.iPrefer_Number
LEFT JOIN Hotels.dbo.Hotel h
	ON cpi.SignedupHotelCode = h.HotelCode
LEFT JOIN iso.[dbo].[countries] cou
	ON cou.shortName = cpi.Country
LEFT JOIN [Superset].[BSI].Customer_Profile_Import cpi2
	ON cpi.iPrefer_Number = cpi2.iPrefer_Number
LEFT JOIN (SELECT MIN([Enrollment Source Code]) AS [Enrollment Source Code] , [Mapped LP Enrollment Source] FROM Superset.[BSI].[Enrollment_Source_Mapping]
GROUP BY [Mapped LP Enrollment Source]
) esm
	ON esm.[Mapped LP Enrollment Source] = cpi2.Enrollment_Source
LEFT JOIN Superset.[BSI].[Enrollment_Channel_Mapping] ecm
	ON ecm.[LP Enrollment_Source] = cpi2.Enrollment_Source



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_PointsExport]'
GO








ALTER PROCEDURE [dbo].[Epsilon_PointsExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT
		CASE WHEN Transaction_Source = 'SFTP' THEN tdr.Booking_ID ELSE NULL END AS TransactionID,
		ISNULL(otn.[Membership Number],tdr.iPrefer_Number) AS CardNumber,
		COALESCE(NULLIF(tdr.Points_Earned, 0.00),tdr.Points_Redemeed * -1,0) as NumPoints,
		CASE 
			WHEN tdr.Arrival_Date > '1900-01-01' 
				THEN tdr.Arrival_Date
			WHEN tdr.Reward_Posting_Date > '1900-01-01'
				THEN tdr.Reward_Posting_Date
			WHEN tdr.Reward_Posting_Date IS NULL AND tdr.Transaction_Date > '1900-01-01'
				THEN tdr.Transaction_Date
			ELSE cpi.Membership_Date END as ActivityDate,
		NULL AS certificateNumber,
		CASE WHEN Transaction_Source = 'SFTP' THEN NULL ELSE tdr.Transaction_Id END as ActAdjustmentId,
		CASE WHEN tdr.Remarks = 'Expired' THEN 'Expired'
			WHEN tdr.Points_Earned <> 0.00 THEN 'EARNED'
			ELSE 'REDEEMED' END AS PointCategory
	  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
	  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
		ON tdr.iPrefer_Number = otn.[Old Membership Number]
	   LEFT JOIN Superset.BSI.Customer_Profile_Import cpi
		ON tdr.iPrefer_Number = cpi.iPrefer_Number
	WHERE COALESCE(NULLIF(tdr.Points_Earned,0.00),tdr.Points_Redemeed * -1,0) <> 0

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_CertificatesExport]'
GO










ALTER PROCEDURE [dbo].[Epsilon_CertificatesExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	  [Voucher_Number] AS CertificateNumber,
	  [Membership_Number] AS CardNumber,
	  'Redeemed' AS CertificateStatus,
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(Voucher_Value * toUSD,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD 
	  Redemption_Date AS CreateDate,
	  NULL AS ExpireDate,
	  NULL AS UpdateDate,
	  Hotel_Code AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  CAST(Voucher_Value AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Account_Statement_Hotel_Redemption_Import] c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code

UNION ALL

SELECT DISTINCT
	  [Voucher_Number] AS CertificateNumber,
	  [Membership_Number] AS CardNumber,
	  'Active' AS CertificateStatus, ---not redeemed
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(c.Payable_Value_USD,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD
	  Redemption_Date AS CreateDate,
	  NULL AS ExpireDate,
	  NULL AS UpdateDate,
	  NULL AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  CAST(Voucher_Value AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Reward_Certificate_Liability_Report_Import] c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_AdjustmentExport]'
GO

ALTER PROCEDURE [dbo].[Epsilon_AdjustmentExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	tdr.Transaction_Id AS ActAdjustmentId,
	ISNULL(otn.[Membership Number],tdr.iPrefer_Number) AS CardNumber,
	AJ.[Epsilon Code] AS AdjustmentReasonCode,
	Remarks AS AdjustmentComments,
	CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int) as NumPoints,
	tdr.Reward_Posting_Date as ActivityDate,
	NULL as CreateUser,
	tdr.Hotel_Code as StoreCode
  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
	ON tdr.iPrefer_Number = otn.[Old Membership Number]
  LEFT JOIN Superset.BSI.[Adjustment_Reason_Codes_Mapping] AJ
  ON tdr.Campaign = aj.Campaign
WHERE TRIM(tdr.Booking_ID) <> ''
AND COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) <> 0
AND Transaction_Source != 'SFTP'
AND ((LEFT(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int),1)= '-' AND LEN(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int))< 9 )
OR (LEFT(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int),1)<> '-' AND LEN(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int))<8 ))

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
