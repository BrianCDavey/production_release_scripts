USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/19/2019 8:16:10 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PointActivity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[PointActivity].[TransactionNumber]', N'ActivityCauseID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[BSI_Populate_PointActivity]'
GO

ALTER   PROCEDURE [dbo].[BSI_Populate_PointActivity]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'BSI'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT t.[Transaction_Date],actT.ActivityTypeID,pt.PointTypeID,ISNULL(t.[TransactionSourceID],'') AS [TransactionSourceID],res.[Booking_ID],res.[Reservation_Revenue],
				res.[Arrival_Date],t.[Reward_Posting_Date],
				CurrencyRates.[dbo].[convertCurrency](res.[Reservation_Revenue],'USD',t.[Currency_Code],res.[Arrival_Date]) AS [CurrencyExchangeRate],
				CASE WHEN t.[Points_Redemeed] > 0 THEN t.[Points_Redemeed] ELSE t.[Points_Earned] END AS [Points],
				t.[Value_of_Redemption_USD],t.[Remarks],ln.[LoyaltyNumberID],@ds AS DataSourceID,t.[TransactionsID],t.[TDR_Transaction_ID],
				t.[Currency_Code]
		FROM [BSI].[Transactions] t
			LEFT JOIN [BSI].[Reservations] res On res.[TransactionsID] = t.[TransactionsID]
			INNER JOIN [dbo].[PointType] pt ON pt.[BSI_ID] = 0
			INNER JOIN [dbo].[LoyaltyNumber] ln ON ln.BSI_ID = t.[LoyaltyNumberID]
			LEFT JOIN [dbo].[ActivityType] actT ON actT.BSI_ID = t.[CampaignID]
	) AS src ON src.DataSourceID = tgt.DataSourceID AND src.[TransactionsID] = tgt.[Internal_SourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [ActivityDate] = src.[Transaction_Date],
				[ActivityTypeID] = src.ActivityTypeID,
				[PointTypeID] = src.PointTypeID,
				[ActivityCauseSystemID] = src.[TransactionSourceID],
				[ActivityCauseID] = src.[Booking_ID],
				[ActivityCauseAmount] = src.[Reservation_Revenue],
				[ActivityCauseDate] = src.[Arrival_Date],
				[CurrencyExchangeDate] = src.[Reward_Posting_Date],
				[CurrencyExchangeRate] = src.[CurrencyExchangeRate],
				[Points] = src.[Points],
				[PointLiabilityUSD] = src.[Value_of_Redemption_USD],
				[Notes] = src.[Remarks],
				[LoyaltyNumberID] = src.[LoyaltyNumberID],
				[External_SourceKey] = src.DataSourceID,
				[ActivityCauseCurrency] = src.[Currency_Code]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([ActivityDate],[ActivityTypeID],[PointTypeID],[ActivityCauseSystemID],[ActivityCauseID],[ActivityCauseAmount],
				[ActivityCauseDate],[CurrencyExchangeDate],
				[CurrencyExchangeRate],
				[Points],
				[PointLiabilityUSD],[Notes],[LoyaltyNumberID],[DataSourceID],[Internal_SourceKey],[External_SourceKey],
				[ActivityCauseCurrency])
		VALUES(src.[Transaction_Date],src.ActivityTypeID,src.PointTypeID,src.[TransactionSourceID],src.[Booking_ID],src.[Reservation_Revenue],
				src.[Arrival_Date],src.[Reward_Posting_Date],
				src.[CurrencyExchangeRate],
				src.[Points],
				src.[Value_of_Redemption_USD],src.[Remarks],src.[LoyaltyNumberID],src.DataSourceID,src.[TransactionsID],src.[TDR_Transaction_ID],
				src.[Currency_Code])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_TravelAgency]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_TravelAgency] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_TravelAgency]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.TravelAgency AS tgt
	USING
	(
		SELECT [TravelAgencyID],[TravelAgencyName] FROM [Epsilon].[TravelAgency]
	) AS src ON src.[TravelAgencyName] = tgt.[TravelAgencyName]
	WHEN MATCHED THEN
		UPDATE
			SET Epsilon_ID = src.[TravelAgencyID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([TravelAgencyName],Epsilon_ID)
		VALUES(src.[TravelAgencyName],src.[TravelAgencyID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_PointActivity]'
GO















-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  
		systemPointActivityID AS [External_SourceKey]
		, [PointActivityID] AS [Internal_SourceKey]
		, CAST(REPLACE(LEFT(NULLIF([ActivityDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityDate],''),2)   AS datetime ) AS [ActivityDate]
		, ac.[ActivityTypeID]
		, pt.[PointTypeID]
		, acs.[ActivityCauseSystemID]
		, [ActivityCauseID]
		, uu.UserUpdatedID AS [ActivityCauseUserID]
		, CAST(NULLIF([ActivityCauseAmount],'') AS decimal(20,5)) AS [ActivityCauseAmount]
		, [ActivityCauseCurrency]
		, CAST(REPLACE(LEFT(NULLIF([ActivityCauseDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityCauseDate],''),2)   AS datetime ) AS [ActivityCauseDate]
		, CAST(REPLACE(LEFT(NULLIF([CurrencyExchangeDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CurrencyExchangeDate],''),2)   AS datetime ) AS [CurrencyExchangeDate]
		, CAST(NULLIF([CurrencyExchangeRate],'') AS decimal(10,9) ) AS [CurrencyExchangeRate]
		, CAST(NULLIF([Points],'') AS decimal(15,5) ) AS [Points]
		, CAST(NULLIF([PointLiabilityUSD],'') AS decimal(10,2) ) AS [PointLiabilityUSD]
		, [Notes]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		,  [QueueID]
		FROM epsilon.PointActivity pa
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = pa.LoyaltyNumberID
		LEFT JOIN dbo.ActivityType ac ON pa.ActivityTypeID  = ac.Epsilon_ID
		LEFT JOIN dbo.PointType pt ON pt.Epsilon_ID  = pa.PointTypeID
		LEFT JOIN dbo.ActivityCauseSystem acs ON acs.Epsilon_ID  = pa.ActivityCauseSystemID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = pa.ActivityCauseUserID
		--WHERE pa.QueueID = @QueueID
	) AS src ON 
	ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	AND tgt.Internal_SourceKey = src.Internal_SourceKey
	AND ISNULL(tgt.[ActivityDate],'1900-01-01')  = ISNULL(src.[ActivityDate],'1900-01-01')
	AND ISNULL(tgt.[ActivityTypeID],'') = ISNULL(src.[ActivityTypeID],'')
	AND ISNULL(tgt.[PointTypeID],'') = ISNULL(src.[PointTypeID],'')
	AND ISNULL(tgt.[ActivityCauseSystemID],'') = ISNULL(src.[ActivityCauseSystemID],'')
	AND ISNULL(tgt.[ActivityCauseID],'') = ISNULL(src.[ActivityCauseID],'')
	AND ISNULL(tgt.[ActivityCauseUserID],'') = ISNULL(src.[ActivityCauseUserID],'')
	AND ISNULL(tgt.[ActivityCauseAmount],0) = ISNULL(src.[ActivityCauseAmount],0)
	AND ISNULL(tgt.[ActivityCauseCurrency],'') = ISNULL(src.[ActivityCauseCurrency],'')
	AND ISNULL(tgt.[ActivityCauseDate],'1900-01-01') = ISNULL(src.[ActivityCauseDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeDate],'1900-01-01') = ISNULL(src.[CurrencyExchangeDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeRate],0) = ISNULL(src.[CurrencyExchangeRate],0)
	AND ISNULL(tgt.[Points],0) = ISNULL(src.[Points],0)
	AND ISNULL(tgt.[PointLiabilityUSD],0) = ISNULL(src.[PointLiabilityUSD],0)
	AND ISNULL(tgt.[Notes],'') = ISNULL(src.[Notes],'')
	AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [ActivityDate], [ActivityTypeID], [PointTypeID], [ActivityCauseSystemID], [ActivityCauseID], [ActivityCauseUserID], [ActivityCauseAmount], [ActivityCauseCurrency], [ActivityCauseDate], [CurrencyExchangeDate], [CurrencyExchangeRate], [Points], [PointLiabilityUSD], [Notes], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey],  [QueueID],[External_SourceKey]	)
		VALUES( src.[ActivityDate], src.[ActivityTypeID], src.[PointTypeID], src.[ActivityCauseSystemID], src.[ActivityCauseID], src.[ActivityCauseUserID], src.[ActivityCauseAmount], src.[ActivityCauseCurrency], src.[ActivityCauseDate], src.[CurrencyExchangeDate], src.[CurrencyExchangeRate], src.[Points], src.[PointLiabilityUSD], src.[Notes], src.[LoyaltyNumberID], src.[DataSourceID], src.[Internal_SourceKey],  src.[QueueID],src.[External_SourceKey])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
