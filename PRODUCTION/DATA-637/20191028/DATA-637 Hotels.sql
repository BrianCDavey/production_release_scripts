USE Hotels
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Hotels    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Hotels

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/28/2019 2:13:56 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Epsilon_DailyHotelExport]'
GO






CREATE PROCEDURE [dbo].[Epsilon_DailyHotelExport]
@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH minContact AS (
		SELECT h.HotelID, UPPER(h.HotelCode) AS HotelCode, h.CRM_HotelID, MIN(hcr.contactID) as minContactID
        FROM Hotels.dbo.Hotel h
        JOIN Hotels.dbo.Hotel_Contact_Role hcr
            ON h.HotelID = hcr.HotelID
            AND GETDATE() BETWEEN ISNULL(hcr.StartDate,'1900-01-01') AND ISNULL(hcr.EndDate,'9999-09-09')
        JOIN Hotels.dbo.Role r
            ON hcr.RoleID = r.RoleID
            AND r.RoleName = 'I Prefer Ambassador'
        GROUP BY h.HotelID, h.HotelCode, h.CRM_HotelID
	),
	jed AS (
		SELECT 
			h.HotelID
			,REPLACE(
			CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
				(
					SELECT
						UPPER(ISNULL(h2.HotelCode,'')) as [HotelCode] 
						, CASE sc.StatusCodeName 
						WHEN 'Former Member' THEN 'Former'						
						WHEN 'Member Hotel' THEN 'Member' 						
						WHEN 'Renewal Hotel' THEN 'Renewal' 
						ELSE '' END as [HotelCurrentStatus]
						, ISNULL(h2.Rooms,'') as [TotalRooms]
						, ISNULL(phg_ipreferquarterlyenrollmentgoal,0)/90 as EnrollmentGoal
					FROM Hotels.dbo.hotel h2
					LEFT JOIN Hotels.dbo.StatusCode sc ON h2.StatusCodeID = sc.StatusCodeID
					LEFT JOIN LocalCRM.dbo.Account2 as a
						ON h2.CRM_HotelID = a.accountid
					WHERE h.HotelID = h2.HotelID 
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				)
				,(
					SELECT 
						ISNULL(phg_contractedprogramstypename,'') as ProgramParticipationStatus
						, CAST(ISNULL(phg_startdate,'1900-01-01') as DATE) as ProgramParticipationStartDate
						, CAST(ISNULL(phg_enddate,'9999-09-09') as DATE) as ProgramParticipationEndDate
					FROM LocalCRM.dbo.phg_contractedprograms cp
					WHERE cp.phg_contractedprogramstypename LIKE '%i%prefer%'
					AND h.CRM_HotelID = cp.phg_accountnameid
					FOR JSON PATH, ROOT('IPreferParticipation')
				)
				,
				(
					SELECT ISNULL(c.CollectionName,'') as [CollectionName]
						, ISNULL(c.Code,'') as [CollectionCode]
						, CAST(ISNULL(hc.StartDate,'1900-01-01') as DATE) as [CollectionParticipationStartDate]
						, CAST(ISNULL(hc.EndDate,'9999-09-09') as DATE) as [CollectionParticipationEndDate] 
					FROM Hotels.dbo.Hotel_Collection hc
					JOIN Hotels.dbo.Collection c
						ON hc.CollectionID = c.CollectionID 
					WHERE h.HotelID = hc.HotelID 
					FOR JSON PATH, ROOT('Collections')
				)
				,
					(
					SELECT ISNULL(iprd.IPreferReward,'') as [BookingReward]
						, CAST(ISNULL(hipr.StartDate,'1900-01-01') as DATE) as [BookingRewardStartDate]
						, CAST(ISNULL(hipr.EndDate,'9999-09-09') as DATE) as [BookingRewardEndDate]
					FROM Hotels.dbo.Hotel_IPreferReward hipr
					JOIN Hotels.dbo.IPreferReward ipr
						ON hipr.IPreferRewardID = ipr.IPreferRewardID
					JOIN Hotels.dbo.IPreferReward_Detail iprd
						ON ipr.IPreferRewardID = iprd.IPreferRewardID
					WHERE h.HotelID = hipr.HotelID 
					FOR JSON PATH, ROOT('IPreferBookingRewards')
				)
				,
					(
					SELECT
						ISNULL(pc.ParentCompanyID,'') as [GroupCompanyID]
						, ISNULL(pc.ParentCompanyName,'') as [GroupName]
						, ISNULL(pr.ParentRelationshipName,'') as [GroupRelationshipType]
						, CAST(ISNULL(hpc.StartDate,'1900-01-01') as DATE) as [GroupStartDate]
						, CAST(ISNULL(hpc.EndDate,'9999-09-09') as DATE) as [GroupEndDate]
					FROM Hotels.dbo.Hotel_ParentCompany hpc
					JOIN Hotels.dbo.ParentCompany pc
						ON hpc.ParentCompanyID = pc.ParentCompanyID
					JOIN Hotels.dbo.ParentRelationship pr
						ON hpc.ParentRelationshipID = pr.ParentRelationshipID
					WHERE h.HotelID = hpc.HotelID 
					FOR JSON PATH, ROOT('HotelGroups')
				)
				,
					(
					SELECT DISTINCT
						 ISNULL(rbcrc.rateCode,'') as [ExcludedRateCode]
						, CAST(ISNULL(rbcrc.startDate, '1900-01-01') as DATE) as [ExcludedRateStartDate]
						, CAST(ISNULL(rbcrc.endDate, '9999-09-09') as DATE) as [ExcludedRateEndDate]
					  FROM 
					  [ReservationBilling].[dbo].[Criteria] rbc
					  JOIN [ReservationBilling].[dbo].[Criteria_RateCodes] rbcrc
						ON rbc.criteriaID = rbcrc.criteriaID
					  JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
						ON rbc.criteriaID = cgic.criteriaID
						JOIN ReservationBilling.dbo.Clauses_ExcludeCriteriaGroups cecg
							ON cgic.criteriaGroupID = cecg.criteriaGroupID
						JOIN ReservationBilling.dbo.Clauses cl
							ON cecg.clauseID = cl.clauseID
					  WHERE rbc.criteriaName LIKE '%I%Prefer%'
					  AND rbc.criteriaName LIKE '%Excluded%Rates%'
					  AND h.HotelCode = cl.hotelCode

				  FOR JSON PATH, ROOT('ExcludedRateCodes')
				)
			),'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',') as jsonExternalData
		FROM Hotels.dbo.Hotel h

	)
	INSERT INTO ETL.dbo.EpsilonExportHotel
	( [QueueID], [StoreCode], [StoreName], [ChainPriority], [ChainName], [FranchiseCode], [ParentStoreCode], [AddressLine1], [AddressLine2], [AddressLine3], [City], [StateCode], [PostalCode], [CountryCode], [Urbanization], [Latitude], [Longitude], [Gaid], [RegionCode], [RegionLevel], [StoreType], [ManagerFirstName], [ManagerMiddleInitial], [ManagerLastName], [ManagerFullName], [ManagerEmailAddr], [PhoneNumber], [DMA], [SalesArea], [TotalArea], [DivCode], [StoreOpenDate], [StoreCloseDate], [RemodelDate], [HierarchyCode], [BrandOrgCode], [ActivityDate], [ClientFileId], [ClientFileRecordNumber], [Status], [JsonExternalData], [GeofenceRadius], [StoreConfiguration], [StoreCompFlag], [StoreTelexNumber], [StoreFaxNumber], [CompStartDate], [CompEndDate], [StoreSizeBand]
	)
	SELECT
		@QueueID,
		REPLACE(REPLACE(UPPER(dh.[Hotel Code]),CHAR(10),''),CHAR(13),'') as [StoreCode],
		REPLACE(REPLACE(dh.[Hotel Name],CHAR(10),''),CHAR(13),'') as [StoreName],
		NULL as [ChainPriority],
		'PHG' as [ChainName],
		NULL as [FranchiseCode],
		NULL as [ParentStoreCode],
		REPLACE(REPLACE(h.Address1,CHAR(10),''),CHAR(13),'') as [AddressLine1],
		REPLACE(REPLACE(h.Address2,CHAR(10),''),CHAR(13),'') as [AddressLine2],
		NULL as [AddressLine3],
		REPLACE(REPLACE(dh.[Hotel City],CHAR(10),''),CHAR(13),'') as [City],
		CASE WHEN REPLACE(REPLACE(crmc.phg_code3,CHAR(10),''),CHAR(13),'') IN ('USA','CAN','MEX','AUS') THEN REPLACE(REPLACE(crms.phg_subareacode,CHAR(10),''),CHAR(13),'')
		ELSE NULL END as [StateCode],
		REPLACE(REPLACE(LEFT(dh.[Hotel Postal Code],10),CHAR(10),''),CHAR(13),'') as [PostalCode],
		REPLACE(REPLACE(crmc.phg_code3,CHAR(10),''),CHAR(13),'') as [CountryCode],
		NULL as [Urbanization],
		REPLACE(REPLACE(h.Latitude,CHAR(10),''),CHAR(13),'') as [Latitude],
		REPLACE(REPLACE(h.Longitude,CHAR(10),''),CHAR(13),'') as [Longitude],
		NULL as [Gaid],
		REPLACE(REPLACE(hr.RegionCode,CHAR(10),''),CHAR(13),'') as [RegionCode],
		REPLACE(REPLACE(hr.RegionName,CHAR(10),''),CHAR(13),'') as [RegionLevel],
		REPLACE(REPLACE(accountclassificationcodename,CHAR(10),''),CHAR(13),'') as [StoreType],
		REPLACE(REPLACE(p.FirstName,CHAR(10),''),CHAR(13),'') as [ManagerFirstName],
		NULL as [ManagerMiddleInitial],
		REPLACE(REPLACE(p.LastName,CHAR(10),''),CHAR(13),'') as [ManagerLastName],
		REPLACE(REPLACE(ISNULL(p.FirstName,'') + ' ' + ISNULL(p.LastName,''),CHAR(10),''),CHAR(13),'') as [ManagerFullName],
		REPLACE(REPLACE(p.Email,CHAR(10),''),CHAR(13),'') as [ManagerEmailAddr],
		LEFT(REPLACE(REPLACE(REPLACE(dbo.GETnumeric(crmA.telephone1),' ',''),CHAR(10),''),CHAR(13),''),20) as [PhoneNumber],
		NULL as [DMA],
		NULL as [SalesArea],
		NULL as [TotalArea],
		NULL as [DivCode],
		NULL as [StoreOpenDate],
		NULL as [StoreCloseDate],
		NULL as [RemodelDate],
		NULL as [HierarchyCode],
		'ALM_BRAND' as [BrandOrgCode],
		GETDATE() as [ActivityDate],
		NULL as [ClientFileId],
		NULL as [ClientFileRecordNumber],
		'A' as [Status],
		'[' + REPLACE(REPLACE(
		jed.jsonExternalData
		,CHAR(10),''),CHAR(13),'') + ']' as [JsonExternalData],
		NULL as [GeofenceRadius],
		NULL as [StoreConfiguration],
		NULL as [StoreCompFlag],
		NULL as [StoreTelexNumber],
		NULL as [StoreFaxNumber],
		NULL as [CompStartDate],
		NULL as [CompEndDate],
		NULL as [StoreSizeBand]
	FROM Dimensional_Warehouse.dim.Hotel dh
	JOIN Hotels.dbo.Hotel h
		ON dh.sourceKey = h.HotelID
	LEFT JOIN LocalCRM.dbo.Account crmA
		ON h.CRM_HotelID = crmA.accountid
	LEFT JOIN Hotels.dbo.Location l
		ON h.LocationID = l.LocationID
	LEFT JOIN Hotels.dbo.Country hc
		ON l.CountryID = hc.CountryID
	LEFT JOIN LocalCRM.dbo.phg_country crmc
		ON hc.CRM_CountryID = crmc.phg_countryid
	LEFT JOIN Hotels.dbo.State hs
		ON l.StateID = hs.StateID
	LEFT JOIN LocalCRM.dbo.phg_state crms
		ON hs.CRM_StateID = crms.phg_stateid
	LEFT JOIN Hotels.dbo.Region hr
		ON l.RegionID = hr.RegionID
	LEFT JOIN minContact mc
		ON h.HotelID = mc.HotelID
	LEFT JOIN Hotels.dbo.Contact c
		ON mc.minContactID = c.ContactID
	LEFT JOIN Hotels.dbo.Person p
		ON c.PersonID = p.PersonID
	LEFT JOIN jed
		ON h.HotelID = jed.HotelID
	WHERE dh.[Status Code] IN ('Member Hotel','Former Member','Renewal Hotel')
	UNION ALL
		SELECT
		@QueueID,
		'0' as [StoreCode],
		'None' as [StoreName],
		NULL as [ChainPriority],
		'PHG' as [ChainName],
		NULL as [FranchiseCode],
		NULL as [ParentStoreCode],
		'' as [AddressLine1],
		'' as [AddressLine2],
		NULL as [AddressLine3],
		'' as [City],
		'' as [StateCode],
		'' as [PostalCode],
		'' as [CountryCode],
		NULL as [Urbanization],
		'' as [Latitude],
		'' as [Longitude],
		NULL as [Gaid],
		'' as [RegionCode],
		'' as [RegionLevel],
		'' as [StoreType],
		'' as [ManagerFirstName],
		NULL as [ManagerMiddleInitial],
		'' as [ManagerLastName],
		'' as [ManagerFullName],
		'' as [ManagerEmailAddr],
		'' as [PhoneNumber],
		NULL as [DMA],
		NULL as [SalesArea],
		NULL as [TotalArea],
		NULL as [DivCode],
		NULL as [StoreOpenDate],
		NULL as [StoreCloseDate],
		NULL as [RemodelDate],
		NULL as [HierarchyCode],
		'ALM_BRAND' as [BrandOrgCode],
		GETDATE() as [ActivityDate],
		NULL as [ClientFileId],
		NULL as [ClientFileRecordNumber],
		'A' as [Status],
		'[{"HotelCode":"0","HotelCurrentStatus":"Former","TotalRooms":0,"EnrollmentGoal":0}]' as [JsonExternalData],
		NULL as [GeofenceRadius],
		NULL as [StoreConfiguration],
		NULL as [StoreCompFlag],
		NULL as [StoreTelexNumber],
		NULL as [StoreFaxNumber],
		NULL as [CompStartDate],
		NULL as [CompEndDate],
		NULL as [StoreSizeBand]

	SELECT  [StoreCode], [StoreName], [ChainPriority], [ChainName], [FranchiseCode], [ParentStoreCode], [AddressLine1], [AddressLine2], [AddressLine3], [City], [StateCode], [PostalCode], [CountryCode], [Urbanization], [Latitude], [Longitude], [Gaid], [RegionCode], [RegionLevel], [StoreType], [ManagerFirstName], [ManagerMiddleInitial], [ManagerLastName], [ManagerFullName], [ManagerEmailAddr], [PhoneNumber], [DMA], [SalesArea], [TotalArea], [DivCode], [StoreOpenDate], [StoreCloseDate], [RemodelDate], [HierarchyCode], [BrandOrgCode], [ActivityDate], [ClientFileId], [ClientFileRecordNumber], [Status], [JsonExternalData], [GeofenceRadius], [StoreConfiguration], [StoreCompFlag], [StoreTelexNumber], [StoreFaxNumber], [CompStartDate], [CompEndDate], [StoreSizeBand]
	FROM ETL.dbo.EpsilonExportHotel
	WHERE QueueID = @QueueID


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_HotelExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_HotelExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH minContact AS (
		SELECT h.HotelID, UPPER(h.HotelCode) AS HotelCode, h.CRM_HotelID, MIN(hcr.contactID) as minContactID
        FROM Hotels.dbo.Hotel h
        JOIN Hotels.dbo.Hotel_Contact_Role hcr
            ON h.HotelID = hcr.HotelID
            AND GETDATE() BETWEEN ISNULL(hcr.StartDate,'1900-01-01') AND ISNULL(hcr.EndDate,'9999-09-09')
        JOIN Hotels.dbo.Role r
            ON hcr.RoleID = r.RoleID
            AND r.RoleName = 'I Prefer Ambassador'
        GROUP BY h.HotelID, h.HotelCode, h.CRM_HotelID
	),
	jed AS (
		SELECT 
			h.HotelID
			,REPLACE(
			CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
				(
					SELECT
						UPPER(ISNULL(h2.HotelCode,'')) as [HotelCode] 
						, CASE sc.StatusCodeName 
						WHEN 'Former Member' THEN 'Former'						
						WHEN 'Member Hotel' THEN 'Member' 						
						WHEN 'Renewal Hotel' THEN 'Renewal' 
						ELSE '' END as [HotelCurrentStatus]
						, ISNULL(h2.Rooms,'') as [TotalRooms]
						, ISNULL(phg_ipreferquarterlyenrollmentgoal,0)/90 as EnrollmentGoal
					FROM Hotels.dbo.hotel h2
					LEFT JOIN Hotels.dbo.StatusCode sc ON h2.StatusCodeID = sc.StatusCodeID
					LEFT JOIN LocalCRM.dbo.Account2 as a
						ON h2.CRM_HotelID = a.accountid
					WHERE h.HotelID = h2.HotelID 
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				)
				,(
					SELECT 
						ISNULL(phg_contractedprogramstypename,'') as ProgramParticipationStatus
						, CAST(ISNULL(phg_startdate,'1900-01-01') as DATE) as ProgramParticipationStartDate
						, CAST(ISNULL(phg_enddate,'9999-09-09') as DATE) as ProgramParticipationEndDate
					FROM LocalCRM.dbo.phg_contractedprograms cp
					WHERE cp.phg_contractedprogramstypename LIKE '%i%prefer%'
					AND h.CRM_HotelID = cp.phg_accountnameid
					FOR JSON PATH, ROOT('IPreferParticipation')
				)
				,
				(
					SELECT ISNULL(c.CollectionName,'') as [CollectionName]
						, ISNULL(c.Code,'') as [CollectionCode]
						, CAST(ISNULL(hc.StartDate,'1900-01-01') as DATE) as [CollectionParticipationStartDate]
						, CAST(ISNULL(hc.EndDate,'9999-09-09') as DATE) as [CollectionParticipationEndDate] 
					FROM Hotels.dbo.Hotel_Collection hc
					JOIN Hotels.dbo.Collection c
						ON hc.CollectionID = c.CollectionID 
					WHERE h.HotelID = hc.HotelID 
					FOR JSON PATH, ROOT('Collections')
				)
				,
					(
					SELECT ISNULL(iprd.IPreferReward,'') as [BookingReward]
						, CAST(ISNULL(hipr.StartDate,'1900-01-01') as DATE) as [BookingRewardStartDate]
						, CAST(ISNULL(hipr.EndDate,'9999-09-09') as DATE) as [BookingRewardEndDate]
					FROM Hotels.dbo.Hotel_IPreferReward hipr
					JOIN Hotels.dbo.IPreferReward ipr
						ON hipr.IPreferRewardID = ipr.IPreferRewardID
					JOIN Hotels.dbo.IPreferReward_Detail iprd
						ON ipr.IPreferRewardID = iprd.IPreferRewardID
					WHERE h.HotelID = hipr.HotelID 
					FOR JSON PATH, ROOT('IPreferBookingRewards')
				)
				,
					(
					SELECT
						ISNULL(pc.ParentCompanyID,'') as [GroupCompanyID]
						, ISNULL(pc.ParentCompanyName,'') as [GroupName]
						, ISNULL(pr.ParentRelationshipName,'') as [GroupRelationshipType]
						, CAST(ISNULL(hpc.StartDate,'1900-01-01') as DATE) as [GroupStartDate]
						, CAST(ISNULL(hpc.EndDate,'9999-09-09') as DATE) as [GroupEndDate]
					FROM Hotels.dbo.Hotel_ParentCompany hpc
					JOIN Hotels.dbo.ParentCompany pc
						ON hpc.ParentCompanyID = pc.ParentCompanyID
					JOIN Hotels.dbo.ParentRelationship pr
						ON hpc.ParentRelationshipID = pr.ParentRelationshipID
					WHERE h.HotelID = hpc.HotelID 
					FOR JSON PATH, ROOT('HotelGroups')
				)
				,
					(
					SELECT DISTINCT
						 ISNULL(rbcrc.rateCode,'') as [ExcludedRateCode]
						, CAST(ISNULL(rbcrc.startDate, '1900-01-01') as DATE) as [ExcludedRateStartDate]
						, CAST(ISNULL(rbcrc.endDate, '9999-09-09') as DATE) as [ExcludedRateEndDate]
					  FROM 
					  [ReservationBilling].[dbo].[Criteria] rbc
					  JOIN [ReservationBilling].[dbo].[Criteria_RateCodes] rbcrc
						ON rbc.criteriaID = rbcrc.criteriaID
					  JOIN ReservationBilling.dbo.CriteriaGroups_IncludeCriteria cgic
						ON rbc.criteriaID = cgic.criteriaID
						JOIN ReservationBilling.dbo.Clauses_ExcludeCriteriaGroups cecg
							ON cgic.criteriaGroupID = cecg.criteriaGroupID
						JOIN ReservationBilling.dbo.Clauses cl
							ON cecg.clauseID = cl.clauseID
					  WHERE rbc.criteriaName LIKE '%I%Prefer%'
					  AND rbc.criteriaName LIKE '%Excluded%Rates%'
					  AND h.HotelCode = cl.hotelCode

				  FOR JSON PATH, ROOT('ExcludedRateCodes')
				)
			),'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',') as jsonExternalData
		FROM Hotels.dbo.Hotel h

	)
	SELECT
		REPLACE(REPLACE(UPPER(dh.[Hotel Code]),CHAR(10),''),CHAR(13),'') as [StoreCode],
		REPLACE(REPLACE(dh.[Hotel Name],CHAR(10),''),CHAR(13),'') as [StoreName],
		NULL as [ChainPriority],
		'PHG' as [ChainName],
		NULL as [FranchiseCode],
		NULL as [ParentStoreCode],
		REPLACE(REPLACE(h.Address1,CHAR(10),''),CHAR(13),'') as [AddressLine1],
		REPLACE(REPLACE(h.Address2,CHAR(10),''),CHAR(13),'') as [AddressLine2],
		NULL as [AddressLine3],
		REPLACE(REPLACE(dh.[Hotel City],CHAR(10),''),CHAR(13),'') as [City],
		CASE WHEN REPLACE(REPLACE(crmc.phg_code3,CHAR(10),''),CHAR(13),'') IN ('USA','CAN','MEX','AUS') THEN REPLACE(REPLACE(crms.phg_subareacode,CHAR(10),''),CHAR(13),'')
		ELSE NULL END as [StateCode],
		REPLACE(REPLACE(LEFT(dh.[Hotel Postal Code],10),CHAR(10),''),CHAR(13),'') as [PostalCode],
		REPLACE(REPLACE(crmc.phg_code3,CHAR(10),''),CHAR(13),'') as [CountryCode],
		NULL as [Urbanization],
		REPLACE(REPLACE(h.Latitude,CHAR(10),''),CHAR(13),'') as [Latitude],
		REPLACE(REPLACE(h.Longitude,CHAR(10),''),CHAR(13),'') as [Longitude],
		NULL as [Gaid],
		REPLACE(REPLACE(hr.RegionCode,CHAR(10),''),CHAR(13),'') as [RegionCode],
		REPLACE(REPLACE(hr.RegionName,CHAR(10),''),CHAR(13),'') as [RegionLevel],
		REPLACE(REPLACE(accountclassificationcodename,CHAR(10),''),CHAR(13),'') as [StoreType],
		REPLACE(REPLACE(p.FirstName,CHAR(10),''),CHAR(13),'') as [ManagerFirstName],
		NULL as [ManagerMiddleInitial],
		REPLACE(REPLACE(p.LastName,CHAR(10),''),CHAR(13),'') as [ManagerLastName],
		REPLACE(REPLACE(ISNULL(p.FirstName,'') + ' ' + ISNULL(p.LastName,''),CHAR(10),''),CHAR(13),'') as [ManagerFullName],
		REPLACE(REPLACE(p.Email,CHAR(10),''),CHAR(13),'') as [ManagerEmailAddr],
		LEFT(REPLACE(REPLACE(REPLACE(dbo.GETnumeric(crmA.telephone1),' ',''),CHAR(10),''),CHAR(13),''),20) as [PhoneNumber],
		NULL as [DMA],
		NULL as [SalesArea],
		NULL as [TotalArea],
		NULL as [DivCode],
		NULL as [StoreOpenDate],
		NULL as [StoreCloseDate],
		NULL as [RemodelDate],
		NULL as [HierarchyCode],
		'ALM_BRAND' as [BrandOrgCode],
		GETDATE() as [ActivityDate],
		NULL as [ClientFileId],
		NULL as [ClientFileRecordNumber],
		'A' as [Status],
		'[' + REPLACE(REPLACE(
		jed.jsonExternalData
		,CHAR(10),''),CHAR(13),'') + ']' as [JsonExternalData],
		NULL as [GeofenceRadius],
		NULL as [StoreConfiguration],
		NULL as [StoreCompFlag],
		NULL as [StoreTelexNumber],
		NULL as [StoreFaxNumber],
		NULL as [CompStartDate],
		NULL as [CompEndDate],
		NULL as [StoreSizeBand]

	FROM Dimensional_Warehouse.dim.Hotel dh
	JOIN Hotels.dbo.Hotel h
		ON dh.sourceKey = h.HotelID
	LEFT JOIN LocalCRM.dbo.Account crmA
		ON h.CRM_HotelID = crmA.accountid
	LEFT JOIN Hotels.dbo.Location l
		ON h.LocationID = l.LocationID
	LEFT JOIN Hotels.dbo.Country hc
		ON l.CountryID = hc.CountryID
	LEFT JOIN LocalCRM.dbo.phg_country crmc
		ON hc.CRM_CountryID = crmc.phg_countryid
	LEFT JOIN Hotels.dbo.State hs
		ON l.StateID = hs.StateID
	LEFT JOIN LocalCRM.dbo.phg_state crms
		ON hs.CRM_StateID = crms.phg_stateid
	LEFT JOIN Hotels.dbo.Region hr
		ON l.RegionID = hr.RegionID
	LEFT JOIN minContact mc
		ON h.HotelID = mc.HotelID
	LEFT JOIN Hotels.dbo.Contact c
		ON mc.minContactID = c.ContactID
	LEFT JOIN Hotels.dbo.Person p
		ON c.PersonID = p.PersonID
	LEFT JOIN jed
		ON h.HotelID = jed.HotelID
	WHERE dh.[Status Code] IN ('Member Hotel','Former Member','Renewal Hotel')
	UNION ALL
		SELECT
		'0' as [StoreCode],
		'None' as [StoreName],
		NULL as [ChainPriority],
		'PHG' as [ChainName],
		NULL as [FranchiseCode],
		NULL as [ParentStoreCode],
		'' as [AddressLine1],
		'' as [AddressLine2],
		NULL as [AddressLine3],
		'' as [City],
		'' as [StateCode],
		'' as [PostalCode],
		'' as [CountryCode],
		NULL as [Urbanization],
		'' as [Latitude],
		'' as [Longitude],
		NULL as [Gaid],
		'' as [RegionCode],
		'' as [RegionLevel],
		'' as [StoreType],
		'' as [ManagerFirstName],
		NULL as [ManagerMiddleInitial],
		'' as [ManagerLastName],
		'' as [ManagerFullName],
		'' as [ManagerEmailAddr],
		'' as [PhoneNumber],
		NULL as [DMA],
		NULL as [SalesArea],
		NULL as [TotalArea],
		NULL as [DivCode],
		NULL as [StoreOpenDate],
		NULL as [StoreCloseDate],
		NULL as [RemodelDate],
		NULL as [HierarchyCode],
		'ALM_BRAND' as [BrandOrgCode],
		GETDATE() as [ActivityDate],
		NULL as [ClientFileId],
		NULL as [ClientFileRecordNumber],
		'A' as [Status],
		'[{"HotelCode":"0","HotelCurrentStatus":"Former","TotalRooms":0,"EnrollmentGoal":0}]' as [JsonExternalData],
		NULL as [GeofenceRadius],
		NULL as [StoreConfiguration],
		NULL as [StoreCompFlag],
		NULL as [StoreTelexNumber],
		NULL as [StoreFaxNumber],
		NULL as [CompStartDate],
		NULL as [CompEndDate],
		NULL as [StoreSizeBand]



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
