USE CurrencyRates
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.CurrencyRates    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.CurrencyRates

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/28/2019 2:10:48 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_DailyCurrencyExport]'
GO




ALTER PROCEDURE [dbo].[Epsilon_DailyCurrencyExport]
 @startDate date, @endDate date, @QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

INSERT INTO ETL.[dbo].[EpsilonExportCurrency]
([QueueID], [BaseCurrencyCode], [TargetCurrencyCode], [Rate], [Status], [StartDate], [EndDate], [JsonExternalData])
SELECT
	@QueueID
	,code AS BaseCurrencyCode
	,'USD' AS TargetCurrencyCode
	,toUSD AS Rate
	,'A' AS Status
	,CAST(rateDate AS DATETIME) AS StartDate
	,DATEADD(SECOND,-1,DATEADD(DAY,1,CAST(rateDate AS DATETIME))) AS EndDate
	,'[{"CurrencyName":"' + [name] + '"}]'AS JsonExternalData
FROM [CurrencyRates].[dbo].[dailyRates]
WHERE rateDate BETWEEN @startDate AND @endDate

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
