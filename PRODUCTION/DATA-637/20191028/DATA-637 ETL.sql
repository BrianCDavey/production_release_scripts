USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/28/2019 2:03:06 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonExport_LOG]'
GO
ALTER TABLE [dbo].[EpsilonExport_LOG] DROP CONSTRAINT [PK_EpsilonExport_LOG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonExport_LOG]'
GO
ALTER TABLE [dbo].[EpsilonExport_LOG] DROP CONSTRAINT [DF_EpsilonExport_LOG_LogStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonExport_LOG]'
GO
DROP TABLE [dbo].[EpsilonExport_LOG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonExport_LoadQueue]'
GO


ALTER PROCEDURE [dbo].[EpsilonExport_LoadQueue]
	 @AppName varchar(255)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	
	DECLARE @SvrName nvarchar(255) = @@SERVERNAME,
			@print varchar(2000),
			@FILESOURCE varchar(255),
			@count int


	SELECT @count = COUNT(*) FROM ETL.dbo.Queue WHERE Application = @AppName AND CAST(CreateDate AS DATE) = CAST(getdate() AS DATE)

	IF (@count = 0 ) --avoid entering two export within the same day
	BEGIN

			
	SELECT TOP 1 @FILESOURCE = FilePath FROM [dbo].[fn_GetFilePathTable](@SvrName,@AppName,'FILE SOURCE')


		INSERT INTO dbo.[Queue]([Application],FilePath,QueueStatus)
		SELECT x.[Application],x.FilePath,x.QueueStatus
		FROM (	SELECT @AppName AS [Application],@FILESOURCE AS FilePath,0 AS QueueStatus
			  ) x
			LEFT JOIN dbo.[Queue] q ON q.[Application] = x.Application AND q.FilePath = x.FilePath AND q.QueueStatus != 3
		WHERE q.QueueID IS NULL
	END

	-- PRINT PROGRESS ---------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	---------------------------------------------------------------

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[qs_Populate_Reservations_Warehouse]'
GO

ALTER PROCEDURE [dbo].[qs_Populate_Reservations_Warehouse]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS(SELECT TOP 1 * FROM dbo.QueueStep WHERE QueueStepStatus = 0)
    BEGIN

		DECLARE @TruncateBeforeLoad bit = 0,@QueueID int = NULL,@StepID int = NULL,@application varchar(255)

		EXEC dbo.qs_ProcessTopQueueItem @QueueID OUTPUT,@StepID OUTPUT

		WHILE @QueueID IS NOT NULL
		BEGIN
			EXEC Reservations.dbo.Populate_Normalized_dbo @TruncateBeforeLoad,@QueueID,@StepID

			SET @QueueID = NULL

			EXEC dbo.qs_ProcessTopQueueItem @QueueID OUTPUT,@StepID OUTPUT
		END

		SELECT @application = Application FROM ETL..Queue WHERE QueueID = @QueueID

		IF(@application = 'Sabre')
		BEGIN
			EXEC [ETL].[dbo].[EpsilonExport_LoadQueue] 'EpsilonExportReservation'
		END
/*
		WHILE 1 = 1
		BEGIN
			IF EXISTS
				(
					SELECT *
					FROM msdb.dbo.sysjobs j
						INNER JOIN msdb.dbo.sysjobactivity ja ON ja.job_id = j.job_id
					WHERE j.name = N'Populate Dimensional Warehouse'
						AND ja.run_requested_date IS NOT NULL
						AND ja.stop_execution_date IS NOT NULL
				)
			BEGIN
				EXEC msdb.dbo.sp_start_job @job_name = N'Populate Dimensional Warehouse'

				BREAK;
			END
			ELSE
			BEGIN
				WAITFOR DELAY '00:01';
			END
		END
*/
    END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonExportHotel]'
GO
CREATE TABLE [dbo].[EpsilonExportHotel]
(
[EpsilonExportHotelID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NULL,
[StoreCode] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChainPriority] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChainName] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FranchiseCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentStoreCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine1] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine2] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressLine3] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostalCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Urbanization] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Latitude] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Longitude] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gaid] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionCode] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RegionLevel] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreType] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerFirstName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerMiddleInitial] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerLastName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerFullName] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManagerEmailAddr] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DMA] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalesArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TotalArea] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DivCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreOpenDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreCloseDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemodelDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HierarchyCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BrandOrgCode] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ActivityDate] [datetime] NOT NULL,
[ClientFileId] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClientFileRecordNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[JsonExternalData] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GeofenceRadius] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreConfiguration] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreCompFlag] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreTelexNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreFaxNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompStartDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompEndDate] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreSizeBand] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_EpsilonExportHotel] on [dbo].[EpsilonExportHotel]'
GO
ALTER TABLE [dbo].[EpsilonExportHotel] ADD CONSTRAINT [PK_dbo_EpsilonExportHotel] PRIMARY KEY CLUSTERED  ([EpsilonExportHotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
