USE ETL
GO

/*
Run this script on:

CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/25/2019 1:21:20 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Add rows to [dbo].[ServerFilePath]')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'FTP', 'sftp.preferredhotelgroup.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'PASSWORD', '0p$R0ck$')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportCurrency', 'USERNAME', 'phg_itdevelopment')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'FTP', 'sftp.preferredhotelgroup.com')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'FTP DIR', '/home/phg_epsilon/uploads')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'PASSWORD', '0p$R0ck$')
INSERT INTO [dbo].[ServerFilePath] ([ServerName], [Application], [Section], [FilePath]) VALUES ('CHI-SQ-PR-01\WAREHOUSE', 'EpsilonExportHotel', 'USERNAME', 'phg_itdevelopment')
PRINT(N'Operation applied to 8 rows out of 8')
COMMIT TRANSACTION
GO
