USE ETL
GO

/*
Run this script on:

CHI-SQ-DP-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Data Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/25/2019 1:24:37 PM

*/
		
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION

PRINT(N'Delete rows from [dbo].[ServerFilePath]')
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'FTP' AND [FilePath] = 'sftp.preferredhotelgroup.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'PASSWORD' AND [FilePath] = '0p$R0ck$'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportCurrency' AND [Section] = 'USERNAME' AND [FilePath] = 'phg_itdevelopment'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'FTP' AND [FilePath] = 'sftp.preferredhotelgroup.com'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'FTP DIR' AND [FilePath] = '/home/phg_epsilon/uploads'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'PASSWORD' AND [FilePath] = '0p$R0ck$'
DELETE FROM [dbo].[ServerFilePath] WHERE [ServerName] = 'CHI-SQ-PR-01\WAREHOUSE' AND [Application] = 'EpsilonExportHotel' AND [Section] = 'USERNAME' AND [FilePath] = 'phg_itdevelopment'
PRINT(N'Operation applied to 8 rows out of 8')
COMMIT TRANSACTION
GO
