USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 12/5/2019 9:46:55 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_AdjustmentExport]'
GO





ALTER PROCEDURE [dbo].[Epsilon_AdjustmentExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	tdr.IP_TDR_ID AS ActAdjustmentId,
	UPPER(ISNULL(otn.[Membership Number],tdr.iPrefer_Number)) AS CardNumber,
	AJ.[Epsilon Code] AS AdjustmentReasonCode,
	Remarks AS AdjustmentComments,
	CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int) as NumPoints,
	tdr.Reward_Posting_Date as ActivityDate,
	NULL as CreateUser,
	CASE WHEN tdr.Hotel_Code NOT IN ('TESTS','phg','PHG123','') THEN tdr.Hotel_Code
	ELSE '0' END as StoreCode
  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
	ON tdr.iPrefer_Number = otn.[Old Membership Number]
  LEFT JOIN Superset.BSI.[Adjustment_Reason_Codes_Mapping] AJ
  ON tdr.Campaign = aj.Campaign
WHERE TRIM(tdr.Booking_ID) <> ''
AND COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) <> 0
AND Transaction_Source != 'SFTP'
AND ((LEFT(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int),1)= '-' AND LEN(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int))< 9 )
OR (LEFT(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int),1)<> '-' AND LEN(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int))<8 ))

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_PointsExport]'
GO












ALTER PROCEDURE [dbo].[Epsilon_PointsExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT DISTINCT
		CASE WHEN Transaction_Source = 'SFTP' THEN tdr.Booking_ID ELSE NULL END AS TransactionID,
		UPPER(ISNULL(otn.[Membership Number],tdr.iPrefer_Number)) AS CardNumber,
		COALESCE(NULLIF(tdr.Points_Earned, 0.00),tdr.Points_Redemeed * -1,0) as NumPoints,
		CASE 
			WHEN tdr.Arrival_Date > '1900-01-01' 
				THEN tdr.Arrival_Date
			WHEN tdr.Reward_Posting_Date > '1900-01-01'
				THEN tdr.Reward_Posting_Date
			WHEN tdr.Reward_Posting_Date IS NULL AND tdr.Transaction_Date > '1900-01-01'
				THEN tdr.Transaction_Date
			ELSE cpi.Membership_Date END as ActivityDate,
		NULL AS certificateNumber,
		CASE WHEN Transaction_Source = 'SFTP' THEN NULL ELSE tdr.IP_TDR_ID END as ActAdjustmentId,
		CASE WHEN tdr.Remarks = 'Expired' THEN 'Expired'
			WHEN tdr.Points_Earned <> 0.00 THEN 'EARNED'
			ELSE 'REDEEMED' END AS PointCategory,
		COALESCE(C.billableDate, ic.billing_date, CASE 
			WHEN tdr.Arrival_Date > '1900-01-01' 
				THEN tdr.Arrival_Date
			WHEN tdr.Reward_Posting_Date > '1900-01-01'
				THEN tdr.Reward_Posting_Date
			WHEN tdr.Reward_Posting_Date IS NULL AND tdr.Transaction_Date > '1900-01-01'
				THEN tdr.Transaction_Date
			ELSE cpi.Membership_Date END ) AS ReportDate
	  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
	  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
		ON tdr.iPrefer_Number = otn.[Old Membership Number]
	   LEFT JOIN Superset.BSI.Customer_Profile_Import cpi
		ON tdr.iPrefer_Number = cpi.iPrefer_Number
		LEFT JOIN (SELECT MIN(billableDate) AS billableDate ,confirmationNumber FROM ReservationBilling.dbo.Charges WHERE classificationID = 5 GROUP BY confirmationNumber) C ON tdr.Booking_ID = c.confirmationNumber 
		LEFT JOIN (SELECT MIN(billing_date) AS billing_date , Booking_ID FROM Superset.BSI.IPrefer_Charges GROUP BY Booking_ID ) ic ON ic.Booking_ID = c.confirmationNumber 
	WHERE COALESCE(NULLIF(tdr.Points_Earned,0.00),tdr.Points_Redemeed * -1,0) <> 0

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
