USE Guests
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Guests    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Guests

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 12/16/2019 11:55:20 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_GuestInfo]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Guest DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_GuestInfo] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_GuestInfo]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- Populate Guest_EmailAddress -----------------------
	MERGE INTO dbo.Guest_EmailAddress AS tgt
	USING
	(
		SELECT DISTINCT [Guest_EmailAddressID],[emailAddress] FROM [Epsilon].[Guest_EmailAddress]
	) AS src ON src.[emailAddress] = tgt.[emailAddress]
	WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.[Guest_EmailAddressID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([emailAddress],[Epsilon_ID])
		VALUES(src.[emailAddress],src.[Guest_EmailAddressID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------

	-- Populate Gender -----------------------
	MERGE INTO dbo.Gender AS tgt
	USING
	(
		SELECT DISTINCT GenderID, GenderName FROM epsilon.Gender
	) AS src ON src.GenderName = tgt.GenderName
	WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.GenderID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(GenderName,[Epsilon_ID])
		VALUES(src.GenderName,GenderID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------


	-- Populate Guest -----------------------------------
	MERGE INTO dbo.Guest AS tgt
	USING
	(
		SELECT [FirstName],[LastName],[Address1],[Address2],loc.[LocationID],gea.[Guest_EmailAddressID],[GuestID],[LoyaltyNumberID],g.birthdate,ge.genderID, g.QueueID
		FROM [Epsilon].[Guest] g
			LEFT JOIN dbo.[Location] loc ON loc.Epsilon_ID = g.LocationID
			LEFT JOIN dbo.Gender ge ON ge.Epsilon_ID = g.GenderID
			LEFT JOIN dbo.[Guest_EmailAddress] gea ON gea.Epsilon_ID = g.[Guest_EmailAddressID]
		WHERE g.QueueID = @QueueID
	) AS src ON src.[LoyaltyNumberID] = tgt.[LoyaltyNumberID]
	WHEN MATCHED THEN
		UPDATE
			SET [FirstName] = src.[FirstName],
				[LastName] = src.[LastName],
				[Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Guest_EmailAddressID] = src.[Guest_EmailAddressID],
				[Epsilon_ID] = src.[GuestID]
				,tgt.birthdate = src.birthdate
				,tgt.genderID = src.genderID
				, tgt.QueueID = src.QueueID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([FirstName],[LastName],[Address1],[Address2],[LocationID],[Guest_EmailAddressID],[Epsilon_ID],[LoyaltyNumberID],birthdate,genderID,QueueID)
		VALUES(src.[FirstName],src.[LastName],src.[Address1],src.[Address2],src.[LocationID],src.[Guest_EmailAddressID],src.[GuestID],src.[LoyaltyNumberID],src.birthdate,src.genderID,src.QueueID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
	-----------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_Location]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Guest DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_Location] 
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_Location]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- City --------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate City'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.City AS tgt
		USING
		(
			SELECT DISTINCT [CityID],[CityName] FROM epsilon.City
		) AS src ON src.CityName = tgt.CityName
		WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.CityID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityName,[Epsilon_ID])
			VALUES(src.CityName,src.CityID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- State -------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate State'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[State] AS tgt
		USING
		(
			SELECT DISTINCT [StateID],[StateName] FROM [Epsilon].[State]
		) AS src ON src.[StateName] = tgt.[StateName]
		WHEN MATCHED THEN
			UPDATE
				SET [Epsilon_ID] = src.[StateID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([StateName],[Epsilon_ID])
			VALUES(src.[StateName],src.[StateID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Country -----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Country'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.Country AS tgt
		USING
		(
			SELECT DISTINCT [CountryID],[CountryName] FROM [Epsilon].[Country]
		) AS src ON src.[CountryName] = tgt.CountryName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([CountryName],[Epsilon_ID])
			VALUES(src.[CountryName],src.[CountryID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Postal Code -------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Postal Code'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[PostalCode] AS tgt
		USING
		(
			SELECT DISTINCT [PostalCodeID],[PostalCodeName] FROM [Epsilon].[PostalCode]
		) AS src ON src.[PostalCodeName] = tgt.[PostalCodeName]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([PostalCodeName],[Epsilon_ID])
			VALUES(src.[PostalCodeName],src.[PostalCodeID])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
	
	-- Location ----------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ':	 Populate Location'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO dbo.[Location] AS tgt
		USING
		(
			SELECT l.LocationID,cit.CityID,st.StateID,con.CountryID,pc.PostalCodeID
			FROM Epsilon.[Location] l
				LEFT JOIN dbo.City cit ON cit.Epsilon_ID = l.CityID
				LEFT JOIN dbo.State st ON st.Epsilon_ID = l.StateID
				LEFT JOIN dbo.Country con ON con.Epsilon_ID = l.CountryID
				LEFT JOIN dbo.PostalCode pc ON pc.Epsilon_ID = l.PostalCodeID
		) AS src ON src.CityID = tgt.CityID AND src.StateID = tgt.StateID AND src.CountryID = tgt.CountryID AND src.PostalCodeID = tgt.PostalCodeID
		WHEN MATCHED THEN
			UPDATE
				SET Epsilon_ID = src.LocationID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT(CityID,StateID,CountryID,PostalCodeID,Epsilon_ID)
			VALUES(src.CityID,src.StateID,src.CountryID,src.PostalCodeID,src.LocationID)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	----------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
