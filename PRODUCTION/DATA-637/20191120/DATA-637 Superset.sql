USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/20/2019 9:05:08 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_CertificatesExport]'
GO

ALTER PROCEDURE [dbo].[Epsilon_CertificatesExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	  [Voucher_Number] AS CertificateNumber,
	  [Membership_Number] AS CardNumber,
	  'Redeemed' AS CertificateStatus,
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(Voucher_Value * toUSD,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD 
	  Redemption_Date AS CreateDate,
	  CAST(Redemption_Date AS DATETIME) AS ExpireDate,
	  NULL AS UpdateDate,
	  Hotel_Code AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  CAST(Voucher_Value AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Account_Statement_Hotel_Redemption_Import] c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code

UNION ALL

SELECT DISTINCT
	  [Voucher_Number] AS CertificateNumber,
	  [Membership_Number] AS CardNumber,
	  'Active' AS CertificateStatus, ---not redeemed
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(c.Payable_Value_USD,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD
	  Redemption_Date AS CreateDate,
	  NULL AS ExpireDate,
	  NULL AS UpdateDate,
	  NULL AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  CAST(Voucher_Value AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Reward_Certificate_Liability_Report_Import] c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code

UNION ALL

SELECT DISTINCT
	  [Certificate Number] AS CertificateNumber,
	  [Member Number] AS CardNumber,
	  'Canelled' AS CertificateStatus, ---not redeemed
	  	CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END + REPLACE(REPLACE(REPLACE(REPLACE([Product Redeemed],'US$',''),'£',''),'€',''), 'Reward Certificate','') AS RewardCode,
	  [Product Redeemed] AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(CAST([Points Redeemded] AS decimal(20,2))*0.002,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD
	  CAST([Redemption Date] AS datetime) AS CreateDate,
	  NULL AS ExpireDate,
	  CAST([Cancellation Date] AS datetime) AS UpdateDate,
	  NULL AS HotelCode,
	  CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END AS CurrencyCode,
	  CAST(REPLACE(REPLACE(REPLACE(REPLACE([Product Redeemed],'US$',''),'£',''),'€',''), 'Reward Certificate','') AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].Cancelled_Certificates_Staging c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL([Cancellation Date],[Redemption Date]) = xe.rateDate
	AND  CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END = xe.code


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
