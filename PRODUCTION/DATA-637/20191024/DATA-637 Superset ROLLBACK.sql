/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 10/24/2019 2:58:24 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport02]'
GO





-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport02]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

SELECT 
'02' AS RecordNumber
,NULL AS AddressId
,address AS AddressLine1
,address2 AS AddressLine2
,NULL AS AddressLine3
,city AS City
,CASE WHEN c.code3 IN ('USA','CAN') THEN sa.subAreaCode 
ELSE NULL END AS StateCode
,c.code3 AS CountryCode
,LEFT(zip_postal,10) AS PostalCode
,NULL AS ChannelCode
,NULL AS LocationCode
,'True' AS IsPreferred
,'False' AS DoNotStandardize
,NULL AS UniqueZipInd
,NULL AS MailScore
,NULL AS Latitude
,NULL AS Longitude
,NULL AS NoStatInd
,NULL AS AceErrorCode
,NULL AS MultiTypeCode
,NULL AS NcoaReturnCode
,NULL AS SeasonalInd
,NULL AS EducationInd
,NULL AS AddressDefInd
,NULL AS EndDate
,NULL AS Status
,import_date AS ActivityDate
,NULL AS CreateFileId
,NULL AS CreateRecordNumber
,address AS SrcAddrLine1
,address2 AS SrcAddrLine2
,NULL AS SrcAddrLine3
,city AS SrcCity
,state AS SrcState
,zip_Postal AS SrcPostalCd
,country AS SrcCountry
,NULL AS SrcIsoCountryCd
,Iprefer_Number AS InteractionId
,NULL AS AddressLine4
,NULL AS NameMatchInd
,NULL AS PacActionCode
,NULL AS PacFootNote
,NULL AS SrcCompanyName
,NULL AS SrcFirmName
,NULL AS Phone1
,NULL AS Phone2
,'ALM_BRAND' AS BrandOrgCode
,'ALMACCT' AS AccountSourceCode
,iprefer_number AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[All_Member_Report_Import] ari
LEFT JOIN iso.[dbo].[countries] c 
ON ari.Country = c.shortName
LEFT JOIN iso.[dbo].[subAreas] sa
ON c.code2 = sa.countryCode2 AND sa.subAreaName = State
;



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
