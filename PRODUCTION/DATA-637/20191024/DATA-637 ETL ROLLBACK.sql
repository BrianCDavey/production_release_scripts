/*
Run this script on:

        chi-lt-00032377.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/23/2019 4:39:45 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonExport_LoadQueue]'
GO
DROP PROCEDURE [dbo].[EpsilonExport_LoadQueue]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonExportPointTransaction]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonExportPointTransaction].[QueueID]', N'EpsilonExport_LOG_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [ACT_TRANSACTION_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROFILE_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TRANS_DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [ELIGIBLE_REVENUE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TAX_AMOUNT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [POST_SALES_ADJUSTMNT_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SHIPPING_HANDLING] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [STATUS] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [DEVICE_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [DEVICE_USERID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [AUTHORIZATION_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SKIP_MOMENT_ENGINE_IND] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [CLIENT_FILE_REC_NUM] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [ASSOCIATE_NUM] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_GROSS_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TXN_SEQ_NUM] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TIER] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [blank1] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [EXCHANGE_RATE_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [GRATUITY] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SRC_CUSTOMER_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [DISC_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TXN_CHANNEL_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [BUYER_TYPE_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [RMB_METHOD_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [RMB_VENDOR_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [RMB_DATE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [blank2] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_NET_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_TAX_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_POST_SALES_ADJ_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_SHIPPING_HANDLING] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_ELIG_REVENUE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_GRATUITY] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TXN_TYPE_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SUSPEND_REASON] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SUSPEND_TRANSACTION] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [POSTING_KEY_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [POSTING_KEY_VALUE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonExportReservation]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonExportReservation].[QueueID]', N'EpsilonExport_LOG_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [ACT_RESERVATION_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROFILE_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RESERVATION_DESCRIPTION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [ELIGIBLE_REVENUE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TAX_AMOUNT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [POST_SALES_ADJUSTMNT_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SHIPPING_HANDLING] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [STATUS] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [DEVICE_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [DEVICE_USERID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [AUTHORIZATION_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [CLIENT_FILE_REC_NUM] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [ASSOCIATE_NUM] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_GROSS_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TXN_SEQ_NUM] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TXN_SUBTYPE_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TIER] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [blank1] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [EXCHANGE_RATE_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [GRATUITY] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SRC_CUSTOMER_ID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [DISC_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RESERVATION_CHANNEL_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [BUYER_TYPE_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RMB_METHOD_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RMB_VENDOR_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RMB_DATE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [blank2] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_RES_TOTAL] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_TAX_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_POST_SALES_ADJ_AMT] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_SHIPPING_HANDLING] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_ELIG_REVENUE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_GRATUITY] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TXN_TYPE_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SUSPEND_REASON] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SUSPEND_RESERVATION] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [POSTING_KEY_CODE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [POSTING_KEY_VALUE] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonPointTransactionLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonPointTransactionLog].[QueueID]', N'EpsilonExport_LOG_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonReservationLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonReservationLog].[QueueID]', N'EpsilonExport_LOG_ID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
