USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/23/2019 4:39:26 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonExport_LoadQueue]'
GO

CREATE PROCEDURE [dbo].[EpsilonExport_LoadQueue]
	 @AppName varchar(255)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	
	DECLARE @SvrName nvarchar(255) = @@SERVERNAME,
			@print varchar(2000),
			@FILESOURCE varchar(255)
			
	SELECT TOP 1 @FILESOURCE = FilePath FROM [dbo].[fn_GetFilePathTable](@SvrName,@AppName,'FILE SOURCE')


		INSERT INTO dbo.[Queue]([Application],FilePath,QueueStatus)
		SELECT x.[Application],x.FilePath,x.QueueStatus
		FROM (	SELECT @AppName AS [Application],@FILESOURCE AS FilePath,0 AS QueueStatus
			  ) x
			LEFT JOIN dbo.[Queue] q ON q.[Application] = x.Application AND q.FilePath = x.FilePath AND q.QueueStatus != 3
		WHERE q.QueueID IS NULL


	-- PRINT PROGRESS ---------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	---------------------------------------------------------------

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonExportPointTransaction]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonExportPointTransaction].[EpsilonExport_LOG_ID]', N'QueueID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [ACT_TRANSACTION_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROFILE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TRANS_DESCRIPTION] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [ELIGIBLE_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TAX_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [POST_SALES_ADJUSTMNT_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [DEVICE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [DEVICE_USERID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [AUTHORIZATION_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SKIP_MOMENT_ENGINE_IND] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [CLIENT_FILE_REC_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [ASSOCIATE_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_GROSS_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TXN_SEQ_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [blank1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [EXCHANGE_RATE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SRC_CUSTOMER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [DISC_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TXN_CHANNEL_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [BUYER_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [RMB_METHOD_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [RMB_VENDOR_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [RMB_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [blank2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_NET_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_TAX_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_POST_SALES_ADJ_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_ELIG_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [PROG_CURR_GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [TXN_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SUSPEND_REASON] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [SUSPEND_TRANSACTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [POSTING_KEY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ALTER COLUMN [POSTING_KEY_VALUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonExportReservation]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonExportReservation].[EpsilonExport_LOG_ID]', N'QueueID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [ACT_RESERVATION_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROFILE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RESERVATION_DESCRIPTION] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [ELIGIBLE_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TAX_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [POST_SALES_ADJUSTMNT_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [DEVICE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [DEVICE_USERID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [AUTHORIZATION_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [CLIENT_FILE_REC_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [ASSOCIATE_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_GROSS_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TXN_SEQ_NUM] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TXN_SUBTYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TIER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [blank1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [EXCHANGE_RATE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SRC_CUSTOMER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [DISC_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RESERVATION_CHANNEL_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [BUYER_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RMB_METHOD_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RMB_VENDOR_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [RMB_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [blank2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_RES_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_TAX_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_POST_SALES_ADJ_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_SHIPPING_HANDLING] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_ELIG_REVENUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [PROG_CURR_GRATUITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [TXN_TYPE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SUSPEND_REASON] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [SUSPEND_RESERVATION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [POSTING_KEY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ALTER COLUMN [POSTING_KEY_VALUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonPointTransactionLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonPointTransactionLog].[EpsilonExport_LOG_ID]', N'QueueID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[EpsilonReservationLog]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[EpsilonReservationLog].[EpsilonExport_LOG_ID]', N'QueueID', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
