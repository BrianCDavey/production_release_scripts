USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/25/2019 7:17:05 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PointActivity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PointActivity] ADD
[ActivityCauseHotelID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [epsilon].[PointActivity]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [epsilon].[PointActivity] ADD
[ActivityCauseHotelID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_PointActivity]'
GO

















-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  
		systemPointActivityID AS [External_SourceKey]
		, [PointActivityID] AS [Internal_SourceKey]
		, CAST(REPLACE(LEFT(NULLIF([ActivityDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityDate],''),2)   AS datetime ) AS [ActivityDate]
		, ac.[ActivityTypeID]
		, pt.[PointTypeID]
		, acs.[ActivityCauseSystemID]
		, [TransactionNumber]
		, uu.UserUpdatedID AS [ActivityCauseUserID]
		, CAST(NULLIF([ActivityCauseAmount],'') AS decimal(20,5)) AS [ActivityCauseAmount]
		, [ActivityCauseCurrency]
		, CAST(REPLACE(LEFT(NULLIF([ActivityCauseDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityCauseDate],''),2)   AS datetime ) AS [ActivityCauseDate]
		, CAST(REPLACE(LEFT(NULLIF([CurrencyExchangeDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CurrencyExchangeDate],''),2)   AS datetime ) AS [CurrencyExchangeDate]
		, CAST(NULLIF([CurrencyExchangeRate],'') AS decimal(10,9) ) AS [CurrencyExchangeRate]
		, CAST(NULLIF([Points],'') AS decimal(15,5) ) AS [Points]
		, CAST(NULLIF([PointLiabilityUSD],'') AS decimal(10,2) ) AS [PointLiabilityUSD]
		, [Notes]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		,  [QueueID]
		, ActivityCauseHotelID
		FROM epsilon.PointActivity pa
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = pa.LoyaltyNumberID
		LEFT JOIN dbo.ActivityType ac ON pa.ActivityTypeID  = ac.Epsilon_ID
		LEFT JOIN dbo.PointType pt ON pt.Epsilon_ID  = pa.PointTypeID
		LEFT JOIN dbo.ActivityCauseSystem acs ON acs.Epsilon_ID  = pa.ActivityCauseSystemID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = pa.ActivityCauseUserID
		--WHERE pa.QueueID = @QueueID
	) AS src ON 
	ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	AND tgt.Internal_SourceKey = src.Internal_SourceKey
	AND ISNULL(tgt.[ActivityDate],'1900-01-01')  = ISNULL(src.[ActivityDate],'1900-01-01')
	AND ISNULL(tgt.[ActivityTypeID],'') = ISNULL(src.[ActivityTypeID],'')
	AND ISNULL(tgt.[PointTypeID],'') = ISNULL(src.[PointTypeID],'')
	AND ISNULL(tgt.[ActivityCauseSystemID],'') = ISNULL(src.[ActivityCauseSystemID],'')
	AND ISNULL(tgt.[TransactionNumber],'') = ISNULL(src.[TransactionNumber],'')
	AND ISNULL(tgt.[ActivityCauseUserID],'') = ISNULL(src.[ActivityCauseUserID],'')
	AND ISNULL(tgt.[ActivityCauseAmount],0) = ISNULL(src.[ActivityCauseAmount],0)
	AND ISNULL(tgt.[ActivityCauseCurrency],'') = ISNULL(src.[ActivityCauseCurrency],'')
	AND ISNULL(tgt.[ActivityCauseDate],'1900-01-01') = ISNULL(src.[ActivityCauseDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeDate],'1900-01-01') = ISNULL(src.[CurrencyExchangeDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeRate],0) = ISNULL(src.[CurrencyExchangeRate],0)
	AND ISNULL(tgt.[Points],0) = ISNULL(src.[Points],0)
	AND ISNULL(tgt.[PointLiabilityUSD],0) = ISNULL(src.[PointLiabilityUSD],0)
	AND ISNULL(tgt.[Notes],'') = ISNULL(src.[Notes],'')
	AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	AND ISNULL(tgt.ActivityCauseHotelID,'') = ISNULL(src.ActivityCauseHotelID,'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [ActivityDate], [ActivityTypeID], [PointTypeID], [ActivityCauseSystemID], [TransactionNumber], [ActivityCauseUserID], [ActivityCauseAmount], [ActivityCauseCurrency], [ActivityCauseDate], [CurrencyExchangeDate], [CurrencyExchangeRate], [Points], [PointLiabilityUSD], [Notes], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey],  [QueueID],[External_SourceKey]	,ActivityCauseHotelID)
		VALUES( src.[ActivityDate], src.[ActivityTypeID], src.[PointTypeID], src.[ActivityCauseSystemID], src.[TransactionNumber], src.[ActivityCauseUserID], src.[ActivityCauseAmount], src.[ActivityCauseCurrency], src.[ActivityCauseDate], src.[CurrencyExchangeDate], src.[CurrencyExchangeRate], src.[Points], src.[PointLiabilityUSD], src.[Notes], src.[LoyaltyNumberID], src.[DataSourceID], src.[Internal_SourceKey],  src.[QueueID],src.[External_SourceKey],src.ActivityCauseHotelID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [epsilon].[Populate_PointActivity]'
GO









-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [epsilon].[Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [epsilon].[Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO epsilon.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  iep.ACT_POINT_ID AS [SystemPointActivityID]
		, iep.QueueID AS QueueID
		, iep.CREATE_DATE AS [ActivityDate]
		, ac.[ActivityTypeID] AS [ActivityTypeID]
		, pt.[PointTypeID] AS [PointTypeID]
		, iep.Update_User AS [UpdateUser]
		, acs.[ActivityCauseSystemID] AS [ActivityCauseSystemID]
		, COALESCE(NULLIF(iep.ACT_TRANSACTION_ID,''),NULLIF(iep.ACT_ADJUSTMENT_ID,''),NULLIF(iep.ACT_ACTIVITY_ID,''),iep.ACT_ORDER_REWARD_ID) AS [ActivityCauseID]
		, iep.[Transaction_Number] AS [TransactionNumber]
		, uu.[UserUpdatedID] AS [ActivityCauseUserID]
		, iep.TRANSACTION_NET_AMOUNT AS [ActivityCauseAmount]
		, iep.CURRENCY_CODE AS [ActivityCauseCurrency]
		, iep.TRANSACTION_DATE AS [ActivityCauseDate]
		, iep.CURRENCY_EXCHANGE_DATE AS [CurrencyExchangeDate]
		, iep.CURRENCY_EXCHANGE_RATE AS [CurrencyExchangeRate]
		, iep.NUM_POINTS AS [Points]
		, iep.USD_PTS_LIABILITY AS [PointLiabilityUSD]
		, iep.TRANS_DESCRIPTION AS [Notes]
		, ln.LoyaltyNumberID
		, hh.hotelID AS ActivityCauseHotelID
		FROM ETL.dbo.Import_EpsilonPoint iep
		INNER JOIN epsilon.LoyaltyNumber ln ON ln.LoyaltyNumberName = iep.Card_Number
		LEFT JOIN epsilon.ActivityType ac ON iep.DISPLAY_POINT_TYPE  = ac.ActivityTypeName
		LEFT JOIN epsilon.PointType pt ON pt.PointTypeName  = iep.SHORT_DESC
		LEFT JOIN epsilon.ActivityCauseSystem acs ON acs.ActivityCauseSystemName  = iep.TXN_SOURCE_CODE
		LEFT JOIN epsilon.UserUpdated uu ON uu.UserUpdatedName = iep.CREATE_USER
		LEFT JOIN hotels.dbo.hotel hh ON hh.hotelcode = iep.STORE_CODE
		WHERE iep.QueueID = @QueueID
	) AS src ON 
	src.[SystemPointActivityID] = tgt.[SystemPointActivityID]
	AND src.[ActivityDate] = tgt.[ActivityDate]
	AND src.[ActivityTypeID] = tgt.[ActivityTypeID]
	AND src.[PointTypeID] = tgt.[PointTypeID]
	AND src.[UpdateUser] = tgt.[UpdateUser]
	AND src.[ActivityCauseSystemID] = tgt.[ActivityCauseSystemID]
	AND src.[ActivityCauseID] = tgt.[ActivityCauseID]
	AND src.[TransactionNumber] = tgt.[TransactionNumber]
	AND src.[ActivityCauseUserID] = tgt.[ActivityCauseUserID]
	AND src.[ActivityCauseAmount] = tgt.[ActivityCauseAmount]
	AND src.[ActivityCauseCurrency] = tgt.[ActivityCauseCurrency]
	AND src.[ActivityCauseDate] = tgt.[ActivityCauseDate]
	AND src.[CurrencyExchangeDate] = tgt.[CurrencyExchangeDate]
	AND src.[CurrencyExchangeRate] = tgt.[CurrencyExchangeRate]
	AND src.[Points] = tgt.[Points]
	AND src.[PointLiabilityUSD] = tgt.[PointLiabilityUSD]
	AND src.[Notes] = tgt.[Notes]
	AND src.LoyaltyNumberID = tgt.LoyaltyNumberID
	AND src.ActivityCauseHotelID = tgt.ActivityCauseHotelID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(	[SystemPointActivityID]
		,QueueID
	,[ActivityDate]
	,[ActivityTypeID]
	,[PointTypeID]
	,[UpdateUser]
	,[ActivityCauseSystemID]
	,[ActivityCauseID]
	,[TransactionNumber]
	,[ActivityCauseUserID]
	,[ActivityCauseAmount]
	,[ActivityCauseCurrency]
	,[ActivityCauseDate]
	,[CurrencyExchangeDate]
	,[CurrencyExchangeRate]
	,[Points]
	,[PointLiabilityUSD]
	,[Notes]
	,LoyaltyNumberID
	,ActivityCauseHotelID)
		VALUES([SystemPointActivityID]
		,QueueID
	,[ActivityDate]
	,[ActivityTypeID]
	,[PointTypeID]
	,[UpdateUser]
	,[ActivityCauseSystemID]
	,[ActivityCauseID]
	,[TransactionNumber]
	,[ActivityCauseUserID]
	,[ActivityCauseAmount]
	,[ActivityCauseCurrency]
	,[ActivityCauseDate]
	,[CurrencyExchangeDate]
	,[CurrencyExchangeRate]
	,[Points]
	,[PointLiabilityUSD]
	,[Notes]
	,LoyaltyNumberID
	,ActivityCauseHotelID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
