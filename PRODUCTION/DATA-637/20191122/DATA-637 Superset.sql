USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 11/22/2019 9:09:26 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_AdjustmentExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_AdjustmentExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	tdr.Transaction_Id AS ActAdjustmentId,
	UPPER(ISNULL(otn.[Membership Number],tdr.iPrefer_Number)) AS CardNumber,
	AJ.[Epsilon Code] AS AdjustmentReasonCode,
	Remarks AS AdjustmentComments,
	CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int) as NumPoints,
	tdr.Reward_Posting_Date as ActivityDate,
	NULL as CreateUser,
	CASE WHEN tdr.Hotel_Code NOT IN ('TESTS','phg','') THEN tdr.Hotel_Code
	ELSE '0' END as StoreCode
  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
	ON tdr.iPrefer_Number = otn.[Old Membership Number]
  LEFT JOIN Superset.BSI.[Adjustment_Reason_Codes_Mapping] AJ
  ON tdr.Campaign = aj.Campaign
WHERE TRIM(tdr.Booking_ID) <> ''
AND COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) <> 0
AND Transaction_Source != 'SFTP'
AND ((LEFT(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int),1)= '-' AND LEN(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int))< 9 )
OR (LEFT(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int),1)<> '-' AND LEN(CAST(COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) AS int))<8 ))

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_CertificatesExport]'
GO















ALTER PROCEDURE [dbo].[Epsilon_CertificatesExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

SELECT DISTINCT
	  [Voucher_Number] AS CertificateNumber,
	  UPPER([Membership_Number]) AS CardNumber,
	  'Redeemed' AS CertificateStatus,
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(Voucher_Value * toUSD,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD 
	  Redemption_Date AS CreateDate,
	  CAST(Redemption_Date AS DATETIME) AS ExpireDate,
	  NULL AS UpdateDate,
	  Hotel_Code AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  CAST(Voucher_Value AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Account_Statement_Hotel_Redemption_Import] c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code

UNION ALL

SELECT DISTINCT
	  [Voucher_Number] AS CertificateNumber,
	  UPPER([Membership_Number]) AS CardNumber,
	  'Active' AS CertificateStatus, ---not redeemed
	  CONCAT(c.voucher_currency,c.voucher_value) AS RewardCode,
	  Voucher_Name AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(c.Payable_Value_USD,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD
	  Redemption_Date AS CreateDate,
	  NULL AS ExpireDate,
	  NULL AS UpdateDate,
	  NULL AS HotelCode,
	  Voucher_Currency AS CurrencyCode,
	  CAST(Voucher_Value AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].[Reward_Certificate_Liability_Report_Import] c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL(c.currency_conversion_date,redemption_date) = xe.rateDate
	AND c.Voucher_Currency = xe.code

UNION ALL

SELECT DISTINCT
	  [Certificate Number] AS CertificateNumber,
	  UPPER([Member Number]) AS CardNumber,
	  'Cancelled' AS CertificateStatus, ---not redeemed
	  	CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END + REPLACE(REPLACE(REPLACE(REPLACE([Product Redeemed],'US$',''),'£',''),'€',''), 'Reward Certificate','') AS RewardCode,
	  [Product Redeemed] AS Title,
	  0 AS TotalPaid,
	  CAST(ROUND(CAST([Points Redeemded] AS decimal(20,2))*0.002,2) AS decimal(20,2)) AS RetailValue, --needs to be in USD
	  CAST([Redemption Date] AS datetime) AS CreateDate,
	  NULL AS ExpireDate,
	  CAST([Cancellation Date] AS datetime) AS UpdateDate,
	  NULL AS HotelCode,
	  CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END AS CurrencyCode,
	  CAST(REPLACE(REPLACE(REPLACE(REPLACE([Product Redeemed],'US$',''),'£',''),'€',''), 'Reward Certificate','') AS decimal(20,0)) AS Denomination,
	  0 AS PointValue,
	  xe.toUSD AS ConversionRate
  FROM [Superset].[BSI].Cancelled_Certificates_Staging c
  INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON ISNULL([Cancellation Date],[Redemption Date]) = xe.rateDate
	AND  CASE 
			WHEN LEFT([Product Redeemed],1) = 'u' THEN 'USD'
			WHEN LEFT([Product Redeemed],1) = '£' THEN 'GBP'
			WHEN LEFT([Product Redeemed],1) = '€' THEN 'EUR'
		END = xe.code


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_InquiriesExport]'
GO





-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-09
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_InquiriesExport]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT
UPPER([Member Number]) AS CardNumber
,[Reference Number] AS CaseId
,'Member' AS Category
,ISNULL(L.Reason,'Other') AS Reason
,CAST([Date of Query] AS DATE) AS CreateDate
,CAST(NULLIF([Date of Query Closed],'') AS DATE) AS UpdateDate
,'False' AS Critical
,[Query Logged By] AS CreateUser
,REPLACE(REPLACE([Query Text],CHAR(10),''),CHAR(13),'') AS Comments
FROM [Superset].BSI.[LogQueryStatusReport_Staging] S
LEFT JOIN Superset.BSI.LoyaltyPrimeSubEpsilonReasonMapping L 
ON S.[Type of sub query] = L.[Type of sub query]


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport02]'
GO








-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport02]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

SELECT 
'02' AS RecordNumber
,NULL AS AddressId
,address AS AddressLine1
,address2 AS AddressLine2
,NULL AS AddressLine3
,city AS City
,CASE WHEN c.code3 IN ('USA','CAN','MEX','AUS') THEN sa.subAreaCode 
ELSE NULL END AS StateCode
,c.code3 AS CountryCode
,CASE WHEN Zip_Postal LIKE '%e+%' THEN NULL ELSE LEFT(Zip_Postal,10) END AS PostalCode
,NULL AS ChannelCode
,NULL AS LocationCode
,'True' AS IsPreferred
,'False' AS DoNotStandardize
,NULL AS UniqueZipInd
,NULL AS MailScore
,NULL AS Latitude
,NULL AS Longitude
,NULL AS NoStatInd
,NULL AS AceErrorCode
,NULL AS MultiTypeCode
,NULL AS NcoaReturnCode
,NULL AS SeasonalInd
,NULL AS EducationInd
,NULL AS AddressDefInd
,NULL AS EndDate
,NULL AS Status
,import_date AS ActivityDate
,NULL AS CreateFileId
,NULL AS CreateRecordNumber
,address AS SrcAddrLine1
,address2 AS SrcAddrLine2
,NULL AS SrcAddrLine3
,city AS SrcCity
,state AS SrcState
,CASE WHEN Zip_Postal LIKE '%e+%' THEN NULL ELSE LEFT(Zip_Postal,10) END AS SrcPostalCd
,country AS SrcCountry
,NULL AS SrcIsoCountryCd
,UPPER(Iprefer_Number) AS InteractionId
,NULL AS AddressLine4
,NULL AS NameMatchInd
,NULL AS PacActionCode
,NULL AS PacFootNote
,NULL AS SrcCompanyName
,NULL AS SrcFirmName
,NULL AS Phone1
,NULL AS Phone2
,'ALM_BRAND' AS BrandOrgCode
,'ALMACCT' AS AccountSourceCode
,UPPER(iprefer_number) AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[All_Member_Report_Import] ari
LEFT JOIN iso.[dbo].[countries] c 
ON ari.Country = c.shortName
LEFT JOIN iso.[dbo].[subAreas] sa
ON c.code2 = sa.countryCode2 AND sa.subAreaName = State

UNION ALL

SELECT 
'02' AS RecordNumber
,NULL AS AddressId
,cpi.address AS AddressLine1
,cpi.address2 AS AddressLine2
,NULL AS AddressLine3
,cpi.city AS City
,CASE WHEN c.code3 IN ('USA','CAN','MEX','AUS') THEN sa.subAreaCode 
ELSE NULL END AS StateCode
,c.code3 AS CountryCode
,CASE WHEN cpi.Zip_Postal LIKE '%e+%' THEN NULL ELSE LEFT(cpi.Zip_Postal,10) END AS PostalCode
,NULL AS ChannelCode
,NULL AS LocationCode
,'True' AS IsPreferred
,'False' AS DoNotStandardize
,NULL AS UniqueZipInd
,NULL AS MailScore
,NULL AS Latitude
,NULL AS Longitude
,NULL AS NoStatInd
,NULL AS AceErrorCode
,NULL AS MultiTypeCode
,NULL AS NcoaReturnCode
,NULL AS SeasonalInd
,NULL AS EducationInd
,NULL AS AddressDefInd
,NULL AS EndDate
,NULL AS Status
,cpi.import_date AS ActivityDate
,NULL AS CreateFileId
,NULL AS CreateRecordNumber
,cpi.address AS SrcAddrLine1
,cpi.address2 AS SrcAddrLine2
,NULL AS SrcAddrLine3
,cpi.city AS SrcCity
,cpi.state AS SrcState
,CASE WHEN cpi.Zip_Postal LIKE '%e+%' THEN NULL ELSE LEFT(cpi.Zip_Postal,10) END AS SrcPostalCd
,cpi.country AS SrcCountry
,NULL AS SrcIsoCountryCd
,UPPER(cpi.Iprefer_Number) AS InteractionId
,NULL AS AddressLine4
,NULL AS NameMatchInd
,NULL AS PacActionCode
,NULL AS PacFootNote
,NULL AS SrcCompanyName
,NULL AS SrcFirmName
,NULL AS Phone1
,NULL AS Phone2
,'ALM_BRAND' AS BrandOrgCode
,'ALMACCT' AS AccountSourceCode
,UPPER(cpi.iprefer_number) AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import] cpi
LEFT JOIN Superset.BSI.All_Member_Report_Import amri
ON cpi.iPrefer_Number = amri.iPrefer_Number
LEFT JOIN iso.[dbo].[countries] c 
ON cpi.Country = c.shortName
LEFT JOIN iso.[dbo].[subAreas] sa
ON c.code2 = sa.countryCode2 AND sa.subAreaName = cpi.State
WHERE amri.iPrefer_Number IS NULL
AND cpi.iPrefer_Number IN (SELECT iPrefer_Number FROM Superset.BSI.TransactionDetailedReport)

;



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport03]'
GO





-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport03]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
'03' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, CAST(ROW_NUMBER()OVER(ORDER BY Iprefer_Number) AS varchar(200)) + 'test@test.com' AS EmailAddress
--, Email AS EmailAddress
, NULL AS EmailId
, 'True' AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, Import_Date AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, CAST(ROW_NUMBER()OVER(ORDER BY Iprefer_Number) AS varchar(200)) + 'test@test.com' AS SrcEmailAddr
--, Email AS SrcEmailAddr
, NULL AS ContactPointId
, UPPER(IPrefer_Number) AS InteractionId
, NULL AS EmailType
, NULL AS SpamStatus
, NULL AS EmailFormatPref
, 'ALMACCT' AS AccountSourceCode
, UPPER(IPrefer_Number) AS SourceAccountNumber
, 'ALM_BRAND' AS BrandOrgCode
, NULL AS JsonExternalData
FROM [Superset].[BSI].[All_Member_Report_Import]
UNION ALL

	SELECT
'03' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, CAST(ROW_NUMBER()OVER(ORDER BY cpi.Iprefer_Number) AS varchar(200)) + 'test@test.com' AS EmailAddress
--, Email AS EmailAddress
, NULL AS EmailId
, 'True' AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, cpi.Import_Date AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, CAST(ROW_NUMBER()OVER(ORDER BY cpi.Iprefer_Number) AS varchar(200)) + 'test@test.com' AS SrcEmailAddr
--, Email AS SrcEmailAddr
, NULL AS ContactPointId
, UPPER(cpi.IPrefer_Number) AS InteractionId
, NULL AS EmailType
, NULL AS SpamStatus
, NULL AS EmailFormatPref
, 'ALMACCT' AS AccountSourceCode
, UPPER(cpi.IPrefer_Number) AS SourceAccountNumber
, 'ALM_BRAND' AS BrandOrgCode
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import] cpi
LEFT JOIN Superset.BSI.All_Member_Report_Import amri
ON cpi.iPrefer_Number = amri.iPrefer_Number
WHERE amri.iPrefer_Number IS NULL
AND cpi.iPrefer_Number IN (SELECT iPrefer_Number FROM Superset.BSI.TransactionDetailedReport)
;


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_MemberExport04]'
GO




-- =============================================
-- Author:		Kris Scott
-- Create date: 2019-07-11
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_MemberExport04]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
SELECT
'04' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, CASE 
WHEN LEN(dbo.GetNumeric(PhoneNumber)) < 5 THEN ''
WHEN Country IN ('United States', 'Canada')
	THEN RIGHT(dbo.GetNumeric(PhoneNumber),10)
	ELSE RIGHT(dbo.GetNumeric(PhoneNumber),16)
	END
 AS PhoneNumber
, NULL AS PhoneId
, NULL AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, NULL AS Extension
, NULL AS PhoneCountryCode
, NULL AS OptStatus
, NULL AS OptInDate
, NULL AS Frequency
, NULL AS NeverBefore
, NULL AS NeverAfter
, NULL AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, NULL AS SrcPhoneNumber
, NULL AS ContactPointId
, UPPER(Iprefer_Number) AS InteractionId
, 'ALM_BRAND' AS BrandOrgCode
, NULL AS AcceptsText
, 'ALMACCT' AS AccountSourceCode
, UPPER(Iprefer_Number) AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[All_Member_Report_Import]

UNION ALL

SELECT
'04' AS RecordNumber
, NULL AS ChannelCode
, NULL AS ContactScore
, NULL AS DeliveryStatus
, CASE 
WHEN LEN(dbo.GetNumeric(PhoneNumber)) < 5 THEN ''
WHEN cpi.Country IN ('United States', 'Canada')
	THEN RIGHT(dbo.GetNumeric(PhoneNumber),10)
	ELSE RIGHT(dbo.GetNumeric(PhoneNumber),16)
	END
 AS PhoneNumber
, NULL AS PhoneId
, NULL AS IsPreferred
, NULL AS LocationCode
, NULL AS Status
, NULL AS Extension
, NULL AS PhoneCountryCode
, NULL AS OptStatus
, NULL AS OptInDate
, NULL AS Frequency
, NULL AS NeverBefore
, NULL AS NeverAfter
, NULL AS ActivityDate
, NULL AS CreateFileId
, NULL AS CreateRecordNumber
, NULL AS SrcPhoneNumber
, NULL AS ContactPointId
, UPPER(cpi.Iprefer_Number) AS InteractionId
, 'ALM_BRAND' AS BrandOrgCode
, NULL AS AcceptsText
, 'ALMACCT' AS AccountSourceCode
, UPPER(cpi.Iprefer_Number) AS SourceAccountNumber
, NULL AS JsonExternalData
FROM [Superset].[BSI].[Customer_Profile_Import] cpi
LEFT JOIN Superset.BSI.All_Member_Report_Import amri
ON cpi.iPrefer_Number = amri.iPrefer_Number
WHERE amri.iPrefer_Number IS NULL
AND cpi.iPrefer_Number IN (SELECT iPrefer_Number FROM Superset.BSI.TransactionDetailedReport)
;



END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_PointsExport]'
GO











ALTER PROCEDURE [dbo].[Epsilon_PointsExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT DISTINCT
		CASE WHEN Transaction_Source = 'SFTP' THEN tdr.Booking_ID ELSE NULL END AS TransactionID,
		UPPER(ISNULL(otn.[Membership Number],tdr.iPrefer_Number)) AS CardNumber,
		COALESCE(NULLIF(tdr.Points_Earned, 0.00),tdr.Points_Redemeed * -1,0) as NumPoints,
		CASE 
			WHEN tdr.Arrival_Date > '1900-01-01' 
				THEN tdr.Arrival_Date
			WHEN tdr.Reward_Posting_Date > '1900-01-01'
				THEN tdr.Reward_Posting_Date
			WHEN tdr.Reward_Posting_Date IS NULL AND tdr.Transaction_Date > '1900-01-01'
				THEN tdr.Transaction_Date
			ELSE cpi.Membership_Date END as ActivityDate,
		NULL AS certificateNumber,
		CASE WHEN Transaction_Source = 'SFTP' THEN NULL ELSE tdr.Transaction_Id END as ActAdjustmentId,
		CASE WHEN tdr.Remarks = 'Expired' THEN 'Expired'
			WHEN tdr.Points_Earned <> 0.00 THEN 'EARNED'
			ELSE 'REDEEMED' END AS PointCategory,
		COALESCE(C.billableDate, ic.billing_date, CASE 
			WHEN tdr.Arrival_Date > '1900-01-01' 
				THEN tdr.Arrival_Date
			WHEN tdr.Reward_Posting_Date > '1900-01-01'
				THEN tdr.Reward_Posting_Date
			WHEN tdr.Reward_Posting_Date IS NULL AND tdr.Transaction_Date > '1900-01-01'
				THEN tdr.Transaction_Date
			ELSE cpi.Membership_Date END ) AS ReportDate
	  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
	  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
		ON tdr.iPrefer_Number = otn.[Old Membership Number]
	   LEFT JOIN Superset.BSI.Customer_Profile_Import cpi
		ON tdr.iPrefer_Number = cpi.iPrefer_Number
		LEFT JOIN (SELECT MIN(billableDate) AS billableDate ,confirmationNumber FROM ReservationBilling.dbo.Charges WHERE classificationID = 5 GROUP BY confirmationNumber) C ON tdr.Booking_ID = c.confirmationNumber 
		LEFT JOIN (SELECT MIN(billing_date) AS billing_date , Booking_ID FROM Superset.BSI.IPrefer_Charges GROUP BY Booking_ID ) ic ON ic.Booking_ID = c.confirmationNumber 
	WHERE COALESCE(NULLIF(tdr.Points_Earned,0.00),tdr.Points_Redemeed * -1,0) <> 0

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
