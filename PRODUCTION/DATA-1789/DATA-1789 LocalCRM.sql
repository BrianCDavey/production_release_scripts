USE LocalCRM
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.LocalCRM    -  This database will be modified

to synchronize it with:

        (local)\WAREHOUSE.LocalCRM

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 8/19/2021 1:11:11 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [staging].[phg_hoteleventregistration]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [staging].[phg_hoteleventregistration] ADD
[phg_registranttype] [int] NULL,
[phg_registranttypename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[phg_hoteleventregistration]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[phg_hoteleventregistration] ADD
[phg_registranttype] [int] NULL,
[phg_registranttypename] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[crm_synchronize_phg_hoteleventregistration]'
GO




ALTER PROCEDURE [dbo].[crm_synchronize_phg_hoteleventregistration]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM staging.phg_hoteleventregistration)
	BEGIN
		MERGE INTO dbo.phg_hoteleventregistration AS tgt
		USING (SELECT * FROM staging.phg_hoteleventregistration) AS src ON src.phg_hoteleventregistrationid = tgt.phg_hoteleventregistrationid
		WHEN MATCHED THEN
			UPDATE
				SET createdby = src.createdby,createdbyname = src.createdbyname,createdbyyominame = src.createdbyyominame,createdon = src.createdon,createdonbehalfby = src.createdonbehalfby,createdonbehalfbyname = src.createdonbehalfbyname,createdonbehalfbyyominame = src.createdonbehalfbyyominame,importsequencenumber = src.importsequencenumber,modifiedby = src.modifiedby,modifiedbyname = src.modifiedbyname,modifiedbyyominame = src.modifiedbyyominame,modifiedon = src.modifiedon,modifiedonbehalfby = src.modifiedonbehalfby,modifiedonbehalfbyname = src.modifiedonbehalfbyname,modifiedonbehalfbyyominame = src.modifiedonbehalfbyyominame,overriddencreatedon = src.overriddencreatedon,ownerid = src.ownerid,owneridname = src.owneridname,owneridtype = src.owneridtype,owneridyominame = src.owneridyominame,owningbusinessunit = src.owningbusinessunit,owningteam = src.owningteam,owninguser = src.owninguser,phg_account = src.phg_account,phg_accountname = src.phg_accountname,phg_accountyominame = src.phg_accountyominame,phg_attendingguestfullname = src.phg_attendingguestfullname,phg_eventdate = src.phg_eventdate,phg_eventlocation = src.phg_eventlocation,phg_eventtype = src.phg_eventtype,phg_eventtypename = src.phg_eventtypename,phg_hoteleventregistrationid = src.phg_hoteleventregistrationid,phg_name = src.phg_name,phg_registration = src.phg_registration,phg_registrationname = src.phg_registrationname,phg_registrationstatus = src.phg_registrationstatus,phg_registrationstatusname = src.phg_registrationstatusname,statecode = src.statecode,statecodename = src.statecodename,statuscode = src.statuscode,statuscodename = src.statuscodename,timezoneruleversionnumber = src.timezoneruleversionnumber,utcconversiontimezonecode = src.utcconversiontimezonecode,versionnumber = src.versionnumber,phg_eventcost = src.phg_eventcost,phg_eventcurrency = src.phg_eventcurrency, phg_registranttype = src.phg_registranttype, phg_registranttypename = src.phg_registranttypename
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([createdby],[createdbyname],[createdbyyominame],[createdon],[createdonbehalfby],[createdonbehalfbyname],[createdonbehalfbyyominame],[importsequencenumber],[modifiedby],[modifiedbyname],[modifiedbyyominame],[modifiedon],[modifiedonbehalfby],[modifiedonbehalfbyname],[modifiedonbehalfbyyominame],[overriddencreatedon],[ownerid],[owneridname],[owneridtype],[owneridyominame],[owningbusinessunit],[owningteam],[owninguser],[phg_account],[phg_accountname],[phg_accountyominame],[phg_attendingguestfullname],[phg_eventdate],[phg_eventlocation],[phg_eventtype],[phg_eventtypename],[phg_hoteleventregistrationid],[phg_name],[phg_registration],[phg_registrationname],[phg_registrationstatus],[phg_registrationstatusname],[statecode],[statecodename],[statuscode],[statuscodename],[timezoneruleversionnumber],[utcconversiontimezonecode],[versionnumber],phg_eventcost,phg_eventcurrency,phg_registranttype,phg_registranttypename)
			VALUES(src.[createdby],src.[createdbyname],src.[createdbyyominame],src.[createdon],src.[createdonbehalfby],src.[createdonbehalfbyname],src.[createdonbehalfbyyominame],src.[importsequencenumber],src.[modifiedby],src.[modifiedbyname],src.[modifiedbyyominame],src.[modifiedon],src.[modifiedonbehalfby],src.[modifiedonbehalfbyname],src.[modifiedonbehalfbyyominame],src.[overriddencreatedon],src.[ownerid],src.[owneridname],src.[owneridtype],src.[owneridyominame],src.[owningbusinessunit],src.[owningteam],src.[owninguser],src.[phg_account],src.[phg_accountname],src.[phg_accountyominame],src.[phg_attendingguestfullname],src.[phg_eventdate],src.[phg_eventlocation],src.[phg_eventtype],src.[phg_eventtypename],src.[phg_hoteleventregistrationid],src.[phg_name],src.[phg_registration],src.[phg_registrationname],src.[phg_registrationstatus],src.[phg_registrationstatusname],src.[statecode],src.[statecodename],src.[statuscode],src.[statuscodename],src.[timezoneruleversionnumber],src.[utcconversiontimezonecode],src.[versionnumber],src.phg_eventcost,src.phg_eventcurrency,src.phg_registranttype,src.phg_registranttypename)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
