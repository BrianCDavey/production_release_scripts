USE Core
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Core    -  This database will be modified

to synchronize it with:

        (local)\WAREHOUSE.Core

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 8/19/2021 1:08:22 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[weboomAccountingRegistrations]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[weboomAccountingRegistrations] ADD
[registrantType] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[wbm_GetSwoogoEventRegistrationExportData]'
GO


ALTER procedure [dbo].[wbm_GetSwoogoEventRegistrationExportData]

AS

SELECT DISTINCT 
  we.[accountCode]
, we.[billingAmount]
, we.[billingAmountCurrency]
, we.[invoiceDueDate]
, we.[serviceDate]
, we.[financeEventCode]
, we.[marketingInclusionFlag]
, we.[marketingInclusionExplanation]
, UPPER(LEFT(we.[registrationStatus],1)) + RIGHT(we.[registrationStatus],LEN(we.[registrationStatus])-1) AS [registrationStatus]
, we.[confirmationNumber]
, we.[attendingGuestFullName]
, REPLACE(we.[eventType],'RS / CE','RS/CE') AS eventType
, we.[eventDate]
, we.[eventLocation]
, we.[timeLoaded]
, we.registrantType
FROM	[dbo].[weboomAccountingEvents]	e
INNER JOIN [dbo].weboomAccountingRegistrations we ON e.financeEventCode = we.financeEventCode
WHERE isswoogo = 1
AND accountCode <> ''
AND e.financeEventCode IN (
SELECT [financeEventCode]
FROM	[dbo].[weboomAccountingEvents]	
WHERE isswoogo = 1 
)

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
