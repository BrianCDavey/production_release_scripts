USE ETL
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ETL    -  This database will be modified

to synchronize it with:

        (local)\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 8/19/2021 1:04:31 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoRegistrant_FinalMerge_Core]'
GO











-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-07-29
-- Description:	This procedure is a part of Swoogo import process loading data from ETL db to Core DB
-- Prototype: EXEC [dbo].[SwoogoRegistrant_FinalMerge_Core] 3
-- History: 2020-08-13 Ti Yao Initial Creation
--			2020-08-18 Ti Yao Add registrantType
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoRegistrant_FinalMerge_Core]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE Core.dbo.weboomAccountingRegistrations tgt
	USING (
		SELECT DISTINCT
		accountCode
		,billingAmount
		,billingAmountCurrency
		,invoiceDueDate
		,serviceDate
		,financeEventCode
		,marketingInclusionFlag
		,marketingInclusionExplanation
		,registrationStatus
		,confirmationNumber
		,attendingGuestFullName
		,eventType
		,eventDate
		,eventLocation
		,timeLoaded
		,registrantType
		FROM Core.Swoogo.weboomAccountingRegistrations 
		WHERE QueueID = @QueueID
		AND (isBad is null or isBad = 0)
	) src
	ON tgt.confirmationNumber = src.confirmationNumber
	WHEN MATCHED
    THEN UPDATE SET 
        tgt.accountCode = src.accountCode,
        tgt.billingAmount = src.billingAmount,
		tgt.billingAmountCurrency = src.billingAmountCurrency,
		tgt.invoiceDueDate = src.invoiceDueDate,
		tgt.serviceDate = src.serviceDate,
		tgt.financeEventCode = src.financeEventCode,
		tgt.marketingInclusionFlag = src.marketingInclusionFlag,
		tgt.marketingInclusionExplanation = src.marketingInclusionExplanation,
		tgt.registrationStatus = src.registrationStatus,
		tgt.attendingGuestFullName = src.attendingGuestFullName,
		tgt.eventType = src.eventType,
		tgt.eventDate = src.eventDate,
		tgt.eventLocation = src.eventLocation,
		tgt.timeLoaded = src.timeLoaded,
		tgt.registrantType = src.registrantType
	WHEN NOT MATCHED
    THEN INSERT ([accountCode], [billingAmount], [billingAmountCurrency], [invoiceDueDate], [serviceDate], [financeEventCode], [marketingInclusionFlag], [marketingInclusionExplanation], [registrationStatus], [confirmationNumber], [attendingGuestFullName], [eventType], [eventDate], [eventLocation], [timeLoaded], registrantType)
	VALUES (src.[accountCode], src.[billingAmount], src.[billingAmountCurrency], src.[invoiceDueDate], src.[serviceDate], src.[financeEventCode], src.[marketingInclusionFlag], src.[marketingInclusionExplanation], src.[registrationStatus], src.[confirmationNumber], src.[attendingGuestFullName], src.[eventType], src.[eventDate], src.[eventLocation], src.[timeLoaded], src.registrantType)
	
	;


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
