/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 12/14/2017 12:55:47 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[ips_iPreferScorecardArrivalData]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferScorecardArrivalData] 
	@MyDate INT
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardArrivalData.sql
--DECLARE @MyDate INT = 2017

DECLARE @IPreferCharges TABLE(	earliestBooking DATETIME,
								Membership_Number NVARCHAR(6)
							);

INSERT INTO @IPreferCharges
SELECT	MIN(Booking_Date) as earliestbooking, Membership_Number
FROM	BSI.IPrefer_Charges
WHERE	Booking_Date >= '2014-05-01'
GROUP BY	Membership_Number;

select 
	core.dbo.hotelsreporting.code,
	count(*) as arrivals,
	sum(reservationrevenueusd) as hotelrevenue,
	CASE WHEN secondarySource = 'iPrefer APP' then 'iPrefer APP' ELSE channelreportlabel END as bookedchannel,
	SUM(Billable_Amount_USD) as phgrevenue,
	case WHEN ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END AS MemberRate,
	OptIn as Checkbox,
	mostrecenttransactionsreporting.status as bookingStatus,
	CASE WHEN Reporting.HyperDisk.OrderID IS NULL then 'NO' ELSE 'YES' END AS MarketingCampaign,
	CASE WHEN Core.dbo.iPreferCheckboxHotels.hotelCode IS NULL then 'NO' ELSE 'YES' END as CheckboxHotel,
	CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < confirmationdate THEN 'NO' ELSE 'YES' END as FirstBooking,
	AVG(DATEDIFF(day,confirmationdate, arrivaldate)) as averageleadtime,
	MONTH(arrivaldate) as arrivalmonth
FROM
	mostrecenttransactionsreporting
JOIN
	core.dbo.hotelsReporting 
ON
	core.dbo.hotelsReporting.SynXisID = mostrecenttransactionsreporting.hotelid
JOIN
	BSI.IPrefer_Charges 
ON
	BSI.IPrefer_Charges.Booking_ID = mostrecenttransactionsreporting.confirmationnumber

LEFT OUTER JOIN
	core.dbo.iPreferCheckboxHotels
ON
	core.dbo.hotelsReporting.code = core.dbo.iPreferCheckboxHotels.hotelCode
LEFT OUTER JOIN 
	@IPreferCharges IPC
ON
	mostrecenttransactionsreporting.loyaltyNumber = IPC.Membership_Number

JOIN
	BSI.Customer_Profile_Import
ON
	BSI.Customer_Profile_Import.iPrefer_Number = mostrecenttransactionsreporting.LoyaltyNumber
LEFT OUTER JOIN 
	Reporting.HyperDisk
ON
	mostrecenttransactionsreporting.confirmationnumber = Reporting.HyperDisk.OrderID
WHERE
	YEAR(arrivaldate) >= @MyDate
AND
	mostrecenttransactionsreporting.loyaltynumber <> ''
GROUP BY
	core.dbo.hotelsreporting.code,
	CASE WHEN secondarySource = 'iPrefer APP' then 'iPrefer APP' ELSE channelreportlabel END,
	case WHEN ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END,
	OptIn,
	mostrecenttransactionsreporting.status,
	CASE WHEN Reporting.HyperDisk.OrderID IS NULL then 'NO' ELSE 'YES' END,
	CASE WHEN Core.dbo.iPreferCheckboxHotels.hotelCode IS NULL then 'NO' ELSE 'YES' END,
	CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < confirmationdate THEN 'NO' ELSE 'YES' END,
	MONTH(arrivaldate);
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferScorecardBookingData]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferScorecardBookingData] 
	@MyDate INT
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardBookingData.sql
--DECLARE @MyDate INT

DECLARE @IPreferCharges TABLE(	earliestBooking DATETIME,
								Membership_Number NVARCHAR(6)
							);

INSERT INTO @IPreferCharges
SELECT	MIN(Booking_Date) as earliestbooking, Membership_Number
FROM	BSI.IPrefer_Charges
WHERE	Booking_Date >= '2017-01-01'
GROUP BY	Membership_Number
ORDER BY Membership_Number;


SELECT 
	core.dbo.hotelsReporting.code,
	count(*) as bookings,
	sum(reservationrevenueusd) as hotelrevenue,
	CASE WHEN secondarySource = 'iPrefer APP' then 'iPrefer APP' ELSE channelreportlabel END as bookedchannel,
	SUM(CASE WHEN channelreportlabel = 'IBE - PHG' then (reservationrevenueusd * 0.12) WHEN channelreportlabel = 'Voice - PHG' THEN (reservationrevenueusd * 0.15) ELSE (reservationrevenueusd *0.025) END)as phgprojectedrevenue,
	case WHEN ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END AS MemberRate,
	OptIn as Checkbox,
	mostrecenttransactionsreporting.status as bookingStatus,
	CASE WHEN Reporting.HyperDisk.OrderID IS NULL then 'NO' ELSE 'YES' END AS MarketingCampaign,
	CASE WHEN Core.dbo.iPreferCheckboxHotels.hotelCode IS NULL then 'NO' ELSE 'YES' END as CheckboxHotel,
	CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < confirmationdate THEN 'NO' ELSE 'YES' END as FirstBooking,
	AVG(DATEDIFF(day,confirmationdate, arrivaldate)) as averageleadtime,
	MONTH(confirmationdate) as bookedmonth
FROM
	mostrecenttransactionsreporting
JOIN
	core.dbo.hotelsReporting 
ON
	core.dbo.hotelsReporting.SynXisID = mostrecenttransactionsreporting.hotelid
LEFT OUTER JOIN
	core.dbo.iPreferCheckboxHotels
ON
	core.dbo.hotelsReporting.code = core.dbo.iPreferCheckboxHotels.hotelCode
LEFT OUTER JOIN 
	@IPreferCharges IPC
ON
	mostrecenttransactionsreporting.loyaltyNumber = IPC.Membership_Number

JOIN
	BSI.Customer_Profile_Import
ON
	BSI.Customer_Profile_Import.iPrefer_Number = mostrecenttransactionsreporting.LoyaltyNumber
LEFT OUTER JOIN 
	Reporting.HyperDisk
ON
	mostrecenttransactionsreporting.confirmationnumber = Reporting.HyperDisk.OrderID
WHERE
	YEAR(confirmationdate) >= @MyDate
AND
	mostrecenttransactionsreporting.loyaltynumber <> ''
GROUP BY
	core.dbo.hotelsReporting.code,
	CASE WHEN secondarySource = 'iPrefer APP' then 'iPrefer APP' ELSE channelreportlabel END,
	case WHEN ratetypecode = 'MKTIPM' THEN 'YES' ELSE 'NO' END,
	OptIn,
	mostrecenttransactionsreporting.status,
	CASE WHEN Reporting.HyperDisk.OrderID IS NULL then 'NO' ELSE 'YES' END,
	CASE WHEN Core.dbo.iPreferCheckboxHotels.hotelCode IS NULL then 'NO' ELSE 'YES' END,
	CASE WHEN COALESCE(IPC.earliestbooking,'2099-01-01') < confirmationdate THEN 'NO' ELSE 'YES' END,
	MONTH(confirmationdate);
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferScorecardHotelWebsiteBookings]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferScorecardHotelWebsiteBookings] 
	@MyDate INT
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardHotelWebsiteBookings.sql
--DECLARE @MyDate INT = 2017

select 
	Core.dbo.hotelsreporting.code,
	mostRecentTransactionsReporting.status as bookingStatus,
	count(*) as totalHotelWebsiteBookings, 
	sum(reservationrevenueusd) as hotelrevenue,
	CASE WHEN Core.dbo.iPreferCheckboxHotels.hotelCode IS NULL then 'NO' ELSE 'YES' END as CheckboxHotel,
	optIn,
	MONTH(confirmationdate) as bookingmonth
FROM
	Superset.dbo.mostrecenttransactionsreporting
JOIN
	Core.dbo.hotelsReporting 
ON
	Core.dbo.hotelsReporting.SynXisID = mostrecenttransactionsreporting.hotelid
LEFT OUTER JOIN
	Core.dbo.iPreferCheckboxHotels
ON
	Core.dbo.hotelsReporting.code = core.dbo.iPreferCheckboxHotels.hotelCode
WHERE
	YEAR(confirmationdate) >= @MyDate
AND
	channelreportlabel = 'IBE - Hotel'
GROUP BY
	Core.dbo.hotelsreporting.code,
	mostRecentTransactionsReporting.status,
	CASE WHEN Core.dbo.iPreferCheckboxHotels.hotelCode IS NULL then 'NO' ELSE 'YES' END,
	optIn,
	MONTH(confirmationdate)	
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [temp].[Hotel_IATA_Data]'
GO
CREATE TABLE [temp].[Hotel_IATA_Data]
(
[IATANumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArrivalYear] [int] NULL,
[ArrivalMonth] [int] NULL,
[hotelID] [int] NULL,
[TotalRoomNights] [int] NULL,
[TotalReservations] [int] NULL,
[AVGNightlyRate] [numeric] (38, 2) NULL,
[TotalRevenueUSD] [decimal] (38, 2) NULL,
[AVGLOS] [decimal] (5, 2) NULL,
[AVGLeadTime] [int] NULL,
[SUMLeadTime] [int] NULL,
[concatDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
