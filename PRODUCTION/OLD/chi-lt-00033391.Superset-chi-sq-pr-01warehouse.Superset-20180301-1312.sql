/*
Run this script on:

        chi-sq-pr-01\warehouse.Superset    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 3/1/2018 1:12:46 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[ips_iPreferChannelBookings]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferChannelBookings] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS rowNumber,'' AS groupHack,
		HR.hotelName,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Direct' THEN IC.Booking_ID END)) AS bookingsDirect,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'GDS' THEN IC.Booking_ID END)) AS bookingsGDS,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Hotel IBE' THEN IC.Booking_ID END)) AS bookingsHotelIBE,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'iPrefer.com' THEN IC.Booking_ID END)) AS bookingsIPrefer,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'PHG Brand Sites' THEN IC.Booking_ID END)) AS bookingsPhgBrand,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Voice' THEN IC.Booking_ID END)) AS bookingsVoice,
		COUNT(DISTINCT (CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Booking_ID END)) AS bookingsTotal,
		SUM(CASE WHEN IBC.Channel = 'Direct' THEN IC.Billable_Amount_USD END) AS revenueDirect,
		SUM(CASE WHEN IBC.Channel = 'GDS' THEN IC.Billable_Amount_USD END) AS revenueGDS,
		SUM(CASE WHEN IBC.Channel = 'Hotel IBE' THEN IC.Billable_Amount_USD END) AS revenueHotelIBE,
		SUM(CASE WHEN IBC.Channel = 'iPrefer.com' THEN IC.Billable_Amount_USD END) AS revenueIPrefer,
		SUM(CASE WHEN IBC.Channel = 'PHG Brand Sites' THEN IC.Billable_Amount_USD END) AS revenuePhgBrand,
		SUM(CASE WHEN IBC.Channel = 'Voice' THEN IC.Billable_Amount_USD END) AS revenueVoice,
		SUM(CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Billable_Amount_USD END) AS revenueTotal
		
FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept

WHERE	IC.Billing_Date BETWEEN @startDate AND @endDate

GROUP BY	HR.code,
			HR.hotelName

HAVING		COUNT(DISTINCT (CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Booking_ID END)) <> 0
			AND
			SUM(CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Billable_Amount_USD END) <> 0

ORDER BY	revenueTotal DESC;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferChannelBookingsDetail]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferChannelBookingsDetail] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	DISTINCT HR.hotelName,
		IBC.Channel,
		IC.Booking_ID,
		IC.Billable_Amount_USD

FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept

WHERE	IC.Billing_Date BETWEEN @startDate AND @endDate

ORDER BY HR.hotelName,
		IBC.Channel,
		IC.Booking_ID;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferChannelCurrentMonth]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferChannelCurrentMonth] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Direct' THEN IC.Booking_ID END)) AS bookingsDirect,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'GDS' THEN IC.Booking_ID END)) AS bookingsGDS,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Hotel IBE' THEN IC.Booking_ID END)) AS bookingsHotelIBE,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'iPrefer.com' THEN IC.Booking_ID END)) AS bookingsIPrefer,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'PHG Brand Sites' THEN IC.Booking_ID END)) AS bookingsPhgBrand,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Voice' THEN IC.Booking_ID END)) AS bookingsVoice,
		COUNT(DISTINCT (CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Booking_ID END)) AS bookingsTotal,
		SUM(CASE WHEN IBC.Channel = 'Direct' THEN IC.Billable_Amount_USD END) AS revenueDirect,
		SUM(CASE WHEN IBC.Channel = 'GDS' THEN IC.Billable_Amount_USD END) AS revenueGDS,
		SUM(CASE WHEN IBC.Channel = 'Hotel IBE' THEN IC.Billable_Amount_USD END) AS revenueHotelIBE,
		SUM(CASE WHEN IBC.Channel = 'iPrefer.com' THEN IC.Billable_Amount_USD END) AS revenueIPrefer,
		SUM(CASE WHEN IBC.Channel = 'PHG Brand Sites' THEN IC.Billable_Amount_USD END) AS revenuePhgBrand,
		SUM(CASE WHEN IBC.Channel = 'Voice' THEN IC.Billable_Amount_USD END) AS revenueVoice,
		SUM(CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Billable_Amount_USD END) AS revenueTotal
		
FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept

WHERE	YEAR(IC.Billing_Date) = YEAR(@endDate)
		AND
		MONTH(IC.Billing_Date) = MONTH(@endDate);

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferChannelPriorYear]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferChannelPriorYear] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Direct' THEN IC.Booking_ID END)) AS bookingsDirect,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'GDS' THEN IC.Booking_ID END)) AS bookingsGDS,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Hotel IBE' THEN IC.Booking_ID END)) AS bookingsHotelIBE,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'iPrefer.com' THEN IC.Booking_ID END)) AS bookingsIPrefer,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'PHG Brand Sites' THEN IC.Booking_ID END)) AS bookingsPhgBrand,
		COUNT(DISTINCT (CASE WHEN IBC.Channel = 'Voice' THEN IC.Booking_ID END)) AS bookingsVoice,
		COUNT(DISTINCT (CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Booking_ID END)) AS bookingsTotal,
		SUM(CASE WHEN IBC.Channel = 'Direct' THEN IC.Billable_Amount_USD END) AS revenueDirect,
		SUM(CASE WHEN IBC.Channel = 'GDS' THEN IC.Billable_Amount_USD END) AS revenueGDS,
		SUM(CASE WHEN IBC.Channel = 'Hotel IBE' THEN IC.Billable_Amount_USD END) AS revenueHotelIBE,
		SUM(CASE WHEN IBC.Channel = 'iPrefer.com' THEN IC.Billable_Amount_USD END) AS revenueIPrefer,
		SUM(CASE WHEN IBC.Channel = 'PHG Brand Sites' THEN IC.Billable_Amount_USD END) AS revenuePhgBrand,
		SUM(CASE WHEN IBC.Channel = 'Voice' THEN IC.Billable_Amount_USD END) AS revenueVoice,
		SUM(CASE WHEN IBC.Channel IN ( 'Direct', 'GDS', 'Hotel IBE', 'iPrefer.com', 'PHG Brand Sites', 'Voice' ) THEN IC.Billable_Amount_USD END) AS revenueTotal
		
FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept

WHERE	IC.Billing_Date BETWEEN DATEADD(YY, -1, @startDate) AND DATEADD(YY, -1, @endDate);

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferRegionBookingsDetail]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferRegionBookingsDetail] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	DISTINCT ISNULL(A.phg_hotelregionname, 'Unknown') AS hotelRegion,
		ISNULL(A.phg_regionaladministrationidname, 'Unknown') AS RD,
		IC.Billing_Date,
		IC.Booking_ID,
		IC.Billable_Amount_USD
		
FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept
		LEFT JOIN
		LocalCRM.dbo.Account A
		ON
		HR.crmGuid = A.accountid

WHERE	IC.Billing_Date BETWEEN @startDate AND @endDate

ORDER BY	hotelRegion,
			RD,
			IC.Billing_Date,
			IC.Booking_ID;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferRegions]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferRegions] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	ISNULL(A.phg_hotelregionname, 'Unknown') AS hotelRegion,
		ISNULL(SUBSTRING(A.phg_regionaladministrationidname, CHARINDEX(' ', A.phg_regionaladministrationidname)+1, len(A.phg_regionaladministrationidname)-(CHARINDEX(' ', A.phg_regionaladministrationidname)-1)), 'Unknown') AS RD,
		MONTH(IC.Billing_Date) AS bookingMonth,
		COUNT(DISTINCT IC.Booking_ID) AS bookingsTotal,
		SUM(IC.Billable_Amount_USD) AS revenueTotal
		
FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept
		LEFT JOIN
		LocalCRM.dbo.Account A
		ON
		HR.crmGuid = A.accountid

WHERE	IC.Billing_Date BETWEEN @startDate AND @endDate

GROUP BY	ISNULL(A.phg_hotelregionname, 'Unknown'),
			ISNULL(SUBSTRING(A.phg_regionaladministrationidname, CHARINDEX(' ', A.phg_regionaladministrationidname)+1, len(A.phg_regionaladministrationidname)-(CHARINDEX(' ', A.phg_regionaladministrationidname)-1)), 'Unknown'),
			MONTH(IC.Billing_Date);

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ips_iPreferRegionsQuarter]'
GO

CREATE PROCEDURE [dbo].[ips_iPreferRegionsQuarter] 
	@startDate DATE,
	@endDate DATE
AS
BEGIN
	SET NOCOUNT ON;

--DECLARE @startDate DATE = '2017-1-1'
--DECLARE @endDate DATE = '2017-10-31'

SELECT	ISNULL(A.phg_hotelregionname, 'Unknown') AS hotelRegion,
		ISNULL(SUBSTRING(A.phg_regionaladministrationidname, CHARINDEX(' ', A.phg_regionaladministrationidname)+1, len(A.phg_regionaladministrationidname)-(CHARINDEX(' ', A.phg_regionaladministrationidname)-1)), 'Unknown') AS RD,
		COUNT(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (1, 2, 3) THEN IC.Booking_ID END)) AS bookingsQ1,
		COUNT(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (4, 5, 6) THEN IC.Booking_ID END)) AS bookingsQ2,
		COUNT(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (7, 8, 9) THEN IC.Booking_ID END)) AS bookingsQ3,
		COUNT(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (10, 11, 12) THEN IC.Booking_ID END)) AS bookingsQ4,
		COUNT(DISTINCT (CASE WHEN YEAR(IC.Billing_Date) = YEAR(@startDate) THEN IC.Booking_ID END)) AS bookingsTotal,
		COUNT(DISTINCT (CASE WHEN YEAR(IC.Billing_Date) = YEAR(@startDate)-1 THEN IC.Booking_ID END)) AS bookingsTotalLY,
		ISNULL(SUM(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (1, 2, 3) THEN IC.Billable_Amount_USD END)), 0) AS revenueQ1,
		ISNULL(SUM(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (4, 5, 6) THEN IC.Billable_Amount_USD END)), 0) AS revenueQ2,
		ISNULL(SUM(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (7, 8, 9) THEN IC.Billable_Amount_USD END)), 0) AS revenueQ3,
		ISNULL(SUM(DISTINCT (CASE WHEN MONTH(IC.Billing_Date) IN (10, 11, 12) THEN IC.Billable_Amount_USD END)), 0) AS revenueQ4,
		ISNULL(SUM(DISTINCT (CASE WHEN YEAR(IC.Billing_Date) = YEAR(@startDate) THEN IC.Billable_Amount_USD END)), 0) AS revenueTotal,
		ISNULL(SUM(DISTINCT (CASE WHEN YEAR(IC.Billing_Date) = YEAR(@startDate)-1 THEN IC.Billable_Amount_USD END)), 0) AS revenueTotalLY
		
FROM	Superset.BSI.IPrefer_Charges IC
		JOIN
		Core.dbo.hotelsReporting HR
		ON
		IC.Hotel_Code = HR.code
		LEFT JOIN
		Superset.BSI.Account_Statement_Import AS ASI
		ON IC.Booking_ID = ASI.Booking_ID
			AND IC.Hotel_Code = ASI.Hotel_Code
		LEFT JOIN
		Superset.dbo.IPreferChannelMapping AS IBC
		ON ASI.Booking_Source = IBC.Concept
		LEFT JOIN
		LocalCRM.dbo.Account A
		ON
		HR.crmGuid = A.accountid

WHERE	IC.Billing_Date BETWEEN DATEADD(YY, -1, @startDate) AND DATEADD(YY, -1, @endDate)
		OR
		IC.Billing_Date BETWEEN @startDate AND @endDate

GROUP BY	ISNULL(A.phg_hotelregionname, 'Unknown'),
			ISNULL(SUBSTRING(A.phg_regionaladministrationidname, CHARINDEX(' ', A.phg_regionaladministrationidname)+1, len(A.phg_regionaladministrationidname)-(CHARINDEX(' ', A.phg_regionaladministrationidname)-1)), 'Unknown')

ORDER BY	hotelRegion,
			RD;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
