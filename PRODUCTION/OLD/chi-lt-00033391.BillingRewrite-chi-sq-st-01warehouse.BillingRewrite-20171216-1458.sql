/*
Run this script on:

        chi-sq-st-01\warehouse.BillingRewrite    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.BillingRewrite

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 12/16/2017 2:58:46 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [test]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping extended properties'
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N'Hotels', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane2', 'SCHEMA', N'dbo', 'VIEW', N'Hotels', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N'Hotels', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [work].[GPCurrencyMaster]'
GO
ALTER TABLE [work].[GPCurrencyMaster] DROP CONSTRAINT [FK_GPCurrencyMaster__RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [work].[GPCustomerTable]'
GO
ALTER TABLE [work].[GPCustomerTable] DROP CONSTRAINT [FK_GPCustomerTable__RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [work].[hotelActiveBrands]'
GO
ALTER TABLE [work].[hotelActiveBrands] DROP CONSTRAINT [FK_hotelActiveBrands__RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [work].[hotelInactiveBrands]'
GO
ALTER TABLE [work].[hotelInactiveBrands] DROP CONSTRAINT [FK_hotelInactiveBrands__RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] DROP CONSTRAINT [FK_local_exchange_rates__RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] DROP CONSTRAINT [FK_MrtForCalculation__RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[GPCurrencyMaster]'
GO
ALTER TABLE [work].[GPCurrencyMaster] DROP CONSTRAINT [PK_work_GPCurrencyMaster]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[GPCustomerTable]'
GO
ALTER TABLE [work].[GPCustomerTable] DROP CONSTRAINT [PK_work_GPCustomerTable]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[hotelActiveBrands]'
GO
ALTER TABLE [work].[hotelActiveBrands] DROP CONSTRAINT [PK_work_hotelActiveBrands]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[hotelInactiveBrands]'
GO
ALTER TABLE [work].[hotelInactiveBrands] DROP CONSTRAINT [PK_work_hotelInactiveBrands]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] DROP CONSTRAINT [PK_work_local_exchange_rates]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] DROP CONSTRAINT [PK_work_MrtForCalculation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping index [IX_RunID_CURNCYID] from [work].[GPCustomerTable]'
GO
DROP INDEX [IX_RunID_CURNCYID] ON [work].[GPCustomerTable]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] DROP
COLUMN [RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_MrtForCalculation] on [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] ADD CONSTRAINT [PK_work_MrtForCalculation] PRIMARY KEY CLUSTERED  ([confirmationNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[MrtForCalc_RulesApplied]'
GO
CREATE TABLE [work].[MrtForCalc_RulesApplied]
(
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[hotelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClauseID] [int] NULL,
[ClauseName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaGroupID] [int] NULL,
[CriteriaGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaID] [int] NULL,
[CriteriaName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceID] [int] NULL,
[SourceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCategoryCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgentGroupID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_MrtForCalc_RulesApplied] on [work].[MrtForCalc_RulesApplied]'
GO
ALTER TABLE [work].[MrtForCalc_RulesApplied] ADD CONSTRAINT [PK_work_MrtForCalc_RulesApplied] PRIMARY KEY CLUSTERED  ([confirmationNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[Populate_MrtForCalc_RulesApplied]'
GO
CREATE PROCEDURE [work].[Populate_MrtForCalc_RulesApplied]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	TRUNCATE TABLE work.MrtForCalc_RulesApplied

	INSERT INTO work.MrtForCalc_RulesApplied(confirmationNumber,hotelCode,hotelName,ClauseID,ClauseName,CriteriaGroupID,
												CriteriaGroupName,CriteriaID,CriteriaName,SourceID,SourceName,
												RateCategoryCode,RateCode,TravelAgentGroupID)
	SELECT DISTINCT mrt.confirmationNumber,mrt.hotelCode,mrt.hotelName,
					GREEN.clauseID as greenClauseID,
					GREEN.clauseName as greenClauseName,
					GREEN.criteriaGroupIncludeID as greenCriteriaGroupID,
					GREEN.criteriaGroupIncludeName as greenCriteriaGroupName,
					GREEN.criteriaGroupIncludeCriteriaIncludeID as greenCriteriaID,
					GREEN.criteriaGroupIncludeCriteriaIncludeName as greenCriteriaName,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID as greenSourceID,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeName as greenSourceName,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode AS greenRateCategoryCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCode AS greenRateCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID AS greenTravelAgentGroupID
	FROM work.MrtForCalculation mrt
		--reservation may or may not have a template in our list of templates
		LEFT JOIN dbo.Templates t ON mrt.xbeTemplateName = t.xbeTemplateName

		--if the res template is not in our list, it is considered a hotel template (group 2)
		LEFT JOIN dbo.TemplateGroups tg ON ISNULL(t.templateGroupID,2) = tg.templateGroupID 

		--reservation may or may not have a CRO Code in our list of templates
		LEFT JOIN dbo.CROCodes cro ON mrt.CROCode = cro.croCode

		--see if the IATA number on the reservation is part of any travel agent groups
		LEFT JOIN Core.dbo.travelAgentIds_travelAgentGroups GREEN_taitag ON mrt.bookingIATA = GREEN_taitag.travelAgentId
								AND mrt.confirmationDate BETWEEN GREEN_taitag.startDate AND GREEN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

		--match to clause by inclusions
		 --CICG_CGIC_SGIS
		INNER JOIN vw_clausesFlattened GREEN ON mrt.hotelCode = GREEN.hotelCode --the clause is for the same hotel as the reservation
			--match the source template group to the reservation's xbe template
			AND (
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID = 0 --included source is using the wildcard group
					OR
					tg.templateGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the included source's template group
				)
			--match the source cro group to the res cro code
			AND (
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID = 0 --included source is using the wildcard group
					OR
					cro.croGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the included source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel = '*' --included source is using the wildcard
					OR
					mrt.bookingChannel = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource = '*' --included source is using the wildcard
					OR
					mrt.bookingSecondarySource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource = '*' --included source is using the wildcard
					OR
					mrt.bookingSubSourceCode = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
			--match criteria travel agent group to res travel agent group
			AND (
					(
						GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID = 0 --included criteria is using wildcard
						OR
						GREEN_taitag.travelAgentGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
				)

			--match criteria rate code to res rate code
			AND (
					(
						GREEN.criteriaGroupIncludeCriteriaIncludeRateCode = '*' --included criteria is using wildcard
						OR
						mrt.bookingRateCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
				)
			--match criteria rate category to res rate category
			AND (
					(
						GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode = '*' --included criteria is using wildcard
						OR
						mrt.bookingRateCategoryCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
				)
			--match criteria loyalty option to res loyalty option
			AND (
					GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
					OR (mrt.LoyaltyNumberValidated = 0 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
					OR (mrt.LoyaltyNumberValidated = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
					OR (mrt.LoyaltyNumberTagged = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
				)

		--see if we match any sources excluded from the matched include criteria
		 --CICG_CGIC_SGES
		LEFT JOIN vw_sourcesFlattened RED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeID = RED.sourceId 
			--make sure the source was excluded at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					RED.templateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = RED.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					RED.CroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = RED.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					RED.channel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = RED.channel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					RED.secondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = RED.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					RED.SubSource = '*' -- source is using the wildcard
					OR
					mrt.bookingSubSourceCode = RED.SubSource --or the value matches exactly
				) 

		--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
		LEFT JOIN Core.dbo.travelAgentIds_travelAgentGroups ORANGE_taitag ON mrt.bookingIATA = ORANGE_taitag.travelAgentId
							AND mrt.confirmationDate BETWEEN ORANGE_taitag.startDate AND ORANGE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

		--see if we match any criteria excluded from the criteria group we matched to
		 --CICG_CGEC_SGIS
		LEFT JOIN vw_criteriaFlattened ORANGE ON GREEN.criteriaGroupIncludeCriteriaExcludeID = ORANGE.criteriaID 
			--the criteria was excluded at the time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					ORANGE.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = ORANGE.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					ORANGE.sourceIncludeCroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = ORANGE.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					ORANGE.sourceIncludeChannel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = ORANGE.sourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					ORANGE.sourceIncludeSecondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = ORANGE.sourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					ORANGE.sourceIncludeSubSource = '*' --source is using the wildcard
					OR
					mrt.bookingSubSourceCode = ORANGE.sourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate
			--match criteria travel agent group to res travel agent group
			AND (
					(
						ORANGE.travelAgentGroupID = 0 --criteria is using wildcard
						OR
						ORANGE_taitag.travelAgentGroupID = ORANGE.travelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND mrt.arrivalDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
				)
			--match criteria rate code to res rate code
			AND (
					(
						ORANGE.rateCode = '*' --criteria is using wildcard
						OR
						mrt.bookingRateCode = ORANGE.rateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND mrt.arrivalDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
				)
			--match criteria rate category to res rate category
			AND (
					(
						ORANGE.rateCategoryCode = '*' --criteria is using wildcard
						OR
						mrt.bookingRateCategoryCode = ORANGE.rateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND mrt.arrivalDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
				)
			--match criteria loyalty option to res loyalty option
			AND (
					ORANGE.loyaltyOptionID = 0 --criteria using wildcard option
					OR (mrt.LoyaltyNumberValidated = 0 AND ORANGE.loyaltyOptionID = 1) --explicitly no valid loyalty number
					OR (mrt.LoyaltyNumberValidated = 1 AND ORANGE.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
					OR (mrt.LoyaltyNumberTagged = 1 AND ORANGE.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
				)

		--see if we match any sources excluded from the excluded criteria
		 --CICG_CGEC_SGES
		LEFT JOIN vw_sourcesFlattened INDIGO ON ORANGE.sourceExcludeID = INDIGO.sourceId 
			--make sure the source was excluded at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					INDIGO.templateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = INDIGO.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					INDIGO.CroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = INDIGO.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					INDIGO.channel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = INDIGO.channel --or the value matches exactly
				) 
	
			--match the source secondary source to the res secondary source
			AND (
					INDIGO.secondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = INDIGO.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					INDIGO.SubSource = '*' --source is using the wildcard
					OR
					mrt.bookingSubSourceCode = INDIGO.SubSource --or the value matches exactly
				) 

		--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
		LEFT JOIN Core.dbo.travelAgentIds_travelAgentGroups BLUE_taitag ON mrt.bookingIATA = BLUE_taitag.travelAgentId
			AND mrt.confirmationDate BETWEEN BLUE_taitag.startDate AND BLUE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

		--see if we match any criteria groups excluded from our matched clause
		 --CECG_CGIC_SGIS
		LEFT JOIN vw_criteriaGroupsFlattened BLUE ON GREEN.criteriaGroupExcludeID = BLUE.criteriaGroupID
			--the criteria group was excluded at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					BLUE.criteriaIncludeSourceIncludeTemplateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = BLUE.criteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					BLUE.criteriaIncludeSourceIncludeCroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = BLUE.criteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					BLUE.criteriaIncludeSourceIncludeChannel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = BLUE.criteriaIncludeSourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					BLUE.criteriaIncludeSourceIncludeSecondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = BLUE.criteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					BLUE.criteriaIncludeSourceIncludeSubSource = '*' --source is using the wildcard
					OR
					mrt.bookingSubSourceCode = BLUE.criteriaIncludeSourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
			--match criteria travel agent group to res travel agent group
			AND (
					(
						BLUE.criteriaIncludeTravelAgentGroupID = 0 --criteria is using wildcard
						OR
						BLUE_taitag.travelAgentGroupID = BLUE.criteriaIncludeTravelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
				)
			--match criteria rate code to res rate code
			AND (
					(
						BLUE.criteriaIncludeRateCode = '*' --criteria is using wildcard
						OR
						mrt.bookingRateCode = BLUE.criteriaIncludeRateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
				)
			--match criteria rate category to res rate category
			AND (
					(
						BLUE.criteriaIncludeRateCategoryCode = '*' --criteria is using wildcard
						OR
						mrt.bookingRateCategoryCode = BLUE.criteriaIncludeRateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
				)
			--match criteria loyalty option to res loyalty option
			AND (
					BLUE.criteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
					OR (mrt.LoyaltyNumberValidated = 0 AND BLUE.criteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
					OR (mrt.LoyaltyNumberValidated = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
					OR (mrt.LoyaltyNumberTagged = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
				)

		--see if we match any sources excluded from the matched criteria group exclusion
		 --CECG_CGIC_SGIS
		LEFT JOIN vw_sourcesFlattened YELLOW ON BLUE.criteriaIncludeSourceExcludeID = YELLOW.sourceId 
			--make sure the source matched was excluded at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					YELLOW.templateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = YELLOW.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					YELLOW.CroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = YELLOW.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					YELLOW.channel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = YELLOW.channel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					YELLOW.secondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = YELLOW.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					YELLOW.SubSource = '*' --source is using the wildcard
					OR
					mrt.bookingSubSourceCode = YELLOW.SubSource --or the value matches exactly
				) 

		--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
		LEFT JOIN Core.dbo.travelAgentIds_travelAgentGroups BROWN_taitag ON mrt.bookingIATA = BROWN_taitag.travelAgentId
			AND mrt.confirmationDate BETWEEN BROWN_taitag.startDate AND BROWN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

		--see if we match a criteria excluded from the criteria group exclusion
		 --CECG_CGEC_SGIS
		LEFT JOIN vw_criteriaFlattened BROWN ON BLUE.criteriaExcludeID = BROWN.criteriaID 
			--the criteria was excluded at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					BROWN.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = BROWN.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					BROWN.sourceIncludeCroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = BROWN.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					BROWN.sourceIncludeChannel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = BROWN.sourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					BROWN.sourceIncludeSecondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = BROWN.sourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					BROWN.sourceIncludeSubSource = '*' --source is using the wildcard
					OR
					mrt.bookingSubSourceCode = BROWN.sourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
			--match criteria travel agent group to res travel agent group
			AND (
					(
						BROWN.travelAgentGroupID = 0 --criteria is using wildcard
						OR
						BROWN_taitag.travelAgentGroupID = BROWN.travelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND mrt.arrivalDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
				)
			--match criteria rate code to res rate code
			AND (
					(
						BROWN.rateCode = '*' --criteria is using wildcard
						OR
						mrt.bookingRateCode = BROWN.rateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND mrt.arrivalDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
			)
			--match criteria rate category to res rate category
			AND (
					(
						BROWN.rateCategoryCode = '*' --criteria is using wildcard
						OR
						mrt.bookingRateCategoryCode = BROWN.rateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND mrt.arrivalDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
				)
			--match criteria loyalty option to res loyalty option
			AND (
					BROWN.loyaltyOptionID = 0 --criteria using wildcard option
					OR (mrt.LoyaltyNumberValidated = 0 AND BROWN.loyaltyOptionID = 1) --explicitly no valid loyalty number
					OR (mrt.LoyaltyNumberValidated = 1 AND BROWN.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
					OR (mrt.LoyaltyNumberTagged = 1 AND BROWN.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
				)

		--see if we match any sources excluded from the excluded criteria
		 --CECG_CGEC_SGES
		LEFT JOIN vw_sourcesFlattened VIOLET ON BROWN.sourceExcludeID = VIOLET.sourceId 
			--make sure the source matched was excluded at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
			--match the source template group to the reservation's xbe template
			AND (
					VIOLET.templateGroupID = 0 --source is using the wildcard group
					OR
					tg.templateGroupID = VIOLET.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					VIOLET.CroGroupID = 0 --source is using the wildcard group
					OR
					cro.croGroupID = VIOLET.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					VIOLET.channel = '*' --source is using the wildcard
					OR
					mrt.bookingChannel = VIOLET.channel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					VIOLET.secondarySource = '*' --source is using the wildcard
					OR
					mrt.bookingSecondarySource = VIOLET.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					VIOLET.SubSource = '*' --source is using the wildcard
					OR
					mrt.bookingSubSourceCode = VIOLET.SubSource --or the value matches exactly
				) 
	WHERE 
	(
		(
			GREEN.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
			AND
			RED.sourceId IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
		)
		AND 
		(
			ORANGE.sourceIncludeID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
			OR
			INDIGO.sourceId IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
		)
	)
	AND
	(
		(
			BLUE.criteriaIncludeSourceIncludeID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
			OR
			YELLOW.sourceId IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
		)
		OR
		(
			BROWN.sourceIncludeID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
			AND
			VIOLET.sourceId IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
		)
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[hotelInactiveBrands]'
GO
ALTER TABLE [work].[hotelInactiveBrands] DROP
COLUMN [RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_hotelInactiveBrands] on [work].[hotelInactiveBrands]'
GO
ALTER TABLE [work].[hotelInactiveBrands] ADD CONSTRAINT [PK_work_hotelInactiveBrands] PRIMARY KEY CLUSTERED  ([hotelCode], [mainHeirarchy])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[hotelActiveBrands]'
GO
ALTER TABLE [work].[hotelActiveBrands] DROP
COLUMN [RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_hotelActiveBrands] on [work].[hotelActiveBrands]'
GO
ALTER TABLE [work].[hotelActiveBrands] ADD CONSTRAINT [PK_work_hotelActiveBrands] PRIMARY KEY CLUSTERED  ([hotelCode], [mainHeirarchy])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] DROP
COLUMN [RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_local_exchange_rates] on [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] ADD CONSTRAINT [PK_work_local_exchange_rates] PRIMARY KEY CLUSTERED  ([EXGTBLID], [CURNCYID], [EXCHDATE])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[GPCustomerTable]'
GO
ALTER TABLE [work].[GPCustomerTable] DROP
COLUMN [RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_GPCustomerTable] on [work].[GPCustomerTable]'
GO
ALTER TABLE [work].[GPCustomerTable] ADD CONSTRAINT [PK_work_GPCustomerTable] PRIMARY KEY CLUSTERED  ([CUSTNMBR])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[GPCurrencyMaster]'
GO
ALTER TABLE [work].[GPCurrencyMaster] DROP
COLUMN [RunID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_work_GPCurrencyMaster] on [work].[GPCurrencyMaster]'
GO
ALTER TABLE [work].[GPCurrencyMaster] ADD CONSTRAINT [PK_work_GPCurrencyMaster] PRIMARY KEY CLUSTERED  ([CURNCYID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[run_LoadWorkTables]'
GO

ALTER PROCEDURE [work].[run_LoadWorkTables]
	@startDate date = null,
	@endDate date = null,
	@hotelCode nvarchar(20) = null,
	@confirmationNumber nvarchar(20) = null
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @RunID int;

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-1,mrt.arrivalDate),
				@endDate = DATEADD(DAY,1,mrt.arrivalDate),
				@hotelCode = COALESCE(synxis.hotelCode,oh.hotelCode)
		FROM Superset.dbo.mostrecenttransactions mrt
			LEFT JOIN Core.dbo.hotels_Synxis synxis ON synxis.synxisID = mrt.hotelID
			LEFT JOIN Core.dbo.hotels_OpenHospitality oh ON oh.openHospitalityCode = mrt.OpenHospitalityID
		WHERE mrt.confirmationNumber = @confirmationNumber
	END

	IF(@startDate IS NULL AND @endDate IS NULL)
	BEGIN
		IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
		BEGIN
			SET @startDate = DATEADD (mm,-1,GETDATE())
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END
		ELSE
		BEGIN
			SET @startDate = GETDATE()
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END

		SET @endDate = DATEADD(YEAR,2,@startDate)
	END
	-----------------------------------------------------------------------------

	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();
	-----------------------------------------------------------------------------

	-- POPULATE [work].[local_exchange_rates] -----------------------------------
	TRUNCATE TABLE [work].[local_exchange_rates];

	INSERT INTO [work].[local_exchange_rates](EXGTBLID,CURNCYID,EXCHDATE,TIME1,XCHGRATE,EXPNDATE,DEX_ROW_ID)
	SELECT EXGTBLID,CURNCYID,EXCHDATE,TIME1,XCHGRATE,EXPNDATE,DEX_ROW_ID
	FROM DYNAMICS.dbo.MC00100
	WHERE
		(
			EXCHDATE BETWEEN DATEADD(YEAR,-1,@startDate) AND @endDate
			OR
			EXPNDATE BETWEEN DATEADD(YEAR,-1,@startDate) AND @endDate
		)
		AND EXGTBLID LIKE '%AVG%'
		AND CURNCYID != 'USD'
	
	UNION ALL

	SELECT 'USD-AVG','USD','1900-01-01','1900-01-01 00:00:00.000',1,'9999-09-09',1
	-----------------------------------------------------------------------------

	-- POPULATE GREAT PLAINS WORKING TABLES -------------------------------------
	TRUNCATE TABLE [work].[GPCustomerTable];

	INSERT INTO [work].[GPCustomerTable](CUSTNMBR,CURNCYID)
	SELECT CUSTNMBR,CURNCYID
	FROM IC.dbo.RM00101


	TRUNCATE TABLE [work].[GPCurrencyMaster];

	INSERT INTO [work].[GPCurrencyMaster](CURNCYID,DECPLCUR)
	SELECT CURNCYID,DECPLCUR
	FROM DYNAMICS.dbo.MC40200
	-----------------------------------------------------------------------------

	-- UPDATE [Core].[dbo].[hotels_OpenHospitality] -----------------------------
		--This updates our Open Hospitality to Core hotel code translation, a bad mapping can cause errors during calculation
	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitality]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);

	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitalityPHG]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);  
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelActiveBrands] ---------------------------------
	TRUNCATE TABLE [work].[hotelActiveBrands];

	INSERT INTO [work].[hotelActiveBrands](hotelCode,mainHeirarchy)
	SELECT hb.hotelCode,MIN(b.heirarchy) AS mainHeirarchy
	FROM Core.dbo.hotels_brands hb
		INNER JOIN Core.dbo.brands AS b ON hb.brandCode = b.code
	WHERE (hb.endDatetime IS NULL)
		AND (hb.startDatetime <= GETDATE())
		OR (GETDATE() BETWEEN hb.startDatetime AND hb.endDatetime)
	GROUP BY hb.hotelCode;
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelInactiveBrands] -------------------------------
	TRUNCATE TABLE [work].[hotelInactiveBrands];

	INSERT INTO [work].[hotelInactiveBrands](hotelCode,mainHeirarchy)
	SELECT hotelCode,MIN(heirarchy) AS mainHeirarchy
	FROM
		(
			SELECT hb.hotelCode,hb.brandCode,b.heirarchy
			FROM Core.dbo.hotels_brands AS hb
				INNER JOIN
					(
						SELECT hotelCode,MAX(endDatetime) AS maxEnd
						FROM Core.dbo.hotels_brands
						GROUP BY hotelCode
					) AS maxDate ON hb.endDatetime = maxDate.maxEnd AND hb.hotelCode = maxDate.hotelCode
				INNER JOIN Core.dbo.brands b ON hb.brandCode = b.code
			GROUP BY hb.hotelCode,hb.brandCode,b.heirarchy
		) AS maxBrands
	GROUP BY hotelCode;

	------------------------------------------------------------------------

	-- POPULATE [work].[MrtForCalculation] ---------------------------------
	TRUNCATE TABLE [work].[MrtForCalculation];

	;WITH cte_mrtj
	AS
	(
		SELECT mrtj.[confirmationNumber],[hotelCode],[SynXisID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
				[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateOptionId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
				[bookingCroOptionID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
				[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookedCurrency],[currencyCodeBooked],[timeLoaded],[CRSSourceID],
				work.[billyItemCode]([synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateAbbreviation],[bookingCroOptionID],[chainID]) AS [ItemCode],
				CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) AS EXCHDATE,
				mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],mrtj.LoyaltyNumberValidated,mrtj.LoyaltyNumberTagged
		FROM work.mrtJoined mrtj
			LEFT JOIN (SELECT confirmationNumber,sopNumber FROM dbo.Charges) cd ON cd.confirmationNumber = mrtj.confirmationNumber AND cd.sopNumber IS NOT NULL
		WHERE mrtj.hotelCode = ISNULL(@hotelCode,mrtj.hotelCode) --either we're running all hotels, or we're just getting a specific hotel
			AND mrtj.confirmationNumber = ISNULL(@confirmationNumber,mrtj.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
			AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
			AND mrtj.hotelCode NOT IN('BCTS4','PHGTEST')
			AND cd.confirmationNumber IS NULL
	)
	INSERT INTO [work].[MrtForCalculation]([confirmationNumber],[hotelCode],[SynXisID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
			[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateOptionId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
			[bookingCroOptionID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
			[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookedCurrency],[currencyCodeBooked],[timeLoaded],[CRSSourceID],
			[ItemCode],
			EXCHDATE,
			[CURNCYID],[DECPLCUR],[hotel_XCHGRATE],[booking_XCHGRATE],loyaltyProgram,loyaltyNumber,[travelAgencyName],
			LoyaltyNumberValidated,LoyaltyNumberTagged)

	SELECT mrtj.[confirmationNumber],[hotelCode],[SynXisID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
			[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateOptionId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
			[bookingCroOptionID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
			[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookedCurrency],[currencyCodeBooked],[timeLoaded],[CRSSourceID],
			work.[billyItemCode]([synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateAbbreviation],[bookingCroOptionID],[chainID]) AS [ItemCode],
			mrtj.EXCHDATE,
			gpCustomer.CURNCYID,hotelCM.DECPLCUR,hotelCE.XCHGRATE,bookingCE.XCHGRATE,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],mrtj.LoyaltyNumberValidated,mrtj.LoyaltyNumberTagged
	FROM cte_mrtj mrtj
		INNER JOIN work.GPCustomerTable gpCustomer ON mrtj.hotelCode = gpCustomer.CUSTNMBR
		INNER JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID AND mrtj.EXCHDATE BETWEEN hotelCE.EXCHDATE AND hotelCE.EXPNDATE
		INNER JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		INNER JOIN work.[local_exchange_rates] bookingCE ON mrtj.currencyCodeBooked = bookingCE.CURNCYID AND mrtj.EXCHDATE BETWEEN bookingCE.EXCHDATE AND bookingCE.EXPNDATE
	------------------------------------------------------------------------

	-- POPULATE MrtForCalc_RulesApplied ------------------------------------
	EXEC work.Populate_MrtForCalc_RulesApplied @RunID
	------------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [test].[Charges]'
GO
CREATE TABLE [test].[Charges]
(
[chargeID] [int] NOT NULL IDENTITY(1, 1),
[billingRuleID] [int] NOT NULL,
[classificationID] [int] NOT NULL,
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[hotelCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[collectionCode] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[clauseName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[arrivalDate] [date] NULL,
[roomNights] [int] NULL,
[hotelCurrencyCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[chargeValueInHotelCurrency] [decimal] (38, 2) NULL,
[roomRevenueInHotelCurrency] [decimal] (38, 2) NULL,
[chargeValueInUSD] [decimal] (38, 2) NULL,
[roomRevenueInUSD] [decimal] (38, 2) NULL,
[itemCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[gpSiteID] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dateCalculated] [datetime] NULL,
[sopNumber] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[invoiceDate] [date] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_test_Charges] on [test].[Charges]'
GO
ALTER TABLE [test].[Charges] ADD CONSTRAINT [PK_test_Charges] PRIMARY KEY NONCLUSTERED  ([chargeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[Calculate_StandardCharges]'
GO

CREATE PROCEDURE [work].[Calculate_StandardCharges]
	@IsTestRun bit = 0
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE TEMP TABLE ---------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES;

	CREATE TABLE #CHARGES
	(
		[billingRuleID] [int] NOT NULL,
		[classificationID] [int] NOT NULL,
		[confirmationNumber] [nvarchar](20) NOT NULL,
		[hotelCode] [nvarchar](10) NOT NULL,
		[collectionCode] [nvarchar](6) NULL,
		[clauseName] [nvarchar](250) NULL,
		[arrivalDate] [date] NULL,
		[roomNights] [int] NULL,
		[hotelCurrencyCode] [char](3) NULL,
		[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
		[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
		[chargeValueInUSD] [decimal](38, 2) NULL,
		[roomRevenueInUSD] [decimal](38, 2) NULL,
		[itemCode] [nvarchar](50) NULL,
		[gpSiteID] [char](2) NULL,
		[dateCalculated] [datetime] NULL,
		[sopNumber] [char](21) NULL,
		[invoiceDate] [date] NULL,
	)
	------------------------------------------------------------------------------------

	
	INSERT INTO #CHARGES(billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,
							clauseName,arrivalDate,roomNights,hotelCurrencyCode,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate)
	SELECT br.billingRuleID,br.classificationID,mrtC.confirmationNumber,mrtC.hotelCode,mrtC.mainBrandCode AS collectionCode,
			rApp.ClauseName,mrtC.arrivalDate,mrtC.roomNights,mrtC.CURNCYID,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * billChargeCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * ((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * billChargeCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * billChargeCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / billChargeCE.XCHGRATE) *  mrtC.hotel_XCHGRATE,mrtC.DECPLCUR-1
			) AS chargeValueInHotelCurrency,
			ROUND((mrtC.roomRevenueInBookedCurrency / mrtc.booking_XCHGRATE) * mrtC.hotel_XCHGRATE,mrtC.DECPLCUR-1) AS roomRevenueInHotelCurrency,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * billChargeCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * 	((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * billChargeCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * billChargeCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / billChargeCE.XCHGRATE) * 1.00, 2
		) AS chargeValueInUSD,
		ROUND((mrtC.roomRevenueInBookedCurrency / bookingCE.XCHGRATE) * 1.00, 2) AS roomRevenueInUSD,
		COALESCE
		(c.itemCodeOverride,CASE cl.classificationName
								WHEN 'Booking' THEN
								--if any of the non-source based criteria fields are anything other than the wildcard, this charge is a 'special booking'
									CASE 
										WHEN (rApp.RateCategoryCode <> '*' OR rApp.RateCode <> '*' OR rApp.TravelAgentGroupID <> 0)
										THEN mrtC.ItemCode + '_SB'
        								ELSE mrtC.ItemCode + '_B'
									END
								WHEN 'Commission' THEN mrtC.ItemCode + '_C'
								WHEN 'Surcharge' THEN mrtC.ItemCode + '_S'
								WHEN 'Non-Billable' THEN NULL
								ELSE NULL
							END
		) AS ItemCode,
		COALESCE(c.gpSiteOverride,mrtC.gpSiteID) AS gpSiteID,
		GETDATE(),NULL,NULL
	FROM BillingRules br
		INNER JOIN work.MrtForCalc_RulesApplied rApp ON rApp.ClauseID = br.clauseID
		INNER JOIN Classifications cl ON cl.classificationID = br.classificationID
		INNER JOIN work.MrtForCalculation mrtC ON mrtC.confirmationNumber = rApp.confirmationNumber
		INNER JOIN work.local_exchange_rates bookingCE ON bookingCE.CURNCYID = mrtC.currencyCodeBooked AND CAST(CASE
																											WHEN mrtC.arrivalDate >= GETDATE() THEN mrtC.confirmationDate
																											ELSE mrtC.arrivaldate END AS date
																									  ) BETWEEN bookingCE.EXCHDATE AND bookingCE.EXPNDATE
		INNER JOIN work.[local_exchange_rates] billChargeCE ON mrtC.CURNCYID = billChargeCE.CURNCYID AND CAST(CASE
																													WHEN mrtC.arrivalDate >= GETDATE() THEN mrtC.confirmationDate
																													ELSE mrtC.arrivaldate END AS date
																											  ) BETWEEN billChargeCE.EXCHDATE AND billChargeCE.EXPNDATE
		INNER JOIN dbo.Criteria c ON c.criteriaID = rApp.CriteriaID
		LEFT JOIN dbo.ThresholdRules tr ON tr.clauseID = rApp.ClauseID
	WHERE tr.clauseID IS NULL
		AND br.classificationID != 4
		AND (
				mrtC.bookingStatus != 'Cancelled'
				OR
				(mrtC.bookingStatus = 'Cancelled' AND br.refundable = 0)
			)
		AND (
				(br.afterConfirmation = 1 AND mrtC.confirmationDate >= br.confirmationDate)
				OR
				(br.afterConfirmation = 0 AND mrtC.confirmationDate <= br.confirmationDate)
			)
		AND mrtC.arrivalDate BETWEEN br.startDate AND br.endDate
	------------------------------------------------------------------------------------

	IF @IsTestRun = 0
	BEGIN
		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE dbo.Charges
		WHERE confirmationNumber IN(SELECT confirmationNumber FROM work.MrtForCalculation)
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO dbo.Charges(billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate)
		SELECT billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate
		FROM #CHARGES
		------------------------------------------------------------------------------------
	END
	ELSE
	BEGIN
		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE test.Charges
		WHERE confirmationNumber IN(SELECT confirmationNumber FROM work.MrtForCalculation)
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO test.Charges(billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate)
		SELECT billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate
		FROM #CHARGES
		------------------------------------------------------------------------------------
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[TestReservationChargesByConfirmationNumber]'
GO

CREATE PROCEDURE work.TestReservationChargesByConfirmationNumber
	@confirmationnumber varchar(20)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	EXEC [work].[run_LoadWorkTables] NULL,NULL,NULL,@confirmationNumber;
	EXEC [work].Calculate_StandardCharges 1

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Hotels]'
GO
ALTER VIEW dbo.Hotels
AS
SELECT        CAST(0 AS bit) AS isTemplate, Core.dbo.hotels.crmGuid, Core.dbo.hotels.code, Core.dbo.hotels_Synxis.synxisID, Core.dbo.hotels.hotelName, 
                         Core.dbo.hotels.physicalCity, Core.dbo.hotels.state, ISO.dbo.subAreas.subAreaName, Core.dbo.hotels.country, ISO.dbo.countries.shortName, 
                         gpCustomer.CURNCYID AS currencyCode, NULL AS geographicRegionCode, Core.dbo.geographicRegions.name AS geographicRegionName, 
                         COALESCE (activeBrands.code, inactiveBrands.code) AS mainBrandCode, COALESCE (account.phg_regionalmanageridname, N'None') 
                         AS phg_regionalmanageridName, COALESCE (account.phg_areamanageridname, N'None') AS phg_areamanageridName, account.statuscodename, 
                         COALESCE (account.phg_revenueaccountmanageridname, N'None') AS phg_revenueaccountmanageridName, 
                         COALESCE (account.phg_regionaladministrationidname, N'None') AS phg_regionaladministrationidName, account.phg_invoicepastduestatus, 
                         RDuser.internalemailaddress AS phg_rdEmailAddress, Core.dbo.hotels_OpenHospitality.openHospitalityCode
FROM            Core.dbo.hotels LEFT OUTER JOIN
                         IC.dbo.RM00101 AS gpCustomer ON Core.dbo.hotels.code = gpCustomer.CUSTNMBR LEFT OUTER JOIN
                             (SELECT        Core.dbo.hotels_brands.hotelCode, MIN(subBrands.heirarchy) AS mainHeirarchy
                               FROM            Core.dbo.hotels_brands INNER JOIN
                                                         Core.dbo.brands AS subBrands ON Core.dbo.hotels_brands.brandCode = subBrands.code
                               WHERE        (Core.dbo.hotels_brands.endDatetime IS NULL) AND (Core.dbo.hotels_brands.startDatetime <= GETDATE()) OR
                                                         (GETDATE() BETWEEN Core.dbo.hotels_brands.startDatetime AND Core.dbo.hotels_brands.endDatetime)
                               GROUP BY Core.dbo.hotels_brands.hotelCode) AS hotelActiveBrands ON Core.dbo.hotels.code = hotelActiveBrands.hotelCode LEFT OUTER JOIN
                         Core.dbo.brands AS activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy LEFT OUTER JOIN
                             (SELECT        hotelCode, MIN(heirarchy) AS mainHeirarchy
                               FROM            (SELECT        hotels_brands_2.hotelCode, hotels_brands_2.brandCode, Core.dbo.brands.heirarchy
                                                         FROM            Core.dbo.hotels_brands AS hotels_brands_2 INNER JOIN
                                                                                       (SELECT        hotelCode, MAX(endDatetime) AS maxEnd
                                                                                         FROM            Core.dbo.hotels_brands AS hotels_brands_1
                                                                                         GROUP BY hotelCode) AS maxDate ON hotels_brands_2.endDatetime = maxDate.maxEnd INNER JOIN
                                                                                   Core.dbo.brands ON hotels_brands_2.brandCode = Core.dbo.brands.code
                                                         GROUP BY hotels_brands_2.hotelCode, hotels_brands_2.brandCode, Core.dbo.brands.heirarchy) AS maxBrands
                               GROUP BY hotelCode) AS hotelInactiveBrands ON Core.dbo.hotels.code = hotelInactiveBrands.hotelCode LEFT OUTER JOIN
                         Core.dbo.brands AS inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy LEFT OUTER JOIN
                         LocalCRM.dbo.Account AS account ON Core.dbo.hotels.crmGuid = account.accountid LEFT OUTER JOIN
                         LocalCRM.dbo.SystemUser AS RDuser ON account.phg_regionalmanagerid = RDuser.systemuserid LEFT OUTER JOIN
                         Core.dbo.hotels_Synxis ON Core.dbo.hotels.code = Core.dbo.hotels_Synxis.hotelCode LEFT OUTER JOIN
                         Core.dbo.hotels_OpenHospitality ON Core.dbo.hotels.code = Core.dbo.hotels_OpenHospitality.hotelCode LEFT OUTER JOIN
                         ISO.dbo.countries ON Core.dbo.hotels.country = ISO.dbo.countries.code2 LEFT OUTER JOIN
                         ISO.dbo.subAreas ON Core.dbo.hotels.country = ISO.dbo.subAreas.countryCode2 AND Core.dbo.hotels.state = ISO.dbo.subAreas.subAreaCode LEFT OUTER JOIN
                         Core.dbo.geographicRegions ON Core.dbo.hotels.geographicRegionCode = Core.dbo.geographicRegions.code
WHERE        (Core.dbo.hotels.code NOT LIKE '%test%') AND (Core.dbo.hotels.code <> 'BCTS4') AND (Core.dbo.hotels_Synxis.synxisID > 0) OR
                         (Core.dbo.hotels.code NOT LIKE '%test%') AND (Core.dbo.hotels.code <> 'BCTS4') AND (Core.dbo.hotels_OpenHospitality.openHospitalityCode IS NOT NULL)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
