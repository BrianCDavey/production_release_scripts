/*
Run this script on:

        chi-sq-pr-01\warehouse.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 2/13/2018 3:09:28 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[Import_CustomerDirect]'
GO
CREATE TABLE [dbo].[Import_CustomerDirect]
(
[QueueId] [int] NOT NULL,
[IsUpdate] [bit] NOT NULL,
[customerDirectId] [int] NOT NULL,
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[hashKey] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[transactionTimeStamp] [datetime] NOT NULL,
[created] [datetime] NOT NULL,
[modified] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Sabre_PopulateMostRecentTransactions]'
GO

ALTER PROCEDURE [dbo].[Sabre_PopulateMostRecentTransactions]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @timeLoaded datetime
	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	-- GET TIME LOADED ---------------------------------
	SELECT @timeLoaded = ISNULL([ImportFinished],[ImportStarted]) FROM [Queue] WHERE QueueID = @QueueID
	----------------------------------------------------

	-- CREATE & POPULATE #NEW_RAW_DATA -----------------
	IF OBJECT_ID('tempdb..#NEW_RAW_DATA') IS NOT NULL
		DROP TABLE #NEW_RAW_DATA;
	CREATE TABLE #NEW_RAW_DATA
	(
		cnt int,
		ConfirmationNumber nvarchar(20),
		maxTimeStamp datetime,
		PRIMARY KEY CLUSTERED(ConfirmationNumber,maxTimeStamp)
	)

	INSERT INTO #NEW_RAW_DATA(cnt,ConfirmationNumber,maxTimeStamp)
	SELECT COUNT(confirmationnumber),confirmationnumber,MAX(transactiontimestamp)
	FROM Superset.dbo.rawData
	WHERE timeloaded = @timeLoaded
	GROUP BY confirmationnumber
	----------------------------------------------------
	
	-- CREATE #INSERTS ---------------------------------
	IF OBJECT_ID('tempdb..#INSERTS') IS NOT NULL
		DROP TABLE #INSERTS;
	CREATE TABLE #INSERTS
	(
		ConfirmationNumber nvarchar(20),
		hashKey binary(16),
		maxTimeStamp datetime,
		maxActionTypeOrder int,
		PRIMARY KEY CLUSTERED(ConfirmationNumber,maxTimeStamp)
	)
	----------------------------------------------------

	-- INSERT INTO #INSERTS (NEW RECORDS) --------------
	INSERT INTO #INSERTS(ConfirmationNumber,hashKey,maxTimeStamp,maxActionTypeOrder)
	SELECT new.ConfirmationNumber,rawd.hashkey,new.maxTimeStamp,rato.actiontypeorder
	FROM #NEW_RAW_DATA new
		INNER JOIN Superset.dbo.RawData rawd ON new.confirmationnumber = rawd.confirmationnumber AND new.maxtimestamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
		LEFT OUTER JOIN Superset.dbo.MostRecentTransactions mrt ON new.confirmationnumber = mrt.confirmationnumber
	WHERE mrt.confirmationnumber IS NULL
		AND new.cnt = 1
	----------------------------------------------------

	-- INSERT INTO #INSERTS (MAX ActionTypeOrder) ------
	;WITH cte_New(confirmationnumber,maxtimestamp,maxActionTypeOrder)
	AS
	(
		SELECT new.confirmationnumber,new.maxtimestamp,MAX(rato.actiontypeorder)
		FROM #NEW_RAW_DATA new
			INNER JOIN Superset.dbo.RawData rawd ON	new.ConfirmationNumber = rawd.confirmationnumber AND new.maxTimeStamp = rawd.transactiontimestamp
			INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
			LEFT OUTER JOIN	Superset.dbo.MostRecentTransactions mrt ON new.ConfirmationNumber = mrt.confirmationnumber
		WHERE mrt.confirmationnumber IS NULL
			AND	new.cnt > 1
		GROUP BY new.ConfirmationNumber,new.maxTimeStamp
	)
	INSERT INTO #INSERTS(ConfirmationNumber,hashKey,maxTimeStamp,maxActionTypeOrder)
	SELECT new.confirmationnumber,MAX(rawd.hashkey),new.maxtimestamp,new.maxActionTypeOrder
	FROM cte_New new
		INNER JOIN Superset.dbo.RawData rawd ON	new.ConfirmationNumber = rawd.confirmationnumber AND new.maxTimeStamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype AND rato.actionTypeOrder = new.maxActionTypeOrder
	GROUP BY new.ConfirmationNumber,new.maxTimeStamp,new.maxActionTypeOrder
	----------------------------------------------------

	-- CREATE #UPDATES ---------------------------------
	IF OBJECT_ID('tempdb..#UPDATES') IS NOT NULL
		DROP TABLE #UPDATES;
	CREATE TABLE #UPDATES
	(
		ConfirmationNumber nvarchar(20),
		maxTimeStamp datetime,
		maxActionTypeOrder int,
	)
	----------------------------------------------------

	-- INSERT INTO #UPDATES (NEW RECORDS) --------------
	INSERT INTO #UPDATES(ConfirmationNumber,maxTimeStamp,maxActionTypeOrder)
	SELECT new.ConfirmationNumber,new.maxTimeStamp,rato.actiontypeorder
	FROM #NEW_RAW_DATA new
		INNER JOIN Superset.dbo.RawData rawd ON new.ConfirmationNumber = rawd.confirmationnumber AND new.maxTimeStamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
		INNER JOIN Superset.dbo.MostRecentTransactions mrt ON new.ConfirmationNumber = mrt.confirmationnumber
	WHERE new.maxTimeStamp > mrt.transactiontimestamp
		AND new.cnt = 1
	----------------------------------------------------

	-- INSERT INTO #UPDATES (MAX ActionTypeOrder) --------------
	INSERT INTO #UPDATES(ConfirmationNumber,maxTimeStamp,maxActionTypeOrder)
	SELECT new.confirmationnumber,new.maxtimestamp,MAX(rato.actiontypeorder)
	FROM #NEW_RAW_DATA new
		INNER JOIN Superset.dbo.RawData rawd ON new.confirmationnumber = rawd.confirmationnumber AND new.maxtimestamp = rawd.transactiontimestamp
	INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
	INNER JOIN Superset.dbo.MostRecentTransactions mrt ON new.confirmationnumber = mrt.confirmationnumber
	WHERE new.maxtimestamp > mrt.transactiontimestamp
		AND	new.cnt > 1
	GROUP BY new.confirmationnumber,new.maxtimestamp
	----------------------------------------------------

	-- INSERT INTO MostRecentTransactions --------------
	;WITH cte_Loyalty([iPrefer_Number],Membership_Date,Email)
	AS
	(
		SELECT [iPrefer_Number],Membership_Date,Email
		FROM [Superset].[BSI].[Customer_Profile_Import]
		WHERE ISDATE(Membership_Date) = 1
	)
	INSERT INTO Superset.dbo.mostrecenttransactions(hashKey,chainName,chainID,hotelName,hotelID,SAPID,hotelCode,billingDescription,transactionTimeStamp,
													faxNotificationCount,channel,secondarySource,subSource,subSourceCode,PMSRateTypeCode,PMSRoomTypeCode,
													marketSourceCode,marketSegmentCode,userName,[status],confirmationNumber,confirmationDate,
													cancellationNumber,cancellationDate,salutation,guestFirstName,guestLastName,customerID,customerAddress1,
													customerAddress2,customerCity,customerState,customerPostalCode,customerPhone,customerCountry,customerArea,
													customerRegion,customerCompanyName,arrivalDate,departureDate,bookingLeadTime,rateCategoryName,
													rateCategoryCode,rateTypeName,rateTypeCode,roomTypeName,roomTypeCode,nights,averageDailyRate,rooms,
													reservationRevenue,currency,IATANumber,travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,
													travelAgencyCity,travelAgencyState,travelAgencyPostalCode,travelAgencyPhone,travelAgencyFax,
													travelAgencyCountry,travelAgencyArea,travelAgencyRegion,travelAgencyEmail,consortiaCount,consortiaName,
													totalPackageRevenue,optIn,customerEmail,totalGuestCount,adultCount,childrenCount,creditCardType,
													actionType,shareWith,arrivalDOW,departureDOW,itineraryNumber,secondaryCurrency,
													secondaryCurrencyExchangeRate,secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,
													secondaryCurrencyPackageRevenue,commisionPercent,membershipNumber,corporationCode,promotionalCode,
													CROCode,channelConnectConfirmationNumber,primaryGuest,loyaltyProgram,loyaltyNumber,vipLevel,
													xbeTemplateName,xbeShellName,profileTypeSelection,averageDailyRateUSD,reservationRevenueUSD,
													totalPackageRevenueUSD,timeLoaded,OpenHospitalityID,CRSSourceID,LoyaltyNumberValidated)
	SELECT rawd.hashKey,chainName,chainID,hotelName,hotelID,SAPID,hotelCode,billingDescription,transactionTimeStamp,
			faxNotificationCount,channel,secondarySource,subSource,subSourceCode,PMSRateTypeCode,PMSRoomTypeCode,
			marketSourceCode,marketSegmentCode,userName,[status],rawd.confirmationNumber,confirmationDate,
			cancellationNumber,cancellationDate,salutation,guestFirstName,guestLastName,customerID,customerAddress1,
			customerAddress2,customerCity,customerState,customerPostalCode,customerPhone,customerCountry,customerArea,
			customerRegion,customerCompanyName,arrivalDate,departureDate,bookingLeadTime,rateCategoryName,
			rateCategoryCode,rateTypeName,rateTypeCode,roomTypeName,roomTypeCode,nights,averageDailyRate,rooms,
			reservationRevenue,currency,IATANumber,travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,
			travelAgencyCity,travelAgencyState,travelAgencyPostalCode,travelAgencyPhone,travelAgencyFax,
			travelAgencyCountry,travelAgencyArea,travelAgencyRegion,travelAgencyEmail,consortiaCount,consortiaName,
			totalPackageRevenue,
			CASE [optIn] WHEN 'Y' THEN 1 ELSE 0 END,
			customerEmail,totalGuestCount,adultCount,childrenCount,creditCardType,
			actionType,shareWith,arrivalDOW,departureDOW,itineraryNumber,secondaryCurrency,
			secondaryCurrencyExchangeRate,secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,
			secondaryCurrencyPackageRevenue,commisionPercent,membershipNumber,corporationCode,promotionalCode,
			CROCode,channelConnectConfirmationNumber,
			CASE [primaryGuest] WHEN 'Y' THEN 1 ELSE 0 END,
			loyaltyProgram,
			COALESCE(rawd.loyaltyNumber,CASE WHEN NULLIF(temp.xbeTemplateName,'') IS NULL THEN NULL ELSE temp_cpi.iPrefer_Number END) AS loyaltyNumber,
			vipLevel,
			rawd.xbeTemplateName,xbeShellName,profileTypeSelection,averageDailyRateUSD,reservationRevenueUSD,
			totalPackageRevenueUSD,timeLoaded,NULL,1,
			CASE
				WHEN COALESCE(cpi.iPrefer_Number,CASE
													WHEN NULLIF(temp.xbeTemplateName,'') IS NULL THEN NULL
													ELSE temp_cpi.iPrefer_Number
												  END
							 ) IS NOT NULL THEN 1
				ELSE 0
			END AS LoyaltyNumberValidated
	FROM #INSERTS i
		INNER JOIN Superset.dbo.rawData rawd ON i.ConfirmationNumber = rawd.confirmationnumber	AND i.maxTimeStamp = rawd.transactiontimestamp AND i.hashKey = rawd.hashKey
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actionType = rato.phgactiontype AND rato.actiontypeorder = i.maxActionTypeOrder
		LEFT JOIN cte_Loyalty cpi ON cpi.iPrefer_Number = rawd.loyaltyNumber
									AND rawd.confirmationDate >= cpi.Membership_Date
									AND rawd.arrivalDate >= @firstDayOfMonth
		LEFT JOIN [BillingProduction].[dbo].[templateOptions_xbeTemplateNames] temp ON temp.xbeTemplateName = rawd.xbeTemplateName AND temp.templateOptionId = 3
		LEFT JOIN cte_Loyalty temp_cpi ON cpi.Email = rawd.customerEmail
									AND rawd.confirmationDate >= temp_cpi.Membership_Date
									AND rawd.arrivalDate >= @firstDayOfMonth
	----------------------------------------------------

	-- UPDATE MostRecentTransactions -------------------
	;WITH cte_Loyalty([iPrefer_Number],Membership_Date,Email)
	AS
	(
		SELECT [iPrefer_Number],Membership_Date,Email
		FROM [Superset].[BSI].[Customer_Profile_Import]
		WHERE ISDATE(Membership_Date) = 1
	)
	UPDATE MRT
		SET	[hashKey] = rawd.[hashKey],
			[chainName] = rawd.[chainName],
			[chainID] = rawd.[chainID],
			[hotelName] = rawd.[hotelName],
			[hotelID] = rawd.[hotelID],
			[SAPID] = rawd.[SAPID],
			[hotelCode] = rawd.[hotelCode],
			[billingDescription] = rawd.[billingDescription],
			[transactionTimeStamp] = rawd.[transactionTimeStamp],
			[faxNotificationCount] = rawd.[faxNotificationCount],
			[channel] = rawd.[channel],
			[secondarySource] = rawd.[secondarySource],
			[subSource] = rawd.[subSource],
			[subSourceCode] = rawd.[subSourceCode],
			[PMSRateTypeCode] = rawd.[PMSRateTypeCode],
			[PMSRoomTypeCode] = rawd.[PMSRoomTypeCode],
			[marketSourceCode] = rawd.[marketSourceCode],
			[marketSegmentCode] = rawd.[marketSegmentCode],
			[userName] = rawd.[userName],
			[status] = rawd.[status],
			[confirmationDate] = rawd.[confirmationDate],
			[cancellationNumber] = rawd.[cancellationNumber],
			[cancellationDate] = rawd.[cancellationDate],
			[salutation] = rawd.[salutation],
			[guestFirstName] = rawd.[guestFirstName],
			[guestLastName] = rawd.[guestLastName],
			[customerID] = rawd.[customerID],
			[customerAddress1] = rawd.[customerAddress1],
			[customerAddress2] = rawd.[customerAddress2],
			[customerCity] = rawd.[customerCity],
			[customerState] = rawd.[customerState],
			[customerPostalCode] = rawd.[customerPostalCode],
			[customerPhone] = rawd.[customerPhone],
			[customerCountry] = rawd.[customerCountry],
			[customerArea] = rawd.[customerArea],
			[customerRegion] = rawd.[customerRegion],
			[customerCompanyName] = rawd.[customerCompanyName],
			[arrivalDate] = rawd.[arrivalDate],
			[departureDate] = rawd.[departureDate],
			[bookingLeadTime] = rawd.[bookingLeadTime],
			[rateCategoryName] = rawd.[rateCategoryName],
			[rateCategoryCode] = rawd.[rateCategoryCode],
			[rateTypeName] = rawd.[rateTypeName],
			[rateTypeCode] = rawd.[rateTypeCode],
			[roomTypeName] = rawd.[roomTypeName],
			[roomTypeCode] = rawd.[roomTypeCode],
			[nights] = rawd.[nights],
			[averageDailyRate] = rawd.[averageDailyRate],
			[rooms] = rawd.[rooms],
			[reservationRevenue] = rawd.[reservationRevenue],
			[currency] = rawd.[currency],
			[IATANumber] = rawd.[IATANumber],
			[travelAgencyName] = rawd.[travelAgencyName],
			[travelAgencyAddress1] = rawd.[travelAgencyAddress1],
			[travelAgencyAddress2] = rawd.[travelAgencyAddress2],
			[travelAgencyCity] = rawd.[travelAgencyCity],
			[travelAgencyState] = rawd.[travelAgencyState],
			[travelAgencyPostalCode] = rawd.[travelAgencyPostalCode],
			[travelAgencyPhone] = rawd.[travelAgencyPhone],
			[travelAgencyFax] = rawd.[travelAgencyFax],
			[travelAgencyCountry] = rawd.[travelAgencyCountry],
			[travelAgencyArea] = rawd.[travelAgencyArea],
			[travelAgencyRegion] = rawd.[travelAgencyRegion],
			[travelAgencyEmail] = rawd.[travelAgencyEmail],
			[consortiaCount] = rawd.[consortiaCount],
			[consortiaName] = rawd.[consortiaName],
			[totalPackageRevenue] = rawd.[totalPackageRevenue],
			[optIn] = CASE rawd.[optIn] WHEN 'Y' THEN 1 ELSE 0 END,
			[customerEmail] = rawd.[customerEmail],
			[totalGuestCount] = rawd.[totalGuestCount],
			[adultCount] = rawd.[adultCount],
			[childrenCount] = rawd.[childrenCount],
			[creditCardType] = rawd.[creditCardType],
			[actionType] = rawd.[actionType],
			[shareWith] = rawd.[shareWith],
			[arrivalDOW] = rawd.[arrivalDOW],
			[departureDOW] = rawd.[departureDOW],
			[itineraryNumber] = rawd.[itineraryNumber],
			[secondaryCurrency] = rawd.[secondaryCurrency],
			[secondaryCurrencyExchangeRate] = rawd.[secondaryCurrencyExchangeRate],
			[secondaryCurrencyAverageDailyRate] = rawd.[secondaryCurrencyAverageDailyRate],
			[secondaryCurrencyReservationRevenue] = rawd.[secondaryCurrencyReservationRevenue],
			[secondaryCurrencyPackageRevenue] = rawd.[secondaryCurrencyPackageRevenue],
			[commisionPercent] = rawd.[commisionPercent],
			[membershipNumber] = rawd.[membershipNumber],
			[corporationCode] = rawd.[corporationCode],
			[promotionalCode] = rawd.[promotionalCode],
			[CROCode] = rawd.[CROCode],
			[channelConnectConfirmationNumber] = rawd.[channelConnectConfirmationNumber],
			[primaryGuest] = CASE rawd.[primaryGuest] WHEN 'Y' THEN 1 ELSE 0 END,
			[loyaltyProgram] = rawd.[loyaltyProgram],
			[loyaltyNumber] = COALESCE(rawd.loyaltyNumber,CASE WHEN NULLIF(temp.xbeTemplateName,'') IS NULL THEN NULL ELSE temp_cpi.iPrefer_Number END),
			[vipLevel] = rawd.[vipLevel],
			[xbeTemplateName] = rawd.[xbeTemplateName],
			[xbeShellName] = rawd.[xbeShellName],
			[profileTypeSelection] = rawd.[profileTypeSelection],
			[averageDailyRateUSD] = rawd.[averageDailyRateUSD],
			[reservationRevenueUSD] = rawd.[reservationRevenueUSD],
			[totalPackageRevenueUSD] = rawd.[totalPackageRevenueUSD],
			[timeLoaded] = rawd.[timeLoaded],
			[LoyaltyNumberValidated] =	CASE WHEN COALESCE(cpi.iPrefer_Number,	CASE
																					WHEN NULLIF(temp.xbeTemplateName,'') IS NULL THEN NULL
																					ELSE temp_cpi.iPrefer_Number
																				END
														  ) IS NOT NULL THEN 1
											ELSE 0
										END
	FROM #UPDATES u
		INNER JOIN Superset.dbo.MostRecentTransactions mrt ON u.confirmationnumber = mrt.Confirmationnumber
		INNER JOIN Superset.dbo.rawData rawd ON mrt.confirmationnumber = rawd.confirmationnumber AND u.maxtimestamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype AND rato.actiontypeorder = u.maxactiontypeorder
		LEFT JOIN cte_Loyalty cpi ON cpi.iPrefer_Number = rawd.loyaltyNumber
									AND rawd.confirmationDate >= cpi.Membership_Date
									AND rawd.arrivalDate >= @firstDayOfMonth
		LEFT JOIN [BillingProduction].[dbo].[templateOptions_xbeTemplateNames] temp ON temp.xbeTemplateName = rawd.xbeTemplateName AND temp.templateOptionId = 3
		LEFT JOIN cte_Loyalty temp_cpi ON cpi.Email = rawd.customerEmail
										AND rawd.confirmationDate >= temp_cpi.Membership_Date
										AND rawd.arrivalDate >= @firstDayOfMonth
	----------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Import_IPTaggedCustomers]'
GO
CREATE TABLE [dbo].[Import_IPTaggedCustomers]
(
[QueueID] [int] NOT NULL,
[Hotel_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iPrefer Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[taggedStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTagged] [date] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPTaggedCustomers_FinalImport]'
GO

CREATE PROCEDURE [dbo].[IPTaggedCustomers_FinalImport]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	-------------------------------------------------------------------
	INSERT INTO Superset.BSI.TaggedCustomers([Hotel_Code],[iPrefer Number],[taggedStatus],[DateTagged])
	SELECT ip.[Hotel_Code],ip.[iPrefer Number],ip.[taggedStatus],ip.[DateTagged]
	FROM dbo.Import_IPTaggedCustomers ip
		LEFT JOIN Superset.BSI.TaggedCustomers tag ON tag.Hotel_Code = ip.Hotel_Code AND tag.[iPrefer Number] = ip.[iPrefer Number]
	WHERE QueueID = @QueueID
		AND tag.Hotel_Code IS NULL

	UPDATE tag
		SET taggedStatus = ip.taggedStatus,
			DateTagged = ip.DateTagged
	FROM Superset.BSI.TaggedCustomers tag
		INNER JOIN dbo.Import_IPTaggedCustomers ip ON ip.[Hotel_Code] = tag.[Hotel_Code] AND ip.[iPrefer Number] = tag.[iPrefer Number]
	WHERE ip.QueueID = @QueueID
	-------------------------------------------------------------------

	-------------------------------------------------------------------
	UPDATE mrt
		SET LoyaltyNumberTagged = 1
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN dbo.Import_IPTaggedCustomers tag ON tag.[iPrefer Number] = mrt.loyaltyNumber AND tag.Hotel_Code = mrt.hotelCode
	WHERE tag.QueueID = @QueueID
		AND mrt.confirmationDate >= tag.DateTagged
		AND mrt.arrivalDate >= @firstDayOfMonth
		

	UPDATE mrt
		SET LoyaltyNumberTagged = 1
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN [Superset].[BSI].[Customer_Profile_Import] cust ON cust.Email = mrt.customerEmail
		INNER JOIN dbo.Import_IPTaggedCustomers tag ON tag.[iPrefer Number] = cust.iPrefer_Number AND tag.Hotel_Code = mrt.hotelCode
		INNER JOIN [BillingProduction].[dbo].[templateOptions_xbeTemplateNames] temp ON temp.xbeTemplateName = mrt.xbeTemplateName
	WHERE tag.QueueID = @QueueID
		AND mrt.confirmationDate >= tag.DateTagged
		AND mrt.arrivalDate >= @firstDayOfMonth
		AND temp.templateOptionId = 3
	-------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPTaggedCustomers_EmailSuccess]'
GO


CREATE PROCEDURE [dbo].[IPTaggedCustomers_EmailSuccess]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE	@emailbody nvarchar(MAX)
	SET	@emailbody = 'The Tagged Customer Import process has completed successfully:

Please contact the IT Help (ithelp@preferredhotelgroup.com) if there are any issues with the report.'

	EXEC	msdb.dbo.sp_send_dbmail
		@profile_name = 'Notify',
		@recipients = 'dbadmins@preferredhotels.com',
		@subject = 'IPrefer Tagged Customer Import Process Complete' ,
		@body = @emailbody
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPCustProf_UpdateLoyaltyNumber]'
GO

CREATE PROCEDURE [dbo].[IPCustProf_UpdateLoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	------------------------------------------------------------------------
	;WITH cte_Loyalty([iPrefer_Number],Membership_Date,value)
	AS
	(
		SELECT [iPrefer_Number],Membership_Date, 1
		FROM [Superset].[BSI].[Customer_Profile_Import]
		WHERE ISDATE(Membership_Date) = 1
	)
	UPDATE mrt
		SET LoyaltyNumberValidated = ISNULL(c.value,0)
	FROM Superset.dbo.mostrecenttransactions mrt
		LEFT OUTER JOIN cte_Loyalty c ON c.[iPrefer_Number] = mrt.loyaltyNumber
	WHERE mrt.confirmationDate >= c.Membership_Date
		AND mrt.arrivalDate >= @firstDayOfMonth
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	;WITH cte_Loyalty([iPrefer_Number],Membership_Date,Email,value)
	AS
	(
		SELECT [iPrefer_Number],Membership_Date,Email, 1
		FROM [Superset].[BSI].[Customer_Profile_Import]
		WHERE ISDATE(Membership_Date) = 1
	)
	UPDATE mrt
		SET loyaltyNumber = c.[iPrefer_Number],
			LoyaltyNumberValidated = ISNULL(c.value,0)
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN [BillingProduction].[dbo].[templateOptions_xbeTemplateNames] temp ON temp.xbeTemplateName = mrt.xbeTemplateName
		LEFT OUTER JOIN cte_Loyalty c ON c.Email = mrt.customerEmail
	WHERE temp.templateOptionId = 3
		AND ISNULL(mrt.loyaltyNumber,'') = ''
		AND mrt.confirmationDate >= c.Membership_Date
		AND mrt.arrivalDate >= @firstDayOfMonth
	------------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
