/*
Run this script on:

        chi-sq-st-01\warehouse.BillingRewrite    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.BillingRewrite

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 1/8/2018 10:38:58 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] DROP CONSTRAINT [PK_work_local_exchange_rates]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[MrtForCalc_RulesApplied]'
GO
ALTER TABLE [work].[MrtForCalc_RulesApplied] DROP CONSTRAINT [PK_work_MrtForCalc_RulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[MrtForCalculation]'
GO
EXEC sp_rename N'[work].[MrtForCalculation].[hotelCode]', N'phgHotelCode', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[synxisID]', N'crsHotelID', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[bookingTemplateOptionId]', N'bookingTemplateGroupID', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[bookingCroOptionID]', N'bookingCroGroupID', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[roomRevenueInBookedCurrency]', N'roomRevenueInBookingCurrency', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[currencyCodeBooked]', N'bookingCurrencyCode', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[EXCHDATE]', N'exchangeDate', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[CURNCYID]', N'hotelCurrencyCode', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[DECPLCUR]', N'hotelCurrencyDecimalPlaces', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[hotel_XCHGRATE]', N'hotelCurrencyExchangeRate', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[MrtForCalculation].[booking_XCHGRATE]', N'bookingCurrencyExchangeRate', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [work].[MrtForCalc_RulesApplied]'
GO
CREATE TABLE [work].[tmp_rg_xx_MrtForCalc_RulesApplied]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[phgHotelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClauseID] [int] NULL,
[ClauseName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaGroupID] [int] NULL,
[CriteriaGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaID] [int] NULL,
[CriteriaName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceID] [int] NULL,
[SourceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCategoryCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgentGroupID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [work].[tmp_rg_xx_MrtForCalc_RulesApplied]([confirmationNumber], [phgHotelCode], [hotelName], [ClauseID], [ClauseName], [CriteriaGroupID], [CriteriaGroupName], [CriteriaID], [CriteriaName], [SourceID], [SourceName], [RateCategoryCode], [RateCode], [TravelAgentGroupID]) SELECT [confirmationNumber], [hotelCode], [hotelName], [ClauseID], [ClauseName], [CriteriaGroupID], [CriteriaGroupName], [CriteriaID], [CriteriaName], [SourceID], [SourceName], [RateCategoryCode], [RateCode], [TravelAgentGroupID] FROM [work].[MrtForCalc_RulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [work].[MrtForCalc_RulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[work].[tmp_rg_xx_MrtForCalc_RulesApplied]', N'MrtForCalc_RulesApplied'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MrtForCalc_RulesApplied] on [work].[MrtForCalc_RulesApplied]'
GO
ALTER TABLE [work].[MrtForCalc_RulesApplied] ADD CONSTRAINT [PK_MrtForCalc_RulesApplied] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[Populate_MrtForCalc_RulesApplied]'
GO
ALTER PROCEDURE [work].[Populate_MrtForCalc_RulesApplied]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	TRUNCATE TABLE work.MrtForCalc_RulesApplied

	INSERT INTO work.MrtForCalc_RulesApplied(confirmationNumber,phgHotelCode,hotelName,ClauseID,ClauseName,CriteriaGroupID,
												CriteriaGroupName,CriteriaID,CriteriaName,SourceID,SourceName,
												RateCategoryCode,RateCode,TravelAgentGroupID)
	SELECT DISTINCT mrt.confirmationNumber,mrt.phgHotelCode,mrt.hotelName,
					GREEN.clauseID as greenClauseID,
					GREEN.clauseName as greenClauseName,
					GREEN.criteriaGroupIncludeID as greenCriteriaGroupID,
					GREEN.criteriaGroupIncludeName as greenCriteriaGroupName,
					GREEN.criteriaGroupIncludeCriteriaIncludeID as greenCriteriaID,
					GREEN.criteriaGroupIncludeCriteriaIncludeName as greenCriteriaName,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID as greenSourceID,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeName as greenSourceName,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode AS greenRateCategoryCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCode AS greenRateCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID AS greenTravelAgentGroupID

	FROM [work].[MrtForCalculation] mrt

	--see if the IATA number on the reservation is part of any travel agent groups
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups GREEN_taitag ON mrt.bookingIATA = GREEN_taitag.travelAgentId AND mrt.confirmationDate BETWEEN GREEN_taitag.startDate AND GREEN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--match to clause by inclusions--CICG_CGIC_SGIS
	INNER JOIN vw_clausesFlattened GREEN ON mrt.phgHotelCode = GREEN.hotelCode --the clause is for the same hotel as the reservation
		--match the source template group to the reservation's xbe template
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID = 0 --included source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the included source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID = 0 --included source is using the wildcard group
			OR
			mrt.bookingCroGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the included source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel = '*' --included source is using the wildcard
			OR
			mrt.bookingChannel = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource = '*' --included source is using the wildcard
			OR
			mrt.bookingSecondarySource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource = '*' --included source is using the wildcard
			OR
			mrt.bookingSubSourceCode = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID = 0 --included criteria is using wildcard
				OR
				GREEN_taitag.travelAgentGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)
	--see if we match any sources excluded from the matched include criteria --CICG_CGIC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened RED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeID = RED.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			RED.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = RED.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			RED.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = RED.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			RED.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = RED.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			RED.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = RED.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			RED.SubSource = '*' -- source is using the wildcard
			OR
			mrt.bookingSubSourceCode = RED.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups ORANGE_taitag
		ON mrt.bookingIATA = ORANGE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN ORANGE_taitag.startDate AND ORANGE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria excluded from the criteria group we matched to
	LEFT OUTER JOIN vw_criteriaFlattened ORANGE --CICG_CGEC_SGIS
		ON GREEN.criteriaGroupIncludeCriteriaExcludeID = ORANGE.criteriaID 

		--the criteria was excluded at the time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate

		--match the source template group to the reservation's xbe template
		AND (ORANGE.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR mrt.bookingTemplateGroupID = ORANGE.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (ORANGE.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR mrt.bookingCroGroupID = ORANGE.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (ORANGE.sourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = ORANGE.sourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND (ORANGE.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR mrt.bookingSecondarySource = ORANGE.sourceIncludeSecondarySource --or the value matches exactly
		)
	 
		--match the source sub source to the res sub source
		AND (ORANGE.sourceIncludeSubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = ORANGE.sourceIncludeSubSource --or the value matches exactly
		) 

		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate

		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				ORANGE.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				ORANGE_taitag.travelAgentGroupID = ORANGE.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				ORANGE.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = ORANGE.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				ORANGE.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = ORANGE.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			ORANGE.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND ORANGE.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND ORANGE.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND ORANGE.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CICG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened INDIGO ON ORANGE.sourceExcludeID = INDIGO.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			INDIGO.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = INDIGO.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			INDIGO.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = INDIGO.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			INDIGO.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = INDIGO.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			INDIGO.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = INDIGO.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND (INDIGO.SubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = INDIGO.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BLUE_taitag
		ON mrt.bookingIATA = BLUE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BLUE_taitag.startDate AND BLUE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria groups excluded from our matched clause
	LEFT OUTER JOIN vw_criteriaGroupsFlattened BLUE --CECG_CGIC_SGIS
		ON GREEN.criteriaGroupExcludeID = BLUE.criteriaGroupID

		--the criteria group was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate


		--match the source template group to the reservation's xbe template
		AND (BLUE.criteriaIncludeSourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR mrt.bookingTemplateGroupID = BLUE.criteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (BLUE.criteriaIncludeSourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR mrt.bookingCroGroupID = BLUE.criteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (BLUE.criteriaIncludeSourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = BLUE.criteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BLUE.criteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BLUE.criteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BLUE.criteriaIncludeTravelAgentGroupID = 0 --criteria is using wildcard
				OR
				BLUE_taitag.travelAgentGroupID = BLUE.criteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BLUE.criteriaIncludeRateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BLUE.criteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BLUE.criteriaIncludeRateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BLUE.criteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BLUE.criteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BLUE.criteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the matched criteria group exclusion --CECG_CGIC_SGIS
	LEFT OUTER JOIN vw_sourcesFlattened YELLOW ON BLUE.criteriaIncludeSourceExcludeID = YELLOW.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			YELLOW.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = YELLOW.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			YELLOW.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = YELLOW.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			YELLOW.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = YELLOW.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			YELLOW.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = YELLOW.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			YELLOW.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = YELLOW.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BROWN_taitag ON mrt.bookingIATA = BROWN_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BROWN_taitag.startDate AND BROWN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match a criteria excluded from the criteria group exclusion --CECG_CGEC_SGIS
	LEFT OUTER JOIN vw_criteriaFlattened BROWN ON BLUE.criteriaExcludeID = BROWN.criteriaID 
		--the criteria was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			BROWN.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = BROWN.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			BROWN.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = BROWN.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			BROWN.sourceIncludeChannel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = BROWN.sourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			BROWN.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BROWN.sourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BROWN.sourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BROWN.sourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BROWN.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				BROWN_taitag.travelAgentGroupID = BROWN.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BROWN.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BROWN.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BROWN.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BROWN.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BROWN.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BROWN.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BROWN.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BROWN.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CECG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened VIOLET ON BROWN.sourceExcludeID = VIOLET.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			VIOLET.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = VIOLET.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			VIOLET.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = VIOLET.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			VIOLET.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = VIOLET.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			VIOLET.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = VIOLET.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			VIOLET.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = VIOLET.SubSource --or the value matches exactly
		) 

	WHERE 
	(
		(
			GREEN.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
			AND RED.sourceId IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
		)
		AND 
		(
			ORANGE.sourceIncludeID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
			OR INDIGO.sourceId IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
		)
	)
	AND
	(
		(
			BLUE.criteriaIncludeSourceIncludeID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
			OR YELLOW.sourceId IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
		)
		OR
		(
			BROWN.sourceIncludeID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
			AND VIOLET.sourceId IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
		)
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[mrtJoined]'
GO

---------------------------------------------------------------------

---------------------------------------------------------------------
ALTER VIEW [work].[mrtJoined]
AS
     SELECT 
            mrt.confirmationNumber,
			COALESCE(SynxisHotels.code, OHHotels.Code) AS phgHotelCode,
            COALESCE(SynxisHotels.synxisID, OHHotels.openHospitalityCode) AS crsHotelID,
            COALESCE(synxisHotels.hotelName, OHHotels.Hotelname) AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            mrt.chainID,
            mrt.chainName,
            mrt.status AS bookingStatus,
            mrt.billingDescription AS synxisBillingDescription,
            mrt.channel AS bookingChannel,
            mrt.secondarySource AS bookingSecondarySource,
            mrt.subSourceCode AS bookingSubSourceCode,
            COALESCE(t.[templateID], 2) AS bookingTemplateGroupId,
            COALESCE(t.siteAbbreviation, 'HOTEL') AS bookingTemplateAbbreviation,
            mrt.xbeTemplateName,
            mrt.CROcode,
            CASE WHEN mrt.CROCode IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            mrt.rateCategoryCode AS bookingRateCategoryCode,
            mrt.rateTypeCode AS bookingRateCode,
            mrt.IATANumber AS bookingIATA,
            mrt.transactionTimeStamp,
            mrt.confirmationDate,
            mrt.arrivalDate,
            mrt.departureDate,
            mrt.cancellationDate,
            mrt.cancellationNumber,
            mrt.nights,
            mrt.rooms,
            mrt.nights * mrt.rooms AS roomNights,
            mrt.reservationRevenue AS roomRevenueInBookingCurrency,
            mrt.currency AS bookingCurrencyCode,
            mrt.timeLoaded,
            mrt.CRSSourceID,
			mrt.loyaltyProgram,
			mrt.loyaltyNumber,
			mrt.travelAgencyName,
			mrt.LoyaltyNumberValidated,
			mrt.LoyaltyNumberTagged

     FROM Superset.dbo.mostrecenttransactions mrt
          LEFT JOIN Core.dbo.hotelsReporting synxisHotels ON synxisHotels.synxisID = mrt.hotelID
          LEFT JOIN Core.dbo.hotelsReporting OHHotels ON OHHotels.openhospitalityCode = mrt.OpenHospitalityID 
          LEFT JOIN work.hotelActiveBrands ON COALESCE(SynxisHotels.code, OHHotels.Code) = hotelActiveBrands.hotelCode 
          LEFT JOIN Core.dbo.brands activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy
          LEFT JOIN work.hotelInactiveBrands ON COALESCE(SynxisHotels.code, OHHotels.Code) = hotelInactiveBrands.hotelCode
          LEFT JOIN Core.dbo.brands inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy
          LEFT JOIN [dbo].[CROCodes] ON mrt.CROCode = croCodes.croCode
		  LEFT JOIN dbo.Templates t ON t.xbeTemplateName = mrt.xbeTemplateName

     WHERE mrt.channel <> 'PMS Rez Synch' --we do not bill bookings made on property
	 AND (synxisHotels.code IS NULL OR synxisHotels.code NOT IN ('PHGTEST','BCTS4'))
	 AND (OHHotels.code IS NULL OR OHHotels.code NOT IN ('PHGTEST','BCTS4'))

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] DROP
COLUMN [EXGTBLID],
COLUMN [TIME1],
COLUMN [EXPNDATE],
COLUMN [DEX_ROW_ID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_local_exchange_rates] on [work].[local_exchange_rates]'
GO
ALTER TABLE [work].[local_exchange_rates] ADD CONSTRAINT [PK_local_exchange_rates] PRIMARY KEY CLUSTERED  ([CURNCYID], [EXCHDATE], [XCHGRATE])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[run_LoadWorkTables]'
GO

ALTER PROCEDURE [work].[run_LoadWorkTables]
	@startDate date = null,
	@endDate date = null,
	@hotelCode nvarchar(20) = null,
	@confirmationNumber nvarchar(20) = null
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @RunID int;

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-1,mrt.arrivalDate),
				@endDate = DATEADD(DAY,1,mrt.arrivalDate),
				@hotelCode = COALESCE(synxis.hotelCode,oh.hotelCode)
		FROM Superset.dbo.mostrecenttransactions mrt
			LEFT JOIN Core.dbo.hotels_Synxis synxis ON synxis.synxisID = mrt.hotelID
			LEFT JOIN Core.dbo.hotels_OpenHospitality oh ON oh.openHospitalityCode = mrt.OpenHospitalityID
		WHERE mrt.confirmationNumber = @confirmationNumber
	END

	IF(@startDate IS NULL AND @endDate IS NULL)
	BEGIN
		IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
		BEGIN
			SET @startDate = DATEADD (mm,-1,GETDATE())
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END
		ELSE
		BEGIN
			SET @startDate = GETDATE()
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END

		SET @endDate = DATEADD(YEAR,2,@startDate)
	END
	-----------------------------------------------------------------------------

	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();
	-----------------------------------------------------------------------------

	-- POPULATE [work].[local_exchange_rates] -----------------------------------
	TRUNCATE TABLE [work].[local_exchange_rates];

	--break up the date ranges in GP exchange rates into individual dates so we don't have to do timespans for currency conversions
	DECLARE @i int = 0
		, @firstCurrencyDate date = DATEADD(YEAR,-1,@startDate)
	DECLARE @end int = DATEDIFF(DAY,@firstCurrencyDate,@endDate) + 1


		WHILE (@i < @end) 
		BEGIN
			INSERT INTO work.local_exchange_rates(CURNCYID, EXCHDATE, XCHGRATE)
			SELECT 'USD' as currencyCode, DATEADD(day, @i, @startdate) as exchangeDate, 1 as exchangeRate
			UNION
			SELECT CURNCYID, DATEADD(day, @i, @startdate), XCHGRATE
			FROM DYNAMICS.dbo.MC00100
			WHERE
				DATEADD(day, @i, @startdate) BETWEEN EXCHDATE AND EXPNDATE 
				AND EXGTBLID LIKE '%AVG%'
				AND CURNCYID != 'USD'		

			SET @i = @i + 1
		END
	-----------------------------------------------------------------------------

	-- POPULATE GREAT PLAINS WORKING TABLES -------------------------------------
	TRUNCATE TABLE [work].[GPCustomerTable];

	INSERT INTO [work].[GPCustomerTable](CUSTNMBR,CURNCYID)
	SELECT CUSTNMBR,CURNCYID
	FROM IC.dbo.RM00101


	TRUNCATE TABLE [work].[GPCurrencyMaster];

	INSERT INTO [work].[GPCurrencyMaster](CURNCYID,DECPLCUR)
	SELECT CURNCYID,DECPLCUR
	FROM DYNAMICS.dbo.MC40200
	-----------------------------------------------------------------------------

	-- UPDATE [Core].[dbo].[hotels_OpenHospitality] -----------------------------
		--This updates our Open Hospitality to Core hotel code translation, a bad mapping can cause errors during calculation
	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitality]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);

	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitalityPHG]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);  
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelActiveBrands] ---------------------------------
	TRUNCATE TABLE [work].[hotelActiveBrands];

	INSERT INTO [work].[hotelActiveBrands](hotelCode,mainHeirarchy)
	SELECT hb.hotelCode,MIN(b.heirarchy) AS mainHeirarchy
	FROM Core.dbo.hotels_brands hb
		INNER JOIN Core.dbo.brands AS b ON hb.brandCode = b.code
	WHERE (hb.endDatetime IS NULL)
		AND (hb.startDatetime <= GETDATE())
		OR (GETDATE() BETWEEN hb.startDatetime AND hb.endDatetime)
	GROUP BY hb.hotelCode;
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelInactiveBrands] -------------------------------
	TRUNCATE TABLE [work].[hotelInactiveBrands];

	INSERT INTO [work].[hotelInactiveBrands](hotelCode,mainHeirarchy)
	SELECT hotelCode,MIN(heirarchy) AS mainHeirarchy
	FROM
		(
			SELECT hb.hotelCode,hb.brandCode,b.heirarchy
			FROM Core.dbo.hotels_brands AS hb
				INNER JOIN
					(
						SELECT hotelCode,MAX(endDatetime) AS maxEnd
						FROM Core.dbo.hotels_brands
						GROUP BY hotelCode
					) AS maxDate ON hb.endDatetime = maxDate.maxEnd AND hb.hotelCode = maxDate.hotelCode
				INNER JOIN Core.dbo.brands b ON hb.brandCode = b.code
			GROUP BY hb.hotelCode,hb.brandCode,b.heirarchy
		) AS maxBrands
	GROUP BY hotelCode;

	------------------------------------------------------------------------

	-- POPULATE [work].[MrtForCalculation] ---------------------------------
	TRUNCATE TABLE [work].[MrtForCalculation];

	
	;WITH cte_charges
	AS (
		SELECT confirmationNumber,sopNumber 
		FROM dbo.Charges
		WHERE hotelCode = ISNULL(@hotelCode,hotelCode) --either we're running all hotels, or we're just getting a specific hotel
		AND confirmationNumber = ISNULL(@confirmationNumber,confirmationNumber) --either we're running all bookings, or just getting a specific conf#
		AND sopNumber IS NOT NULL
	) 
	,cte_mrtj
	AS
	(
		SELECT mrtj.[confirmationNumber],[phgHotelCode],[crsHotelID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
				[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateGroupID],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
				[bookingCroGroupID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
				[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookingCurrency],[bookingCurrencyCode],[timeLoaded],[CRSSourceID],
				work.[billyItemCode]([synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateAbbreviation],[bookingCroGroupID],[chainID]) AS [ItemCode],
				CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) AS exchangeDate,
				mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],ISNULL(mrtj.LoyaltyNumberValidated,0) AS LoyaltyNumberValidated,ISNULL(mrtj.LoyaltyNumberTagged,0) AS LoyaltyNumberTagged
		FROM work.mrtJoined mrtj
			LEFT JOIN cte_charges cd ON cd.confirmationNumber = mrtj.confirmationNumber 
		WHERE mrtj.phgHotelCode = ISNULL(@hotelCode,mrtj.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
			AND mrtj.confirmationNumber = ISNULL(@confirmationNumber,mrtj.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
			AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
			AND mrtj.phgHotelCode NOT IN('BCTS4','PHGTEST')
			AND cd.confirmationNumber IS NULL
	)
	INSERT INTO [work].[MrtForCalculation]([confirmationNumber],[phgHotelCode],[crsHotelID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
			[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateGroupID],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
			[bookingCroGroupID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
			[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookingCurrency],bookingCurrencyCode,[timeLoaded],[CRSSourceID],
			[ItemCode],
			exchangeDate,
			hotelCurrencyCode,hotelCurrencyDecimalPlaces,hotelCurrencyExchangeRate,bookingCurrencyExchangeRate,loyaltyProgram,loyaltyNumber,[travelAgencyName],
			LoyaltyNumberValidated,LoyaltyNumberTagged)

	SELECT 
		mrtj.[confirmationNumber]
		,mrtj.[phgHotelCode]
		,mrtj.[crsHotelID]
		,mrtj.[hotelName]
		,mrtj.[mainBrandCode]
		,mrtj.[gpSiteID]
		,mrtj.[chainID]
		,mrtj.[chainName]
		,mrtj.[bookingStatus]
		,mrtj.[synxisBillingDescription]
		,mrtj.[bookingChannel]
		,mrtj.[bookingSecondarySource]
		,mrtj.[bookingSubSourceCode]
		,mrtj.[bookingTemplateGroupID]
		,mrtj.[bookingTemplateAbbreviation]
		,mrtj.[xbeTemplateName]
		,mrtj.[CROcode]
		,mrtj.[bookingCroGroupID]
		,mrtj.[bookingRateCategoryCode]
		,mrtj.[bookingRateCode]
		,mrtj.[bookingIATA]
		,mrtj.[transactionTimeStamp]
		,mrtj.[confirmationDate]
		,mrtj.[arrivalDate]
		,mrtj.[departureDate]
		,mrtj.[cancellationDate]
		,mrtj.[cancellationNumber]
		,mrtj.[nights]
		,mrtj.[rooms]
		,mrtj.[roomNights]
		,mrtj.[roomRevenueInBookingCurrency]
		,mrtj.[bookingCurrencyCode]
		,mrtj.[timeLoaded]
		,mrtj.[CRSSourceID]
		,mrtj.[ItemCode]
		,mrtj.exchangeDate
		,gpCustomer.CURNCYID as hotelCurrencyCode
		,hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces
		,hotelCE.XCHGRATE as hotelCurrencyExchangeRate
		,bookingCE.XCHGRATE as bookingCurrencyExchangeRate
		,mrtj.loyaltyProgram
		,mrtj.loyaltyNumber
		,mrtj.[travelAgencyName]
		,mrtj.LoyaltyNumberValidated
		,mrtj.LoyaltyNumberTagged
	FROM cte_mrtj mrtj
		INNER JOIN work.GPCustomerTable gpCustomer 
			ON mrtj.phgHotelCode = gpCustomer.CUSTNMBR
		INNER JOIN work.[local_exchange_rates] hotelCE 
			ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND mrtj.exchangeDate = hotelCE.EXCHDATE
		INNER JOIN work.GPCurrencyMaster hotelCM 
			ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		INNER JOIN work.[local_exchange_rates] bookingCE 
			ON mrtj.bookingCurrencyCode = bookingCE.CURNCYID 
			AND mrtj.exchangeDate = bookingCE.EXCHDATE
	------------------------------------------------------------------------

	-- POPULATE MrtForCalc_RulesApplied ------------------------------------
	EXEC work.Populate_MrtForCalc_RulesApplied @RunID
	------------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[Calculate_StandardCharges]'
GO

ALTER PROCEDURE [work].[Calculate_StandardCharges]
	@IsTestRun bit = 0
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE TEMP TABLE ---------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES;

	CREATE TABLE #CHARGES
	(
		[billingRuleID] [int] NOT NULL,
		[classificationID] [int] NOT NULL,
		[confirmationNumber] [nvarchar](20) NOT NULL,
		[hotelCode] [nvarchar](10) NOT NULL,
		[collectionCode] [nvarchar](6) NULL,
		[clauseName] [nvarchar](250) NULL,
		[arrivalDate] [date] NULL,
		[roomNights] [int] NULL,
		[hotelCurrencyCode] [char](3) NULL,
		[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
		[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
		[chargeValueInUSD] [decimal](38, 2) NULL,
		[roomRevenueInUSD] [decimal](38, 2) NULL,
		[itemCode] [nvarchar](50) NULL,
		[gpSiteID] [char](2) NULL,
		[dateCalculated] [datetime] NULL,
		[sopNumber] [char](21) NULL,
		[invoiceDate] [date] NULL,
	)
	------------------------------------------------------------------------------------

	
	INSERT INTO #CHARGES(billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,
							clauseName,arrivalDate,roomNights,hotelCurrencyCode,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate)
	SELECT br.billingRuleID,br.classificationID,mrtC.confirmationNumber,mrtC.phgHotelCode,mrtC.mainBrandCode AS collectionCode,
			rApp.ClauseName,mrtC.arrivalDate,mrtC.roomNights,mrtC.hotelCurrencyCode,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) *  mrtC.hotelCurrencyExchangeRate, mrtC.hotelCurrencyDecimalPlaces-1
			) AS chargeValueInHotelCurrency,
			ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * mrtC.hotelCurrencyExchangeRate,mrtC.hotelCurrencyDecimalPlaces-1) AS roomRevenueInHotelCurrency,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * 	((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) * 1.00, 2
		) AS chargeValueInUSD,
		ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * 1.00, 2) AS roomRevenueInUSD,
		COALESCE
		(c.itemCodeOverride,CASE cl.classificationName
								WHEN 'Booking' THEN
								--if any of the non-source based criteria fields are anything other than the wildcard, this charge is a 'special booking'
									CASE 
										WHEN (rApp.RateCategoryCode <> '*' OR rApp.RateCode <> '*' OR rApp.TravelAgentGroupID <> 0)
										THEN mrtC.ItemCode + '_SB'
        								ELSE mrtC.ItemCode + '_B'
									END
								WHEN 'Commission' THEN mrtC.ItemCode + '_C'
								WHEN 'Surcharge' THEN mrtC.ItemCode + '_S'
								WHEN 'Non-Billable' THEN NULL
								ELSE NULL
							END
		) AS ItemCode,
		COALESCE(c.gpSiteOverride,mrtC.gpSiteID) AS gpSiteID,
		GETDATE(),NULL,NULL
	FROM BillingRules br
		INNER JOIN work.MrtForCalc_RulesApplied rApp ON rApp.ClauseID = br.clauseID
		INNER JOIN Classifications cl ON cl.classificationID = br.classificationID
		INNER JOIN work.MrtForCalculation mrtC ON mrtC.confirmationNumber = rApp.confirmationNumber
		INNER JOIN work.[local_exchange_rates] brCE 
		ON br.currencyCode = brCE.CURNCYID 
			AND mrtC.exchangeDate = brCE.EXCHDATE
		INNER JOIN dbo.Criteria c ON c.criteriaID = rApp.CriteriaID
		LEFT JOIN dbo.ThresholdRules tr ON tr.clauseID = rApp.ClauseID
	WHERE tr.clauseID IS NULL --don't evaluate thresholds 
		AND br.classificationID != 4 --don't evaluate non-billable rules 
		AND (
				mrtC.bookingStatus != 'Cancelled'
				OR
				(mrtC.bookingStatus = 'Cancelled' AND br.refundable = 0)
			)
		AND (
				(br.afterConfirmation = 1 AND mrtC.confirmationDate >= br.confirmationDate)
				OR
				(br.afterConfirmation = 0 AND mrtC.confirmationDate <= br.confirmationDate)
			)
		AND mrtC.arrivalDate BETWEEN br.startDate AND br.endDate
	------------------------------------------------------------------------------------

	IF @IsTestRun = 0
	BEGIN
		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE dbo.Charges
		WHERE confirmationNumber IN(SELECT confirmationNumber FROM work.MrtForCalculation)
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO dbo.Charges(billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate)
		SELECT billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate
		FROM #CHARGES
		------------------------------------------------------------------------------------
	END
	ELSE
	BEGIN
		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE test.Charges
		WHERE confirmationNumber IN(SELECT confirmationNumber FROM work.MrtForCalculation)
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO test.Charges(billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate)
		SELECT billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate
		FROM #CHARGES
		------------------------------------------------------------------------------------
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
