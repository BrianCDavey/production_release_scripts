USE ETL
GO

/*
Run this script on:

        chi-sq-pr-01\warehouse.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 11/1/2017 5:07:45 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[Import_iHamms]'
GO
CREATE TABLE [dbo].[Import_iHamms]
(
[QueueID] [int] NOT NULL,
[standardStaysItemCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[standardStaysQuantity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bonusStaysItemCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[bonusStaysQuantity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[confirmationNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[airlineNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[arrivalDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[departureDate] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[roomNights] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[guestFirstName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[guestLastName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[reconciledStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[iHamms_FinalImport]'
GO

CREATE PROCEDURE dbo.iHamms_FinalImport
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @timeLoaded datetime,
			@fileName varchar(255),
			@processedDate date;

	-- GET TIME LOADED ---------------------------------
	SELECT @timeLoaded = ISNULL([ImportFinished],[ImportStarted]),
			@fileName = REVERSE(LEFT(REVERSE(FilePath),19)),
			@processedDate = CONVERT(date,SUBSTRING(FilePath,LEN(FilePath) - 9,6) + '01')
	FROM [Queue]
	WHERE QueueID = @QueueID
	----------------------------------------------------

	

	INSERT INTO Core.DBO.ihamms(ihammID,standardStaysItemCode,standardStaysQuantity,bonusStaysItemCode,bonusStaysQuantity,confirmationNumber,airlineNumber,arrivalDate,departureDate,roomNights,hotelCode,guestFirstName,guestLastName,timeLoaded,processedDate,loadedFrom,reconciledStatus,commonKey)
	SELECT ihammID,standardStaysItemCode,standardStaysQuantity,bonusStaysItemCode,bonusStaysQuantity,confirmationNumber,airlineNumber,arrivalDate,departureDate,roomNights,hotelCode,guestFirstName,guestLastName,
			timeLoaded,processedDate,loadedFrom,
			reconciledStatus,
			commonKey
	FROM
		(
			SELECT HASHBYTES('MD5',COALESCE(standardStaysItemCode,'NULL') + COALESCE(standardStaysQuantity,'NULL') + COALESCE(bonusStaysItemCode,'NULL') + COALESCE(bonusStaysQuantity,'NULL') + COALESCE(confirmationNumber,'NULL') + COALESCE(airlineNumber,'NULL') + COALESCE(arrivalDate,'NULL') + COALESCE(departureDate,'NULL') + COALESCE(roomNights,'NULL') + COALESCE(hotelCode,'NULL') + COALESCE(guestFirstName,'NULL') + COALESCE(guestLastName,'NULL') + COALESCE(reconciledStatus,'NULL') + @fileName + CONVERT(varchar(20),@processedDate)) AS ihammID,
				standardStaysItemCode,CONVERT(int,standardStaysQuantity) AS standardStaysQuantity,
				bonusStaysItemCode,CONVERT(int,bonusStaysQuantity) AS bonusStaysQuantity,
				confirmationNumber,airlineNumber,
				arrivalDate,departureDate,
				CONVERT(int,roomNights) AS roomNights,
				hotelCode,guestFirstName,guestLastName,
				@timeLoaded AS timeLoaded,@processedDate AS processedDate,@fileName AS loadedFrom,
				reconciledStatus,
				HASHBYTES('MD5',COALESCE([guestFirstName],'NULL') + COALESCE([guestLastName],'NULL') + COALESCE([airlineNumber],'NULL') + COALESCE([arrivalDate],'NULL') + COALESCE([departureDate],'NULL') + COALESCE([hotelCode],'NULL')) AS commonKey
			FROM Import_iHamms
			WHERE QueueID = @QueueID
		) x
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Sabre_FinalImport]'
GO

ALTER PROCEDURE [dbo].[Sabre_FinalImport]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @timeLoaded datetime,@ErrMsg varchar(2000)

	-- GET TIME LOADED ---------------------------------
	SELECT @timeLoaded = ISNULL([ImportFinished],[ImportStarted]) FROM [Queue] WHERE QueueID = @QueueID
	----------------------------------------------------

	-- CREATE & POPULATE #CURRENCY ---------------------
	IF OBJECT_ID('tempdb..#CURRENCY') IS NOT NULL
		DROP TABLE #CURRENCY

	CREATE TABLE #CURRENCY
	(
		[code] [char](3) NOT NULL,
		[rateDate] [date] NOT NULL,
		[toUSD] [decimal](18, 9) NOT NULL
		PRIMARY KEY CLUSTERED([code],[rateDate])
	)

	INSERT INTO #CURRENCY([code],[rateDate],[toUSD])
	SELECT [code],[rateDate],[toUSD]
	FROM CurrencyRates.dbo.dailyRates
	WHERE rateDate IN(SELECT DISTINCT CONVERT(date,confirmationDate) FROM Import_Sabre WHERE QueueID = @QueueID)
	---------------------------------------------------

	-- MARK DATA AS BAD --------------------------------
	UPDATE dbo.Import_Sabre
		SET IsBadData = 0
	WHERE QueueID = @QueueID
		AND IsBadData = 1

	UPDATE s
		SET IsBadData = 1
	FROM Import_Sabre s
	WHERE QueueID = @QueueID
		AND (
				ISNUMERIC(ISNULL(s.averageDailyRate,0)) = 0
				OR ISDATE(s.confirmationdate) = 0
				OR ISNUMERIC(ISNULL(s.reservationRevenue,0)) = 0
				OR ISNUMERIC(ISNULL(s.totalPackageRevenue,0)) = 0
			)

	UPDATE s
		SET IsBadData = 1
	FROM Import_Sabre s
		LEFT JOIN #CURRENCY c ON c.code = s.currency AND c.rateDate = CONVERT(date,s.confirmationDate)
	WHERE QueueID = @QueueID
		AND c.toUSD IS NULL
	----------------------------------------------------

	IF EXISTS(SELECT * FROM Import_Sabre WHERE QueueID = @QueueID AND IsBadData = 1)
	BEGIN
		SET @ErrMsg = 'Sabre Import into RawData FAILED. Failure is most likely due to missing currency data. ' +
					'Check using the following query: SELECT * FROM ETL.dbo.Import_Sabre WHERE QueueID = ' + CONVERT(varchar(20),@QueueID) + ' AND IsBadData = 1'
		
		RAISERROR(@ErrMsg,16,1)
	END
	ELSE
	BEGIN
		-- INSERT INTO [Superset].[dbo].rawData ------------
		INSERT INTO [Superset].[dbo].rawData(hashKey,chainName,chainID,hotelName,hotelID,SAPID,hotelCode,billingDescription,transactionTimeStamp,faxNotificationCount,
											channel,secondarySource,subSource,subSourceCode,PMSRateTypeCode,PMSRoomTypeCode,marketSourceCode,marketSegmentCode,userName,
											[status],confirmationNumber,confirmationDate,cancellationNumber,cancellationDate,salutation,guestFirstName,guestLastName,
											customerID,customerAddress1,customerAddress2,customerCity,customerState,customerPostalCode,customerPhone,customerCountry,
											customerArea,customerRegion,customerCompanyName,arrivalDate,departureDate,bookingLeadTime,rateCategoryName,rateCategoryCode,
											rateTypeName,rateTypeCode,roomTypeName,roomTypeCode,nights,averageDailyRate,rooms,reservationRevenue,currency,IATANumber,
											travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,travelAgencyCity,travelAgencyState,travelAgencyPostalCode,
											travelAgencyPhone,travelAgencyFax,travelAgencyCountry,travelAgencyArea,travelAgencyRegion,travelAgencyEmail,consortiaCount,
											consortiaName,totalPackageRevenue,optIn,customerEmail,totalGuestCount,adultCount,childrenCount,creditCardType,actionType,
											shareWith,arrivalDOW,departureDOW,itineraryNumber,secondaryCurrency,secondaryCurrencyExchangeRate,
											secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,secondaryCurrencyPackageRevenue,commisionPercent,
											membershipNumber,corporationCode,promotionalCode,CROCode,channelConnectConfirmationNumber,primaryGuest,loyaltyProgram,
											loyaltyNumber,vipLevel,xbeTemplateName,xbeShellName,profileTypeSelection,averageDailyRateUSD,reservationRevenueUSD,
											totalPackageRevenueUSD,timeLoaded)
			SELECT x.hashKey,x.chainName,x.chainID,x.hotelName,x.hotelID,x.SAPID,x.hotelCode,x.billingDescription,x.transactionTimeStamp,x.faxNotificationCount,x.
					channel,x.secondarySource,x.subSource,x.subSourceCode,x.PMSRateTypeCode,x.PMSRoomTypeCode,x.marketSourceCode,x.marketSegmentCode,x.userName,x.
					[status],x.confirmationNumber,x.confirmationDate,x.cancellationNumber,x.cancellationDate,x.salutation,x.guestFirstName,x.guestLastName,x.
					customerID,x.customerAddress1,x.customerAddress2,x.customerCity,x.customerState,x.customerPostalCode,x.customerPhone,x.customerCountry,x.
					customerArea,x.customerRegion,x.customerCompanyName,x.arrivalDate,x.departureDate,x.bookingLeadTime,x.rateCategoryName,x.rateCategoryCode,x.
					rateTypeName,x.rateTypeCode,x.roomTypeName,x.roomTypeCode,x.nights,x.averageDailyRate,x.rooms,x.reservationRevenue,x.currency,x.IATANumber,x.
					travelAgencyName,x.travelAgencyAddress1,x.travelAgencyAddress2,x.travelAgencyCity,x.travelAgencyState,x.travelAgencyPostalCode,x.
					travelAgencyPhone,x.travelAgencyFax,x.travelAgencyCountry,x.travelAgencyArea,x.travelAgencyRegion,x.travelAgencyEmail,x.consortiaCount,x.
					consortiaName,x.totalPackageRevenue,x.optIn,x.customerEmail,x.totalGuestCount,x.adultCount,x.childrenCount,x.creditCardType,x.actionType,x.
					shareWith,x.arrivalDOW,x.departureDOW,x.itineraryNumber,x.secondaryCurrency,x.secondaryCurrencyExchangeRate,x.
					secondaryCurrencyAverageDailyRate,x.secondaryCurrencyReservationRevenue,x.secondaryCurrencyPackageRevenue,x.commisionPercent,x.
					membershipNumber,x.corporationCode,x.promotionalCode,x.CROCode,x.channelConnectConfirmationNumber,x.primaryGuest,x.loyaltyProgram,x.
					loyaltyNumber,x.vipLevel,x.xbeTemplateName,x.xbeShellName,x.profileTypeSelection,x.averageDailyRateUSD,x.reservationRevenueUSD,x.
					totalPackageRevenueUSD,x.timeLoaded
			FROM
				(
					SELECT DISTINCT
						HASHBYTES('MD5',COALESCE(chainName,'NULL') + COALESCE([chainID],'NULL') + COALESCE([hotelName],'NULL') + COALESCE([hotelID],'NULL') + COALESCE([SAPID],'NULL') +
										 COALESCE([hotelCode],'NULL') + COALESCE([billingDescription],'NULL') + COALESCE([transactionTimeStamp],'NULL') +
										 COALESCE([faxNotificationCount],'NULL') + COALESCE([channel],'NULL') + COALESCE([secondarySource],'NULL') + COALESCE([subSource],'NULL') +
										 COALESCE([subSourceCode],'NULL') + COALESCE([PMSRateTypeCode],'NULL') + COALESCE([PMSRoomTypeCode],'NULL') + COALESCE([marketSourceCode],'NULL') +
										 COALESCE([marketSegmentCode],'NULL') + COALESCE([userName],'NULL') + COALESCE([status],'NULL') + COALESCE([confirmationNumber],'NULL') +
										 COALESCE([confirmationDate],'NULL') + COALESCE([cancellationNumber],'NULL') + COALESCE([cancellationDate],'NULL') + COALESCE([salutation],'NULL') +
										 COALESCE([guestFirstName],'NULL') + COALESCE([guestLastName],'NULL') + COALESCE(substring(customerID,1,12) ,'NULL') +
										 COALESCE([customerAddress1],'NULL') + COALESCE([customerAddress2],'NULL') + COALESCE([customerCity],'NULL') + COALESCE([customerState],'NULL') +
										 COALESCE([customerPostalCode],'NULL') + COALESCE([customerPhone],'NULL') + COALESCE([customerCountry],'NULL') + COALESCE([customerArea],'NULL') +
										 COALESCE([customerRegion],'NULL') + COALESCE([customerCompanyName],'NULL') + COALESCE([arrivalDate],'NULL') + COALESCE([departureDate],'NULL') +
										 COALESCE([bookingLeadTime],'NULL') + COALESCE([rateCategoryName],'NULL') + COALESCE([rateCategoryCode],'NULL') + COALESCE([rateTypeName],'NULL') +
										 COALESCE([rateTypeCode],'NULL') + COALESCE([roomTypeName],'NULL') + COALESCE([roomTypeCode],'NULL') + COALESCE([nights],'NULL') +
										 COALESCE([averageDailyRate],'NULL') + COALESCE([rooms],'NULL') + COALESCE([reservationRevenue],'NULL') + COALESCE([currency],'NULL') +
										 COALESCE([IATANumber],'NULL') + COALESCE([travelAgencyName],'NULL') + COALESCE([travelAgencyAddress1],'NULL') +
										 COALESCE([travelAgencyAddress2],'NULL') + COALESCE([travelAgencyCity],'NULL') + COALESCE([travelAgencyState],'NULL') +
										 COALESCE([travelAgencyPostalCode],'NULL') + COALESCE([travelAgencyPhone],'NULL') + COALESCE([travelAgencyFax],'NULL') +
										 COALESCE([travelAgencyCountry],'NULL') + COALESCE([travelAgencyArea],'NULL') + COALESCE([travelAgencyRegion],'NULL') +
										 COALESCE([travelAgencyEmail],'NULL') + COALESCE([consortiaCount],'NULL') + COALESCE([consortiaName],'NULL') + COALESCE([totalPackageRevenue],'NULL') +
										 COALESCE([optIn],'NULL') + COALESCE([customerEmail],'NULL') + COALESCE([totalGuestCount],'NULL') + COALESCE([adultCount],'NULL') +
										 COALESCE([childrenCount],'NULL') + COALESCE([creditCardType],'NULL') + COALESCE([actionType],'NULL') + COALESCE([shareWith],'NULL') +
										 COALESCE([arrivalDOW],'NULL') + COALESCE([departureDOW],'NULL') + COALESCE([itineraryNumber],'NULL') + COALESCE([secondaryCurrency],'NULL') +
										 COALESCE([secondaryCurrencyExchangeRate],'NULL') + COALESCE([secondaryCurrencyAverageDailyRate],'NULL') +
										 COALESCE([secondaryCurrencyReservationRevenue],'NULL') + COALESCE([secondaryCurrencyPackageRevenue],'NULL') + COALESCE([commisionPercent],'NULL') +
										 COALESCE([membershipNumber],'NULL') + COALESCE([corporationCode],'NULL') + COALESCE([promotionalCode],'NULL') + COALESCE([CROCode],'NULL') +
										 COALESCE([channelConnectConfirmationNumber],'NULL') + COALESCE([primaryGuest],'NULL') + COALESCE([loyaltyProgram],'NULL') +
										 COALESCE([loyaltyNumber],'NULL') + COALESCE([vipLevel],'NULL') + COALESCE([xbeTemplateName],'NULL') + COALESCE([xbeShellName],'NULL') +
										 COALESCE([profileTypeSelection],'NULL')
								) AS [hashKey],
						ISNULL(chainName,'') AS chainName,CONVERT(int,NULLIF(chainID,'')) AS chainID,ISNULL(hotelName,'') AS hotelName,NULLIF(hotelID,'') AS hotelID,ISNULL(SAPID,'') AS SAPID,
						ISNULL(hotelCode,'') AS hotelCode,ISNULL(billingDescription,'') AS billingDescription,CONVERT(datetime,NULLIF(transactionTimeStamp,'')) AS transactionTimeStamp,
						CONVERT(int,NULLIF(faxNotificationCount,'')) AS faxNotificationCount,
						CASE WHEN channel = 'PMS' THEN 'PMS Rez Synch' ELSE ISNULL(channel,'') END AS channel,
						ISNULL(secondarySource,'') AS secondarySource,
						ISNULL(subSource,'') AS subSource,ISNULL(subSourceCode,'') AS subSourceCode,ISNULL(PMSRateTypeCode,'') AS PMSRateTypeCode,ISNULL(PMSRoomTypeCode,'') AS PMSRoomTypeCode,
						ISNULL(marketSourceCode,'') AS marketSourceCode,ISNULL(marketSegmentCode,'') AS marketSegmentCode,ISNULL(userName,'') AS userName,ISNULL(status,'') AS status,
						ISNULL(confirmationNumber,'') AS confirmationNumber,CONVERT(datetime,NULLIF(confirmationDate,'')) AS confirmationDate,ISNULL(cancellationNumber,'') AS cancellationNumber,
						NULLIF(CONVERT(datetime,NULLIF(cancellationDate,'')),'1900-01-01') AS cancellationDate,ISNULL(salutation,'') AS salutation,ISNULL(guestFirstName,'') AS guestFirstName,
						ISNULL(guestLastName,'') AS guestLastName,SUBSTRING(ISNULL(customerID,''),1,12) AS customerID,ISNULL(customerAddress1,'') AS customerAddress1,
						ISNULL(customerAddress2,'') AS customerAddress2,ISNULL(customerCity,'') AS customerCity,ISNULL(customerState,'') AS customerState,ISNULL(customerPostalCode,'') AS customerPostalCode,
						ISNULL(customerPhone,'') AS customerPhone,ISNULL(customerCountry,'') AS customerCountry,ISNULL(customerArea,'') AS customerArea,ISNULL(customerRegion,'') AS customerRegion,
						ISNULL(customerCompanyName,'') AS customerCompanyName,CONVERT(datetime,NULLIF(arrivalDate,'')) AS arrivalDate,CONVERT(datetime,NULLIF(departureDate,'')) AS departureDate,
						CONVERT(int,NULLIF(bookingLeadTime,'')) AS bookingLeadTime,ISNULL(rateCategoryName,'') AS rateCategoryName,ISNULL(rateCategoryCode,'') AS rateCategoryCode,
						ISNULL(rateTypeName,'') AS rateTypeName,ISNULL(rateTypeCode,'') AS rateTypeCode,ISNULL(roomTypeName,'') AS roomTypeName,ISNULL(roomTypeCode,'') AS roomTypeCode,
						CONVERT(int,NULLIF(nights,'')) AS nights,CONVERT(decimal(12,2),NULLIF(averageDailyRate,'')) AS averageDailyRate,CONVERT(int,NULLIF(rooms,'')) AS rooms,
						CONVErt(DECIMAL(18,2),NULLIF(reservationRevenue,'')) AS reservationRevenue,ISNULL(currency,'') AS currency,ISNULL(IATANumber,'') AS IATANumber,
						ISNULL(travelAgencyName,'') AS travelAgencyName,ISNULL(travelAgencyAddress1,'') AS travelAgencyAddress1,ISNULL(travelAgencyAddress2,'') AS travelAgencyAddress2,
						ISNULL(travelAgencyCity,'') AS travelAgencyCity,ISNULL(travelAgencyState,'') AS travelAgencyState,ISNULL(travelAgencyPostalCode,'') AS travelAgencyPostalCode,
						ISNULL(travelAgencyPhone,'') AS travelAgencyPhone,ISNULL(travelAgencyFax,'') AS travelAgencyFax,ISNULL(travelAgencyCountry,'') AS travelAgencyCountry,
						ISNULL(travelAgencyArea,'') AS travelAgencyArea,ISNULL(travelAgencyRegion,'') AS travelAgencyRegion,ISNULL(travelAgencyEmail,'') AS travelAgencyEmail,
						CONVERT(int,NULLIF(consortiaCount,'')) AS consortiaCount,ISNULL(consortiaName,'') AS consortiaName,CONVERT(decimal(18,2),NULLIF(totalPackageRevenue,'')) AS totalPackageRevenue,
						ISNULL(optIn,'') AS optIn,ISNULL(customerEmail,'') AS customerEmail,CONVERT(int,NULLIF(totalGuestCount,'')) AS totalGuestCount,CONVERT(int,NULLIF(adultCount,'')) AS adultCount,
						CONVERT(int,NULLIF(childrenCount,'')) AS childrenCount,ISNULL(creditCardType,'') AS creditCardType,ISNULL(actionType,'') AS actionType,ISNULL(shareWith,'') AS shareWith,
						ISNULL(arrivalDOW,'') AS arrivalDOW,ISNULL(departureDOW,'') AS departureDOW,ISNULL(itineraryNumber,'') AS itineraryNumber,ISNULL(secondaryCurrency,'') AS secondaryCurrency,
						CONVERT(decimal(18,2),NULLIF(secondaryCurrencyExchangeRate,'')) AS secondaryCurrencyExchangeRate,CONVERT(decimal(18,2),NULLIF(secondaryCurrencyAverageDailyRate,'')) AS secondaryCurrencyAverageDailyRate,
						CONVERT(decimal(18,2),NULLIF(secondaryCurrencyReservationRevenue,'')) AS secondaryCurrencyReservationRevenue,CONVERT(decimal(18,2),NULLIF(secondaryCurrencyPackageRevenue,'')) AS secondaryCurrencyPackageRevenue,
						CONVERT(decimal(18,2),NULLIF(commisionPercent,'')) AS commisionPercent,ISNULL(membershipNumber,'') AS membershipNumber,ISNULL(corporationCode,'') AS corporationCode,
						ISNULL(promotionalCode,'') AS promotionalCode,ISNULL(CROCode,'') AS CROCode,ISNULL(channelConnectConfirmationNumber,'') AS channelConnectConfirmationNumber,
						ISNULL(primaryGuest,'') AS primaryGuest,ISNULL(loyaltyProgram,'') AS loyaltyProgram,ISNULL(loyaltyNumber,'') AS loyaltyNumber,ISNULL(vipLevel,'') AS vipLevel,
						ISNULL(xbeTemplateName,'') AS xbeTemplateName,ISNULL(xbeShellName,'') AS xbeShellName,ISNULL(profileTypeSelection,'') AS profileTypeSelection,
						Superset.dbo.convertCurrencyToUSD(averageDailyRate,currency,confirmationDate) AS averageDailyRateUSD,
						Superset.dbo.convertCurrencyToUSD(reservationRevenue,currency,confirmationDate) AS reservationRevenueUSD,
						Superset.dbo.convertCurrencyToUSD(totalPackageRevenue,currency,confirmationDate) AS totalPackageRevenueUSD,
						@timeLoaded AS timeLoaded
					FROM dbo.Import_Sabre
					WHERE primaryGuest = 'Y'
						--AND IsBadData = 0
						AND QueueID = @QueueID
				) x
			LEFT JOIN Superset.dbo.rawData r ON r.arrivalDate = x.arrivalDate AND r.hotelCode = x.hotelCode AND r.hashKey = x.hashKey
		WHERE r.ID IS NULL
		----------------------------------------------------

		-- UPDATE [Superset].[dbo].rawData -----------------
		UPDATE [Superset].[dbo].rawData
			SET currency = 'MXN',
				averageDailyRateUSD = Superset.dbo.convertCurrencyToUSD(averageDailyRate,'MXN',confirmationDate),
				reservationRevenueUSD = Superset.dbo.convertCurrencyToUSD(reservationRevenue,'MXN',confirmationDate),
				totalPackageRevenueUSD = Superset.dbo.convertCurrencyToUSD(totalPackageRevenue,'MXN',confirmationDate)
		WHERE timeLoaded = @timeLoaded
			AND hotelid = 68656
			AND rateCategoryCode = 'GRPMXN'
			AND currency <> 'MXN'
		----------------------------------------------------
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
