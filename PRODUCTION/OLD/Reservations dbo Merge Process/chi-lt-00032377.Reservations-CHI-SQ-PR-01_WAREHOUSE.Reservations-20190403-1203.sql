/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.3.9483 from Red Gate Software Ltd at 4/3/2019 12:05:29 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [temp].[openHosp_TransactionStatus]'
GO
ALTER TABLE [temp].[openHosp_TransactionStatus] DROP CONSTRAINT [PK_temp_openHosp_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [temp].[openHosp_Transactions]'
GO
ALTER TABLE [temp].[openHosp_Transactions] DROP CONSTRAINT [PK_temp_openHosp_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [temp].[synXis_RoomType]'
GO
ALTER TABLE [temp].[synXis_RoomType] DROP CONSTRAINT [PK_temp_synXis_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [temp].[synXis_TransactionStatus]'
GO
ALTER TABLE [temp].[synXis_TransactionStatus] DROP CONSTRAINT [PK_temp_synXis_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [temp].[synXis_Transactions]'
GO
ALTER TABLE [temp].[synXis_Transactions] DROP CONSTRAINT [PK_temp_synXis_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_openHospID_IC_TransactionStatusID] from [temp].[openHosp_TransactionStatus]'
GO
DROP INDEX [IX_openHospID_IC_TransactionStatusID] ON [temp].[openHosp_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_synXisID_IC_RoomTypeID] from [temp].[synXis_RoomType]'
GO
DROP INDEX [IX_synXisID_IC_RoomTypeID] ON [temp].[synXis_RoomType]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_synXisID_IC_TransactionStatusID] from [temp].[synXis_TransactionStatus]'
GO
DROP INDEX [IX_synXisID_IC_TransactionStatusID] ON [temp].[synXis_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [temp].[synXis_Transactions]'
GO
DROP TABLE [temp].[synXis_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [temp].[openHosp_Transactions]'
GO
DROP TABLE [temp].[openHosp_Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [temp].[openHosp_TransactionStatus]'
GO
DROP TABLE [temp].[openHosp_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [temp].[synXis_TransactionStatus]'
GO
DROP TABLE [temp].[synXis_TransactionStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Transactions]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Transactions] ADD
[LoyaltyProgramID] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoyaltyProgram]'
GO
CREATE TABLE [dbo].[LoyaltyProgram]
(
[synXisID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[openHospID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyProgramID] [int] NOT NULL IDENTITY(1, 1),
[LoyaltyProgram] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_LoyaltyProgram] on [dbo].[LoyaltyProgram]'
GO
ALTER TABLE [dbo].[LoyaltyProgram] ADD CONSTRAINT [PK_dbo_LoyaltyProgram] PRIMARY KEY CLUSTERED  ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UPDATE_CRS_BookingSourceID]'
GO

ALTER PROCEDURE [dbo].[UPDATE_CRS_BookingSourceID]
	@QueueID int = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_BS(confirmationNumber,BookingSourceID,rowNum)
	AS
	(
		SELECT t.confirmationNumber,tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM synxis.Transactions t
			INNER JOIN synxis.MostRecentTransaction mrt ON mrt.confirmationNumber = t.confirmationNumber
			INNER JOIN synxis.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN temp.[synXis_CRS_BookingSource] tbs ON tbs.synXisID = bs.BookingSourceID
			INNER JOIN synxis.Channel ch ON ch.ChannelID = bs.ChannelID
		WHERE NULLIF(ch.channel,'') IS NOT NULL
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM openHosp.Transactions t
			INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.confirmationNumber = t.confirmationNumber
			INNER JOIN openHosp.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN temp.[openHosp_CRS_BookingSource] tbs ON tbs.openHospID = bs.BookingSourceID
			INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.QueueID = @QueueID
	)
	UPDATE t
		SET CRS_BookingSourceID = bs.BookingSourceID
	FROM dbo.Transactions t
		INNER JOIN cte_BS bs ON bs.confirmationNumber = t.confirmationNumber
	WHERE bs.rowNum = 1
		AND t.CRS_BookingSourceID != bs.BookingSourceID
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SabreIATA_Update_MRT]'
GO

ALTER PROCEDURE [dbo].[SabreIATA_Update_MRT]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	

	-- Add Missing IATA's ------------------------------------------
	INSERT INTO dbo.IATANumber([synXisID],[openHospID],[IATANumber])
	SELECT DISTINCT NULL,NULL,iata_no
	FROM ETL.dbo.Import_SabreIATA
		EXCEPT
	SELECT NULL,NULL,IATANumber
	FROM dbo.IATANumber
	----------------------------------------------------------------

	UPDATE t
		SET IATANumberID = iata.IATANumberID
	FROM dbo.Transactions t
		INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		INNER JOIN ETL.dbo.Import_SabreIATA i ON i.crs_confirm_no = t.confirmationNumber AND CONVERT(date,i.arrival_date) = td.arrivalDate
		LEFT JOIN ETL.dbo.Queue q ON q.QueueID = i.QueueID
		INNER JOIN dbo.IATANumber iata ON iata.IATANumber = i.iata_no
	WHERE t.timeLoaded <= q.ImportFinished
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [operations].[Run_ManualFixes]'
GO
ALTER PROCEDURE [operations].[Run_ManualFixes]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	-- MRT TRANSACTION STATUS ------------------
	UPDATE ts
		SET [status] = mf.changeValue
	FROM dbo.TransactionStatus ts
		INNER JOIN dbo.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
		INNER JOIN operations.ManualFixes mf ON mf.confirmationNumber = ts.confirmationNumber
	WHERE mf.changeName = 'MRT TS Status'

	UPDATE ts
		SET cancellationNumber = mf.changeValue
	FROM dbo.TransactionStatus ts
		INNER JOIN dbo.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
		INNER JOIN operations.ManualFixes mf ON mf.confirmationNumber = ts.confirmationNumber
	WHERE mf.changeName = 'MRT TS Cancel Number'

	UPDATE ts
		SET cancellationDate = mf.changeValue
	FROM dbo.TransactionStatus ts
		INNER JOIN dbo.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
		INNER JOIN operations.ManualFixes mf ON mf.confirmationNumber = ts.confirmationNumber
	WHERE mf.changeName = 'MRT TS Cancel Date'
	--------------------------------------------

	-- MRT Currency -----------------------------
	UPDATE td
		SET currency = mf.changeValue
	FROM dbo.TransactionDetail td
		INNER JOIN operations.ManualFixes mf ON mf.confirmationNumber = td.confirmationNumber
	WHERE mf.changeName = 'MRT Currency'
	---------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_VoiceAgent]'
GO

ALTER PROCEDURE [dbo].[Populate_VoiceAgent]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[VoiceAgent] AS tgt
	USING
	(
		SELECT DISTINCT [UserNameID] AS [synXisID],NULL AS [openHospID],[userName]
		FROM synxis.UserName
		WHERE [UserNameID] IN(SELECT [UserNameID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[userName] = tgt.[VoiceAgent]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[VoiceAgent])
		VALUES([synXisID],[openHospID],[userName])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TravelAgent]'
GO

ALTER PROCEDURE [dbo].[Populate_TravelAgent]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[TravelAgent] tgt
	USING
	(
		SELECT DISTINCT STRING_AGG(CONVERT(varchar(MAX),t.TravelAgentID),',') AS [synXisID],NULL AS [openHospID],i.[IATANumberID],t.[Name],MAX(t.[Address1]) AS [Address1],
						MAX(t.[Address2]) AS [Address2],l.[LocationID],MAX(t.[Phone]) AS [Phone],MAX(t.[Fax]) AS [Fax],t.[Email]
		FROM synxis.TravelAgent t
			INNER JOIN (SELECT [LocationID],value AS synXisID FROM dbo.[Location] CROSS APPLY string_split(synXisID,',')) l ON l.synXisID = t.LocationID
			INNER JOIN (SELECT [IATANumberID],value AS synXisID FROM dbo.IATANumber CROSS APPLY string_split(synXisID,',')) i ON i.synXisID = t.IATANumberID
		WHERE t.[TravelAgentID] IN(SELECT [TravelAgentID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		GROUP BY i.[IATANumberID],t.[Name],l.[LocationID],t.[Email]
	) AS src ON ISNULL(src.[IATANumberID],'') = ISNULL(tgt.[IATANumberID],'') AND ISNULL(src.[Name],'') = ISNULL(tgt.[Name],'') AND ISNULL(src.[LocationID],'') = ISNULL(tgt.[LocationID],'') AND ISNULL(src.[Email],'') = ISNULL(tgt.[Email],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[Phone] = src.[Phone],
				[Fax] = src.[Fax]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[IATANumberID],[Name],[Address1],[Address2],[LocationID],[Phone],[Fax],[Email])
		VALUES([synXisID],[openHospID],[IATANumberID],[Name],[Address1],[Address2],[LocationID],[Phone],[Fax],[Email])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionStatus]'
GO

ALTER PROCEDURE [dbo].[Populate_TransactionStatus]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[TransactionStatus] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID]
		FROM
		(
			SELECT ts.[TransactionStatusID] AS [synXisID],NULL AS [openHospID],t.[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID]
			FROM synxis.TransactionStatus ts
				INNER JOIN synxis.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS synXisID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(synXisID,',')) x ON x.synXisID = ts.ActionTypeID
			WHERE t.QueueID = @QueueID
			
			UNION ALL
			
			SELECT NULL AS [synXisID],ts.[TransactionStatusID] AS [openHospID],
				CASE WHEN dups.[confirmationNumber] IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID]
			FROM openHosp.TransactionStatus ts
				INNER JOIN openHosp.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS openHospID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(openHospID,',')) x ON x.openHospID = ts.ActionTypeID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = ts.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
		GROUP BY [confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID]
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[status] = src.[status],
				[confirmationDate] = src.[confirmationDate],
				[cancellationNumber] = src.[cancellationNumber],
				[cancellationDate] = src.[cancellationDate],
				[creditCardType] = src.[creditCardType],
				[ActionTypeID] = src.[ActionTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID])
		VALUES([synXisID],[openHospID],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [temp].[synXis_LoyaltyProgram]'
GO
CREATE TABLE [temp].[synXis_LoyaltyProgram]
(
[synXisID] [int] NOT NULL,
[LoyaltyProgramID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_temp_synXis_LoyaltyProgram] on [temp].[synXis_LoyaltyProgram]'
GO
ALTER TABLE [temp].[synXis_LoyaltyProgram] ADD CONSTRAINT [PK_temp_synXis_LoyaltyProgram] PRIMARY KEY CLUSTERED  ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Transactions]'
GO

ALTER PROCEDURE [dbo].[Populate_Transactions]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Transactions] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID
		FROM
		(
			SELECT t.[TransactionID] AS [synXisID],NULL AS [openHospID],t.[QueueID],t.[itineraryNumber],t.[confirmationNumber],
					t.[transactionTimeStamp],t.[channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					cc.[CorporateCodeID],rt.[RoomTypeID],rc.[RateCategoryID],u.UserNameID,
					bs.[BookingSourceID],ln.[LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],ta.[TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,gc.[Guest_CompanyNameID],lp.LoyaltyProgramID
			FROM synxis.Transactions t
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.synXisID = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.synXisID = t.TransactionDetailID AND td.[synXisTransExID] = t.TransactionsExtendedID
				LEFT JOIN temp.[synXis_CorporateCode] cc ON cc.synXisID = t.CorporateCodeID
				LEFT JOIN temp.[synXis_RoomType] rt	ON rt.synXisID = t.RoomTypeID
				LEFT JOIN temp.[synXis_RateCategory] rc	ON rc.synXisID = t.RateCategoryID
				LEFT JOIN temp.[synXis_VoiceAgent] u ON u.synXisID = t.UserNameID
				LEFT JOIN temp.[synXis_CRS_BookingSource] bs ON bs.synXisID = t.BookingSourceID
				LEFT JOIN temp.[synXis_LoyaltyNumber] ln ON ln.synXisID = t.LoyaltyNumberID
				LEFT JOIN temp.[synXis_LoyaltyProgram] lp ON lp.synXisID = t.LoyaltyProgramID
				LEFT JOIN temp.[synXis_Guest] g	ON g.synXisID = t.GuestID
				LEFT JOIN temp.[synXis_RateCode] rtc ON rtc.synXisID = t.RateTypeCodeID
				LEFT JOIN temp.[synXis_PromoCode] pc ON pc.synXisID = t.PromoCodeID
				LEFT JOIN temp.[synXis_TravelAgent] ta ON ta.synXisID = t.TravelAgentID
				LEFT JOIN temp.[synXis_IATANumber] i ON i.synXisID = t.IATANumberID
				LEFT JOIN temp.[synXis_hotel] h	ON h.synXisID = t.intHotelID
				LEFT JOIN temp.[synXis_Chain] ch ON ch.synXisID = t.intChainID
				LEFT JOIN temp.[synXis_Guest_CompanyName] gc ON gc.synXisID = t.Guest_CompanyNameID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID

			UNION ALL

			SELECT NULL AS [synXisID],t.[TransactionID] AS [openHospID],t.[QueueID],NULL AS [itineraryNumber],
					CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
					t.[transactionTimeStamp],NULL AS [channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					NULL AS [CorporateCodeID],NULL AS [RoomTypeID],NULL AS [RateCategoryID],NULL AS [VoiceAgentID],
					bs.[BookingSourceID],NULL AS [LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],NULL AS [TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,NULL AS [Guest_CompanyNameID],NULL AS LoyaltyProgramID
			FROM openHosp.Transactions t
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.openHospID = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.openHospID = t.TransactionDetailID
				LEFT JOIN temp.[openHosp_CRS_BookingSource] bs ON bs.openHospID = t.BookingSourceID
				LEFT JOIN temp.[openHosp_Guest] g ON g.openHospID = t.GuestID
				LEFT JOIN temp.[openHosp_RateCode] rtc ON rtc.openHospID = t.RateTypeCodeID
				LEFT JOIN temp.[openHosp_PromoCode] pc ON pc.openHospID = t.PromoCodeID
				LEFT JOIN temp.[openHosp_IATANumber] i ON i.openHospID = t.IATANumberID
				LEFT JOIN temp.[openHosp_hotel] h ON h.openHospID = t.intHotelID
				LEFT JOIN temp.[openHosp_Chain] ch ON ch.openHospID = t.intChainID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID
		) x
		GROUP BY [QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[QueueID] = src.[QueueID],
				[itineraryNumber] = src.[itineraryNumber],
				[transactionTimeStamp] = src.[transactionTimeStamp],
				[channelConnectConfirmationNumber] = src.[channelConnectConfirmationNumber],
				[timeLoaded] = src.[timeLoaded],
				[TransactionStatusID] = src.[TransactionStatusID],
				[TransactionDetailID] = src.[TransactionDetailID],
				[CorporateCodeID] = src.[CorporateCodeID],
				[RoomTypeID] = src.[RoomTypeID],
				[RateCategoryID] = src.[RateCategoryID],
				[VoiceAgentID] = src.UserNameID,
				[CRS_BookingSourceID] = src.[BookingSourceID],
				[LoyaltyNumberID] = src.[LoyaltyNumberID],
				[GuestID] = src.[GuestID],
				[RateCodeID] = src.RateTypeCodeID,
				[PromoCodeID] = src.[PromoCodeID],
				[TravelAgentID] = src.[TravelAgentID],
				[IATANumberID] = src.[IATANumberID],
				[HotelID] = src.intHotelID,
				[ChainID] = src.intChainID,
				[Guest_CompanyNameID] = src.[Guest_CompanyNameID],
				LoyaltyProgramID = src.LoyaltyProgramID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],[VoiceAgentID],[CRS_BookingSourceID],[LoyaltyNumberID],[GuestID],[RateCodeID],[PromoCodeID],[TravelAgentID],[IATANumberID],[HotelID],[ChainID],[Guest_CompanyNameID],LoyaltyProgramID)
		VALUES([synXisID],[openHospID],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionDetail]'
GO

ALTER PROCEDURE [dbo].[Populate_TransactionDetail]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[TransactionDetail] AS tgt
	USING
	(
		SELECT [synXisID],[synXisTransExID],[openHospID],[confirmationNumber],CASE WHEN [nights] = 0 THEN 1 ELSE [nights] END AS [nights],
			[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
			[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
			[departureDOW],[optIn],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[IsPrimaryGuest]
		FROM
		(
			SELECT td.[TransactionDetailID] AS [synXisID],x.TransactionsExtendedID AS [synXisTransExID],NULL AS [openHospID],t.[confirmationNumber],[nights],
					[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
					[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
					[departureDOW],x.[optIn],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[IsPrimaryGuest]
			FROM synxis.TransactionDetail td
				INNER JOIN synxis.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN
					(
						SELECT DISTINCT t.TransactionID,te.TransactionsExtendedID,
									CASE te.optIn WHEN 'Y' THEN 1 ELSE 0 END AS optIn,
									te.IsPrimaryGuest
						FROM synxis.TransactionsExtended te
							INNER JOIN synxis.Transactions t ON t.TransactionsExtendedID = te.TransactionsExtendedID
					) x ON x.TransactionID = mrt.TransactionID
			WHERE t.QueueID = @QueueID
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [synXisTransExID],td.[TransactionDetailID] AS [openHospID],
				CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[nights],
				NULL AS [averageDailyRate],[rooms],[reservationRevenue],[currency],NULL AS [totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],NULL AS [commisionPercent],[arrivalDate],[departureDate],NULL AS [bookingLeadTime],NULL AS [arrivalDOW],
				NULL AS [departureDOW],NULL AS [optIn],NULL AS [LoyaltyNumberValidated],NULL AS [LoyaltyNumberTagged],NULL AS [IsPrimaryGuest]
			FROM openHosp.TransactionDetail td
				INNER JOIN openHosp.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[synXisTransExID] = src.[synXisTransExID],
				[openHospID] = src.[openHospID],
				[nights] = src.[nights],
				[averageDailyRate] = src.[averageDailyRate],
				[rooms] = src.[rooms],
				[reservationRevenue] = src.[reservationRevenue],
				[currency] = src.[currency],
				[totalPackageRevenue] = src.[totalPackageRevenue],
				[totalGuestCount] = src.[totalGuestCount],
				[adultCount] = src.[adultCount],
				[childrenCount] = src.[childrenCount],
				[commisionPercent] = src.[commisionPercent],
				[arrivalDate] = src.[arrivalDate],
				[departureDate] = src.[departureDate],
				[bookingLeadTime] = src.[bookingLeadTime],
				[arrivalDOW] = src.[arrivalDOW],
				[departureDOW] = src.[departureDOW],
				[optIn] = src.[optIn],
				[LoyaltyNumberValidated] = src.[LoyaltyNumberValidated],
				[LoyaltyNumberTagged] = src.[LoyaltyNumberTagged],
				[IsPrimaryGuest] = src.[IsPrimaryGuest]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[synXisTransExID],[openHospID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[IsPrimaryGuest])
		VALUES([synXisID],[synXisTransExID],[openHospID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[IsPrimaryGuest])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [temp].[Populate_TempTables]'
GO

ALTER PROCEDURE [temp].[Populate_TempTables]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': TRUNCATE TEMP TABLES'
	RAISERROR(@print,10,1) WITH NOWAIT

	EXEC dbo.TruncateTablesWithinSchema 'temp'
	-------------------

	-- PRINT STATUS --
/*
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_TransactionStatus]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_TransactionStatus](synXisID,TransactionStatusID)
	SELECT value,TransactionStatusID FROM dbo.TransactionStatus CROSS APPLY string_split(synXisID,',')
	WHERE IsSynXis = 1
		AND value IN(SELECT TransactionStatusID FROM synxis.Transactions WHERE QueueID = @QueueID)
*/
	-------------------



	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_CorporateCode]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_CorporateCode](synXisID,CorporateCodeID)
	SELECT value,CorporateCodeID FROM dbo.CorporateCode CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_RoomType]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_RoomType](synXisID,RoomTypeID)
	SELECT value,RoomTypeID FROM dbo.RoomType CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_RateCategory]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_RateCategory](synXisID,RateCategoryID)
	SELECT value,RateCategoryID FROM dbo.RateCategory CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_VoiceAgent]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_VoiceAgent](synXisID,UserNameID)
	SELECT value,VoiceAgentID FROM dbo.VoiceAgent CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_CRS_BookingSource]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_CRS_BookingSource](synXisID,BookingSourceID)
	SELECT value,BookingSourceID FROM dbo.CRS_BookingSource CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_LoyaltyNumber]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_LoyaltyNumber](synXisID,LoyaltyNumberID)
	SELECT value,LoyaltyNumberID FROM dbo.LoyaltyNumber CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_LoyaltyProgram]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_LoyaltyProgram](synXisID,LoyaltyProgramID)
	SELECT value,LoyaltyProgramID FROM dbo.LoyaltyProgram CROSS APPLY string_split(synXisID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_Guest]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_Guest](synXisID,GuestID)
	SELECT DISTINCT st.GuestID,dg.GuestID
	FROM dbo.Guest dg
		INNER JOIN synxis.Transactions st ON dg.synXisID = CONVERT(varchar(20),st.GuestID)
	WHERE st.QueueID = @QueueID
		AND dg.IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_RateCode]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_RateCode](synXisID,RateTypeCodeID)
	SELECT value,RateCodeID FROM dbo.RateCode CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_PromoCode]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_PromoCode](synXisID,PromoCodeID)
	SELECT value,PromoCodeID FROM dbo.PromoCode CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_TravelAgent]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_TravelAgent](synXisID,TravelAgentID)
	SELECT value,TravelAgentID FROM dbo.TravelAgent CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_IATANumber]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_IATANumber](synXisID,IATANumberID)
	SELECT value,IATANumberID FROM dbo.IATANumber CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_hotel]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_hotel](synXisID,intHotelID)
	SELECT value,HotelID FROM dbo.hotel CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_Chain]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_Chain](synXisID,intChainID)
	SELECT value,ChainID FROM dbo.Chain CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_Guest_CompanyName]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_Guest_CompanyName](synXisID,Guest_CompanyNameID)
	SELECT value,Guest_CompanyNameID FROM dbo.Guest_CompanyName CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
	-------------------


	-- PRINT STATUS --
/*
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[synXis_Transactions]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[synXis_Transactions](synXisID)
	SELECT DISTINCT value FROM dbo.Transactions CROSS APPLY string_split(synXisID,',') WHERE IsSynXis = 1
*/	
	-------------------




	-- PRINT STATUS --
/*
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_TransactionStatus]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_TransactionStatus](openHospID,TransactionStatusID)
	SELECT value,TransactionStatusID FROM dbo.TransactionStatus CROSS APPLY string_split(openHospID,',')
*/
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_CRS_BookingSource]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_CRS_BookingSource](openHospID,BookingSourceID)
	SELECT value,BookingSourceID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_Guest]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_Guest](openHospID,GuestID)
	SELECT value,GuestID FROM dbo.Guest CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_RateCode]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_RateCode](openHospID,RateTypeCodeID)
	SELECT value,RateCodeID FROM dbo.RateCode CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_PromoCode]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_PromoCode](openHospID,PromoCodeID)
	SELECT value,PromoCodeID FROM dbo.PromoCode CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_IATANumber]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_IATANumber](openHospID,IATANumberID)
	SELECT value,IATANumberID FROM dbo.IATANumber CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_hotel]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_hotel](openHospID,intHotelID)
	SELECT value,HotelID FROM dbo.hotel CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_Chain]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_Chain](openHospID,intChainID)
	SELECT value,ChainID FROM dbo.Chain CROSS APPLY string_split(openHospID,',')
	-------------------


	-- PRINT STATUS --
/*
	SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': temp.[openHosp_Transactions]'
	RAISERROR(@print,10,1) WITH NOWAIT

	INSERT INTO temp.[openHosp_Transactions](openHospID)
	SELECT DISTINCT value FROM dbo.Transactions CROSS APPLY string_split(openHospID,',')
*/	
	-------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_State]'
GO

ALTER PROCEDURE [dbo].[Populate_State]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[State] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[State_Text]
		FROM
		(
			SELECT [StateID] AS [synXisID],NULL AS [openHospID],ISNULL(NULLIF([State_Text],''),'unknown') AS [State_Text]
			FROM synxis.[State]
				UNION ALL
			SELECT NULL AS [synXisID],[StateID] AS [openHospID],ISNULL(NULLIF([State_Text],''),'unknown') AS [State_Text]
			FROM openHosp.[State]
		) x
		GROUP BY [State_Text]
	) AS src ON src.[State_Text] = tgt.[State_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[State_Text])
		VALUES([synXisID],[openHospID],[State_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RoomType]'
GO

ALTER PROCEDURE [dbo].[Populate_RoomType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RoomType] AS tgt
	USING
	(
		SELECT STRING_AGG([RoomTypeID],',') AS [synXisID],NULL AS [openHospID],[roomTypeName],[roomTypeCode]
		FROM synxis.RoomType
		WHERE [RoomTypeID] IN(SELECT [RoomTypeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		GROUP BY [roomTypeName],[roomTypeCode]
	) AS src ON src.[roomTypeName] = tgt.[roomTypeName] AND src.[roomTypeCode] = tgt.[roomTypeCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[roomTypeName],[roomTypeCode])
		VALUES([synXisID],[openHospID],[roomTypeName],[roomTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Region]'
GO

ALTER PROCEDURE [dbo].[Populate_Region]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Region] AS tgt
	USING
	(
		SELECT DISTINCT [RegionID] AS [synXisID],NULL AS [openHospID],ISNULL(NULLIF([Region_Text],''),'unknown') AS [Region_Text]
		FROM synxis.Region
	) AS src ON src.[Region_Text] = tgt.[Region_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[Region_Text])
		VALUES([synXisID],[openHospID],[Region_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RateCode]'
GO

ALTER PROCEDURE [dbo].[Populate_RateCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RateCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[RateTypeName],[RateTypeCode]
		FROM
		(
			SELECT [RateTypeCodeID] AS [synXisID],NULL AS [openHospID],[RateTypeName],[RateTypeCode]
			FROM synxis.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[RateTypeCodeID] AS [openHospID],[RateTypeName],[RateTypeCode]
			FROM openHosp.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [RateTypeName],[RateTypeCode]
	) AS src ON src.[RateTypeName] = tgt.[RateName] AND src.[RateTypeCode] = tgt.[RateCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[RateName],[RateCode])
		VALUES([synXisID],[openHospID],[RateTypeName],[RateTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RateCategory]'
GO

ALTER PROCEDURE [dbo].[Populate_RateCategory]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RateCategory] AS tgt
	USING
	(
		SELECT DISTINCT [RateCategoryID] AS [synXisID],NULL AS [openHospID],[rateCategoryName],ISNULL(NULLIF([rateCategoryCode],''),'Unassigned') as [rateCategoryCode]
		FROM synxis.RateCategory
		WHERE [RateCategoryID] IN(SELECT [RateCategoryID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[rateCategoryName] = tgt.[rateCategoryName] AND src.[rateCategoryCode] = tgt.[rateCategoryCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[rateCategoryName],[rateCategoryCode])
		VALUES([synXisID],[openHospID],[rateCategoryName],[rateCategoryCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PromoCode]'
GO

ALTER PROCEDURE [dbo].[Populate_PromoCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PromoCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[promotionalCode]
		FROM
		(
			SELECT [PromoCodeID] AS [synXisID],NULL AS [openHospID],[promotionalCode]
			FROM synxis.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[PromoCodeID] AS [openHospID],[promotionalCode]
			FROM openHosp.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [promotionalCode]
	) AS src ON src.[promotionalCode] = tgt.[promotionalCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[promotionalCode])
		VALUES([synXisID],[openHospID],[promotionalCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PostalCode]'
GO

ALTER PROCEDURE [dbo].[Populate_PostalCode]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PostalCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[PostalCode_Text]
		FROM
		(
			SELECT [PostalCodeID] AS [synXisID],NULL AS [openHospID],ISNULL(NULLIF([PostalCode_Text],''),'unknown') AS [PostalCode_Text]
			FROM synxis.PostalCode
				UNION ALL
			SELECT NULL AS [synXisID],[PostalCodeID] AS [openHospID],ISNULL(NULLIF([PostalCode_Text],''),'unknown') AS [PostalCode_Text]
			FROM openHosp.PostalCode
		) x
		GROUP BY [PostalCode_Text]
	) AS src ON src.[PostalCode_Text] = tgt.[PostalCode_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[PostalCode_Text])
		VALUES([synXisID],[openHospID],[PostalCode_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PMSRateTypeCode]'
GO

ALTER PROCEDURE [dbo].[Populate_PMSRateTypeCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PMSRateTypeCode] AS tgt
	USING
	(
		SELECT DISTINCT [PMSRateTypeCodeID] AS [synXisID],NULL AS [openHospID],[PMSRateTypeCode]
		FROM synxis.PMSRateTypeCode
		WHERE [PMSRateTypeCodeID] IN(SELECT [PMSRateTypeCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[PMSRateTypeCode] = tgt.[PMSRateTypeCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[PMSRateTypeCode])
		VALUES([synXisID],[openHospID],[PMSRateTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_LoyaltyNumber]'
GO

ALTER PROCEDURE [dbo].[Populate_LoyaltyNumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[LoyaltyNumber] AS tgt
	USING
	(
		SELECT DISTINCT [LoyaltyNumberID] AS [synXisID],NULL AS [openHospID],[loyaltyNumber]
		FROM synxis.LoyaltyNumber
		WHERE [LoyaltyNumberID] IN(SELECT [LoyaltyNumberID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[loyaltyNumber] = tgt.[loyaltyNumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[loyaltyNumber])
		VALUES([synXisID],[openHospID],[loyaltyNumber])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Location]'
GO

ALTER PROCEDURE [dbo].[Populate_Location]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Location] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[CityID],[StateID],[CountryID],[PostalCodeID],[RegionID]
		FROM
		(
			SELECT [LocationID] AS [synXisID],NULL AS [openHospID],c.[CityID],s.[StateID],cn.[CountryID],p.[PostalCodeID],r.[RegionID]
			FROM synxis.[Location] l
				LEFT JOIN (SELECT [CityID],value AS synXisID FROM dbo.City CROSS APPLY string_split(synXisID,',')) c ON c.synXisID = l.CityID
				LEFT JOIN (SELECT [StateID],value AS synXisID FROM dbo.[State] CROSS APPLY string_split(synXisID,',')) s ON s.synXisID = l.StateID
				LEFT JOIN (SELECT [CountryID],value AS synXisID FROM dbo.Country CROSS APPLY string_split(synXisID,',')) cn ON cn.synXisID = l.CountryID
				LEFT JOIN (SELECT [PostalCodeID],value AS synXisID FROM dbo.PostalCode CROSS APPLY string_split(synXisID,',')) p ON p.synXisID = l.PostalCodeID
				LEFT JOIN (SELECT [RegionID],value AS synXisID FROM dbo.Region CROSS APPLY string_split(synXisID,',')) r ON r.synXisID = l.RegionID
			WHERE [LocationID] IN
								(
								SELECT DISTINCT [LocationID] FROM [synxis].[Transactions] t INNER JOIN [synxis].[Guest] g ON g.[GuestID] = t.[GuestID] WHERE QueueID = @QueueID
									UNION
								SELECT DISTINCT [LocationID] FROM [synxis].[Transactions] t INNER JOIN [synxis].[TravelAgent] ta ON ta.[TravelAgentID] = t.[TravelAgentID] WHERE QueueID = @QueueID
								)

			UNION ALL

			SELECT NULL AS [synXisID],[LocationID] AS [openHospID],c.[CityID],s.[StateID],cn.[CountryID],p.[PostalCodeID],NULL AS [RegionID]
			FROM openHosp.[Location] l
				LEFT JOIN (SELECT [CityID],value AS openHospID FROM dbo.City CROSS APPLY string_split(openHospID,',')) c ON c.openHospID = l.CityID
				LEFT JOIN (SELECT [StateID],value AS openHospID FROM dbo.[State] CROSS APPLY string_split(openHospID,',')) s ON s.openHospID = l.StateID
				LEFT JOIN (SELECT [CountryID],value AS openHospID FROM dbo.Country CROSS APPLY string_split(openHospID,',')) cn ON cn.openHospID = l.CountryID
				LEFT JOIN (SELECT [PostalCodeID],value AS openHospID FROM dbo.PostalCode CROSS APPLY string_split(openHospID,',')) p ON p.openHospID = l.PostalCodeID
			WHERE [LocationID] IN
								(
								SELECT DISTINCT [LocationID] FROM openHosp.[Transactions] t INNER JOIN openHosp.[Guest] g ON g.[GuestID] = t.[GuestID] WHERE QueueID = @QueueID
								)
		) X
		GROUP BY [CityID],[StateID],[CountryID],[PostalCodeID],[RegionID]
	) AS src ON ISNULL(src.[CityID],'') = ISNULL(tgt.[CityID],'') AND ISNULL(src.[StateID],'') = ISNULL(tgt.[StateID],'') AND ISNULL(src.[CountryID],'') = ISNULL(tgt.[CountryID],'') AND ISNULL(src.[PostalCodeID],'') = ISNULL(tgt.[PostalCodeID],'') AND ISNULL(src.[RegionID],'') = ISNULL(tgt.[RegionID],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[CityID],[StateID],[CountryID],[PostalCodeID],[RegionID])
		VALUES([synXisID],[openHospID],[CityID],[StateID],[CountryID],[PostalCodeID],[RegionID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_ibeSource]'
GO

ALTER PROCEDURE [dbo].[Populate_ibeSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[ibeSource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],ibe.ibeSourceID
		FROM
		(
			SELECT [xbeTemplateID] AS [synXisID],NULL AS [openHospID],[xbeTemplateName]
			FROM synxis.xbeTemplate
			WHERE [xbeTemplateID] IN(
								SELECT DISTINCT [xbeTemplateID]
								FROM synxis.Transactions t
									INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
								WHERE t.QueueID = @QueueID
								)
				UNION ALL
			SELECT NULL AS [synXisID],bs.BookingSourceID AS [openHospID],obs.Template AS [xbeTemplateName]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
		) x
		INNER JOIN authority.ibeSource ibe ON ibe.ibeSourceName = x.xbeTemplateName
		GROUP BY ibe.ibeSourceID
	) AS src ON src.ibeSourceID = tgt.[auth_ibeSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[auth_ibeSourceID])
		VALUES([synXisID],[openHospID],ibeSourceID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_IATANumber]'
GO

ALTER PROCEDURE [dbo].[Populate_IATANumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[IATANumber] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[IATANumber]
		FROM
		(
			SELECT [IATANumberID] AS [synXisID],NULL AS [openHospID],[IATANumber]
			FROM synxis.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[IATANumberID] AS [openHospID],[IATANumber]
			FROM openHosp.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
		) x
		WHERE ISNULL(IATANumber,'') != ''
		GROUP BY [IATANumber]
	) AS src ON src.[IATANumber] = tgt.[IATANumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[IATANumber])
		VALUES([synXisID],[openHospID],[IATANumber])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_hotel]'
GO

ALTER PROCEDURE [dbo].[Populate_hotel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- ADD MISSING HOTELS to Hotels.dbo.Hotel -----------------
	;WITH cte_Code(HotelCode)
	AS
	(
		SELECT HotelCode FROM Reservations.openHosp.hotel
			EXCEPT
		SELECT OpenHospCode FROM Hotels.dbo.Hotel
	)
	INSERT INTO Hotels.dbo.Hotel(HotelCode,HotelName,OpenHospID,OpenHospCode)
	SELECT h.HotelCode,h.HotelName,h.HotelId,h.HotelCode
	FROM Reservations.openHosp.hotel h
		INNER JOIN cte_Code c ON c.HotelCode = h.HotelCode

	;WITH cte_Code(HotelId)
	AS
	(
		SELECT HotelId FROM Reservations.synXis.hotel
			EXCEPT
		SELECT SynXisID FROM Hotels.dbo.Hotel
	)
	INSERT INTO Hotels.dbo.Hotel(HotelCode,HotelName,SynXisID,SynXisCode)
	SELECT h.HotelCode,h.HotelName,h.HotelId,h.HotelCode
	FROM Reservations.synXis.hotel h
		INNER JOIN cte_Code c ON c.HotelId = h.HotelId
	-----------------------------------------------------------

	MERGE INTO [dbo].[hotel] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[HotelId]
		FROM
		(
			SELECT [intHotelID] AS [synXisID],NULL AS [openHospID],MAX(shh.[HotelId]) AS [HotelId]
			FROM synxis.hotel sh
				INNER JOIN Hotels.[dbo].[Hotel] shh ON shh.[SynXisID] = sh.[HotelId]
			WHERE [intHotelID] IN(SELECT [intHotelID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
			GROUP BY [intHotelID]
				UNION ALL
			SELECT NULL AS [synXisID],[intHotelID] AS [openHospID],MAX(ISNULL(ohID.[HotelId],ohCode.[HotelId])) AS [HotelId]
			FROM openHosp.hotel oh
				LEFT JOIN Hotels.[dbo].[Hotel] ohID ON ohID.[OpenHospID] = oh.[HotelId]
				LEFT JOIN Hotels.[dbo].[Hotel] ohCode ON ohCode.OpenHospCode = oh.HotelCode
			WHERE ISNULL(ohID.[HotelId],ohCode.[HotelId]) IS NOT NULL
				AND [intHotelID] IN(SELECT [intHotelID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
			GROUP BY [intHotelID]
		) x
		GROUP BY [HotelId]
	) AS src ON src.[HotelId] = tgt.[Hotel_hotelID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[Hotel_hotelID])
		VALUES([synXisID],[openHospID],[HotelId])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_EmailAddress]'
GO

ALTER PROCEDURE [dbo].[Populate_Guest_EmailAddress]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Guest_EmailAddress] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[emailAddress]
		FROM
		(
			SELECT [Guest_EmailAddressID] AS [synXisID],NULL AS [openHospID],[emailAddress]
			FROM synxis.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM [synxis].[Transactions] t
												INNER JOIN [synxis].[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
				UNION ALL
			SELECT NULL AS [synXisID],[Guest_EmailAddressID] AS [openHospID],[emailAddress]
			FROM openHosp.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM openHosp.[Transactions] t
												INNER JOIN openHosp.[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
		) x
		GROUP BY [emailAddress]
	) AS src ON src.[emailAddress] = tgt.[emailAddress]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[emailAddress])
		VALUES([synXisID],[openHospID],[emailAddress])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_CompanyName]'
GO

ALTER PROCEDURE [dbo].[Populate_Guest_CompanyName]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Guest_CompanyName] AS tgt
	USING
	(
		SELECT DISTINCT [Guest_CompanyNameID] AS [synXisID],NULL AS [openHospID],[CompanyName]
		FROM synxis.Guest_CompanyName
		WHERE [Guest_CompanyNameID] IN(SELECT [Guest_CompanyNameID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[CompanyName] = tgt.[CompanyName]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[CompanyName])
		VALUES([synXisID],[openHospID],[CompanyName])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest]'
GO

ALTER PROCEDURE [dbo].[Populate_Guest]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- #SYNXIS_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#SYNXIS_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #SYNXIS_Guest_EmailAddress;
	CREATE TABLE #SYNXIS_Guest_EmailAddress([Guest_EmailAddressID] int,synXisID int NOT NULL, PRIMARY KEY CLUSTERED(synXisID))

	INSERT INTO #SYNXIS_Guest_EmailAddress(Guest_EmailAddressID,synXisID)
	SELECT [Guest_EmailAddressID], value AS synXisID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(synXisID,',')
	------------------------------------------------------

	-- #OPEN_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#OPEN_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #OPEN_Guest_EmailAddress;
	CREATE TABLE #OPEN_Guest_EmailAddress([Guest_EmailAddressID] int,openHospID int NOT NULL, PRIMARY KEY CLUSTERED(openHospID))

	INSERT INTO #OPEN_Guest_EmailAddress(Guest_EmailAddressID,openHospID)
	SELECT [Guest_EmailAddressID], value AS openHospID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(openHospID,',')
	------------------------------------------------------

	-- #SYNXIS_Location ------------------------
	IF OBJECT_ID('tempdb..#SYNXIS_Location') IS NOT NULL
		DROP TABLE #SYNXIS_Location;
	CREATE TABLE #SYNXIS_Location([LocationID] int,synXisID int NOT NULL, PRIMARY KEY CLUSTERED(synXisID))

	INSERT INTO #SYNXIS_Location([LocationID],synXisID)
	SELECT [LocationID], value AS synXisID FROM dbo.[Location] CROSS APPLY string_split(synXisID,',')
	------------------------------------------------------

	-- #OPEN_Location ------------------------
	IF OBJECT_ID('tempdb..#OPEN_Location') IS NOT NULL
		DROP TABLE #OPEN_Location;
	CREATE TABLE #OPEN_Location([LocationID] int,openHospID int NOT NULL, PRIMARY KEY CLUSTERED(openHospID))

	INSERT INTO #OPEN_Location([LocationID],openHospID)
	SELECT [LocationID], value AS openHospID FROM dbo.[Location] CROSS APPLY string_split(openHospID,',')
	------------------------------------------------------

	MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID]
		FROM
		(
			SELECT [GuestID] AS [synXisID],NULL AS [openHospID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM synxis.Guest g
				LEFT JOIN #SYNXIS_Guest_EmailAddress ge ON ge.synXisID = g.Guest_EmailAddressID
				LEFT JOIN #SYNXIS_Location l ON l.synXisID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)

			UNION ALL

			SELECT NULL AS [synXisID],[GuestID] AS [openHospID],NULL AS [customerID],NULL AS [salutation],[FirstName],[LastName],[Address1],NULL AS [Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM openHosp.Guest g
				LEFT JOIN #OPEN_Guest_EmailAddress ge ON ge.openHospID = g.Guest_EmailAddressID
				LEFT JOIN #OPEN_Location l ON l.openHospID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID]
	) AS src ON ISNULL(src.[customerID],'') = ISNULL(tgt.[customerID],'') AND ISNULL(src.[salutation],'') = ISNULL(tgt.[salutation],'')
				AND ISNULL(src.[FirstName],'') = ISNULL(tgt.[FirstName],'') AND ISNULL(src.[LastName],'') = ISNULL(tgt.[LastName],'')
				AND ISNULL(src.[Address1],'') = ISNULL(tgt.[Address1],'') AND ISNULL(src.[Address2],'') = ISNULL(tgt.[Address2],'')
				AND ISNULL(src.[LocationID],'') = ISNULL(tgt.[LocationID],'') AND ISNULL(src.[phone],'') = ISNULL(tgt.[phone],'')
				AND ISNULL(src.[Guest_EmailAddressID],'') = ISNULL(tgt.[Guest_EmailAddressID],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID])
		VALUES([synXisID],[openHospID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_SubSource]'
GO

ALTER PROCEDURE [dbo].[Populate_CRS_SubSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_SubSource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG(openHospID,',') AS openHospID,[subSource],[subSourceCode]
		FROM
		(
			SELECT [SubSourceID] AS [synXisID],NULL AS [openHospID],[subSource],[subSourceCode]
			FROM synxis.SubSource
			WHERE [SubSourceID] IN(
										SELECT DISTINCT [SubSourceID]
										FROM synxis.Transactions t
											INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
										WHERE t.QueueID = @QueueID
										)

				UNION ALL

			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],bs.Bkg_Src_Cd AS [subSource],obs.Sub_Source AS [subSourceCode]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [subSource],[subSourceCode]
	) AS src ON src.[subSource] = tgt.[subSource] AND src.[subSourceCode] = tgt.[subSourceCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				openHospID = src.openHospID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],openHospID,[subSource],[subSourceCode])
		VALUES([synXisID],openHospID,[subSource],[subSourceCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_SecondarySource]'
GO

ALTER PROCEDURE [dbo].[Populate_CRS_SecondarySource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_SecondarySource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[secondarySource]
		FROM
		(
			SELECT [SecondarySourceID] AS [synXisID],NULL AS [openHospID],[secondarySource]
			FROM synxis.SecondarySource
			WHERE [SecondarySourceID] IN(
									SELECT DISTINCT [SecondarySourceID]
									FROM synxis.Transactions t
										INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
									WHERE t.QueueID = @QueueID
									)

				UNION ALL
			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],obs.Secondary_Source AS [secondarySource]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [secondarySource]
	) AS src ON src.[secondarySource] = tgt.[secondarySource]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
	INSERT([synXisID],[openHospID],[secondarySource])
	VALUES([synXisID],[openHospID],[secondarySource])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_Channel]'
GO


ALTER PROCEDURE [dbo].[Populate_CRS_Channel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_Channel] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[channel]
		FROM
		(
			SELECT [ChannelID] AS [synXisID],NULL AS [openHospID],[channel]
			FROM synxis.Channel
			WHERE [ChannelID] IN(
									SELECT DISTINCT bs.[ChannelID]
									FROM synxis.Transactions t
										INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
									WHERE t.QueueID = @QueueID
								)

				UNION ALL

			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],obs.Channel
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [channel]
	) AS src ON src.[channel] = tgt.[channel]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[channel])
		VALUES([synXisID],[openHospID],[channel])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_BookingSource]'
GO

ALTER PROCEDURE [dbo].[Populate_CRS_BookingSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_BookingSource] tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID]
		FROM
		(
			SELECT sbs.[BookingSourceID] AS [synXisID],NULL AS [openHospID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.[SubSourceID],dcro.[CROCodeID],dx.[ibeSourceID] AS [xbeTemplateID]
			FROM synxis.BookingSource sbs
				LEFT JOIN (SELECT [ChannelID], value AS synXisID FROM dbo.CRS_Channel CROSS APPLY string_split(synXisID,','))  dc ON dc.synXisID = sbs.ChannelID
				LEFT JOIN (SELECT [SecondarySourceID], value AS synXisID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(synXisID,',')) dss ON dss.synXisID = sbs.SecondarySourceID
				LEFT JOIN (SELECT [SubSourceID], value AS synXisID FROM dbo.CRS_SubSource CROSS APPLY string_split(synXisID,',')) ds ON ds.synXisID = sbs.SubSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS synXisID FROM dbo.CROCode CROSS APPLY string_split(synXisID,',')) dcro ON dcro.synXisID = sbs.CROCodeID
				LEFT JOIN (SELECT [ibeSourceID], value AS synXisID FROM dbo.ibeSource CROSS APPLY string_split(synXisID,',')) dx ON dx.synXisID = sbs.xbeTemplateID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				
			UNION ALL

			SELECT NULL AS [synXisID],obs.[BookingSourceID] AS [openHospID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.SubSourceID,dcro.CROCodeID AS [CROCodeID],dx.ibeSourceID AS [xbeTemplateID]
			FROM openHosp.BookingSource obs
				LEFT JOIN (SELECT [ChannelID], value AS openHospID FROM dbo.CRS_Channel CROSS APPLY string_split(openHospID,',')) dc ON dc.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [SecondarySourceID], value AS openHospID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(openHospID,',')) dss ON dss.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [SubSourceID], value AS openHospID FROM dbo.CRS_SubSource CROSS APPLY string_split(openHospID,',')) ds ON ds.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS openHospID FROM dbo.CROCode CROSS APPLY string_split(openHospID,',')) dcro ON dcro.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [ibeSourceID], value AS openHospID FROM dbo.ibeSource CROSS APPLY string_split(openHospID,',')) dx ON dx.openHospID = obs.BookingSourceID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
		) X
		GROUP BY [ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID]
	) AS src ON ISNULL(src.[ChannelID],'') = ISNULL(tgt.[ChannelID],'') AND ISNULL(src.[SecondarySourceID],'') = ISNULL(tgt.[SecondarySourceID],'') AND ISNULL(src.[SubSourceID],'') = ISNULL(tgt.[SubSourceID],'')
				AND ISNULL(src.[CROCodeID],'') = ISNULL(tgt.[CROCodeID],'') AND ISNULL(src.[xbeTemplateID],'') = ISNULL(tgt.[ibeSourceNameID],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[ibeSourceNameID])
		VALUES([synXisID],[openHospID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CROCode]'
GO

ALTER PROCEDURE [dbo].[Populate_CROCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CROCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],NULL AS [openHospID],[auth_CRO_CodeID]
		FROM
		(
			SELECT sCRO.[CROCodeID] AS [synXisID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM synxis.CROCode sCRO
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = sCRO.CROCode
			WHERE [CROCodeID] IN(
								SELECT DISTINCT [CROCodeID]
								FROM synxis.Transactions t
									INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
								WHERE t.QueueID = @QueueID
								)

			--UNION ALL

			--SELECT NULL AS [synXisID],bs.BookingSourceID AS [openHospID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			--FROM openHosp.BookingSource bs
			--	INNER JOIN authority.OpenHosp_BookingSource obh ON obh.Bkg_Src_Cd = bs.Bkg_Src_Cd
			--	INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = obh.CRO_Code
		) X
		GROUP BY [auth_CRO_CodeID]
	) AS src ON src.[auth_CRO_CodeID] = tgt.[auth_CRO_CodeID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[auth_CRO_CodeID])
		VALUES([synXisID],[openHospID],[auth_CRO_CodeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Country]'
GO

ALTER PROCEDURE [dbo].[Populate_Country]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Country] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],ISNULL(NULLIF([Country_Text],''),'unknown') AS [Country_Text]
		FROM
		(
			SELECT [CountryID] AS [synXisID],NULL AS [openHospID],[Country_Text]
			FROM synxis.Country
				UNION ALL
			SELECT NULL AS [synXisID],[CountryID] AS [openHospID],[Country_Text]
			FROM openHosp.Country
		) x
		GROUP BY [Country_Text]
	) AS src ON src.[Country_Text] = tgt.[Country_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[Country_Text])
		VALUES([synXisID],[openHospID],[Country_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CorporateCode]'
GO

ALTER PROCEDURE [dbo].[Populate_CorporateCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CorporateCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[corporationCode]
		FROM
		(
			SELECT [CorporateCodeID] AS [synXisID],NULL AS [openHospID],[corporationCode]
			FROM synxis.CorporateCode
			WHERE [CorporateCodeID] IN(SELECT [CorporateCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],CorpInfoCodeID AS [openHospID],CorpInfoCode AS [corporationCode]
			FROM openHosp.CorpInfoCode
			WHERE [CorpInfoCodeID] IN(SELECT [CorpInfoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [corporationCode]
	) AS src ON src.[corporationCode] = tgt.[corporationCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[corporationCode])
		VALUES([synXisID],[openHospID],[corporationCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_City]'
GO

ALTER PROCEDURE [dbo].[Populate_City]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[City] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[City_Text]
		FROM
		(
			SELECT [CityID] AS [synXisID],NULL AS [openHospID],ISNULL(NULLIF([City_Text],''),'unknown') AS [City_Text]
			FROM synxis.City
				UNION ALL
			SELECT NULL AS [synXisID],[CityID] AS [openHospID],ISNULL(NULLIF([City_Text],''),'unknown') AS [City_Text]
			FROM openHosp.City
		) x
		GROUP BY [City_Text]
	) AS src ON src.[City_Text] = tgt.[City_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[City_Text])
		VALUES([synXisID],[openHospID],[City_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Chain]'
GO

ALTER PROCEDURE [dbo].[Populate_Chain]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Chain] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[ChainName],[ChainID]
		FROM
			(
				SELECT [intChainID] AS [synXisID],NULL AS [openHospID],[ChainName],[ChainID]
				FROM synxis.Chain
					UNION ALL
				SELECT NULL AS [synXisID],[intChainID] AS [openHospID],[ChainName],[ChainID]
				FROM openHosp.Chain
			) x
		GROUP BY [ChainName],[ChainID]
	) AS src ON src.[ChainID] = tgt.[CRS_ChainID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[ChainName] = src.[ChainName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[ChainName],[CRS_ChainID])
		VALUES(src.[synXisID],src.[openHospID],src.[ChainName],src.[ChainID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_ActionType]'
GO

ALTER PROCEDURE [dbo].[Populate_ActionType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.ActionType AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],[actionType],[actionTypeOrder]
		FROM
			(
				SELECT [ActionTypeID] AS [synXisID],NULL AS [openHospID],[actionType],[actionTypeOrder]
				FROM synxis.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM synxis.Transactions t
											INNER JOIN synxis.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
					UNION ALL
				SELECT NULL AS [synXisID],[ActionTypeID] AS [openHospID],[actionType],[actionTypeOrder]
				FROM openHosp.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM openHosp.Transactions t
											INNER JOIN openHosp.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
			) x
		GROUP BY [actionType],[actionTypeOrder]
	) AS src ON src.[actionType] = tgt.[actionType]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[actionTypeOrder] = src.[actionTypeOrder]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[actionType],[actionTypeOrder])
		VALUES(src.[synXisID],src.[openHospID],src.[actionType],src.[actionTypeOrder])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_RunRules]'
GO


ALTER PROCEDURE [dbo].[PH_BookingSource_RunRules]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- CREATE #BOOKING ----------------------------------------------------
		IF OBJECT_ID('tempdb..#BOOKING') IS NOT NULL
			DROP TABLE #BOOKING;

		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	-----------------------------------------------------------------------


	-- POPULATE GDS -------------------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE GDS'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'GDS',ss.secondarySource,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),''))
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel = 'GDS'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE IBE - Brand -----------------------------------------------
		-- PRINT STATUS --
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE IBE - Brand'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',autx.ibeSourceName,ss.secondarySource
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID IN(	SELECT ibeSource_GroupID
											FROM Reservations.[authority].[ibeSource_Group]
											WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
										  )
			AND t.QueueID = @QueueID
		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
		WHERE t.openHospID IS NOT NULL
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE IBE - Hotel -----------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE IBE - Hotel'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'IBE - Hotel',autx.ibeSourceName,ss.secondarySource
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			LEFT JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			LEFT JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID NOT IN(	SELECT ibeSource_GroupID
												FROM Reservations.[authority].[ibeSource_Group]
												WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
											  )
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE OTA Connect -----------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE OTA Connect'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'OTA Connect',
				CASE WHEN sub.subSourceCode = 'CCX' THEN 'Channel Connect Express' ELSE c.channel END,
				CASE
					WHEN c.channel = 'Channel Connect' THEN ss.secondarySource
					WHEN ss.secondarySource = 'Pegs ADS' THEN ISNULL(sub.subSource,sub.subSourceCode)
					WHEN c.channel = 'IDS' THEN ISNULL(sub.subSource,ss.secondarySource)
				END
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel IN('Channel Connect','IDS','Google')
			AND t.QueueID = @QueueID


		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE PMS -------------------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE PMS'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT confirmationNumber,'PMS','PMS','PMS'
		FROM
			(
				SELECT DISTINCT t.confirmationNumber
				FROM dbo.Transactions t
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
				WHERE t.synXisID IS NOT NULL
					AND c.channel IN('PMS','PMS Rez Synch')
					AND t.QueueID = @QueueID
		
				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
				WHERE hh.HotelCode = 'CHIAE'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'SHSIntegrationTeam@sabre.com(Integration Integration)'
					AND t.QueueID = @QueueID

				UNION ALL

				SELECT DISTINCT t.confirmationNumber
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					INNER JOIN dbo.Transactions t ON t.HotelID = h.HotelID
					INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
					INNER JOIN dbo.CRS_Channel ch ON ch.ChannelID = bs.ChannelID
					INNER JOIN dbo.VoiceAgent va ON va.VoiceAgentID = t.VoiceAgentID
					INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
					INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
				WHERE hh.HotelCode = 'DALHL'
					AND ch.channel = 'Voice'
					AND va.VoiceAgent = 'integrations@sabre.com(integration user)'
					AND acro.CRO_Code = ''
					AND t.QueueID = @QueueID
			) x


		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------
	
	-- POPULATE Voice - Hotel ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Voice - Hotel'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'Voice - Hotel',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			INNER JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			INNER JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel IN('Voice')
			AND acrog.CRO_Code_Group = 'Hotel Voice Agent'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE Voice - Brand ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Voice - Brand'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM Reservations.authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND ISNULL(sub.subSourceCode,'') != 'VCG'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.openHospID IS NOT NULL
			AND ISNULL(acro.CRO_Code_GroupID,0) NOT IN(SELECT CRO_Code_GroupID FROM Reservations.authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- POPULATE Voice - Call Gated ----------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Voice - Call Gated'
		RAISERROR(@print,10,1) WITH NOWAIT

		DELETE #BOOKING

		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel)
		SELECT DISTINCT t.confirmationNumber,'Voice - Call Gated',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')) 
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN [authority].[CRO_Code_Group] acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.synXisID IS NOT NULL
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM Reservations.authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND sub.subSourceCode = 'VCG'
			AND t.QueueID = @QueueID

		EXEC dbo.PH_BookingSource_Populate
	-----------------------------------------------------------------------

	-- MAP UNKNOWN PH Booking Sources -------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': MAP UNKNOWN PH Booking Sources'
		RAISERROR(@print,10,1) WITH NOWAIT

		EXEC dbo.PH_BookingSource_MapUnknowns
	-----------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Normalized_dbo]'
GO

ALTER PROCEDURE [dbo].[Populate_Normalized_dbo]
	@TruncateBeforeLoad bit = 0,
	@QueueID int = NULL,
	@StepID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000),
			@errorMessage nvarchar(MAX),
			@count int

	BEGIN TRY
		-- TRUNCATE DBO TABLES -------------------------------------------------
		IF @TruncateBeforeLoad = 1
		BEGIN
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': TRUNCATE DBO TABLES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.TruncateTablesWithinSchema 'dbo'
		END
		-------------------------------------------------------------------

		-- dbo.ActionType -------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.ActionType'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_ActionType @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Channel] ------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Channel'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [dbo].[Populate_CRS_Channel] @QueueID
		-------------------------------------------------------------------

		-- [dbo].[CRS_SecondarySource] ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.CRS_SecondarySource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CRS_SecondarySource @QueueID
		-------------------------------------------------------------------

		-- [dbo].[SubSource] ----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.SubSource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CRS_SubSource @QueueID
		-------------------------------------------------------------------

		-- [authority].[CRO_Code] -----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': authority.CROCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC authority.Populate_CRO_Code
		-------------------------------------------------------------------

		-- [dbo].[CROCode] ------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.CROCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CROCode @QueueID
		-------------------------------------------------------------------

		-- [authority].[ibeSource] ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [authority].ibeSource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC authority.Populate_ibeSource
		-------------------------------------------------------------------

		-- [dbo].[ibeSource] --------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.[ibeSource]'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_ibeSource @QueueID
		-------------------------------------------------------------------

		-- [dbo].[BookingSource] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.BookingSource'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CRS_BookingSource @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Chain] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Chain'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Chain
		-------------------------------------------------------------------

		-- [dbo].[City] ---------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.City'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_City
		-------------------------------------------------------------------

		-- [dbo].[CorporateCode] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.CorporateCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_CorporateCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Country] ------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Country'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Country
		-------------------------------------------------------------------

		-- [dbo].[Guest_EmailAddress] -------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Guest_EmailAddress'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Guest_EmailAddress @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Guest_CompanyName] --------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Guest_CompanyName'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Guest_CompanyName @QueueID
		-------------------------------------------------------------------

		-- [dbo].[hotel] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.hotel'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_hotel @QueueID
		-------------------------------------------------------------------

		-- [dbo].[IATANumber] ---------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.IATANumber'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_IATANumber @QueueID
		-------------------------------------------------------------------

		-- [dbo].[State] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.State'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_State
		-------------------------------------------------------------------

		-- [dbo].[PostalCode] ---------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.PostalCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_PostalCode
		-------------------------------------------------------------------

		-- [dbo].[Region] -------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Region'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Region
		-------------------------------------------------------------------

		-- [dbo].[Location] -----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Location'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Location @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Guest] --------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Guest'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Guest @QueueID
		-------------------------------------------------------------------

		-- [dbo].[LoyaltyNumber] ------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.LoyaltyNumber'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_LoyaltyNumber @QueueID
		-------------------------------------------------------------------

		-- [dbo].[PMSRateTypeCode] ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.PMSRateTypeCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_PMSRateTypeCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[PromoCode] ----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.PromoCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_PromoCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[RateCategory] -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.RateCategory'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_RateCategory @QueueID
		-------------------------------------------------------------------

		-- [dbo].[RateCode] -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.RateCode'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_RateCode @QueueID
		-------------------------------------------------------------------

		-- [dbo].[RoomType] -----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.RoomType'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_RoomType @QueueID
		-------------------------------------------------------------------

		-- [dbo].[TravelAgent] --------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.TravelAgent'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_TravelAgent @QueueID
		-------------------------------------------------------------------

		-- [dbo].[VoiceAgent] ----------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.VoiceAgent'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_VoiceAgent @QueueID
		-------------------------------------------------------------------

		-- [dbo].[TransactionStatus] --------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.TransactionStatus'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_TransactionStatus @QueueID
		-------------------------------------------------------------------

		-- [dbo].[TransactionDetail] --------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.TransactionDetail'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_TransactionDetail @QueueID
		-------------------------------------------------------------------

		-- POPULATE TEMP TABLES -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE TEMP TABLES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC temp.Populate_TempTables @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Transactions] -------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Transactions'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Populate_Transactions @QueueID
		-------------------------------------------------------------------

		-- [dbo].[Upload_LoyaltyNumber] -----------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Upload_LoyaltyNumber'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.Upload_LoyaltyNumber
		-------------------------------------------------------------------

		-- [dbo].[Update_LoyaltyTagged] -----------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.Update_LoyaltyTagged'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.[Update_LoyaltyTagged]
		-------------------------------------------------------------------
	
		-- dbo.UPDATE_CRS_BookingSourceID ---------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': dbo.UPDATE_CRS_BookingSourceID'
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.UPDATE_CRS_BookingSourceID @QueueID
		-------------------------------------------------------------------
	
		-- [dbo].[MostRecentTransaction] ----------------------------------
			-- PRINT STATUS --
			--SET @print = '	' + CONVERT(varchar(100),GETDATE(),120) + ': dbo.MostRecentTransaction'
			--RAISERROR(@print,10,1) WITH NOWAIT

			--EXEC dbo.Populate_MostRecentTransaction
		-------------------------------------------------------------------

		-- MANUAL FIXES ---------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': MANUAL FIXES'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC [operations].[Run_ManualFixes]
		-------------------------------------------------------------------

		-- UPDATE MRT IATA NUMBERS ----------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': UPDATE MRT IATA NUMBERS'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.SabreIATA_Update_MRT
		-------------------------------------------------------------------

		-- RUN PH Booking Source Rules ------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': RUN PH Booking Source Rules'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT

			EXEC dbo.PH_BookingSource_RunRules @QueueID
		-------------------------------------------------------------------

		-- FINISHED -------------------------------------------------------
			-- PRINT STATUS --
			SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
			RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
			RAISERROR(@print,10,1) WITH NOWAIT
		-------------------------------------------------------------------

		SELECT @count = COUNT(*) FROM dbo.Transactions WHERE QueueID = @QueueID
		EXEC ETL.dbo.qs_UpdateQueueStatus @QueueID,@StepID,2,'SUCCESS',@count

	END TRY
	BEGIN CATCH
		SELECT @errorMessage =  ERROR_PROCEDURE() + ': ' + ERROR_MESSAGE()

		EXEC ETL.dbo.qs_UpdateQueueStatus @QueueID,@StepID,3,@errorMessage,0

		RAISERROR(@errorMessage,16,1)
	END CATCH
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_LoyaltyProgram]'
GO

CREATE PROCEDURE [dbo].[Populate_LoyaltyProgram]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[LoyaltyProgram] AS tgt
	USING
	(
		SELECT DISTINCT [LoyaltyProgramID] AS [synXisID],NULL AS [openHospID],[loyaltyProgram]
		FROM synxis.LoyaltyProgram
		WHERE [LoyaltyProgramID] IN(SELECT [LoyaltyProgramID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[loyaltyProgram] = tgt.[loyaltyProgram]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[loyaltyProgram])
		VALUES([synXisID],[openHospID],[loyaltyProgram])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_temp_synXis_RoomType] on [temp].[synXis_RoomType]'
GO
ALTER TABLE [temp].[synXis_RoomType] ADD CONSTRAINT [PK_temp_synXis_RoomType] PRIMARY KEY CLUSTERED  ([synXisID], [RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_emailAddress] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_emailAddress] ON [dbo].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_IC_Others] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_IC_Others] ON [dbo].[TransactionDetail] ([confirmationNumber]) INCLUDE ([adultCount], [arrivalDate], [arrivalDOW], [averageDailyRate], [bookingLeadTime], [childrenCount], [commisionPercent], [currency], [departureDate], [departureDOW], [IsPrimaryGuest], [LoyaltyNumberTagged], [LoyaltyNumberValidated], [nights], [optIn], [reservationRevenue], [rooms], [totalGuestCount], [totalPackageRevenue])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_IC_Others] on [dbo].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_IC_Others] ON [dbo].[TransactionStatus] ([confirmationNumber]) INCLUDE ([ActionTypeID], [cancellationDate], [cancellationNumber], [confirmationDate], [creditCardType], [status])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionID] on [openHosp].[MostRecentTransaction]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionID] ON [openHosp].[MostRecentTransaction] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_KeyColumns] on [openHosp].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_KeyColumns] ON [openHosp].[Transactions] ([QueueID]) INCLUDE ([BookingSourceID], [CorpInfoCodeID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [PromoCodeID], [RateTypeCodeID], [TransactionDetailID], [TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionID] on [synxis].[MostRecentTransaction]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionID] ON [synxis].[MostRecentTransaction] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_KeyColumns] on [synxis].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_KeyColumns] ON [synxis].[Transactions] ([QueueID]) INCLUDE ([BillingDescriptionID], [BookingSourceID], [confirmationNumber], [ConsortiaID], [CorporateCodeID], [Guest_CompanyNameID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [LoyaltyNumberID], [LoyaltyProgramID], [PMSRateTypeCodeID], [PromoCodeID], [RateCategoryID], [RateTypeCodeID], [RoomTypeID], [TransactionDetailID], [TransactionsExtendedID], [TransactionStatusID], [TravelAgentID], [UserNameID], [VIP_LevelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [dbo].[LoyaltyProgram] ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
