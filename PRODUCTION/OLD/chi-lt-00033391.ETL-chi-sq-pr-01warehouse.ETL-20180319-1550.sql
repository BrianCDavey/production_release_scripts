/*
Run this script on:

        chi-sq-pr-01\warehouse.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 3/19/2018 3:50:29 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[IPTaggedCustomers_FinalImport]'
GO

ALTER PROCEDURE [dbo].[IPTaggedCustomers_FinalImport]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	-------------------------------------------------------------------
	INSERT INTO Superset.BSI.TaggedCustomers([Hotel_Code],[iPrefer Number],[taggedStatus],[DateTagged])
	SELECT DISTINCT ip.[Hotel_Code],ip.[iPrefer Number],ip.[taggedStatus],ip.[DateTagged]
	FROM dbo.Import_IPTaggedCustomers ip
		LEFT JOIN Superset.BSI.TaggedCustomers tag ON tag.Hotel_Code = ip.Hotel_Code AND tag.[iPrefer Number] = ip.[iPrefer Number]
	WHERE QueueID = @QueueID
		AND tag.Hotel_Code IS NULL

	UPDATE tag
		SET taggedStatus = ip.taggedStatus,
			DateTagged = ip.DateTagged
	FROM Superset.BSI.TaggedCustomers tag
		INNER JOIN dbo.Import_IPTaggedCustomers ip ON ip.[Hotel_Code] = tag.[Hotel_Code] AND ip.[iPrefer Number] = tag.[iPrefer Number]
	WHERE ip.QueueID = @QueueID
	-------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[IPCustProf_UpdateLoyaltyNumber]'
GO

ALTER PROCEDURE [dbo].[IPCustProf_UpdateLoyaltyNumber]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	------------------------------------------------------------------------
	;WITH cte_Loyalty([iPrefer_Number],Membership_Date,value)
	AS
	(
		SELECT [iPrefer_Number],Membership_Date, 1
		FROM [Superset].[BSI].[Customer_Profile_Import]
		WHERE ISDATE(Membership_Date) = 1
	)
	UPDATE mrt
		SET LoyaltyNumberValidated = ISNULL(c.value,0)
	FROM Superset.dbo.mostrecenttransactions mrt
		LEFT OUTER JOIN cte_Loyalty c ON c.[iPrefer_Number] = mrt.loyaltyNumber
	WHERE mrt.arrivalDate >= c.Membership_Date
		AND mrt.arrivalDate >= @firstDayOfMonth
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	;WITH cte_Loyalty([iPrefer_Number],Membership_Date,Email,value)
	AS
	(
		SELECT [iPrefer_Number],Membership_Date,Email, 1
		FROM [Superset].[BSI].[Customer_Profile_Import]
		WHERE ISDATE(Membership_Date) = 1
	)
	UPDATE mrt
		SET loyaltyNumber = c.[iPrefer_Number],
			LoyaltyNumberValidated = ISNULL(c.value,0)
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN [ReservationBilling].[dbo].[Templates] temp ON temp.xbeTemplateName = mrt.xbeTemplateName
		LEFT OUTER JOIN cte_Loyalty c ON c.Email = mrt.customerEmail
	WHERE temp.templateGroupID = 3
		AND ISNULL(mrt.loyaltyNumber,'') = ''
		AND mrt.arrivalDate >= c.Membership_Date
		AND mrt.arrivalDate >= @firstDayOfMonth
	------------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPTaggedCustomers_UpdateLoyaltyTagged]'
GO

CREATE PROCEDURE [dbo].[IPTaggedCustomers_UpdateLoyaltyTagged]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	-------------------------------------------------------------------
	UPDATE mrt
		SET LoyaltyNumberTagged = 1
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN Superset.[BSI].[TaggedCustomers] tag ON tag.[iPrefer Number] = mrt.loyaltyNumber AND tag.Hotel_Code = mrt.hotelCode
	WHERE mrt.arrivalDate >= tag.DateTagged
		AND mrt.arrivalDate >= @firstDayOfMonth
		

	UPDATE mrt
		SET LoyaltyNumberTagged = 1
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN [Superset].[BSI].[Customer_Profile_Import] cust ON cust.Email = mrt.customerEmail
		INNER JOIN Superset.[BSI].[TaggedCustomers] tag ON tag.[iPrefer Number] = cust.iPrefer_Number AND tag.Hotel_Code = mrt.hotelCode
		INNER JOIN [ReservationBilling].[dbo].[Templates] temp ON temp.xbeTemplateName = mrt.xbeTemplateName
	WHERE mrt.arrivalDate >= tag.DateTagged
		AND mrt.arrivalDate >= @firstDayOfMonth
		AND temp.templateGroupID = 3
	-------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
