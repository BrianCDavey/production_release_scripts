/*
Run this script on:

        chi-sq-st-01\warehouse.BillingRewrite    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.BillingRewrite

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 1/8/2018 9:34:10 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[CROCodes]'
GO
ALTER TABLE [dbo].[CROCodes] DROP CONSTRAINT [FK_CROCodes_CROGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Sources]'
GO
ALTER TABLE [dbo].[Sources] DROP CONSTRAINT [FK_Sources_CROGroups]
ALTER TABLE [dbo].[Sources] DROP CONSTRAINT [FK_Sources_TemplateGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Templates]'
GO
ALTER TABLE [dbo].[Templates] DROP CONSTRAINT [FK_Templates_TemplateGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ThresholdRules]'
GO
ALTER TABLE [dbo].[ThresholdRules] DROP CONSTRAINT [FK_ThresholdRules_ThresholdTypes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [translate].[CriteriaGroupsPerHotel]'
GO
CREATE TABLE [translate].[CriteriaGroupsPerHotel]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[newCriteriaGroupID] [int] NOT NULL,
[oldCriteriaID] [int] NOT NULL,
[hotelCode] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TranslateCriteriaGroupPerHotel] on [translate].[CriteriaGroupsPerHotel]'
GO
ALTER TABLE [translate].[CriteriaGroupsPerHotel] ADD CONSTRAINT [PK_TranslateCriteriaGroupPerHotel] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [translate].[insertTranslateCriteriaGroupsPerHotel]'
GO


CREATE PROCEDURE [translate].[insertTranslateCriteriaGroupsPerHotel]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET IDENTITY_INSERT [translate].[CriteriaGroupsPerHotel] ON 
;

INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (1, 109, 233, N'CHICC')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (2, 274, 233, N'LONML')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (3, 274, 139, N'LONML')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (4, 275, 139, N'MEXMM')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (5, 282, 149, N'ILGHD')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (6, 283, 149, N'ANKDA')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (7, 283, 155, N'ANKDA')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (8, 284, 155, N'ANKDC')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (9, 285, 155, N'AYTDA')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (10, 285, 158, N'AYTDA')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (11, 284, 155, N'BXNDB')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (12, 286, 158, N'ISTDI')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (13, 286, 155, N'ISTDI')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (14, 286, 149, N'ISTDI')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (15, 287, 158, N'ISTDH')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (16, 287, 149, N'ISTDH')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (17, 297, 208, N'MADMM')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (18, 298, 208, N'BCNMB')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (19, 299, 208, N'SVQGM')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (20, 302, 147, N'PEKHK')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (21, 302, 245, N'PEKHK')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (22, 303, 147, N'ISTDA')
;
INSERT [translate].[CriteriaGroupsPerHotel] ([id], [newCriteriaGroupID], [oldCriteriaID], [hotelCode]) VALUES (23, 304, 245, N'SHAPH')
;
SET IDENTITY_INSERT [translate].[CriteriaGroupsPerHotel] OFF
;


END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[Populate_MrtForCalc_RulesApplied]'
GO
ALTER PROCEDURE [work].[Populate_MrtForCalc_RulesApplied]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	TRUNCATE TABLE work.MrtForCalc_RulesApplied

	INSERT INTO work.MrtForCalc_RulesApplied(confirmationNumber,hotelCode,hotelName,ClauseID,ClauseName,CriteriaGroupID,
												CriteriaGroupName,CriteriaID,CriteriaName,SourceID,SourceName,
												RateCategoryCode,RateCode,TravelAgentGroupID)
	SELECT DISTINCT mrt.confirmationNumber,mrt.hotelCode,mrt.hotelName,
					GREEN.clauseID as greenClauseID,
					GREEN.clauseName as greenClauseName,
					GREEN.criteriaGroupIncludeID as greenCriteriaGroupID,
					GREEN.criteriaGroupIncludeName as greenCriteriaGroupName,
					GREEN.criteriaGroupIncludeCriteriaIncludeID as greenCriteriaID,
					GREEN.criteriaGroupIncludeCriteriaIncludeName as greenCriteriaName,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID as greenSourceID,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeName as greenSourceName,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode AS greenRateCategoryCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCode AS greenRateCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID AS greenTravelAgentGroupID
	FROM [work].[MrtForCalculation] mrt

	--get PHR hotel code from MRT
	LEFT OUTER JOIN Core.dbo.hotelsReporting hr ON (mrt.synxisID = hr.synxisID OR mrt.synxisID = hr.openHospitalityCode)

	--reservation may or may not have a template in our list of templates
	LEFT OUTER JOIN dbo.Templates t ON mrt.xbeTemplateName = t.xbeTemplateName

	--if the res template is not in our list, it is considered a hotel template (group 2)
	LEFT OUTER JOIN dbo.TemplateGroups tg ON ISNULL(t.templateGroupID,2) = tg.templateGroupID 

	--reservation may or may not have a CRO Code in our list of templates
	LEFT OUTER JOIN dbo.CROCodes cro ON mrt.CROCode = cro.croCode

	--see if the IATA number on the reservation is part of any travel agent groups
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups GREEN_taitag ON mrt.bookingIATA = GREEN_taitag.travelAgentId AND mrt.confirmationDate BETWEEN GREEN_taitag.startDate AND GREEN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--match to clause by inclusions--CICG_CGIC_SGIS
	INNER JOIN vw_clausesFlattened GREEN ON hr.code = GREEN.hotelCode --the clause is for the same hotel as the reservation
		--match the source template group to the reservation's xbe template
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID = 0 --included source is using the wildcard group
			OR
			tg.templateGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the included source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID = 0 --included source is using the wildcard group
			OR
			cro.croGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the included source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel = '*' --included source is using the wildcard
			OR
			mrt.bookingChannel = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource = '*' --included source is using the wildcard
			OR
			mrt.bookingSecondarySource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource = '*' --included source is using the wildcard
			OR
			mrt.bookingSubSourceCode = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID = 0 --included criteria is using wildcard
				OR
				GREEN_taitag.travelAgentGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)
	--see if we match any sources excluded from the matched include criteria --CICG_CGIC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened RED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeID = RED.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			RED.templateGroupID = 0 --source is using the wildcard group
			OR
			tg.templateGroupID = RED.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			RED.CroGroupID = 0 --source is using the wildcard group
			OR
			cro.croGroupID = RED.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			RED.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = RED.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			RED.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = RED.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			RED.SubSource = '*' -- source is using the wildcard
			OR
			mrt.bookingSubSourceCode = RED.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups ORANGE_taitag
		ON mrt.bookingIATA = ORANGE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN ORANGE_taitag.startDate AND ORANGE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria excluded from the criteria group we matched to
	LEFT OUTER JOIN vw_criteriaFlattened ORANGE --CICG_CGEC_SGIS
		ON GREEN.criteriaGroupIncludeCriteriaExcludeID = ORANGE.criteriaID 

		--the criteria was excluded at the time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate

		--match the source template group to the reservation's xbe template
		AND (ORANGE.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR tg.templateGroupID = ORANGE.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (ORANGE.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR cro.croGroupID = ORANGE.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (ORANGE.sourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = ORANGE.sourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND (ORANGE.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR mrt.bookingSecondarySource = ORANGE.sourceIncludeSecondarySource --or the value matches exactly
		)
	 
		--match the source sub source to the res sub source
		AND (ORANGE.sourceIncludeSubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = ORANGE.sourceIncludeSubSource --or the value matches exactly
		) 

		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate

		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				ORANGE.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				ORANGE_taitag.travelAgentGroupID = ORANGE.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				ORANGE.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = ORANGE.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				ORANGE.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = ORANGE.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			ORANGE.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND ORANGE.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND ORANGE.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND ORANGE.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CICG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened INDIGO ON ORANGE.sourceExcludeID = INDIGO.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			INDIGO.templateGroupID = 0 --source is using the wildcard group
			OR
			tg.templateGroupID = INDIGO.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			INDIGO.CroGroupID = 0 --source is using the wildcard group
			OR
			cro.croGroupID = INDIGO.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			INDIGO.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = INDIGO.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			INDIGO.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = INDIGO.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND (INDIGO.SubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = INDIGO.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BLUE_taitag
		ON mrt.bookingIATA = BLUE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BLUE_taitag.startDate AND BLUE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria groups excluded from our matched clause
	LEFT OUTER JOIN vw_criteriaGroupsFlattened BLUE --CECG_CGIC_SGIS
		ON GREEN.criteriaGroupExcludeID = BLUE.criteriaGroupID

		--the criteria group was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate


		--match the source template group to the reservation's xbe template
		AND (BLUE.criteriaIncludeSourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR tg.templateGroupID = BLUE.criteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (BLUE.criteriaIncludeSourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR cro.croGroupID = BLUE.criteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (BLUE.criteriaIncludeSourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = BLUE.criteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BLUE.criteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BLUE.criteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BLUE.criteriaIncludeTravelAgentGroupID = 0 --criteria is using wildcard
				OR
				BLUE_taitag.travelAgentGroupID = BLUE.criteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BLUE.criteriaIncludeRateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BLUE.criteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BLUE.criteriaIncludeRateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BLUE.criteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BLUE.criteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BLUE.criteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the matched criteria group exclusion --CECG_CGIC_SGIS
	LEFT OUTER JOIN vw_sourcesFlattened YELLOW ON BLUE.criteriaIncludeSourceExcludeID = YELLOW.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			YELLOW.templateGroupID = 0 --source is using the wildcard group
			OR
			tg.templateGroupID = YELLOW.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			YELLOW.CroGroupID = 0 --source is using the wildcard group
			OR
			cro.croGroupID = YELLOW.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			YELLOW.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = YELLOW.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			YELLOW.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = YELLOW.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			YELLOW.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = YELLOW.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BROWN_taitag ON mrt.bookingIATA = BROWN_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BROWN_taitag.startDate AND BROWN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match a criteria excluded from the criteria group exclusion --CECG_CGEC_SGIS
	LEFT OUTER JOIN vw_criteriaFlattened BROWN ON BLUE.criteriaExcludeID = BROWN.criteriaID 
		--the criteria was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			BROWN.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR
			tg.templateGroupID = BROWN.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			BROWN.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR
			cro.croGroupID = BROWN.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			BROWN.sourceIncludeChannel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = BROWN.sourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			BROWN.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BROWN.sourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BROWN.sourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BROWN.sourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BROWN.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				BROWN_taitag.travelAgentGroupID = BROWN.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BROWN.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BROWN.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BROWN.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BROWN.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BROWN.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BROWN.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BROWN.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BROWN.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CECG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened VIOLET ON BROWN.sourceExcludeID = VIOLET.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			VIOLET.templateGroupID = 0 --source is using the wildcard group
			OR
			tg.templateGroupID = VIOLET.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			VIOLET.CroGroupID = 0 --source is using the wildcard group
			OR
			cro.croGroupID = VIOLET.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			VIOLET.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = VIOLET.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			VIOLET.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = VIOLET.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			VIOLET.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = VIOLET.SubSource --or the value matches exactly
		) 

	WHERE 
	(
		(
			GREEN.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
			AND RED.sourceId IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
		)
		AND 
		(
			ORANGE.sourceIncludeID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
			OR INDIGO.sourceId IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
		)
	)
	AND
	(
		(
			BLUE.criteriaIncludeSourceIncludeID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
			OR YELLOW.sourceId IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
		)
		OR
		(
			BROWN.sourceIncludeID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
			AND VIOLET.sourceId IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
		)
	)
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[run_LoadWorkTables]'
GO

ALTER PROCEDURE [work].[run_LoadWorkTables]
	@startDate date = null,
	@endDate date = null,
	@hotelCode nvarchar(20) = null,
	@confirmationNumber nvarchar(20) = null
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @RunID int;

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-1,mrt.arrivalDate),
				@endDate = DATEADD(DAY,1,mrt.arrivalDate),
				@hotelCode = COALESCE(synxis.hotelCode,oh.hotelCode)
		FROM Superset.dbo.mostrecenttransactions mrt
			LEFT JOIN Core.dbo.hotels_Synxis synxis ON synxis.synxisID = mrt.hotelID
			LEFT JOIN Core.dbo.hotels_OpenHospitality oh ON oh.openHospitalityCode = mrt.OpenHospitalityID
		WHERE mrt.confirmationNumber = @confirmationNumber
	END

	IF(@startDate IS NULL AND @endDate IS NULL)
	BEGIN
		IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
		BEGIN
			SET @startDate = DATEADD (mm,-1,GETDATE())
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END
		ELSE
		BEGIN
			SET @startDate = GETDATE()
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END

		SET @endDate = DATEADD(YEAR,2,@startDate)
	END
	-----------------------------------------------------------------------------

	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();
	-----------------------------------------------------------------------------

	-- POPULATE [work].[local_exchange_rates] -----------------------------------
	TRUNCATE TABLE [work].[local_exchange_rates];

	INSERT INTO [work].[local_exchange_rates](EXGTBLID,CURNCYID,EXCHDATE,TIME1,XCHGRATE,EXPNDATE,DEX_ROW_ID)
	SELECT EXGTBLID,CURNCYID,EXCHDATE,TIME1,XCHGRATE,EXPNDATE,DEX_ROW_ID
	FROM DYNAMICS.dbo.MC00100
	WHERE
		(
			EXCHDATE BETWEEN DATEADD(YEAR,-1,@startDate) AND @endDate
			OR
			EXPNDATE BETWEEN DATEADD(YEAR,-1,@startDate) AND @endDate
		)
		AND EXGTBLID LIKE '%AVG%'
		AND CURNCYID != 'USD'
	
	UNION ALL

	SELECT 'USD-AVG','USD','1900-01-01','1900-01-01 00:00:00.000',1,'9999-09-09',1
	-----------------------------------------------------------------------------

	-- POPULATE GREAT PLAINS WORKING TABLES -------------------------------------
	TRUNCATE TABLE [work].[GPCustomerTable];

	INSERT INTO [work].[GPCustomerTable](CUSTNMBR,CURNCYID)
	SELECT CUSTNMBR,CURNCYID
	FROM IC.dbo.RM00101


	TRUNCATE TABLE [work].[GPCurrencyMaster];

	INSERT INTO [work].[GPCurrencyMaster](CURNCYID,DECPLCUR)
	SELECT CURNCYID,DECPLCUR
	FROM DYNAMICS.dbo.MC40200
	-----------------------------------------------------------------------------

	-- UPDATE [Core].[dbo].[hotels_OpenHospitality] -----------------------------
		--This updates our Open Hospitality to Core hotel code translation, a bad mapping can cause errors during calculation
	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitality]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);

	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitalityPHG]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);  
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelActiveBrands] ---------------------------------
	TRUNCATE TABLE [work].[hotelActiveBrands];

	INSERT INTO [work].[hotelActiveBrands](hotelCode,mainHeirarchy)
	SELECT hb.hotelCode,MIN(b.heirarchy) AS mainHeirarchy
	FROM Core.dbo.hotels_brands hb
		INNER JOIN Core.dbo.brands AS b ON hb.brandCode = b.code
	WHERE (hb.endDatetime IS NULL)
		AND (hb.startDatetime <= GETDATE())
		OR (GETDATE() BETWEEN hb.startDatetime AND hb.endDatetime)
	GROUP BY hb.hotelCode;
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelInactiveBrands] -------------------------------
	TRUNCATE TABLE [work].[hotelInactiveBrands];

	INSERT INTO [work].[hotelInactiveBrands](hotelCode,mainHeirarchy)
	SELECT hotelCode,MIN(heirarchy) AS mainHeirarchy
	FROM
		(
			SELECT hb.hotelCode,hb.brandCode,b.heirarchy
			FROM Core.dbo.hotels_brands AS hb
				INNER JOIN
					(
						SELECT hotelCode,MAX(endDatetime) AS maxEnd
						FROM Core.dbo.hotels_brands
						GROUP BY hotelCode
					) AS maxDate ON hb.endDatetime = maxDate.maxEnd AND hb.hotelCode = maxDate.hotelCode
				INNER JOIN Core.dbo.brands b ON hb.brandCode = b.code
			GROUP BY hb.hotelCode,hb.brandCode,b.heirarchy
		) AS maxBrands
	GROUP BY hotelCode;

	------------------------------------------------------------------------

	-- POPULATE [work].[MrtForCalculation] ---------------------------------
	TRUNCATE TABLE [work].[MrtForCalculation];

	;WITH cte_mrtj
	AS
	(
		SELECT mrtj.[confirmationNumber],[hotelCode],[SynXisID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
				[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateOptionId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
				[bookingCroOptionID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
				[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookedCurrency],[currencyCodeBooked],[timeLoaded],[CRSSourceID],
				work.[billyItemCode]([synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateAbbreviation],[bookingCroOptionID],[chainID]) AS [ItemCode],
				CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) AS EXCHDATE,
				mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],ISNULL(mrtj.LoyaltyNumberValidated,0) AS LoyaltyNumberValidated,ISNULL(mrtj.LoyaltyNumberTagged,0) AS LoyaltyNumberTagged
		FROM work.mrtJoined mrtj
			LEFT JOIN (SELECT confirmationNumber,sopNumber FROM dbo.Charges) cd ON cd.confirmationNumber = mrtj.confirmationNumber AND cd.sopNumber IS NOT NULL
		WHERE mrtj.hotelCode = ISNULL(@hotelCode,mrtj.hotelCode) --either we're running all hotels, or we're just getting a specific hotel
			AND mrtj.confirmationNumber = ISNULL(@confirmationNumber,mrtj.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
			AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
			AND mrtj.hotelCode NOT IN('BCTS4','PHGTEST')
			AND cd.confirmationNumber IS NULL
	)
	INSERT INTO [work].[MrtForCalculation]([confirmationNumber],[hotelCode],[SynXisID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
			[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateOptionId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
			[bookingCroOptionID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
			[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookedCurrency],[currencyCodeBooked],[timeLoaded],[CRSSourceID],
			[ItemCode],
			EXCHDATE,
			[CURNCYID],[DECPLCUR],[hotel_XCHGRATE],[booking_XCHGRATE],loyaltyProgram,loyaltyNumber,[travelAgencyName],
			LoyaltyNumberValidated,LoyaltyNumberTagged)

	SELECT mrtj.[confirmationNumber],[hotelCode],[SynXisID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
			[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateOptionId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
			[bookingCroOptionID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
			[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookedCurrency],[currencyCodeBooked],[timeLoaded],[CRSSourceID],
			work.[billyItemCode]([synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateAbbreviation],[bookingCroOptionID],[chainID]) AS [ItemCode],
			mrtj.EXCHDATE,
			gpCustomer.CURNCYID,hotelCM.DECPLCUR,hotelCE.XCHGRATE,bookingCE.XCHGRATE,
			mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],mrtj.LoyaltyNumberValidated,mrtj.LoyaltyNumberTagged
	FROM cte_mrtj mrtj
		INNER JOIN work.GPCustomerTable gpCustomer ON mrtj.hotelCode = gpCustomer.CUSTNMBR
		INNER JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID AND mrtj.EXCHDATE BETWEEN hotelCE.EXCHDATE AND hotelCE.EXPNDATE
		INNER JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		INNER JOIN work.[local_exchange_rates] bookingCE ON mrtj.currencyCodeBooked = bookingCE.CURNCYID AND mrtj.EXCHDATE BETWEEN bookingCE.EXCHDATE AND bookingCE.EXPNDATE
	------------------------------------------------------------------------

	-- POPULATE MrtForCalc_RulesApplied ------------------------------------
	EXEC work.Populate_MrtForCalc_RulesApplied @RunID
	------------------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[clearExistingData]'
GO


ALTER PROCEDURE [translate].[clearExistingData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DELETE FROM BillingRewrite.[dbo].[BillingRules];
DELETE FROM BillingRewrite.[dbo].[ThresholdRules];
DELETE FROM BillingRewrite.[dbo].[Clauses_ExcludeCriteriaGroups];
DELETE FROM BillingRewrite.[dbo].[Clauses_IncludeCriteriaGroups];
DELETE FROM BillingRewrite.[dbo].[Clauses];
DELETE FROM BillingRewrite.[dbo].[CriteriaGroups_ExcludeCriteria];
DELETE FROM BillingRewrite.[dbo].[CriteriaGroups_IncludeCriteria];
DELETE FROM BillingRewrite.[dbo].[CriteriaGroups];
DELETE FROM BillingRewrite.[dbo].[Criteria_TravelAgentGroups];
DELETE FROM BillingRewrite.[dbo].[Criteria_SourceGroups];
DELETE FROM BillingRewrite.[dbo].[Criteria_RateCodes];
DELETE FROM BillingRewrite.[dbo].[Criteria_RateCategories];
DELETE FROM BillingRewrite.[dbo].[Criteria];
DELETE FROM BillingRewrite.[dbo].[SourceGroups_ExcludeSources];
DELETE FROM BillingRewrite.[dbo].[SourceGroups_IncludeSources];
DELETE FROM BillingRewrite.[dbo].[SourceGroups];
DELETE FROM BillingRewrite.[dbo].[Sources];
DELETE FROM BillingRewrite.[dbo].[Templates];
DELETE FROM BillingRewrite.[dbo].[CROcodes];
DELETE FROM BillingRewrite.[translate].[CriteriaGroupsPerHotel];
DELETE FROM BillingRewrite.[translate].[CriteriaGroups];
DELETE FROM BillingRewrite.[translate].[Clauses];

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertBillingRules]'
GO


ALTER PROCEDURE [translate].[insertBillingRules]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--first insert the per-hotel mapped rules
INSERT INTO [BillingRewrite].dbo.[BillingRules] (clauseID, classificationID, startDate, endDate, confirmationDate, afterConfirmation, currencyCode, thresholdMinimum, perReservationFlatFee, perRoomNightFlatFee, perReservationPercentage, percentageMinimum, percentageMaximum, refundable)
SELECT DISTINCT
	tc.newClauseID
	, c.chargeClassificationID
	, c.startDate
	, c.endDate
	, c.confirmationDate
	, c.afterOrOn
	, --duplicates were being caused by multiple currencies being selected for charges where currency didn't matter in old billy
	  CASE 
		WHEN c.chargeClassificationID = 4 --non-billable charges, currency doesn't matter
			OR (ISNULL(c.perReservationFlatFee,0) + ISNULL(c.perRoomNightFlatFee,0) + ISNULL(c.perReservationPercentageMaximum,0) + ISNULL(c.perReservationPercentageMinimum,0)  + ISNULL(c.minimumThreshold,0)) = 0 --none of the fields needing currency have values
			THEN ISNULL(h.currencyCode, 'USD') --if currency doesn't matter, use the hotel contracted currency or USD
		ELSE COALESCE(c.currencyCode, h.currencyCode,'USD') --if currency matters, use the charge currency, hotel currency, or USD
	  END as currencyCode
	, c.minimumThreshold
	, c.perReservationFlatFee
	, c.perRoomNightFlatFee
	, c.perReservationPercentage
	, c.perReservationPercentageMinimum
	, c.perReservationPercentageMaximum
	, c.refundable

  FROM [BillingRewrite].[translate].[Clauses] tc
  INNER JOIN BillingRewrite.translate.CriteriaGroupsPerHotel tcg
	ON tc.newCriteriaGroupID = tcg.newCriteriaGroupID
	AND tc.hotelCode = tcg.hotelCode
  INNER JOIN BillingProduction.dbo.charges c
	ON tcg.oldCriteriaID = c.criteriaID
	AND tcg.hotelCode = c.hotelCode
	AND tc.thresholdTypeID = c.thresholdTypeID
	AND tc.global = c.compareGlobalTotals
	AND tc.periodYears = c.resetPeriodYears
	AND tc.periodMonths = c.resetPeriodMonths
	AND tc.periodWeeks = c.resetPeriodWeeks
	AND tc.periodDays = c.resetPeriodDays
  LEFT OUTER JOIN [BillingRewrite].[dbo].[Hotels] h
	ON tcg.hotelCode = h.code

  WHERE c.criteriaID != 432 
  --432 is the FCM TMC criteria, which has a mistake loaded in 2017 at all properties, 
  --but is now being rolled up into the global TMC rules which will come from one of the other criterias mapped to the same clause

ORDER BY tc.newClauseID, c.startDate, c.chargeClassificationID, c.minimumThreshold;

--then insert the generally mapped rules
INSERT INTO [BillingRewrite].dbo.[BillingRules] (clauseID, classificationID, startDate, endDate, confirmationDate, afterConfirmation, currencyCode, thresholdMinimum, perReservationFlatFee, perRoomNightFlatFee, perReservationPercentage, percentageMinimum, percentageMaximum, refundable)
SELECT DISTINCT
	tc.newClauseID
	, c.chargeClassificationID
	, c.startDate
	, c.endDate
	, c.confirmationDate
	, c.afterOrOn
	, --duplicates were being caused by multiple currencies being selected for charges where currency didn't matter in old billy
	  CASE 
		WHEN c.chargeClassificationID = 4 --non-billable charges, currency doesn't matter
			OR (ISNULL(c.perReservationFlatFee,0) + ISNULL(c.perRoomNightFlatFee,0) + ISNULL(c.perReservationPercentageMaximum,0) + ISNULL(c.perReservationPercentageMinimum,0)  + ISNULL(c.minimumThreshold,0)) = 0 --none of the fields needing currency have values
			THEN ISNULL(h.currencyCode, 'USD') --if currency doesn't matter, use the hotel contracted currency or USD
		ELSE COALESCE(c.currencyCode, h.currencyCode,'USD') --if currency matters, use the charge currency, hotel currency, or USD
	  END as currencyCode
	, c.minimumThreshold
	, c.perReservationFlatFee
	, c.perRoomNightFlatFee
	, c.perReservationPercentage
	, c.perReservationPercentageMinimum
	, c.perReservationPercentageMaximum
	, c.refundable

  FROM [BillingRewrite].[translate].[Clauses] tc
  INNER JOIN BillingRewrite.translate.CriteriaGroups tcg
	ON tc.newCriteriaGroupID = tcg.newCriteriaGroupID
  INNER JOIN BillingProduction.dbo.charges c
	ON tcg.oldCriteriaID = c.criteriaID
	AND tc.hotelCode = c.hotelCode
	AND tc.thresholdTypeID = c.thresholdTypeID
	AND tc.global = c.compareGlobalTotals
	AND tc.periodYears = c.resetPeriodYears
	AND tc.periodMonths = c.resetPeriodMonths
	AND tc.periodWeeks = c.resetPeriodWeeks
	AND tc.periodDays = c.resetPeriodDays
  LEFT OUTER JOIN [BillingRewrite].[dbo].[Hotels] h
	ON tc.hotelCode = h.code
  LEFT OUTER JOIN BillingRewrite.dbo.BillingRules br
	ON tc.newClauseID = br.clauseID

  WHERE c.criteriaID != 432 
  --432 is the FCM TMC criteria, which has a mistake loaded in 2017 at all properties, 
  --but is now being rolled up into the global TMC rules which will come from one of the other criterias mapped to the same clause
  AND br.clauseID IS NULL --it wasn't in the insert statement above
ORDER BY tc.newClauseID, c.startDate, c.chargeClassificationID, c.minimumThreshold;

--In old billy the users thought setting an end date to before the start date was a fun way to fix mistakes, we don't need to import their mistakes
DELETE FROM BillingRewrite.dbo.BillingRules WHERE endDate <= startDate;

--only keep the earliest start date if multiple billing rules exist for the same clause with the same end date
--the assumption being these are due to combining multiple old billy criteria that may have started at different times into one new clause
--if these criteria can truly be merged into one clause, they should all be using the same billing rules
WITH cte AS 
(SELECT clauseID, classificationID, thresholdMinimum, afterConfirmation, confirmationDate, endDate, MIN(startDate) as minStart
FROM BillingRewrite.dbo.BillingRules br
GROUP BY clauseID, classificationID, thresholdMinimum, afterConfirmation, confirmationDate, endDate
HAVING COUNT(*) > 1)
DELETE br
FROM BillingRewrite.dbo.BillingRules br
INNER JOIN cte
	ON br.clauseID = cte.clauseID
	AND br.classificationID = cte.classificationID
	AND br.thresholdMinimum = cte.thresholdMinimum
	AND br.afterConfirmation = cte.afterConfirmation
	AND br.confirmationDate = cte.confirmationDate
	AND br.endDate = cte.endDate
	AND br.startDate <> cte.minStart;


--only keep the latest end date if multiple billing rules exist for the same clause with the same start date
--the assumption being these are due to combining multiple old billy criteria that may have ended at different times into one new clause
--if these criteria can truly be merged into one clause, they should all be using the same billing rules
WITH cte AS 
(SELECT clauseID, classificationID, thresholdMinimum, afterConfirmation, confirmationDate, startDate, MAX(endDate) as maxEnd
FROM BillingRewrite.dbo.BillingRules br
GROUP BY clauseID, classificationID, thresholdMinimum, afterConfirmation, confirmationDate, startDate
HAVING COUNT(*) > 1)
DELETE br
FROM BillingRewrite.dbo.BillingRules br
INNER JOIN cte
	ON br.clauseID = cte.clauseID
	AND br.classificationID = cte.classificationID
	AND br.thresholdMinimum = cte.thresholdMinimum
	AND br.afterConfirmation = cte.afterConfirmation
	AND br.confirmationDate = cte.confirmationDate
	AND br.startDate = cte.startDate
	AND br.endDate <> cte.maxEnd;


END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertClausesExclusions]'
GO


ALTER PROCEDURE [translate].[insertClausesExclusions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--add the criteria group exclusions for the clauses
INSERT INTO [BillingRewrite].[dbo].[Clauses_ExcludeCriteriaGroups] ([clauseID]
      ,[criteriaGroupID]
      ,[startDate]
      ,[endDate])

SELECT DISTINCT tc.newClauseID
      ,CASE tcge.newCriteriaGroupID
		WHEN 16 THEN 1 --Employee rate, part of standard exclusions
		WHEN 273 THEN 1 --comp marketing, part of standard exclusions
		WHEN 271 THEN 1 --travelweb, part of standard exclusions
	  ELSE tcge.newCriteriaGroupID END
      ,'1900-01-01'
      ,'9999-09-09'
FROM BillingProduction.dbo.exclusions e
INNER JOIN BillingProduction.dbo.charges c
	ON e.chargeID = c.id
INNER JOIN BillingRewrite.translate.CriteriaGroups tcgc
	ON c.criteriaID = tcgc.oldCriteriaID
INNER JOIN BillingRewrite.translate.Clauses tc
	ON tcgc.newCriteriaGroupID = tc.newCriteriaGroupID
	AND c.hotelCode = tc.hotelCode
INNER JOIN BillingRewrite.dbo.Clauses_IncludeCriteriaGroups cicg
	ON tc.newClauseID = cicg.clauseID
INNER JOIN BillingRewrite.dbo.CriteriaGroups cicgg
	ON cicg.criteriaGroupID = cicgg.criteriaGroupID
INNER JOIN BillingRewrite.translate.CriteriaGroups tcge
	ON e.excludeCriteriaID = tcge.oldCriteriaID
INNER JOIN BillingRewrite.dbo.CriteriaGroups cge
	ON tcge.newCriteriaGroupID = cge.criteriaGroupID
WHERE tcge.newCriteriaGroupID <> 0 --the any criteria group should never be excluded, this is the same as never charging
AND tcge.newCriteriaGroupID <> 4 --call gating exclusion is now handled in the groups
AND tcge.newCriteriaGroupID NOT IN (38, 39, 74) --IBE exclusions to handle template differences is now handled in the groups
AND tcge.newCriteriaGroupID <> 238 --voice agent exclusion is now handled in the groups
AND tcge.newCriteriaGroupID NOT IN (201,213) --call gating exclusion is now handled in the groups
AND tcge.newCriteriaGroupID <> cicgg.criteriaGroupID --the exclusion isn't now excluding the charge itself
AND LEFT(cge.criteriaGroupName,3) <> 'IBE' --the only reason to exclude an IBE source is now eliminated by source groups


END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteria]'
GO


ALTER PROCEDURE [translate].[insertCriteria]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET IDENTITY_INSERT [dbo].[Criteria] ON 

;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (0, N'*', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (1, N'iPrefer Manual Entry', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (3, N'Voice - Call Gated PH - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (4, N'Channel Connect', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (5, N'Comp Marketing Rooms', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (6, N'Connect X', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (7, N'COR Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (16, N'Employee Rate', 0, N'DISEMP', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (17, N'GDS - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (18, N'GDS - NEG Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (19, N'GDS - OPQ Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (34, N'GRP Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (37, N'Voice - Brand HE - No iPrefer', 1, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (38, N'IBE - Brand HE - No iPrefer', 1, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (39, N'IBE - Brand HW - No iPrefer', 1, NULL, N'08', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (40, N'IBE - Hotel - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (41, N'IBE - Hotel - GRP Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (43, N'IBE - Hotel - NEG Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (45, N'TMC - HRG NEGPAL', 0, N'HRGTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (49, N'IDS', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (50, N'IDS - OPQ Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (60, N'iPrefer Excluded Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (61, N'GDS - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (65, N'GDS - NEG Category - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (68, N'IBE - Brand iPrefer', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (69, N'IBE - Brand HE - With iPrefer', 2, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (70, N'IBE - Brand HW - With iPrefer', 2, NULL, N'08', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (71, N'IBE - Brand PH - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (72, N'IBE - Brand Residences - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (73, N'IBE - Hotel - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (76, N'MSYHM iPrefer Exclusion Rates', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (79, N'PMS - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (81, N'Voice - Brand HE - With iPrefer', 2, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (82, N'Voice - Brand HE/iPrefer - With iPrefer', 2, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (83, N'Voice - Brand PH - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (84, N'Voice - Call Gated PH - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (86, N'MER Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (96, N'Meta - DS Google', 0, N'GOOGLE', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (97, N'Meta - DS Kayak', 0, N'KAYAK', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (98, N'Meta - DS Skyscanner', 0, N'SKYSCAN', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (99, N'Meta - DS Trivago', 0, N'TRIVAGO', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (100, N'Meta - DS Wego', 0, N'WEGO', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (101, N'Meta - HotelScan', 0, N'HOTELSCAN', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (102, N'Meta - Kayak', 0, N'KAYAK', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (103, N'Meta - NexTag', 0, N'NEXTAG', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (104, N'Meta - Oyster', 0, N'OYSTER', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (105, N'Meta - PHG Affiliate Commission', 0, N'COMMJUNCTION', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (106, N'Meta - PHG Affiliate Commission Promotion', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (107, N'Meta - Skyscanner', 0, N'SKYSCAN', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (108, N'Meta - SUITEST', 0, N'SUITEST', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (109, N'Meta - Swiftrank', 0, N'SWIFTRANK', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (110, N'Meta - Trivago', 0, N'TRIVAGO', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (111, N'Meta - WEGO', 0, N'WEGO', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (115, N'NEG Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (135, N'CHICC Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (212, N'OPQ Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (223, N'OTA Connectivity', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (224, N'IBE - Brand Golf - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (225, N'IBE - Brand PH - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (226, N'IBE - Brand PH - NEG Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (227, N'IBE - Brand Residences - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (228, N'Voice - Brand PH - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (229, N'PKG Category', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (230, N'PMS - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (232, N'Priceline', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (236, N'TMC - Amex', 0, N'AMEXTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (237, N'TMC - BCD', 0, N'BCDTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (238, N'TMC - CWT', 0, N'CWTTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (239, N'TMC - FCM', 0, N'FCM', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (240, N'TMC - HRG', 0, N'HRGTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (241, N'TMC - PEHP AMEX', 0, N'AMEXTMC-INCENTIVE', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (242, N'Travelweb', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (245, N'Voice - Call Gated HE - No iPrefer', 1, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (254, N'Voice - Hotel Voice Agent', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (255, N'Voice - Brand Golf - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (274, N'IDS Hotwire', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (275, N'Voice - Call Gated HE - With iPrefer', 2, NULL, N'06', NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (276, N'Voice - Brand Golf - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (279, N'Voice - Call Gated AKA - With iPrefer', 2, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (280, N'Voice - Call Gated AKA - No iPrefer', 1, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (281, N'LONML Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (282, N'MEXMM Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (283, N'MKEHM AMEX Rates', 0, N'AMEXTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (284, N'MSPSP AMEX NEGTTG', 0, N'AMEXTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (285, N'MKEHM BCD Rates', 0, N'BCDTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (286, N'MKEHM CWT Rates', 0, N'CWTTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (287, N'MKEHM HRG Rates', 0, N'HRGTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (288, N'AMEXFHRS', 0, N'AMEXTMC', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (289, N'Classic Vacations', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (290, N'PAETR Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (291, N'NEGSHE Rate', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (292, N'ILGHD Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (293, N'ANKDA Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (294, N'NEGB2B Rate', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (295, N'AYTDA Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (296, N'ISTDI Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (297, N'ISTDH Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (298, N'Meta - DS ChaNet', 0, N'DS-CHANET', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (299, N'Meta - DS Daodao', 0, N'DS-DAODAO', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (300, N'Meta - DS Dianpin', 0, N'DS-DIANPIN', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (301, N'Meta - DS Hipmunk', 0, N'DS-HIPMUNK', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (302, N'Meta - DS Hotellook', 0, N'DS-HOTELLOOK', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (303, N'Meta - DS Lotour', 0, N'DS-LOTOUR', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (304, N'Meta - DS MediaV', 0, N'DS-MEDIAV', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (305, N'Meta - DS OpenDoor', 0, N'DS-OPENDOOR', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (306, N'Meta - DS Qunar', 0, N'DS-QUNAR', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (307, N'Meta - DS TaoBao', 0, N'DS-TAOBAO', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (308, N'Meta - DS TripReviewer', 0, N'DS-TRIPREVIEWER', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (309, N'Meta - DS Youbibi', 0, N'DS-YOUBIBI', NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (310, N'ANCCC GOV Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (311, N'ZRHZS Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (312, N'LUGTP Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (313, N'Group Rooming List Import', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (314, N'MRYPB Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (315, N'HOTWD Rate', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (316, N'TPAIR Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (317, N'Orbitz', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (318, N'LITCH Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (319, N'MADMM Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (320, N'BCNMB Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (321, N'SVQGM Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (322, N'PARLE Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (323, N'CHIAE Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (324, N'PEKHK Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (325, N'ISTDA Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (326, N'SHAPH Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (327, N'PARMP Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (328, N'SANWS Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (329, N'AMSGH Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (330, N'AMSGH Non-billable Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (331, N'BOSSH Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (332, N'STLTC Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (333, N'SJCHV Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (334, N'SANRB Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (335, N'CUNGM Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (336, N'LASTT Contracted Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (337, N'PREFE301012 Rate', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (338, N'PROZOO Rates', 0, NULL, NULL, NULL)
;
INSERT [dbo].[Criteria] ([criteriaID], [criteriaName], [loyaltyOptionID], [itemCodeOverride], [gpSiteOverride], [lastBilled]) VALUES (339, N'HOUTW Contracted Rates', 0, NULL, NULL, NULL)
;
SET IDENTITY_INSERT [dbo].[Criteria] OFF
;
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteria_RateCategories]'
GO



ALTER PROCEDURE [translate].[insertCriteria_RateCategories]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET IDENTITY_INSERT [dbo].[Criteria_RateCategories] ON 

;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1, 7, N'COR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (2, 18, N'NEG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (3, 19, N'OPQ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (4, 34, N'GRP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (5, 41, N'GRP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (6, 41, N'GROUP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (7, 43, N'NEG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (8, 50, N'OPQ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (9, 65, N'NEG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (10, 86, N'MER', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (11, 115, N'NEG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (12, 226, N'NEG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (13, 229, N'PKG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (14, 212, N'OPQ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1071, 0, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1072, 1, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1073, 3, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1074, 4, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1075, 5, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1076, 6, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1077, 16, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1078, 17, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1079, 37, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1080, 38, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1081, 39, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1082, 40, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1083, 45, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1084, 49, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1085, 60, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1086, 61, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1087, 68, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1088, 69, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1089, 70, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1090, 71, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1091, 72, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1092, 73, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1093, 76, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1094, 79, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1095, 81, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1096, 82, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1097, 83, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1098, 84, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1099, 96, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1100, 97, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1101, 98, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1102, 99, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1103, 100, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1104, 101, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1105, 102, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1106, 103, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1107, 104, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1108, 105, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1109, 106, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1110, 107, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1111, 108, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1112, 109, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1113, 110, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1114, 111, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1115, 135, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1116, 223, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1117, 224, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1118, 225, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1119, 227, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1120, 228, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1121, 230, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1122, 232, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1123, 236, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1124, 237, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1125, 238, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1126, 239, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1127, 240, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1128, 241, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1129, 242, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1130, 245, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1131, 254, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1132, 255, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1133, 274, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1134, 275, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1135, 276, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1136, 279, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1137, 280, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1138, 281, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1139, 282, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1140, 283, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1141, 284, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1143, 285, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1145, 286, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1146, 287, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1147, 288, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1148, 289, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1149, 290, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1150, 291, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1151, 292, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1152, 293, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1153, 294, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1154, 295, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1155, 296, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1156, 297, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1157, 298, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1158, 299, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1159, 300, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1160, 301, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1161, 302, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1162, 303, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1163, 304, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1164, 306, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1165, 307, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1166, 308, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1167, 309, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1168, 310, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1169, 311, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1170, 312, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1171, 313, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1172, 314, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1173, 315, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1174, 316, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1175, 317, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1176, 318, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1177, 319, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1178, 320, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1179, 321, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1180, 322, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1181, 323, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1182, 324, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1183, 325, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1184, 326, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1185, 327, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1186, 328, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1187, 329, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1188, 330, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1189, 331, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1190, 332, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1191, 333, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1192, 334, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1193, 335, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1194, 336, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1195, 337, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1196, 338, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCategories] ([id], [criteriaID], [rateCategoryCode], [startDate], [endDate], [lastBilled]) VALUES (1197, 339, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[Criteria_RateCategories] OFF
;

--add wildcards to any criteria that don't have any records
INSERT INTO BillingRewrite.dbo.Criteria_RateCategories (criteriaID, rateCategoryCode, startDate, endDate)
SELECT c.criteriaID, '*', '1900-01-01', '9999-09-09'
  FROM [BillingRewrite].[dbo].[Criteria] c
  LEFT OUTER JOIN BillingRewrite.dbo.Criteria_RateCategories rc
	ON c.criteriaID = rc.criteriaID
WHERE rc.criteriaID IS NULL

END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteria_RateCodes]'
GO



ALTER PROCEDURE [translate].[insertCriteria_RateCodes]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET IDENTITY_INSERT [dbo].[Criteria_RateCodes] ON 

;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (1, 16, N'DISEMP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2, 5, N'CMPMKT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3, 5, N'CMPMKT2', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (4, 5, N'CMPMKTB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (5, 5, N'CMPMKT3', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (6, 241, N'CONAMX', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (7, 241, N'CONJMX', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (9, 241, N'CONZMX', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (10, 241, N'CONXPV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (11, 45, N'NEGPAL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (12, 60, N'AMEXMR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (13, 60, N'CHOICE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (14, 60, N'CMOMKT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (15, 60, N'CMPMKT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (16, 60, N'CMPMKTB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (17, 60, N'DISEMP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (18, 60, N'DISTVL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (19, 60, N'GOTBLB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (20, 60, N'GOTBLS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (21, 60, N'GOTVLO', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (22, 60, N'GOTVLP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (23, 60, N'GOVSGV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (24, 60, N'GOVXVC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (25, 60, N'GOVXVI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (26, 60, N'GOVXVR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (27, 60, N'GOVXVS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (28, 60, N'GOVXVU', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (29, 60, N'LION', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (30, 60, N'NEGCHN', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (31, 60, N'NEGEIB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (32, 60, N'NEGMIN', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (33, 60, N'NEGTO2', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (34, 60, N'NEGXON', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (35, 60, N'PBTOUR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (36, 60, N'SVCELITE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (37, 60, N'TOUPRP2', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (38, 60, N'TOUR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (39, 60, N'TOURBB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (40, 60, N'TOURGS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (41, 60, N'TOURPPP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (42, 60, N'TOURPS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (43, 60, N'TOURPV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (44, 60, N'TOURRPV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2898, 76, N'1702ROY', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2899, 76, N'ACE21B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2900, 76, N'AFW15I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2901, 76, N'AMW11E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2902, 76, N'ARI31C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2903, 76, N'ATE29A', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2904, 76, N'BDW10B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2905, 76, N'CAD05A', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2906, 76, N'CBW26E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2907, 76, N'CHAD03', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2908, 76, N'COMP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2909, 76, N'CPW12D', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2910, 76, N'CTRIPBAR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2911, 76, N'DBGC01', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2912, 76, N'DCW30C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2913, 76, N'DDW26J', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2914, 76, N'DGW31C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2915, 76, N'DIXI14', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2916, 76, N'DMW08F', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2917, 76, N'EDWI11', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2918, 76, N'EPBB09', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2919, 76, N'EXL13F', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2920, 76, N'FAR04I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2921, 76, N'FIT1', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2922, 76, N'FIT2', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2923, 76, N'FPW07I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2924, 76, N'GDW15I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2925, 76, N'GHW14I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2926, 76, N'GOVGOV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2927, 76, N'HAW12E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2928, 76, N'HJW02C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2929, 76, N'HSFG07', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2930, 76, N'HVV21J', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2931, 76, N'ICCH01', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2932, 76, N'IMP08B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2933, 76, N'INS13I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2934, 76, N'IRV02C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2935, 76, N'JUU13I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2936, 76, N'KCW08I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2937, 76, N'KDW10C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2938, 76, N'KOT01I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2939, 76, N'KWW25E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2940, 76, N'LAW15I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2941, 76, N'LFW11E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2942, 76, N'LIT06I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2943, 76, N'LMW21C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2944, 76, N'LPAF04', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2945, 76, N'LTA2017', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2946, 76, N'MCC05I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2947, 76, N'MCK05I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2948, 76, N'MERBA2PH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2949, 76, N'MERBA4PH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2950, 76, N'MERBOOK', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2951, 76, N'MERBOOKIT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2952, 76, N'MEREXP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2953, 76, N'MEREXR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2954, 76, N'MEREXRETP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2955, 76, N'MERGARNR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2956, 76, N'MERGARS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2957, 76, N'MERJET', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2958, 76, N'MERMLT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2959, 76, N'MERMLTP20', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2960, 76, N'MERMLTP25', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2961, 76, N'MERTW2', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2962, 76, N'MERTWB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2963, 76, N'MLW15K', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2964, 76, N'MPW14I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2965, 76, N'MQW13A', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2966, 76, N'MYW23C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2967, 76, N'NAII10', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2968, 76, N'OAW19J', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2969, 76, N'OPQCTJ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2970, 76, N'OPQHW0', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2971, 76, N'PBW17C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2972, 76, N'PDW17K', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2973, 76, N'PFZ13I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2974, 76, N'PNW26E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2975, 76, N'PRP07I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2976, 76, N'QBS04I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2977, 76, N'SAV15I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2978, 76, N'SCE06I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2979, 76, N'SCIH30', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2980, 76, N'SHU08I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2981, 76, N'SLMC02', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2982, 76, N'SOP08E', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2983, 76, N'SPN16C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2984, 76, N'STW16F', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2985, 76, N'SUNB08', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2986, 76, N'SWE14I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2987, 76, N'TAU10I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2988, 76, N'TNW19C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2989, 76, N'WBW03C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2990, 76, N'WEBBAH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2991, 76, N'WEBBKC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2992, 76, N'WEBBOOKIT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2993, 76, N'WEBEXGPO', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2994, 76, N'WEBEXP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2995, 76, N'WEBEXR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2996, 76, N'WEBGETAR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2997, 76, N'WEBJET', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2998, 76, N'WEBOPQ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (2999, 76, N'WEBORB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3000, 76, N'WEBPLN', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3001, 76, N'WEBPLR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3002, 76, N'WEBTVL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3003, 76, N'WPW01I', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3004, 0, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3005, 1, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3006, 3, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3007, 4, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3008, 6, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3009, 7, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3010, 17, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3011, 18, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3012, 19, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3013, 34, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3014, 37, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3015, 38, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3016, 39, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3017, 40, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3018, 41, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3019, 43, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3020, 49, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3021, 50, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3022, 61, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3023, 65, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3024, 68, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3025, 69, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3026, 70, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3027, 71, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3028, 72, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3029, 73, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3030, 79, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3031, 81, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3032, 82, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3033, 83, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3034, 84, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3035, 86, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3036, 96, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3037, 97, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3038, 98, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3039, 99, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3040, 100, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3041, 101, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3042, 102, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3043, 103, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3044, 104, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3045, 105, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3046, 106, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3047, 107, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3048, 108, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3049, 109, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3050, 110, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3051, 111, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3052, 115, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3054, 212, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3055, 223, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3056, 224, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3057, 225, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3058, 226, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3059, 227, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3060, 228, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3061, 229, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3062, 230, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3063, 232, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3064, 236, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3065, 237, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3066, 238, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3067, 239, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3068, 240, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3069, 242, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3070, 245, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3071, 254, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3072, 255, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3073, 274, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3074, 275, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3075, 276, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3076, 279, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3077, 280, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3078, 135, N'NEGBPA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3079, 135, N'NEGIID', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3080, 135, N'NEGN10', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3081, 281, N'NEGBPA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3082, 281, N'CORADA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3083, 281, N'CORAUS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3084, 281, N'CORBLA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3085, 281, N'CORCHE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3086, 281, N'CORLYC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3087, 281, N'CORMIZ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3088, 281, N'CORNAV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3089, 281, N'CORTOS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3090, 281, N'NEG222', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3091, 281, N'NEGAH3', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3092, 281, N'NEGAOV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3093, 281, N'NEGAPP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3094, 281, N'NEGARW', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3095, 281, N'NEGB1C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3096, 281, N'NEGBAG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3097, 281, N'NEGBBU', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3098, 281, N'NEGBLO', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3099, 281, N'NEGBMK', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3100, 281, N'NEGBOO', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3101, 281, N'NEGC6C', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3102, 281, N'NEGCG4', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3103, 281, N'NEGDBK', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3104, 281, N'NEGFBB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3105, 281, N'NEGFRF', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3106, 281, N'NEGFTP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3107, 281, N'NEGGIO', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3108, 281, N'NEGGUP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3109, 281, N'NEGJCH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3110, 281, N'NEGLAW', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3111, 281, N'NEGLWF', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3112, 281, N'NEGMRE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3113, 281, N'NEGMTM', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3114, 281, N'NEGPEA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3115, 281, N'NEGPON', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3116, 281, N'NEGPVH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3117, 281, N'NEGRAB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3118, 281, N'NEGREL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3119, 281, N'NEGRMK', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3120, 281, N'NEGSAN', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3121, 281, N'NEGTSM', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3122, 281, N'NEGULT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3123, 281, N'NEGVIA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3124, 281, N'NEGWK1', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3125, 281, N'NEGWPP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3126, 281, N'NEGYPU', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3127, 282, N'NEGBBU', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3128, 283, N'NEGJCI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3129, 283, N'NEGKOH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3130, 284, N'NEGTTG', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3131, 285, N'NEGJCI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3132, 285, N'NEGKOH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3133, 286, N'NEGJCI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3134, 286, N'NEGKOH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3135, 287, N'NEGJCI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3136, 287, N'NEGKOH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3137, 288, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3138, 289, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3139, 290, N'CYBER', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3140, 291, N'NEGSHE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3141, 292, N'NEGDUP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3142, 292, N'NEGKGP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3143, 292, N'NEGEYP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3144, 292, N'NEGEYC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3145, 292, N'NEGDOW', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3146, 292, N'NEGDNI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3147, 292, N'NEGDLH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3148, 292, N'NEGCND', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3149, 293, N'NEGIMF', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3150, 293, N'NEGB2B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3151, 293, N'NEGEYC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3152, 294, N'NEGB2B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3153, 295, N'NEGACI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3154, 295, N'NEGB2B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3155, 296, N'NEGACI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3156, 296, N'NEGB2B', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3157, 296, N'NEGEYC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3158, 296, N'NEGNXA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3159, 296, N'NEGSEK', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3160, 297, N'NEGACI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3161, 297, N'NEGCIT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3162, 297, N'NEGERI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3163, 297, N'NEGEYC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3164, 297, N'NEGPMC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3165, 298, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3166, 299, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3167, 300, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3168, 301, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3169, 302, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3170, 303, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3171, 304, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3172, 306, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3173, 307, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3174, 308, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3175, 309, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3176, 310, N'GOVFED', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3177, 310, N'GOVGOV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3178, 311, N'NEGAON', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3179, 311, N'NEGBAR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3180, 311, N'NEGCIT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3181, 311, N'NEGCSB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3182, 311, N'NEGGLD', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3183, 312, N'NEGFHR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3184, 312, N'NEGFHZ', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3185, 313, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3186, 314, N'GRPAC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3187, 314, N'GRPPAC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3188, 315, N'HOTWD', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3189, 316, N'NEGPAL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3190, 317, N'*', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3191, 318, N'NCSTCL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3192, 318, N'NEGSIR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3193, 319, N'NEG1AM', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3194, 319, N'NEGFHD', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3195, 320, N'NEGAOI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3196, 320, N'NEGDHS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3197, 320, N'NEGFHD', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3198, 320, N'NEGGF8', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3199, 320, N'NEGTFE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3200, 321, N'NEGFHD', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3201, 321, N'NEGSH5', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3202, 322, N'NEG1AV', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3203, 322, N'NEGLVM', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3204, 322, N'NEGLVMH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3205, 323, N'NEG4AM', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3206, 323, N'NEGIRI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3207, 324, N'NEGABB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3208, 324, N'NEGABB07', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3209, 324, N'NEGEMC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3210, 324, N'NEGGE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3211, 324, N'NEGGEC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3212, 325, N'NEGABB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3213, 325, N'NEGEJA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3214, 325, N'NEGLGS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3215, 325, N'NEGSIE', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3216, 325, N'NEGZAH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3217, 326, N'NEGGEC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3218, 327, N'NEGALT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3219, 327, N'NEGBNP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3220, 327, N'NEGFIA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3221, 327, N'NEGFIS', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3222, 327, N'NEGTL7', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3223, 328, N'NEGATT', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3224, 328, N'NEGCDH', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3225, 328, N'NEGCHI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3226, 328, N'NEGCNL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3227, 328, N'NEGCRD', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3228, 328, N'NEGCWI', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3229, 328, N'NEGQUA', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3230, 328, N'NEGQUL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3231, 329, N'NEGES0', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3232, 330, N'PROPREF', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3233, 331, N'NEGFID', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3234, 332, N'NEGWAC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3235, 332, N'NEGWFB', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3236, 333, N'NEGEBY', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3237, 334, N'NEGHEW', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3238, 334, N'NEGNOR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3239, 334, N'NEGSON', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3240, 335, N'NEGGSK', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3241, 336, N'NFC1', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3242, 336, N'NFC2', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3243, 336, N'NFCC', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3244, 336, N'NOWP', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3245, 337, N'PREFE301012', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3246, 338, N'PROGPN', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3247, 338, N'PROZOO', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3248, 290, N'ONL', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_RateCodes] ([id], [criteriaID], [rateCode], [startDate], [endDate], [lastBilled]) VALUES (3249, 339, N'NEGKBR', CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[Criteria_RateCodes] OFF
;

--add wildcards to any criteria that don't have any records
INSERT INTO BillingRewrite.dbo.[Criteria_RateCodes] (criteriaID, rateCode, startDate, endDate)
SELECT c.criteriaID, '*', '1900-01-01', '9999-09-09'
  FROM [BillingRewrite].[dbo].[Criteria] c
  LEFT OUTER JOIN BillingRewrite.dbo.[Criteria_RateCodes] rc
	ON c.criteriaID = rc.criteriaID
WHERE rc.criteriaID IS NULL


END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteria_SourceGroups]'
GO



ALTER PROCEDURE [translate].[insertCriteria_SourceGroups]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET IDENTITY_INSERT [dbo].[Criteria_SourceGroups] ON 
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (1, 0, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (2, 7, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (3, 16, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (4, 241, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (5, 5, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (6, 96, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (7, 97, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (8, 98, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (9, 99, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (10, 100, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (11, 101, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (12, 102, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (13, 103, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (14, 104, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (15, 105, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (16, 106, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (17, 107, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (18, 108, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (19, 109, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (20, 110, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (21, 111, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (22, 236, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (23, 237, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (24, 238, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (25, 239, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (26, 240, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (27, 17, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (28, 38, 6, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (29, 39, 7, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (30, 224, 9, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (31, 49, 11, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (32, 68, 12, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (33, 37, 20, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (34, 242, 19, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (35, 254, 39, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (36, 1, 1, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (37, 3, 23, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (38, 4, 2, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (39, 6, 3, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (40, 18, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (41, 19, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (42, 34, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (43, 40, 8, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (44, 41, 8, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (45, 43, 8, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (46, 45, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (47, 50, 11, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (48, 60, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (49, 61, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (50, 65, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (51, 76, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (52, 86, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (53, 223, 14, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (459, 69, 6, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (460, 70, 7, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (461, 71, 15, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (462, 72, 16, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (463, 73, 8, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (464, 79, 17, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (465, 230, 17, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (466, 81, 20, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (467, 82, 21, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (468, 83, 22, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (469, 84, 23, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (470, 275, 24, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (471, 245, 24, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (472, 225, 15, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (473, 226, 15, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (474, 227, 16, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (475, 228, 22, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (476, 232, 18, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (477, 255, 40, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (478, 276, 40, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (479, 274, 44, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (480, 279, 45, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (481, 280, 45, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (482, 115, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (483, 135, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (484, 212, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (485, 229, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (486, 281, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (487, 282, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (488, 283, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (489, 284, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (490, 285, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (491, 286, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (492, 287, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (493, 288, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (494, 289, 51, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (495, 290, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (496, 291, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (497, 292, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (498, 293, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (499, 294, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (500, 295, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (501, 296, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (502, 297, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (503, 298, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (504, 299, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (505, 300, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (506, 301, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (507, 302, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (508, 303, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (509, 304, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (510, 305, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (511, 306, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (512, 307, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (513, 308, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (514, 309, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (515, 310, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (516, 311, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (517, 312, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (518, 313, 53, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (519, 314, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (520, 315, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (521, 316, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (522, 317, 54, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (523, 318, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (524, 319, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (525, 320, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (526, 321, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (527, 322, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (528, 323, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (529, 324, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (530, 325, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (531, 326, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (533, 327, 12, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (534, 328, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (535, 329, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (536, 330, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (537, 331, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (538, 332, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (539, 333, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (540, 334, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (541, 335, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (542, 336, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (543, 337, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (544, 338, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[Criteria_SourceGroups] ([id], [criteriaID], [sourceGroupID], [startDate], [endDate], [lastBilled]) VALUES (545, 339, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[Criteria_SourceGroups] OFF
;

--add wildcard to any criteria with no travel agent group records
INSERT INTO BillingRewrite.dbo.[Criteria_SourceGroups] (criteriaID, [sourceGroupID], startDate, endDate)
SELECT c.criteriaID, 0, '1900-01-01', '9999-09-09'
  FROM [BillingRewrite].[dbo].[Criteria] c
  LEFT OUTER JOIN BillingRewrite.dbo.[Criteria_SourceGroups] rc
	ON c.criteriaID = rc.criteriaID
WHERE rc.criteriaID IS NULL

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteria_TravelAgentGroups]'
GO



ALTER PROCEDURE [translate].[insertCriteria_TravelAgentGroups]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET IDENTITY_INSERT [dbo].[Criteria_TravelAgentGroups] ON 
;

INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1, 103, 103, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'NEXTAG', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (2, 102, 138, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'Kayak', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (3, 109, 141, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'SwiftrankTravel', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (4, 105, 142, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'PHGAffiliateCommission', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (5, 106, 143, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'PHGAffiliateCommissionPromotion', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (6, 110, 144, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'Trivago', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (7, 104, 145, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'Oyster', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (8, 101, 147, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'HotelScan', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (9, 236, 149, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'AMEX-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (10, 241, 149, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'AMEX-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (11, 237, 150, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'BCD-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (12, 238, 151, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'CWT-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (13, 240, 152, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'HRG-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (14, 107, 153, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'Skyscanner', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (15, 108, 156, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'Suitest', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (16, 239, 72, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'FCM', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (17, 97, 159, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Kayak', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (18, 96, 157, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-GoogleHotelFinder', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (19, 99, 161, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Trivago', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (20, 45, 152, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'HRG-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (21, 98, 162, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Skyscanner', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (22, 100, 164, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Wego', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (23, 111, 52, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'BEZURK', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (909, 0, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (910, 1, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (911, 3, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (912, 4, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (913, 5, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (914, 6, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (915, 7, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (916, 16, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (917, 17, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (918, 18, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (919, 19, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (920, 34, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (921, 37, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (922, 38, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (923, 39, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (924, 40, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (925, 41, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (926, 43, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (927, 49, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (928, 50, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (929, 60, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (930, 61, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (931, 65, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (932, 68, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (933, 69, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (934, 70, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (935, 71, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (936, 72, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (937, 73, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (938, 76, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (939, 79, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (940, 81, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (941, 82, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (942, 83, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (943, 84, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (944, 86, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (945, 115, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (946, 135, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (947, 212, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (948, 223, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (949, 224, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (950, 225, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (951, 226, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (952, 227, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (953, 228, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (954, 229, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (955, 230, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (956, 232, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (957, 242, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (958, 245, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (959, 254, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (960, 255, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (961, 274, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (962, 275, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (963, 276, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (964, 279, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (965, 280, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (966, 281, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (967, 282, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (968, 283, 149, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'AMEX-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (969, 285, 150, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'BCD-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (970, 286, 151, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'CWT-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (971, 287, 152, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'HRG-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (972, 284, 149, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'AMEX-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (973, 288, 9, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'AMEXFHRS', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (974, 289, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (975, 290, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (976, 291, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (977, 292, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (978, 293, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (979, 294, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (980, 295, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (981, 296, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (982, 297, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (983, 298, 166, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-ChaNet', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (984, 299, 167, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Daodao', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (985, 300, 168, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Dianpin', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (986, 301, 158, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Hipmunk', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (987, 302, 163, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-HotelLook', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (988, 303, 170, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Lotour', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (989, 304, 171, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-MediaV', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (990, 305, 165, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-OpenDoor', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (991, 306, 172, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Qunar', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (992, 307, 162, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Skyscanner', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (993, 307, 173, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-TaoBao', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (994, 308, 160, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-TripReviewer', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (995, 309, 164, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Wego', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (996, 309, 174, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'DS-Youbibi', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (997, 310, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (998, 311, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (999, 312, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1000, 313, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1001, 314, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1002, 315, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1003, 316, 152, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'HRG-all', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1004, 317, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1005, 318, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1006, 319, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1007, 320, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1008, 321, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1009, 322, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1010, 323, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1011, 324, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1012, 325, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1013, 326, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1014, 327, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1015, 328, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1016, 329, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1017, 330, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1018, 331, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1019, 332, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1020, 333, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1021, 334, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1022, 335, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1023, 336, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1024, 337, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1025, 338, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
INSERT [dbo].[Criteria_TravelAgentGroups] ([id], [criteriaID], [travelAgentGroupID], [startDate], [endDate], [TravelAgentGroupName], [lastBilled]) VALUES (1026, 339, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), N'*', NULL)
;
SET IDENTITY_INSERT [dbo].[Criteria_TravelAgentGroups] OFF
;

--add wildcard to any criteria with no travel agent group records
INSERT INTO BillingRewrite.dbo.[Criteria_TravelAgentGroups] (criteriaID, [travelAgentGroupID], startDate, endDate, TravelAgentGroupName)
SELECT c.criteriaID, 0, '1900-01-01', '9999-09-09', '*'
  FROM [BillingRewrite].[dbo].[Criteria] c
  LEFT OUTER JOIN BillingRewrite.dbo.[Criteria_TravelAgentGroups] rc
	ON c.criteriaID = rc.criteriaID
WHERE rc.criteriaID IS NULL

END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteriaGroups]'
GO

ALTER PROCEDURE [translate].[insertCriteriaGroups]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET IDENTITY_INSERT [dbo].[CriteriaGroups] ON 

INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (0, N'*', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (1, N'Standard Exclusions', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (2, N'TMC - AMEX', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (4, N'Voice - Call Gating - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (5, N'Channel Connect', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (6, N'Connect X', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (7, N'COR Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (16, N'Employee Rate', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (17, N'GDS', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (18, N'GDS - NEG Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (19, N'GDS - OPQ Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (32, N'IBE - Brand Golf - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (35, N'GRP Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (38, N'IBE - Brand HE - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (39, N'IBE - Brand HW - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (40, N'IBE - Hotel - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (41, N'IBE - Hotel - GRP Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (43, N'IBE - Hotel - NEG Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (45, N'HRGAP NEGPAL Rate', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (49, N'IDS', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (50, N'IDS - OPQ Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (58, N'IBE - Brand Sites - With iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (59, N'iPrefer - Direct to property', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (60, N'GDS - With iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (68, N'IBE - Hotel - With iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (72, N'Voice - Brand - With iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (74, N'IBE - Brand iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (76, N'MER Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (86, N'Metasearch', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (90, N'NEG Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (109, N'CHICC Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (185, N'OPQ Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (196, N'OTA Connectivity', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (197, N'PHG Affiliate Promotion', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (198, N'IBE - Brand PH - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (199, N'IBE - Brand PH - NEG Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (200, N'IBE - Brand Residences - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (201, N'Voice - Brand PH - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (202, N'PKG Category', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (204, N'Priceline', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (209, N'TMC - Standard', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (210, N'TMC - AMEX PEHP', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (212, N'Voice - Brand All - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (213, N'Voice - Brand HE - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (238, N'Voice - Hotel Voice Agent', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (239, N'Voice - Brand Golf - No iPrefer', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (271, N'Travelweb', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (272, N'IDS Hotwire', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (273, N'Comp Marketing Rooms', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (274, N'LONML Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (275, N'MEXMM Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (276, N'MKEHM Contracted TMC Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (277, N'MSPSP AMEX NEGTTG', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (278, N'AMEXFHRS', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (279, N'Classic Vacations', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (280, N'PAETR Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (281, N'NEGSHE Rate', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (282, N'ILGHD Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (283, N'ANKDA Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (284, N'NEGB2B Rate', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (285, N'AYTDA Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (286, N'ISTDI Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (287, N'ISTDH Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (288, N'ANCCC GOV Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (289, N'ZRHZS Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (290, N'LUGTP Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (291, N'Group Rooming List Import', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (292, N'MRYPB Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (293, N'HOTWD', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (294, N'TPAIR Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (295, N'Orbitz', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (296, N'LITCH Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (297, N'MADMM Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (298, N'BCNMB Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (299, N'SVQGM Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (300, N'PARLE Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (301, N'CHIAE Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (302, N'PEKHK Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (303, N'ISTDA Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (304, N'SHAPH Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (305, N'PARMP Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (306, N'SANWS Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (307, N'AMSGH Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (308, N'AMSGH Non-billable Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (309, N'BOSSH Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (310, N'STLTC Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (311, N'SJCHV Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (312, N'SANRB Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (313, N'CUNGM Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (314, N'LASTT Contracted Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (315, N'PREFE301012 Rate', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (316, N'PROZOO Rates', NULL)
;
INSERT [dbo].[CriteriaGroups] ([criteriaGroupID], [criteriaGroupName], [lastBilled]) VALUES (317, N'HOUTW Contracted Rates', NULL)
;
SET IDENTITY_INSERT [dbo].[CriteriaGroups] OFF;

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteriaGroupsExclusions]'
GO



ALTER PROCEDURE [translate].[insertCriteriaGroupsExclusions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET IDENTITY_INSERT [dbo].[CriteriaGroups_ExcludeCriteria] ON 

;
INSERT [dbo].[CriteriaGroups_ExcludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (26, 58, 60, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_ExcludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (27, 59, 60, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_ExcludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (28, 60, 60, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_ExcludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (29, 68, 60, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_ExcludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (30, 72, 60, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[CriteriaGroups_ExcludeCriteria] OFF
;

END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertCriteriaGroupsInclusions]'
GO


ALTER PROCEDURE [translate].[insertCriteriaGroupsInclusions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET IDENTITY_INSERT [dbo].[CriteriaGroups_IncludeCriteria] ON 

;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (1, 0, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (3, 5, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (4, 273, 5, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (5, 16, 16, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (6, 17, 17, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (7, 213, 37, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (8, 38, 38, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (9, 39, 39, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (10, 49, 49, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (11, 74, 68, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (12, 197, 106, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (13, 32, 224, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (14, 198, 225, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (15, 201, 228, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (16, 2, 236, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (17, 210, 241, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (18, 271, 242, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (19, 238, 254, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (20, 209, 237, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (21, 209, 238, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (22, 209, 239, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (23, 209, 240, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (24, 1, 5, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (25, 1, 16, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (26, 86, 96, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (27, 86, 97, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (28, 86, 98, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (29, 86, 99, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (30, 86, 100, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (31, 86, 101, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (32, 86, 102, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (33, 86, 103, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (34, 86, 104, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (35, 86, 105, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (36, 86, 107, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (37, 86, 108, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (38, 86, 109, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (39, 86, 110, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (40, 86, 111, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (41, 1, 242, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (42, 1, 230, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (357, 4, 245, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (358, 4, 280, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (359, 4, 3, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (360, 6, 6, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (361, 7, 7, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (362, 18, 18, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (363, 19, 19, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (364, 35, 34, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (365, 40, 40, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (366, 41, 41, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (367, 43, 43, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (368, 45, 45, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (369, 50, 50, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (370, 58, 69, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (371, 58, 70, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (372, 58, 71, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (373, 58, 72, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (374, 59, 1, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (375, 59, 79, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (376, 60, 61, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (377, 68, 73, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (378, 72, 81, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (379, 72, 82, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (380, 72, 83, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (381, 74, 68, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (382, 76, 86, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (383, 196, 223, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (384, 199, 226, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (385, 272, 274, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (386, 239, 255, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (387, 200, 227, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (388, 202, 229, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (389, 204, 232, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (390, 212, 228, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (391, 212, 37, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (392, 212, 255, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (393, 90, 115, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (394, 185, 212, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (395, 109, 135, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (396, 274, 281, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (397, 275, 282, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (398, 276, 283, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (399, 277, 284, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (400, 278, 288, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (401, 279, 289, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (402, 280, 290, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (403, 281, 291, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (404, 282, 292, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (405, 283, 293, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (406, 284, 294, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (407, 285, 295, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (408, 286, 296, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (409, 287, 297, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (410, 288, 310, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (411, 289, 311, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (412, 290, 312, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (413, 291, 313, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (414, 292, 314, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (415, 293, 315, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (416, 294, 316, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (417, 295, 317, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (418, 296, 318, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (419, 297, 319, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (420, 298, 320, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (421, 299, 321, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (422, 300, 322, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (423, 301, 323, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (424, 302, 324, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (425, 303, 325, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (426, 304, 326, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (427, 305, 327, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (428, 306, 328, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (429, 307, 329, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (430, 308, 330, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (431, 309, 331, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (432, 310, 332, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (433, 311, 333, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (434, 312, 334, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (435, 313, 335, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (436, 314, 336, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (437, 315, 337, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (438, 316, 338, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[CriteriaGroups_IncludeCriteria] ([id], [criteriaGroupID], [criteriaID], [startDate], [endDate], [lastBilled]) VALUES (439, 317, 339, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[CriteriaGroups_IncludeCriteria] OFF
;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertSourceGroups]'
GO


ALTER PROCEDURE [translate].[insertSourceGroups]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET IDENTITY_INSERT [dbo].[SourceGroups] ON 

;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (0, N'*', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (1, N'iPrefer Manual Entry', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (2, N'Channel Connect', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (3, N'Connect X', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (4, N'GDS', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (6, N'IBE - Brand HHA', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (7, N'IBE - Brand HHW', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (8, N'IBE - Hotel', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (9, N'IBE - Brand Golf', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (10, N'IBE - Hotel Mobile', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (11, N'IDS', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (12, N'IBE - Brand iPrefer', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (14, N'OTA Connect', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (15, N'IBE - Brand PH', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (16, N'IBE - Brand Residences', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (17, N'PMS', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (18, N'Priceline', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (19, N'Travelweb', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (20, N'Voice - Brand HE', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (21, N'Voice - Brand HE/iPrefer', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (22, N'Voice - Brand PH', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (23, N'Voice - Call Gated PH', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (24, N'Voice - Call Gated HE', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (39, N'Voice - Hotel Voice Agent', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (40, N'Voice - Golf', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (44, N'IDS Hotwire', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (45, N'Voice - Call Gated AKA', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (51, N'Classic Vacations', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (53, N'Group Rooming List Import', NULL)
;
INSERT [dbo].[SourceGroups] ([sourceGroupID], [sourceGroupName], [lastBilled]) VALUES (54, N'Orbitz', NULL)
;
SET IDENTITY_INSERT [dbo].[SourceGroups] OFF
;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertSourceGroupsExclusions]'
GO


ALTER PROCEDURE [translate].[insertSourceGroupsExclusions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET IDENTITY_INSERT [dbo].[SourceGroups_ExcludeSources] ON 
;

INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (3, 20, 34, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (4, 22, 37, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (24, 20, 32, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (25, 22, 32, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (45, 22, 34, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (62, 20, 37, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (64, 6, 12, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (65, 6, 13, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (66, 7, 12, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (67, 7, 13, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (68, 12, 20, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_ExcludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (69, 12, 21, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[SourceGroups_ExcludeSources] OFF
;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertSourceGroupsInclusions]'
GO


ALTER PROCEDURE [translate].[insertSourceGroupsInclusions]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET IDENTITY_INSERT [dbo].[SourceGroups_IncludeSources] ON 
;

INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (1, 0, 0, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (2, 2, 2, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (3, 3, 39, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (4, 4, 3, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (6, 6, 8, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (7, 7, 16, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (8, 8, 14, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (9, 9, 5, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (10, 10, 15, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (11, 11, 28, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (12, 12, 22, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (13, 14, 2, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (14, 15, 23, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (15, 16, 25, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (16, 17, 30, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (17, 18, 38, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (18, 19, 31, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (19, 20, 10, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (20, 21, 20, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (21, 22, 36, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (23, 24, 34, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (38, 39, 35, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (39, 40, 40, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (43, 44, 41, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (45, 6, 9, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (46, 7, 17, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (47, 8, 15, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (48, 9, 6, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (49, 12, 18, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (50, 14, 28, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (51, 15, 24, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (52, 16, 26, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (53, 20, 11, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (54, 21, 21, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (73, 8, 4, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (74, 12, 19, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (75, 20, 12, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (76, 23, 37, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (94, 8, 7, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (95, 20, 13, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (113, 8, 27, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (114, 20, 33, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (115, 45, 32, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (116, 51, 42, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (117, 53, 43, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
INSERT [dbo].[SourceGroups_IncludeSources] ([id], [sourceGroupID], [sourceID], [startDate], [endDate], [lastBilled]) VALUES (118, 54, 29, CAST(N'1900-01-01' AS Date), CAST(N'9999-09-09' AS Date), NULL)
;
SET IDENTITY_INSERT [dbo].[SourceGroups_IncludeSources] OFF
;
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertSources]'
GO


ALTER PROCEDURE [translate].[insertSources]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET IDENTITY_INSERT [dbo].[Sources] ON 
;

INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (0, N'Any', N'*', N'*', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (2, N'Channel Connect', N'Channel Connect', N'*', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (3, N'GDS', N'GDS', N'*', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (4, N'Google', N'Google', N'*', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (5, N'IBE - Golf Sites', N'Booking Engine', N'*', N'*', 4, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (6, N'IBE - Golf Sites Mobile', N'Mobile Web', N'*', N'*', 4, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (7, N'IBE - Google', N'Booking Engine', N'Google Direct Book', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (8, N'IBE - HHA Sites', N'Booking Engine', N'*', N'*', 5, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (9, N'IBE - HHA Sites Mobile', N'Mobile Web', N'*', N'*', 5, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (10, N'IBE - HE Voice', N'Booking Engine', N'*', N'*', 7, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (11, N'IBE - HE Voice Mobile', N'Mobile Web', N'*', N'*', 7, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (12, N'IBE - HE Voice Stopgap', N'Booking Engine', N'*', N'18009515', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (13, N'IBE - HE Voice Stopgap Slash', N'Booking Engine', N'*', N'/18009515', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (14, N'IBE - Hotel Site', N'Booking Engine', N'*', N'*', 2, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (15, N'IBE - Hotel Site Mobile', N'Mobile Web', N'*', N'*', 2, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (16, N'IBE - HHW Sites', N'Booking Engine', N'*', N'*', 6, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (17, N'IBE - HHW Sites Mobile', N'Mobile Web', N'*', N'*', 6, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (18, N'IBE - iPrefer Sites', N'Booking Engine', N'*', N'*', 3, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (19, N'IBE - iPrefer Sites Mobile', N'Mobile Web', N'*', N'*', 3, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (20, N'IBE - iPrefer Voice', N'Booking Engine', N'*', N'*', 8, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (21, N'IBE - iPrefer Voice Mobile', N'Mobile Web', N'*', N'*', 8, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (22, N'IBE - iPrefer App', N'Booking Engine', N'iPrefer App', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (23, N'IBE - PH Sites', N'Booking Engine', N'*', N'*', 1, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (24, N'IBE - PH Sites Mobile', N'Mobile Web', N'*', N'*', 1, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (25, N'IBE - Residences', N'Booking Engine', N'*', N'*', 9, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (26, N'IBE - Residences Mobile', N'Mobile Web', N'*', N'*', 9, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (27, N'IBE - Tripadvisor', N'Booking Engine', N'TripAdvisor', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (28, N'IDS', N'IDS', N'*', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (29, N'Orbitz', N'*', N'Orbitz.com', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (30, N'PMS', N'PMS Rez Synch', N'*', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (31, N'Travelweb', N'*', N'Travelweb', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (32, N'Voice - AKA Call Gated', N'Voice', N'*', N'VAKA', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (33, N'Voice - HE', N'Voice', N'*', N'*', 0, 3, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (34, N'Voice - HE Call Gated', N'Voice', N'*', N'VCG', 0, 3, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (35, N'Voice - Hotel', N'Voice', N'*', N'*', 0, 2, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (36, N'Voice - PH', N'Voice', N'*', N'*', 0, 1, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (37, N'Voice - PH Call Gated', N'Voice', N'*', N'VCG', 0, 1, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (38, N'Priceline', N'GDS', N'Worldspan', N'PL', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (39, N'CCX', N'Channel Connect', N'*', N'CCX', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (40, N'Voice - Golf', N'Voice', N'*', N'PHGGOLF', 0, 1, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (41, N'IDS Hotwire', N'IDS', N'*', N'S1', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (42, N'Classic Vacations', N'Channel Connect', N'Classic Vacations.com', N'*', 0, 0, NULL)
;
INSERT [dbo].[Sources] ([sourceId], [sourceName], [channel], [secondarySource], [subsource], [templateGroupID], [croGroupID], [lastBilled]) VALUES (43, N'Group Rooming List Import', N'*', N'Group Rooming List Import', N'*', 0, 0, NULL)
;
SET IDENTITY_INSERT [dbo].[Sources] OFF
;

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertThresholdRules]'
GO


ALTER PROCEDURE [translate].[insertThresholdRules]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--first insert the per-hotel mapped rules
INSERT INTO [BillingRewrite].[dbo].[ThresholdRules] (clauseID, thresholdTypeID, global, resetDate, periodYears, periodMonths, periodWeeks, periodDays)

SELECT 
	tc.newClauseID
	, c.thresholdTypeID
	, c.compareGlobalTotals
	, MIN(c.resetPeriodStartDate)
	, c.resetPeriodYears
	, c.resetPeriodMonths
	, c.resetPeriodWeeks
	, c.resetPeriodDays

  FROM [BillingRewrite].[translate].[Clauses] tc
  INNER JOIN BillingRewrite.translate.CriteriaGroupsPerHotel tcg
	ON tc.newCriteriaGroupID = tcg.newCriteriaGroupID
	AND tc.hotelCode = tcg.hotelCode
  INNER JOIN BillingProduction.dbo.charges c
	ON tcg.oldCriteriaID = c.criteriaID
	AND tcg.hotelCode = c.hotelCode
	AND tc.thresholdTypeID = c.thresholdTypeID
	AND tc.global = c.compareGlobalTotals
	AND tc.periodYears = c.resetPeriodYears
	AND tc.periodMonths = c.resetPeriodMonths
	AND tc.periodWeeks = c.resetPeriodWeeks
	AND tc.periodDays = c.resetPeriodDays
WHERE c.thresholdTypeID <> 0
AND (resetPeriodYears + resetPeriodMonths + resetPeriodWeeks + resetPeriodDays) <> 0
GROUP BY 	tc.newClauseID
	, c.thresholdTypeID
	, c.compareGlobalTotals
	, c.resetPeriodYears
	, c.resetPeriodMonths
	, c.resetPeriodWeeks
	, c.resetPeriodDays;

--then insert the generally mapped rules
INSERT INTO [BillingRewrite].[dbo].[ThresholdRules] (clauseID, thresholdTypeID, global, resetDate, periodYears, periodMonths, periodWeeks, periodDays)

SELECT 
	tc.newClauseID
	, c.thresholdTypeID
	, c.compareGlobalTotals
	, MIN(c.resetPeriodStartDate)
	, c.resetPeriodYears
	, c.resetPeriodMonths
	, c.resetPeriodWeeks
	, c.resetPeriodDays

  FROM [BillingRewrite].[translate].[Clauses] tc
  INNER JOIN BillingRewrite.translate.CriteriaGroups tcg
	ON tc.newCriteriaGroupID = tcg.newCriteriaGroupID
  INNER JOIN BillingProduction.dbo.charges c
	ON tcg.oldCriteriaID = c.criteriaID
	AND tc.hotelCode = c.hotelCode
	AND tc.thresholdTypeID = c.thresholdTypeID
	AND tc.global = c.compareGlobalTotals
	AND tc.periodYears = c.resetPeriodYears
	AND tc.periodMonths = c.resetPeriodMonths
	AND tc.periodWeeks = c.resetPeriodWeeks
	AND tc.periodDays = c.resetPeriodDays
LEFT OUTER JOIN BillingRewrite.dbo.ThresholdRules tr
	ON tc.newClauseID = tr.clauseID
WHERE c.thresholdTypeID <> 0
AND (resetPeriodYears + resetPeriodMonths + resetPeriodWeeks + resetPeriodDays) <> 0
AND tr.clauseID IS NULL --there is no existing threshold rule from the first insert statement
GROUP BY 	tc.newClauseID
	, c.thresholdTypeID
	, c.compareGlobalTotals
	, c.resetPeriodYears
	, c.resetPeriodMonths
	, c.resetPeriodWeeks
	, c.resetPeriodDays




END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertTranslateClauses]'
GO


ALTER PROCEDURE [translate].[insertTranslateClauses]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


WITH newClauses AS (
--the generally mapped criteria
SELECT DISTINCT 
  c.hotelCode,
	cg.criteriaGroupID,
	cg.criteriaGroupName,
	c.thresholdTypeID,
	c.compareGlobalTotals,
	c.resetPeriodYears,
	c.resetPeriodMonths,
	c.resetPeriodWeeks,
	c.resetPeriodDays
  FROM [BillingRewrite].[translate].[CriteriaGroups] tcg
  INNER JOIN BillingRewrite.dbo.CriteriaGroups cg
	ON tcg.newCriteriaGroupID = cg.criteriaGroupID
  INNER JOIN BillingProduction.dbo.Charges c
	ON tcg.oldCriteriaID = c.criteriaID

UNION

--the per-hotel mapped criteria
SELECT DISTINCT 
  c.hotelCode,
	cg.criteriaGroupID,
	cg.criteriaGroupName,
	c.thresholdTypeID,
	c.compareGlobalTotals,
	c.resetPeriodYears,
	c.resetPeriodMonths,
	c.resetPeriodWeeks,
	c.resetPeriodDays
  FROM [BillingRewrite].[translate].[CriteriaGroupsPerHotel] tcg
  INNER JOIN BillingRewrite.dbo.CriteriaGroups cg
	ON tcg.newCriteriaGroupID = cg.criteriaGroupID
  INNER JOIN BillingProduction.dbo.Charges c
	ON tcg.oldCriteriaID = c.criteriaID
	AND tcg.hotelCode = c.hotelCode
)
INSERT INTO [BillingRewrite].[translate].[Clauses] (hotelCode
	, newClauseID
	, newCriteriaGroupID
	, thresholdTypeID
	, global
	, periodYears
	, periodMonths
	, periodWeeks
	, periodDays
      )
SELECT DISTINCT
	hotelCode,
	ROW_NUMBER() OVER(ORDER BY hotelCode, criteriaGroupName) AS clauseID,
	criteriaGroupID,
	thresholdTypeID,
	compareGlobalTotals,
	resetPeriodYears,
	resetPeriodMonths,
	resetPeriodWeeks,
	resetPeriodDays
  FROM newClauses
  ORDER BY clauseID

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[insertTranslateCriteriaGroups]'
GO


ALTER PROCEDURE [translate].[insertTranslateCriteriaGroups]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

INSERT [translate].[CriteriaGroups] ([newCriteriaGroupID], [oldCriteriaID]) 
VALUES (0,89),
(2,320),
(2,25),
(2,64),
(2,66),
(2,67),
(2,68),
(4,357),
(4,116),
(4,362),
(5,65),
(6,364),
(7,128),
(8,394),
(16,85),
(17,10),
(18,348),
(32,343),
(32,342),
(35,114),
(38,373),
(38,344),
(38,455),
(39,360),
(39,467),
(40,63),
(40,250),
(40,345),
(40,365),
(40,391),
(40,57),
(40,310),
(40,115),
(40,118),
(40,231),
(40,430),
(40,392),
(40,407),
(40,363),
(40,422),
(40,454),
(41,306),
(41,429),
(41,431),
(43,453),
(43,452),
(49,15),
(50,419),
(74,249),
(74,389),
(74,428),
(74,423),
(76,17),
(76,217),
(76,222),
(76,223),
(76,224),
(76,355),
(76,90),
(76,402),
(76,18),
(76,401),
(76,91),
(76,92),
(76,93),
(76,94),
(86,442),
(86,443),
(86,444),
(86,434),
(86,435),
(86,439),
(86,433),
(86,446),
(86,447),
(86,441),
(86,448),
(86,438),
(86,449),
(86,451),
(86,436),
(86,437),
(86,440),
(86,450),
(86,129),
(86,113),
(86,166),
(86,358),
(86,140),
(86,241),
(86,111),
(86,304),
(86,303),
(86,347),
(90,2),
(109,235),
(109,234),
(185,417),
(185,340),
(185,88),
(185,400),
(185,106),
(185,108),
(185,105),
(185,352),
(185,353),
(185,354),
(185,104),
(185,107),
(185,152),
(197,167),
(198,346),
(198,58),
(198,309),
(198,421),
(200,424),
(200,371),
(200,425),
(201,13),
(201,305),
(202,81),
(204,338),
(209,321),
(209,22),
(209,71),
(209,74),
(209,78),
(209,322),
(209,24),
(209,75),
(209,76),
(209,77),
(209,432),
(209,323),
(209,23),
(209,70),
(209,72),
(209,73),
(210,227),
(210,230),
(210,228),
(210,229),
(213,361),
(213,367),
(213,393),
(213,366),
(213,405),
(226,463),
(226,464),
(226,462),
(232,119),
(238,169),
(239,372),
(271,341),
(272,215),
(273,84),
(273,339),
(273,300),
(274,251),
(274,256),
(274,260),
(274,265),
(274,276),
(274,278),
(274,281),
(274,291),
(274,264),
(274,255),
(274,252),
(274,253),
(274,254),
(274,257),
(274,259),
(274,261),
(274,258),
(274,262),
(274,266),
(274,263),
(274,267),
(274,268),
(274,270),
(274,269),
(274,272),
(274,271),
(274,273),
(274,275),
(274,274),
(274,277),
(274,279),
(274,283),
(274,282),
(274,284),
(274,285),
(274,287),
(274,286),
(274,288),
(274,290),
(274,292),
(274,280),
(274,293),
(274,314),
(274,294),
(274,289),
(276,317),
(276,318),
(276,171),
(276,189),
(276,172),
(276,190),
(276,173),
(276,191),
(276,174),
(276,192),
(276,175),
(276,193),
(276,176),
(276,194),
(276,177),
(276,195),
(276,178),
(276,196),
(276,179),
(276,197),
(276,180),
(276,198),
(276,181),
(276,199),
(276,182),
(276,200),
(276,183),
(276,201),
(276,184),
(276,202),
(276,185),
(276,203),
(276,186),
(276,204),
(276,187),
(276,205),
(276,188),
(276,206),
(276,324),
(276,325),
(276,326),
(276,327),
(276,328),
(276,329),
(277,319),
(277,132),
(277,134),
(277,133),
(277,135),
(277,136),
(277,137),
(278,69),
(279,403),
(280,248),
(280,247),
(281,79),
(282,226),
(282,415),
(282,225),
(282,216),
(282,413),
(282,80),
(282,414),
(282,416),
(283,148),
(286,160),
(286,154),
(287,157),
(287,159),
(287,156),
(288,420),
(288,142),
(288,170),
(288,141),
(289,377),
(289,378),
(289,375),
(289,374),
(289,376),
(289,382),
(289,383),
(289,380),
(289,379),
(289,381),
(290,408),
(290,409),
(290,410),
(290,411),
(291,412),
(292,316),
(292,315),
(293,356),
(294,296),
(294,297),
(294,298),
(294,299),
(295,337),
(295,214),
(296,125),
(296,126),
(297,211),
(298,212),
(298,213),
(298,209),
(298,161),
(299,210),
(300,465),
(300,295),
(300,244),
(300,466),
(301,87),
(301,86),
(302,368),
(302,312),
(302,313),
(303,145),
(303,144),
(303,146),
(303,143),
(305,398),
(305,397),
(305,395),
(305,396),
(305,399),
(306,101),
(306,98),
(306,97),
(306,96),
(306,95),
(306,102),
(306,100),
(306,99),
(307,83),
(308,336),
(309,103),
(310,109),
(310,122),
(311,117),
(312,163),
(312,164),
(312,162),
(313,165),
(314,219),
(314,220),
(314,218),
(314,221),
(315,232),
(316,239),
(316,238),
(317,406),
(318,308),
(318,307),
(319,120),
(320,110),
(320,331),
(320,460),
(320,332),
(320,242),
(320,243),
(320,333),
(320,461),
(320,335),
(321,236),
(321,237),
(322,246),
(323,131)




END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[RUN_TRANSLATION]'
GO


ALTER PROCEDURE [translate].[RUN_TRANSLATION]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

EXECUTE [BillingRewrite].[translate].[clearExistingData];
EXECUTE [BillingRewrite].[translate].[insertCROcodes];
EXECUTE [BillingRewrite].[translate].[insertTemplates];
EXECUTE [BillingRewrite].[translate].[insertSources];
EXECUTE [BillingRewrite].[translate].[insertSourceGroups];
EXECUTE [BillingRewrite].[translate].[insertSourceGroupsExclusions];
EXECUTE [BillingRewrite].[translate].[insertSourceGroupsInclusions];
EXECUTE [BillingRewrite].[translate].[insertCriteria];
EXECUTE [BillingRewrite].[translate].[insertCriteria_RateCategories];
EXECUTE [BillingRewrite].[translate].[insertCriteria_RateCodes];
EXECUTE [BillingRewrite].[translate].[insertCriteria_SourceGroups];
EXECUTE [BillingRewrite].[translate].[insertCriteria_TravelAgentGroups];
EXECUTE [BillingRewrite].[translate].[insertCriteriaGroups];
EXECUTE [BillingRewrite].[translate].[insertCriteriaGroupsExclusions];
EXECUTE [BillingRewrite].[translate].[insertCriteriaGroupsInclusions];
EXECUTE [BillingRewrite].[translate].[insertTranslateCriteriaGroupsPerHotel];
EXECUTE [BillingRewrite].[translate].[insertTranslateCriteriaGroups];
EXECUTE [BillingRewrite].[translate].[insertTranslateClauses];
EXECUTE [BillingRewrite].[translate].[insertClauses];
EXECUTE [BillingRewrite].[translate].[insertClausesExclusions];
EXECUTE [BillingRewrite].[translate].[insertClausesInclusions];
EXECUTE [BillingRewrite].[translate].[insertThresholdRules];
EXECUTE [BillingRewrite].[translate].[insertBillingRules];
EXECUTE [BillingRewrite].[translate].[postTranslationIssues];


END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[collision_Timeframe]'
GO

ALTER PROCEDURE dbo.collision_Timeframe
	@billingRuleID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	SELECT br2.billingRuleID,br2.startDate,br2.endDate,br2.thresholdMinimum,c.classificationName,
		CASE 
			WHEN br1.startDate < br2.startDate THEN
				CASE
					WHEN br1.endDate < br2.startDate THEN 'BEFORE' 
					WHEN br2.endDate <= br1.endDate THEN 'EXCEEDING' 
					ELSE 'UNDERLAPPING'
				END
			WHEN br1.startDate = br2.startDate THEN
				CASE
					WHEN br1.endDate < br2.endDate THEN 'UNDERLAPPING' 
					WHEN br2.endDate < br1.endDate THEN 'EXCEEDING' 
					ELSE 'EXACTLY'
				END
			WHEN br2.startDate < br1.startDate THEN
				CASE
					WHEN br2.endDate < br1.startDate THEN 'AFTER' 
					WHEN br2.endDate <= br1.endDate THEN 'OVERLAPPING' 
					ELSE 'INSIDE'
				END
			WHEN br2.endDate = br1.startDate THEN 'OVERLAPPING' 
			ELSE 'This should not ever happen'
		END AS timeframeMatchType
	FROM BillingRules br1
		INNER JOIN BillingRules br2 ON br2.clauseID = br1.clauseID
		INNER JOIN Classifications c ON c.classificationID = br2.classificationID
	WHERE br1.billingRuleID = @billingRuleID
		AND br2.billingRuleID != @billingRuleID
		AND br1.classificationID = br2.classificationID
		AND br1.thresholdMinimum = br2.thresholdMinimum
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CROCodes]'
GO
ALTER TABLE [dbo].[CROCodes] ADD CONSTRAINT [FK_CROCodes_CROGroups] FOREIGN KEY ([croGroupID]) REFERENCES [dbo].[CROGroups] ([croGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Sources]'
GO
ALTER TABLE [dbo].[Sources] ADD CONSTRAINT [FK_Sources_CROGroups] FOREIGN KEY ([croGroupID]) REFERENCES [dbo].[CROGroups] ([croGroupID])
ALTER TABLE [dbo].[Sources] ADD CONSTRAINT [FK_Sources_TemplateGroups] FOREIGN KEY ([templateGroupID]) REFERENCES [dbo].[TemplateGroups] ([templateGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Templates]'
GO
ALTER TABLE [dbo].[Templates] ADD CONSTRAINT [FK_Templates_TemplateGroups] FOREIGN KEY ([templateGroupID]) REFERENCES [dbo].[TemplateGroups] ([templateGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThresholdRules]'
GO
ALTER TABLE [dbo].[ThresholdRules] ADD CONSTRAINT [FK_ThresholdRules_ThresholdTypes] FOREIGN KEY ([thresholdTypeID]) REFERENCES [dbo].[ThresholdTypes] ([thresholdTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
