/*
Run this script on:

        chi-sq-st-01\warehouse.BillingRewrite    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.BillingRewrite

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 12/19/2017 6:53:26 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[BillingRules]'
GO
ALTER TABLE [dbo].[BillingRules] DROP CONSTRAINT [FK_BillingRules_Classifications1]
ALTER TABLE [dbo].[BillingRules] DROP CONSTRAINT [FK_BillingRules_Clauses]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Criteria_SourceGroups]'
GO
ALTER TABLE [dbo].[Criteria_SourceGroups] DROP CONSTRAINT [FK_Criteria_SourceGroups_Criteria]
ALTER TABLE [dbo].[Criteria_SourceGroups] DROP CONSTRAINT [FK_Criteria_SourceGroups_SourceGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Criteria_RateCategories]'
GO
ALTER TABLE [dbo].[Criteria_RateCategories] DROP CONSTRAINT [FK_Criteria_RateCategories_Criteria]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Clauses_IncludeCriteriaGroups]'
GO
ALTER TABLE [dbo].[Clauses_IncludeCriteriaGroups] DROP CONSTRAINT [FK_Clauses_IncludeCriteriaGroups_Clauses]
ALTER TABLE [dbo].[Clauses_IncludeCriteriaGroups] DROP CONSTRAINT [FK_Clauses_IncludeCriteriaGroups_CriteriaGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Criteria]'
GO
ALTER TABLE [dbo].[Criteria] DROP CONSTRAINT [FK_Criteria_LoyaltyOptions]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Criteria_RateCodes]'
GO
ALTER TABLE [dbo].[Criteria_RateCodes] DROP CONSTRAINT [FK_Criteria_RateCodes_Criteria]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Criteria_TravelAgentGroups]'
GO
ALTER TABLE [dbo].[Criteria_TravelAgentGroups] DROP CONSTRAINT [FK_Criteria_TravelAgentGroups_Criteria]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[SourceGroups_ExcludeSources]'
GO
ALTER TABLE [dbo].[SourceGroups_ExcludeSources] DROP CONSTRAINT [FK_SourceGroups_ExcludeSources_SourceGroups]
ALTER TABLE [dbo].[SourceGroups_ExcludeSources] DROP CONSTRAINT [FK_SourceGroups_ExcludeSources_Sources]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[SourceGroups_IncludeSources]'
GO
ALTER TABLE [dbo].[SourceGroups_IncludeSources] DROP CONSTRAINT [FK_SourceGroups_IncludeSources_SourceGroups]
ALTER TABLE [dbo].[SourceGroups_IncludeSources] DROP CONSTRAINT [FK_SourceGroups_IncludeSources_Sources]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Sources]'
GO
ALTER TABLE [dbo].[Sources] DROP CONSTRAINT [FK_Sources_CROGroups]
ALTER TABLE [dbo].[Sources] DROP CONSTRAINT [FK_Sources_TemplateGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Templates]'
GO
ALTER TABLE [dbo].[Templates] DROP CONSTRAINT [FK_Templates_TemplateGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[ThresholdRules]'
GO
ALTER TABLE [dbo].[ThresholdRules] DROP CONSTRAINT [FK_ThresholdRules_Clauses]
ALTER TABLE [dbo].[ThresholdRules] DROP CONSTRAINT [FK_ThresholdRules_ThresholdTypes]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[CriteriaGroups_ExcludeCriteria]'
GO
ALTER TABLE [dbo].[CriteriaGroups_ExcludeCriteria] DROP CONSTRAINT [FK_CriteriaGroups_ExcludeCriteria_CriteriaGroups]
ALTER TABLE [dbo].[CriteriaGroups_ExcludeCriteria] DROP CONSTRAINT [FK_CriteriaGroups_ExcludeCriteria_Criteria]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Clauses_ExcludeCriteriaGroups]'
GO
ALTER TABLE [dbo].[Clauses_ExcludeCriteriaGroups] DROP CONSTRAINT [FK_Clauses_ExcludeCriteriaGroups_Clauses]
ALTER TABLE [dbo].[Clauses_ExcludeCriteriaGroups] DROP CONSTRAINT [FK_Clauses_ExcludeCriteriaGroups_CriteriaGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[CriteriaGroups_IncludeCriteria]'
GO
ALTER TABLE [dbo].[CriteriaGroups_IncludeCriteria] DROP CONSTRAINT [FK_CriteriaGroups_IncludeCriteria_CriteriaGroups]
ALTER TABLE [dbo].[CriteriaGroups_IncludeCriteria] DROP CONSTRAINT [FK_CriteriaGroups_IncludeCriteria_Criteria]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[CROCodes]'
GO
ALTER TABLE [dbo].[CROCodes] DROP CONSTRAINT [FK_CROCodes_CROGroups]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [work].[MrtRulesApplied]'
GO
DROP TABLE [work].[MrtRulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BillingRules]'
GO
ALTER TABLE [dbo].[BillingRules] ADD CONSTRAINT [FK_BillingRules_Classifications1] FOREIGN KEY ([classificationID]) REFERENCES [dbo].[Classifications] ([classificationID])
ALTER TABLE [dbo].[BillingRules] ADD CONSTRAINT [FK_BillingRules_Clauses] FOREIGN KEY ([clauseID]) REFERENCES [dbo].[Clauses] ([clauseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Criteria_SourceGroups]'
GO
ALTER TABLE [dbo].[Criteria_SourceGroups] ADD CONSTRAINT [FK_Criteria_SourceGroups_Criteria] FOREIGN KEY ([criteriaID]) REFERENCES [dbo].[Criteria] ([criteriaID])
ALTER TABLE [dbo].[Criteria_SourceGroups] ADD CONSTRAINT [FK_Criteria_SourceGroups_SourceGroups] FOREIGN KEY ([sourceGroupID]) REFERENCES [dbo].[SourceGroups] ([sourceGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Criteria_RateCategories]'
GO
ALTER TABLE [dbo].[Criteria_RateCategories] ADD CONSTRAINT [FK_Criteria_RateCategories_Criteria] FOREIGN KEY ([criteriaID]) REFERENCES [dbo].[Criteria] ([criteriaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Clauses_IncludeCriteriaGroups]'
GO
ALTER TABLE [dbo].[Clauses_IncludeCriteriaGroups] ADD CONSTRAINT [FK_Clauses_IncludeCriteriaGroups_Clauses] FOREIGN KEY ([clauseID]) REFERENCES [dbo].[Clauses] ([clauseID])
ALTER TABLE [dbo].[Clauses_IncludeCriteriaGroups] ADD CONSTRAINT [FK_Clauses_IncludeCriteriaGroups_CriteriaGroups] FOREIGN KEY ([criteriaGroupID]) REFERENCES [dbo].[CriteriaGroups] ([criteriaGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Criteria]'
GO
ALTER TABLE [dbo].[Criteria] ADD CONSTRAINT [FK_Criteria_LoyaltyOptions] FOREIGN KEY ([loyaltyOptionID]) REFERENCES [dbo].[LoyaltyOptions] ([loyaltyOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Criteria_RateCodes]'
GO
ALTER TABLE [dbo].[Criteria_RateCodes] ADD CONSTRAINT [FK_Criteria_RateCodes_Criteria] FOREIGN KEY ([criteriaID]) REFERENCES [dbo].[Criteria] ([criteriaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Criteria_TravelAgentGroups]'
GO
ALTER TABLE [dbo].[Criteria_TravelAgentGroups] ADD CONSTRAINT [FK_Criteria_TravelAgentGroups_Criteria] FOREIGN KEY ([criteriaID]) REFERENCES [dbo].[Criteria] ([criteriaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SourceGroups_ExcludeSources]'
GO
ALTER TABLE [dbo].[SourceGroups_ExcludeSources] ADD CONSTRAINT [FK_SourceGroups_ExcludeSources_SourceGroups] FOREIGN KEY ([sourceGroupID]) REFERENCES [dbo].[SourceGroups] ([sourceGroupID])
ALTER TABLE [dbo].[SourceGroups_ExcludeSources] ADD CONSTRAINT [FK_SourceGroups_ExcludeSources_Sources] FOREIGN KEY ([sourceID]) REFERENCES [dbo].[Sources] ([sourceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SourceGroups_IncludeSources]'
GO
ALTER TABLE [dbo].[SourceGroups_IncludeSources] ADD CONSTRAINT [FK_SourceGroups_IncludeSources_SourceGroups] FOREIGN KEY ([sourceGroupID]) REFERENCES [dbo].[SourceGroups] ([sourceGroupID])
ALTER TABLE [dbo].[SourceGroups_IncludeSources] ADD CONSTRAINT [FK_SourceGroups_IncludeSources_Sources] FOREIGN KEY ([sourceID]) REFERENCES [dbo].[Sources] ([sourceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Sources]'
GO
ALTER TABLE [dbo].[Sources] ADD CONSTRAINT [FK_Sources_CROGroups] FOREIGN KEY ([croGroupID]) REFERENCES [dbo].[CROGroups] ([croGroupID])
ALTER TABLE [dbo].[Sources] ADD CONSTRAINT [FK_Sources_TemplateGroups] FOREIGN KEY ([templateGroupID]) REFERENCES [dbo].[TemplateGroups] ([templateGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Templates]'
GO
ALTER TABLE [dbo].[Templates] ADD CONSTRAINT [FK_Templates_TemplateGroups] FOREIGN KEY ([templateGroupID]) REFERENCES [dbo].[TemplateGroups] ([templateGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThresholdRules]'
GO
ALTER TABLE [dbo].[ThresholdRules] ADD CONSTRAINT [FK_ThresholdRules_Clauses] FOREIGN KEY ([clauseID]) REFERENCES [dbo].[Clauses] ([clauseID])
ALTER TABLE [dbo].[ThresholdRules] ADD CONSTRAINT [FK_ThresholdRules_ThresholdTypes] FOREIGN KEY ([thresholdTypeID]) REFERENCES [dbo].[ThresholdTypes] ([thresholdTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CriteriaGroups_ExcludeCriteria]'
GO
ALTER TABLE [dbo].[CriteriaGroups_ExcludeCriteria] ADD CONSTRAINT [FK_CriteriaGroups_ExcludeCriteria_CriteriaGroups] FOREIGN KEY ([criteriaGroupID]) REFERENCES [dbo].[CriteriaGroups] ([criteriaGroupID])
ALTER TABLE [dbo].[CriteriaGroups_ExcludeCriteria] ADD CONSTRAINT [FK_CriteriaGroups_ExcludeCriteria_Criteria] FOREIGN KEY ([criteriaID]) REFERENCES [dbo].[Criteria] ([criteriaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Clauses_ExcludeCriteriaGroups]'
GO
ALTER TABLE [dbo].[Clauses_ExcludeCriteriaGroups] ADD CONSTRAINT [FK_Clauses_ExcludeCriteriaGroups_Clauses] FOREIGN KEY ([clauseID]) REFERENCES [dbo].[Clauses] ([clauseID])
ALTER TABLE [dbo].[Clauses_ExcludeCriteriaGroups] ADD CONSTRAINT [FK_Clauses_ExcludeCriteriaGroups_CriteriaGroups] FOREIGN KEY ([criteriaGroupID]) REFERENCES [dbo].[CriteriaGroups] ([criteriaGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CriteriaGroups_IncludeCriteria]'
GO
ALTER TABLE [dbo].[CriteriaGroups_IncludeCriteria] ADD CONSTRAINT [FK_CriteriaGroups_IncludeCriteria_CriteriaGroups] FOREIGN KEY ([criteriaGroupID]) REFERENCES [dbo].[CriteriaGroups] ([criteriaGroupID])
ALTER TABLE [dbo].[CriteriaGroups_IncludeCriteria] ADD CONSTRAINT [FK_CriteriaGroups_IncludeCriteria_Criteria] FOREIGN KEY ([criteriaID]) REFERENCES [dbo].[Criteria] ([criteriaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CROCodes]'
GO
ALTER TABLE [dbo].[CROCodes] ADD CONSTRAINT [FK_CROCodes_CROGroups] FOREIGN KEY ([croGroupID]) REFERENCES [dbo].[CROGroups] ([croGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
