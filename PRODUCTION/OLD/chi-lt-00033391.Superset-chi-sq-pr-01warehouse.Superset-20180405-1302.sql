/*
Run this script on:

        chi-sq-pr-01\warehouse.Superset    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 4/5/2018 1:03:07 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [Reporting].[AdaraHaloByRegion]'
GO

CREATE procedure [Reporting].[AdaraHaloByRegion]
	@startdate date,
	@enddate date,
	@region nvarchar(50)
as

SELECT mrt.hotelName
    , mrt.hotelCode
    , mrt.confirmationNumber
     , mrt.confirmationDate
    , mrt.arrivalDate
    , COUNT(mrt.confirmationNumber) AS HotelBookings  
    , SUM(mrt.roomNights) AS RoomNights
    ,SUM(mrt.reservationRevenueUSD) AS RevenueUSD
    ,COUNT(halo.Confirmation_Number) AS HaloBookings
    ,SUM(COALESCE(halo.Total_Room_Nights, 0)) AS HaloRoomNights
    ,SUM(COALESCE(halo.Total_Revenue_USD, 0)) AS HaloRevenueUSD
    ,halo.First_Brand_Site_Visited
FROM Superset.dbo.mostrecenttransactionsReporting AS mrt
	   LEFT JOIN Superset.Reporting.Adara AS halo ON halo.Confirmation_Number = mrt.confirmationNumber  
AND halo.Last_Site_Visit_to_Booking_Window <= 30
	   JOIN Core.dbo.hotelsReporting AS core on core.code = mrt.hotelCode
WHERE mrt.confirmationDate  BETWEEN @startDate AND @endDate
    AND mrt.subSourceReportLabel = 'Hotel Booking Engine'
	AND Core.geographicRegionName = @region
GROUP BY mrt.hotelName
    , mrt.hotelCode
    , halo.Confirmation_Number
    , mrt.confirmationNumber
    , mrt.confirmationDate 
    , mrt.arrivalDate
    ,halo.First_Brand_Site_Visited


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Reporting].[FindHotelRegion]'
GO

CREATE procedure [Reporting].[FindHotelRegion]

as

SELECT DISTINCT isnull(geographicRegionCode, 'Unknown') as geographicRegionCode

From Core.dbo.hotelsReporting as hr

WHERE hr.statuscodename in ('Member Hotel','Renewal Hotel')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
