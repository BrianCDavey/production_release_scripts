/*
Run this script on:

        chi-sq-st-01\warehouse.Superset    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 1/8/2018 1:47:51 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[IPreferGuestNameReport_2017]'
GO



CREATE  procedure [dbo].[IPreferGuestNameReport_2017]
	@SOPNumber varchar(20)
AS


-- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'

--DECLARE @SOPNumber varchar(20) = 'RES0115115'
-- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'



DECLARE @currencymaster TABLE (
								CURNCYID char(15),
								CURRNIDX smallint,
								NOTEINDX numeric(19, 5),
								CRNCYDSC char(31),
								CRNCYSYM char(3),
								CNYSYMAR_1 smallint,
								CNYSYMAR_2 smallint,
								CNYSYMAR_3 smallint,
								CYSYMPLC smallint,
								INCLSPAC tinyint,
								NEGSYMBL smallint,
								NGSMAMPC smallint,
								NEGSMPLC smallint,
								DECSYMBL smallint,
								DECPLCUR smallint,
								THOUSSYM smallint,
								CURTEXT_1 char(25),
								CURTEXT_2 char(25),
								CURTEXT_3 char(25),
								ISOCURRC char(3),
								CURLNGID smallint,
								DEX_ROW_TS datetime,
								DEX_ROW_ID int
							);

insert into @currencymaster
select	CURNCYID,
		CURRNIDX,
		NOTEINDX,
		CRNCYDSC,
		CRNCYSYM,
		CNYSYMAR_1,
		CNYSYMAR_2,
		CNYSYMAR_3,
		CYSYMPLC,
		INCLSPAC,
		NEGSYMBL,
		NGSMAMPC,
		NEGSMPLC,
		DECSYMBL,
		DECPLCUR,
		THOUSSYM,
		CURTEXT_1,
		CURTEXT_2,
		CURTEXT_3,
		ISOCURRC,
		CURLNGID,
		DEX_ROW_TS,
		DEX_ROW_ID
FROM DYNAMICS.dbo.MC40200



DECLARE @GP_SOPNumber_ItemCodes TABLE (	
										ITEMNMBR varchar(50),
										ItemGrouping varchar(250)
									);

INSERT INTO @GP_SOPNumber_ItemCodes(ITEMNMBR,ItemGrouping)
SELECT itemCode.ITEMNMBR,
	CASE
		WHEN itemCode.ITEMDESC <> COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC) THEN COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC)
		ELSE
			CASE COALESCE(RTRIM(itemClasses.ITMCLSDC), '') WHEN '' THEN ''
			ELSE RTRIM(itemClasses.ITMCLSDC) + ' '
		END
		+
		CASE COALESCE(RTRIM(itemCode.ITMSHNAM),'')
			WHEN '' THEN '' 
			ELSE RTRIM(itemCode.ITMSHNAM)
		END 
	END as itemGrouping	
FROM IC.dbo.IV00101 AS itemCode 
	LEFT JOIN IC.dbo.SY03900 itemNotes ON itemCode.NOTEINDX = itemNotes.NOTEINDX 
	LEFT JOIN IC.dbo.IV40600 itemClassifications ON itemClassifications.USCATNUM = 1 AND itemCode.USCATVLS_1 = itemClassifications.USCATVAL 
	LEFT JOIN IC.dbo.IV40400 itemClasses ON itemCode.ITMCLSCD = itemClasses.ITMCLSCD
	LEFT JOIN IC.dbo.SOP30300 historyLines ON historyLines.SOPNUMBE = 'abc' AND itemCode.itemCode = historyLines.ITEMNMBR
	LEFT JOIN IC.dbo.SOP10200 workLines ON  workLines.SOPNUMBE = 'ABC' AND itemCode.itemCode = workLines.ITEMNMBR

--EXEC sp_ExecuteSQL @SqlCommand;


SELECT	COALESCE(main.currency, iPrefer.currency) AS currency
,		COALESCE(main.arrivalDate, iPrefer.arrivalDate) AS arrivalDate
,		COALESCE(main.guestLastName, iPrefer.guestLastName) AS guestLastName
,		COALESCE(main.guestFirstName, iPrefer.guestFirstName) AS guestFirstName
,		COALESCE(main.confirmationNumber, iPrefer.confirmationNumber) AS confirmationNumber
,		COALESCE(main.confirmationDate, iPrefer.confirmationDate) AS confirmationDate
,		COALESCE(main.consortia, iPrefer.consortia) AS consortia
,		COALESCE(main.IATANumber, iPrefer.IATANumber) AS IATANumber
,		COALESCE(main.rateTypeCode, iPrefer.rateTypeCode) AS rateTypeCode
,		COALESCE(main.iPreferNumber, iPrefer.iPreferNumber) AS iPreferNumber
,		COALESCE(main.averageDailyRate, iPrefer.averageDailyRate) AS averageDailyRate
,		COALESCE(main.roomNights, iPrefer.roomNights) AS roomNights
,		COALESCE(main.reservationRevenue, iPrefer.reservationRevenue) AS reservationRevenue
,		COALESCE(main.iPreferCharge, iPrefer.iPreferCharge) AS iPreferCharge
,		COALESCE(main.commCharge, iPrefer.commCharge) AS commCharge
,		COALESCE(main.bookCharge, iPrefer.bookCharge) AS bookCharge
,		COALESCE(main.surcharge, iPrefer.surcharge) AS surcharge
,		ISNULL(main.totalCharge, 0) + ISNULL(iPrefer.totalCharge, 0) AS totalCharge
,		COALESCE(main.itemGrouping, iPrefer.itemGrouping) AS itemGrouping
,		COALESCE(main.criteriaGroupID, iPrefer.criteriaGroupID) AS criteriaGroupID

FROM (
Select	chargedReservations.hotelCurrencyCode as currency, 
		chargedReservations.arrivalDate, 
		UPPER(MRT.guestLastName) AS guestLastName, 
		UPPER(MRT.guestFirstName) AS guestFirstName, 
		chargedReservations.confirmationNumber, 
		MRT.confirmationDate,
		Superset.dbo.getBilledConsortia(chargedReservations.confirmationNumber) AS consortia, 
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		MRT.rateTypeCode,
		NULL AS iPreferNumber,
		chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights as averageDailyRate, 
		chargedReservations.roomNights, 
		chargedReservations.roomRevenueInHotelCurrency as reservationRevenue, 
		NULL AS iPreferCharge,
		SUM(case when chargedReservations.[classificationID] = 1 -- Booking
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency
					end
				else null
			end) AS bookCharge, 
		SUM(case when chargedReservations.[classificationID] = 2 -- commission
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS commCharge, 
		SUM(case when chargedReservations.classificationID = 3 -- surcharges
				then 
					case when chargedReservations.chargeValueInHotelCurrency = 0
						then null
						else chargedReservations.chargeValueInHotelCurrency						
					end
				else null
			end) AS surcharge,
		SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
		MIN(case when chargedReservations.[classificationID] <> 3 -- surcharges
				then LTRIM(RTRIM(GPSopIC.itemGrouping))
			END ) AS itemGrouping,
		0 AS criteriaGroupID
		
FROM	@GP_SOPNumber_ItemCodes GPSopIC

JOIN dbo.CalculatedDebits chargedReservations with (nolock)
	ON  chargedReservations.sopNumber = @SOPNumber
	AND	chargedReservations.itemCode = GPSopIC.ITEMNMBR 

LEFT OUTER JOIN 	dbo.mostrecenttransactions MRT with (nolock)
	ON	chargedReservations.confirmationNumber = MRT.confirmationNumber 

GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
		chargedReservations.arrivalDate, MRT.guestLastName, 
		MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
		MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency
) AS main
full outer join
(
SELECT	C.hotelCurrencyCode AS currency,
		CAST(C.arrivalDate AS DATE) AS arrivalDate,
		UPPER(SUBSTRING(Account_Statement_Import.Member_Name, CHARINDEX(' ', Account_Statement_Import.Member_Name) + 1, LEN(Account_Statement_Import.Member_Name))) AS guestLastName,
		UPPER(SUBSTRING(Account_Statement_Import.Member_Name, 1, NULLIF(CHARINDEX(' ', Account_Statement_Import.Member_Name) - 1, -1))) AS guestFirstName,
		C.confirmationNumber AS confirmationNumber,
		MRT.confirmationDate AS confirmationDate,
		Superset.dbo.getBilledConsortia(C.confirmationNumber) AS consortia,
		COALESCE (MRT.IATANumber, N'') AS IATANumber, 
		COALESCE (MRT.rateTypeCode, N'') AS rateTypeCode,
		MRT.loyaltyNumber AS iPreferNumber,
		Case When C.roomNights <> 0
			 then ROUND(C.roomRevenueInHotelCurrency, currencyMaster.DECPLCUR-1) / C.roomNights 
			 ELSE 0 end
		AS averageDailyRate,
		C.roomNights AS roomNights,
		ROUND(C.roomRevenueInHotelCurrency,currencyMaster.DECPLCUR-1) AS reservationRevenue,
		SUM(C.chargeValueInHotelCurrency) AS iPreferCharge,
		NULL AS bookCharge, 
		NULL AS commCharge, 
		NULL AS surcharge,
		SUM(C.chargeValueInHotelCurrency) AS totalCharge,
		'Hotel & Manual iPrefer Point Awards' AS itemGrouping,
		C_ECG.criteriaGroupID

FROM BillingRewrite.dbo.Charges C

LEFT JOIN dbo.mostrecenttransactions MRT
	ON C.confirmationNumber = MRT.confirmationNumber
			
JOIN	BSI.Account_Statement_Import
	ON	C.confirmationNumber = Account_Statement_Import.Booking_ID
     AND MRT.loyaltyNumber = Account_Statement_Import.Membership_Number
     AND C.roomRevenueInHotelCurrency = Account_Statement_Import.Reservation_Amount	
	 --AND IPC.Billing_Concept = Account_Statement_Import.Billing_Concept
	
JOIN @currencyMaster as currencyMaster
	ON	C.hotelCurrencyCode = currencyMaster.CURNCYID
	LEFT JOIN
	BillingRewrite.dbo.BillingRules
	ON
	C.billingRuleID = BillingRules.billingRuleID
	LEFT JOIN
	BillingRewrite.dbo.Clauses_ExcludeCriteriaGroups C_ECG
	ON
	BillingRules.clauseID = C_ECG.clauseID

WHERE	C.classificationID = 5
		AND
		C.SOPNumber = @SOPNumber 

GROUP BY C.arrivalDate,
		C.SOPNumber,
		Account_Statement_Import.Member_Name,
		MRT.loyaltyNumber,
		C.confirmationNumber,
		MRT.confirmationDate,
		MRT.IATANumber,
		MRT.rateTypeCode,
		MRT.confirmationDate,
		C.roomNights,
		--IPC.Billing_Concept,
		C.hotelCode,
		Account_Statement_Import.Hotel_Name,
		C.hotelCurrencyCode,
		C.chargeValueInUSD,
		C.roomRevenueInHotelCurrency,
		--IPC.Flat_Charges,
		--IPC.Percentage_Charges, 
		currencyMaster.DECPLCUR,
		C_ECG.criteriaGroupID
) AS iPrefer
ON main.confirmationNumber = iPrefer.confirmationNumber

ORDER BY itemGrouping, arrivalDate, guestLastName, confirmationNumber
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
