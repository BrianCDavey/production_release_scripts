/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 9/11/2019 11:06:03 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [Prof_Rpt].[FinalResults]'
GO
ALTER TABLE [Prof_Rpt].[FinalResults] DROP CONSTRAINT [PK_FinalResults]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [Prof_Rpt].[Reservations]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [Prof_Rpt].[Reservations] ALTER COLUMN [ConfirmedSynxisConfirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [Prof_Rpt].[Reservations] ALTER COLUMN [hotelCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_IPrefer]'
GO

ALTER PROCEDURE [rpt].[ProfitabilityReport_IPrefer]
	@startDate date,
	@endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	TRUNCATE TABLE Prof_Rpt.IPrefer;

	INSERT INTO [Prof_Rpt].[IPrefer]
           ([confirmationNumber]
           , [ConfirmedSynxisConfirmationNumber]
           , [hotelCode]
           , [sopNumber]
           , [invoiceDate]
           , [invoiceYear]
           , [invoiceMonth]
           , [billableDate]
           , [billableYear]
           , [billableMonth]
           , [BillyConfirmationNumber]
           , [PHGSiteBillyConfirmationNumber]
           , [HHASiteBillyConfirmationNumber]
           , [IPreferConfirmationNumber]
           , [BillyConfirmationNumberWithZeroBookAndComm]
           , [itemCodeFamily]
           , [surchargeItemCode]
           , [BillyBookingChargesUSD]
           , [PHGSiteBillyBookingChargesUSD]
           , [HHASiteBillyBookingChargesUSD]
           , [commissionsUSD]
           , [PHGSiteCommissionsUSD]
           , [HHASiteCommissionsUSD]
           , [surchargesUSD]
           , [PHGSiteSurchargesUSD]
           , [HHASiteSurchargesUSD]
           , [BookAndCommUSD]
           , [PHGSiteBookAndCommUSD]
           , [HHASiteBookAndCommUSD]
           , [IPreferChargesUSD]
           , [BillyBookingChargesHotelCurrency]
           , [PHGSiteBillyBookingChargesHotelCurrency]
           , [HHASiteBillyBookingChargesHotelCurrency]
           , [commissionsHotelCurrency]
           , [PHGSiteCommissionsHotelCurrency]
           , [HHASiteCommissionsHotelCurrency]
           , [surchargesHotelCurrency]
           , [PHGSiteSurchargesHotelCurrency]
           , [HHASiteSurchargesHotelCurrency]
           , [BookAndCommHotelCurrency]
           , [PHGSiteBookAndCommHotelCurrency]
           , [HHASiteBookAndCommHotelCurrency]
           , [IPreferChargesHotelCurrency]
           , [IPreferCost])
	SELECT 
	ipc.Booking_ID AS confirmationNumber
	, t.confirmationNumber AS [ConfirmedSynxisConfirmationNumber]
	, ipc.Hotel_Code AS hotelCode
	, ipc.sopNumber AS sopNumber

	, ipc.invoiceDate AS invoicedDate
	, YEAR(ipc.invoiceDate) AS invoicedYear
	, MONTH(ipc.invoiceDate) AS invoicedMonth

	, ipc.Billing_Date AS billableDate
	, YEAR(MIN(ipc.Billing_Date)) AS billableYear
	, MONTH(MIN(ipc.Billing_Date)) AS billableMonth

	, NULL AS [BillyConfirmationNumber]
	, NULL AS [PHGSiteBillyConfirmationNumber]
	, NULL AS [HHASiteBillyConfirmationNumber]
	, ipc.Booking_ID AS [IPreferConfirmationNumber]
	, NULL AS [BillyConfirmationNumberWithZeroBookAndComm]

	, 'IPREFER' AS itemCodeFamily
	, NULL AS surchargeItemCode

	, NULL AS [BillyBookingChargesUSD]
	, NULL AS [PHGSiteBillyBookingChargesUSD]
	, NULL AS [HHASiteBillyBookingChargesUSD]
	, NULL AS [commissionsUSD]
	, NULL AS [PHGSiteCommissionsUSD]
	, NULL AS [HHASiteCommissionsUSD]
	, NULL AS [surchargesUSD]
	, NULL AS [PHGSiteSurchargesUSD]
	, NULL AS [HHASiteSurchargesUSD]
	, NULL AS [BookAndCommUSD]
	, NULL AS [PHGSiteBookAndCommUSD]
	, NULL AS [HHASiteBookAndCommUSD]
	, SUM(ipc.Billable_Amount_USD) AS [IPreferChargesUSD]

	, NULL AS [BillyBookingChargesHotelCurrency]
	, NULL AS [PHGSiteBillyBookingChargesHotelCurrency]
	, NULL AS [HHASiteBillyBookingChargesHotelCurrency]
	, NULL AS [commissionsHotelCurrency]
	, NULL AS [PHGSiteCommissionsHotelCurrency]
	, NULL AS [HHASiteCommissionsHotelCurrency]
	, NULL AS [surchargesHotelCurrency]
	, NULL AS [PHGSiteSurchargesHotelCurrency]
	, NULL AS [HHASiteSurchargesHotelCurrency]
	, NULL AS [BookAndCommHotelCurrency]
	, NULL AS [PHGSiteBookAndCommHotelCurrency]
	, NULL AS [HHASiteBookAndCommHotelCurrency]
	, SUM(ipc.Billable_Amount_Hotel_Currency) AS [IPreferChargesHotelCurrency]

	, 0 AS [IPreferCost]

FROM Superset.BSI.IPrefer_Charges ipc
	LEFT JOIN Reservations.dbo.Transactions t ON ipc.Booking_ID = t.confirmationNumber
	LEFT JOIN Reservations.dbo.TransactionDetail td ON t.transactionDetailID = td.transactionDetailID

WHERE ipc.Billing_Date BETWEEN @startDate AND @endDate

GROUP BY ipc.Booking_ID
	,t.confirmationNumber
	,ipc.Hotel_Code
	,ipc.Billing_Date
	,ipc.invoiceDate
	,ipc.sopNumber;

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [Prof_Rpt].[FinalResults]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [Prof_Rpt].[FinalResults] ADD
[ConfirmedSynxisBookingCount] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [Prof_Rpt].[FinalResults] DROP
COLUMN [ConfirmedSynxisConfirmationNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[Prof_Rpt].[FinalResults].[BillyConfirmationNumber]', N'BillyBookingCount', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[Prof_Rpt].[FinalResults].[PHGSiteBillyConfirmationNumber]', N'PHGSiteBillyBookingCount', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[Prof_Rpt].[FinalResults].[HHASiteBillyConfirmationNumber]', N'HHASiteBillyBookingCount', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[Prof_Rpt].[FinalResults].[IPreferConfirmationNumber]', N'IPreferBookingCount', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[Prof_Rpt].[FinalResults].[BillyWithZeroBookAndCommConfirmationNumber]', N'BillyWithZeroBookAndCommBookingCount', N'COLUMN'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [Prof_Rpt].[FinalResults] ALTER COLUMN [hotelCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [Prof_Rpt].[FinalResults] ALTER COLUMN [IPreferCost] [decimal] (38, 2) NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_FinalResults] on [Prof_Rpt].[FinalResults]'
GO
ALTER TABLE [Prof_Rpt].[FinalResults] ADD CONSTRAINT [PK_FinalResults] PRIMARY KEY CLUSTERED  ([confirmationNumber], [hotelCode], [invoiceDate], [billableYear], [billableMonth])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_FinalResult_WithRes]'
GO

ALTER PROCEDURE [rpt].[ProfitabilityReport_FinalResult_WithRes]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	INSERT INTO [Prof_Rpt].[FinalResults]
           ([confirmationNumber]
           ,[ConfirmedSynxisBookingCount]
           ,[hotelCode]
           ,[sopNumber]
           ,[invoiceDate]
           ,[invoiceYear]
           ,[invoiceMonth]
           ,[billableDate]
           ,[billableYear]
           ,[billableMonth]
           ,[BillyBookingCount]
           ,[PHGSiteBillyBookingCount]
           ,[HHASiteBillyBookingCount]
           ,[IPreferBookingCount]
           ,[BillyWithZeroBookAndCommBookingCount]
           ,[itemCodeFamily]
           ,[surchargeItemCode]

           ,[BillyBookingChargesUSD]
           ,[PHGSiteBillyBookingChargesUSD]
           ,[HHASiteBillyBookingChargesUSD]
           ,[commissionsUSD]
           ,[PHGSiteCommissionsUSD]
           ,[HHASiteCommissionsUSD]
           ,[surchargesUSD]
           ,[PHGSiteSurchargesUSD]
           ,[HHASiteSurchargesUSD]
           ,[BookAndCommUSD]
           ,[PHGSiteBookAndCommUSD]
           ,[HHASiteBookAndCommUSD]
           ,[IPreferChargesUSD]

           ,[BillyBookingChargesHotelCurrency]
           ,[PHGSiteBillyBookingChargesHotelCurrency]
           ,[HHASiteBillyBookingChargesHotelCurrency]
           ,[commissionsHotelCurrency]
           ,[PHGSiteCommissionsHotelCurrency]
           ,[HHASiteCommissionsHotelCurrency]
           ,[surchargesHotelCurrency]
           ,[PHGSiteSurchargesHotelCurrency]
           ,[HHASiteSurchargesHotelCurrency]
           ,[BookAndCommHotelCurrency]
           ,[PHGSiteBookAndCommHotelCurrency]
           ,[HHASiteBookAndCommHotelCurrency]
           ,[IPreferChargesHotelCurrency]
           ,[IPreferCost])
	SELECT r.[confirmationNumber]
           ,COUNT(DISTINCT r.[ConfirmedSynxisConfirmationNumber])
           ,r.[hotelCode]
           ,b.[sopNumber]
           ,b.[invoiceDate]
           ,b.[invoiceYear]
           ,b.[invoiceMonth]
           ,MIN(b.[billableDate])
           ,b.[billableYear]
           ,b.[billableMonth]
           ,COUNT(DISTINCT b.[BillyConfirmationNumber])
           ,COUNT(DISTINCT b.[PHGSiteBillyConfirmationNumber])
           ,COUNT(DISTINCT b.[HHASiteBillyConfirmationNumber])
		   ,SUM(CASE WHEN i.IPreferConfirmationNumber = b.IPreferConfirmationNumber THEN 1 
				WHEN i.IPreferConfirmationNumber IS NOT NULL OR b.IPreferConfirmationNumber IS NOT NULL THEN 1
				ELSE 0
				END) AS IPreferBookingCount
           ,COUNT(DISTINCT b.[BillyConfirmationNumberWithZeroBookAndComm])
           ,b.[itemCodeFamily]
           ,b.[surchargeItemCode]

           ,SUM(b.[BillyBookingChargesUSD])
           ,SUM(b.[PHGSiteBillyBookingChargesUSD])
           ,SUM(b.[HHASiteBillyBookingChargesUSD])
           ,SUM(b.[commissionsUSD])
           ,SUM(b.[PHGSiteCommissionsUSD])
           ,SUM(b.[HHASiteCommissionsUSD])
           ,SUM(b.[surchargesUSD])
           ,SUM(b.[PHGSiteSurchargesUSD])
           ,SUM(b.[HHASiteSurchargesUSD])
           ,SUM(b.[BookAndCommUSD])
           ,SUM(b.[PHGSiteBookAndCommUSD])
           ,SUM(b.[HHASiteBookAndCommUSD])
           ,SUM(b.[IPreferChargesUSD]) + SUM(i.[IPreferChargesUSD])

           ,SUM(b.[BillyBookingChargesHotelCurrency])
           ,SUM(b.[PHGSiteBillyBookingChargesHotelCurrency])
           ,SUM(b.[HHASiteBillyBookingChargesHotelCurrency])
           ,SUM(b.[commissionsHotelCurrency])
           ,SUM(b.[PHGSiteCommissionsHotelCurrency])
           ,SUM(b.[HHASiteCommissionsHotelCurrency])
           ,SUM(b.[surchargesHotelCurrency])
           ,SUM(b.[PHGSiteSurchargesHotelCurrency])
           ,SUM(b.[HHASiteSurchargesHotelCurrency])
           ,SUM(b.[BookAndCommHotelCurrency])
           ,SUM(b.[PHGSiteBookAndCommHotelCurrency])
           ,SUM(b.[HHASiteBookAndCommHotelCurrency])
           ,SUM(b.[IPreferChargesHotelCurrency]) + SUM(b.[IPreferChargesHotelCurrency])
           ,SUM(b.[IPreferCost]) + SUM(i.[IPreferCost])

	FROM Prof_Rpt.Reservations as r
		LEFT JOIN Prof_Rpt.Billy as b 
			ON r.confirmationNumber = b.confirmationNumber
			AND r.hotelCode = b.hotelCode
			AND r.billableYear = b.billableYear
			AND r.billableMonth = b.billableMonth
		LEFT JOIN Prof_Rpt.IPrefer as i 
			ON r.confirmationNumber = i.confirmationNumber
			AND r.hotelCode = i.hotelCode
			AND r.billableYear = i.billableYear
			AND r.billableMonth = i.billableMonth

	GROUP BY r.[confirmationNumber]
           ,r.[hotelCode]
           ,b.[sopNumber]
           ,b.[invoiceDate]
           ,b.[invoiceYear]
           ,b.[invoiceMonth]
		   ,b.[billableYear]
           ,b.[billableMonth]
		   ,b.[itemCodeFamily]
           ,b.[surchargeItemCode];
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_FinalResult_NoRes]'
GO

ALTER PROCEDURE [rpt].[ProfitabilityReport_FinalResult_NoRes]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	INSERT INTO [Prof_Rpt].[FinalResults]
           ([confirmationNumber]
           ,[ConfirmedSynxisBookingCount]
           ,[hotelCode]
           ,[sopNumber]
           ,[invoiceDate]
           ,[invoiceYear]
           ,[invoiceMonth]
           ,[billableDate]
           ,[billableYear]
           ,[billableMonth]
           ,[BillyBookingCount]
           ,[PHGSiteBillyBookingCount]
           ,[HHASiteBillyBookingCount]
           ,[IPreferBookingCount]
           ,[BillyWithZeroBookAndCommBookingCount]
           ,[itemCodeFamily]
           ,[surchargeItemCode]

           ,[BillyBookingChargesUSD]
           ,[PHGSiteBillyBookingChargesUSD]
           ,[HHASiteBillyBookingChargesUSD]
           ,[commissionsUSD]
           ,[PHGSiteCommissionsUSD]
           ,[HHASiteCommissionsUSD]
           ,[surchargesUSD]
           ,[PHGSiteSurchargesUSD]
           ,[HHASiteSurchargesUSD]
           ,[BookAndCommUSD]
           ,[PHGSiteBookAndCommUSD]
           ,[HHASiteBookAndCommUSD]
           ,[IPreferChargesUSD]

           ,[BillyBookingChargesHotelCurrency]
           ,[PHGSiteBillyBookingChargesHotelCurrency]
           ,[HHASiteBillyBookingChargesHotelCurrency]
           ,[commissionsHotelCurrency]
           ,[PHGSiteCommissionsHotelCurrency]
           ,[HHASiteCommissionsHotelCurrency]
           ,[surchargesHotelCurrency]
           ,[PHGSiteSurchargesHotelCurrency]
           ,[HHASiteSurchargesHotelCurrency]
           ,[BookAndCommHotelCurrency]
           ,[PHGSiteBookAndCommHotelCurrency]
           ,[HHASiteBookAndCommHotelCurrency]
           ,[IPreferChargesHotelCurrency]
           ,[IPreferCost])
	SELECT b.[confirmationNumber]
           ,COUNT(DISTINCT b.[ConfirmedSynxisConfirmationNumber])
           ,b.[hotelCode]
           ,b.[sopNumber]
           ,b.[invoiceDate]
           ,b.[invoiceYear]
           ,b.[invoiceMonth]
           ,MIN(b.[billableDate])
           ,b.[billableYear]
           ,b.[billableMonth]
           ,COUNT(DISTINCT b.[BillyConfirmationNumber])
           ,COUNT(DISTINCT b.[PHGSiteBillyConfirmationNumber])
           ,COUNT(DISTINCT b.[HHASiteBillyConfirmationNumber])
		   ,SUM(CASE WHEN i.IPreferConfirmationNumber = b.IPreferConfirmationNumber THEN 1 
						WHEN i.IPreferConfirmationNumber IS NOT NULL OR b.IPreferConfirmationNumber IS NOT NULL THEN 1
						ELSE 0
						END) AS IPreferBookingCount
           ,COUNT(DISTINCT b.[BillyConfirmationNumberWithZeroBookAndComm])
           ,b.[itemCodeFamily]
           ,b.[surchargeItemCode]

           ,SUM(b.[BillyBookingChargesUSD])
           ,SUM(b.[PHGSiteBillyBookingChargesUSD])
           ,SUM(b.[HHASiteBillyBookingChargesUSD])
           ,SUM(b.[commissionsUSD])
           ,SUM(b.[PHGSiteCommissionsUSD])
           ,SUM(b.[HHASiteCommissionsUSD])
           ,SUM(b.[surchargesUSD])
           ,SUM(b.[PHGSiteSurchargesUSD])
           ,SUM(b.[HHASiteSurchargesUSD])
           ,SUM(b.[BookAndCommUSD])
           ,SUM(b.[PHGSiteBookAndCommUSD])
           ,SUM(b.[HHASiteBookAndCommUSD])
           ,SUM(b.[IPreferChargesUSD]) + SUM(i.[IPreferChargesUSD])

           ,SUM(b.[BillyBookingChargesHotelCurrency])
           ,SUM(b.[PHGSiteBillyBookingChargesHotelCurrency])
           ,SUM(b.[HHASiteBillyBookingChargesHotelCurrency])
           ,SUM(b.[commissionsHotelCurrency])
           ,SUM(b.[PHGSiteCommissionsHotelCurrency])
           ,SUM(b.[HHASiteCommissionsHotelCurrency])
           ,SUM(b.[surchargesHotelCurrency])
           ,SUM(b.[PHGSiteSurchargesHotelCurrency])
           ,SUM(b.[HHASiteSurchargesHotelCurrency])
           ,SUM(b.[BookAndCommHotelCurrency])
           ,SUM(b.[PHGSiteBookAndCommHotelCurrency])
           ,SUM(b.[HHASiteBookAndCommHotelCurrency])
           ,SUM(b.[IPreferChargesHotelCurrency]) + SUM(b.[IPreferChargesHotelCurrency])
           ,SUM(b.[IPreferCost]) + SUM(i.[IPreferCost])

	FROM Prof_Rpt.Billy as b 
		LEFT JOIN Prof_Rpt.IPrefer as i 
			ON b.confirmationNumber = i.confirmationNumber
			AND b.hotelCode = i.hotelCode
			AND b.billableYear = i.billableYear
			AND b.billableMonth = i.billableMonth
		LEFT JOIN Prof_Rpt.FinalResults as f 
			ON b.confirmationNumber = f.confirmationNumber
			AND b.hotelCode = f.hotelCode
			AND b.billableYear = f.billableYear
			AND b.billableMonth = f.billableMonth
	
	WHERE f.confirmationNumber IS NULL

	GROUP BY b.[confirmationNumber]
           ,b.[hotelCode]
           ,b.[sopNumber]
           ,b.[invoiceDate]
           ,b.[invoiceYear]
           ,b.[invoiceMonth]
		   ,b.[billableYear]
           ,b.[billableMonth]
		   ,b.[itemCodeFamily]
           ,b.[surchargeItemCode];


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_FinalResult_NoBilly]'
GO

ALTER PROCEDURE [rpt].[ProfitabilityReport_FinalResult_NoBilly]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	INSERT INTO [Prof_Rpt].[FinalResults]
           ([confirmationNumber]
           ,[ConfirmedSynxisBookingCount]
           ,[hotelCode]
           ,[sopNumber]
           ,[invoiceDate]
           ,[invoiceYear]
           ,[invoiceMonth]
           ,[billableDate]
           ,[billableYear]
           ,[billableMonth]
           ,[BillyBookingCount]
           ,[PHGSiteBillyBookingCount]
           ,[HHASiteBillyBookingCount]
           ,[IPreferBookingCount]
           ,[BillyWithZeroBookAndCommBookingCount]
           ,[itemCodeFamily]
           ,[surchargeItemCode]

           ,[BillyBookingChargesUSD]
           ,[PHGSiteBillyBookingChargesUSD]
           ,[HHASiteBillyBookingChargesUSD]
           ,[commissionsUSD]
           ,[PHGSiteCommissionsUSD]
           ,[HHASiteCommissionsUSD]
           ,[surchargesUSD]
           ,[PHGSiteSurchargesUSD]
           ,[HHASiteSurchargesUSD]
           ,[BookAndCommUSD]
           ,[PHGSiteBookAndCommUSD]
           ,[HHASiteBookAndCommUSD]
           ,[IPreferChargesUSD]

           ,[BillyBookingChargesHotelCurrency]
           ,[PHGSiteBillyBookingChargesHotelCurrency]
           ,[HHASiteBillyBookingChargesHotelCurrency]
           ,[commissionsHotelCurrency]
           ,[PHGSiteCommissionsHotelCurrency]
           ,[HHASiteCommissionsHotelCurrency]
           ,[surchargesHotelCurrency]
           ,[PHGSiteSurchargesHotelCurrency]
           ,[HHASiteSurchargesHotelCurrency]
           ,[BookAndCommHotelCurrency]
           ,[PHGSiteBookAndCommHotelCurrency]
           ,[HHASiteBookAndCommHotelCurrency]
           ,[IPreferChargesHotelCurrency]
           ,[IPreferCost])
	SELECT i.[confirmationNumber]
           ,COUNT(DISTINCT i.[ConfirmedSynxisConfirmationNumber])
           ,i.[hotelCode]
           ,i.[sopNumber]
           ,i.[invoiceDate]
           ,i.[invoiceYear]
           ,i.[invoiceMonth]
           ,MIN(i.[billableDate])
           ,i.[billableYear]
           ,i.[billableMonth]
           ,COUNT(DISTINCT i.[BillyConfirmationNumber])
           ,COUNT(DISTINCT i.[PHGSiteBillyConfirmationNumber])
           ,COUNT(DISTINCT i.[HHASiteBillyConfirmationNumber])
		   ,SUM(CASE WHEN i.IPreferConfirmationNumber = i.IPreferConfirmationNumber THEN 1 
						WHEN i.IPreferConfirmationNumber IS NOT NULL OR i.IPreferConfirmationNumber IS NOT NULL THEN 1
						ELSE 0
						END) AS IPreferBookingCount
           ,COUNT(DISTINCT i.[BillyConfirmationNumberWithZeroBookAndComm])
           ,i.[itemCodeFamily]
           ,i.[surchargeItemCode]

           ,SUM(i.[BillyBookingChargesUSD])
           ,SUM(i.[PHGSiteBillyBookingChargesUSD])
           ,SUM(i.[HHASiteBillyBookingChargesUSD])
           ,SUM(i.[commissionsUSD])
           ,SUM(i.[PHGSiteCommissionsUSD])
           ,SUM(i.[HHASiteCommissionsUSD])
           ,SUM(i.[surchargesUSD])
           ,SUM(i.[PHGSiteSurchargesUSD])
           ,SUM(i.[HHASiteSurchargesUSD])
           ,SUM(i.[BookAndCommUSD])
           ,SUM(i.[PHGSiteBookAndCommUSD])
           ,SUM(i.[HHASiteBookAndCommUSD])
           ,SUM(i.[IPreferChargesUSD]) + SUM(i.[IPreferChargesUSD])

           ,SUM(i.[BillyBookingChargesHotelCurrency])
           ,SUM(i.[PHGSiteBillyBookingChargesHotelCurrency])
           ,SUM(i.[HHASiteBillyBookingChargesHotelCurrency])
           ,SUM(i.[commissionsHotelCurrency])
           ,SUM(i.[PHGSiteCommissionsHotelCurrency])
           ,SUM(i.[HHASiteCommissionsHotelCurrency])
           ,SUM(i.[surchargesHotelCurrency])
           ,SUM(i.[PHGSiteSurchargesHotelCurrency])
           ,SUM(i.[HHASiteSurchargesHotelCurrency])
           ,SUM(i.[BookAndCommHotelCurrency])
           ,SUM(i.[PHGSiteBookAndCommHotelCurrency])
           ,SUM(i.[HHASiteBookAndCommHotelCurrency])
           ,SUM(i.[IPreferChargesHotelCurrency]) + SUM(i.[IPreferChargesHotelCurrency])
           ,SUM(i.[IPreferCost]) + SUM(i.[IPreferCost])

	FROM Prof_Rpt.IPrefer as i 
		LEFT JOIN Prof_Rpt.FinalResults as f 
			ON i.confirmationNumber = f.confirmationNumber
			AND i.hotelCode = f.hotelCode
			AND i.billableYear = f.billableYear
			AND i.billableMonth = f.billableMonth
	
	WHERE f.confirmationNumber IS NULL

	GROUP BY i.[confirmationNumber]
           ,i.[hotelCode]
           ,i.[sopNumber]
           ,i.[invoiceDate]
           ,i.[invoiceYear]
           ,i.[invoiceMonth]
		   ,i.[billableYear]
           ,i.[billableMonth]
		   ,i.[itemCodeFamily]
           ,i.[surchargeItemCode];


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_FINAL]'
GO

ALTER PROCEDURE [rpt].[ProfitabilityReport_FINAL]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'


	--build IATA table
	-----------------------------------------------------------------------
		DROP TABLE IF EXISTS #iataCRM

		CREATE TABLE #iataCRM
		(
			iataNumber nvarchar(20) NOT NULL,
			iatamanager nvarchar(200) NOT NULL,

			PRIMARY KEY CLUSTERED(iataNumber,iatamanager)
		)

		INSERT INTO #iataCRM(iataNumber,iatamanager)
		SELECT accountnumber as iataNumber,MIN(phg_iataglobalaccountmanageridName) iatamanager
		FROM LocalCRM.dbo.account
		WHERE phg_iataglobalaccountmanageridName IS NOT NULL
			AND accountnumber IS NOT NULL
		GROUP BY accountnumber
	----------------------------------------------------------------------

	TRUNCATE TABLE rpt.[ProfitabilityReport]

	INSERT INTO [rpt].[ProfitabilityReport]
           ([crsHotelID]
           ,[hotelCode]
           ,[hotelName]
           ,[primaryBrand]
           ,[gpSalesTerritory]
           ,[AMD]
           ,[RD]
           ,[RAM]
           ,[AccountManager]
           ,[geographicRegion]
           ,[country]
           ,[city]
           ,[state]
           ,[hotelRooms]
           ,[hotelStatus]
           ,[CRMClassification]
           ,[hotelcurrency]
           ,[billingDescription]
           ,[channel]
           ,[secondarySource]
           ,[subSource]
           ,[CROcode]
           ,[reportingChannel]
           ,[reportingSecondarySource]
           ,[reportingSubSource]
           ,[itemCodeFamily]
           ,[surchargeItemCode]
           ,[IataGam]
           ,[IATAGroups]
           ,[IATANumber]
           ,[travelAgencyName]
           ,[travelAgencyCity]
           ,[travelAgencyState]
           ,[travelAgencyCountry]
           ,[rateCategoryCode]
           ,[rateTypeCode]
           ,[ratename]
           ,[rateCategory_PHG]
           ,[ArrivalYear]
           ,[ArrivalMonth]
           ,[ConfirmationYear]
           ,[ConfirmationMonth]
           ,[CancellationYear]
           ,[CancellationMonth]
           ,[BillableYear]
           ,[BillableMonth]
           ,[InvoicedYear]
           ,[InvoicedMonth]
           ,[InvoicedDate]
           ,[SopNumber]
           ,[BookingStatus]
           ,[ConfirmedSynxisBookingCount]
           ,[ConfirmedRevenueUSD]
           ,[ConfirmedRoomNights]
           ,[ConfirmedTotalCost]
           ,[avgNightlyRateUSD]
           ,[avgLengthOfStay]
           ,[BillyBookingCount]
           ,[PHGSiteBillyBookingCount]
           ,[HHASiteBillyBookingCount]
           ,[BillyBookingCharges]
           ,[PHGSiteBillyBookingCharges]
           ,[HHASiteBillyBookingCharges]
           ,[commissions]
           ,[PHGSiteCommissions]
           ,[HHASiteCommissions]
           ,[surcharges]
           ,[PHGSiteSurcharges]
           ,[HHASiteSurcharges]
           ,[BookAndComm]
           ,[PHGSiteBookAndComm]
           ,[HHASiteBookAndComm]
           ,[costperbooking]
           ,[IPreferBookingCount]
           ,[IPreferCharges]
           ,[IPreferCost]
           ,[BillyBookingWithZeroBookAndComm]
           ,[ConfirmedRevenueHotelCurrency]
           ,[BillyBookingChargesHotelCurrency]
           ,[PHGSiteBillyBookingChargesHotelCurrency]
           ,[HHASiteBillyBookingChargesHotelCurrency]
           ,[CommissionsHotelCurrency]
           ,[PHGSiteCommissionsHotelCurrency]
           ,[HHASiteCommissionsHotelCurrency]
           ,[SurchargesHotelCurrency]
           ,[PHGSiteSurchargesHotelCurrency]
           ,[HHASiteSurchargesHotelCurrency]
           ,[BookAndCommHotelCurrency]
           ,[PHGSiteBookAndCommHotelCurrency]
           ,[HHASiteBookAndCommHotelCurrency]
           ,[IPreferChargesHotelCurrency])
	SELECT
		CASE 
			WHEN t.DataSourceID != @SynXis_DS THEN ISNULL(h.synxisID,h.OpenHospID)
			WHEN t.DataSourceID = @SynXis_DS THEN h.SynXisID 
			ELSE h.OpenHospID 
		END as [crsHotelID]
		, f.[hotelCode]
		, h.[hotelName]
		, hr.MainBrandCode AS [primaryBrand]
		, hr.PHG_FinanceRegion as [gpSalesTerritory]
		, hr.PHG_AreaManagerIdName as [AMD]
		, hr.PHG_RegionalManagerIdName as [RD]
		, hr.PHG_RevenueAccountManagerIdName as [RAM]
		, hr.PHG_AccountManagerName as [AccountManager]
		, hr.GeographicRegionName as [geographicRegion]
		, hr.ShortName as [country]
		, hr.PhysicalCity as [city]
		, hr.State as [state]
		, hr.TotalRooms as [hotelRooms]
		, hr.StatusCodeName as [hotelStatus]
		, ha.accountclassificationcodename as [CRMClassification]
		, hr.currencycode as [hotelcurrency]
		, sbd.billingDescription as [billingDescription]
		, ISNULL(cc.channel,'Billy charge for non-reservation') as [channel]
		, c2s.secondarySource as [secondarySource]
		, css.subSource as [subSource]
		, cr.CRO_Code as [CROcode]
		, pc.PH_Channel as reportChannel
		, p2s.PH_SecondaryChannel as reportSecondaryChannel
		, pss.PH_SubChannel as reportSubChannel
		, f.[itemCodeFamily]
		, f.[surchargeItemCode]
		, taa.iataManager as [IataGam]
		, Core.dbo.GetIATAGroups(iata.IATANumber,ts.confirmationDate) as [IATAGroups]
		, iata.IATANumber as [IATANumber]
		, ta.Name as [travelAgencyName]
		, taLcy.City_Text as [travelAgencyCity]
		, taLs.State_Text as [travelAgencyState]
		, taLc.Country_Text as [travelAgencyCountry]
		, rcat.rateCategoryCode as [rateCategoryCode]
		, rc.RateCode as [rateTypeCode]
		, rc.RateName as [ratename]
		,	CASE 
				WHEN LEFT(rc.RateCode,3) = 'NEG' OR LEFT(rcat.rateCategoryCode,3) = 'NEG' THEN 'NEG' 
				WHEN LEFT(rc.RateCode,3) = 'CON' OR LEFT(rcat.rateCategoryCode,3) = 'CON' THEN 'CON' 
				WHEN LEFT(rc.RateCode,3) = 'MKT' OR LEFT(rcat.rateCategoryCode,3) = 'MKT' THEN 'MKT' 
				WHEN LEFT(rc.RateCode,3) = 'PRO' OR LEFT(rcat.rateCategoryCode,3) = 'PRO' THEN 'PRO'
				WHEN LEFT(rc.RateCode,3) = 'PKG' OR LEFT(rcat.rateCategoryCode,3) = 'PKG' THEN 'PKG'  
				WHEN LEFT(rc.RateCode,3) = 'GOV' OR LEFT(rcat.rateCategoryCode,3) = 'GOV' THEN 'GOV' 
				WHEN LEFT(rc.RateCode,3) = 'BAR' OR LEFT(rcat.rateCategoryCode,3) = 'BAR' THEN 'BAR' 
				WHEN LEFT(rc.RateCode,3) = 'DIS' OR LEFT(rcat.rateCategoryCode,3) = 'DIS' THEN 'DIS' 
				ELSE LEFT(rcat.rateCategoryCode,3)
			END as [rateCategory_PHG]
		, YEAR(td.arrivalDate) as [ArrivalYear]
		, MONTH(td.arrivalDate) as [ArrivalMonth]
		, YEAR(ts.confirmationDate) as [ConfirmationYear]
		, MONTH(ts.confirmationDate) as [ConfirmationMonth]
		, YEAR(ts.cancellationDate) as CancellationYear
		, MONTH(ts.cancellationDate) as CancellationMonth
		, f.[BillableYear]
		, f.[BillableMonth]
		, f.[InvoiceYear]
		, f.[InvoiceMonth]
		, f.[InvoiceDate]
		, f.[SopNumber]
		, ts.status as bookingStatus

		,SUM(f.ConfirmedSynxisBookingCount) AS ConfirmedSynxisBookingCount
		,MAX(td.reservationRevenue * ISNULL(xeArri.toUSD,xeConf.toUSD)) as [ConfirmedRevenueUSD]
		,MAX(td.rooms * td.nights) as [ConfirmedRoomNights]
		,MAX(
			CASE sbd.billingDescription 
				WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost 
			END 
			+ 
			CASE cc.channel 
				WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 
			END) as [ConfirmedTotalCost]
		,MAX((td.reservationRevenue * ISNULL(xeArri.toUSD,xeConf.toUSD)) / (td.rooms * td.nights)) as [avgNightlyRateUSD]
		,MAX(td.nights) as [avgLengthOfStay]
		,SUM(f.BillyBookingCount) AS BillyBookingCount
		,SUM(f.PHGSiteBillyBookingCount) AS PHGSiteBillyBookingCount
		,SUM(f.HHASiteBillyBookingCount) AS HHASiteBillyBookingCount
		,SUM(f.BillyBookingCharges) AS BillyBookingCharges
		,SUM(f.PHGSiteBillyBookingCharges) AS PHGSiteBillyBookingCharges
		,SUM(f.HHASiteBillyBookingCharges) AS HHASiteBillyBookingCharges
		,SUM(f.commissionsUSD) AS commissions
		,SUM(f.PHGSiteCommissionsUSD) AS PHGSiteCommissions
		,SUM(f.HHASiteCommissionsUSD) AS HHASiteCommissions
		,SUM(f.surchargesUSD) AS surcharges
		,SUM(f.PHGSiteSurchargesUSD) AS PHGSiteSurcharges
		,SUM(f.HHASiteSurchargesUSD) AS HHASiteSurcharges
		,SUM(f.BookAndCommUSD) BookAndComm
		,SUM(f.PHGSiteBookAndCommUSD) AS PHGSiteBookAndComm
		,SUM(f.HHASiteBookAndCommUSD) AS HHASiteBookAndComm
		,MAX(
			CASE sbd.billingDescription 
				WHEN '' THEN pegsBookingCost.cost 
				ELSE synxisBookingCost.cost 
			END 
			+ 
			CASE cc.channel 
				WHEN 'GDS' THEN gdsBookingCost.cost 
				ELSE 0 
			END) / SUM(f.ConfirmedSynxisBookingCount) AS costperbooking
		,SUM(f.IPreferBookingCount) AS IPreferBookingCount
		,SUM(f.IPreferCharges) AS IPreferCharges
		,SUM(f.IPreferCost ) AS IPreferCost
		,SUM(f.BillyWithZeroBookAndCommBookingCount) AS BillyBookingWithZeroBookAndComm

		,MAX((td.reservationRevenue  * ISNULL(xeArri.toUSD,xeConf.toUSD)) / (ISNULL(xeArriGP.fromUSD,xeConfGP.fromUSD))) as [ConfirmedRevenueHotelCurrency]
		,SUM(f.BillyBookingChargesHotelCurrency) AS BillyBookingChargesHotelCurrency
		,SUM(f.PHGSiteBillyBookingChargesHotelCurrency) AS PHGSiteBillyBookingChargesHotelCurrency
		,SUM(f.HHASiteBillyBookingChargesHotelCurrency) AS HHASiteBillyBookingChargesHotelCurrency
		,SUM(f.commissionsHotelCurrency) AS CommissionsHotelCurrency
		,SUM(f.PHGSiteCommissionsHotelCurrency) AS PHGSiteCommissionsHotelCurrency
		,SUM(f.HHASiteCommissionsHotelCurrency) AS HHASiteCommissionsHotelCurrency
		,SUM(f.surchargesHotelCurrency) AS SurchargesHotelCurrency
		,SUM(f.PHGSiteSurchargesHotelCurrency) AS PHGSiteSurchargesHotelCurrency
		,SUM(f.HHASiteSurchargesHotelCurrency) AS HHASiteSurchargesHotelCurrency
		,SUM(f.BookAndCommHotelCurrency) AS BookAndCommHotelCurrency
		,SUM(f.PHGSiteBookAndCommHotelCurrency) AS PHGSiteBookAndCommHotelCurrency
		,SUM(f.HHASiteBookAndCommHotelCurrency) AS HHASiteBookAndCommHotelCurrency
		,SUM(f.IPreferChargesHotelCurrency) AS IPreferChargesHotelCurrency 
	FROM Prof_Rpt.FinalResults as f
	LEFT JOIN Reservations.dbo.Transactions t ON f.confirmationNumber = t.confirmationNumber
	LEFT JOIN Reservations.dbo.TransactionDetail td ON t.transactionDetailID = td.transactionDetailID
	LEFT JOIN Reservations.dbo.TransactionStatus ts ON t.TransactionStatusID = ts.TransactionStatusID
	LEFT JOIN Reservations.dbo.hotel th ON t.HotelID = th.HotelID
	LEFT JOIN Hotels.dbo.Hotel h ON th.Hotel_hotelID = h.HotelID
	LEFT JOIN Hotels.dbo.hotelsReporting hr ON h.HotelCode = hr.code
	LEFT JOIN LocalCRM.dbo.Account ha ON hr.crmGUID = ha.accountID
	LEFT JOIN IC.dbo.RM00101 as gpCust ON h.HotelCode = gpCust.CUSTNMBR
	LEFT JOIN CurrencyRates.dbo.dailyRates xeConfGP ON ts.confirmationDate = xeConfGP.rateDate AND LTRIM(RTRIM(gpCust.CURNCYID)) = xeConfGP.code
	LEFT JOIN CurrencyRates.dbo.dailyRates xeArriGP ON td.arrivalDate = xeArriGP.rateDate AND LTRIM(RTRIM(gpCust.CURNCYID)) = xeArriGP.code
	LEFT JOIN Reservations.synxis.Transactions st ON t.sourceKey = st.TransactionID AND t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis')
	LEFT JOIN Reservations.synxis.BillingDescription sbd ON st.BillingDescriptionID = sbd.BillingDescriptionID
	LEFT JOIN Reservations.dbo.CRS_BookingSource crsBS ON t.CRS_BookingSourceID = crsBS.BookingSourceID
	LEFT JOIN Reservations.dbo.CRS_Channel cc ON crsBS.ChannelID = cc.ChannelID
	LEFT JOIN Reservations.dbo.CRS_SecondarySource c2s ON crsBS.SecondarySourceID = c2s.SecondarySourceID
	LEFT JOIN Reservations.dbo.CRS_SubSource css ON crsBS.SubSourceID = css.SubSourceID
	LEFT JOIN Reservations.dbo.PH_BookingSource phBS ON t.PH_BookingSourceID = phBS.PH_BookingSourceID
	LEFT JOIN Reservations.dbo.PH_Channel pc ON phBS.PH_ChannelID = pc.PH_ChannelID
	LEFT JOIN Reservations.dbo.PH_SecondaryChannel p2s ON phBS.PH_SecondaryChannelID = p2s.PH_SecondaryChannelID
	LEFT JOIN Reservations.dbo.PH_SubChannel pss ON phBS.PH_SubChannelID = pss.PH_SubChannelID
	LEFT JOIN Reservations.dbo.CROCode ccro ON crsBS.CROCodeID = ccro.CROCodeID
	LEFT JOIN Reservations.authority.CRO_Code cro ON ccro.auth_CRO_CodeID = cro.CRO_CodeID
	LEFT JOIN Reservations.dbo.IATANumber iata ON t.IATANumberID = iata.IATANumberID
	LEFT JOIN Reservations.dbo.TravelAgent ta ON t.TravelAgentID = ta.TravelAgentID
	LEFT JOIN Reservations.dbo.Location taL ON ta.LocationID = taL.LocationID
	LEFT JOIN Reservations.dbo.City taLcy ON taL.CityID = taLcy.CityID
	LEFT JOIN Reservations.dbo.State taLs ON taL.StateID = taLs.StateID
	LEFT JOIN Reservations.dbo.Country taLc ON taL.CountryID = taLc.CountryID
	LEFT JOIN #iataCrm taa ON iata.IATANumber = taa.iataNumber
	LEFT JOIN Reservations.dbo.RateCode rc ON t.RateCodeID = rc.RateCodeID
	LEFT JOIN Reservations.dbo.RateCategory rcat ON t.RateCategoryID = rcat.RateCategoryID
	LEFT JOIN CurrencyRates.dbo.dailyRates xeConf ON ts.confirmationDate = xeConf.rateDate AND td.currency = xeConf.code
	LEFT JOIN CurrencyRates.dbo.dailyRates xeArri ON td.arrivalDate = xeArri.rateDate AND td.currency = xeArri.code

	LEFT OUTER JOIN Superset.dbo.gdsBookingCost ON c2s.secondarySource = gdsBookingCost.gdsType 
												AND td.arrivalDate BETWEEN gdsBookingCost.startDate
												AND ISNULL(gdsBookingCost.endDate,'12/31/2999')
	LEFT OUTER JOIN Superset.dbo.synxisBookingCost ON sbd.billingDescription = synxisBookingCost.billingDescription 
												AND td.arrivalDate BETWEEN synxisBookingCost.startDate
												AND ISNULL(synxisBookingCost.endDate,'12/31/2999')
	LEFT OUTER JOIN Superset.dbo.pegsBookingCost ON cc.channel = pegsBookingCost.channel 
												AND td.arrivalDate BETWEEN pegsBookingCost.startDate
												AND ISNULL(pegsBookingCost.endDate,'12/31/2999')

	GROUP BY 		CASE 
			WHEN t.DataSourceID != @SynXis_DS THEN ISNULL(h.synxisID,h.OpenHospID)
			WHEN t.DataSourceID = @SynXis_DS THEN h.SynXisID 
			ELSE h.OpenHospID 
		END
		, f.[hotelCode]
		, h.[hotelName]
		, hr.MainBrandCode
		,hr.PHG_FinanceRegion
		,hr.PHG_AreaManagerIdName
		,hr.PHG_RegionalManagerIdName
		,hr.PHG_RevenueAccountManagerIdName
		,hr.PHG_AccountManagerName
		,hr.GeographicRegionName
		,hr.ShortName
		,hr.PhysicalCity
		,hr.State
		,hr.TotalRooms
		,hr.StatusCodeName
		,ha.accountclassificationcodename
		,hr.currencycode
		,sbd.billingDescription
		,ISNULL(cc.channel,'charge for non-reservation')
		,c2s.secondarySource
		,css.subSource
		,cr.CRO_Code
		,pc.PH_Channel
		,p2s.PH_SecondaryChannel
		,pss.PH_SubChannel
		, f.[itemCodeFamily]
		, f.[surchargeItemCode]
		,taa.iataManager
		,Core.dbo.GetIATAGroups(iata.IATANumber,ts.confirmationDate)
		,iata.IATANumber
		,ta.Name
		,taLcy.City_Text
		,taLs.State_Text
		,taLc.Country_Text

		,rcat.rateCategoryCode
		,rc.RateCode
		,rc.RateName
		,	CASE 
				WHEN LEFT(rc.RateCode,3) = 'NEG' OR LEFT(rcat.rateCategoryCode,3) = 'NEG' THEN 'NEG' 
				WHEN LEFT(rc.RateCode,3) = 'CON' OR LEFT(rcat.rateCategoryCode,3) = 'CON' THEN 'CON' 
				WHEN LEFT(rc.RateCode,3) = 'MKT' OR LEFT(rcat.rateCategoryCode,3) = 'MKT' THEN 'MKT' 
				WHEN LEFT(rc.RateCode,3) = 'PRO' OR LEFT(rcat.rateCategoryCode,3) = 'PRO' THEN 'PRO'
				WHEN LEFT(rc.RateCode,3) = 'PKG' OR LEFT(rcat.rateCategoryCode,3) = 'PKG' THEN 'PKG'  
				WHEN LEFT(rc.RateCode,3) = 'GOV' OR LEFT(rcat.rateCategoryCode,3) = 'GOV' THEN 'GOV' 
				WHEN LEFT(rc.RateCode,3) = 'BAR' OR LEFT(rcat.rateCategoryCode,3) = 'BAR' THEN 'BAR' 
				WHEN LEFT(rc.RateCode,3) = 'DIS' OR LEFT(rcat.rateCategoryCode,3) = 'DIS' THEN 'DIS' 
				ELSE LEFT(rcat.rateCategoryCode,3)
			END 
		, YEAR(td.arrivalDate)
		, MONTH(td.arrivalDate)
		, YEAR(ts.confirmationDate)
		, MONTH(ts.confirmationDate)
		, YEAR(ts.cancellationDate)
		, MONTH(ts.cancellationDate)
		, f.[BillableYear]
		, f.[BillableMonth]
		, f.[InvoiceYear]
		, f.[InvoiceMonth]
		, f.[InvoiceDate]
		, f.[SopNumber]
		, ts.status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_Billy]'
GO

ALTER PROCEDURE [rpt].[ProfitabilityReport_Billy]
	@startDate date,
	@endDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	TRUNCATE TABLE prof_Rpt.Billy;

	INSERT INTO [Prof_Rpt].[Billy]
           ([confirmationNumber]
           ,[ConfirmedSynxisConfirmationNumber]
           ,[hotelCode]
           ,[sopNumber]
           ,[invoiceDate]
           ,[invoiceYear]
           ,[invoiceMonth]
           ,[billableDate]
           ,[billableYear]
           ,[billableMonth]
           ,[BillyConfirmationNumber]
           ,[PHGSiteBillyConfirmationNumber]
           ,[HHASiteBillyConfirmationNumber]
           ,[IPreferConfirmationNumber]
           ,[BillyConfirmationNumberWithZeroBookAndComm]
           ,[itemCodeFamily]
           ,[surchargeItemCode]
           ,[BillyBookingChargesUSD]
           ,[PHGSiteBillyBookingChargesUSD]
           ,[HHASiteBillyBookingChargesUSD]
           ,[commissionsUSD]
           ,[PHGSiteCommissionsUSD]
           ,[HHASiteCommissionsUSD]
           ,[surchargesUSD]
           ,[PHGSiteSurchargesUSD]
           ,[HHASiteSurchargesUSD]
           ,[BookAndCommUSD]
           ,[PHGSiteBookAndCommUSD]
           ,[HHASiteBookAndCommUSD]
           ,[IPreferChargesUSD]
           ,[BillyBookingChargesHotelCurrency]
           ,[PHGSiteBillyBookingChargesHotelCurrency]
           ,[HHASiteBillyBookingChargesHotelCurrency]
           ,[commissionsHotelCurrency]
           ,[PHGSiteCommissionsHotelCurrency]
           ,[HHASiteCommissionsHotelCurrency]
           ,[surchargesHotelCurrency]
           ,[PHGSiteSurchargesHotelCurrency]
           ,[HHASiteSurchargesHotelCurrency]
           ,[BookAndCommHotelCurrency]
           ,[PHGSiteBookAndCommHotelCurrency]
           ,[HHASiteBookAndCommHotelCurrency]
           ,[IPreferChargesHotelCurrency]
           ,[IPreferCost])
SELECT ch.confirmationNumber
	,t.confirmationNumber as [ConfirmedSynxisConfirmationNumber]
	,ch.hotelCode
	,ch.sopNumber as sopNumber

	,ch.invoiceDate as invoicedDate
	,YEAR(ch.invoiceDate) as invoicedYear
	,MONTH(ch.invoiceDate) as invoicedMonth

	,MIN(ch.billableDate) as billableDate
	,YEAR(MIN(ch.billableDate)) as billableYear
	,MONTH(MIN(ch.billableDate)) as billableMonth

	,MAX(CASE WHEN ch.classificationID <> 5 THEN ch.confirmationNumber ELSE NULL END) as [BillyConfirmationNumber]
	,MAX(CASE WHEN gpSiteID = '01' THEN ch.confirmationNumber ELSE NULL END) as [PHGSiteBillyConfirmationNumber]
	,MAX(CASE WHEN gpSiteID = '06' THEN ch.confirmationNumber ELSE NULL END) as [HHASiteBillyConfirmationNumber]
	,MAX(CASE WHEN ch.classificationID = 5 THEN ch.confirmationNumber ELSE NULL END) as [IPreferConfirmationNumber]
	,CASE 
			WHEN SUM(CASE 
						WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (5) THEN ISNULL(ch.chargeValueInUSD,0) 
						ELSE 0 
					END) = 0 THEN ch.confirmationNumber
			ELSE NULL
		END as [BillyConfirmationNumberWithZeroBookAndComm]

	,MAX(
		case
			when CHARINDEX('_I', ch.itemCode) > 0 then ' ' + rtrim(left(ch.itemCode, CHARINDEX('_', ch.itemCode) - 1))
			when CHARINDEX('_', ch.itemCode) > 0 then rtrim(left(ch.itemCode, CHARINDEX('_', ch.itemCode) - 1))
			else NULL
		end) as itemCodeFamily
	,MAX(
		case
			when CHARINDEX('_', ch.itemCode) > 0 then NULL
			else ch.itemCode
		end) as surchargeItemCode

	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInUSD ELSE 0 END) as [BillyBookingChargesUSD]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInUSD ELSE 0 END) as [PHGSiteBillyBookingChargesUSD]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInUSD ELSE 0 END) as [HHASiteBillyBookingChargesUSD]
	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInUSD ELSE 0 END) as [commissionsUSD]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInUSD ELSE 0 END) as [PHGSiteCommissionsUSD]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInUSD ELSE 0 END) as [HHASiteCommissionsUSD]
	,SUM(CASE WHEN items.ITMCLSCD = 'RES-TMP' THEN ch.chargeValueInUSD ELSE 0 END) as [surchargesUSD]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD = 'RES-TMP' THEN ch.chargeValueInUSD ELSE 0 END) as [PHGSiteSurchargesUSD]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD = 'RES-TMP' THEN ch.chargeValueInUSD ELSE 0 END) as [HHASiteSurchargesUSD]
	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInUSD ELSE 0 END) 
		+ SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInUSD ELSE 0 END) as [BookAndCommUSD]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInUSD ELSE 0 END) 
		+ SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInUSD ELSE 0 END) as [PHGSiteBookAndCommUSD]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInUSD ELSE 0 END) 
		+ SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInUSD ELSE 0 END) as [HHASiteBookAndCommUSD]
	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 5 THEN ch.chargeValueInUSD ELSE 0 END) as [IPreferChargesUSD]

	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [BillyBookingChargesHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [PHGSiteBillyBookingChargesHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [HHASiteBillyBookingChargesHotelCurrency]
	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [commissionsHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [PHGSiteCommissionsHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [HHASiteCommissionsHotelCurrency]
	,SUM(CASE WHEN items.ITMCLSCD = 'RES-TMP' THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [surchargesHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD = 'RES-TMP' THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [PHGSiteSurchargesHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD = 'RES-TMP' THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [HHASiteSurchargesHotelCurrency]
	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInHotelCurrency ELSE 0 END) 
		+ SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [BookAndCommHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInHotelCurrency ELSE 0 END) 
		+ SUM(CASE WHEN gpSiteID = '01' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [PHGSiteBookAndCommHotelCurrency]
	,SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID NOT IN (2,5) THEN ch.chargeValueInHotelCurrency ELSE 0 END) 
		+ SUM(CASE WHEN gpSiteID = '06' AND items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 2 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [HHASiteBookAndCommHotelCurrency]
	,SUM(CASE WHEN items.ITMCLSCD != 'RES-TMP' AND ch.classificationID = 5 THEN ch.chargeValueInHotelCurrency ELSE 0 END) as [IPreferChargesHotelCurrency]

	,0 as [IPreferCost]

FROM ReservationBilling.dbo.Charges ch
	LEFT JOIN IC.dbo.IV00101 items ON ch.itemCode = items.ITEMNMBR
	LEFT JOIN Reservations.dbo.Transactions t ON ch.confirmationNumber = t.confirmationNumber
	LEFT JOIN Reservations.dbo.TransactionDetail td ON t.transactionDetailID = td.transactionDetailID

WHERE ch.billableDate BETWEEN @startDate AND @endDate

GROUP BY ch.confirmationNumber
	,t.confirmationNumber
	,ch.hotelCode
	,ch.invoiceDate
	,ch.sopNumber;


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[Populate_ProfitabilityReport]'
GO


ALTER PROCEDURE [rpt].[Populate_ProfitabilityReport]
	@startDate DATE ,
	@endDate   DATE
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	--build Reservations table
	----------------------------------------------------------------------
		EXEC rpt.ProfitabilityReport_Reservations @startDate,@endDate
	---------------------------------------------------------------------------------


	--build Billy charges table
	----------------------------------------------------------------------
		--reservations can be expected to have only one row per confirmation number,but the charges tables will have multiple charges per reservations
		--we need to flatten all charges down to one row per confirmation number for the final queries

		EXEC rpt.ProfitabilityReport_Billy @startDate,@endDate
	---------------------------------------------------------------------------------


	--build I Prefer charges table
	----------------------------------------------------------------------
		--reservations can be expected to have only one row per confirmation number,but the charges tables will have multiple charges per reservations
		--we need to flatten all charges down to one row per confirmation number for the final queries

		EXEC rpt.ProfitabilityReport_IPrefer @startDate,@endDate
	---------------------------------------------------------------------------------

	------------All the above is just building the source tables,now we insert the venn diagram grouping of those tables into final temp table
		TRUNCATE TABLE Prof_Rpt.FinalResults;

		--insert all the records that have reservations
		EXEC rpt.ProfitabilityReport_FinalResult_WithRes


		--insert all the records that have billy records,but no reservation
		EXEC rpt.ProfitabilityReport_FinalResult_NoRes


		--insert all the records that have I Prefer records,but no billy or reservation
		EXEC rpt.ProfitabilityReport_FinalResult_NoBilly


		--put everything in the final real table
		EXEC rpt.ProfitabilityReport_FINAL
	---------------------------------------------------------------------------------
END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityExtended]'
GO

ALTER procedure [rpt].[ProfitabilityExtended]
	@arrivalStart date
	,@arrivalEnd date
	,@hotelCodes nvarchar(max) = ''
	,@channels nvarchar(max) = ''
	,@rateCodes nvarchar(max) = ''
	,@iatas nvarchar(max) = ''
	,@iataGroups nvarchar(max) = ''

AS

BEGIN

	-- #iataGroups -----------------------------------------------
	IF OBJECT_ID('tempdb..#iataGroups') IS NOT NULL
		DROP TABLE #iataGroups
	CREATE TABLE #iataGroups(value nvarchar(2048))

	INSERT INTO #iataGroups(value)
	SELECT value FROM dbo.SplitString(@iataGroups, ',') 
	--------------------------------------------------------------

	-- #hotelCodes -----------------------------------------------
	IF OBJECT_ID('tempdb..#hotelCodes') IS NOT NULL
		DROP TABLE #hotelCodes
	CREATE TABLE #hotelCodes(value nvarchar(2048))

	INSERT INTO #hotelCodes(value)
	SELECT value FROM dbo.SplitString(@hotelCodes, ',') 
	--------------------------------------------------------------

	-- #channels -----------------------------------------------
	IF OBJECT_ID('tempdb..#channels') IS NOT NULL
		DROP TABLE #channels
	CREATE TABLE #channels(value nvarchar(2048))

	INSERT INTO #channels(value)
	SELECT value FROM dbo.SplitString(@channels, ',') 
	--------------------------------------------------------------

	-- #rateCodes -----------------------------------------------
	IF OBJECT_ID('tempdb..#rateCodes') IS NOT NULL
		DROP TABLE #rateCodes
	CREATE TABLE #rateCodes(value nvarchar(2048))

	INSERT INTO #rateCodes(value)
	SELECT value FROM dbo.SplitString(@rateCodes, ',') 
	--------------------------------------------------------------

	-- #iatas -----------------------------------------------
	IF OBJECT_ID('tempdb..#iatas') IS NOT NULL
		DROP TABLE #iatas
	CREATE TABLE #iatas(value nvarchar(2048))

	INSERT INTO #iatas(value)
	SELECT value FROM dbo.SplitString(@iatas, ',') 
	--------------------------------------------------------------

;WITH iataGroupMatches AS
	(
		--get all distinct profitability record ids where one of the iata groups is present
		SELECT DISTINCT pr.id
		FROM rpt.[ProfitabilityReport] pr
			INNER JOIN #iataGroups i
				ON pr.IATAGroups LIKE '%' + i.value + '%'
		WHERE DATEFROMPARTS(ArrivalYear, ArrivalMonth, 1) BETWEEN @arrivalStart AND @arrivalEnd
			AND (@hotelCodes = '' OR hotelCode IN (SELECT value FROM #hotelCodes))
			AND (@channels = '' OR channel IN (SELECT value FROM #channels))
			AND (@rateCodes = '' OR rateTypeCode IN (SELECT value FROM #rateCodes))
			AND (@iatas = '' OR IATANumber IN (SELECT value FROM #iatas))
	)
SELECT [ConfirmationYear]
      ,[ConfirmationMonth]
	  ,[ArrivalYear]
      ,[ArrivalMonth]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[IataGam]
      ,[IATAGroups]
      ,[IATANumber]
      ,[travelAgencyName]
      ,[travelAgencyCity]
      ,[travelAgencyState]
      ,[travelAgencyCountry]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[ratename]
      ,[rateCategory_PHG]
	  ,[hotelCurrency]

      ,SUM([ConfirmedSynxisBookingCount]) AS [ConfirmedSynxisBookingCount]
      ,SUM([ConfirmedRevenueUSD]) AS [ConfirmedRevenueUSD]
      ,SUM([ConfirmedRevenueHotelCurrency]) AS [ConfirmedRevenueHotelCurrency]
      ,SUM([ConfirmedRoomNights]) AS [ConfirmedRoomNights]
	  ,SUM([ConfirmedRevenueUSD]) / SUM([ConfirmedRoomNights]) AS [avgNightlyRateUSD]
	  ,SUM([ConfirmedRoomNights]) / SUM([ConfirmedSynxisBookingCount]) AS [avgLengthOfStay]
      ,SUM([ConfirmedTotalCost]) AS [ConfirmedTotalCost]
      ,SUM(ConfirmedTotalCost) / SUM(ConfirmedSynxisBookingCount) AS [costperbooking]      
      
      ,SUM([BillyBookingCount]) AS [BillyBookingCount]
      ,SUM([PHGSiteBillyBookingCount]) AS [PHGSiteBillyBookingCount]
      ,SUM([HHASiteBillyBookingCount]) AS [HHASiteBillyBookingCount]
      ,SUM([BillyBookingCharges]) AS [BillyBookingCharges]
      ,SUM([PHGSiteBillyBookingCharges]) AS [PHGSiteBillyBookingCharges]
      ,SUM([HHASiteBillyBookingCharges]) AS [HHASiteBillyBookingCharges]

      ,SUM([commissions]) AS [commissions]
      ,SUM([PHGSiteCommissions]) AS [PHGSiteCommissions]
      ,SUM([HHASiteCommissions]) AS [HHASiteCommissions]
      ,SUM([surcharges]) AS [surcharges]
      ,SUM([PHGSiteSurcharges]) AS [PHGSiteSurcharges]
      ,SUM([HHASiteSurcharges]) AS [HHASiteSurcharges]
      ,SUM([BookAndComm]) AS [BookAndComm]
      ,SUM([BookAndCommHotelCurrency]) AS [BookAndCommHotelCurrency]
      ,SUM([PHGSiteBookAndComm]) AS [PHGSiteBookAndComm]
      ,SUM([HHASiteBookAndComm]) AS [HHASiteBookAndComm]

      ,SUM([IPreferBookingCount]) AS [IPreferBookingCount]
      ,SUM([IPreferCharges]) AS [IPreferCharges]
      ,SUM([IPreferCost]) AS [IPreferCost]
      ,SUM([BillyBookingWithZeroBookAndComm]) AS [BillyBookingWithZeroBookAndComm]

  FROM rpt.[ProfitabilityReport] pr
  LEFT OUTER JOIN iataGroupMatches igm
	ON pr.id = igm.id

  WHERE DATEFROMPARTS(ArrivalYear, ArrivalMonth, 1) BETWEEN @arrivalStart AND @arrivalEnd
	AND (@hotelCodes = '' OR hotelCode IN (SELECT value FROM #hotelCodes))
	AND (@channels = '' OR channel IN (SELECT value FROM #channels))
	AND (@rateCodes = '' OR rateTypeCode IN (SELECT value FROM #rateCodes))
	AND (@iatas = '' OR IATANumber IN (SELECT value FROM #iatas))
	AND (@iataGroups = '' OR igm.id IS NOT NULL)

  GROUP BY  [ConfirmationYear]
      ,[ConfirmationMonth]
	  ,[ArrivalYear]
      ,[ArrivalMonth]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[IataGam]
      ,[IATAGroups]
      ,[IATANumber]
      ,[travelAgencyName]
      ,[travelAgencyCity]
      ,[travelAgencyState]
      ,[travelAgencyCountry]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[ratename]
      ,[rateCategory_PHG]
	  ,[hotelCurrency]

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityStandard]'
GO

ALTER procedure [rpt].[ProfitabilityStandard]
	@arrivalStart date
	,@arrivalEnd date
	,@hotelCodes nvarchar(max) = ''
	,@channels nvarchar(max) = ''
AS

BEGIN
	-- #hotelCodes -----------------------------------------------
	IF OBJECT_ID('tempdb..#hotelCodes') IS NOT NULL
		DROP TABLE #hotelCodes
	CREATE TABLE #hotelCodes(value nvarchar(2048))

	INSERT INTO #hotelCodes(value)
	SELECT value FROM dbo.SplitString(@hotelCodes, ',') 
	--------------------------------------------------------------

	-- #channels -----------------------------------------------
	IF OBJECT_ID('tempdb..#channels') IS NOT NULL
		DROP TABLE #channels
	CREATE TABLE #channels(value nvarchar(2048))

	INSERT INTO #channels(value)
	SELECT value FROM dbo.SplitString(@channels, ',') 
	--------------------------------------------------------------

SELECT [ArrivalYear]
      ,[ArrivalMonth]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[hotelCurrency]

      ,SUM([ConfirmedSynxisBookingCount]) AS [ConfirmedSynxisBookingCount]
      ,SUM([ConfirmedRevenueUSD]) AS [ConfirmedRevenueUSD]
      ,SUM([ConfirmedRevenueHotelCurrency]) AS [ConfirmedRevenueHotelCurrency]
      ,SUM([ConfirmedRoomNights]) AS [ConfirmedRoomNights]
	  ,SUM([ConfirmedRevenueUSD]) / SUM([ConfirmedRoomNights]) AS [avgNightlyRateUSD]
	  ,SUM([ConfirmedRoomNights]) / SUM([ConfirmedSynxisBookingCount]) AS [avgLengthOfStay]
      ,SUM([ConfirmedTotalCost]) AS [ConfirmedTotalCost]
      ,SUM(ConfirmedTotalCost) / SUM(ConfirmedSynxisBookingCount) AS [costperbooking]      
      
      ,SUM([BillyBookingCount]) AS [BillyBookingCount]
      ,SUM([PHGSiteBillyBookingCount]) AS [PHGSiteBillyBookingCount]
      ,SUM([HHASiteBillyBookingCount]) AS [HHASiteBillyBookingCount]
      ,SUM([BillyBookingCharges]) AS [BillyBookingCharges]
      ,SUM([PHGSiteBillyBookingCharges]) AS [PHGSiteBillyBookingCharges]
      ,SUM([HHASiteBillyBookingCharges]) AS [HHASiteBillyBookingCharges]

      ,SUM([commissions]) AS [commissions]
      ,SUM([PHGSiteCommissions]) AS [PHGSiteCommissions]
      ,SUM([HHASiteCommissions]) AS [HHASiteCommissions]
      ,SUM([surcharges]) AS [surcharges]
      ,SUM([PHGSiteSurcharges]) AS [PHGSiteSurcharges]
      ,SUM([HHASiteSurcharges]) AS [HHASiteSurcharges]
      ,SUM([BookAndComm]) AS [BookAndComm]
      ,SUM([BookAndCommHotelCurrency]) AS [BookAndCommHotelCurrency]
      ,SUM([PHGSiteBookAndComm]) AS [PHGSiteBookAndComm]
      ,SUM([HHASiteBookAndComm]) AS [HHASiteBookAndComm]

      ,SUM([IPreferBookingCount]) AS [IPreferBookingCount]
      ,SUM([IPreferCharges]) AS [IPreferCharges]
      ,SUM([IPreferCost]) AS [IPreferCost]
      ,SUM([BillyBookingWithZeroBookAndComm]) AS [BillyBookingWithZeroBookAndComm]

  FROM  rpt.[ProfitabilityReport]

  WHERE DATEFROMPARTS(ArrivalYear, ArrivalMonth, 1) BETWEEN @arrivalStart AND @arrivalEnd
	AND (@hotelCodes = '' OR hotelCode IN (SELECT value FROM #hotelCodes))
	AND (@channels = '' OR channel IN (SELECT value FROM #channels))

  GROUP BY [ArrivalYear]
      ,[ArrivalMonth]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[hotelCurrency]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
