/*
Run this script on:

        chi-sq-st-01\warehouse.BillingRewrite    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.BillingRewrite

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 1/9/2018 9:38:06 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] DROP CONSTRAINT [PK_work_MrtForCalculation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [work].[MrtForCalc_RulesApplied]'
GO
ALTER TABLE [work].[MrtForCalc_RulesApplied] DROP CONSTRAINT [PK_MrtForCalc_RulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[BillyCalcErrors]'
GO
DROP TABLE [dbo].[BillyCalcErrors]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [work].[run_LoadWorkTables]'
GO
DROP PROCEDURE [work].[run_LoadWorkTables]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [work].[Populate_MrtForCalc_RulesApplied]'
GO
DROP PROCEDURE [work].[Populate_MrtForCalc_RulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [work].[MrtForCalc_RulesApplied]'
GO
DROP TABLE [work].[MrtForCalc_RulesApplied]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] ADD
[id] [int] NOT NULL IDENTITY(1, 1),
[runID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MrtForCalculation] on [work].[MrtForCalculation]'
GO
ALTER TABLE [work].[MrtForCalculation] ADD CONSTRAINT [PK_MrtForCalculation] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UQIX_MrtForCalculation] on [work].[MrtForCalculation]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQIX_MrtForCalculation] ON [work].[MrtForCalculation] ([runID], [confirmationNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_GetTable_MrtForCalculation]'
GO

CREATE FUNCTION [dbo].[fnc_GetTable_MrtForCalculation]
(
	@IsTest bit = 0
)
RETURNS 
@MRT TABLE 
(
	[confirmationNumber] [nvarchar](20) NOT NULL,
	[phgHotelCode] [nvarchar](50) NULL,
	[crsHotelID] [int] NULL,
	[hotelName] [nvarchar](250) NULL,
	[mainBrandCode] [nvarchar](6) NULL,
	[gpSiteID] [char](2) NULL,
	[chainID] [int] NULL,
	[chainName] [nvarchar](60) NULL,
	[bookingStatus] [nvarchar](10) NULL,
	[synxisBillingDescription] [nvarchar](255) NULL,
	[bookingChannel] [nvarchar](50) NULL,
	[bookingSecondarySource] [nvarchar](50) NULL,
	[bookingSubSourceCode] [nvarchar](100) NULL,
	[bookingTemplateGroupID] [int] NULL,
	[bookingTemplateAbbreviation] [nvarchar](25) NULL,
	[xbeTemplateName] [nvarchar](50) NULL,
	[CROcode] [nvarchar](20) NULL,
	[bookingCroGroupID] [int] NULL,
	[bookingRateCategoryCode] [nvarchar](20) NULL,
	[bookingRateCode] [nvarchar](25) NULL,
	[bookingIATA] [nvarchar](20) NULL,
	[transactionTimestamp] [datetime] NULL,
	[confirmationDate] [date] NULL,
	[arrivalDate] [date] NULL,
	[departureDate] [date] NULL,
	[cancellationDate] [date] NULL,
	[cancellationNumber] [nvarchar](20) NULL,
	[nights] [int] NULL,
	[rooms] [int] NULL,
	[roomNights] [int] NULL,
	[roomRevenueInBookingCurrency] [decimal](18,2) NULL,
	[bookingCurrencyCode] [nvarchar](12) NULL,
	[timeLoaded] [datetime] NULL,
	[CRSSourceID] [smallint] NULL,
	[ItemCode] [nvarchar](50) NULL,
	[exchangeDate] [date] NULL,
	[hotelCurrencyCode] [char](15) NULL,
	[hotelCurrencyDecimalPlaces] [smallint] NULL,
	[hotelCurrencyExchangeRate] [numeric](19,7) NULL,
	[bookingCurrencyExchangeRate] [numeric](19,7) NULL,
	[loyaltyProgram] [nvarchar](50) NULL,
	[loyaltyNumber] [nvarchar](50) NULL,
	[travelAgencyName] [nvarchar](450) NULL,
	[LoyaltyNumberValidated] [bit] NULL,
	[LoyaltyNumberTagged] [bit] NULL,
	PRIMARY KEY CLUSTERED([confirmationNumber])
)
AS
BEGIN
	IF @IsTest = 0
		INSERT INTO @MRT(confirmationNumber,phgHotelCode,crsHotelID,hotelName,mainBrandCode,gpSiteID,chainID,chainName,bookingStatus,synxisBillingDescription,bookingChannel,bookingSecondarySource,bookingSubSourceCode,bookingTemplateGroupID,bookingTemplateAbbreviation,xbeTemplateName,CROcode,bookingCroGroupID,bookingRateCategoryCode,bookingRateCode,bookingIATA,transactionTimestamp,confirmationDate,arrivalDate,departureDate,cancellationDate,cancellationNumber,nights,rooms,roomNights,roomRevenueInBookingCurrency,bookingCurrencyCode,timeLoaded,CRSSourceID,ItemCode,exchangeDate,hotelCurrencyCode,hotelCurrencyDecimalPlaces,hotelCurrencyExchangeRate,bookingCurrencyExchangeRate,loyaltyProgram,loyaltyNumber,travelAgencyName,LoyaltyNumberValidated,LoyaltyNumberTagged)
		SELECT confirmationNumber,phgHotelCode,crsHotelID,hotelName,mainBrandCode,gpSiteID,chainID,chainName,bookingStatus,synxisBillingDescription,bookingChannel,bookingSecondarySource,bookingSubSourceCode,bookingTemplateGroupID,bookingTemplateAbbreviation,xbeTemplateName,CROcode,bookingCroGroupID,bookingRateCategoryCode,bookingRateCode,bookingIATA,transactionTimestamp,confirmationDate,arrivalDate,departureDate,cancellationDate,cancellationNumber,nights,rooms,roomNights,roomRevenueInBookingCurrency,bookingCurrencyCode,timeLoaded,CRSSourceID,ItemCode,exchangeDate,hotelCurrencyCode,hotelCurrencyDecimalPlaces,hotelCurrencyExchangeRate,bookingCurrencyExchangeRate,loyaltyProgram,loyaltyNumber,travelAgencyName,LoyaltyNumberValidated,LoyaltyNumberTagged
		FROM [work].[MrtForCalculation]
	ELSE
		INSERT INTO @MRT(confirmationNumber,phgHotelCode,crsHotelID,hotelName,mainBrandCode,gpSiteID,chainID,chainName,bookingStatus,synxisBillingDescription,bookingChannel,bookingSecondarySource,bookingSubSourceCode,bookingTemplateGroupID,bookingTemplateAbbreviation,xbeTemplateName,CROcode,bookingCroGroupID,bookingRateCategoryCode,bookingRateCode,bookingIATA,transactionTimestamp,confirmationDate,arrivalDate,departureDate,cancellationDate,cancellationNumber,nights,rooms,roomNights,roomRevenueInBookingCurrency,bookingCurrencyCode,timeLoaded,CRSSourceID,ItemCode,exchangeDate,hotelCurrencyCode,hotelCurrencyDecimalPlaces,hotelCurrencyExchangeRate,bookingCurrencyExchangeRate,loyaltyProgram,loyaltyNumber,travelAgencyName,LoyaltyNumberValidated,LoyaltyNumberTagged)
		SELECT confirmationNumber,phgHotelCode,crsHotelID,hotelName,mainBrandCode,gpSiteID,chainID,chainName,bookingStatus,synxisBillingDescription,bookingChannel,bookingSecondarySource,bookingSubSourceCode,bookingTemplateGroupID,bookingTemplateAbbreviation,xbeTemplateName,CROcode,bookingCroGroupID,bookingRateCategoryCode,bookingRateCode,bookingIATA,transactionTimestamp,confirmationDate,arrivalDate,departureDate,cancellationDate,cancellationNumber,nights,rooms,roomNights,roomRevenueInBookingCurrency,bookingCurrencyCode,timeLoaded,CRSSourceID,ItemCode,exchangeDate,hotelCurrencyCode,hotelCurrencyDecimalPlaces,hotelCurrencyExchangeRate,bookingCurrencyExchangeRate,loyaltyProgram,loyaltyNumber,travelAgencyName,LoyaltyNumberValidated,LoyaltyNumberTagged
		FROM [test].[MrtForCalculation]
		
	RETURN 
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_MrtForCalc_RulesApplied]'
GO

CREATE FUNCTION [dbo].[fnc_MrtForCalc_RulesApplied]
(
	@IsTest bit = 0
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT DISTINCT mrt.confirmationNumber,mrt.phgHotelCode,mrt.hotelName,
					GREEN.clauseID as greenClauseID,
					GREEN.clauseName as greenClauseName,
					GREEN.criteriaGroupIncludeID as greenCriteriaGroupID,
					GREEN.criteriaGroupIncludeName as greenCriteriaGroupName,
					GREEN.criteriaGroupIncludeCriteriaIncludeID as greenCriteriaID,
					GREEN.criteriaGroupIncludeCriteriaIncludeName as greenCriteriaName,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID as greenSourceID,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeName as greenSourceName,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode AS greenRateCategoryCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCode AS greenRateCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID AS greenTravelAgentGroupID

	FROM dbo.fnc_GetTable_MrtForCalculation(@IsTest) mrt

	--see if the IATA number on the reservation is part of any travel agent groups
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups GREEN_taitag ON mrt.bookingIATA = GREEN_taitag.travelAgentId AND mrt.confirmationDate BETWEEN GREEN_taitag.startDate AND GREEN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--match to clause by inclusions--CICG_CGIC_SGIS
	INNER JOIN vw_clausesFlattened GREEN ON mrt.phgHotelCode = GREEN.hotelCode --the clause is for the same hotel as the reservation
		--match the source template group to the reservation's xbe template
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID = 0 --included source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the included source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID = 0 --included source is using the wildcard group
			OR
			mrt.bookingCroGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the included source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel = '*' --included source is using the wildcard
			OR
			mrt.bookingChannel = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource = '*' --included source is using the wildcard
			OR
			mrt.bookingSecondarySource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource = '*' --included source is using the wildcard
			OR
			mrt.bookingSubSourceCode = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID = 0 --included criteria is using wildcard
				OR
				GREEN_taitag.travelAgentGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)
	--see if we match any sources excluded from the matched include criteria --CICG_CGIC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened RED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeID = RED.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			RED.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = RED.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			RED.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = RED.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			RED.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = RED.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			RED.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = RED.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			RED.SubSource = '*' -- source is using the wildcard
			OR
			mrt.bookingSubSourceCode = RED.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups ORANGE_taitag
		ON mrt.bookingIATA = ORANGE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN ORANGE_taitag.startDate AND ORANGE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria excluded from the criteria group we matched to
	LEFT OUTER JOIN vw_criteriaFlattened ORANGE --CICG_CGEC_SGIS
		ON GREEN.criteriaGroupIncludeCriteriaExcludeID = ORANGE.criteriaID 

		--the criteria was excluded at the time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate

		--match the source template group to the reservation's xbe template
		AND (ORANGE.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR mrt.bookingTemplateGroupID = ORANGE.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (ORANGE.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR mrt.bookingCroGroupID = ORANGE.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (ORANGE.sourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = ORANGE.sourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND (ORANGE.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR mrt.bookingSecondarySource = ORANGE.sourceIncludeSecondarySource --or the value matches exactly
		)
	 
		--match the source sub source to the res sub source
		AND (ORANGE.sourceIncludeSubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = ORANGE.sourceIncludeSubSource --or the value matches exactly
		) 

		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate

		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				ORANGE.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				ORANGE_taitag.travelAgentGroupID = ORANGE.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				ORANGE.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = ORANGE.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				ORANGE.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = ORANGE.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			ORANGE.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND ORANGE.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND ORANGE.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND ORANGE.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CICG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened INDIGO ON ORANGE.sourceExcludeID = INDIGO.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			INDIGO.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = INDIGO.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			INDIGO.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = INDIGO.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			INDIGO.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = INDIGO.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			INDIGO.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = INDIGO.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND (INDIGO.SubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = INDIGO.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BLUE_taitag
		ON mrt.bookingIATA = BLUE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BLUE_taitag.startDate AND BLUE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria groups excluded from our matched clause
	LEFT OUTER JOIN vw_criteriaGroupsFlattened BLUE --CECG_CGIC_SGIS
		ON GREEN.criteriaGroupExcludeID = BLUE.criteriaGroupID

		--the criteria group was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate


		--match the source template group to the reservation's xbe template
		AND (BLUE.criteriaIncludeSourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR mrt.bookingTemplateGroupID = BLUE.criteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (BLUE.criteriaIncludeSourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR mrt.bookingCroGroupID = BLUE.criteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (BLUE.criteriaIncludeSourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = BLUE.criteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BLUE.criteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BLUE.criteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BLUE.criteriaIncludeTravelAgentGroupID = 0 --criteria is using wildcard
				OR
				BLUE_taitag.travelAgentGroupID = BLUE.criteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BLUE.criteriaIncludeRateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BLUE.criteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BLUE.criteriaIncludeRateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BLUE.criteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BLUE.criteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BLUE.criteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the matched criteria group exclusion --CECG_CGIC_SGIS
	LEFT OUTER JOIN vw_sourcesFlattened YELLOW ON BLUE.criteriaIncludeSourceExcludeID = YELLOW.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			YELLOW.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = YELLOW.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			YELLOW.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = YELLOW.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			YELLOW.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = YELLOW.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			YELLOW.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = YELLOW.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			YELLOW.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = YELLOW.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BROWN_taitag ON mrt.bookingIATA = BROWN_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BROWN_taitag.startDate AND BROWN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match a criteria excluded from the criteria group exclusion --CECG_CGEC_SGIS
	LEFT OUTER JOIN vw_criteriaFlattened BROWN ON BLUE.criteriaExcludeID = BROWN.criteriaID 
		--the criteria was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			BROWN.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = BROWN.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			BROWN.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = BROWN.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			BROWN.sourceIncludeChannel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = BROWN.sourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			BROWN.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BROWN.sourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BROWN.sourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BROWN.sourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BROWN.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				BROWN_taitag.travelAgentGroupID = BROWN.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BROWN.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BROWN.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BROWN.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BROWN.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BROWN.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BROWN.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BROWN.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BROWN.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CECG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened VIOLET ON BROWN.sourceExcludeID = VIOLET.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			VIOLET.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = VIOLET.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			VIOLET.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = VIOLET.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			VIOLET.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = VIOLET.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			VIOLET.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = VIOLET.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			VIOLET.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = VIOLET.SubSource --or the value matches exactly
		) 

	WHERE 
	(
		(
			GREEN.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
			AND RED.sourceId IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
		)
		AND 
		(
			ORANGE.sourceIncludeID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
			OR INDIGO.sourceId IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
		)
	)
	AND
	(
		(
			BLUE.criteriaIncludeSourceIncludeID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
			OR YELLOW.sourceId IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
		)
		OR
		(
			BROWN.sourceIncludeID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
			AND VIOLET.sourceId IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
		)
	)
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[Populate_LocalMasterTables]'
GO


CREATE PROCEDURE [work].[Populate_LocalMasterTables]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @startDate DATE = DATEADD(YEAR,-3,GETDATE())
		,@endDate DATE = GETDATE()

	
	-- POPULATE [work].[local_exchange_rates] -----------------------------------
	TRUNCATE TABLE [work].[local_exchange_rates];

	--break up the date ranges in GP exchange rates into individual dates so we don't have to do timespans for currency conversions
	DECLARE @i int = 0
		, @firstCurrencyDate date = DATEADD(YEAR,-1,@startDate)
	DECLARE @end int = DATEDIFF(DAY,@firstCurrencyDate,@endDate) + 1


		WHILE (@i < @end) 
		BEGIN
			INSERT INTO work.local_exchange_rates(CURNCYID, EXCHDATE, XCHGRATE)
			SELECT 'USD' as currencyCode, DATEADD(day, @i, @startdate) as exchangeDate, 1 as exchangeRate
			UNION
			SELECT CURNCYID, DATEADD(day, @i, @startdate), XCHGRATE
			FROM DYNAMICS.dbo.MC00100
			WHERE
				DATEADD(day, @i, @startdate) BETWEEN EXCHDATE AND EXPNDATE 
				AND EXGTBLID LIKE '%AVG%'
				AND CURNCYID != 'USD'		

			SET @i = @i + 1
		END
	-----------------------------------------------------------------------------

	-- POPULATE GREAT PLAINS WORKING TABLES -------------------------------------
	TRUNCATE TABLE [work].[GPCustomerTable];

	INSERT INTO [work].[GPCustomerTable](CUSTNMBR,CURNCYID)
	SELECT CUSTNMBR,CURNCYID
	FROM IC.dbo.RM00101


	TRUNCATE TABLE [work].[GPCurrencyMaster];

	INSERT INTO [work].[GPCurrencyMaster](CURNCYID,DECPLCUR)
	SELECT CURNCYID,DECPLCUR
	FROM DYNAMICS.dbo.MC40200
	-----------------------------------------------------------------------------

	-- UPDATE [Core].[dbo].[hotels_OpenHospitality] -----------------------------
		--This updates our Open Hospitality to Core hotel code translation, a bad mapping can cause errors during calculation
	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitality]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);

	INSERT INTO [Core].[dbo].[hotels_OpenHospitality]
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitalityPHG]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality]);  
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelActiveBrands] ---------------------------------
	TRUNCATE TABLE [work].[hotelActiveBrands];

	INSERT INTO [work].[hotelActiveBrands](hotelCode,mainHeirarchy)
	SELECT hb.hotelCode,MIN(b.heirarchy) AS mainHeirarchy
	FROM Core.dbo.hotels_brands hb
		INNER JOIN Core.dbo.brands AS b ON hb.brandCode = b.code
	WHERE (hb.endDatetime IS NULL)
		AND (hb.startDatetime <= GETDATE())
		OR (GETDATE() BETWEEN hb.startDatetime AND hb.endDatetime)
	GROUP BY hb.hotelCode;
	------------------------------------------------------------------------

	-- POPULATE [work].[hotelInactiveBrands] -------------------------------
	TRUNCATE TABLE [work].[hotelInactiveBrands];

	INSERT INTO [work].[hotelInactiveBrands](hotelCode,mainHeirarchy)
	SELECT hotelCode,MIN(heirarchy) AS mainHeirarchy
	FROM
		(
			SELECT hb.hotelCode,hb.brandCode,b.heirarchy
			FROM Core.dbo.hotels_brands AS hb
				INNER JOIN
					(
						SELECT hotelCode,MAX(endDatetime) AS maxEnd
						FROM Core.dbo.hotels_brands
						GROUP BY hotelCode
					) AS maxDate ON hb.endDatetime = maxDate.maxEnd AND hb.hotelCode = maxDate.hotelCode
				INNER JOIN Core.dbo.brands b ON hb.brandCode = b.code
			GROUP BY hb.hotelCode,hb.brandCode,b.heirarchy
		) AS maxBrands
	GROUP BY hotelCode;
	------------------------------------------------------------------------

END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[MrtForCalc_Clauses]'
GO
CREATE TABLE [work].[MrtForCalc_Clauses]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[runID] [int] NULL,
[confirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[phgHotelCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClauseID] [int] NULL,
[ClauseName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaGroupID] [int] NULL,
[CriteriaGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CriteriaID] [int] NULL,
[CriteriaName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceGroupID] [int] NULL,
[SourceGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceID] [int] NULL,
[SourceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCategoryCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RateCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TravelAgentGroupID] [int] NULL,
[TravelAgentGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[excludedCriteriaGroupID] [int] NULL,
[excludedCriteriaGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[excludedCriteriaID] [int] NULL,
[excludedCriteriaName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[excludedSourceID] [int] NULL,
[excludedSourceName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MrtForCalc_RulesApplied] on [work].[MrtForCalc_Clauses]'
GO
ALTER TABLE [work].[MrtForCalc_Clauses] ADD CONSTRAINT [PK_MrtForCalc_RulesApplied] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[Populate_MrtForCalc_Clauses]'
GO
CREATE PROCEDURE [work].[Populate_MrtForCalc_Clauses]
	@RunID int,
	@RunTypeID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	INSERT INTO work.MrtForCalc_Clauses(runID, confirmationNumber, phgHotelCode, hotelName
		, ClauseID, ClauseName, CriteriaGroupID, CriteriaGroupName, CriteriaID, CriteriaName
		, SourceGroupID, SourceGroupName, SourceID, SourceName, RateCategoryCode, RateCode, TravelAgentGroupID, TravelAgentGroupName
		, excludedCriteriaGroupID, excludedCriteriaGroupName
		, excludedCriteriaID, excludedCriteriaName
		, excludedSourceID, excludedSourceName)
	SELECT DISTINCT @runId, mrt.confirmationNumber,mrt.phgHotelCode,mrt.hotelName,
					GREEN.clauseID as greenClauseID,
					GREEN.clauseName as greenClauseName,
					GREEN.criteriaGroupIncludeID as greenCriteriaGroupID,
					GREEN.criteriaGroupIncludeName as greenCriteriaGroupName,
					GREEN.criteriaGroupIncludeCriteriaIncludeID as greenCriteriaID,
					GREEN.criteriaGroupIncludeCriteriaIncludeName as greenCriteriaName,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceGroupID as greenSourceGroupID,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceGroupName as greenSourceGroupName,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID as greenSourceID,
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeName as greenSourceName,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode as greenRateCategoryCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeRateCode as greenRateCode,
					GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID as greenTravelAgentGroupID,
					GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupName as greenTravelAgentGroupName,
					BLUE.criteriaGroupID as blueCriteriaGroupID,
					BLUE.criteriaGroupName as blueCriteriaGroupName,
					ORANGE.criteriaID as orangeCriteriaID,
					ORANGE.criteriaName as orangeCriteriaName,
					RED.sourceID as redSourceID,
					RED.sourceName as redSourceName

	FROM [work].[MrtForCalculation] mrt

	--see if the IATA number on the reservation is part of any travel agent groups
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups GREEN_taitag ON mrt.bookingIATA = GREEN_taitag.travelAgentId AND mrt.confirmationDate BETWEEN GREEN_taitag.startDate AND GREEN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--match to clause by inclusions--CICG_CGIC_SGIS
	INNER JOIN vw_clausesFlattened GREEN ON mrt.phgHotelCode = GREEN.hotelCode --the clause is for the same hotel as the reservation
		--match the source template group to the reservation's xbe template
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID = 0 --included source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the included source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID = 0 --included source is using the wildcard group
			OR
			mrt.bookingCroGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the included source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel = '*' --included source is using the wildcard
			OR
			mrt.bookingChannel = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource = '*' --included source is using the wildcard
			OR
			mrt.bookingSecondarySource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource = '*' --included source is using the wildcard
			OR
			mrt.bookingSubSourceCode = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID = 0 --included criteria is using wildcard
				OR
				GREEN_taitag.travelAgentGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode = '*' --included criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)
	--see if we match any sources excluded from the matched include criteria --CICG_CGIC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened RED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeID = RED.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			RED.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = RED.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			RED.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = RED.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			RED.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = RED.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			RED.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = RED.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			RED.SubSource = '*' -- source is using the wildcard
			OR
			mrt.bookingSubSourceCode = RED.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups ORANGE_taitag
		ON mrt.bookingIATA = ORANGE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN ORANGE_taitag.startDate AND ORANGE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria excluded from the criteria group we matched to
	LEFT OUTER JOIN vw_criteriaFlattened ORANGE --CICG_CGEC_SGIS
		ON GREEN.criteriaGroupIncludeCriteriaExcludeID = ORANGE.criteriaID 

		--the criteria was excluded at the time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate

		--match the source template group to the reservation's xbe template
		AND (ORANGE.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR mrt.bookingTemplateGroupID = ORANGE.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (ORANGE.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR mrt.bookingCroGroupID = ORANGE.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (ORANGE.sourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = ORANGE.sourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND (ORANGE.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR mrt.bookingSecondarySource = ORANGE.sourceIncludeSecondarySource --or the value matches exactly
		)
	 
		--match the source sub source to the res sub source
		AND (ORANGE.sourceIncludeSubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = ORANGE.sourceIncludeSubSource --or the value matches exactly
		) 

		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate

		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				ORANGE.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				ORANGE_taitag.travelAgentGroupID = ORANGE.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				ORANGE.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = ORANGE.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				ORANGE.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = ORANGE.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			ORANGE.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND ORANGE.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND ORANGE.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND ORANGE.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CICG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened INDIGO ON ORANGE.sourceExcludeID = INDIGO.sourceId 
		--make sure the source was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			INDIGO.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = INDIGO.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			INDIGO.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = INDIGO.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			INDIGO.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = INDIGO.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			INDIGO.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = INDIGO.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND (INDIGO.SubSource = '*' --source is using the wildcard
			OR mrt.bookingSubSourceCode = INDIGO.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BLUE_taitag
		ON mrt.bookingIATA = BLUE_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BLUE_taitag.startDate AND BLUE_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match any criteria groups excluded from our matched clause
	LEFT OUTER JOIN vw_criteriaGroupsFlattened BLUE --CECG_CGIC_SGIS
		ON GREEN.criteriaGroupExcludeID = BLUE.criteriaGroupID

		--the criteria group was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate


		--match the source template group to the reservation's xbe template
		AND (BLUE.criteriaIncludeSourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR mrt.bookingTemplateGroupID = BLUE.criteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)

		--match the source cro group to the res cro code
		AND (BLUE.criteriaIncludeSourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR mrt.bookingCroGroupID = BLUE.criteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)

		--match the source channel to the res channel
		AND (BLUE.criteriaIncludeSourceIncludeChannel = '*' --source is using the wildcard
			OR mrt.bookingChannel = BLUE.criteriaIncludeSourceIncludeChannel --or the value matches exactly
		) 
	
		--match the source secondary source to the res secondary source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BLUE.criteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BLUE.criteriaIncludeSourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BLUE.criteriaIncludeSourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BLUE.criteriaIncludeTravelAgentGroupID = 0 --criteria is using wildcard
				OR
				BLUE_taitag.travelAgentGroupID = BLUE.criteriaIncludeTravelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BLUE.criteriaIncludeRateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BLUE.criteriaIncludeRateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BLUE.criteriaIncludeRateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BLUE.criteriaIncludeRateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BLUE.criteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BLUE.criteriaIncludeLoyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BLUE.criteriaIncludeLoyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the matched criteria group exclusion --CECG_CGIC_SGIS
	LEFT OUTER JOIN vw_sourcesFlattened YELLOW ON BLUE.criteriaIncludeSourceExcludeID = YELLOW.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			YELLOW.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = YELLOW.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			YELLOW.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = YELLOW.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			YELLOW.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = YELLOW.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			YELLOW.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = YELLOW.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			YELLOW.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = YELLOW.SubSource --or the value matches exactly
		) 

	--see if the IATA number on the reservation is part of any travel agent groups other than the one used in the any of the above joins
	LEFT OUTER JOIN Core.dbo.travelAgentIds_travelAgentGroups BROWN_taitag ON mrt.bookingIATA = BROWN_taitag.travelAgentId
		AND mrt.confirmationDate BETWEEN BROWN_taitag.startDate AND BROWN_taitag.endDate --travel agent participation is determined by confirmation date (the date the travel agent did their work)

	--see if we match a criteria excluded from the criteria group exclusion --CECG_CGEC_SGIS
	LEFT OUTER JOIN vw_criteriaFlattened BROWN ON BLUE.criteriaExcludeID = BROWN.criteriaID 
		--the criteria was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			BROWN.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = BROWN.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			BROWN.sourceIncludeCroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = BROWN.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			BROWN.sourceIncludeChannel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = BROWN.sourceIncludeChannel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			BROWN.sourceIncludeSecondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = BROWN.sourceIncludeSecondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			BROWN.sourceIncludeSubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = BROWN.sourceIncludeSubSource --or the value matches exactly
		) 
		--make sure the source matched was included at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
		--match criteria travel agent group to res travel agent group
		AND
		(
			(
				BROWN.travelAgentGroupID = 0 --criteria is using wildcard
				OR
				BROWN_taitag.travelAgentGroupID = BROWN.travelAgentGroupID --or the value matches exactly
			)
			--travel agent group was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
		)
		--match criteria rate code to res rate code
		AND
		(
			(
				BROWN.rateCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCode = BROWN.rateCode --or the value matches exactly
			)
			--rate code was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
		)
		--match criteria rate category to res rate category
		AND
		(
			(
				BROWN.rateCategoryCode = '*' --criteria is using wildcard
				OR
				mrt.bookingRateCategoryCode = BROWN.rateCategoryCode --or the value matches exactly
			)
			--rate category was included at time of arrival
			AND mrt.arrivalDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
		)
		--match criteria loyalty option to res loyalty option
		AND
		(
			BROWN.loyaltyOptionID = 0 --criteria using wildcard option
			OR (mrt.LoyaltyNumberValidated = 0 AND BROWN.loyaltyOptionID = 1) --explicitly no valid loyalty number
			OR (mrt.LoyaltyNumberValidated = 1 AND BROWN.loyaltyOptionID = 2) --explicitly does have a valid loyalty number
			OR (mrt.LoyaltyNumberTagged = 1 AND BROWN.loyaltyOptionID = 3) --explicitly a valid loyalty number that was tagged by the hotel
		)

	--see if we match any sources excluded from the excluded criteria --CECG_CGEC_SGES
	LEFT OUTER JOIN vw_sourcesFlattened VIOLET ON BROWN.sourceExcludeID = VIOLET.sourceId 
		--make sure the source matched was excluded at time of arrival
		AND mrt.arrivalDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
		--match the source template group to the reservation's xbe template
		AND
		(
			VIOLET.templateGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingTemplateGroupID = VIOLET.templateGroupID --we've determined the template is in the source's template group
		)
		--match the source cro group to the res cro code
		AND
		(
			VIOLET.CroGroupID = 0 --source is using the wildcard group
			OR
			mrt.bookingCroGroupID = VIOLET.croGroupID --we've determined the CRO Code is in the source's CRO Group
		)
		--match the source channel to the res channel
		AND
		(
			VIOLET.channel = '*' --source is using the wildcard
			OR
			mrt.bookingChannel = VIOLET.channel --or the value matches exactly
		) 
		--match the source secondary source to the res secondary source
		AND
		(
			VIOLET.secondarySource = '*' --source is using the wildcard
			OR
			mrt.bookingSecondarySource = VIOLET.secondarySource --or the value matches exactly
		)
		--match the source sub source to the res sub source
		AND
		(
			VIOLET.SubSource = '*' --source is using the wildcard
			OR
			mrt.bookingSubSourceCode = VIOLET.SubSource --or the value matches exactly
		) 

	WHERE 
	mrt.runID = @RunID
	AND
	(
		@RunTypeID = 0 --if its a test run, return all rows
		OR --else its a production run, only return non-excluded GREEN matches
		(
			(
				(
					GREEN.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
					AND RED.sourceId IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
				)
				AND 
				(
					ORANGE.sourceIncludeID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
					OR INDIGO.sourceId IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
				)
			)
			AND
			(
				(
					BLUE.criteriaIncludeSourceIncludeID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
					OR YELLOW.sourceId IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
				)
				OR
				(
					BROWN.sourceIncludeID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
					AND VIOLET.sourceId IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
				)
			)
		)
	)

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[Run]'
GO
ALTER TABLE [work].[Run] ADD
[RunType] [tinyint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [work].[Run] ADD
[RunType_Desc] AS (case [RunStatus] when (0) then 'Test' when (1) then 'Standard' when (2) then 'Threshold'  end)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[mrtJoined]'
GO



---------------------------------------------------------------------

---------------------------------------------------------------------
ALTER VIEW [work].[mrtJoined]
AS
     SELECT 
            mrt.confirmationNumber,
			COALESCE(SynxisHotels.code, OHHotels.Code) AS phgHotelCode,
            COALESCE(SynxisHotels.synxisID, OHHotels.openHospitalityCode) AS crsHotelID,
            COALESCE(synxisHotels.hotelName, OHHotels.Hotelname) AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            mrt.chainID,
            mrt.chainName,
            mrt.status AS bookingStatus,
            mrt.billingDescription AS synxisBillingDescription,
            mrt.channel AS bookingChannel,
            mrt.secondarySource AS bookingSecondarySource,
            mrt.subSourceCode AS bookingSubSourceCode,
            COALESCE(t.templateGroupID, 2) AS bookingTemplateGroupId,
            COALESCE(t.siteAbbreviation, 'HOTEL') AS bookingTemplateAbbreviation,
            mrt.xbeTemplateName,
            mrt.CROcode,
            CASE WHEN mrt.CROCode IS NULL THEN 0 ELSE COALESCE(croCodes.croGroupID, 2) END AS bookingCroGroupID,
            mrt.rateCategoryCode AS bookingRateCategoryCode,
            mrt.rateTypeCode AS bookingRateCode,
            mrt.IATANumber AS bookingIATA,
            mrt.transactionTimeStamp,
            mrt.confirmationDate,
            mrt.arrivalDate,
            mrt.departureDate,
            mrt.cancellationDate,
            mrt.cancellationNumber,
            mrt.nights,
            mrt.rooms,
            mrt.nights * mrt.rooms AS roomNights,
            mrt.reservationRevenue AS roomRevenueInBookingCurrency,
            mrt.currency AS bookingCurrencyCode,
            mrt.timeLoaded,
			work.[billyItemCode](mrt.billingDescription,mrt.channel,mrt.secondarySource,mrt.subSourceCode,t.siteAbbreviation,croCodes.croGroupID,mrt.chainID) AS [ItemCode],
			CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            mrt.CRSSourceID,
			mrt.loyaltyProgram,
			mrt.loyaltyNumber,
			mrt.travelAgencyName,
			ISNULL(mrt.LoyaltyNumberValidated,0) as LoyaltyNumberValidated,
			ISNULL(mrt.LoyaltyNumberTagged,0) as LoyaltyNumberTagged


     FROM Superset.dbo.mostrecenttransactions mrt
        LEFT JOIN Core.dbo.hotelsReporting synxisHotels ON synxisHotels.synxisID = mrt.hotelID
        LEFT JOIN Core.dbo.hotelsReporting OHHotels ON OHHotels.openhospitalityCode = mrt.OpenHospitalityID 
        LEFT JOIN work.hotelActiveBrands ON COALESCE(SynxisHotels.code, OHHotels.Code) = hotelActiveBrands.hotelCode 
        LEFT JOIN Core.dbo.brands activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.heirarchy
        LEFT JOIN work.hotelInactiveBrands ON COALESCE(SynxisHotels.code, OHHotels.Code) = hotelInactiveBrands.hotelCode
        LEFT JOIN Core.dbo.brands inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.heirarchy
        LEFT JOIN [dbo].[CROCodes] ON mrt.CROCode = croCodes.croCode
		LEFT JOIN dbo.Templates t ON t.xbeTemplateName = mrt.xbeTemplateName
		LEFT JOIN work.GPCustomerTable gpCustomer ON COALESCE(SynxisHotels.code, OHHotels.Code) = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON mrt.currency = bookingCE.CURNCYID 
			AND CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) = bookingCE.EXCHDATE


     WHERE (synxisHotels.code IS NULL OR synxisHotels.code NOT IN ('PHGTEST','BCTS4'))
	 AND (OHHotels.code IS NULL OR OHHotels.code NOT IN ('PHGTEST','BCTS4'))



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Charges]'
GO
ALTER TABLE [dbo].[Charges] ADD
[runID] [int] NULL,
[clauseID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [test].[Charges]'
GO
ALTER TABLE [test].[Charges] ADD
[runID] [int] NULL,
[clauseID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[Calculate_StandardCharges]'
GO

ALTER PROCEDURE [work].[Calculate_StandardCharges]
	@RunID int,
	@RunType int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE TEMP TABLE ---------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES;

	CREATE TABLE #CHARGES
	(
		[clauseID] [int] NOT NULL,
		[billingRuleID] [int] NOT NULL,
		[classificationID] [int] NOT NULL,
		[confirmationNumber] [nvarchar](20) NOT NULL,
		[hotelCode] [nvarchar](10) NOT NULL,
		[collectionCode] [nvarchar](6) NULL,
		[clauseName] [nvarchar](250) NULL,
		[arrivalDate] [date] NULL,
		[roomNights] [int] NULL,
		[hotelCurrencyCode] [char](3) NULL,
		[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
		[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
		[chargeValueInUSD] [decimal](38, 2) NULL,
		[roomRevenueInUSD] [decimal](38, 2) NULL,
		[itemCode] [nvarchar](50) NULL,
		[gpSiteID] [char](2) NULL,
		[dateCalculated] [datetime] NULL,
		[sopNumber] [char](21) NULL,
		[invoiceDate] [date] NULL,
	)
	------------------------------------------------------------------------------------

	
	INSERT INTO #CHARGES(clauseID,billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,
							clauseName,arrivalDate,roomNights,hotelCurrencyCode,
							chargeValueInHotelCurrency,
							roomRevenueInHotelCurrency,
							chargeValueInUSD,
							roomRevenueInUSD,
							itemCode,
							gpSiteID,
							dateCalculated,sopNumber,invoiceDate)
	SELECT clauses.clauseID,br.billingRuleID,br.classificationID,mrtC.confirmationNumber,mrtC.phgHotelCode,mrtC.mainBrandCode AS collectionCode,
			clauses.ClauseName,mrtC.arrivalDate,mrtC.roomNights,mrtC.hotelCurrencyCode,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) *  mrtC.hotelCurrencyExchangeRate, mrtC.hotelCurrencyDecimalPlaces-1
			) AS chargeValueInHotelCurrency,
			ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * mrtC.hotelCurrencyExchangeRate,mrtC.hotelCurrencyDecimalPlaces-1) AS roomRevenueInHotelCurrency,
			ROUND
			(
				(
					(CASE --get percentage charge amount
						WHEN (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) <= br.percentageMinimum) THEN br.percentageMinimum
						WHEN (br.percentageMaximum > 0 AND (br.perReservationPercentage * 	((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE) >= br.percentageMaximum)) THEN br.percentageMaximum
						ELSE (br.perReservationPercentage * ((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * brCE.XCHGRATE))
					 END
					 --add flat fees
					 + br.perReservationFlatFee + (br.perRoomNightFlatFee * (mrtC.rooms * mrtC.nights))
				) / brCE.XCHGRATE) * 1.00, 2
		) AS chargeValueInUSD,
		ROUND((mrtC.roomRevenueInBookingCurrency / mrtC.bookingCurrencyExchangeRate) * 1.00, 2) AS roomRevenueInUSD,
		COALESCE
		(c.itemCodeOverride,CASE cl.classificationName
								WHEN 'Booking' THEN
								--if any of the non-source based criteria fields are anything other than the wildcard, this charge is a 'special booking'
									CASE 
										WHEN (clauses.RateCategoryCode <> '*' OR clauses.RateCode <> '*' OR clauses.TravelAgentGroupID <> 0)
										THEN mrtC.ItemCode + '_SB'
        								ELSE mrtC.ItemCode + '_B'
									END
								WHEN 'Commission' THEN mrtC.ItemCode + '_C'
								WHEN 'Surcharge' THEN mrtC.ItemCode + '_S'
								WHEN 'Non-Billable' THEN NULL
								ELSE NULL
							END
		) AS ItemCode,
		COALESCE(c.gpSiteOverride,mrtC.gpSiteID) AS gpSiteID,
		GETDATE(),NULL,NULL
	FROM BillingRules br
		INNER JOIN work.MrtForCalc_Clauses as clauses ON clauses.ClauseID = br.clauseID
		INNER JOIN Classifications cl ON cl.classificationID = br.classificationID
		INNER JOIN work.MrtForCalculation mrtC ON mrtC.confirmationNumber = clauses.confirmationNumber AND mrtc.runID = clauses.runID
		INNER JOIN work.[local_exchange_rates] brCE 
		ON br.currencyCode = brCE.CURNCYID 
			AND mrtC.exchangeDate = brCE.EXCHDATE
		INNER JOIN dbo.Criteria c ON c.criteriaID = clauses.CriteriaID
		LEFT JOIN dbo.ThresholdRules tr ON tr.clauseID = clauses.ClauseID
	WHERE clauses.runID = @RunID
		AND (
				mrtC.bookingStatus != 'Cancelled'
				OR
				(mrtC.bookingStatus = 'Cancelled' AND br.refundable = 0)
			)
		AND (
				(br.afterConfirmation = 1 AND mrtC.confirmationDate >= br.confirmationDate)
				OR
				(br.afterConfirmation = 0 AND mrtC.confirmationDate <= br.confirmationDate)
			)
		AND mrtC.arrivalDate BETWEEN br.startDate AND br.endDate
	------------------------------------------------------------------------------------

	IF @RunType = 0 --this is a test run
	BEGIN
		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO test.Charges(runID,clauseID,billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate)
		SELECT @runID,clauseID,billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate
		FROM #CHARGES
		------------------------------------------------------------------------------------
	END
	ELSE
		BEGIN
		-- DELETE EXISTING CHARGES ---------------------------------------------------------
		DELETE dbo.Charges
		WHERE confirmationNumber IN(SELECT confirmationNumber FROM work.MrtForCalculation)
		AND sopNumber IS NULL
		------------------------------------------------------------------------------------

		-- ADD NEW CHARGES------------------------------------------------------------------
		INSERT INTO dbo.Charges(runID,clauseID,billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate)
		SELECT @RunID,clauseID,billingRuleID,classificationID,confirmationNumber,hotelCode,collectionCode,clauseName,arrivalDate,roomNights,
							hotelCurrencyCode,chargeValueInHotelCurrency,roomRevenueInHotelCurrency,chargeValueInUSD,roomRevenueInUSD,
							itemCode,gpSiteID,dateCalculated,sopNumber,invoiceDate
		FROM #CHARGES
		------------------------------------------------------------------------------------
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[runCalculator]'
GO

CREATE PROCEDURE [work].[runCalculator]
	@runTypeID int = 1, --0=test, 1=standard, 2=threshold
	@startDate date = null,
	@endDate date = null,
	@hotelCode nvarchar(20) = null,
	@confirmationNumber nvarchar(20) = null
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @RunID int;

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-1,mrt.arrivalDate),
				@endDate = DATEADD(DAY,1,mrt.arrivalDate),
				@hotelCode = COALESCE(synxis.hotelCode,oh.hotelCode)
		FROM Superset.dbo.mostrecenttransactions mrt
			LEFT JOIN Core.dbo.hotels_Synxis synxis ON synxis.synxisID = mrt.hotelID
			LEFT JOIN Core.dbo.hotels_OpenHospitality oh ON oh.openHospitalityCode = mrt.OpenHospitalityID
		WHERE mrt.confirmationNumber = @confirmationNumber
	END

	IF(@startDate IS NULL AND @endDate IS NULL)
	BEGIN
		IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
		BEGIN
			SET @startDate = DATEADD (mm,-1,GETDATE())
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END
		ELSE
		BEGIN
			SET @startDate = GETDATE()
			SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
		END

		SET @endDate = DATEADD(YEAR,2,@startDate)
	END
	-----------------------------------------------------------------------------

	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunType,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),0,@runTypeID,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();
	-----------------------------------------------------------------------------

	-- POPULATE [work].[MrtForCalculation] ---------------------------------
	;WITH cte_invoiced
	AS (
		SELECT confirmationNumber,sopNumber 
		FROM dbo.Charges
		WHERE hotelCode = ISNULL(@hotelCode,hotelCode) --either we're running all hotels, or we're just getting a specific hotel
		AND confirmationNumber = ISNULL(@confirmationNumber,confirmationNumber) --either we're running all bookings, or just getting a specific conf#
		AND sopNumber IS NOT NULL
	) 
	INSERT INTO [work].[MrtForCalculation]([RunID],[confirmationNumber],[phgHotelCode],[crsHotelID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
			[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateGroupID],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
			[bookingCroGroupID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
			[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookingCurrency],bookingCurrencyCode,[timeLoaded],[CRSSourceID],
			[ItemCode],
			exchangeDate,
			hotelCurrencyCode,hotelCurrencyDecimalPlaces,hotelCurrencyExchangeRate,bookingCurrencyExchangeRate,loyaltyProgram,loyaltyNumber,[travelAgencyName],
			LoyaltyNumberValidated,LoyaltyNumberTagged)

	SELECT
		@RunID 
		,mrtj.[confirmationNumber]
		,mrtj.[phgHotelCode]
		,mrtj.[crsHotelID]
		,mrtj.[hotelName]
		,mrtj.[mainBrandCode]
		,mrtj.[gpSiteID]
		,mrtj.[chainID]
		,mrtj.[chainName]
		,mrtj.[bookingStatus]
		,mrtj.[synxisBillingDescription]
		,mrtj.[bookingChannel]
		,mrtj.[bookingSecondarySource]
		,mrtj.[bookingSubSourceCode]
		,mrtj.[bookingTemplateGroupID]
		,mrtj.[bookingTemplateAbbreviation]
		,mrtj.[xbeTemplateName]
		,mrtj.[CROcode]
		,mrtj.[bookingCroGroupID]
		,mrtj.[bookingRateCategoryCode]
		,mrtj.[bookingRateCode]
		,mrtj.[bookingIATA]
		,mrtj.[transactionTimeStamp]
		,mrtj.[confirmationDate]
		,mrtj.[arrivalDate]
		,mrtj.[departureDate]
		,mrtj.[cancellationDate]
		,mrtj.[cancellationNumber]
		,mrtj.[nights]
		,mrtj.[rooms]
		,mrtj.[roomNights]
		,mrtj.[roomRevenueInBookingCurrency]
		,mrtj.[bookingCurrencyCode]
		,mrtj.[timeLoaded]
		,mrtj.[CRSSourceID]
		,mrtj.[ItemCode]
		,mrtj.exchangeDate
		,mrtj.hotelCurrencyCode
		,mrtj.hotelCurrencyDecimalPlaces
		,mrtj.hotelCurrencyExchangeRate
		,mrtj.bookingCurrencyExchangeRate
		,mrtj.loyaltyProgram
		,mrtj.loyaltyNumber
		,mrtj.[travelAgencyName]
		,mrtj.LoyaltyNumberValidated
		,mrtj.LoyaltyNumberTagged
	FROM work.mrtJoined mrtj
		LEFT JOIN cte_invoiced inv ON inv.confirmationNumber = mrtj.confirmationNumber 
	WHERE mrtj.phgHotelCode = ISNULL(@hotelCode,mrtj.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
		AND mrtj.confirmationNumber = ISNULL(@confirmationNumber,mrtj.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
		AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
		AND inv.confirmationNumber IS NULL

	------------------------------------------------------------------------

	-- POPULATE MrtForCalc_Clauses ------------------------------------
	EXEC work.Populate_MrtForCalc_Clauses @RunID, @RunTypeID
	------------------------------------------------------------------------

	-- RUN standard calculation ------------------------------------
	EXEC work.Calculate_StandardCharges @RunID, @RunTypeID
	------------------------------------------------------------------------

	RETURN @runID
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [work].[truncateWorkTables]'
GO


CREATE PROCEDURE [work].[truncateWorkTables]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
  TRUNCATE TABLE work.MrtForCalculation;
  TRUNCATE TABLE work.MrtForCalc_Clauses;
  TRUNCATE TABLE test.Charges;
 
 END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [work].[TestReservationChargesByConfirmationNumber]'
GO

ALTER PROCEDURE [work].[TestReservationChargesByConfirmationNumber]
	@confirmation varchar(20)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

DECLARE	@RunID int

EXEC	@RunID = [work].[runCalculator]
		@runTypeID = 0, --test type
		@confirmationNumber = @confirmation

	SELECT cl.*
		,ch.*
	FROM [test].[Charges] ch
	JOIN work.MrtForCalc_Clauses cl
		ON ch.runID = cl.runID
		AND ch.clauseID = cl.clauseID
	WHERE ch.runID = @RunID
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[collision_Criteria]'
GO

ALTER PROCEDURE [dbo].[collision_Criteria]
	@hotelCode varchar(20) = NULL,
	@clauseID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT  DISTINCT  theClause.clauseID as theClauseID,theClause.clauseName as theClauseName,
					theClause.[criteriaGroupID] as theClauseCriteriaGroupID, theClause.[criteriaGroupName] as theClauseCriteriaGroupName,
					GREEN.hotelCode as collisionHotelCode,GREEN.clauseID as collisionClauseID,
					GREEN.clauseName as collisionClauseName,GREEN.criteriaGroupIncludeID as collisionCriteriaGroupID
					,GREEN.criteriaGroupIncludeName as collisionCriteriaGroupName
	FROM (SELECT *  FROM vw_GREEN WHERE clauseID = @clauseID) theClause
		--get the clause's billing rules
		INNER JOIN BillingRules br ON theClause.clauseID = br.clauseID

		--match to other clauses by inclusions only
		 --CICG_CGIC_SGIS
		INNER JOIN vw_clausesFlattened GREEN ON GREEN.hotelCode = @hotelCode --the clause is for the hotel we are checking
			--match the source template group to the clause xbe template
			AND (
					theClause.templateGroupID = 0 --clause is using the wildcard group
					OR
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID = 0 --or source is using the wildcard group
					OR
					theClause.templateGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeTemplateGroupID --or the value matches exactly
				)
			--match the source cro group to the clause cro code
			AND (
					theClause.croGroupID = 0 --clause is using the wildcard group
					OR
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID = 0 --included source is using the wildcard group
					OR
					theClause.croGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeCroGroupID --or the value matches exactly
				)
			--match the source channel to the clause channel
			AND (
					theClause.channel = '*' --clause is using the wildcard
					OR
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel = '*' --included source is using the wildcard
					OR
					theClause.channel = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					theClause.secondarySource = '*' --clause is using the wildcard
					OR
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource = '*' --included source is using the wildcard
					OR
					theClause.secondarySource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					theClause.subsource = '*' --clause is using the wildcard
					OR
					GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource = '*' --included source is using the wildcard
					OR
					theClause.subsource = GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched for a billing timeframe
			AND (
					br.startDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
					OR
					br.endDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeEndDate
				)
			--match criteria travel agent group to res travel agent group
			AND (
					(
						theClause.travelAgentGroupID =  0 --clause is using wildcard
						OR
						GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID = 0 --included criteria is using wildcard
						OR
						theClause.travelAgentGroupID = GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND (
							br.startDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
							OR
							br.endDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeTravelAgentGroupEndDate
						)
				)
			--match criteria rate code to res rate code
			AND (
					(
						theClause.rateCode = '*' --clause is using wildcard
						OR
						GREEN.criteriaGroupIncludeCriteriaIncludeRateCode = '*' --included criteria is using wildcard
						OR
						theClause.rateCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND (
							br.startDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
							OR
							br.endDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCodeEndDate
						)
				)
			--match criteria rate category to res rate category
			AND (
					(
						theClause.rateCategoryCode = '*' --clause is using wildcard
						OR
						GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode = '*' --included criteria is using wildcard
						OR
						theClause.rateCategoryCode = GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND (
							br.startDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
							OR
							br.endDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeRateCategoryEndDate
						)
				)
			--match criteria loyalty option to res loyalty option
			AND (
					theClause.loyaltyOptionID = 0 --clause is using wildcard
					OR
					GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
					OR
					theClause.loyaltyOptionID = GREEN.criteriaGroupIncludeCriteriaIncludeLoyaltyOptionID --or the value matches exactly
				)

		--can only be a collision if both matching clauses have overlapping billing rules
		INNER JOIN BillingRules AS GREENbr ON GREEN.clauseID = GREENbr.clauseID
			AND (
					br.startDate BETWEEN GREENbr.startDate AND GREENbr.endDate
					OR
					br.endDate BETWEEN GREENbr.startDate AND GREENbr.endDate
				)

		--see if we match any sources excluded from the matched include criteria
		 --CICG_CGIC_SGES
		LEFT JOIN vw_sourcesFlattened RED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeID = RED.sourceId 
			--make sure the source was excluded during billing timeframe
			AND (
					br.startDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
					OR
					br.endDate BETWEEN GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceExcludeEndDate
				)
			--match the source template group to the reservation's xbe template
			AND (
					RED.templateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = RED.templateGroupID --or the value matches exactly
				)
			--match the source cro group to the res cro code
			AND (
					RED.CroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = RED.croGroupID --or the value matches exactly
				)
			--match the source channel to the res channel
			AND (
					RED.channel = '*' --source is using the wildcard
					OR
					theClause.channel = RED.channel --or the value matches exactly
				) 
	
			--match the source secondary source to the res secondary source
			AND (
					RED.secondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = RED.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					RED.SubSource = '*' -- source is using the wildcard
					OR
					theClause.subsource = RED.SubSource --or the value matches exactly
				) 

		--see if we match any criteria excluded from the criteria group we matched to
		 --CICG_CGEC_SGIS
		LEFT OUTER JOIN vw_criteriaFlattened ORANGE ON GREEN.criteriaGroupIncludeCriteriaExcludeID = ORANGE.criteriaID 
			--the criteria was excluded during billing timeframe
			AND (
					br.startDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate
					OR
					br.endDate BETWEEN GREEN.criteriaGroupIncludeCriteriaExcludeStartDate AND GREEN.criteriaGroupIncludeCriteriaExcludeEndDate
				)
			--match the source template group to the reservation's xbe template
			AND (
					ORANGE.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = ORANGE.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					ORANGE.sourceIncludeCroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = ORANGE.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					ORANGE.sourceIncludeChannel = '*' --source is using the wildcard
					OR
					theClause.channel = ORANGE.sourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					ORANGE.sourceIncludeSecondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = ORANGE.sourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					ORANGE.sourceIncludeSubSource = '*' --source is using the wildcard
					OR
					theClause.subsource = ORANGE.sourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND (
					br.startDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate
					OR
					br.endDate BETWEEN ORANGE.sourceIncludeStartDate AND ORANGE.sourceIncludeEndDate
				)
			--match criteria travel agent group to res travel agent group
			AND (
					(
						ORANGE.travelAgentGroupID = 0 --criteria is using wildcard
						OR
						theClause.travelAgentGroupID = ORANGE.travelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND (
							br.startDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
							OR
							br.endDate BETWEEN ORANGE.travelAgentGroupStartDate AND ORANGE.travelAgentGroupEndDate
						)
				)
			--match criteria rate code to res rate code
			AND (
					(
						ORANGE.rateCode = '*' --criteria is using wildcard
						OR
						theClause.rateCode = ORANGE.rateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND (
							br.startDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
							OR
							br.endDate BETWEEN ORANGE.rateCodeStartDate AND ORANGE.rateCodeEndDate
						)
				)
			--match criteria rate category to res rate category
			AND (
					(
						ORANGE.rateCategoryCode = '*' --criteria is using wildcard
						OR
						theClause.rateCategoryCode = ORANGE.rateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND (
							br.startDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
							OR
							br.endDate BETWEEN ORANGE.rateCategoryStartDate AND ORANGE.rateCategoryEndDate
						)
				)

			--match criteria loyalty option to res loyalty option
			AND (
					ORANGE.loyaltyOptionID = 0 --criteria using wildcard option
					OR
					theClause.loyaltyOptionID = ORANGE.loyaltyOptionID --or value matches exactly
				)

		--see if we match any sources excluded from the excluded criteria
		 --CICG_CGEC_SGES
		LEFT JOIN vw_sourcesFlattened INDIGO ON ORANGE.sourceExcludeID = INDIGO.sourceId 
			--make sure the source was excluded at time of arrival
			AND (
					br.startDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
					OR
					br.endDate BETWEEN ORANGE.sourceExcludeStartDate AND ORANGE.sourceExcludeEndDate
				)
			--match the source template group to the reservation's xbe template
			AND (
					INDIGO.templateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = INDIGO.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					INDIGO.CroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = INDIGO.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					INDIGO.channel = '*' --source is using the wildcard
					OR
					theClause.channel = INDIGO.channel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					INDIGO.secondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = INDIGO.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					INDIGO.SubSource = '*' --source is using the wildcard
					OR
					theClause.subsource = INDIGO.SubSource --or the value matches exactly
				) 

		--see if we match any criteria groups excluded from our matched clause
		 --CECG_CGIC_SGIS
		LEFT OUTER JOIN vw_criteriaGroupsFlattened BLUE ON GREEN.criteriaGroupExcludeID = BLUE.criteriaGroupID
			--the criteria group was excluded at time of arrival
			AND (
					br.startDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate
					OR
					br.endDate BETWEEN GREEN.criteriaGroupExcludeStartDate AND GREEN.criteriaGroupExcludeEndDate
				)
			--match the source template group to the reservation's xbe template
			AND (
					BLUE.criteriaIncludeSourceIncludeTemplateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = BLUE.criteriaIncludeSourceIncludeTemplateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					BLUE.criteriaIncludeSourceIncludeCroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = BLUE.criteriaIncludeSourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					BLUE.criteriaIncludeSourceIncludeChannel = '*' --source is using the wildcard
					OR
					theClause.channel = BLUE.criteriaIncludeSourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					BLUE.criteriaIncludeSourceIncludeSecondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = BLUE.criteriaIncludeSourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					BLUE.criteriaIncludeSourceIncludeSubSource = '*' --source is using the wildcard
					OR
					theClause.subsource = BLUE.criteriaIncludeSourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND (
					br.startDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
					OR
					br.endDate BETWEEN BLUE.criteriaIncludeSourceIncludeStartDate AND BLUE.criteriaIncludeSourceIncludeEndDate
				)
			--match criteria travel agent group to res travel agent group
			AND (
					(
						BLUE.criteriaIncludeTravelAgentGroupID = 0 --criteria is using wildcard
						OR
						theClause.travelAgentGroupID = BLUE.criteriaIncludeTravelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND (
							br.startDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
							OR
							br.endDate BETWEEN BLUE.criteriaIncludeTravelAgentGroupStartDate AND BLUE.criteriaIncludeTravelAgentGroupEndDate
						)
				)
			--match criteria rate code to res rate code
			AND (
					(
						BLUE.criteriaIncludeRateCode = '*' --criteria is using wildcard
						OR
						theClause.rateCode = BLUE.criteriaIncludeRateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND (
							br.startDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
							OR
							br.endDate BETWEEN BLUE.criteriaIncludeRateCodeStartDate AND BLUE.criteriaIncludeRateCodeEndDate
						)
				)
			--match criteria rate category to res rate category
			AND (
					(
						BLUE.criteriaIncludeRateCategoryCode = '*' --criteria is using wildcard
						OR
						theClause.rateCategoryCode = BLUE.criteriaIncludeRateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND (
							br.startDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
							OR
							br.endDate BETWEEN BLUE.criteriaIncludeRateCategoryStartDate AND BLUE.criteriaIncludeRateCategoryEndDate
						)
				)
			--match criteria loyalty option to res loyalty option
			AND (
					BLUE.criteriaIncludeLoyaltyOptionID = 0 --criteria using wildcard option
					OR
					theClause.loyaltyOptionID = BLUE.criteriaIncludeLoyaltyOptionID --or the value matches exactly
				)

		--see if we match any sources excluded from the matched criteria group exclusion
		 --CECG_CGIC_SGIS
		LEFT JOIN vw_sourcesFlattened YELLOW ON BLUE.criteriaIncludeSourceExcludeID = YELLOW.sourceId 
			--make sure the source matched was excluded at time of arrival
			AND (
					br.startDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
					OR
					br.endDate BETWEEN BLUE.criteriaIncludeSourceExcludeStartDate AND BLUE.criteriaIncludeSourceExcludeEndDate
				)

			--match the source template group to the reservation's xbe template
			AND (
					YELLOW.templateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = YELLOW.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					YELLOW.CroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = YELLOW.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					YELLOW.channel = '*' --source is using the wildcard
					OR
					theClause.channel = YELLOW.channel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					YELLOW.secondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = YELLOW.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					YELLOW.SubSource = '*' --source is using the wildcard
					OR
					theClause.subsource = YELLOW.SubSource --or the value matches exactly
				) 

		--see if we match a criteria excluded from the criteria group exclusion
		 --CECG_CGEC_SGIS 
		LEFT JOIN vw_criteriaFlattened BROWN ON BLUE.criteriaExcludeID = BROWN.criteriaID 
			--the criteria was excluded at time of arrival
			AND (
					br.startDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
					OR
					br.endDate BETWEEN BLUE.criteriaExcludeStartDate AND BLUE.criteriaExcludeEndDate
				)
			--match the source template group to the reservation's xbe template
			AND (
					BROWN.sourceIncludeTemplateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = BROWN.sourceIncludeTemplateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					BROWN.sourceIncludeCroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = BROWN.sourceIncludeCroGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					BROWN.sourceIncludeChannel = '*' --source is using the wildcard
					OR
					theClause.channel = BROWN.sourceIncludeChannel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					BROWN.sourceIncludeSecondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = BROWN.sourceIncludeSecondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					BROWN.sourceIncludeSubSource = '*' --source is using the wildcard
					OR
					theClause.subsource = BROWN.sourceIncludeSubSource --or the value matches exactly
				) 
			--make sure the source matched was included at time of arrival
			AND (
					br.startDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
					OR
					br.endDate BETWEEN BROWN.sourceIncludeStartDate AND BROWN.sourceIncludeEndDate
				)
			--match criteria travel agent group to res travel agent group
			AND (
					(
						BROWN.travelAgentGroupID = 0 --criteria is using wildcard
						OR
						theClause.travelAgentGroupID = BROWN.travelAgentGroupID --or the value matches exactly
					)
					--travel agent group was included at time of arrival
					AND (
							br.startDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
							OR
							br.endDate BETWEEN BROWN.travelAgentGroupStartDate AND BROWN.travelAgentGroupEndDate
						)
				)

			--match criteria rate code to res rate code
			AND (
					(
						BROWN.rateCode = '*' --criteria is using wildcard
						OR
						theClause.rateCode = BROWN.rateCode --or the value matches exactly
					)
					--rate code was included at time of arrival
					AND (
							br.startDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
							OR
							br.endDate BETWEEN BROWN.rateCodeStartDate AND BROWN.rateCodeEndDate
						)
				)
			--match criteria rate category to res rate category
			AND (
					(
						BROWN.rateCategoryCode = '*' --criteria is using wildcard
						OR
						theClause.rateCategoryCode = BROWN.rateCategoryCode --or the value matches exactly
					)
					--rate category was included at time of arrival
					AND (
							br.startDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
							OR
							br.endDate BETWEEN BROWN.rateCategoryStartDate AND BROWN.rateCategoryEndDate
						)
				)
			--match criteria loyalty option to res loyalty option
			AND (
					BROWN.loyaltyOptionID = 0 --criteria using wildcard option
					OR
					theClause.loyaltyOptionID = BROWN.loyaltyOptionID --or the value matches exactly
				)

		--see if we match any sources excluded from the excluded criteria
		 --CECG_CGEC_SGES
		LEFT JOIN vw_sourcesFlattened VIOLET ON BROWN.sourceExcludeID = VIOLET.sourceId 
			--make sure the source matched was excluded at time of arrival
			AND (
					br.startDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
					OR
					br.endDate BETWEEN BROWN.sourceExcludeStartDate AND BROWN.sourceExcludeEndDate
				)
			--match the source template group to the reservation's xbe template
			AND (
					VIOLET.templateGroupID = 0 --source is using the wildcard group
					OR
					theClause.templateGroupID = VIOLET.templateGroupID --we've determined the template is in the source's template group
				)
			--match the source cro group to the res cro code
			AND (
					VIOLET.CroGroupID = 0 --source is using the wildcard group
					OR
					theClause.croGroupID = VIOLET.croGroupID --we've determined the CRO Code is in the source's CRO Group
				)
			--match the source channel to the res channel
			AND (
					VIOLET.channel = '*' --source is using the wildcard
					OR
					theClause.channel = VIOLET.channel --or the value matches exactly
				) 
			--match the source secondary source to the res secondary source
			AND (
					VIOLET.secondarySource = '*' --source is using the wildcard
					OR
					theClause.secondarySource = VIOLET.secondarySource --or the value matches exactly
				)
			--match the source sub source to the res sub source
			AND (
					VIOLET.SubSource = '*' --source is using the wildcard
					OR
					theClause.subsource = VIOLET.SubSource --or the value matches exactly
				) 


		--see if the clause has already excluded the GREEN match by a RED exclusion
		LEFT JOIN (SELECT * FROM vw_RED WHERE clauseID = @clauseID) theClauseRED ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID = theClauseRED.sourceId
			AND (
					br.startDate BETWEEN theClauseRED.SG_startDate AND theClauseRED.SG_endDate
					OR
					br.endDate BETWEEN theClauseRED.SG_startDate AND theClauseRED.SG_endDate
				)

		--see if the clause has already excluded the GREEN match by an ORANGE exclusion
		LEFT JOIN (SELECT * FROM vw_ORANGE WHERE clauseID = @clauseID) theClauseORANGE ON GREEN.criteriaGroupIncludeCriteriaIncludeID = theClauseORANGE.criteriaID
			AND (
					br.startDate BETWEEN theClauseORANGE.CGEC_startDate AND theClauseORANGE.CGEC_endDate
					OR
					br.endDate BETWEEN theClauseORANGE.CGEC_startDate AND theClauseORANGE.CGEC_endDate
				)

		--see if theClauseORANGE exclusion is itself excluded by an INDIGO exclusion
		LEFT JOIN (SELECT * FROM vw_INDIGO WHERE clauseID = @clauseID) theClauseINDIGO ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID = theClauseINDIGO.sourceId
			AND (
					br.startDate BETWEEN theClauseINDIGO.SG_startDate AND theClauseINDIGO.SG_endDate
					OR
					br.endDate BETWEEN theClauseINDIGO.SG_startDate AND theClauseINDIGO.SG_endDate
				)

		--see if the clause has already excluded the GREEN match by a BLUE exclusion
		LEFT JOIN (SELECT * FROM vw_BLUE WHERE clauseID = @clauseID) theClauseBLUE ON GREEN.criteriaGroupIncludeCriteriaIncludeID = theClauseBLUE.criteriaID
			AND (
					br.startDate BETWEEN theClauseBLUE.CGIC_startDate AND theClauseBLUE.CGIC_endDate
					OR
					br.endDate BETWEEN theClauseBLUE.CGIC_startDate AND theClauseBLUE.CGIC_endDate
				)		

		--see if theClauseBLUE exclusion is itself excluded by a YELLOW exclusion
		LEFT JOIN (SELECT * FROM vw_YELLOW WHERE clauseID = @clauseID) theClauseYELLOW ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID = theClauseYELLOW.sourceId
			AND (
					br.startDate BETWEEN theClauseYELLOW.SG_startDate AND theClauseYELLOW.SG_endDate
					OR
					br.endDate BETWEEN theClauseYELLOW.SG_startDate AND theClauseYELLOW.SG_endDate
				)

		--see if theClauseBLUE exclusion is itself excluded by a BROWN exclusion
		LEFT JOIN (SELECT * FROM vw_BROWN WHERE clauseID = @clauseID) theClauseBROWN ON GREEN.criteriaGroupIncludeCriteriaIncludeID = theClauseBROWN.criteriaID
			AND (
					br.startDate BETWEEN theClauseBROWN.CGEC_startDate AND theClauseBROWN.CGEC_endDate
					OR
					br.endDate BETWEEN theClauseBROWN.CGEC_startDate AND theClauseBROWN.CGEC_endDate
				)

		--see if theClauseBROWN exclusion is itself excluded by a VIOLET exclusion
		LEFT JOIN (SELECT * FROM vw_VIOLET WHERE clauseID = @clauseID) theClauseVIOLET ON GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID = theClauseVIOLET.sourceId
			AND (
					br.startDate BETWEEN theClauseVIOLET.SG_startDate AND theClauseVIOLET.SG_endDate
					OR
					br.endDate BETWEEN theClauseVIOLET.SG_startDate AND theClauseVIOLET.SG_endDate
				)
	WHERE 
	-- the clause is not already excluded from the matched GREEN clause
	(
		(
			(
				GREEN.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
				AND
				RED.sourceId IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
			)
			AND 
			(
				ORANGE.sourceIncludeID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
				OR
				INDIGO.sourceId IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
			)
		)
		AND
		(
			(
				BLUE.criteriaIncludeSourceIncludeID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
				OR
				YELLOW.sourceId IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
			)
			OR
			(
				BROWN.sourceIncludeID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
				AND
				VIOLET.sourceId IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
			)
		)
	)
	AND
	--the matched GREEN clause is not already excluded from the clause
	(
		(
			(
				theClause.clauseID IS NOT NULL --DID match some clause (the CICG_CGIC_SGIS branch)
				AND
				theClauseRED.clauseID IS NULL --did NOT match an exclusion on the included source group (the CICG_CGIC_SGES branch)
			)
			AND 
			(
				theClauseORANGE.clauseID IS NULL --did NOT match an exclusion on the clause's criteria groups (the CICG_CGEC_SGIS branch)
				OR
				theClauseINDIGO.clauseID IS NOT NULL --DID match an exclusion, but then also matched an exclusion of the exclusion (the CICG_CGEC_SGES branch)
			)
		)
		AND
		(
			(
				theClauseBLUE.clauseID IS NULL --did NOT match an excluded criteria group on the matched clause (the CECG_CGIC_SGIS branch)
				OR
				theClauseYELLOW.clauseID IS NOT NULL --DID match an excluded criteria group, but then also matched a source exclusion of that exclusion (the CECG_CGIC_SGES branch)
			)
			OR
			(
				theClauseBROWN.clauseID IS NOT NULL --DID match a BLUE exclusion, but then also matched an exclusion of that exclusion (the CECG_CGEC_SGIS branch)
				AND
				theClauseVIOLET.clauseID IS NULL --DID match a BROWN exclusion, and did NOT match an exclusion of that exclusion (the CECG_CGEC_SGES branch)
			)
		)
	)
	AND GREEN.criteriaGroupIncludeCriteriaIncludeSourceIncludeID != 0 --the collision isn't using the ALL SOURCES option, which will always collide with everything
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [translate].[RUN_TRANSLATION]'
GO


ALTER PROCEDURE [translate].[RUN_TRANSLATION]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

EXECUTE [BillingRewrite].[translate].[clearExistingData];
EXECUTE [BillingRewrite].[translate].[insertCROcodes];
EXECUTE [BillingRewrite].[translate].[insertTemplates];
EXECUTE [BillingRewrite].[translate].[insertSources];
EXECUTE [BillingRewrite].[translate].[insertSourceGroups];
EXECUTE [BillingRewrite].[translate].[insertSourceGroupsExclusions];
EXECUTE [BillingRewrite].[translate].[insertSourceGroupsInclusions];
EXECUTE [BillingRewrite].[translate].[insertCriteria];
EXECUTE [BillingRewrite].[translate].[insertCriteria_RateCategories];
EXECUTE [BillingRewrite].[translate].[insertCriteria_RateCodes];
EXECUTE [BillingRewrite].[translate].[insertCriteria_SourceGroups];
EXECUTE [BillingRewrite].[translate].[insertCriteria_TravelAgentGroups];
EXECUTE [BillingRewrite].[translate].[insertCriteriaGroups];
EXECUTE [BillingRewrite].[translate].[insertCriteriaGroupsExclusions];
EXECUTE [BillingRewrite].[translate].[insertCriteriaGroupsInclusions];
EXECUTE [BillingRewrite].[translate].[insertTranslateCriteriaGroupsPerHotel];
EXECUTE [BillingRewrite].[translate].[insertTranslateCriteriaGroups];
EXECUTE [BillingRewrite].[translate].[insertTranslateClauses];
EXECUTE [BillingRewrite].[translate].[insertClauses];
EXECUTE [BillingRewrite].[translate].[insertClausesInclusions];
EXECUTE [BillingRewrite].[translate].[insertThresholdRules];
EXECUTE [BillingRewrite].[translate].[insertBillingRules];
EXECUTE [BillingRewrite].[translate].[insertClausesExclusions];
EXECUTE [BillingRewrite].[translate].[postTranslationIssues];


END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[collision_Timeframe]'
GO

ALTER PROCEDURE [dbo].[collision_Timeframe]
	@billingRuleID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	WITH cte AS (
	SELECT br2.billingRuleID,br2.startDate,br2.endDate,br2.thresholdMinimum,c.classificationName,
		CASE 
			WHEN br1.startDate < br2.startDate THEN
				CASE
					WHEN br1.endDate < br2.startDate THEN 'BEFORE' 
					WHEN br2.endDate <= br1.endDate THEN 'EXCEEDING' 
					ELSE 'UNDERLAPPING'
				END
			WHEN br1.startDate = br2.startDate THEN
				CASE
					WHEN br1.endDate < br2.endDate THEN 'UNDERLAPPING' 
					WHEN br2.endDate < br1.endDate THEN 'EXCEEDING' 
					ELSE 'EXACTLY'
				END
			WHEN br2.startDate < br1.startDate THEN
				CASE
					WHEN br2.endDate < br1.startDate THEN 'AFTER' 
					WHEN br2.endDate <= br1.endDate THEN 'OVERLAPPING' 
					ELSE 'INSIDE'
				END
			WHEN br2.endDate = br1.startDate THEN 'OVERLAPPING' 
			ELSE 'This should not ever happen'
		END AS timeframeMatchType
	FROM BillingRules br1
		INNER JOIN BillingRules br2 ON br2.clauseID = br1.clauseID
		INNER JOIN Classifications c ON c.classificationID = br2.classificationID
	WHERE br1.billingRuleID = @billingRuleID
		AND br2.billingRuleID != @billingRuleID
		AND br1.classificationID = br2.classificationID
		AND br1.thresholdMinimum = br2.thresholdMinimum
	)
	SELECT *
	FROM cte
	WHERE timeframeMatchType NOT IN ('BEFORE','AFTER')
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_MrtForCalculation]'
GO

--MrtForCalculation
CREATE FUNCTION fnc_MrtForCalculation
(
	@startDate date = null,
	@endDate date = null,
	@hotelCode nvarchar(20) = null,
	@confirmationNumber nvarchar(20) = null	
)
RETURNS TABLE 
AS
RETURN 
(
	WITH cte_charges
	AS (
		SELECT confirmationNumber,sopNumber 
		FROM dbo.Charges
		WHERE hotelCode = ISNULL(@hotelCode,hotelCode) --either we're running all hotels, or we're just getting a specific hotel
		AND confirmationNumber = ISNULL(@confirmationNumber,confirmationNumber) --either we're running all bookings, or just getting a specific conf#
		AND sopNumber IS NOT NULL
	) 
	,cte_mrtj
	AS
	(
		SELECT mrtj.[confirmationNumber],[phgHotelCode],[crsHotelID],[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],[synxisBillingDescription],
				[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateGroupID],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
				[bookingCroGroupID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],
				[cancellationDate],[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookingCurrency],[bookingCurrencyCode],[timeLoaded],[CRSSourceID],
				work.[billyItemCode]([synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],[bookingTemplateAbbreviation],[bookingCroGroupID],[chainID]) AS [ItemCode],
				CONVERT(date,CASE WHEN arrivalDate >= GETDATE() THEN confirmationDate ELSE arrivaldate END) AS exchangeDate,
				mrtj.loyaltyProgram,mrtj.loyaltyNumber,mrtj.[travelAgencyName],ISNULL(mrtj.LoyaltyNumberValidated,0) AS LoyaltyNumberValidated,ISNULL(mrtj.LoyaltyNumberTagged,0) AS LoyaltyNumberTagged
		FROM work.mrtJoined mrtj
			LEFT JOIN cte_charges cd ON cd.confirmationNumber = mrtj.confirmationNumber 
		WHERE mrtj.phgHotelCode = ISNULL(@hotelCode,mrtj.phgHotelCode) --either we're running all hotels, or we're just getting a specific hotel
			AND mrtj.confirmationNumber = ISNULL(@confirmationNumber,mrtj.confirmationNumber) --either we're running all bookings, or just getting a specific conf#
			AND mrtj.arrivalDate BETWEEN @startDate AND @endDate
			AND mrtj.phgHotelCode NOT IN('BCTS4','PHGTEST')
			AND cd.confirmationNumber IS NULL
	)
	SELECT 
		mrtj.[confirmationNumber]
		,mrtj.[phgHotelCode]
		,mrtj.[crsHotelID]
		,mrtj.[hotelName]
		,mrtj.[mainBrandCode]
		,mrtj.[gpSiteID]
		,mrtj.[chainID]
		,mrtj.[chainName]
		,mrtj.[bookingStatus]
		,mrtj.[synxisBillingDescription]
		,mrtj.[bookingChannel]
		,mrtj.[bookingSecondarySource]
		,mrtj.[bookingSubSourceCode]
		,mrtj.[bookingTemplateGroupID]
		,mrtj.[bookingTemplateAbbreviation]
		,mrtj.[xbeTemplateName]
		,mrtj.[CROcode]
		,mrtj.[bookingCroGroupID]
		,mrtj.[bookingRateCategoryCode]
		,mrtj.[bookingRateCode]
		,mrtj.[bookingIATA]
		,mrtj.[transactionTimeStamp]
		,mrtj.[confirmationDate]
		,mrtj.[arrivalDate]
		,mrtj.[departureDate]
		,mrtj.[cancellationDate]
		,mrtj.[cancellationNumber]
		,mrtj.[nights]
		,mrtj.[rooms]
		,mrtj.[roomNights]
		,mrtj.[roomRevenueInBookingCurrency]
		,mrtj.[bookingCurrencyCode]
		,mrtj.[timeLoaded]
		,mrtj.[CRSSourceID]
		,mrtj.[ItemCode]
		,mrtj.exchangeDate
		,gpCustomer.CURNCYID as hotelCurrencyCode
		,hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces
		,hotelCE.XCHGRATE as hotelCurrencyExchangeRate
		,bookingCE.XCHGRATE as bookingCurrencyExchangeRate
		,mrtj.loyaltyProgram
		,mrtj.loyaltyNumber
		,mrtj.[travelAgencyName]
		,mrtj.LoyaltyNumberValidated
		,mrtj.LoyaltyNumberTagged
	FROM cte_mrtj mrtj
		INNER JOIN work.GPCustomerTable gpCustomer ON mrtj.phgHotelCode = gpCustomer.CUSTNMBR
		INNER JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID AND mrtj.exchangeDate = hotelCE.EXCHDATE
		INNER JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		INNER JOIN work.[local_exchange_rates] bookingCE ON mrtj.bookingCurrencyCode = bookingCE.CURNCYID AND mrtj.exchangeDate = bookingCE.EXCHDATE
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_local_exchange_rates]'
GO

CREATE FUNCTION fnc_local_exchange_rates
(	
	@startDate date = null,
	@i int = 0
)
RETURNS TABLE 
AS
RETURN 
(
			SELECT 'USD' as currencyCode, DATEADD(day, @i, @startdate) as exchangeDate, 1 as exchangeRate
			UNION
			SELECT CURNCYID, DATEADD(day, @i, @startdate), XCHGRATE
			FROM DYNAMICS.dbo.MC00100
			WHERE
				DATEADD(day, @i, @startdate) BETWEEN EXCHDATE AND EXPNDATE 
				AND EXGTBLID LIKE '%AVG%'
				AND CURNCYID != 'USD'		
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_GPCustomerTable]'
GO
CREATE FUNCTION fnc_GPCustomerTable
(	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT CUSTNMBR,CURNCYID
	FROM IC.dbo.RM00101
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_GPCurrencyMaster]'
GO
CREATE FUNCTION fnc_GPCurrencyMaster
(	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT CURNCYID,DECPLCUR
	FROM DYNAMICS.dbo.MC40200
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_hotelActiveBrands]'
GO

--hotelActiveBrands
CREATE FUNCTION fnc_hotelActiveBrands
(	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT hb.hotelCode,MIN(b.heirarchy) AS mainHeirarchy
	FROM Core.dbo.hotels_brands hb
		INNER JOIN Core.dbo.brands AS b ON hb.brandCode = b.code
	WHERE (hb.endDatetime IS NULL)
		AND (hb.startDatetime <= GETDATE())
		OR (GETDATE() BETWEEN hb.startDatetime AND hb.endDatetime)
	GROUP BY hb.hotelCode
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_hotels_OpenHospitality]'
GO

--hotels_OpenHospitality
CREATE FUNCTION fnc_hotels_OpenHospitality
(	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitality]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality])

	UNION ALL

	SELECT [hotelCode],[openHospitalityCode]
	FROM [Core].[dbo].[brands_bookingOpenHospitalityPHG]
	WHERE hotelCode NOT IN(SELECT hotelCode FROM [Core].[dbo].[hotels_OpenHospitality])
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fnc_hotelInactiveBrands]'
GO
--hotelInactiveBrands
CREATE FUNCTION fnc_hotelInactiveBrands
(	
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT hotelCode,MIN(heirarchy) AS mainHeirarchy
	FROM
		(
			SELECT hb.hotelCode,hb.brandCode,b.heirarchy
			FROM Core.dbo.hotels_brands AS hb
				INNER JOIN
					(
						SELECT hotelCode,MAX(endDatetime) AS maxEnd
						FROM Core.dbo.hotels_brands
						GROUP BY hotelCode
					) AS maxDate ON hb.endDatetime = maxDate.maxEnd AND hb.hotelCode = maxDate.hotelCode
				INNER JOIN Core.dbo.brands b ON hb.brandCode = b.code
			GROUP BY hb.hotelCode,hb.brandCode,b.heirarchy
		) AS maxBrands
	GROUP BY hotelCode
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
