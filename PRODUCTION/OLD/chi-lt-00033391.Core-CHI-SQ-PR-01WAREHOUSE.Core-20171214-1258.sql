/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Core    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.Core

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 12/14/2017 12:58:21 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[ips_iPreferScorecardHotelInfo]'
GO
CREATE PROCEDURE [dbo].[ips_iPreferScorecardHotelInfo] 
AS
BEGIN
	SET NOCOUNT ON;

--iPreferScorecardHotelInfo.sql
select 
	code, 
	hotelname, 
	statuscodename as type,
	phg_regionalmanageridname as RD,
	CASE WHEN iPreferCheckboxHotels.hotelCode IS NULL THEN 'NO' ELSE 'YES' END AS CheckboxHotel
FROM
	hotelsreporting
LEFT OUTER JOIN
	iPreferCheckboxHotels 
ON
	hotelsreporting.code = iPreferCheckboxHotels.hotelCode
WHERE
	statuscodename in ('Member Hotel','Former Member', 'Renewal Hotel');
	
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
