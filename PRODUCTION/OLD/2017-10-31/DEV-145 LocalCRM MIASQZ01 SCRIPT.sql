USE LocalCRM
GO

/*
Run this script on:

        miaSQZ01.LocalCRM    -  This database will be modified

to synchronize it with:

        chi-lt-00033391\miasqz01.LocalCRM

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 10/31/2017 11:42:07 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [staging].[IPreferBookingReward]'
GO
CREATE SYNONYM [staging].[IPreferBookingReward] FOR [10.122.138.81\WAREHOUSE].[LocalCRM].[dbo].[IPreferBookingReward]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPreferBookingReward]'
GO
CREATE TABLE [dbo].[IPreferBookingReward]
(
[createdBy] [uniqueidentifier] NULL,
[createdByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOn] [datetime] NULL,
[createdOnBehalfBy] [uniqueidentifier] NULL,
[createdOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importSequenceNumber] [int] NULL,
[modifiedBy] [uniqueidentifier] NULL,
[modifiedByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOn] [datetime] NULL,
[modifiedOnBehalfBy] [uniqueidentifier] NULL,
[modifiedOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[organizationId] [uniqueidentifier] NULL,
[organizationIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[overriddenCreatedOn] [datetime] NULL,
[phg_endDate] [datetime] NULL,
[phg_hotelId] [uniqueidentifier] NULL,
[phg_hotelIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_hotelIdYomiName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_ipreferBookingRewardId] [uniqueidentifier] NULL,
[phg_name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_startDate] [datetime] NULL,
[stateCode] [int] NULL,
[stateCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusCode] [int] NULL,
[statusCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timezoneRuleVersionNumber] [int] NULL,
[utcConversionTimezoneCode] [int] NULL,
[versionNumber] [bigint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_IPreferBookingReward_1] on [dbo].[IPreferBookingReward]'
GO
ALTER TABLE [dbo].[IPreferBookingReward] ADD CONSTRAINT [PK_IPreferBookingReward_1] PRIMARY KEY CLUSTERED  ([versionNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[crm_synchronize_IPreferBookingReward]'
GO

CREATE PROCEDURE [dbo].[crm_synchronize_IPreferBookingReward]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE INTO dbo.IPreferBookingReward AS tgt
	USING (SELECT * FROM staging.IPreferBookingReward) AS src ON src.[versionNumber] = tgt.[versionNumber]
	WHEN MATCHED THEN
		UPDATE
			SET createdBy = src.createdBy,createdByName = src.createdByName,createdByYomiName = src.createdByYomiName,createdOn = src.createdOn,createdOnBehalfBy = src.createdOnBehalfBy,createdOnBehalfByName = src.createdOnBehalfByName,createdOnBehalfByYomiName = src.createdOnBehalfByYomiName,importSequenceNumber = src.importSequenceNumber,modifiedBy = src.modifiedBy,modifiedByName = src.modifiedByName,modifiedByYomiName = src.modifiedByYomiName,modifiedOn = src.modifiedOn,modifiedOnBehalfBy = src.modifiedOnBehalfBy,modifiedOnBehalfByName = src.modifiedOnBehalfByName,modifiedOnBehalfByYomiName = src.modifiedOnBehalfByYomiName,organizationId = src.organizationId,organizationIdName = src.organizationIdName,overriddenCreatedOn = src.overriddenCreatedOn,phg_endDate = src.phg_endDate,phg_hotelId = src.phg_hotelId,phg_hotelIdName = src.phg_hotelIdName,phg_hotelIdYomiName = src.phg_hotelIdYomiName,phg_ipreferBookingRewardId = src.phg_ipreferBookingRewardId,phg_name = src.phg_name,phg_startDate = src.phg_startDate,stateCode = src.stateCode,stateCodeName = src.stateCodeName,statusCode = src.statusCode,statusCodeName = src.statusCodeName,timezoneRuleVersionNumber = src.timezoneRuleVersionNumber,utcConversionTimezoneCode = src.utcConversionTimezoneCode,versionNumber = src.versionNumber
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([createdBy],[createdByName],[createdByYomiName],[createdOn],[createdOnBehalfBy],[createdOnBehalfByName],[createdOnBehalfByYomiName],[importSequenceNumber],[modifiedBy],[modifiedByName],[modifiedByYomiName],[modifiedOn],[modifiedOnBehalfBy],[modifiedOnBehalfByName],[modifiedOnBehalfByYomiName],[organizationId],[organizationIdName],[overriddenCreatedOn],[phg_endDate],[phg_hotelId],[phg_hotelIdName],[phg_hotelIdYomiName],[phg_ipreferBookingRewardId],[phg_name],[phg_startDate],[stateCode],[stateCodeName],[statusCode],[statusCodeName],[timezoneRuleVersionNumber],[utcConversionTimezoneCode],[versionNumber])
		VALUES(src.[createdBy],src.[createdByName],src.[createdByYomiName],src.[createdOn],src.[createdOnBehalfBy],src.[createdOnBehalfByName],src.[createdOnBehalfByYomiName],src.[importSequenceNumber],src.[modifiedBy],src.[modifiedByName],src.[modifiedByYomiName],src.[modifiedOn],src.[modifiedOnBehalfBy],src.[modifiedOnBehalfByName],src.[modifiedOnBehalfByYomiName],src.[organizationId],src.[organizationIdName],src.[overriddenCreatedOn],src.[phg_endDate],src.[phg_hotelId],src.[phg_hotelIdName],src.[phg_hotelIdYomiName],src.[phg_ipreferBookingRewardId],src.[phg_name],src.[phg_startDate],src.[stateCode],src.[stateCodeName],src.[statusCode],src.[statusCodeName],src.[timezoneRuleVersionNumber],src.[utcConversionTimezoneCode],src.[versionNumber]);
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [staging].[phg_experiences]'
GO
CREATE SYNONYM [staging].[phg_experiences] FOR [10.122.138.81\WAREHOUSE].[LocalCRM].[dbo].[phg_experiences]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[phg_experiences]'
GO
CREATE TABLE [dbo].[phg_experiences]
(
[createdBy] [uniqueidentifier] NULL,
[createdByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOn] [datetime] NULL,
[createdOnBehalfBy] [uniqueidentifier] NULL,
[createdOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importSequenceNumber] [int] NULL,
[modifiedBy] [uniqueidentifier] NULL,
[modifiedByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOn] [datetime] NULL,
[modifiedOnBehalfBy] [uniqueidentifier] NULL,
[modifiedOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[organizationId] [uniqueidentifier] NULL,
[organizationIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[overriddenCreatedOn] [datetime] NULL,
[phg_account] [uniqueidentifier] NULL,
[phg_accountName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_accountNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_accountYomiName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_endDate] [datetime] NULL,
[phg_experiencesId] [uniqueidentifier] NOT NULL,
[phg_experiencesType] [int] NULL,
[phg_experiencesTypeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_startDate] [datetime] NULL,
[stateCode] [int] NULL,
[stateCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusCode] [int] NULL,
[statusCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timezoneRuleVersionNumber] [int] NULL,
[utcConversionTimezoneCode] [int] NULL,
[versionNumber] [bigint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_phg_experiences] on [dbo].[phg_experiences]'
GO
ALTER TABLE [dbo].[phg_experiences] ADD CONSTRAINT [PK_phg_experiences] PRIMARY KEY CLUSTERED  ([phg_experiencesId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[crm_synchronize_phg_experiences]'
GO

CREATE PROCEDURE [dbo].[crm_synchronize_phg_experiences]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE INTO dbo.phg_experiences AS tgt
	USING (SELECT * FROM staging.phg_experiences) AS src ON src.phg_experiencesid = tgt.phg_experiencesid
	WHEN MATCHED THEN
		UPDATE
			SET createdBy = src.createdBy,createdByName = src.createdByName,createdByYomiName = src.createdByYomiName,createdOn = src.createdOn,createdOnBehalfBy = src.createdOnBehalfBy,createdOnBehalfByName = src.createdOnBehalfByName,createdOnBehalfByYomiName = src.createdOnBehalfByYomiName,importSequenceNumber = src.importSequenceNumber,modifiedBy = src.modifiedBy,modifiedByName = src.modifiedByName,modifiedByYomiName = src.modifiedByYomiName,modifiedOn = src.modifiedOn,modifiedOnBehalfBy = src.modifiedOnBehalfBy,modifiedOnBehalfByName = src.modifiedOnBehalfByName,modifiedOnBehalfByYomiName = src.modifiedOnBehalfByYomiName,organizationId = src.organizationId,organizationIdName = src.organizationIdName,overriddenCreatedOn = src.overriddenCreatedOn,phg_account = src.phg_account,phg_accountName = src.phg_accountName,phg_accountNumber = src.phg_accountNumber,phg_accountYomiName = src.phg_accountYomiName,phg_endDate = src.phg_endDate,phg_experiencesId = src.phg_experiencesId,phg_experiencesType = src.phg_experiencesType,phg_experiencesTypeName = src.phg_experiencesTypeName,phg_name = src.phg_name,phg_startDate = src.phg_startDate,stateCode = src.stateCode,stateCodeName = src.stateCodeName,statusCode = src.statusCode,statusCodeName = src.statusCodeName,timezoneRuleVersionNumber = src.timezoneRuleVersionNumber,utcConversionTimezoneCode = src.utcConversionTimezoneCode,versionNumber = src.versionNumber
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([createdBy],[createdByName],[createdByYomiName],[createdOn],[createdOnBehalfBy],[createdOnBehalfByName],[createdOnBehalfByYomiName],[importSequenceNumber],[modifiedBy],[modifiedByName],[modifiedByYomiName],[modifiedOn],[modifiedOnBehalfBy],[modifiedOnBehalfByName],[modifiedOnBehalfByYomiName],[organizationId],[organizationIdName],[overriddenCreatedOn],[phg_account],[phg_accountName],[phg_accountNumber],[phg_accountYomiName],[phg_endDate],[phg_experiencesId],[phg_experiencesType],[phg_experiencesTypeName],[phg_name],[phg_startDate],[stateCode],[stateCodeName],[statusCode],[statusCodeName],[timezoneRuleVersionNumber],[utcConversionTimezoneCode],[versionNumber])
		VALUES(src.[createdBy],src.[createdByName],src.[createdByYomiName],src.[createdOn],src.[createdOnBehalfBy],src.[createdOnBehalfByName],src.[createdOnBehalfByYomiName],src.[importSequenceNumber],src.[modifiedBy],src.[modifiedByName],src.[modifiedByYomiName],src.[modifiedOn],src.[modifiedOnBehalfBy],src.[modifiedOnBehalfByName],src.[modifiedOnBehalfByYomiName],src.[organizationId],src.[organizationIdName],src.[overriddenCreatedOn],src.[phg_account],src.[phg_accountName],src.[phg_accountNumber],src.[phg_accountYomiName],src.[phg_endDate],src.[phg_experiencesId],src.[phg_experiencesType],src.[phg_experiencesTypeName],src.[phg_name],src.[phg_startDate],src.[stateCode],src.[stateCodeName],src.[statusCode],src.[statusCodeName],src.[timezoneRuleVersionNumber],src.[utcConversionTimezoneCode],src.[versionNumber]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Account]'
GO
ALTER TABLE [dbo].[Account] ADD
[phg_openHospitalityCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_openHospitalityNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_phgMeetingsCom] [bit] NULL,
[phg_phgMeetingsComName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_phgMeetingsEndDate] [datetime] NULL,
[phg_phgMeetingsStartDate] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[crm_synchronize]'
GO
ALTER PROCEDURE [dbo].[crm_synchronize]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	EXEC crm_synchronize_Account
	EXEC crm_synchronize_Connection
	EXEC crm_synchronize_ConnectionRole
	EXEC crm_synchronize_Contact
	EXEC crm_synchronize_customeraddress
	EXEC crm_synchronize_HotelAttributes
	EXEC crm_synchronize_HotelAttributeType
	EXEC crm_synchronize_iTools
	EXEC crm_synchronize_Leads
	EXEC crm_synchronize_Opportunity
	EXEC crm_synchronize_opportunityproduct
	EXEC crm_synchronize_phg_city
	EXEC crm_synchronize_phg_collection
	EXEC crm_synchronize_phg_contractedprograms
	EXEC crm_synchronize_phg_country
	EXEC crm_synchronize_phg_event
	EXEC crm_synchronize_phg_hotelcollection
	EXEC crm_synchronize_phg_hoteleventregistration
	EXEC crm_synchronize_phg_hotelreportsubscription
	EXEC crm_synchronize_phg_requestforproposal
	EXEC crm_synchronize_phg_state
	EXEC crm_synchronize_product
	EXEC crm_synchronize_Role
	EXEC crm_synchronize_SystemUser
	EXEC crm_synchronize_systemuserroles
	EXEC crm_synchronize_transactioncurrency

	EXEC crm_synchronize_phg_experiences
	EXEC crm_synchronize_IPreferBookingReward
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
