USE LocalCRM
GO

/*
Run this script on:

        chi-sq-pr-01\warehouse.LocalCRM    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.LocalCRM

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 10/31/2017 10:09:11 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [staging].[Account]'
GO
ALTER TABLE [staging].[Account] ADD
[phg_openHospitalityCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_openHospitalityNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_phgMeetingsCom] [bit] NULL,
[phg_phgMeetingsComName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_phgMeetingsEndDate] [datetime] NULL,
[phg_phgMeetingsStartDate] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [staging].[Account] ALTER COLUMN [phg_brandname] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Account]'
GO
ALTER TABLE [dbo].[Account] ADD
[phg_openHospitalityCode] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_openHospitalityNumber] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_phgMeetingsCom] [bit] NULL,
[phg_phgMeetingsComName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_phgMeetingsEndDate] [datetime] NULL,
[phg_phgMeetingsStartDate] [datetime] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[crm_synchronize_Account]'
GO
ALTER PROCEDURE [dbo].[crm_synchronize_Account]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	IF EXISTS(SELECT * FROM staging.Account)
	BEGIN
		MERGE INTO dbo.Account AS tgt
		USING (SELECT * FROM staging.Account) AS src ON src.Accountid = tgt.Accountid
		WHEN MATCHED THEN
			UPDATE
				SET accountcategorycode = src.accountcategorycode,accountcategorycodename = src.accountcategorycodename,accountclassificationcode = src.accountclassificationcode,accountclassificationcodename = src.accountclassificationcodename,accountid = src.accountid,accountnumber = src.accountnumber,accountratingcode = src.accountratingcode,accountratingcodename = src.accountratingcodename,address1_addressid = src.address1_addressid,address1_addresstypecode = src.address1_addresstypecode,address1_addresstypecodename = src.address1_addresstypecodename,address1_city = src.address1_city,address1_composite = src.address1_composite,address1_country = src.address1_country,address1_county = src.address1_county,address1_fax = src.address1_fax,address1_freighttermscode = src.address1_freighttermscode,address1_freighttermscodename = src.address1_freighttermscodename,address1_latitude = src.address1_latitude,address1_line1 = src.address1_line1,address1_line2 = src.address1_line2,address1_line3 = src.address1_line3,address1_longitude = src.address1_longitude,address1_name = src.address1_name,address1_postalcode = src.address1_postalcode,address1_postofficebox = src.address1_postofficebox,address1_primarycontactname = src.address1_primarycontactname,address1_shippingmethodcode = src.address1_shippingmethodcode,address1_shippingmethodcodename = src.address1_shippingmethodcodename,address1_stateorprovince = src.address1_stateorprovince,address1_telephone1 = src.address1_telephone1,address1_telephone2 = src.address1_telephone2,address1_telephone3 = src.address1_telephone3,address1_upszone = src.address1_upszone,address1_utcoffset = src.address1_utcoffset,address2_addressid = src.address2_addressid,address2_addresstypecode = src.address2_addresstypecode,address2_addresstypecodename = src.address2_addresstypecodename,address2_city = src.address2_city,address2_composite = src.address2_composite,address2_country = src.address2_country,address2_county = src.address2_county,address2_fax = src.address2_fax,address2_freighttermscode = src.address2_freighttermscode,address2_freighttermscodename = src.address2_freighttermscodename,address2_latitude = src.address2_latitude,address2_line1 = src.address2_line1,address2_line2 = src.address2_line2,address2_line3 = src.address2_line3,address2_longitude = src.address2_longitude,address2_name = src.address2_name,address2_postalcode = src.address2_postalcode,address2_postofficebox = src.address2_postofficebox,address2_primarycontactname = src.address2_primarycontactname,address2_shippingmethodcode = src.address2_shippingmethodcode,address2_shippingmethodcodename = src.address2_shippingmethodcodename,address2_stateorprovince = src.address2_stateorprovince,address2_telephone1 = src.address2_telephone1,address2_telephone2 = src.address2_telephone2,address2_telephone3 = src.address2_telephone3,address2_upszone = src.address2_upszone,address2_utcoffset = src.address2_utcoffset,aging30 = src.aging30,aging30_base = src.aging30_base,aging60 = src.aging60,aging60_base = src.aging60_base,aging90 = src.aging90,aging90_base = src.aging90_base,businesstypecode = src.businesstypecode,businesstypecodename = src.businesstypecodename,createdby = src.createdby,createdbyexternalparty = src.createdbyexternalparty,createdbyexternalpartyname = src.createdbyexternalpartyname,createdbyexternalpartyyominame = src.createdbyexternalpartyyominame,createdbyname = src.createdbyname,createdbyyominame = src.createdbyyominame,createdon = src.createdon,createdonbehalfby = src.createdonbehalfby,createdonbehalfbyname = src.createdonbehalfbyname,createdonbehalfbyyominame = src.createdonbehalfbyyominame,creditlimit = src.creditlimit,creditlimit_base = src.creditlimit_base,creditonhold = src.creditonhold,creditonholdname = src.creditonholdname,customersizecode = src.customersizecode,customersizecodename = src.customersizecodename,customertypecode = src.customertypecode,customertypecodename = src.customertypecodename,defaultpricelevelid = src.defaultpricelevelid,defaultpricelevelidname = src.defaultpricelevelidname,description = src.description,donotbulkemail = src.donotbulkemail,donotbulkemailname = src.donotbulkemailname,donotbulkpostalmail = src.donotbulkpostalmail,donotbulkpostalmailname = src.donotbulkpostalmailname,donotemail = src.donotemail,donotemailname = src.donotemailname,donotfax = src.donotfax,donotfaxname = src.donotfaxname,donotphone = src.donotphone,donotphonename = src.donotphonename,donotpostalmail = src.donotpostalmail,donotpostalmailname = src.donotpostalmailname,donotsendmarketingmaterialname = src.donotsendmarketingmaterialname,donotsendmm = src.donotsendmm,emailaddress1 = src.emailaddress1,emailaddress2 = src.emailaddress2,emailaddress3 = src.emailaddress3,entityimage = src.entityimage,entityimage_timestamp = src.entityimage_timestamp,entityimage_url = src.entityimage_url,entityimageid = src.entityimageid,exchangerate = src.exchangerate,fax = src.fax,ftpsiteurl = src.ftpsiteurl,importsequencenumber = src.importsequencenumber,industrycode = src.industrycode,industrycodename = src.industrycodename,isprivatename = src.isprivatename,lastonholdtime = src.lastonholdtime,lastusedincampaign = src.lastusedincampaign,marketcap = src.marketcap,marketcap_base = src.marketcap_base,masteraccountidname = src.masteraccountidname,masteraccountidyominame = src.masteraccountidyominame,masterid = src.masterid,merged = src.merged,mergedname = src.mergedname,modifiedby = src.modifiedby,modifiedbyexternalparty = src.modifiedbyexternalparty,modifiedbyexternalpartyname = src.modifiedbyexternalpartyname,modifiedbyexternalpartyyominame = src.modifiedbyexternalpartyyominame,modifiedbyname = src.modifiedbyname,modifiedbyyominame = src.modifiedbyyominame,modifiedon = src.modifiedon,modifiedonbehalfby = src.modifiedonbehalfby,modifiedonbehalfbyname = src.modifiedonbehalfbyname,modifiedonbehalfbyyominame = src.modifiedonbehalfbyyominame,name = src.name,numberofemployees = src.numberofemployees,onholdtime = src.onholdtime,opendeals = src.opendeals,opendeals_date = src.opendeals_date,opendeals_state = src.opendeals_state,openrevenue = src.openrevenue,openrevenue_base = src.openrevenue_base,openrevenue_date = src.openrevenue_date,openrevenue_state = src.openrevenue_state,originatingleadid = src.originatingleadid,originatingleadidname = src.originatingleadidname,originatingleadidyominame = src.originatingleadidyominame,overriddencreatedon = src.overriddencreatedon,ownerid = src.ownerid,owneridname = src.owneridname,owneridtype = src.owneridtype,owneridyominame = src.owneridyominame,ownershipcode = src.ownershipcode,ownershipcodename = src.ownershipcodename,owningbusinessunit = src.owningbusinessunit,owningteam = src.owningteam,owninguser = src.owninguser,parentaccountid = src.parentaccountid,parentaccountidname = src.parentaccountidname,parentaccountidyominame = src.parentaccountidyominame,participatesinworkflow = src.participatesinworkflow,participatesinworkflowname = src.participatesinworkflowname,paymenttermscode = src.paymenttermscode,paymenttermscodename = src.paymenttermscodename,phg_accountdirectorid = src.phg_accountdirectorid,phg_accountdirectoridname = src.phg_accountdirectoridname,phg_accountdirectoridyominame = src.phg_accountdirectoridyominame,phg_accountspayableid = src.phg_accountspayableid,phg_accountspayableidname = src.phg_accountspayableidname,phg_accountspayableidyominame = src.phg_accountspayableidyominame,phg_accountsreceivablecontactid = src.phg_accountsreceivablecontactid,phg_accountsreceivablecontactidname = src.phg_accountsreceivablecontactidname,phg_accountsreceivablecontactidyominame = src.phg_accountsreceivablecontactidyominame,phg_alliancepartner = src.phg_alliancepartner,phg_alliancepartnermanagerid = src.phg_alliancepartnermanagerid,phg_alliancepartnermanageridname = src.phg_alliancepartnermanageridname,phg_alliancepartnermanageridyominame = src.phg_alliancepartnermanageridyominame,phg_alliancepartnername = src.phg_alliancepartnername,phg_alliancepartnerstatus = src.phg_alliancepartnerstatus,phg_alliancepartnerstatusname = src.phg_alliancepartnerstatusname,phg_areamanagerid = src.phg_areamanagerid,phg_areamanageridname = src.phg_areamanageridname,phg_areamanageridyominame = src.phg_areamanageridyominame,phg_brand = src.phg_brand,phg_brandname = src.phg_brandname,phg_businesstermsoverview = src.phg_businesstermsoverview,phg_channelmanager = src.phg_channelmanager,phg_channelmanagername = src.phg_channelmanagername,phg_cityid = src.phg_cityid,phg_cityidname = src.phg_cityidname,phg_competitiveset = src.phg_competitiveset,phg_consortiasales = src.phg_consortiasales,phg_consortiasalesname = src.phg_consortiasalesname,phg_consortiasalesstatus = src.phg_consortiasalesstatus,phg_consortiasalesstatusname = src.phg_consortiasalesstatusname,phg_contractenddate = src.phg_contractenddate,phg_contractstartdate = src.phg_contractstartdate,phg_corporateaccount = src.phg_corporateaccount,phg_corporateaccountname = src.phg_corporateaccountname,phg_corporateglobalaccountmanagerid = src.phg_corporateglobalaccountmanagerid,phg_corporateglobalaccountmanageridname = src.phg_corporateglobalaccountmanageridname,phg_corporateglobalaccountmanageridyominame = src.phg_corporateglobalaccountmanageridyominame,phg_corporatermamericasid = src.phg_corporatermamericasid,phg_corporatermamericasidname = src.phg_corporatermamericasidname,phg_corporatermamericasidyominame = src.phg_corporatermamericasidyominame,phg_corporatermaspacid = src.phg_corporatermaspacid,phg_corporatermaspacidname = src.phg_corporatermaspacidname,phg_corporatermaspacidyominame = src.phg_corporatermaspacidyominame,phg_corporatermemeaid = src.phg_corporatermemeaid,phg_corporatermemeaidname = src.phg_corporatermemeaidname,phg_corporatermemeaidyominame = src.phg_corporatermemeaidyominame,phg_corporatesalesstatus = src.phg_corporatesalesstatus,phg_corporatesalesstatusname = src.phg_corporatesalesstatusname,phg_countryid = src.phg_countryid,phg_countryidname = src.phg_countryidname,phg_cruisecompany = src.phg_cruisecompany,phg_cruisecompanyname = src.phg_cruisecompanyname,phg_cruiseship = src.phg_cruiseship,phg_cruiseshipname = src.phg_cruiseshipname,phg_currencycode = src.phg_currencycode,phg_customerdirectid = src.phg_customerdirectid,phg_distributiontitle = src.phg_distributiontitle,phg_exceptionnotes = src.phg_exceptionnotes,phg_executiveownerid = src.phg_executiveownerid,phg_executiveowneridname = src.phg_executiveowneridname,phg_executiveowneridyominame = src.phg_executiveowneridyominame,phg_executivevicepresidentid = src.phg_executivevicepresidentid,phg_executivevicepresidentidname = src.phg_executivevicepresidentidname,phg_executivevicepresidentidyominame = src.phg_executivevicepresidentidyominame,phg_finaltermination = src.phg_finaltermination,phg_finalterminationname = src.phg_finalterminationname,phg_finalterminationnotes = src.phg_finalterminationnotes,phg_financeregion = src.phg_financeregion,phg_fleetsize = src.phg_fleetsize,phg_floors = src.phg_floors,phg_frontdeskemailaddress = src.phg_frontdeskemailaddress,phg_gdsamadeuschaincode = src.phg_gdsamadeuschaincode,phg_gdsamadeusgdscode = src.phg_gdsamadeusgdscode,phg_gdsgalileoapollochaincode = src.phg_gdsgalileoapollochaincode,phg_gdsgalileoapollogdscode = src.phg_gdsgalileoapollogdscode,phg_gdspegasusids = src.phg_gdspegasusids,phg_gdspegasusidschaincode = src.phg_gdspegasusidschaincode,phg_gdssabrechaincode = src.phg_gdssabrechaincode,phg_gdssabregdscode = src.phg_gdssabregdscode,phg_gdsworldspanchaincode = src.phg_gdsworldspanchaincode,phg_gdsworldspangdscode = src.phg_gdsworldspangdscode,phg_groupaccount = src.phg_groupaccount,phg_groupaccountname = src.phg_groupaccountname,phg_groupsalesdirectorid = src.phg_groupsalesdirectorid,phg_groupsalesdirectoridname = src.phg_groupsalesdirectoridname,phg_groupsalesdirectoridyominame = src.phg_groupsalesdirectoridyominame,phg_groupsalesstatus = src.phg_groupsalesstatus,phg_groupsalesstatusname = src.phg_groupsalesstatusname,phg_hospitalitydivision = src.phg_hospitalitydivision,phg_hospitalitydivisionname = src.phg_hospitalitydivisionname,phg_hotelaccount = src.phg_hotelaccount,phg_hotelaccountname = src.phg_hotelaccountname,phg_hotelconsultancyassetmanagement = src.phg_hotelconsultancyassetmanagement,phg_hotelconsultancyassetmanagementname = src.phg_hotelconsultancyassetmanagementname,phg_hotelfinancecompany = src.phg_hotelfinancecompany,phg_hotelfinancecompanyname = src.phg_hotelfinancecompanyname,phg_hotelmanagementcompany = src.phg_hotelmanagementcompany,phg_hotelmanagementcompanyname = src.phg_hotelmanagementcompanyname,phg_hotelownersanddevelopers = src.phg_hotelownersanddevelopers,phg_hotelownersanddevelopersname = src.phg_hotelownersanddevelopersname,phg_hotelregion = src.phg_hotelregion,phg_hotelregionname = src.phg_hotelregionname,phg_hoteltotalrooms = src.phg_hoteltotalrooms,phg_iataaccount = src.phg_iataaccount,phg_iataaccountname = src.phg_iataaccountname,phg_iataglobalaccountmanagerid = src.phg_iataglobalaccountmanagerid,phg_iataglobalaccountmanageridname = src.phg_iataglobalaccountmanageridname,phg_iataglobalaccountmanageridyominame = src.phg_iataglobalaccountmanageridyominame,phg_ibecompany = src.phg_ibecompany,phg_ibecompanyname = src.phg_ibecompanyname,phg_invoicepastduestatus = src.phg_invoicepastduestatus,phg_invoicepastduestatusname = src.phg_invoicepastduestatusname,phg_ipreferexecutiveadminid = src.phg_ipreferexecutiveadminid,phg_ipreferexecutiveadminidname = src.phg_ipreferexecutiveadminidname,phg_ipreferexecutiveadminidyominame = src.phg_ipreferexecutiveadminidyominame,phg_iprefergoal = src.phg_iprefergoal,phg_iprefergoalmanagerid = src.phg_iprefergoalmanagerid,phg_iprefergoalmanageridname = src.phg_iprefergoalmanageridname,phg_iprefergoalmanageridyominame = src.phg_iprefergoalmanageridyominame,phg_legalentity = src.phg_legalentity,phg_leisuresales = src.phg_leisuresales,phg_leisuresalesname = src.phg_leisuresalesname,phg_leisuresalesstatus = src.phg_leisuresalesstatus,phg_leisuresalesstatusname = src.phg_leisuresalesstatusname,phg_marketingcontactid = src.phg_marketingcontactid,phg_marketingcontactidname = src.phg_marketingcontactidname,phg_marketingcontactidyominame = src.phg_marketingcontactidyominame,phg_marketinglocation = src.phg_marketinglocation,phg_marketingmanagerid = src.phg_marketingmanagerid,phg_marketingmanageridname = src.phg_marketingmanageridname,phg_marketingmanageridyominame = src.phg_marketingmanageridyominame,phg_marketingname = src.phg_marketingname,phg_memberportalimportoverride = src.phg_memberportalimportoverride,phg_memberportalimportoverridename = src.phg_memberportalimportoverridename,phg_numberofcabins = src.phg_numberofcabins,phg_onlinebookingreward = src.phg_onlinebookingreward,phg_openingdate = src.phg_openingdate,phg_operationsmanagerid = src.phg_operationsmanagerid,phg_operationsmanageridname = src.phg_operationsmanageridname,phg_operationsmanageridyominame = src.phg_operationsmanageridyominame,phg_phgcontactid = src.phg_phgcontactid,phg_phgcontactidname = src.phg_phgcontactidname,phg_phgcontactidyominame = src.phg_phgcontactidyominame,phg_pmsprovider = src.phg_pmsprovider,phg_pmsprovidername = src.phg_pmsprovidername,phg_primaryaccountmanagerid = src.phg_primaryaccountmanagerid,phg_primaryaccountmanageridname = src.phg_primaryaccountmanageridname,phg_primaryaccountmanageridyominame = src.phg_primaryaccountmanageridyominame,phg_propertiesuseservice = src.phg_propertiesuseservice,phg_propertiesuseservicename = src.phg_propertiesuseservicename,phg_rategainid = src.phg_rategainid,phg_regionaladministrationid = src.phg_regionaladministrationid,phg_regionaladministrationidname = src.phg_regionaladministrationidname,phg_regionaladministrationidyominame = src.phg_regionaladministrationidyominame,phg_regionaldistribution = src.phg_regionaldistribution,phg_regionaldistributionname = src.phg_regionaldistributionname,phg_regionaldistributionvalue = src.phg_regionaldistributionvalue,phg_regionalmanagerid = src.phg_regionalmanagerid,phg_regionalmanageridname = src.phg_regionalmanageridname,phg_regionalmanageridyominame = src.phg_regionalmanageridyominame,phg_regionalsecondaryaccountmanagerid = src.phg_regionalsecondaryaccountmanagerid,phg_regionalsecondaryaccountmanageridname = src.phg_regionalsecondaryaccountmanageridname,phg_regionalsecondaryaccountmanageridyominame = src.phg_regionalsecondaryaccountmanageridyominame,phg_relationshipoverview = src.phg_relationshipoverview,phg_releatedothermanagerid = src.phg_releatedothermanagerid,phg_releatedothermanageridname = src.phg_releatedothermanageridname,phg_releatedothermanageridyominame = src.phg_releatedothermanageridyominame,phg_revenueaccountmanagerid = src.phg_revenueaccountmanagerid,phg_revenueaccountmanageridname = src.phg_revenueaccountmanageridname,phg_revenueaccountmanageridyominame = src.phg_revenueaccountmanageridyominame,phg_rmsprovider = src.phg_rmsprovider,phg_rmsprovidername = src.phg_rmsprovidername,phg_showinmemberportalconsortia = src.phg_showinmemberportalconsortia,phg_showinmemberportalconsortianame = src.phg_showinmemberportalconsortianame,phg_showinmemberportalcorporatesales = src.phg_showinmemberportalcorporatesales,phg_showinmemberportalcorporatesalesname = src.phg_showinmemberportalcorporatesalesname,phg_showinmemberportalgroupsales = src.phg_showinmemberportalgroupsales,phg_showinmemberportalgroupsalesname = src.phg_showinmemberportalgroupsalesname,phg_showinmemberportalleisure = src.phg_showinmemberportalleisure,phg_showinmemberportalleisurename = src.phg_showinmemberportalleisurename,phg_stateprovinceid = src.phg_stateprovinceid,phg_stateprovinceidname = src.phg_stateprovinceidname,phg_synxiscode = src.phg_synxiscode,phg_synxisid = src.phg_synxisid,phg_technicalmanagerid = src.phg_technicalmanagerid,phg_technicalmanageridname = src.phg_technicalmanageridname,phg_technicalmanageridyominame = src.phg_technicalmanageridyominame,phg_touroperatoraccount = src.phg_touroperatoraccount,phg_touroperatoraccountname = src.phg_touroperatoraccountname,phg_touroperatorglobalaccountmanagerid = src.phg_touroperatorglobalaccountmanagerid,phg_touroperatorglobalaccountmanageridname = src.phg_touroperatorglobalaccountmanageridname,phg_touroperatorglobalaccountmanageridyominame = src.phg_touroperatorglobalaccountmanageridyominame,phg_travelagencygroup = src.phg_travelagencygroup,phg_travelagencygroupname = src.phg_travelagencygroupname,phg_typeofvendor = src.phg_typeofvendor,phg_typeofvendorname = src.phg_typeofvendorname,phg_underconstructionenddate = src.phg_underconstructionenddate,phg_underconstructionnotes = src.phg_underconstructionnotes,phg_underconstructionorrenovation = src.phg_underconstructionorrenovation,phg_underconstructionorrenovationname = src.phg_underconstructionorrenovationname,phg_underconstructionstartdate = src.phg_underconstructionstartdate,phg_vendoraccount = src.phg_vendoraccount,phg_vendoraccountname = src.phg_vendoraccountname,phg_yearbuilt = src.phg_yearbuilt,phg_yearlastrenovated = src.phg_yearlastrenovated,preferredappointmentdaycode = src.preferredappointmentdaycode,preferredappointmentdaycodename = src.preferredappointmentdaycodename,preferredappointmenttimecode = src.preferredappointmenttimecode,preferredappointmenttimecodename = src.preferredappointmenttimecodename,preferredcontactmethodcode = src.preferredcontactmethodcode,preferredcontactmethodcodename = src.preferredcontactmethodcodename,preferredequipmentid = src.preferredequipmentid,preferredequipmentidname = src.preferredequipmentidname,preferredserviceid = src.preferredserviceid,preferredserviceidname = src.preferredserviceidname,preferredsystemuserid = src.preferredsystemuserid,preferredsystemuseridname = src.preferredsystemuseridname,preferredsystemuseridyominame = src.preferredsystemuseridyominame,primarycontactid = src.primarycontactid,primarycontactidname = src.primarycontactidname,primarycontactidyominame = src.primarycontactidyominame,primarysatoriid = src.primarysatoriid,primarytwitterid = src.primarytwitterid,processid = src.processid,revenue = src.revenue,revenue_base = src.revenue_base,sharesoutstanding = src.sharesoutstanding,shippingmethodcode = src.shippingmethodcode,shippingmethodcodename = src.shippingmethodcodename,sic = src.sic,slaid = src.slaid,slainvokedid = src.slainvokedid,slainvokedidname = src.slainvokedidname,slaname = src.slaname,stageid = src.stageid,statecode = src.statecode,statecodename = src.statecodename,statuscode = src.statuscode,statuscodename = src.statuscodename,stockexchange = src.stockexchange,telephone1 = src.telephone1,telephone2 = src.telephone2,telephone3 = src.telephone3,territorycode = src.territorycode,territorycodename = src.territorycodename,territoryid = src.territoryid,territoryidname = src.territoryidname,tickersymbol = src.tickersymbol,timezoneruleversionnumber = src.timezoneruleversionnumber,transactioncurrencyid = src.transactioncurrencyid,transactioncurrencyidname = src.transactioncurrencyidname,traversedpath = src.traversedpath,utcconversiontimezonecode = src.utcconversiontimezonecode,versionnumber = src.versionnumber,websiteurl = src.websiteurl,yominame = src.yominame,phg_legalentityname = src.phg_legalentityname,phg_EnterpriseAccountLead = src.phg_EnterpriseAccountLead,phg_EnterpriseAccountLeadName = src.phg_EnterpriseAccountLeadName,phg_EnterpriseAccountLeadYomiName = src.phg_EnterpriseAccountLeadYomiName,phg_EnterpriseRevenueLead = src.phg_EnterpriseRevenueLead,phg_EnterpriseRevenueLeadName = src.phg_EnterpriseRevenueLeadName,phg_EnterpriseRevenueLeadYomiName = src.phg_EnterpriseRevenueLeadYomiName,
					[phg_openHospitalityCode] = src.[phg_openHospitalityCode],[phg_openHospitalityNumber] = src.[phg_openHospitalityNumber],[phg_phgMeetingsCom] = src.[phg_phgMeetingsCom],[phg_phgMeetingsComName] = src.[phg_phgMeetingsComName],[phg_phgMeetingsEndDate] = src.[phg_phgMeetingsEndDate],[phg_phgMeetingsStartDate] = src.[phg_phgMeetingsStartDate]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([accountcategorycode],[accountcategorycodename],[accountclassificationcode],[accountclassificationcodename],[accountid],[accountnumber],[accountratingcode],[accountratingcodename],[address1_addressid],[address1_addresstypecode],[address1_addresstypecodename],[address1_city],[address1_composite],[address1_country],[address1_county],[address1_fax],[address1_freighttermscode],[address1_freighttermscodename],[address1_latitude],[address1_line1],[address1_line2],[address1_line3],[address1_longitude],[address1_name],[address1_postalcode],[address1_postofficebox],[address1_primarycontactname],[address1_shippingmethodcode],[address1_shippingmethodcodename],[address1_stateorprovince],[address1_telephone1],[address1_telephone2],[address1_telephone3],[address1_upszone],[address1_utcoffset],[address2_addressid],[address2_addresstypecode],[address2_addresstypecodename],[address2_city],[address2_composite],[address2_country],[address2_county],[address2_fax],[address2_freighttermscode],[address2_freighttermscodename],[address2_latitude],[address2_line1],[address2_line2],[address2_line3],[address2_longitude],[address2_name],[address2_postalcode],[address2_postofficebox],[address2_primarycontactname],[address2_shippingmethodcode],[address2_shippingmethodcodename],[address2_stateorprovince],[address2_telephone1],[address2_telephone2],[address2_telephone3],[address2_upszone],[address2_utcoffset],[aging30],[aging30_base],[aging60],[aging60_base],[aging90],[aging90_base],[businesstypecode],[businesstypecodename],[createdby],[createdbyexternalparty],[createdbyexternalpartyname],[createdbyexternalpartyyominame],[createdbyname],[createdbyyominame],[createdon],[createdonbehalfby],[createdonbehalfbyname],[createdonbehalfbyyominame],[creditlimit],[creditlimit_base],[creditonhold],[creditonholdname],[customersizecode],[customersizecodename],[customertypecode],[customertypecodename],[defaultpricelevelid],[defaultpricelevelidname],[description],[donotbulkemail],[donotbulkemailname],[donotbulkpostalmail],[donotbulkpostalmailname],[donotemail],[donotemailname],[donotfax],[donotfaxname],[donotphone],[donotphonename],[donotpostalmail],[donotpostalmailname],[donotsendmarketingmaterialname],[donotsendmm],[emailaddress1],[emailaddress2],[emailaddress3],[entityimage],[entityimage_timestamp],[entityimage_url],[entityimageid],[exchangerate],[fax],[ftpsiteurl],[importsequencenumber],[industrycode],[industrycodename],[isprivatename],[lastonholdtime],[lastusedincampaign],[marketcap],[marketcap_base],[masteraccountidname],[masteraccountidyominame],[masterid],[merged],[mergedname],[modifiedby],[modifiedbyexternalparty],[modifiedbyexternalpartyname],[modifiedbyexternalpartyyominame],[modifiedbyname],[modifiedbyyominame],[modifiedon],[modifiedonbehalfby],[modifiedonbehalfbyname],[modifiedonbehalfbyyominame],[name],[numberofemployees],[onholdtime],[opendeals],[opendeals_date],[opendeals_state],[openrevenue],[openrevenue_base],[openrevenue_date],[openrevenue_state],[originatingleadid],[originatingleadidname],[originatingleadidyominame],[overriddencreatedon],[ownerid],[owneridname],[owneridtype],[owneridyominame],[ownershipcode],[ownershipcodename],[owningbusinessunit],[owningteam],[owninguser],[parentaccountid],[parentaccountidname],[parentaccountidyominame],[participatesinworkflow],[participatesinworkflowname],[paymenttermscode],[paymenttermscodename],[phg_accountdirectorid],[phg_accountdirectoridname],[phg_accountdirectoridyominame],[phg_accountspayableid],[phg_accountspayableidname],[phg_accountspayableidyominame],[phg_accountsreceivablecontactid],[phg_accountsreceivablecontactidname],[phg_accountsreceivablecontactidyominame],[phg_alliancepartner],[phg_alliancepartnermanagerid],[phg_alliancepartnermanageridname],[phg_alliancepartnermanageridyominame],[phg_alliancepartnername],[phg_alliancepartnerstatus],[phg_alliancepartnerstatusname],[phg_areamanagerid],[phg_areamanageridname],[phg_areamanageridyominame],[phg_brand],[phg_brandname],[phg_businesstermsoverview],[phg_channelmanager],[phg_channelmanagername],[phg_cityid],[phg_cityidname],[phg_competitiveset],[phg_consortiasales],[phg_consortiasalesname],[phg_consortiasalesstatus],[phg_consortiasalesstatusname],[phg_contractenddate],[phg_contractstartdate],[phg_corporateaccount],[phg_corporateaccountname],[phg_corporateglobalaccountmanagerid],[phg_corporateglobalaccountmanageridname],[phg_corporateglobalaccountmanageridyominame],[phg_corporatermamericasid],[phg_corporatermamericasidname],[phg_corporatermamericasidyominame],[phg_corporatermaspacid],[phg_corporatermaspacidname],[phg_corporatermaspacidyominame],[phg_corporatermemeaid],[phg_corporatermemeaidname],[phg_corporatermemeaidyominame],[phg_corporatesalesstatus],[phg_corporatesalesstatusname],[phg_countryid],[phg_countryidname],[phg_cruisecompany],[phg_cruisecompanyname],[phg_cruiseship],[phg_cruiseshipname],[phg_currencycode],[phg_customerdirectid],[phg_distributiontitle],[phg_exceptionnotes],[phg_executiveownerid],[phg_executiveowneridname],[phg_executiveowneridyominame],[phg_executivevicepresidentid],[phg_executivevicepresidentidname],[phg_executivevicepresidentidyominame],[phg_finaltermination],[phg_finalterminationname],[phg_finalterminationnotes],[phg_financeregion],[phg_fleetsize],[phg_floors],[phg_frontdeskemailaddress],[phg_gdsamadeuschaincode],[phg_gdsamadeusgdscode],[phg_gdsgalileoapollochaincode],[phg_gdsgalileoapollogdscode],[phg_gdspegasusids],[phg_gdspegasusidschaincode],[phg_gdssabrechaincode],[phg_gdssabregdscode],[phg_gdsworldspanchaincode],[phg_gdsworldspangdscode],[phg_groupaccount],[phg_groupaccountname],[phg_groupsalesdirectorid],[phg_groupsalesdirectoridname],[phg_groupsalesdirectoridyominame],[phg_groupsalesstatus],[phg_groupsalesstatusname],[phg_hospitalitydivision],[phg_hospitalitydivisionname],[phg_hotelaccount],[phg_hotelaccountname],[phg_hotelconsultancyassetmanagement],[phg_hotelconsultancyassetmanagementname],[phg_hotelfinancecompany],[phg_hotelfinancecompanyname],[phg_hotelmanagementcompany],[phg_hotelmanagementcompanyname],[phg_hotelownersanddevelopers],[phg_hotelownersanddevelopersname],[phg_hotelregion],[phg_hotelregionname],[phg_hoteltotalrooms],[phg_iataaccount],[phg_iataaccountname],[phg_iataglobalaccountmanagerid],[phg_iataglobalaccountmanageridname],[phg_iataglobalaccountmanageridyominame],[phg_ibecompany],[phg_ibecompanyname],[phg_invoicepastduestatus],[phg_invoicepastduestatusname],[phg_ipreferexecutiveadminid],[phg_ipreferexecutiveadminidname],[phg_ipreferexecutiveadminidyominame],[phg_iprefergoal],[phg_iprefergoalmanagerid],[phg_iprefergoalmanageridname],[phg_iprefergoalmanageridyominame],[phg_legalentity],[phg_leisuresales],[phg_leisuresalesname],[phg_leisuresalesstatus],[phg_leisuresalesstatusname],[phg_marketingcontactid],[phg_marketingcontactidname],[phg_marketingcontactidyominame],[phg_marketinglocation],[phg_marketingmanagerid],[phg_marketingmanageridname],[phg_marketingmanageridyominame],[phg_marketingname],[phg_memberportalimportoverride],[phg_memberportalimportoverridename],[phg_numberofcabins],[phg_onlinebookingreward],[phg_openingdate],[phg_operationsmanagerid],[phg_operationsmanageridname],[phg_operationsmanageridyominame],[phg_phgcontactid],[phg_phgcontactidname],[phg_phgcontactidyominame],[phg_pmsprovider],[phg_pmsprovidername],[phg_primaryaccountmanagerid],[phg_primaryaccountmanageridname],[phg_primaryaccountmanageridyominame],[phg_propertiesuseservice],[phg_propertiesuseservicename],[phg_rategainid],[phg_regionaladministrationid],[phg_regionaladministrationidname],[phg_regionaladministrationidyominame],[phg_regionaldistribution],[phg_regionaldistributionname],[phg_regionaldistributionvalue],[phg_regionalmanagerid],[phg_regionalmanageridname],[phg_regionalmanageridyominame],[phg_regionalsecondaryaccountmanagerid],[phg_regionalsecondaryaccountmanageridname],[phg_regionalsecondaryaccountmanageridyominame],[phg_relationshipoverview],[phg_releatedothermanagerid],[phg_releatedothermanageridname],[phg_releatedothermanageridyominame],[phg_revenueaccountmanagerid],[phg_revenueaccountmanageridname],[phg_revenueaccountmanageridyominame],[phg_rmsprovider],[phg_rmsprovidername],[phg_showinmemberportalconsortia],[phg_showinmemberportalconsortianame],[phg_showinmemberportalcorporatesales],[phg_showinmemberportalcorporatesalesname],[phg_showinmemberportalgroupsales],[phg_showinmemberportalgroupsalesname],[phg_showinmemberportalleisure],[phg_showinmemberportalleisurename],[phg_stateprovinceid],[phg_stateprovinceidname],[phg_synxiscode],[phg_synxisid],[phg_technicalmanagerid],[phg_technicalmanageridname],[phg_technicalmanageridyominame],[phg_touroperatoraccount],[phg_touroperatoraccountname],[phg_touroperatorglobalaccountmanagerid],[phg_touroperatorglobalaccountmanageridname],[phg_touroperatorglobalaccountmanageridyominame],[phg_travelagencygroup],[phg_travelagencygroupname],[phg_typeofvendor],[phg_typeofvendorname],[phg_underconstructionenddate],[phg_underconstructionnotes],[phg_underconstructionorrenovation],[phg_underconstructionorrenovationname],[phg_underconstructionstartdate],[phg_vendoraccount],[phg_vendoraccountname],[phg_yearbuilt],[phg_yearlastrenovated],[preferredappointmentdaycode],[preferredappointmentdaycodename],[preferredappointmenttimecode],[preferredappointmenttimecodename],[preferredcontactmethodcode],[preferredcontactmethodcodename],[preferredequipmentid],[preferredequipmentidname],[preferredserviceid],[preferredserviceidname],[preferredsystemuserid],[preferredsystemuseridname],[preferredsystemuseridyominame],[primarycontactid],[primarycontactidname],[primarycontactidyominame],[primarysatoriid],[primarytwitterid],[processid],[revenue],[revenue_base],[sharesoutstanding],[shippingmethodcode],[shippingmethodcodename],[sic],[slaid],[slainvokedid],[slainvokedidname],[slaname],[stageid],[statecode],[statecodename],[statuscode],[statuscodename],[stockexchange],[telephone1],[telephone2],[telephone3],[territorycode],[territorycodename],[territoryid],[territoryidname],[tickersymbol],[timezoneruleversionnumber],[transactioncurrencyid],[transactioncurrencyidname],[traversedpath],[utcconversiontimezonecode],[versionnumber],[websiteurl],[yominame],[phg_legalentityname],[phg_EnterpriseAccountLead],[phg_EnterpriseAccountLeadName],[phg_EnterpriseAccountLeadYomiName],[phg_EnterpriseRevenueLead],[phg_EnterpriseRevenueLeadName],[phg_EnterpriseRevenueLeadYomiName],
					[phg_openHospitalityCode],[phg_openHospitalityNumber],[phg_phgMeetingsCom],[phg_phgMeetingsComName],[phg_phgMeetingsEndDate],[phg_phgMeetingsStartDate])
			VALUES(src.[accountcategorycode],src.[accountcategorycodename],src.[accountclassificationcode],src.[accountclassificationcodename],src.[accountid],src.[accountnumber],src.[accountratingcode],src.[accountratingcodename],src.[address1_addressid],src.[address1_addresstypecode],src.[address1_addresstypecodename],src.[address1_city],src.[address1_composite],src.[address1_country],src.[address1_county],src.[address1_fax],src.[address1_freighttermscode],src.[address1_freighttermscodename],src.[address1_latitude],src.[address1_line1],src.[address1_line2],src.[address1_line3],src.[address1_longitude],src.[address1_name],src.[address1_postalcode],src.[address1_postofficebox],src.[address1_primarycontactname],src.[address1_shippingmethodcode],src.[address1_shippingmethodcodename],src.[address1_stateorprovince],src.[address1_telephone1],src.[address1_telephone2],src.[address1_telephone3],src.[address1_upszone],src.[address1_utcoffset],src.[address2_addressid],src.[address2_addresstypecode],src.[address2_addresstypecodename],src.[address2_city],src.[address2_composite],src.[address2_country],src.[address2_county],src.[address2_fax],src.[address2_freighttermscode],src.[address2_freighttermscodename],src.[address2_latitude],src.[address2_line1],src.[address2_line2],src.[address2_line3],src.[address2_longitude],src.[address2_name],src.[address2_postalcode],src.[address2_postofficebox],src.[address2_primarycontactname],src.[address2_shippingmethodcode],src.[address2_shippingmethodcodename],src.[address2_stateorprovince],src.[address2_telephone1],src.[address2_telephone2],src.[address2_telephone3],src.[address2_upszone],src.[address2_utcoffset],src.[aging30],src.[aging30_base],src.[aging60],src.[aging60_base],src.[aging90],src.[aging90_base],src.[businesstypecode],src.[businesstypecodename],src.[createdby],src.[createdbyexternalparty],src.[createdbyexternalpartyname],src.[createdbyexternalpartyyominame],src.[createdbyname],src.[createdbyyominame],src.[createdon],src.[createdonbehalfby],src.[createdonbehalfbyname],src.[createdonbehalfbyyominame],src.[creditlimit],src.[creditlimit_base],src.[creditonhold],src.[creditonholdname],src.[customersizecode],src.[customersizecodename],src.[customertypecode],src.[customertypecodename],src.[defaultpricelevelid],src.[defaultpricelevelidname],src.[description],src.[donotbulkemail],src.[donotbulkemailname],src.[donotbulkpostalmail],src.[donotbulkpostalmailname],src.[donotemail],src.[donotemailname],src.[donotfax],src.[donotfaxname],src.[donotphone],src.[donotphonename],src.[donotpostalmail],src.[donotpostalmailname],src.[donotsendmarketingmaterialname],src.[donotsendmm],src.[emailaddress1],src.[emailaddress2],src.[emailaddress3],src.[entityimage],src.[entityimage_timestamp],src.[entityimage_url],src.[entityimageid],src.[exchangerate],src.[fax],src.[ftpsiteurl],src.[importsequencenumber],src.[industrycode],src.[industrycodename],src.[isprivatename],src.[lastonholdtime],src.[lastusedincampaign],src.[marketcap],src.[marketcap_base],src.[masteraccountidname],src.[masteraccountidyominame],src.[masterid],src.[merged],src.[mergedname],src.[modifiedby],src.[modifiedbyexternalparty],src.[modifiedbyexternalpartyname],src.[modifiedbyexternalpartyyominame],src.[modifiedbyname],src.[modifiedbyyominame],src.[modifiedon],src.[modifiedonbehalfby],src.[modifiedonbehalfbyname],src.[modifiedonbehalfbyyominame],src.[name],src.[numberofemployees],src.[onholdtime],src.[opendeals],src.[opendeals_date],src.[opendeals_state],src.[openrevenue],src.[openrevenue_base],src.[openrevenue_date],src.[openrevenue_state],src.[originatingleadid],src.[originatingleadidname],src.[originatingleadidyominame],src.[overriddencreatedon],src.[ownerid],src.[owneridname],src.[owneridtype],src.[owneridyominame],src.[ownershipcode],src.[ownershipcodename],src.[owningbusinessunit],src.[owningteam],src.[owninguser],src.[parentaccountid],src.[parentaccountidname],src.[parentaccountidyominame],src.[participatesinworkflow],src.[participatesinworkflowname],src.[paymenttermscode],src.[paymenttermscodename],src.[phg_accountdirectorid],src.[phg_accountdirectoridname],src.[phg_accountdirectoridyominame],src.[phg_accountspayableid],src.[phg_accountspayableidname],src.[phg_accountspayableidyominame],src.[phg_accountsreceivablecontactid],src.[phg_accountsreceivablecontactidname],src.[phg_accountsreceivablecontactidyominame],src.[phg_alliancepartner],src.[phg_alliancepartnermanagerid],src.[phg_alliancepartnermanageridname],src.[phg_alliancepartnermanageridyominame],src.[phg_alliancepartnername],src.[phg_alliancepartnerstatus],src.[phg_alliancepartnerstatusname],src.[phg_areamanagerid],src.[phg_areamanageridname],src.[phg_areamanageridyominame],src.[phg_brand],src.[phg_brandname],src.[phg_businesstermsoverview],src.[phg_channelmanager],src.[phg_channelmanagername],src.[phg_cityid],src.[phg_cityidname],src.[phg_competitiveset],src.[phg_consortiasales],src.[phg_consortiasalesname],src.[phg_consortiasalesstatus],src.[phg_consortiasalesstatusname],src.[phg_contractenddate],src.[phg_contractstartdate],src.[phg_corporateaccount],src.[phg_corporateaccountname],src.[phg_corporateglobalaccountmanagerid],src.[phg_corporateglobalaccountmanageridname],src.[phg_corporateglobalaccountmanageridyominame],src.[phg_corporatermamericasid],src.[phg_corporatermamericasidname],src.[phg_corporatermamericasidyominame],src.[phg_corporatermaspacid],src.[phg_corporatermaspacidname],src.[phg_corporatermaspacidyominame],src.[phg_corporatermemeaid],src.[phg_corporatermemeaidname],src.[phg_corporatermemeaidyominame],src.[phg_corporatesalesstatus],src.[phg_corporatesalesstatusname],src.[phg_countryid],src.[phg_countryidname],src.[phg_cruisecompany],src.[phg_cruisecompanyname],src.[phg_cruiseship],src.[phg_cruiseshipname],src.[phg_currencycode],src.[phg_customerdirectid],src.[phg_distributiontitle],src.[phg_exceptionnotes],src.[phg_executiveownerid],src.[phg_executiveowneridname],src.[phg_executiveowneridyominame],src.[phg_executivevicepresidentid],src.[phg_executivevicepresidentidname],src.[phg_executivevicepresidentidyominame],src.[phg_finaltermination],src.[phg_finalterminationname],src.[phg_finalterminationnotes],src.[phg_financeregion],src.[phg_fleetsize],src.[phg_floors],src.[phg_frontdeskemailaddress],src.[phg_gdsamadeuschaincode],src.[phg_gdsamadeusgdscode],src.[phg_gdsgalileoapollochaincode],src.[phg_gdsgalileoapollogdscode],src.[phg_gdspegasusids],src.[phg_gdspegasusidschaincode],src.[phg_gdssabrechaincode],src.[phg_gdssabregdscode],src.[phg_gdsworldspanchaincode],src.[phg_gdsworldspangdscode],src.[phg_groupaccount],src.[phg_groupaccountname],src.[phg_groupsalesdirectorid],src.[phg_groupsalesdirectoridname],src.[phg_groupsalesdirectoridyominame],src.[phg_groupsalesstatus],src.[phg_groupsalesstatusname],src.[phg_hospitalitydivision],src.[phg_hospitalitydivisionname],src.[phg_hotelaccount],src.[phg_hotelaccountname],src.[phg_hotelconsultancyassetmanagement],src.[phg_hotelconsultancyassetmanagementname],src.[phg_hotelfinancecompany],src.[phg_hotelfinancecompanyname],src.[phg_hotelmanagementcompany],src.[phg_hotelmanagementcompanyname],src.[phg_hotelownersanddevelopers],src.[phg_hotelownersanddevelopersname],src.[phg_hotelregion],src.[phg_hotelregionname],src.[phg_hoteltotalrooms],src.[phg_iataaccount],src.[phg_iataaccountname],src.[phg_iataglobalaccountmanagerid],src.[phg_iataglobalaccountmanageridname],src.[phg_iataglobalaccountmanageridyominame],src.[phg_ibecompany],src.[phg_ibecompanyname],src.[phg_invoicepastduestatus],src.[phg_invoicepastduestatusname],src.[phg_ipreferexecutiveadminid],src.[phg_ipreferexecutiveadminidname],src.[phg_ipreferexecutiveadminidyominame],src.[phg_iprefergoal],src.[phg_iprefergoalmanagerid],src.[phg_iprefergoalmanageridname],src.[phg_iprefergoalmanageridyominame],src.[phg_legalentity],src.[phg_leisuresales],src.[phg_leisuresalesname],src.[phg_leisuresalesstatus],src.[phg_leisuresalesstatusname],src.[phg_marketingcontactid],src.[phg_marketingcontactidname],src.[phg_marketingcontactidyominame],src.[phg_marketinglocation],src.[phg_marketingmanagerid],src.[phg_marketingmanageridname],src.[phg_marketingmanageridyominame],src.[phg_marketingname],src.[phg_memberportalimportoverride],src.[phg_memberportalimportoverridename],src.[phg_numberofcabins],src.[phg_onlinebookingreward],src.[phg_openingdate],src.[phg_operationsmanagerid],src.[phg_operationsmanageridname],src.[phg_operationsmanageridyominame],src.[phg_phgcontactid],src.[phg_phgcontactidname],src.[phg_phgcontactidyominame],src.[phg_pmsprovider],src.[phg_pmsprovidername],src.[phg_primaryaccountmanagerid],src.[phg_primaryaccountmanageridname],src.[phg_primaryaccountmanageridyominame],src.[phg_propertiesuseservice],src.[phg_propertiesuseservicename],src.[phg_rategainid],src.[phg_regionaladministrationid],src.[phg_regionaladministrationidname],src.[phg_regionaladministrationidyominame],src.[phg_regionaldistribution],src.[phg_regionaldistributionname],src.[phg_regionaldistributionvalue],src.[phg_regionalmanagerid],src.[phg_regionalmanageridname],src.[phg_regionalmanageridyominame],src.[phg_regionalsecondaryaccountmanagerid],src.[phg_regionalsecondaryaccountmanageridname],src.[phg_regionalsecondaryaccountmanageridyominame],src.[phg_relationshipoverview],src.[phg_releatedothermanagerid],src.[phg_releatedothermanageridname],src.[phg_releatedothermanageridyominame],src.[phg_revenueaccountmanagerid],src.[phg_revenueaccountmanageridname],src.[phg_revenueaccountmanageridyominame],src.[phg_rmsprovider],src.[phg_rmsprovidername],src.[phg_showinmemberportalconsortia],src.[phg_showinmemberportalconsortianame],src.[phg_showinmemberportalcorporatesales],src.[phg_showinmemberportalcorporatesalesname],src.[phg_showinmemberportalgroupsales],src.[phg_showinmemberportalgroupsalesname],src.[phg_showinmemberportalleisure],src.[phg_showinmemberportalleisurename],src.[phg_stateprovinceid],src.[phg_stateprovinceidname],src.[phg_synxiscode],src.[phg_synxisid],src.[phg_technicalmanagerid],src.[phg_technicalmanageridname],src.[phg_technicalmanageridyominame],src.[phg_touroperatoraccount],src.[phg_touroperatoraccountname],src.[phg_touroperatorglobalaccountmanagerid],src.[phg_touroperatorglobalaccountmanageridname],src.[phg_touroperatorglobalaccountmanageridyominame],src.[phg_travelagencygroup],src.[phg_travelagencygroupname],src.[phg_typeofvendor],src.[phg_typeofvendorname],src.[phg_underconstructionenddate],src.[phg_underconstructionnotes],src.[phg_underconstructionorrenovation],src.[phg_underconstructionorrenovationname],src.[phg_underconstructionstartdate],src.[phg_vendoraccount],src.[phg_vendoraccountname],src.[phg_yearbuilt],src.[phg_yearlastrenovated],src.[preferredappointmentdaycode],src.[preferredappointmentdaycodename],src.[preferredappointmenttimecode],src.[preferredappointmenttimecodename],src.[preferredcontactmethodcode],src.[preferredcontactmethodcodename],src.[preferredequipmentid],src.[preferredequipmentidname],src.[preferredserviceid],src.[preferredserviceidname],src.[preferredsystemuserid],src.[preferredsystemuseridname],src.[preferredsystemuseridyominame],src.[primarycontactid],src.[primarycontactidname],src.[primarycontactidyominame],src.[primarysatoriid],src.[primarytwitterid],src.[processid],src.[revenue],src.[revenue_base],src.[sharesoutstanding],src.[shippingmethodcode],src.[shippingmethodcodename],src.[sic],src.[slaid],src.[slainvokedid],src.[slainvokedidname],src.[slaname],src.[stageid],src.[statecode],src.[statecodename],src.[statuscode],src.[statuscodename],src.[stockexchange],src.[telephone1],src.[telephone2],src.[telephone3],src.[territorycode],src.[territorycodename],src.[territoryid],src.[territoryidname],src.[tickersymbol],src.[timezoneruleversionnumber],src.[transactioncurrencyid],src.[transactioncurrencyidname],src.[traversedpath],src.[utcconversiontimezonecode],src.[versionnumber],src.[websiteurl],src.[yominame],src.[phg_legalentityname],src.[phg_EnterpriseAccountLead],src.[phg_EnterpriseAccountLeadName],src.[phg_EnterpriseAccountLeadYomiName],src.[phg_EnterpriseRevenueLead],src.[phg_EnterpriseRevenueLeadName],src.[phg_EnterpriseRevenueLeadYomiName],
					src.[phg_openHospitalityCode],src.[phg_openHospitalityNumber],src.[phg_phgMeetingsCom],src.[phg_phgMeetingsComName],src.[phg_phgMeetingsEndDate],src.[phg_phgMeetingsStartDate])
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [staging].[phg_experiences]'
GO
CREATE TABLE [staging].[phg_experiences]
(
[createdBy] [uniqueidentifier] NULL,
[createdByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOn] [datetime] NULL,
[createdOnBehalfBy] [uniqueidentifier] NULL,
[createdOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importSequenceNumber] [int] NULL,
[modifiedBy] [uniqueidentifier] NULL,
[modifiedByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOn] [datetime] NULL,
[modifiedOnBehalfBy] [uniqueidentifier] NULL,
[modifiedOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[organizationId] [uniqueidentifier] NULL,
[organizationIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[overriddenCreatedOn] [datetime] NULL,
[phg_account] [uniqueidentifier] NULL,
[phg_accountName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_accountNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_accountYomiName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_endDate] [datetime] NULL,
[phg_experiencesId] [uniqueidentifier] NULL,
[phg_experiencesType] [int] NULL,
[phg_experiencesTypeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_startDate] [datetime] NULL,
[stateCode] [int] NULL,
[stateCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusCode] [int] NULL,
[statusCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timezoneRuleVersionNumber] [int] NULL,
[utcConversionTimezoneCode] [int] NULL,
[versionNumber] [bigint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[phg_experiences]'
GO
CREATE TABLE [dbo].[phg_experiences]
(
[createdBy] [uniqueidentifier] NULL,
[createdByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOn] [datetime] NULL,
[createdOnBehalfBy] [uniqueidentifier] NULL,
[createdOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importSequenceNumber] [int] NULL,
[modifiedBy] [uniqueidentifier] NULL,
[modifiedByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOn] [datetime] NULL,
[modifiedOnBehalfBy] [uniqueidentifier] NULL,
[modifiedOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[organizationId] [uniqueidentifier] NULL,
[organizationIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[overriddenCreatedOn] [datetime] NULL,
[phg_account] [uniqueidentifier] NULL,
[phg_accountName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_accountNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_accountYomiName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_endDate] [datetime] NULL,
[phg_experiencesId] [uniqueidentifier] NOT NULL,
[phg_experiencesType] [int] NULL,
[phg_experiencesTypeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_name] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_startDate] [datetime] NULL,
[stateCode] [int] NULL,
[stateCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusCode] [int] NULL,
[statusCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timezoneRuleVersionNumber] [int] NULL,
[utcConversionTimezoneCode] [int] NULL,
[versionNumber] [bigint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_phg_experiences] on [dbo].[phg_experiences]'
GO
ALTER TABLE [dbo].[phg_experiences] ADD CONSTRAINT [PK_phg_experiences] PRIMARY KEY CLUSTERED  ([phg_experiencesId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[crm_synchronize_phg_experiences]'
GO
CREATE PROCEDURE [dbo].[crm_synchronize_phg_experiences]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE INTO dbo.phg_experiences AS tgt
	USING (SELECT * FROM staging.phg_experiences) AS src ON src.phg_experiencesid = tgt.phg_experiencesid
	WHEN MATCHED THEN
		UPDATE
			SET createdBy = src.createdBy,createdByName = src.createdByName,createdByYomiName = src.createdByYomiName,createdOn = src.createdOn,createdOnBehalfBy = src.createdOnBehalfBy,createdOnBehalfByName = src.createdOnBehalfByName,createdOnBehalfByYomiName = src.createdOnBehalfByYomiName,importSequenceNumber = src.importSequenceNumber,modifiedBy = src.modifiedBy,modifiedByName = src.modifiedByName,modifiedByYomiName = src.modifiedByYomiName,modifiedOn = src.modifiedOn,modifiedOnBehalfBy = src.modifiedOnBehalfBy,modifiedOnBehalfByName = src.modifiedOnBehalfByName,modifiedOnBehalfByYomiName = src.modifiedOnBehalfByYomiName,organizationId = src.organizationId,organizationIdName = src.organizationIdName,overriddenCreatedOn = src.overriddenCreatedOn,phg_account = src.phg_account,phg_accountName = src.phg_accountName,phg_accountNumber = src.phg_accountNumber,phg_accountYomiName = src.phg_accountYomiName,phg_endDate = src.phg_endDate,phg_experiencesId = src.phg_experiencesId,phg_experiencesType = src.phg_experiencesType,phg_experiencesTypeName = src.phg_experiencesTypeName,phg_name = src.phg_name,phg_startDate = src.phg_startDate,stateCode = src.stateCode,stateCodeName = src.stateCodeName,statusCode = src.statusCode,statusCodeName = src.statusCodeName,timezoneRuleVersionNumber = src.timezoneRuleVersionNumber,utcConversionTimezoneCode = src.utcConversionTimezoneCode,versionNumber = src.versionNumber
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([createdBy],[createdByName],[createdByYomiName],[createdOn],[createdOnBehalfBy],[createdOnBehalfByName],[createdOnBehalfByYomiName],[importSequenceNumber],[modifiedBy],[modifiedByName],[modifiedByYomiName],[modifiedOn],[modifiedOnBehalfBy],[modifiedOnBehalfByName],[modifiedOnBehalfByYomiName],[organizationId],[organizationIdName],[overriddenCreatedOn],[phg_account],[phg_accountName],[phg_accountNumber],[phg_accountYomiName],[phg_endDate],[phg_experiencesId],[phg_experiencesType],[phg_experiencesTypeName],[phg_name],[phg_startDate],[stateCode],[stateCodeName],[statusCode],[statusCodeName],[timezoneRuleVersionNumber],[utcConversionTimezoneCode],[versionNumber])
		VALUES(src.[createdBy],src.[createdByName],src.[createdByYomiName],src.[createdOn],src.[createdOnBehalfBy],src.[createdOnBehalfByName],src.[createdOnBehalfByYomiName],src.[importSequenceNumber],src.[modifiedBy],src.[modifiedByName],src.[modifiedByYomiName],src.[modifiedOn],src.[modifiedOnBehalfBy],src.[modifiedOnBehalfByName],src.[modifiedOnBehalfByYomiName],src.[organizationId],src.[organizationIdName],src.[overriddenCreatedOn],src.[phg_account],src.[phg_accountName],src.[phg_accountNumber],src.[phg_accountYomiName],src.[phg_endDate],src.[phg_experiencesId],src.[phg_experiencesType],src.[phg_experiencesTypeName],src.[phg_name],src.[phg_startDate],src.[stateCode],src.[stateCodeName],src.[statusCode],src.[statusCodeName],src.[timezoneRuleVersionNumber],src.[utcConversionTimezoneCode],src.[versionNumber]);
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [staging].[IPreferBookingReward]'
GO
CREATE TABLE [staging].[IPreferBookingReward]
(
[createdBy] [uniqueidentifier] NULL,
[createdByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOn] [datetime] NULL,
[createdOnBehalfBy] [uniqueidentifier] NULL,
[createdOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importSequenceNumber] [int] NULL,
[modifiedBy] [uniqueidentifier] NULL,
[modifiedByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOn] [datetime] NULL,
[modifiedOnBehalfBy] [uniqueidentifier] NULL,
[modifiedOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[organizationId] [uniqueidentifier] NULL,
[organizationIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[overriddenCreatedOn] [datetime] NULL,
[phg_endDate] [datetime] NULL,
[phg_hotelId] [uniqueidentifier] NULL,
[phg_hotelIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_hotelIdYomiName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_ipreferBookingRewardId] [uniqueidentifier] NULL,
[phg_name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_startDate] [datetime] NULL,
[stateCode] [int] NULL,
[stateCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusCode] [int] NULL,
[statusCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timezoneRuleVersionNumber] [int] NULL,
[utcConversionTimezoneCode] [int] NULL,
[versionNumber] [bigint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPreferBookingReward]'
GO
CREATE TABLE [dbo].[IPreferBookingReward]
(
[createdBy] [uniqueidentifier] NULL,
[createdByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOn] [datetime] NULL,
[createdOnBehalfBy] [uniqueidentifier] NULL,
[createdOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[createdOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[importSequenceNumber] [int] NULL,
[modifiedBy] [uniqueidentifier] NULL,
[modifiedByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOn] [datetime] NULL,
[modifiedOnBehalfBy] [uniqueidentifier] NULL,
[modifiedOnBehalfByName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[modifiedOnBehalfByYomiName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[organizationId] [uniqueidentifier] NULL,
[organizationIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[overriddenCreatedOn] [datetime] NULL,
[phg_endDate] [datetime] NULL,
[phg_hotelId] [uniqueidentifier] NULL,
[phg_hotelIdName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_hotelIdYomiName] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_ipreferBookingRewardId] [uniqueidentifier] NULL,
[phg_name] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[phg_startDate] [datetime] NULL,
[stateCode] [int] NULL,
[stateCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[statusCode] [int] NULL,
[statusCodeName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[timezoneRuleVersionNumber] [int] NULL,
[utcConversionTimezoneCode] [int] NULL,
[versionNumber] [bigint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_IPreferBookingReward_1] on [dbo].[IPreferBookingReward]'
GO
ALTER TABLE [dbo].[IPreferBookingReward] ADD CONSTRAINT [PK_IPreferBookingReward_1] PRIMARY KEY CLUSTERED  ([versionNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[crm_synchronize_IPreferBookingReward]'
GO
CREATE PROCEDURE [dbo].[crm_synchronize_IPreferBookingReward]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	MERGE INTO dbo.IPreferBookingReward AS tgt
	USING (SELECT * FROM staging.IPreferBookingReward) AS src ON src.[versionNumber] = tgt.[versionNumber]
	WHEN MATCHED THEN
		UPDATE
			SET createdBy = src.createdBy,createdByName = src.createdByName,createdByYomiName = src.createdByYomiName,createdOn = src.createdOn,createdOnBehalfBy = src.createdOnBehalfBy,createdOnBehalfByName = src.createdOnBehalfByName,createdOnBehalfByYomiName = src.createdOnBehalfByYomiName,importSequenceNumber = src.importSequenceNumber,modifiedBy = src.modifiedBy,modifiedByName = src.modifiedByName,modifiedByYomiName = src.modifiedByYomiName,modifiedOn = src.modifiedOn,modifiedOnBehalfBy = src.modifiedOnBehalfBy,modifiedOnBehalfByName = src.modifiedOnBehalfByName,modifiedOnBehalfByYomiName = src.modifiedOnBehalfByYomiName,organizationId = src.organizationId,organizationIdName = src.organizationIdName,overriddenCreatedOn = src.overriddenCreatedOn,phg_endDate = src.phg_endDate,phg_hotelId = src.phg_hotelId,phg_hotelIdName = src.phg_hotelIdName,phg_hotelIdYomiName = src.phg_hotelIdYomiName,phg_ipreferBookingRewardId = src.phg_ipreferBookingRewardId,phg_name = src.phg_name,phg_startDate = src.phg_startDate,stateCode = src.stateCode,stateCodeName = src.stateCodeName,statusCode = src.statusCode,statusCodeName = src.statusCodeName,timezoneRuleVersionNumber = src.timezoneRuleVersionNumber,utcConversionTimezoneCode = src.utcConversionTimezoneCode,versionNumber = src.versionNumber
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([createdBy],[createdByName],[createdByYomiName],[createdOn],[createdOnBehalfBy],[createdOnBehalfByName],[createdOnBehalfByYomiName],[importSequenceNumber],[modifiedBy],[modifiedByName],[modifiedByYomiName],[modifiedOn],[modifiedOnBehalfBy],[modifiedOnBehalfByName],[modifiedOnBehalfByYomiName],[organizationId],[organizationIdName],[overriddenCreatedOn],[phg_endDate],[phg_hotelId],[phg_hotelIdName],[phg_hotelIdYomiName],[phg_ipreferBookingRewardId],[phg_name],[phg_startDate],[stateCode],[stateCodeName],[statusCode],[statusCodeName],[timezoneRuleVersionNumber],[utcConversionTimezoneCode],[versionNumber])
		VALUES(src.[createdBy],src.[createdByName],src.[createdByYomiName],src.[createdOn],src.[createdOnBehalfBy],src.[createdOnBehalfByName],src.[createdOnBehalfByYomiName],src.[importSequenceNumber],src.[modifiedBy],src.[modifiedByName],src.[modifiedByYomiName],src.[modifiedOn],src.[modifiedOnBehalfBy],src.[modifiedOnBehalfByName],src.[modifiedOnBehalfByYomiName],src.[organizationId],src.[organizationIdName],src.[overriddenCreatedOn],src.[phg_endDate],src.[phg_hotelId],src.[phg_hotelIdName],src.[phg_hotelIdYomiName],src.[phg_ipreferBookingRewardId],src.[phg_name],src.[phg_startDate],src.[stateCode],src.[stateCodeName],src.[statusCode],src.[statusCodeName],src.[timezoneRuleVersionNumber],src.[utcConversionTimezoneCode],src.[versionNumber]);
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[crm_synchronize]'
GO
ALTER PROCEDURE [dbo].[crm_synchronize]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	EXEC crm_synchronize_Account
	EXEC crm_synchronize_Connection
	EXEC crm_synchronize_ConnectionRole
	EXEC crm_synchronize_Contact
	EXEC crm_synchronize_customeraddress
	EXEC crm_synchronize_HotelAttributes
	EXEC crm_synchronize_HotelAttributeType
	EXEC crm_synchronize_iTools
	EXEC crm_synchronize_Leads
	EXEC crm_synchronize_Opportunity
	EXEC crm_synchronize_opportunityproduct
	EXEC crm_synchronize_phg_city
	EXEC crm_synchronize_phg_collection
	EXEC crm_synchronize_phg_contractedprograms
	EXEC crm_synchronize_phg_country
	EXEC crm_synchronize_phg_event
	EXEC crm_synchronize_phg_hotelcollection
	EXEC crm_synchronize_phg_hoteleventregistration
	EXEC crm_synchronize_phg_hotelreportsubscription
	EXEC crm_synchronize_phg_requestforproposal
	EXEC crm_synchronize_phg_state
	EXEC crm_synchronize_product
	EXEC crm_synchronize_Role
	EXEC crm_synchronize_SystemUser
	EXEC crm_synchronize_systemuserroles
	EXEC crm_synchronize_transactioncurrency

	EXEC crm_synchronize_phg_experiences
	EXEC crm_synchronize_IPreferBookingReward
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
