/*
Run this script on:

        chi-sq-st-01\warehouse.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 1/9/2018 11:24:30 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[Import_Currency_Data]'
GO
ALTER TABLE [dbo].[Import_Currency_Data] DROP CONSTRAINT [FK_Import_Currency_Data_Import_CurrencyID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[Import_Currency_Header]'
GO
ALTER TABLE [dbo].[Import_Currency_Header] DROP CONSTRAINT [FK_Import_Currency_Header_Import_CurrencyID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Import_Currency]'
GO
ALTER TABLE [dbo].[Import_Currency] DROP CONSTRAINT [PK_Import_Currency]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Import_Currency]'
GO
ALTER TABLE [dbo].[Import_Currency] DROP CONSTRAINT [DF__Import_Cu__Impor__2AD55B43]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Queue]'
GO
ALTER TABLE [dbo].[Queue] DROP CONSTRAINT [DF_Queue_CreateDate]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Queue]'
GO
ALTER TABLE [dbo].[Queue] DROP CONSTRAINT [DF__Queue__RunInOldP__607251E5]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[Sabre_subQueue]'
GO
ALTER TABLE [dbo].[Sabre_subQueue] DROP CONSTRAINT [DF__Sabre_sub__Creat__6383C8BA]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[LOG_IPExport]'
GO
DROP TABLE [dbo].[LOG_IPExport]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping [dbo].[LOG_Sabre]'
GO
DROP TABLE [dbo].[LOG_Sabre]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SabreExport_LOG]'
GO
CREATE TABLE [dbo].[SabreExport_LOG]
(
[SabreExport_LOG_ID] [int] NOT NULL IDENTITY(1, 1),
[LogDate] [datetime] NOT NULL,
[LogStatus] [tinyint] NOT NULL CONSTRAINT [DF_SabreExport_LOG_LogStatus] DEFAULT ((0)),
[LogStatus_Desc] AS (case [LogStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' when (3) then 'Error' when (4) then 'Resolved' else 'Error' end),
[LogMessage] [xml] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SabreExport_LOG] on [dbo].[SabreExport_LOG]'
GO
ALTER TABLE [dbo].[SabreExport_LOG] ADD CONSTRAINT [PK_SabreExport_LOG] PRIMARY KEY CLUSTERED  ([SabreExport_LOG_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SabreExport_FTP_OUTPUT]'
GO
CREATE TABLE [dbo].[SabreExport_FTP_OUTPUT]
(
[output] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[Queue]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_Queue]
(
[QueueID] [int] NOT NULL IDENTITY(1, 1),
[Application] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FilePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_Queue_CreateDate] DEFAULT (getdate()),
[QueueStatus] [tinyint] NOT NULL,
[QueueStatus_Desc] AS (case [QueueStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' when (3) then 'Error' when (4) then 'Resolved' else 'Error' end) PERSISTED NOT NULL,
[ImportStarted] [datetime] NULL,
[ImportFinished] [datetime] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportCount] [int] NULL,
[RunInOldProd] [bit] NULL CONSTRAINT [DF_RunInOldProd] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Queue] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_Queue]([QueueID], [Application], [FilePath], [CreateDate], [QueueStatus], [ImportStarted], [ImportFinished], [Message], [ImportCount], [RunInOldProd]) SELECT [QueueID], [Application], [FilePath], [CreateDate], [QueueStatus], [ImportStarted], [ImportFinished], [Message], [ImportCount], [RunInOldProd] FROM [dbo].[Queue]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Queue] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Queue]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[tmp_rg_xx_Queue]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[Queue]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_Queue]', N'Queue'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Sabre_ExportToFTP]'
GO

ALTER PROCEDURE [dbo].[Sabre_ExportToFTP]
	@QueueID int,
	@AppName varchar(255)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE  @SvrName nvarchar(255) = @@SERVERNAME,
			@cmd varchar(2000),
			@Dest varchar(255),
			@App varchar(255),
			@Scripts varchar(255),
			@FilePath varchar(255)

	SELECT @Dest = dbo.fn_GetFilePath(@SvrName,@AppName,'FTP DEST'),
			@App = dbo.fn_GetFilePath(@SvrName,@AppName,'FTP APP'),
			@Scripts = dbo.fn_GetFilePath(@SvrName,@AppName,'SCRIPTS')

	SELECT @FilePath = FilePath FROM [Queue] WHERE QueueID = @QueueID

	-- CREATE DOS BATCH FILE ------------------------------------
	DELETE dbo.FTP_COMMAND WHERE [Application] = @AppName
	
	INSERT INTO dbo.FTP_COMMAND([Application],Command)
	SELECT @AppName,@App + ' ' + @FilePath + ' ' + @Dest
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO SupersetCopyFAIL'
		UNION ALL
	SELECT @AppName,' '
		UNION ALL
	SELECT @AppName,':Success'
		UNION ALL
	SELECT @AppName,'REM SUCCESS'
		UNION ALL
	SELECT @AppName,' '
		UNION ALL
	SELECT @AppName,'Exit'
		UNION ALL
	SELECT @AppName,' '
		UNION ALL
	SELECT @AppName,':SupersetCopyFAIL'
		UNION ALL
	SELECT @AppName,'REM FAILED'

	SET @cmd = 'BCP "SELECT Command FROM ETL.dbo.FTP_COMMAND WHERE [Application] = ' + QUOTENAME(@AppName,'''') + '" queryout "' + @Scripts + '\SabreFTP_Export.bat" -S' + @SvrName + ' -t"~" -c -T'
	EXEC xp_cmdshell @cmd
	-------------------------------------------------------------

	-- FTP OUTPUT LOG -------------------------------------------
	DELETE dbo.SabreExport_FTP_OUTPUT

	SET @cmd = @Scripts + '\SabreFTP_Export.bat'

	INSERT INTO dbo.SabreExport_FTP_OUTPUT(output)
	EXEC xp_cmdshell @cmd
	-------------------------------------------------------------

	-- LOG RESULTS ----------------------------------------------
	INSERT INTO SabreExport_LOG(LogDate,LogStatus,LogMessage)
	SELECT GETDATE(),
		CASE WHEN [output] LIKE '%REM SUCCESS%' THEN 2 ELSE 3 END,
		(SELECT * FROM dbo.[SabreExport_FTP_OUTPUT] FOR XML RAW)
	FROM dbo.[SabreExport_FTP_OUTPUT]
	WHERE [output] LIKE '%REM%'
	-------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[Import_Currency]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_Import_Currency]
(
[Import_CurrencyID] [int] NOT NULL IDENTITY(1, 1),
[CreateDate] [datetime] NOT NULL,
[ImportFinished] [datetime] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportCount] [int] NULL,
[ImportStatus] [tinyint] NOT NULL CONSTRAINT [DF__Import_Cu__Impor__0FEC5ADD] DEFAULT ((0)),
[ImportStatus_Desc] AS (case [ImportStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' when (3) then 'Error' when (4) then 'Resolved' else 'Error' end) PERSISTED NOT NULL,
[URL] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Import_Currency] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_Import_Currency]([Import_CurrencyID], [CreateDate], [ImportFinished], [Message], [ImportCount], [ImportStatus]) SELECT [Import_CurrencyID], [CreateDate], [ImportFinished], [Message], [ImportCount], [ImportStatus] FROM [dbo].[Import_Currency]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Import_Currency] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Import_Currency]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[tmp_rg_xx_Import_Currency]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[Import_Currency]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_Import_Currency]', N'Import_Currency'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Import_Currency] on [dbo].[Import_Currency]'
GO
ALTER TABLE [dbo].[Import_Currency] ADD CONSTRAINT [PK_Import_Currency] PRIMARY KEY CLUSTERED  ([Import_CurrencyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Currency_StartImport]'
GO

ALTER PROCEDURE [dbo].[Currency_StartImport]
	@Import_CurrencyID int OUT,
	@ServerURL varchar(255) OUT
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	SET @ServerURL = 'http://www.xe.com/dfs/datafeed2.cgi?preferredhotelgroup&date=' + CONVERT(varchar(20),YEAR(GETDATE())) + RIGHT('00' + CONVERT(varchar(20),MONTH(GETDATE())),2) + RIGHT('00' + CONVERT(varchar(20),DAY(GETDATE())),2)

	INSERT INTO Import_Currency(CreateDate,ImportStatus,URL)
	VALUES(GETDATE(),1,@ServerURL)

	SET @Import_CurrencyID = SCOPE_IDENTITY()
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[fn_GetIPreferFileName]'
GO
CREATE FUNCTION [dbo].[fn_GetIPreferFileName] 
(
	@date date
)
RETURNS varchar(255)
AS
BEGIN
	DECLARE @Result varchar(255)

	SELECT @Result = RIGHT('00' + CONVERT(varchar(2),DAY(@date)),2) 
					+ UPPER(LEFT(CONVERT(varchar(20),@date,107),3))
					+ CONVERT(varchar(4),YEAR(@date))
					+ '_' + UPPER(LEFT(CONVERT(varchar(20),DATEADD(month,-1,@date),107),3))
					+ '-' + CONVERT(varchar(4),YEAR(DATEADD(month,-1,@date)))

	RETURN @Result
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[IPHotel_BillingReset]'
GO

ALTER PROCEDURE [dbo].[IPHotel_BillingReset]
	@invoiceDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE	@HotelPoint_FileName varchar(255),
			@HotelRedemption_FileName varchar(255),
			@date date = @invoiceDate

	SELECT @HotelPoint_FileName = '%Hotel[_]Point[_]Billing[_]Report[_]' + FileDate + '.csv',
			@HotelRedemption_FileName = '%Hotel[_]Redemption[_]Billing[_]Report[_]' + FileDate + '.csv'
	FROM (SELECT dbo.fn_GetIPreferFileName(@invoiceDate) AS FileDate) x


	UPDATE dbo.Queue
		SET FilePath = '[BILLING RESET](' + CONVERT(varchar(20),@date) + ')' + FilePath
	WHERE QueueStatus != 3
		AND [Application] = 'IPHotelPointBilling'
		AND FilePath LIKE @HotelPoint_FileName

	UPDATE dbo.Queue
		SET FilePath = '[BILLING RESET](' + CONVERT(varchar(20),@date) + ')' + FilePath
	WHERE QueueStatus != 3
		AND [Application] = 'IPHotelRedemption'
		AND FilePath LIKE @HotelRedemption_FileName
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Sabre_PopulateMostRecentTransactions]'
GO

ALTER PROCEDURE [dbo].[Sabre_PopulateMostRecentTransactions]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @timeLoaded datetime
	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	-- GET TIME LOADED ---------------------------------
	SELECT @timeLoaded = ISNULL([ImportFinished],[ImportStarted]) FROM [Queue] WHERE QueueID = @QueueID
	----------------------------------------------------

	-- CREATE & POPULATE #NEW_RAW_DATA -----------------
	IF OBJECT_ID('tempdb..#NEW_RAW_DATA') IS NOT NULL
		DROP TABLE #NEW_RAW_DATA;
	CREATE TABLE #NEW_RAW_DATA
	(
		cnt int,
		ConfirmationNumber nvarchar(20),
		maxTimeStamp datetime,
		PRIMARY KEY CLUSTERED(ConfirmationNumber,maxTimeStamp)
	)

	INSERT INTO #NEW_RAW_DATA(cnt,ConfirmationNumber,maxTimeStamp)
	SELECT COUNT(confirmationnumber),confirmationnumber,MAX(transactiontimestamp)
	FROM Superset.dbo.rawData
	WHERE timeloaded = @timeLoaded
	GROUP BY confirmationnumber
	----------------------------------------------------
	
	-- CREATE #INSERTS ---------------------------------
	IF OBJECT_ID('tempdb..#INSERTS') IS NOT NULL
		DROP TABLE #INSERTS;
	CREATE TABLE #INSERTS
	(
		ConfirmationNumber nvarchar(20),
		hashKey binary(16),
		maxTimeStamp datetime,
		maxActionTypeOrder int,
		PRIMARY KEY CLUSTERED(ConfirmationNumber,maxTimeStamp)
	)
	----------------------------------------------------

	-- INSERT INTO #INSERTS (NEW RECORDS) --------------
	INSERT INTO #INSERTS(ConfirmationNumber,hashKey,maxTimeStamp,maxActionTypeOrder)
	SELECT new.ConfirmationNumber,rawd.hashkey,new.maxTimeStamp,rato.actiontypeorder
	FROM #NEW_RAW_DATA new
		INNER JOIN Superset.dbo.RawData rawd ON new.confirmationnumber = rawd.confirmationnumber AND new.maxtimestamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
		LEFT OUTER JOIN Superset.dbo.MostRecentTransactions mrt ON new.confirmationnumber = mrt.confirmationnumber
	WHERE mrt.confirmationnumber IS NULL
		AND new.cnt = 1
	----------------------------------------------------

	-- INSERT INTO #INSERTS (MAX ActionTypeOrder) ------
	;WITH cte_New(confirmationnumber,maxtimestamp,maxActionTypeOrder)
	AS
	(
		SELECT new.confirmationnumber,new.maxtimestamp,MAX(rato.actiontypeorder)
		FROM #NEW_RAW_DATA new
			INNER JOIN Superset.dbo.RawData rawd ON	new.ConfirmationNumber = rawd.confirmationnumber AND new.maxTimeStamp = rawd.transactiontimestamp
			INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
			LEFT OUTER JOIN	Superset.dbo.MostRecentTransactions mrt ON new.ConfirmationNumber = mrt.confirmationnumber
		WHERE mrt.confirmationnumber IS NULL
			AND	new.cnt > 1
		GROUP BY new.ConfirmationNumber,new.maxTimeStamp
	)
	INSERT INTO #INSERTS(ConfirmationNumber,hashKey,maxTimeStamp,maxActionTypeOrder)
	SELECT new.confirmationnumber,MAX(rawd.hashkey),new.maxtimestamp,new.maxActionTypeOrder
	FROM cte_New new
		INNER JOIN Superset.dbo.RawData rawd ON	new.ConfirmationNumber = rawd.confirmationnumber AND new.maxTimeStamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype AND rato.actionTypeOrder = new.maxActionTypeOrder
	GROUP BY new.ConfirmationNumber,new.maxTimeStamp,new.maxActionTypeOrder
	----------------------------------------------------

	-- CREATE #UPDATES ---------------------------------
	IF OBJECT_ID('tempdb..#UPDATES') IS NOT NULL
		DROP TABLE #UPDATES;
	CREATE TABLE #UPDATES
	(
		ConfirmationNumber nvarchar(20),
		maxTimeStamp datetime,
		maxActionTypeOrder int,
	)
	----------------------------------------------------

	-- INSERT INTO #UPDATES (NEW RECORDS) --------------
	INSERT INTO #UPDATES(ConfirmationNumber,maxTimeStamp,maxActionTypeOrder)
	SELECT new.ConfirmationNumber,new.maxTimeStamp,rato.actiontypeorder
	FROM #NEW_RAW_DATA new
		INNER JOIN Superset.dbo.RawData rawd ON new.ConfirmationNumber = rawd.confirmationnumber AND new.maxTimeStamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
		INNER JOIN Superset.dbo.MostRecentTransactions mrt ON new.ConfirmationNumber = mrt.confirmationnumber
	WHERE new.maxTimeStamp > mrt.transactiontimestamp
		AND new.cnt = 1
	----------------------------------------------------

	-- INSERT INTO #UPDATES (MAX ActionTypeOrder) --------------
	INSERT INTO #UPDATES(ConfirmationNumber,maxTimeStamp,maxActionTypeOrder)
	SELECT new.confirmationnumber,new.maxtimestamp,MAX(rato.actiontypeorder)
	FROM #NEW_RAW_DATA new
		INNER JOIN Superset.dbo.RawData rawd ON new.confirmationnumber = rawd.confirmationnumber AND new.maxtimestamp = rawd.transactiontimestamp
	INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype
	INNER JOIN Superset.dbo.MostRecentTransactions mrt ON new.confirmationnumber = mrt.confirmationnumber
	WHERE new.maxtimestamp > mrt.transactiontimestamp
		AND	new.cnt > 1
	GROUP BY new.confirmationnumber,new.maxtimestamp
	----------------------------------------------------

	-- INSERT INTO MostRecentTransactions --------------
	INSERT INTO Superset.dbo.mostrecenttransactions(hashKey,chainName,chainID,hotelName,hotelID,SAPID,hotelCode,billingDescription,transactionTimeStamp,
													faxNotificationCount,channel,secondarySource,subSource,subSourceCode,PMSRateTypeCode,PMSRoomTypeCode,
													marketSourceCode,marketSegmentCode,userName,[status],confirmationNumber,confirmationDate,
													cancellationNumber,cancellationDate,salutation,guestFirstName,guestLastName,customerID,customerAddress1,
													customerAddress2,customerCity,customerState,customerPostalCode,customerPhone,customerCountry,customerArea,
													customerRegion,customerCompanyName,arrivalDate,departureDate,bookingLeadTime,rateCategoryName,
													rateCategoryCode,rateTypeName,rateTypeCode,roomTypeName,roomTypeCode,nights,averageDailyRate,rooms,
													reservationRevenue,currency,IATANumber,travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,
													travelAgencyCity,travelAgencyState,travelAgencyPostalCode,travelAgencyPhone,travelAgencyFax,
													travelAgencyCountry,travelAgencyArea,travelAgencyRegion,travelAgencyEmail,consortiaCount,consortiaName,
													totalPackageRevenue,optIn,customerEmail,totalGuestCount,adultCount,childrenCount,creditCardType,
													actionType,shareWith,arrivalDOW,departureDOW,itineraryNumber,secondaryCurrency,
													secondaryCurrencyExchangeRate,secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,
													secondaryCurrencyPackageRevenue,commisionPercent,membershipNumber,corporationCode,promotionalCode,
													CROCode,channelConnectConfirmationNumber,primaryGuest,loyaltyProgram,loyaltyNumber,vipLevel,
													xbeTemplateName,xbeShellName,profileTypeSelection,averageDailyRateUSD,reservationRevenueUSD,
													totalPackageRevenueUSD,timeLoaded,OpenHospitalityID,CRSSourceID,LoyaltyNumberValidated)
	SELECT rawd.hashKey,chainName,chainID,hotelName,hotelID,SAPID,hotelCode,billingDescription,transactionTimeStamp,
			faxNotificationCount,channel,secondarySource,subSource,subSourceCode,PMSRateTypeCode,PMSRoomTypeCode,
			marketSourceCode,marketSegmentCode,userName,[status],rawd.confirmationNumber,confirmationDate,
			cancellationNumber,cancellationDate,salutation,guestFirstName,guestLastName,customerID,customerAddress1,
			customerAddress2,customerCity,customerState,customerPostalCode,customerPhone,customerCountry,customerArea,
			customerRegion,customerCompanyName,arrivalDate,departureDate,bookingLeadTime,rateCategoryName,
			rateCategoryCode,rateTypeName,rateTypeCode,roomTypeName,roomTypeCode,nights,averageDailyRate,rooms,
			reservationRevenue,currency,IATANumber,travelAgencyName,travelAgencyAddress1,travelAgencyAddress2,
			travelAgencyCity,travelAgencyState,travelAgencyPostalCode,travelAgencyPhone,travelAgencyFax,
			travelAgencyCountry,travelAgencyArea,travelAgencyRegion,travelAgencyEmail,consortiaCount,consortiaName,
			totalPackageRevenue,
			CASE [optIn] WHEN 'Y' THEN 1 ELSE 0 END,
			customerEmail,totalGuestCount,adultCount,childrenCount,creditCardType,
			actionType,shareWith,arrivalDOW,departureDOW,itineraryNumber,secondaryCurrency,
			secondaryCurrencyExchangeRate,secondaryCurrencyAverageDailyRate,secondaryCurrencyReservationRevenue,
			secondaryCurrencyPackageRevenue,commisionPercent,membershipNumber,corporationCode,promotionalCode,
			CROCode,channelConnectConfirmationNumber,
			CASE [primaryGuest] WHEN 'Y' THEN 1 ELSE 0 END,
			loyaltyProgram,loyaltyNumber,vipLevel,
			xbeTemplateName,xbeShellName,profileTypeSelection,averageDailyRateUSD,reservationRevenueUSD,
			totalPackageRevenueUSD,timeLoaded,NULL,1,
			CASE WHEN cpi.iPrefer_Number IS NOT NULL THEN 1 ELSE 0 END
	FROM #INSERTS i
		INNER JOIN Superset.dbo.rawData rawd ON i.ConfirmationNumber = rawd.confirmationnumber	AND i.maxTimeStamp = rawd.transactiontimestamp AND i.hashKey = rawd.hashKey
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actionType = rato.phgactiontype AND rato.actiontypeorder = i.maxActionTypeOrder
		LEFT JOIN Superset.BSI.Customer_Profile_Import cpi ON cpi.iPrefer_Number = rawd.loyaltyNumber
																AND rawd.confirmationDate >= cpi.Membership_Date
																AND rawd.confirmationDate >= @firstDayOfMonth
	----------------------------------------------------

	-- UPDATE MostRecentTransactions -------------------
	UPDATE MRT
		SET	[hashKey] = rawd.[hashKey],
			[chainName] = rawd.[chainName],
			[chainID] = rawd.[chainID],
			[hotelName] = rawd.[hotelName],
			[hotelID] = rawd.[hotelID],
			[SAPID] = rawd.[SAPID],
			[hotelCode] = rawd.[hotelCode],
			[billingDescription] = rawd.[billingDescription],
			[transactionTimeStamp] = rawd.[transactionTimeStamp],
			[faxNotificationCount] = rawd.[faxNotificationCount],
			[channel] = rawd.[channel],
			[secondarySource] = rawd.[secondarySource],
			[subSource] = rawd.[subSource],
			[subSourceCode] = rawd.[subSourceCode],
			[PMSRateTypeCode] = rawd.[PMSRateTypeCode],
			[PMSRoomTypeCode] = rawd.[PMSRoomTypeCode],
			[marketSourceCode] = rawd.[marketSourceCode],
			[marketSegmentCode] = rawd.[marketSegmentCode],
			[userName] = rawd.[userName],
			[status] = rawd.[status],
			[confirmationDate] = rawd.[confirmationDate],
			[cancellationNumber] = rawd.[cancellationNumber],
			[cancellationDate] = rawd.[cancellationDate],
			[salutation] = rawd.[salutation],
			[guestFirstName] = rawd.[guestFirstName],
			[guestLastName] = rawd.[guestLastName],
			[customerID] = rawd.[customerID],
			[customerAddress1] = rawd.[customerAddress1],
			[customerAddress2] = rawd.[customerAddress2],
			[customerCity] = rawd.[customerCity],
			[customerState] = rawd.[customerState],
			[customerPostalCode] = rawd.[customerPostalCode],
			[customerPhone] = rawd.[customerPhone],
			[customerCountry] = rawd.[customerCountry],
			[customerArea] = rawd.[customerArea],
			[customerRegion] = rawd.[customerRegion],
			[customerCompanyName] = rawd.[customerCompanyName],
			[arrivalDate] = rawd.[arrivalDate],
			[departureDate] = rawd.[departureDate],
			[bookingLeadTime] = rawd.[bookingLeadTime],
			[rateCategoryName] = rawd.[rateCategoryName],
			[rateCategoryCode] = rawd.[rateCategoryCode],
			[rateTypeName] = rawd.[rateTypeName],
			[rateTypeCode] = rawd.[rateTypeCode],
			[roomTypeName] = rawd.[roomTypeName],
			[roomTypeCode] = rawd.[roomTypeCode],
			[nights] = rawd.[nights],
			[averageDailyRate] = rawd.[averageDailyRate],
			[rooms] = rawd.[rooms],
			[reservationRevenue] = rawd.[reservationRevenue],
			[currency] = rawd.[currency],
			[IATANumber] = rawd.[IATANumber],
			[travelAgencyName] = rawd.[travelAgencyName],
			[travelAgencyAddress1] = rawd.[travelAgencyAddress1],
			[travelAgencyAddress2] = rawd.[travelAgencyAddress2],
			[travelAgencyCity] = rawd.[travelAgencyCity],
			[travelAgencyState] = rawd.[travelAgencyState],
			[travelAgencyPostalCode] = rawd.[travelAgencyPostalCode],
			[travelAgencyPhone] = rawd.[travelAgencyPhone],
			[travelAgencyFax] = rawd.[travelAgencyFax],
			[travelAgencyCountry] = rawd.[travelAgencyCountry],
			[travelAgencyArea] = rawd.[travelAgencyArea],
			[travelAgencyRegion] = rawd.[travelAgencyRegion],
			[travelAgencyEmail] = rawd.[travelAgencyEmail],
			[consortiaCount] = rawd.[consortiaCount],
			[consortiaName] = rawd.[consortiaName],
			[totalPackageRevenue] = rawd.[totalPackageRevenue],
			[optIn] = CASE rawd.[optIn] WHEN 'Y' THEN 1 ELSE 0 END,
			[customerEmail] = rawd.[customerEmail],
			[totalGuestCount] = rawd.[totalGuestCount],
			[adultCount] = rawd.[adultCount],
			[childrenCount] = rawd.[childrenCount],
			[creditCardType] = rawd.[creditCardType],
			[actionType] = rawd.[actionType],
			[shareWith] = rawd.[shareWith],
			[arrivalDOW] = rawd.[arrivalDOW],
			[departureDOW] = rawd.[departureDOW],
			[itineraryNumber] = rawd.[itineraryNumber],
			[secondaryCurrency] = rawd.[secondaryCurrency],
			[secondaryCurrencyExchangeRate] = rawd.[secondaryCurrencyExchangeRate],
			[secondaryCurrencyAverageDailyRate] = rawd.[secondaryCurrencyAverageDailyRate],
			[secondaryCurrencyReservationRevenue] = rawd.[secondaryCurrencyReservationRevenue],
			[secondaryCurrencyPackageRevenue] = rawd.[secondaryCurrencyPackageRevenue],
			[commisionPercent] = rawd.[commisionPercent],
			[membershipNumber] = rawd.[membershipNumber],
			[corporationCode] = rawd.[corporationCode],
			[promotionalCode] = rawd.[promotionalCode],
			[CROCode] = rawd.[CROCode],
			[channelConnectConfirmationNumber] = rawd.[channelConnectConfirmationNumber],
			[primaryGuest] = CASE rawd.[primaryGuest] WHEN 'Y' THEN 1 ELSE 0 END,
			[loyaltyProgram] = rawd.[loyaltyProgram],
			[loyaltyNumber] = rawd.[loyaltyNumber],
			[vipLevel] = rawd.[vipLevel],
			[xbeTemplateName] = rawd.[xbeTemplateName],
			[xbeShellName] = rawd.[xbeShellName],
			[profileTypeSelection] = rawd.[profileTypeSelection],
			[averageDailyRateUSD] = rawd.[averageDailyRateUSD],
			[reservationRevenueUSD] = rawd.[reservationRevenueUSD],
			[totalPackageRevenueUSD] = rawd.[totalPackageRevenueUSD],
			[timeLoaded] = rawd.[timeLoaded],
			[LoyaltyNumberValidated] = CASE WHEN cpi.iPrefer_Number IS NOT NULL THEN 1 ELSE 0 END
	FROM #UPDATES u
		INNER JOIN Superset.dbo.MostRecentTransactions mrt ON u.confirmationnumber = mrt.Confirmationnumber
		INNER JOIN Superset.dbo.rawData rawd ON mrt.confirmationnumber = rawd.confirmationnumber AND u.maxtimestamp = rawd.transactiontimestamp
		INNER JOIN Superset.dbo.reportActionTypeOrder rato ON rawd.actiontype = rato.phgactiontype AND rato.actiontypeorder = u.maxactiontypeorder
		LEFT JOIN Superset.BSI.Customer_Profile_Import cpi ON cpi.iPrefer_Number = rawd.loyaltyNumber
																AND rawd.confirmationDate >= cpi.Membership_Date
																AND rawd.confirmationDate >= @firstDayOfMonth
	----------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ftp_CopyFTP_LoadQueue]'
GO




ALTER PROCEDURE [dbo].[ftp_CopyFTP_LoadQueue]
	@AppName varchar(255),
	@ImportType tinyint = 0 -- 0=PHG FTP, 1=BSI FTP, 2=Network File
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	-- PRINT PROGRESS ---------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': STARTING'
	RAISERROR(@print,10,1) WITH NOWAIT
	---------------------------------------------------------------

	-- GET PATHS ------------------------------------------------
	DECLARE @SvrName nvarchar(255) = @@SERVERNAME,
			@cmd varchar(2000),
			@FTP_DIR varchar(255),
			@FILE_DIR varchar(255),
			@SCRIPTS varchar(255),
			@DOWNLOAD varchar(255),
			@PSCP varchar(255),
			@FilePattern varchar(255),
			@FileName varchar(255),
			@NewFiles varchar(255);

	SELECT @FTP_DIR = [dbo].[fn_GetFilePath](@SvrName,@AppName,'FTP DIR'),
			@SCRIPTS = [dbo].[fn_GetFilePath](@SvrName,@AppName,'SCRIPTS'),
			@PSCP = [dbo].[fn_GetFilePath](@SvrName,@AppName,'PSCP'),
			@FilePattern = [dbo].[fn_GetFilePath](@SvrName,@AppName,'FILE Pattern'),
			@FILE_DIR = [dbo].[fn_GetFilePath](@SvrName,@AppName,'FILE DIR')
	-------------------------------------------------------------

	-- FILE PATTERN FOR IPHotelPointBilling/IPHotelRedemption ---
	IF @AppName = 'IPHotelPointBilling'
		SELECT @FilePattern = '%Hotel[_]Point[_]Billing[_]Report[_]' + dbo.fn_GetIPreferFileName(GETDATE()) + '.csv'
	ELSE IF @AppName = 'IPHotelRedemption'
		SELECT @FilePattern = '%Hotel[_]Redemption[_]Billing[_]Report[_]' + dbo.fn_GetIPreferFileName(GETDATE()) + '.csv'
	-------------------------------------------------------------

	PRINT '@FTP_DIR = ' + @FTP_DIR
	PRINT '@SCRIPTS = ' + @SCRIPTS
	PRINT '@PSCP = ' + @PSCP
	PRINT '@FilePattern = ' + @FilePattern
	PRINT '@FILE_DIR = ' + @FILE_DIR
	

	-- PRINT PROGRESS ---------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': GET LIST OF FILES ON FTP SERVER'
	RAISERROR(@print,10,1) WITH NOWAIT
	---------------------------------------------------------------

	-- GET LIST OF FILES ON FTP SERVER --------------------------
	IF OBJECT_ID('tempdb..#OUTPUT') IS NOT NULL
		DROP TABLE #OUTPUT;
	CREATE TABLE #OUTPUT(output nvarchar(MAX))

	IF @ImportType = 0 -- PGH FTP
	BEGIN
		DELETE dbo.FTP_COMMAND WHERE [Application] = @AppName
	
		INSERT INTO dbo.FTP_COMMAND([Application],Command)
		SELECT @AppName,'cd ' + @FTP_DIR
			UNION ALL
		SELECT @AppName,'dir'
			UNION ALL
		SELECT @AppName,'quit'

		SET @cmd = 'BCP "SELECT Command FROM ETL.[dbo].FTP_COMMAND WHERE [Application] = ' + QUOTENAME(@AppName,'''') + '" queryout "' + @SCRIPTS + '\' + @AppName + '_FTP_DIR.txt" -S' + @SvrName + ' -t"~" -c -T'
		EXEC xp_cmdshell @cmd

		SET @cmd = @PSCP + ' -b ' + @SCRIPTS + '\' + @AppName + '_FTP_DIR.txt'
		INSERT INTO #OUTPUT(output)
		EXEC xp_cmdshell @cmd
	END
	ELSE IF @ImportType = 1 -- BSI FTP
	BEGIN
		SET @cmd = @PSCP + ' -ls ' + @FTP_DIR
		INSERT INTO #OUTPUT(output)
		EXEC xp_cmdshell @cmd
	END
	ELSE IF @ImportType = 2 -- Network File
	BEGIN
		SET @cmd = 'dir ' + QUOTENAME(@FILE_DIR,'"')
		INSERT INTO #OUTPUT(output)
		EXEC xp_cmdshell @cmd
	END
	-------------------------------------------------------------

	IF EXISTS(SELECT * FROM #OUTPUT WHERE output LIKE @FilePattern)
	BEGIN
		-- PRINT PROGRESS ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': COPY FTP FILES FILE SCRIPT'
		RAISERROR(@print,10,1) WITH NOWAIT
		---------------------------------------------------------------

		-- COPY FTP FILES FILE SCRIPT -------------------------------
		IF @ImportType = 0 -- PHG FTP
		BEGIN
			DELETE dbo.FTP_COMMAND WHERE [Application] = @AppName
	
			INSERT INTO dbo.FTP_COMMAND([Application],Command)
			SELECT @AppName,'cd ' + @FTP_DIR

			DECLARE curPath CURSOR FAST_FORWARD LOCAL FOR
				SELECT FilePath FROM [dbo].[fn_GetFilePathTable](@SvrName,@AppName,'DOWNLOAD')
			OPEN curPath
			FETCH NEXT FROM curPath INTO @DOWNLOAD

			WHILE @@FETCH_STATUS = 0
			BEGIN
				INSERT INTO dbo.FTP_COMMAND([Application],Command)
				SELECT @AppName,'get ' + RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) + ' ' + QUOTENAME(@DOWNLOAD + RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1),'"')
				FROM #OUTPUT
				WHERE RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) LIKE @FilePattern
		
				FETCH NEXT FROM curPath INTO @DOWNLOAD
			END
			DEALLOCATE curPath

			INSERT INTO dbo.FTP_COMMAND([Application],Command)
			SELECT @AppName,'quit'

			SET @cmd = 'BCP "SELECT Command FROM ETL.[dbo].FTP_COMMAND WHERE [Application] = ' + QUOTENAME(@AppName,'''') + '" queryout "' + @SCRIPTS + '\' + @AppName + '_FTP_COPY.txt" -S' + @SvrName + ' -t"~" -c -T'
			EXEC xp_cmdshell @cmd

			SET @cmd = @PSCP + ' -b ' + @SCRIPTS + '\' + @AppName + '_FTP_COPY.txt'
			EXEC xp_cmdshell @cmd
		END
		ELSE IF @ImportType IN(1,2) -- BSI FTP & Network File
		BEGIN
			IF OBJECT_ID('tempdb..#DOS_OUTPUT') IS NOT NULL
				DROP TABLE #DOS_OUTPUT;
			CREATE TABLE #DOS_OUTPUT(output nvarchar(MAX))


			DECLARE curPath CURSOR FAST_FORWARD LOCAL FOR
				SELECT FilePath FROM [dbo].[fn_GetFilePathTable](@SvrName,@AppName,'DOWNLOAD')
			OPEN curPath
			FETCH NEXT FROM curPath INTO @DOWNLOAD

			WHILE @@FETCH_STATUS = 0
			BEGIN
				-- GET EXISTING FILES ---------------------------------------
				DELETE #DOS_OUTPUT

				SET @cmd = 'dir/b ' + @DOWNLOAD
				INSERT INTO #DOS_OUTPUT(output)
				EXEC xp_cmdshell @cmd
				-------------------------------------------------------------

				-- COPY NEW FILES -------------------------------------------
				DECLARE curFile CURSOR FAST_FORWARD LOCAL FOR
					SELECT [FileName]
					FROM (
							SELECT RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) AS [FileName]
							FROM #OUTPUT
							WHERE RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) LIKE @FilePattern
								EXCEPT
							SELECT output AS [FileName] FROM #DOS_OUTPUT
						  ) x
				OPEN curFile
				FETCH NEXT FROM curFile INTO @FileName
				SET @NewFiles = @DOWNLOAD + @FileName

				WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @ImportType = 1 -- BSI FTP
						SET @cmd = @PSCP + ' ' + @FTP_DIR + '/' + @FileName + ' ' + QUOTENAME(@NewFiles,'"')
					ELSE IF @ImportType = 2 -- Network File
						SET @cmd = 'COPY/Y "' + @FILE_DIR + '\' + @FileName + '" ' + QUOTENAME(@NewFiles,'"')

					EXEC xp_cmdshell @cmd

					FETCH NEXT FROM curFile INTO @FileName
					SET @NewFiles = @DOWNLOAD + @FileName
				END
				DEALLOCATE curFile
				-------------------------------------------------------------

				FETCH NEXT FROM curPath INTO @DOWNLOAD
			END
			DEALLOCATE curPath

		END
		-------------------------------------------------------------

		-- PRINT PROGRESS ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Queue'
		RAISERROR(@print,10,1) WITH NOWAIT
		---------------------------------------------------------------
		
		-- POPULATE Queue -------------------------------------------
		SELECT TOP 1 @DOWNLOAD = FilePath FROM [dbo].[fn_GetFilePathTable](@SvrName,@AppName,'DOWNLOAD')

		INSERT INTO dbo.[Queue]([Application],FilePath,QueueStatus,RunInOldProd)
		SELECT x.[Application],x.FilePath,x.QueueStatus,1
		FROM (	SELECT @AppName AS [Application],@DOWNLOAD + RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) AS FilePath,0 AS QueueStatus
				FROM #OUTPUT
				WHERE RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) LIKE @FilePattern
			  ) x
			LEFT JOIN dbo.[Queue] q ON q.[Application] = x.Application AND q.FilePath = x.FilePath AND q.QueueStatus != 3
		WHERE q.QueueID IS NULL
		-------------------------------------------------------------

		-- PRINT PROGRESS ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': VERIFY FILE EXISTS'
		RAISERROR(@print,10,1) WITH NOWAIT
		---------------------------------------------------------------

		-- VERIFY FILE EXISTS ---------------------------------------
		IF OBJECT_ID('tempdb..#OUTPUT2') IS NOT NULL
			DROP TABLE #OUTPUT2;
		CREATE TABLE #OUTPUT2(output nvarchar(MAX))

		DECLARE @FileVerify varchar(255)

		DECLARE curPath CURSOR FAST_FORWARD LOCAL FOR
			SELECT FilePath FROM [dbo].[fn_GetFilePathTable](@SvrName,@AppName,'DOWNLOAD')
		OPEN curPath
		FETCH NEXT FROM curPath INTO @DOWNLOAD

		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE curVerify CURSOR FAST_FORWARD LOCAL FOR
				SELECT 'dir/b "' + @DOWNLOAD + RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) + '"'
				FROM #OUTPUT
				WHERE RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) LIKE @FilePattern
			OPEN curVerify
			FETCH NEXT FROM curVerify INTO @FileVerify

			WHILE @@FETCH_STATUS = 0
			BEGIN
				DELETE #OUTPUT2

				INSERT INTO #OUTPUT2(output)
				EXEC xp_cmdshell @FileVerify

				IF EXISTS(SELECT * FROM #OUTPUT2 WHERE output = 'File Not Found')
				BEGIN
					SET @print = 'File Not Copied Local [' + @FileVerify + ']'
					RAISERROR(@print,16,1)
					RETURN -1;
				END

				FETCH NEXT FROM curVerify INTO @FileVerify
			END
			DEALLOCATE curVerify

			FETCH NEXT FROM curPath INTO @DOWNLOAD
		END
		DEALLOCATE curPath
		-------------------------------------------------------------

		-- PRINT PROGRESS ---------------------------------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': DELETE FILES OFF FTP SERVER'
		RAISERROR(@print,10,1) WITH NOWAIT
		---------------------------------------------------------------

		-- DELETE FILES OFF FTP SERVER ------------------------------
		IF @ImportType = 0 -- PHG FTP
		BEGIN
			DELETE dbo.FTP_COMMAND WHERE [Application] = @AppName
	
			INSERT INTO dbo.FTP_COMMAND([Application],Command)
			SELECT @AppName,'cd ' + @FTP_DIR
				UNION ALL
			SELECT @AppName,'del ' + RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1)
			FROM #OUTPUT
			WHERE RIGHT(output,CHARINDEX(' ',REVERSE(output),1) -1) LIKE @FilePattern
				UNION ALL
			SELECT @AppName,'quit'

			SET @cmd = 'BCP "SELECT Command FROM ETL.[dbo].FTP_COMMAND WHERE [Application] = ' + QUOTENAME(@AppName,'''') + '" queryout "' + @SCRIPTS + '\' + @AppName + '_FTP_DEL.txt" -S' + @SvrName + ' -t"~" -c -T'
			EXEC xp_cmdshell @cmd

			SET @cmd = @PSCP + ' -b ' + @SCRIPTS + '\' + @AppName + '_FTP_DEL.txt'
			EXEC xp_cmdshell @cmd
		END
		-------------------------------------------------------------
	END

	-- PRINT PROGRESS ---------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	---------------------------------------------------------------

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPExport_LOG]'
GO
CREATE TABLE [dbo].[IPExport_LOG]
(
[IPExport_LOG_ID] [int] NOT NULL IDENTITY(1, 1),
[LogDate] [datetime] NOT NULL,
[LogStatus] [tinyint] NOT NULL CONSTRAINT [DF_IPExport_LOG_LogStatus] DEFAULT ((0)),
[LogStatus_Desc] AS (case [LogStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' when (3) then 'Error' when (4) then 'Resolved' else 'Error' end),
[LogMessage] [xml] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_IPExport_LOG] on [dbo].[IPExport_LOG]'
GO
ALTER TABLE [dbo].[IPExport_LOG] ADD CONSTRAINT [PK_IPExport_LOG] PRIMARY KEY CLUSTERED  ([IPExport_LOG_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[IPExport_FTP_OUTPUT]'
GO
CREATE TABLE [dbo].[IPExport_FTP_OUTPUT]
(
[output] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[IPExport_ExportToFTP]'
GO

ALTER PROCEDURE [dbo].[IPExport_ExportToFTP]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE  @SvrName nvarchar(255) = @@SERVERNAME,
			@AppName varchar(255) = 'IPExport',
			@cmd varchar(2000),
			@Source varchar(255),
			@Dest varchar(255),
			@App varchar(255),
			@Scripts varchar(255)

	SELECT @Source = dbo.fn_GetFilePath(@SvrName,@AppName,'FTP SOURCE'),
			@Dest = dbo.fn_GetFilePath(@SvrName,@AppName,'FTP DEST'),
			@App = dbo.fn_GetFilePath(@SvrName,@AppName,'FTP APP'),
			@Scripts = dbo.fn_GetFilePath(@SvrName,@AppName,'SCRIPTS')



	-- CREATE DOS BATCH FILE ------------------------------------
	DELETE dbo.FTP_COMMAND WHERE [Application] = @AppName
	
	INSERT INTO dbo.FTP_COMMAND([Application],Command)
	SELECT @AppName,'for /f "tokens=2-4 delims=/ " %%a in (''date /t'') do (set mydate=%%c%%a%%b)'
		UNION ALL
	SELECT @AppName,' '
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\city_master.txt ' + @Dest + '/CityMasterFile/CityMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\country_master.txt ' + @Dest + '/CountryMasterFile/CountryMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\crogroup_master.txt ' + @Dest + '/CROGroupMasterFile/CROGroupMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\currency_master.txt ' + @Dest + '/CurrencyMasterFile/CurrencyMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\hotels_master.txt ' + @Dest + '/HotelMasterFile/HotelMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\state_master.txt ' + @Dest + '/StateMasterFile/StateMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,@App + ' ' + @Source + '\templategroup_master.txt ' + @Dest + '/TemplateGroupMasterFile/TemplateGroupMaster_%mydate%.csv'
		UNION ALL
	SELECT @AppName,'IF NOT ERRORLEVEL 0 GOTO BSIFAIL'
		UNION ALL
	SELECT @AppName,' '
		UNION ALL
	SELECT @AppName,':Success'
		UNION ALL
	SELECT @AppName,'REM SUCCESS (%mydate%)'
		UNION ALL
	SELECT @AppName,'EXIT'
		UNION ALL
	SELECT @AppName,' '
		UNION ALL
	SELECT @AppName,':BSIFAIL'
		UNION ALL
	SELECT @AppName,'REM FAILED (%mydate%)'
		UNION ALL
	SELECT @AppName,'EXIT'

	SET @cmd = 'BCP "SELECT Command FROM ETL.dbo.FTP_COMMAND WHERE [Application] = ' + QUOTENAME(@AppName,'''') + '" queryout "' + @SCRIPTS + '\BSIExport\BSIExport.bat" -S' + @SvrName + ' -t"~" -c -T'
	EXEC xp_cmdshell @cmd
	-------------------------------------------------------------

	-- FTP OUTPUT LOG -------------------------------------------
	DELETE dbo.IPExport_FTP_OUTPUT

	SET @cmd = @SCRIPTS + '\BSIExport\BSIExport.bat'

	INSERT INTO dbo.IPExport_FTP_OUTPUT(output)
	EXEC xp_cmdshell @cmd
	-------------------------------------------------------------

	-- LOG RESULTS ----------------------------------------------
	INSERT INTO IPExport_LOG(LogDate,LogStatus,LogMessage)
	SELECT GETDATE(),
		CASE WHEN [output] LIKE '%REM SUCCESS %' THEN 2 ELSE 3 END,
		(SELECT * FROM dbo.[IPExport_FTP_OUTPUT] FOR XML RAW)
	FROM dbo.[IPExport_FTP_OUTPUT]
	WHERE [output] LIKE '%REM%'
	-------------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[IPCustProf_FinalImport]'
GO


ALTER PROCEDURE [dbo].[IPCustProf_FinalImport]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @FileName varchar(100)
	SELECT @FileName = RIGHT(FilePath,CHARINDEX('\',REVERSE(FilePath),1) -1) FROM dbo.[Queue] WHERE QueueID = @QueueID

	DECLARE @firstDayOfMonth datetime = DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0);

	UPDATE	Cust
		SET	[First_Name] = STG.[First Name],
			[Middle_Name] = STG.[Middle Name],
			[Last_Name] = STG.[Last Name],
			[Zip_Postal] = STG.[Zip Postal],
			[City] = STG.City,
			[Address] = STG.[Address],
			[Address2] = STG.[Address 2],
			[State] = STG.[State],
			[Country] = STG.Country,
			[Email] = STG.Email,
			[Current_Points_Balance] = STG.[Current_Points_Balance],
			[Tier] = STG.Tier,
			[Membership_Date] = STG.[Membership Date],
			[e-statement] = STG.[e-statement],
			[Special_Offers] = STG.[Special Offers],
			[Member_Surveys] = STG.[Member Surveys],
			[Partner_Offers] = STG.[Partner Offers],
			[SignedupHotelCode] = STG.[sign up hotel],
			[Enrollment_Source] = STG.[source of enrollment],
			[Import_Date] = GETDATE(),
			[File_Name] = @FileName
	FROM [Superset].[BSI].[Customer_Profile_Import] Cust
		INNER JOIN(SELECT [iPrefer Number],[First Name],[Middle Name],[Last Name],[Zip Postal],[City],[Address],[Address 2],[State], Country, Email,
						MAX([Current points balance]) AS [Current_Points_Balance],
						[Tier],[Membership Date],[e-statement],[Special Offers],[Member Surveys],[Partner Offers],[sign up hotel],[source of enrollment]
					FROM [dbo].[Import_IPCustProf]
					WHERE QueueID = @QueueID
					GROUP BY [iPrefer Number],[First Name],[Middle Name],[Last Name],[Zip Postal],[City],[Address],[Address 2],[State],[Country],[Email],[Tier],
							[Membership Date],[e-statement],[Special Offers],[Member Surveys],[Partner Offers],[sign up hotel],[source of enrollment]
				   ) STG ON Cust.iPRefer_number = STG.[iprefer number]
	WHERE	ISNULL(Cust.[First_Name],'') <> ISNULL(STG.[First Name],'')
		OR	ISNULL(Cust.[Middle_Name],'') <> ISNULL(STG.[Middle Name],'')
		OR	ISNULL(Cust.[Last_Name],'') <> ISNULL(STG.[Last Name],'')
		OR	ISNULL(Cust.[Zip_Postal],'') <> ISNULL(STG.[Zip Postal],'')
		OR	ISNULL(Cust.[City],'') <> ISNULL(STG.City, '')
		OR	ISNULL(Cust.[Address],'') <> ISNULL(STG.[Address],'')
		OR	ISNULL(Cust.[Address2],'') <> ISNULL(STG.[Address 2],'')
		OR	ISNULL(Cust.[State],'') <> ISNULL(STG.[State],'')
		OR	ISNULL(Cust.[Country],'') <> ISNULL(STG.Country, '')
		OR	ISNULL(Cust.[Email],'') <> ISNULL(STG.Email, '')
		OR	ISNULL(Cust.[Current_Points_Balance],'') <> ISNULL(STG.[Current_Points_Balance],'')
		OR	ISNULL(Cust.[Tier],'') <> ISNULL(STG.Tier, '')
		OR	ISNULL(Cust.[Membership_Date],'') <> ISNULL(STG.[Membership Date],'')
		OR	ISNULL(Cust.[e-statement],'') <> ISNULL(STG.[e-statement],'')
		OR	ISNULL(Cust.[Special_Offers],'') <> ISNULL(STG.[Special Offers],'')
		OR	ISNULL(Cust.[Member_Surveys],'') <> ISNULL(STG.[Member Surveys],'')
		OR	ISNULL(Cust.[Partner_Offers],'') <> ISNULL(STG.[Partner Offers],'')
		OR	ISNULL(Cust.[SignedupHotelCode],'') <> ISNULL(STG.[sign up hotel],'')
		OR	ISNULL(Cust.[Enrollment_Source],'') <> ISNULL(STG.[source of enrollment],'')



	INSERT INTO [Superset].[BSI].[Customer_Profile_Import]([iPrefer_Number],[First_Name],[Middle_Name],[Last_Name],[Zip_Postal],[City],[Address],[Address2],[State],
															[Country],[Email],[Current_Points_Balance],[Tier],[Membership_Date],[e-statement],[Special_Offers],
															[Member_Surveys],[Partner_Offers],[SignedupHotelCode],[Enrollment_Source],[Import_Date],[File_Name])
	SELECT [iPrefer Number],[First Name],[Middle Name],[Last Name],[Zip Postal],[City],[Address],[Address 2],[State],
			[Country],[Email],MAX([Current points balance]),[Tier],[Membership Date],[e-statement],[Special Offers],
			[Member Surveys],[Partner Offers],[sign up hotel],[source of enrollment],GETDATE(),@FileName
	FROM	[dbo].[Import_IPCustProf]
	WHERE [iprefer number] NOT IN(SELECT iPRefer_number FROM [Superset].[BSI].[Customer_Profile_Import])
	GROUP BY [iPrefer Number],[First Name],[Middle Name],[Last Name],[Zip Postal],[City],[Address],[Address 2],[State],
			[Country],[Email],[Tier],[Membership Date],[e-statement],[Special Offers],
			[Member Surveys],[Partner Offers],[sign up hotel],[source of enrollment]
	

	;WITH cte_IpNum([iprefer number],MembershipDate)
	AS
	(
		SELECT DISTINCT [iprefer number],[Membership Date]
		FROM [dbo].[Import_IPCustProf]
		WHERE [iprefer number] NOT IN(	SELECT iPRefer_number
										FROM [Superset].[BSI].[Customer_Profile_Import]
									 )
	)
	UPDATE mrt
		SET LoyaltyNumberValidated = 1
	FROM Superset.dbo.mostrecenttransactions mrt
		INNER JOIN cte_IpNum c ON c.[iprefer number] = mrt.loyaltyNumber
	WHERE mrt.confirmationDate >= c.MembershipDate
		AND mrt.confirmationDate >= @firstDayOfMonth
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[Sabre_subQueue]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_Sabre_subQueue]
(
[subQueueID] [int] NOT NULL IDENTITY(1, 1),
[QueueID] [int] NOT NULL,
[FilePath] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF__Sabre_sub__Creat__6383C8BA] DEFAULT (getdate()),
[QueueStatus] [tinyint] NOT NULL,
[ImportStarted] [datetime] NULL,
[ImportFinished] [datetime] NULL,
[Message] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImportCount] [int] NULL,
[QueueStatus_Desc] AS (case [QueueStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' else 'Error' end) PERSISTED NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Sabre_subQueue] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_Sabre_subQueue]([subQueueID], [QueueID], [FilePath], [CreateDate], [QueueStatus], [ImportStarted], [ImportFinished], [Message], [ImportCount]) SELECT [subQueueID], [QueueID], [FilePath], [CreateDate], [QueueStatus], [ImportStarted], [ImportFinished], [Message], [ImportCount] FROM [dbo].[Sabre_subQueue]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_Sabre_subQueue] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Sabre_subQueue]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[tmp_rg_xx_Sabre_subQueue]', RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[Sabre_subQueue]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_Sabre_subQueue]', N'Sabre_subQueue'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[ETL_ApplicationImportStatus]'
GO

ALTER PROCEDURE [dbo].[ETL_ApplicationImportStatus]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-- UPDATE ETL STATUS --------------------------------------
	;WITH cte_Queue
	AS
	(
		SELECT q.[Application],q.FilePath,q.CreateDate,q.QueueStatus_Desc,q.[Message],q.ImportCount
		FROM dbo.[Queue] q
			INNER JOIN (SELECT [Application],MAX(QueueID) AS QueueID FROM dbo.[Queue] GROUP BY [Application]) maxQ ON maxQ.QueueID = q.QueueID
		
		UNION ALL

		SELECT 'CurrencyImport' AS [Application],c.URL AS FilePath,c.CreateDate,c.ImportStatus_Desc,c.[Message],c.ImportCount
		FROM Import_Currency c
			INNER JOIN (SELECT MAX([Import_CurrencyID]) AS [Import_CurrencyID] FROM Import_Currency) maxC ON maxC.Import_CurrencyID = c.Import_CurrencyID

		UNION ALL

		SELECT 'IPExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM IPExport_LOG
		WHERE IPExport_LOG_ID IN(SELECT MAX(IPExport_LOG_ID) FROM IPExport_LOG)

		UNION ALL

		SELECT 'SabreExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,LogMessage AS [Message],NULL AS [ImportCount]
		FROM SabreExport_LOG
		WHERE SabreExport_LOG_ID IN(SELECT MAX(SabreExport_LOG_ID) FROM SabreExport_LOG)
	)
	UPDATE a
		SET ETL_LastRunDate = c.CreateDate
	FROM ETL.dbo.ETL_Applications a
		INNER JOIN cte_Queue c ON c.[Application] = a.[Application] AND (c.CreateDate > a.ETL_LastRunDate OR a.ETL_LastRunDate IS NULL)
	WHERE a.IsActive = 1
	-----------------------------------------------------------

	-- REPORT ETL STATUS --------------------------------------
	;WITH cte_Queue
	AS
	(
		SELECT q.[Application],q.FilePath,q.CreateDate,q.QueueStatus_Desc,q.[Message],q.ImportCount
		FROM dbo.[Queue] q
			INNER JOIN (SELECT [Application],MAX(QueueID) AS QueueID FROM dbo.[Queue] GROUP BY [Application]) maxQ ON maxQ.QueueID = q.QueueID
		
		UNION ALL

		SELECT 'CurrencyImport' AS [Application],c.URL AS FilePath,c.CreateDate,c.ImportStatus_Desc,c.[Message],c.ImportCount
		FROM Import_Currency c
			INNER JOIN (SELECT MAX([Import_CurrencyID]) AS [Import_CurrencyID] FROM Import_Currency) maxC ON maxC.Import_CurrencyID = c.Import_CurrencyID

		UNION ALL

		SELECT 'IPExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,CONVERT(nvarchar(MAX),LogMessage) AS [Message],NULL AS [ImportCount]
		FROM IPExport_LOG
		WHERE IPExport_LOG_ID IN(SELECT MAX(IPExport_LOG_ID) FROM IPExport_LOG)

		UNION ALL

		SELECT 'SabreExport' AS [Application],'' AS [FilePath],LogDate AS [CreateDate],LogStatus_Desc,CONVERT(nvarchar(MAX),LogMessage) AS [Message],NULL AS [ImportCount]
		FROM SabreExport_LOG
		WHERE SabreExport_LOG_ID IN(SELECT MAX(SabreExport_LOG_ID) FROM SabreExport_LOG)
	)
	SELECT a.JobName AS [Job Name],
			'Every ' + CONVERT(varchar(20),a.ETL_FrequencyInterval) + CASE a.ETL_FrequencyType WHEN 1 THEN ' day(s)' WHEN 2 THEN ' week(s)' WHEN 3 THEN ' month(s)' END AS [File Upload Interval],
			CASE WHEN a.ETL_NextRunDate >= GETDATE() THEN 'ON TIME' ELSE 'LATE' END AS [File Upload Status],
			CONVERT(varchar(50),a.ETL_NextRunDate,120) AS [Next Import Date],
			CONVERT(varchar(50),c.CreateDate,120) AS [Last Import Date],c.QueueStatus_Desc AS [Last Import Status],
			c.[Message] AS [Last Import Message],c.ImportCount AS [Last Import Count],
			a.DocumentationPath,c.FilePath
	FROM dbo.ETL_Applications a
		INNER JOIN cte_Queue c ON c.[Application] = a.[Application]
	WHERE a.IsActive = 1
	ORDER BY a.JobName
	-----------------------------------------------------------
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Import_IPTaggedCustomers]'
GO
CREATE TABLE [dbo].[Import_IPTaggedCustomers]
(
[QueueID] [int] NOT NULL,
[Hotel_Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iPrefer Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[taggedStatus] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateTagged] [date] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Import_IPTransactionDetailedReport]'
GO
CREATE TABLE [dbo].[Import_IPTransactionDetailedReport]
(
[QueueID] [int] NOT NULL,
[Transaction Id] [bigint] NULL,
[Transaction Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Remarks] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[iPrefer Number] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Member Status] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Booking ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Hotel Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Arrival Date] [datetime] NULL,
[Departure Date] [datetime] NULL,
[Booking Source] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Campaign] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Points Earned] [decimal] (28, 0) NULL,
[Points Redemeed] [decimal] (28, 0) NULL,
[Reservation Revenue] [decimal] (28, 0) NULL,
[Currency Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transaction Date] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[Sabre_RunReport]'
GO

ALTER PROCEDURE [dbo].[Sabre_RunReport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--Send iPreferFraud Report
	exec msdb.dbo.sp_start_job 'A8E06629-0870-4616-9085-6D52E854D458'

	--Send iPreferAutoEnroll Report
	exec msdb.dbo.sp_start_job 'EC2B5DDA-D41E-4587-91CA-4BC1A913ADC5'

	--Send iPreferAutoEnroll Bookings Report
	exec msdb.dbo.sp_start_job 'F82541DD-0058-4DE4-AA41-17B298CB8166'

	--Send iPreferEmailMatchBookings Report
	--exec msdb.dbo.sp_start_job 'D991F475-1A4A-4BF0-810E-9E42586CB427'

END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Import_Currency_Data]'
GO
ALTER TABLE [dbo].[Import_Currency_Data] ADD CONSTRAINT [FK_Import_Currency_Data_Import_CurrencyID] FOREIGN KEY ([Import_CurrencyID]) REFERENCES [dbo].[Import_Currency] ([Import_CurrencyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Import_Currency_Header]'
GO
ALTER TABLE [dbo].[Import_Currency_Header] ADD CONSTRAINT [FK_Import_Currency_Header_Import_CurrencyID] FOREIGN KEY ([Import_CurrencyID]) REFERENCES [dbo].[Import_Currency] ([Import_CurrencyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
