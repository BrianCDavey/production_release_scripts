/*
Run this script on:

        chi-sq-pr-01\warehouse.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00033391.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 10.7.0 from Red Gate Software Ltd at 1/12/2018 1:55:01 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[Import_LuxLink]'
GO
ALTER TABLE [dbo].[Import_LuxLink] ADD
[Inventory_Type] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SELLER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SELLER_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ORDER_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ORDER_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_MODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_CAPTURE_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_AUTH_TIME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_CAPTURE_TIME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_CREDIT_TIME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_VOID_TIME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_AUTH_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_CAPTURE_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_VOID_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_CREDIT_AMT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_AUTH_REF] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAYMENT_MER_REF] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [DOWNLOAD_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ORDER_SUBTOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIPPING_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [TAX_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [REFUND_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [CHARGED_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [DISCOUNT_TOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [CREATE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIPMENT_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIPMENT_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIPMENT_METHOD] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIPMENT_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [USER_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_TO_ADDRESS_1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_TO_ADDRESS_2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_CITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_TO_STATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_TO_ZIP] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_COUNTRY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PHONE_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [EMAIL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [USER_REFERENCE1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ORDER_ITEM_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ITEM_SHIPMENT_REQ] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SITE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SITE_NAMECODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [LISTING_FORMAT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [LISTING_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [QTY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ITEM_PRICE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [DISCOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIP_DISCOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [ITEM_SUBTOTAL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SHIPPING_CHARGES] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SKU_TAG_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SKU_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [LOCATION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [TITLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [REL_REFERENCE1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [FRAUD_SCORE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [PAID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [RETAIL_PRICE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [RULE_APPLIED] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [RULE_DISCOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [RULE_SHIP_DISCOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [RULE_DISCOUNT_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SALE_EVENT_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [SALE_EVENT_NAMECODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [BASE_ITEM_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [BASE_ITEM_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [CATEGORY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [Hotel_Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[Import_LuxLink] ALTER COLUMN [Minimum_Number_of_Nights] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[LuxLink_FinalImport]'
GO



ALTER PROCEDURE [dbo].[LuxLink_FinalImport]
	@QueueId int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	INSERT INTO LuxuryLink.dbo.LLOrders(LLOrders_QueueId,SELLER_ID,SELLER_NAME,ORDER_ID,ORDER_STATUS,PAYMENT_MODE,PAYMENT_STATUS,PAYMENT_CAPTURE_STATUS,PAYMENT_AUTH_TIME,
			PAYMENT_CAPTURE_TIME,PAYMENT_CREDIT_TIME,PAYMENT_VOID_TIME,PAYMENT_AUTH_AMT,PAYMENT_CAPTURE_AMT,
			PAYMENT_CREDIT_AMT,PAYMENT_VOID_AMT,
			PAYMENT_AUTH_REF,PAYMENT_MER_REF,
			DOWNLOAD_STATUS,ORDER_SUBTOTAL,SHIPPING_TOTAL,TAX_TOTAL,TOTAL,REFUND_TOTAL,CHARGED_TOTAL,DISCOUNT_TOTAL,CREATE_DATE,SHIPMENT_ID,
			SHIPMENT_STATUS,SHIPMENT_METHOD,SHIPMENT_DATE,[USER_NAME],FIRST_NAME,LAST_NAME,SHIP_TO_ADDRESS_1,SHIP_TO_ADDRESS_2,SHIP_CITY,
			SHIP_TO_STATE,SHIP_TO_ZIP,SHIP_COUNTRY,PHONE_NUMBER,EMAIL,USER_REFERENCE1,ORDER_ITEM_ID,ITEM_SHIPMENT_REQ,SITE_ID,SITE_NAMECODE,
			LISTING_FORMAT,LISTING_ID,QTY,ITEM_PRICE,DISCOUNT,SHIP_DISCOUNT,ITEM_SUBTOTAL,SHIPPING_CHARGES,SKU_TAG_NAME,SKU_NAME,LOCATION,
			TITLE,REL_REFERENCE1,FRAUD_SCORE,PAID,RETAIL_PRICE,RULE_APPLIED,RULE_DISCOUNT,RULE_SHIP_DISCOUNT,RULE_DISCOUNT_CODE,SALE_EVENT_ID,
			SALE_EVENT_NAMECODE,BASE_ITEM_TYPE,BASE_ITEM_ID,CATEGORY,Hotel_Name,Minimum_Number_of_Nights,Inventory_Type)
	SELECT QueueId,REPLACE(SELLER_ID,',',''),SELLER_NAME,REPLACE(ORDER_ID,',',''),ORDER_STATUS,PAYMENT_MODE,PAYMENT_STATUS,PAYMENT_CAPTURE_STATUS,PAYMENT_AUTH_TIME,
			PAYMENT_CAPTURE_TIME,PAYMENT_CREDIT_TIME,PAYMENT_VOID_TIME,REPLACE(PAYMENT_AUTH_AMT,',',''),REPLACE(PAYMENT_CAPTURE_AMT,',',''),
			REPLACE(PAYMENT_CREDIT_AMT,',',''),REPLACE(PAYMENT_VOID_AMT,',',''),
			PAYMENT_AUTH_REF,PAYMENT_MER_REF,DOWNLOAD_STATUS,REPLACE(ORDER_SUBTOTAL,',',''),REPLACE(SHIPPING_TOTAL,',',''),REPLACE(TAX_TOTAL,',',''),
			REPLACE(TOTAL,',',''),REPLACE(REFUND_TOTAL,',',''),REPLACE(CHARGED_TOTAL,',',''),REPLACE(DISCOUNT_TOTAL,',',''),CREATE_DATE,SHIPMENT_ID,
			SHIPMENT_STATUS,SHIPMENT_METHOD,SHIPMENT_DATE,[USER_NAME],FIRST_NAME,LAST_NAME,SHIP_TO_ADDRESS_1,SHIP_TO_ADDRESS_2,SHIP_CITY,
			SHIP_TO_STATE,SHIP_TO_ZIP,SHIP_COUNTRY,PHONE_NUMBER,EMAIL,USER_REFERENCE1,ORDER_ITEM_ID,ITEM_SHIPMENT_REQ,SITE_ID,SITE_NAMECODE,
			LISTING_FORMAT,LISTING_ID,REPLACE(QTY,',',''),REPLACE(ITEM_PRICE,',',''),REPLACE(DISCOUNT,',',''),REPLACE(SHIP_DISCOUNT,',',''),
			REPLACE(ITEM_SUBTOTAL,',',''),REPLACE(SHIPPING_CHARGES,',',''),SKU_TAG_NAME,SKU_NAME,LOCATION,TITLE,REL_REFERENCE1,FRAUD_SCORE,PAID,
			REPLACE(RETAIL_PRICE,',',''),RULE_APPLIED,REPLACE(RULE_DISCOUNT,',',''),REPLACE(RULE_SHIP_DISCOUNT,',',''),RULE_DISCOUNT_CODE,
			SALE_EVENT_ID,SALE_EVENT_NAMECODE,BASE_ITEM_TYPE,BASE_ITEM_ID,CATEGORY,Hotel_Name,Minimum_Number_of_Nights,Inventory_Type
	FROM dbo.Import_LuxLink
	WHERE QueueId = @QueueId
		AND REPLACE(ORDER_ID,',','') NOT IN(SELECT ORDER_ID FROM LuxuryLink.dbo.LLOrders)
END




GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
