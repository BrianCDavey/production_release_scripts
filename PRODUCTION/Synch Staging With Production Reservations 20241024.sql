/*
Run this script on:

        wus2-data-st-vm.Reservations    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/24/2024 3:37:43 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [FK_dbo_Guest_Guest_EmailAddressID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [FK_dbo_Guest_LocationID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] DROP CONSTRAINT [PK_dbo_GuestID]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_hashKey_IC_Others] from [dbo].[Guest]'
GO
DROP INDEX [IX_hashKey_IC_Others] ON [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Guest_EmailAddressID_IC_Others] from [dbo].[Guest]'
GO
DROP INDEX [IX_Guest_EmailAddressID_IC_Others] ON [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_Guest_EmailAddressID] from [dbo].[Guest]'
GO
DROP INDEX [IX_Guest_EmailAddressID] ON [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsSynXis_IC_synXisID] from [dbo].[Guest]'
GO
DROP INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_location_AddressID_IC_synXisID_openHospID_IsSynXis_IsOpenHosp] from [dbo].[Guest]'
GO
DROP INDEX [IX_location_AddressID_IC_synXisID_openHospID_IsSynXis_IsOpenHosp] ON [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_IsOpenHosp_IC_openHospID] from [dbo].[Guest]'
GO
DROP INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LoyaltyNumber] from [dbo].[LoyaltyNumber]'
GO
DROP INDEX [IX_LoyaltyNumber] ON [dbo].[LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_PostalCode_Text] from [dbo].[PostalCode]'
GO
DROP INDEX [IX_PostalCode_Text] ON [dbo].[PostalCode]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ConfirmationNumber_IC_sourceKey] from [dbo].[Transactions]'
GO
DROP INDEX [IX_ConfirmationNumber_IC_sourceKey] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_LoyaltyNumberIsNewMember_IC_TransactionStatusID_LoyaltyNumberID] from [dbo].[Transactions]'
GO
DROP INDEX [IX_LoyaltyNumberIsNewMember_IC_TransactionStatusID_LoyaltyNumberID] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_SourceKey_IC_ForeignKeys] from [dbo].[Transactions]'
GO
DROP INDEX [IX_SourceKey_IC_ForeignKeys] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionTimeStamp_IC_ForeignKeys] from [dbo].[Transactions]'
GO
DROP INDEX [IX_TransactionTimeStamp_IC_ForeignKeys] ON [dbo].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID] from [openHosp].[Transactions]'
GO
DROP INDEX [IX_QueueID] ON [openHosp].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID] from [pegasus].[Transactions]'
GO
DROP INDEX [IX_QueueID] ON [pegasus].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_ApiTransactionID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_ApiTransactionID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_intHotelID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_intHotelID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_BookingSourceID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_BookingSourceID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_ConfirmationNumber_TransactionStatusID_TransactionDetailID_BillingDescriptionID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_ConfirmationNumber_TransactionStatusID_TransactionDetailID_BillingDescriptionID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_GuestID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_GuestID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_TransactionStatusID] from [sabreAPI].[Transactions]'
GO
DROP INDEX [IX_QueueID_TransactionStatusID] ON [sabreAPI].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_TransactionID] from [synxis].[MostRecentTransaction]'
GO
DROP INDEX [IX_TransactionID] ON [synxis].[MostRecentTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_intHotelID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_intHotelID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_BookingSourceID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_BookingSourceID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_ConfirmationNumber_TransactionDetailID_BillingDescriptionID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_ConfirmationNumber_TransactionDetailID_BillingDescriptionID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_ConfirmationNumber_TransactionTimeStamp_TransactionStatusID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_ConfirmationNumber_TransactionTimeStamp_TransactionStatusID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_GuestID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_GuestID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_TransactionStatusID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_QueueID_TransactionStatusID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_QueueID_IC_TravelAgentID] from [synxis].[Transactions]'
GO
DROP INDEX [IX_QueueID_IC_TravelAgentID] ON [synxis].[Transactions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [authority].[trg_CRO_Code_Group] on [authority].[CRO_Code_Group]'
GO

CREATE TRIGGER [authority].[trg_CRO_Code_Group]
   ON  [authority].[CRO_Code_Group]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO authority.Process_PH_BookingSource_Rules(EventSource)
	VALUES('authority.trg_CRO_Code_Group')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [authority].[trg_CRO_Code] on [authority].[CRO_Code]'
GO


CREATE TRIGGER [authority].[trg_CRO_Code]
   ON  [authority].[CRO_Code] 
   AFTER INSERT,DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO authority.Process_PH_BookingSource_Rules(EventSource)
	VALUES('authority.trg_CRO_Code')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [authority].[trg_ibeSource] on [authority].[ibeSource]'
GO

CREATE TRIGGER [authority].[trg_ibeSource]
   ON  [authority].[ibeSource]
   AFTER INSERT,DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO authority.Process_PH_BookingSource_Rules(EventSource)
	VALUES('authority.trg_ibeSource')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [authority].[trg_ibeSource_Group] on [authority].[ibeSource_Group]'
GO

CREATE TRIGGER [authority].[trg_ibeSource_Group]
   ON  [authority].[ibeSource_Group]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO authority.Process_PH_BookingSource_Rules(EventSource)
	VALUES('authority.trg_ibeSource_Group')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [authority].[trg_OpenHosp_BookingSource] on [authority].[OpenHosp_BookingSource]'
GO

CREATE TRIGGER [authority].[trg_OpenHosp_BookingSource]
   ON  [authority].[OpenHosp_BookingSource]
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO authority.Process_PH_BookingSource_Rules(EventSource)
	VALUES('authority.trg_OpenHosp_BookingSource')

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Rebuilding [dbo].[Guest]'
GO
CREATE TABLE [dbo].[RG_Recovery_1_Guest]
(
[synXisID] [int] NULL,
[openHospID] [int] NULL,
[GuestID] [int] NOT NULL IDENTITY(1, 1) NOT FOR REPLICATION,
[customerID] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[salutation] [nvarchar] (160) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LocationID] [int] NULL,
[phone] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Guest_EmailAddressID] [int] NULL,
[Location_LocationHashKey] [varbinary] (8000) NULL,
[location_AddressID] [int] NULL,
[hashKey] AS (hashbytes('MD5',(((((((((((((((((upper(isnull([customerID],''))+'|')+upper(isnull([salutation],'')))+'|')+upper(isnull([FirstName],'')))+'|')+upper(isnull([FirstName],'')))+'|')+upper(isnull([LastName],'')))+'|')+upper(isnull([Address1],'')))+'|')+upper(isnull([Address2],'')))+'|')+upper(isnull(CONVERT([varchar](20),[LocationID],(0)),'')))+'|')+upper(isnull([phone],'')))+'|')+upper(isnull(CONVERT([varchar](20),[Guest_EmailAddressID],(0)),'')))) PERSISTED,
[sabreAPI_ID] [int] NULL,
[IsSabreAPI] AS (case  when [sabreAPI_ID] IS NULL then (0) else (1) end),
[IsSynXis] AS (case  when [synXisID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[IsOpenHosp] AS (case  when [openHospID] IS NULL then CONVERT([bit],(0),(0)) else CONVERT([bit],(1),(0)) end) PERSISTED,
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_Guest] ON
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
INSERT INTO [dbo].[RG_Recovery_1_Guest]([synXisID], [openHospID], [GuestID], [customerID], [salutation], [FirstName], [LastName], [Address1], [Address2], [LocationID], [phone], [Guest_EmailAddressID], [Location_LocationHashKey], [location_AddressID], [sabreAPI_ID], [pegasusID]) SELECT [synXisID], [openHospID], [GuestID], [customerID], [salutation], [FirstName], [LastName], [Address1], [Address2], [LocationID], [phone], [Guest_EmailAddressID], [Location_LocationHashKey], [location_AddressID], [sabreAPI_ID], [pegasusID] FROM [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
SET IDENTITY_INSERT [dbo].[RG_Recovery_1_Guest] OFF
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @idVal BIGINT
SELECT @idVal = IDENT_CURRENT(N'[dbo].[Guest]')
IF @idVal IS NOT NULL
    DBCC CHECKIDENT(N'[dbo].[RG_Recovery_1_Guest]', RESEED, @idVal)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP TABLE [dbo].[Guest]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_Guest]', N'Guest', N'OBJECT'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Guest] on [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [PK_dbo_Guest] PRIMARY KEY CLUSTERED ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Guest_EmailAddressID] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_Guest_EmailAddressID] ON [dbo].[Guest] ([Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Guest_EmailAddressID_IC_Others] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_Guest_EmailAddressID_IC_Others] ON [dbo].[Guest] ([Guest_EmailAddressID]) INCLUDE ([salutation], [FirstName], [LastName], [Address1], [Address2], [LocationID], [phone])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hashKey_IC_Others] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_hashKey_IC_Others] ON [dbo].[Guest] ([hashKey]) INCLUDE ([synXisID], [openHospID], [customerID], [salutation], [FirstName], [LastName], [Address1], [Address2], [LocationID], [phone], [Guest_EmailAddressID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Guest] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Guest] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_location_AddressID_IC_synXisID_openHospID_IsSynXis_IsOpenHosp] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_location_AddressID_IC_synXisID_openHospID_IsSynXis_IsOpenHosp] ON [dbo].[Guest] ([location_AddressID]) INCLUDE ([synXisID], [openHospID], [IsSynXis], [IsOpenHosp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_synXisID_IsSynXis] on [dbo].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_synXisID_IsSynXis] ON [dbo].[Guest] ([synXisID], [IsSynXis])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_] on [Prof_Rpt].[Billy]'
GO
CREATE NONCLUSTERED INDEX [IX_] ON [Prof_Rpt].[Billy] ([confirmationNumber], [hotelCode], [billableYear], [billableMonth]) INCLUDE ([sopNumber], [invoiceDate], [invoiceYear], [invoiceMonth], [billableDate], [BillyConfirmationNumber], [PHGSiteBillyConfirmationNumber], [HHASiteBillyConfirmationNumber], [IPreferConfirmationNumber], [BillyConfirmationNumberWithZeroBookAndComm], [itemCodeFamily], [surchargeItemCode], [BillyBookingChargesUSD], [PHGSiteBillyBookingChargesUSD], [HHASiteBillyBookingChargesUSD], [commissionsUSD], [PHGSiteCommissionsUSD], [HHASiteCommissionsUSD], [surchargesUSD], [PHGSiteSurchargesUSD], [HHASiteSurchargesUSD], [BookAndCommUSD], [PHGSiteBookAndCommUSD], [HHASiteBookAndCommUSD], [IPreferChargesUSD], [BillyBookingChargesHotelCurrency], [PHGSiteBillyBookingChargesHotelCurrency], [HHASiteBillyBookingChargesHotelCurrency], [commissionsHotelCurrency], [PHGSiteCommissionsHotelCurrency], [HHASiteCommissionsHotelCurrency], [surchargesHotelCurrency], [PHGSiteSurchargesHotelCurrency], [HHASiteSurchargesHotelCurrency], [BookAndCommHotelCurrency], [PHGSiteBookAndCommHotelCurrency], [HHASiteBookAndCommHotelCurrency], [IPreferChargesHotelCurrency], [IPreferCost])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hotelCode_billableYear_billableMonth] on [Prof_Rpt].[FinalResults]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hotelCode_billableYear_billableMonth] ON [Prof_Rpt].[FinalResults] ([confirmationNumber], [hotelCode], [billableYear], [billableMonth])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hotelCode_billableYear_billableMonth_IC_Others] on [Prof_Rpt].[IPrefer]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hotelCode_billableYear_billableMonth_IC_Others] ON [Prof_Rpt].[IPrefer] ([confirmationNumber], [hotelCode], [billableYear], [billableMonth]) INCLUDE ([sopNumber], [invoiceDate], [invoiceYear], [invoiceMonth], [billableDate], [IPreferConfirmationNumber], [IPreferChargesUSD], [IPreferChargesHotelCurrency], [IPreferCost])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SubSourceCode_Group_SecondarySubSourceCode_Group] on [authority].[SubSourceCode_Group]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SubSourceCode_Group_SecondarySubSourceCode_Group] ON [authority].[SubSourceCode_Group] ([SubSourceCode_Group], [SecondarySubSourceCode_Group])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[ActionType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[ActionType] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[ActionType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[ActionType] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CROCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CROCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CROCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CROCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_BookingSource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_BookingSource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_Channel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_Channel] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_Channel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_Channel] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_SecondarySource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_SecondarySource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_SecondarySource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_SecondarySource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CRS_SubSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CRS_SubSource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CRS_SubSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CRS_SubSource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Chain]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Chain] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Chain]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Chain] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_City_Text] on [dbo].[City]'
GO
CREATE NONCLUSTERED INDEX [IX_City_Text] ON [dbo].[City] ([City_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[City]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[City] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[City]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[City] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CorporateCodeID_corporationCode] on [dbo].[CorporateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_CorporateCodeID_corporationCode] ON [dbo].[CorporateCode] ([CorporateCodeID], [corporationCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[CorporateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[CorporateCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[CorporateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[CorporateCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Country]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Country] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Country]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Country] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Guest_CompanyName]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Guest_CompanyName] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Guest_CompanyName]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Guest_CompanyName] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_emailAddress] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_emailAddress] ON [dbo].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Guest_EmailAddress] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Guest_EmailAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Guest_EmailAddress] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID_IATANumber] on [dbo].[IATANumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID_IATANumber] ON [dbo].[IATANumber] ([IATANumberID], [IATANumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[IATANumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[IATANumber] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[IATANumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[IATANumber] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Location]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Location] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Location]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Location] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[LoyaltyNumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[LoyaltyNumber] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[LoyaltyNumber]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[LoyaltyNumber] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_loyaltyNumber] on [dbo].[LoyaltyNumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_loyaltyNumber] ON [dbo].[LoyaltyNumber] ([loyaltyNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PH_ChannelID] on [dbo].[PH_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_PH_ChannelID] ON [dbo].[PH_BookingSource] ([PH_ChannelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PH_SecondaryChannelID] on [dbo].[PH_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_PH_SecondaryChannelID] ON [dbo].[PH_BookingSource] ([PH_SecondaryChannelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PH_SubChannelID] on [dbo].[PH_BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_PH_SubChannelID] ON [dbo].[PH_BookingSource] ([PH_SubChannelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[PMSRateTypeCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PMSRateTypeCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[PMSRateTypeCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PMSRateTypeCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[PostalCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PostalCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[PostalCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PostalCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_PostalCode_Text_IC_synXisID_openHospID] on [dbo].[PostalCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_PostalCode_Text_IC_synXisID_openHospID] ON [dbo].[PostalCode] ([PostalCode_Text]) INCLUDE ([openHospID], [synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[PromoCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[PromoCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[PromoCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[PromoCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PromoCodeID_promotionalCode] on [dbo].[PromoCode]'
GO
CREATE NONCLUSTERED INDEX [IX_PromoCodeID_promotionalCode] ON [dbo].[PromoCode] ([PromoCodeID], [promotionalCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[RateCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RateCategory] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[RateCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RateCategory] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_rateCategoryCode_RateCategoryID] on [dbo].[RateCategory]'
GO
CREATE NONCLUSTERED INDEX [IX_rateCategoryCode_RateCategoryID] ON [dbo].[RateCategory] ([rateCategoryCode], [RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[RateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RateCode] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[RateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RateCode] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RateCodeID_RateCode] on [dbo].[RateCode]'
GO
CREATE NONCLUSTERED INDEX [IX_RateCodeID_RateCode] ON [dbo].[RateCode] ([RateCodeID], [RateCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[Region]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[Region] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[Region]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[Region] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[RoomType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[RoomType] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[RoomType]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[RoomType] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_RoomTypeID_roomTypeName] on [dbo].[RoomType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_RoomTypeID_roomTypeName] ON [dbo].[RoomType] ([RoomTypeID], [roomTypeName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RoomTypeID_roomTypeName_roomTypeCode] on [dbo].[RoomType]'
GO
CREATE NONCLUSTERED INDEX [IX_RoomTypeID_roomTypeName_roomTypeCode] ON [dbo].[RoomType] ([RoomTypeID], [roomTypeName], [roomTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[State]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[State] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[State]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[State] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_State_Text] on [dbo].[State]'
GO
CREATE NONCLUSTERED INDEX [IX_State_Text] ON [dbo].[State] ([State_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_arrivalDate_IC_DepartureDate_LoyaltyNumberValidated] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_arrivalDate_IC_DepartureDate_LoyaltyNumberValidated] ON [dbo].[TransactionDetail] ([arrivalDate]) INCLUDE ([departureDate], [LoyaltyNumberValidated])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_arrivalDate_IC_nights_rooms_reservationRevenue_currency] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_arrivalDate_IC_nights_rooms_reservationRevenue_currency] ON [dbo].[TransactionDetail] ([arrivalDate]) INCLUDE ([nights], [rooms], [reservationRevenue], [currency])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_arrivalDate_optIn] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_arrivalDate_optIn] ON [dbo].[TransactionDetail] ([arrivalDate], [optIn])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_DataSourceID_IC_Others] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_DataSourceID_IC_Others] ON [dbo].[TransactionDetail] ([confirmationNumber], [DataSourceID]) INCLUDE ([nights], [averageDailyRate], [rooms], [reservationRevenue], [currency], [LoyaltyNumberTagged], [IsPrimaryGuest], [departureDate], [bookingLeadTime], [arrivalDOW], [departureDOW], [optIn], [LoyaltyNumberValidated], [totalPackageRevenue], [totalGuestCount], [adultCount], [childrenCount], [commisionPercent], [arrivalDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetail] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetail] ON [dbo].[TransactionDetail] ([currency])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_DataSourceID_IC_sourceKey] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_DataSourceID_IC_sourceKey] ON [dbo].[TransactionDetail] ([DataSourceID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_synXisTransExID_IC_sourceKey] on [dbo].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_synXisTransExID_IC_sourceKey] ON [dbo].[TransactionDetail] ([synXisTransExID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionDetailID_IC_Others] on [dbo].[TransactionDetail]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionDetailID_IC_Others] ON [dbo].[TransactionDetail] ([TransactionDetailID]) INCLUDE ([arrivalDate], [billingDescription], [commisionPercent], [currency], [departureDate], [LoyaltyNumberTagged], [LoyaltyNumberValidated], [nights], [optIn], [reservationRevenue], [rooms])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_POPULATE_WAREHOUSE] on [dbo].[TransactionDetail]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_POPULATE_WAREHOUSE] ON [dbo].[TransactionDetail] ([TransactionDetailID], [currency], [arrivalDate], [departureDate], [optIn], [LoyaltyNumberValidated], [LoyaltyNumberTagged])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationDate_IC_status] on [dbo].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationDate_IC_status] ON [dbo].[TransactionStatus] ([confirmationDate]) INCLUDE ([status])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationDate_status] on [dbo].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationDate_status] ON [dbo].[TransactionStatus] ([confirmationDate], [status])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_DataSourceID_IC_Others] on [dbo].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_DataSourceID_IC_Others] ON [dbo].[TransactionStatus] ([confirmationNumber], [DataSourceID]) INCLUDE ([status], [confirmationDate], [cancellationNumber], [cancellationDate], [creditCardType], [ActionTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_DataSourceID_IC_sourceKey] on [dbo].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_DataSourceID_IC_sourceKey] ON [dbo].[TransactionStatus] ([DataSourceID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionStatusID_IC_status_confirmationDate_cancellationDate_creditCardType] on [dbo].[TransactionStatus]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionStatusID_IC_status_confirmationDate_cancellationDate_creditCardType] ON [dbo].[TransactionStatus] ([TransactionStatusID]) INCLUDE ([status], [confirmationDate], [cancellationDate], [creditCardType])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionStatusID_IC_Others] on [dbo].[TransactionStatus]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionStatusID_IC_Others] ON [dbo].[TransactionStatus] ([TransactionStatusID]) INCLUDE ([status], [confirmationDate], [cancellationDate], [creditCardType], [cancellationNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ChainID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_ChainID] ON [dbo].[Transactions] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_IC_FK_Cols] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_IC_FK_Cols] ON [dbo].[Transactions] ([confirmationNumber]) INCLUDE ([ChainID], [CorporateCodeID], [CRS_BookingSourceID], [DataSourceID], [Guest_CompanyNameID], [GuestID], [HotelID], [IATANumberID], [LoyaltyNumberID], [LoyaltyProgramID], [PH_BookingSourceID], [PromoCodeID], [RateCategoryID], [RateCodeID], [RoomTypeID], [TransactionDetailID], [TransactionStatusID], [TravelAgentID], [VoiceAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_confirmationNumber_IC_Others] on [dbo].[Transactions]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_confirmationNumber_IC_Others] ON [dbo].[Transactions] ([confirmationNumber]) INCLUDE ([CRS_BookingSourceID], [DataSourceID], [IATANumberID], [PH_BookingSourceID], [RateCategoryID], [RateCodeID], [sourceKey], [TransactionDetailID], [TransactionStatusID], [TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_IC_Others] ON [dbo].[Transactions] ([confirmationNumber]) INCLUDE ([DataSourceID], [HotelID], [RateCodeID], [sourceKey], [TransactionDetailID], [TransactionStatusID], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey] ON [dbo].[Transactions] ([confirmationNumber], [transactionTimeStamp]) INCLUDE ([sourceKey], [TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey_CSV] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_transactionTimeStamp_IC_TransactionID_sourceKey_CSV] ON [dbo].[Transactions] ([confirmationNumber], [transactionTimeStamp]) INCLUDE ([sourceKey], [TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CorporateCodeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_CorporateCodeID] ON [dbo].[Transactions] ([CorporateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CRS_BookingSourceID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_CRS_BookingSourceID] ON [dbo].[Transactions] ([CRS_BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_CRS_BookingSourceID_IC_sourceKey_TransactionID_confirmationNumber_transactionTimeStamp] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_CRS_BookingSourceID_IC_sourceKey_TransactionID_confirmationNumber_transactionTimeStamp] ON [dbo].[Transactions] ([CRS_BookingSourceID]) INCLUDE ([confirmationNumber], [sourceKey], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_DataSourceID_IC_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_DataSourceID_IC_sourceKey] ON [dbo].[Transactions] ([DataSourceID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Guest_CompanyNameID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_Guest_CompanyNameID] ON [dbo].[Transactions] ([Guest_CompanyNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GuestID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_GuestID] ON [dbo].[Transactions] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GuestID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_GuestID_IC_Others] ON [dbo].[Transactions] ([GuestID]) INCLUDE ([confirmationNumber], [CRS_BookingSourceID], [HotelID], [LoyaltyNumberID], [RateCodeID], [TransactionDetailID], [TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_GuestID_LoyaltyNumberID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_GuestID_LoyaltyNumberID] ON [dbo].[Transactions] ([GuestID], [LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HotelID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_HotelID] ON [dbo].[Transactions] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID] ON [dbo].[Transactions] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID_IC_TransactionID_TravelAgentID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID_IC_TransactionID_TravelAgentID] ON [dbo].[Transactions] ([IATANumberID]) INCLUDE ([TransactionID], [TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LoyaltyNumberID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_LoyaltyNumberID] ON [dbo].[Transactions] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_LoyaltyNumberID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_LoyaltyNumberID_IC_Others] ON [dbo].[Transactions] ([LoyaltyNumberID]) INCLUDE ([CRS_BookingSourceID], [GuestID], [HotelID], [PH_BookingSourceID], [RateCodeID], [TransactionDetailID], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PH_BookingSourceID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_PH_BookingSourceID] ON [dbo].[Transactions] ([PH_BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_PromoCodeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_PromoCodeID] ON [dbo].[Transactions] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_confirmationNumber] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_confirmationNumber] ON [dbo].[Transactions] ([QueueID]) INCLUDE ([confirmationNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RateCategoryID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_RateCategoryID] ON [dbo].[Transactions] ([RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RateCodeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_RateCodeID] ON [dbo].[Transactions] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_RoomTypeID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_RoomTypeID] ON [dbo].[Transactions] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetailID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetailID] ON [dbo].[Transactions] ([TransactionDetailID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetailID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetailID_IC_Others] ON [dbo].[Transactions] ([TransactionDetailID]) INCLUDE ([ChainID], [confirmationNumber], [CRS_BookingSourceID], [DataSourceID], [HotelID], [IATANumberID], [LoyaltyNumberID], [LoyaltyProgramID], [RateCategoryID], [RateCodeID], [sourceKey], [timeLoaded], [TransactionStatusID], [transactionTimeStamp], [TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_POPULATE_FACT_RES] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_POPULATE_FACT_RES] ON [dbo].[Transactions] ([TransactionID], [TransactionStatusID], [TransactionDetailID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [CRS_BookingSourceID], [GuestID], [RateCodeID], [PromoCodeID], [TravelAgentID], [HotelID], [ChainID], [PH_BookingSourceID], [VoiceAgentID]) INCLUDE ([confirmationNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionStatusID] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionStatusID] ON [dbo].[Transactions] ([TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionStatusID_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionStatusID_IC_Others] ON [dbo].[Transactions] ([TransactionStatusID]) INCLUDE ([confirmationNumber], [TransactionDetailID], [CRS_BookingSourceID], [GuestID], [RateCodeID], [TravelAgentID], [IATANumberID], [HotelID], [DataSourceID], [itineraryNumber], [transactionTimeStamp], [RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_transactionTimeStamp_IC_Others] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_transactionTimeStamp_IC_Others] ON [dbo].[Transactions] ([transactionTimeStamp]) INCLUDE ([CRS_BookingSourceID], [DataSourceID], [GuestID], [HotelID], [LoyaltyNumberID], [PH_BookingSourceID], [RateCodeID], [sourceKey], [TransactionDetailID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TravelAgentID_IC_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TravelAgentID_IC_sourceKey] ON [dbo].[Transactions] ([TravelAgentID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_VoiceAgentID_IC_sourceKey] on [dbo].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_VoiceAgentID_IC_sourceKey] ON [dbo].[Transactions] ([TravelAgentID]) INCLUDE ([sourceKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IATANumberID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IATANumberID] ON [dbo].[TravelAgent] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[TravelAgent] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[TravelAgent] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TravelAgentID_IATANumberID] on [dbo].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_TravelAgentID_IATANumberID] ON [dbo].[TravelAgent] ([TravelAgentID], [IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[VIP_Level]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[VIP_Level] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[VIP_Level]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[VIP_Level] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[VoiceAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[VoiceAgent] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[VoiceAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[VoiceAgent] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Hotel_hotelID] on [dbo].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_Hotel_hotelID] ON [dbo].[hotel] ([Hotel_hotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[hotel] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[hotel] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_auth_ibeSourceID] on [dbo].[ibeSource]'
GO
CREATE NONCLUSTERED INDEX [IX_auth_ibeSourceID] ON [dbo].[ibeSource] ([auth_ibeSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsOpenHosp_IC_openHospID] on [dbo].[ibeSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsOpenHosp_IC_openHospID] ON [dbo].[ibeSource] ([IsOpenHosp]) INCLUDE ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_IsSynXis_IC_synXisID] on [dbo].[ibeSource]'
GO
CREATE NONCLUSTERED INDEX [IX_IsSynXis_IC_synXisID] ON [dbo].[ibeSource] ([IsSynXis]) INCLUDE ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHospID] on [map].[openHosp_Chain]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHospID] ON [map].[openHosp_Chain] ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHospID] on [map].[openHosp_IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHospID] ON [map].[openHosp_IATANumber] ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHospID] on [map].[openHosp_PromoCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHospID] ON [map].[openHosp_PromoCode] ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHospID] on [map].[openHosp_RateCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHospID] ON [map].[openHosp_RateCode] ([openHospID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_CRS_BookingSource]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_CRS_BookingSource] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_Chain]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_Chain] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_CorporateCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_CorporateCode] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_Guest_CompanyName]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_Guest_CompanyName] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_IATANumber] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_LoyaltyNumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_LoyaltyNumber] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_LoyaltyProgram]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_LoyaltyProgram] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_PromoCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_PromoCode] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_RateCategory]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_RateCategory] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_RateCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_RateCode] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_RoomType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_RoomType] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_VoiceAgent]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_VoiceAgent] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXisID] on [map].[synXis_hotel]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXisID] ON [map].[synXis_hotel] ([synXisID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_ChainID_IC_ChainName] on [openHosp].[Chain]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_ChainID_IC_ChainName] ON [openHosp].[Chain] ([ChainID]) INCLUDE ([ChainName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_City] on [openHosp].[City]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_City] ON [openHosp].[City] ([City_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_CorpInfoCode] on [openHosp].[CorpInfoCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_CorpInfoCode] ON [openHosp].[CorpInfoCode] ([CorpInfoCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_Country] on [openHosp].[Country]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_Country] ON [openHosp].[Country] ([Country_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_EmailAddress] on [openHosp].[Guest_EmailAddress]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_EmailAddress] ON [openHosp].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_IATANumber] on [openHosp].[IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_IATANumber] ON [openHosp].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_CityID_StateID_CountryID_PostalCodeID] on [openHosp].[Location]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_CityID_StateID_CountryID_PostalCodeID] ON [openHosp].[Location] ([CityID], [StateID], [CountryID], [PostalCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionID] on [openHosp].[MostRecentTransaction]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionID] ON [openHosp].[MostRecentTransaction] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_PostalCode] on [openHosp].[PostalCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_PostalCode] ON [openHosp].[PostalCode] ([PostalCode_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_promotionalCode] on [openHosp].[PromoCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_promotionalCode] ON [openHosp].[PromoCode] ([promotionalCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_RateTypeName_RateTypeCode] on [openHosp].[RateTypeCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_RateTypeName_RateTypeCode] ON [openHosp].[RateTypeCode] ([RateTypeName], [RateTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_openHosp_State] on [openHosp].[State]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_openHosp_State] ON [openHosp].[State] ([State_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_KeyColumns] on [openHosp].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_KeyColumns] ON [openHosp].[Transactions] ([QueueID]) INCLUDE ([BookingSourceID], [CorpInfoCodeID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [PromoCodeID], [RateTypeCodeID], [TransactionDetailID], [TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Area_Text] on [sabreAPI].[Area]'
GO
CREATE NONCLUSTERED INDEX [IX_Area_Text] ON [sabreAPI].[Area] ([Area_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_Area] on [sabreAPI].[Area]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_Area] ON [sabreAPI].[Area] ([Area_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_billingDescription] on [sabreAPI].[BillingDescription]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_billingDescription] ON [sabreAPI].[BillingDescription] ([billingDescription])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_CROCode] on [sabreAPI].[CROCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_CROCode] ON [sabreAPI].[CROCode] ([CROCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_ChainID_IC_ChainName] on [sabreAPI].[Chain]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_ChainID_IC_ChainName] ON [sabreAPI].[Chain] ([ChainID]) INCLUDE ([ChainName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_channel] on [sabreAPI].[Channel]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_channel] ON [sabreAPI].[Channel] ([channel])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_City] on [sabreAPI].[City]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_City] ON [sabreAPI].[City] ([City_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_consortiaName] on [sabreAPI].[Consortia]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_consortiaName] ON [sabreAPI].[Consortia] ([consortiaName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_corporationCode] on [sabreAPI].[CorporateCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_corporationCode] ON [sabreAPI].[CorporateCode] ([corporationCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Country_Text] on [sabreAPI].[Country]'
GO
CREATE NONCLUSTERED INDEX [IX_Country_Text] ON [sabreAPI].[Country] ([Country_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_Country] on [sabreAPI].[Country]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_Country] ON [sabreAPI].[Country] ([Country_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Guest_EmailAddressID_hashKey] on [sabreAPI].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_Guest_EmailAddressID_hashKey] ON [sabreAPI].[Guest] ([Guest_EmailAddressID], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hashKey] on [sabreAPI].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_hashKey] ON [sabreAPI].[Guest] ([hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_CompanyName] on [sabreAPI].[Guest_CompanyName]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_CompanyName] ON [sabreAPI].[Guest_CompanyName] ([CompanyName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_EmailAddress] on [sabreAPI].[Guest_EmailAddress]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_EmailAddress] ON [sabreAPI].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_IATANumber_str] on [sabreAPI].[IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_IATANumber_str] ON [sabreAPI].[IATANumber] ([IATANumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_IATANumber] on [sabreAPI].[IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_IATANumber] ON [sabreAPI].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_CityID_StateID_CountryID_PostalCodeID_AreaID_RegionID] on [sabreAPI].[Location]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_CityID_StateID_CountryID_PostalCodeID_AreaID_RegionID] ON [sabreAPI].[Location] ([CityID], [StateID], [CountryID], [PostalCodeID], [AreaID], [RegionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_loyaltyNumber] on [sabreAPI].[LoyaltyNumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_loyaltyNumber] ON [sabreAPI].[LoyaltyNumber] ([loyaltyNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_loyaltyProgram] on [sabreAPI].[LoyaltyProgram]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_loyaltyProgram] ON [sabreAPI].[LoyaltyProgram] ([loyaltyProgram])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionID] on [sabreAPI].[MostRecentTransaction]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionID] ON [sabreAPI].[MostRecentTransaction] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_PMSRateTypeCode] on [sabreAPI].[PMSRateTypeCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_PMSRateTypeCode] ON [sabreAPI].[PMSRateTypeCode] ([PMSRateTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_PostalCode] on [sabreAPI].[PostalCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_PostalCode] ON [sabreAPI].[PostalCode] ([PostalCode_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_promotionalCode] on [sabreAPI].[PromoCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_promotionalCode] ON [sabreAPI].[PromoCode] ([promotionalCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_rateCategoryName_rateCategoryCode] on [sabreAPI].[RateCategory]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_rateCategoryName_rateCategoryCode] ON [sabreAPI].[RateCategory] ([rateCategoryName], [rateCategoryCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_All_Columns] on [sabreAPI].[RateTypeCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_All_Columns] ON [sabreAPI].[RateTypeCode] ([RateTypeName], [RateTypeCode], [GDSRateAccessCode], [GDSBookingCode], [rateTaxInclusive])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Region_Text] on [sabreAPI].[Region]'
GO
CREATE NONCLUSTERED INDEX [IX_Region_Text] ON [sabreAPI].[Region] ([Region_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_Region] on [sabreAPI].[Region]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_Region] ON [sabreAPI].[Region] ([Region_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_roomTypeName_roomTypeCode_PMSRoomTypeCode] on [sabreAPI].[RoomType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_roomTypeName_roomTypeCode_PMSRoomTypeCode] ON [sabreAPI].[RoomType] ([roomTypeName], [roomTypeCode], [PMSRoomTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_secondarySource] on [sabreAPI].[SecondarySource]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_secondarySource] ON [sabreAPI].[SecondarySource] ([secondarySource])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_State] on [sabreAPI].[State]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_State] ON [sabreAPI].[State] ([State_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_subSource_subSourceCode] on [sabreAPI].[SubSource]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_subSource_subSourceCode] ON [sabreAPI].[SubSource] ([subSource], [subSourceCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hashKey] on [sabreAPI].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hashKey] ON [sabreAPI].[TransactionDetail] ([confirmationNumber], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hashKey_IC_All] on [sabreAPI].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hashKey_IC_All] ON [sabreAPI].[TransactionStatus] ([confirmationNumber], [hashKey]) INCLUDE ([ActionTypeID], [cancellationDate], [cancellationNumber], [confirmationDate], [creditCardType], [status], [TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hashKey] on [sabreAPI].[TransactionsExtended]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hashKey] ON [sabreAPI].[TransactionsExtended] ([confirmationNumber], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_TransactionStatusID_IC_transactionTimeStamp_hashKey] on [sabreAPI].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_TransactionStatusID_IC_transactionTimeStamp_hashKey] ON [sabreAPI].[Transactions] ([confirmationNumber], [TransactionStatusID]) INCLUDE ([hashKey], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_KeyColumns] on [sabreAPI].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_KeyColumns] ON [sabreAPI].[Transactions] ([QueueID]) INCLUDE ([BillingDescriptionID], [BookingSourceID], [confirmationNumber], [ConsortiaID], [CorporateCodeID], [Guest_CompanyNameID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [LoyaltyNumberID], [LoyaltyProgramID], [PMSRateTypeCodeID], [PromoCodeID], [RateCategoryID], [RateTypeCodeID], [RoomTypeID], [TransactionDetailID], [TransactionsExtendedID], [TransactionStatusID], [TravelAgentID], [UserNameID], [VIP_LevelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetailID_TransactionsExtendedID_IC_Others] on [sabreAPI].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetailID_TransactionsExtendedID_IC_Others] ON [sabreAPI].[Transactions] ([TransactionDetailID], [TransactionsExtendedID]) INCLUDE ([BookingSourceID], [channelConnectConfirmationNumber], [confirmationNumber], [CorporateCodeID], [Guest_CompanyNameID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [itineraryNumber], [LoyaltyNumberID], [PromoCodeID], [QueueID], [RateCategoryID], [RateTypeCodeID], [RoomTypeID], [timeLoaded], [TransactionStatusID], [transactionTimeStamp], [TravelAgentID], [UserNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionID_IC_BillingDescriptionID] on [sabreAPI].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionID_IC_BillingDescriptionID] ON [sabreAPI].[Transactions] ([TransactionID]) INCLUDE ([BillingDescriptionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_POPULATE_DBO] on [sabreAPI].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_POPULATE_DBO] ON [sabreAPI].[Transactions] ([TransactionStatusID], [TransactionsExtendedID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [UserNameID], [BookingSourceID], [LoyaltyNumberID], [GuestID], [RateTypeCodeID], [PromoCodeID], [TravelAgentID], [IATANumberID], [intHotelID], [intChainID], [Guest_CompanyNameID]) INCLUDE ([channelConnectConfirmationNumber], [confirmationNumber], [itineraryNumber], [QueueID], [timeLoaded], [TransactionDetailID], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hashKey] on [sabreAPI].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_hashKey] ON [sabreAPI].[TravelAgent] ([hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_userName] on [sabreAPI].[UserName]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_userName] ON [sabreAPI].[UserName] ([userName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_vipLevel] on [sabreAPI].[VIP_Level]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_vipLevel] ON [sabreAPI].[VIP_Level] ([vipLevel])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HotelCode_HotelID] on [sabreAPI].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_HotelCode_HotelID] ON [sabreAPI].[hotel] ([HotelCode], [HotelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_SabreAPI_xbeTemplateName_xbeShellName] on [sabreAPI].[xbeTemplate]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_SabreAPI_xbeTemplateName_xbeShellName] ON [sabreAPI].[xbeTemplate] ([xbeTemplateName], [xbeShellName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Area_Text] on [synxis].[Area]'
GO
CREATE NONCLUSTERED INDEX [IX_Area_Text] ON [synxis].[Area] ([Area_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_Area] on [synxis].[Area]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_Area] ON [synxis].[Area] ([Area_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_billingDescription] on [synxis].[BillingDescription]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_billingDescription] ON [synxis].[BillingDescription] ([billingDescription])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_ChannelID_SecondarySourceID_SubSourceID_CROCodeID_xbeTemplateID] on [synxis].[BookingSource]'
GO
CREATE NONCLUSTERED INDEX [IX_ChannelID_SecondarySourceID_SubSourceID_CROCodeID_xbeTemplateID] ON [synxis].[BookingSource] ([ChannelID], [SecondarySourceID], [SubSourceID], [CROCodeID], [xbeTemplateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_CROCode] on [synxis].[CROCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_CROCode] ON [synxis].[CROCode] ([CROCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_ChainID_IC_ChainName] on [synxis].[Chain]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_ChainID_IC_ChainName] ON [synxis].[Chain] ([ChainID]) INCLUDE ([ChainName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_channel] on [synxis].[Channel]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_channel] ON [synxis].[Channel] ([channel])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_City] on [synxis].[City]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_City] ON [synxis].[City] ([City_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_consortiaName] on [synxis].[Consortia]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_consortiaName] ON [synxis].[Consortia] ([consortiaName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_corporationCode] on [synxis].[CorporateCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_corporationCode] ON [synxis].[CorporateCode] ([corporationCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Country_Text] on [synxis].[Country]'
GO
CREATE NONCLUSTERED INDEX [IX_Country_Text] ON [synxis].[Country] ([Country_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_Country] on [synxis].[Country]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_Country] ON [synxis].[Country] ([Country_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Guest_EmailAddressID_hashKey] on [synxis].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_Guest_EmailAddressID_hashKey] ON [synxis].[Guest] ([Guest_EmailAddressID], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hashKey] on [synxis].[Guest]'
GO
CREATE NONCLUSTERED INDEX [IX_hashKey] ON [synxis].[Guest] ([hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_CompanyName] on [synxis].[Guest_CompanyName]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_CompanyName] ON [synxis].[Guest_CompanyName] ([CompanyName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_EmailAddress] on [synxis].[Guest_EmailAddress]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_EmailAddress] ON [synxis].[Guest_EmailAddress] ([emailAddress])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_IATANumber_str] on [synxis].[IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_IATANumber_str] ON [synxis].[IATANumber] ([IATANumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_IATANumber] on [synxis].[IATANumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_IATANumber] ON [synxis].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_CityID_StateID_CountryID_PostalCodeID_AreaID_RegionID] on [synxis].[Location]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_CityID_StateID_CountryID_PostalCodeID_AreaID_RegionID] ON [synxis].[Location] ([CityID], [StateID], [CountryID], [PostalCodeID], [AreaID], [RegionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_loyaltyNumber] on [synxis].[LoyaltyNumber]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_loyaltyNumber] ON [synxis].[LoyaltyNumber] ([loyaltyNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_loyaltyProgram] on [synxis].[LoyaltyProgram]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_loyaltyProgram] ON [synxis].[LoyaltyProgram] ([loyaltyProgram])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionID] on [synxis].[MostRecentTransaction]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionID] ON [synxis].[MostRecentTransaction] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_PMSRateTypeCode] on [synxis].[PMSRateTypeCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_PMSRateTypeCode] ON [synxis].[PMSRateTypeCode] ([PMSRateTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_PostalCode] on [synxis].[PostalCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_PostalCode] ON [synxis].[PostalCode] ([PostalCode_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_promotionalCode] on [synxis].[PromoCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_promotionalCode] ON [synxis].[PromoCode] ([promotionalCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_rateCategoryName_rateCategoryCode] on [synxis].[RateCategory]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_rateCategoryName_rateCategoryCode] ON [synxis].[RateCategory] ([rateCategoryName], [rateCategoryCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_RateTypeName_RateTypeCode] on [synxis].[RateTypeCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_RateTypeName_RateTypeCode] ON [synxis].[RateTypeCode] ([RateTypeName], [RateTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_Region_Text] on [synxis].[Region]'
GO
CREATE NONCLUSTERED INDEX [IX_Region_Text] ON [synxis].[Region] ([Region_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_Region] on [synxis].[Region]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_Region] ON [synxis].[Region] ([Region_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_roomTypeName_roomTypeCode_PMSRoomTypeCode] on [synxis].[RoomType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_roomTypeName_roomTypeCode_PMSRoomTypeCode] ON [synxis].[RoomType] ([roomTypeName], [roomTypeCode], [PMSRoomTypeCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_secondarySource] on [synxis].[SecondarySource]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_secondarySource] ON [synxis].[SecondarySource] ([secondarySource])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_State] on [synxis].[State]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_State] ON [synxis].[State] ([State_Text])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_subSource_subSourceCode] on [synxis].[SubSource]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_subSource_subSourceCode] ON [synxis].[SubSource] ([subSource], [subSourceCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hashKey] on [synxis].[TransactionDetail]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hashKey] ON [synxis].[TransactionDetail] ([confirmationNumber], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hashKey_IC_All] on [synxis].[TransactionStatus]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hashKey_IC_All] ON [synxis].[TransactionStatus] ([confirmationNumber], [hashKey]) INCLUDE ([ActionTypeID], [cancellationDate], [cancellationNumber], [confirmationDate], [creditCardType], [status], [TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_hashKey] on [synxis].[TransactionsExtended]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_hashKey] ON [synxis].[TransactionsExtended] ([confirmationNumber], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_TransactionStatusID_IC_transactionTimeStamp_hashKey] on [synxis].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_TransactionStatusID_IC_transactionTimeStamp_hashKey] ON [synxis].[Transactions] ([confirmationNumber], [TransactionStatusID]) INCLUDE ([transactionTimeStamp], [hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_QueueID_IC_KeyColumns] on [synxis].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_QueueID_IC_KeyColumns] ON [synxis].[Transactions] ([QueueID]) INCLUDE ([BillingDescriptionID], [BookingSourceID], [confirmationNumber], [ConsortiaID], [CorporateCodeID], [Guest_CompanyNameID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [LoyaltyNumberID], [LoyaltyProgramID], [PMSRateTypeCodeID], [PromoCodeID], [RateCategoryID], [RateTypeCodeID], [RoomTypeID], [TransactionDetailID], [TransactionsExtendedID], [TransactionStatusID], [TravelAgentID], [UserNameID], [VIP_LevelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionDetailID_TransactionsExtendedID_IC_Others] on [synxis].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionDetailID_TransactionsExtendedID_IC_Others] ON [synxis].[Transactions] ([TransactionDetailID], [TransactionsExtendedID]) INCLUDE ([BookingSourceID], [channelConnectConfirmationNumber], [confirmationNumber], [CorporateCodeID], [Guest_CompanyNameID], [GuestID], [IATANumberID], [intChainID], [intHotelID], [itineraryNumber], [LoyaltyNumberID], [PromoCodeID], [QueueID], [RateCategoryID], [RateTypeCodeID], [RoomTypeID], [timeLoaded], [TransactionStatusID], [transactionTimeStamp], [TravelAgentID], [UserNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_TransactionID_IC_BillingDescriptionID] on [synxis].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_TransactionID_IC_BillingDescriptionID] ON [synxis].[Transactions] ([TransactionID]) INCLUDE ([BillingDescriptionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_POPULATE_DBO] on [synxis].[Transactions]'
GO
CREATE NONCLUSTERED INDEX [IX_POPULATE_DBO] ON [synxis].[Transactions] ([TransactionStatusID], [TransactionsExtendedID], [CorporateCodeID], [RoomTypeID], [RateCategoryID], [UserNameID], [BookingSourceID], [LoyaltyNumberID], [GuestID], [RateTypeCodeID], [PromoCodeID], [TravelAgentID], [IATANumberID], [intHotelID], [intChainID], [Guest_CompanyNameID]) INCLUDE ([channelConnectConfirmationNumber], [confirmationNumber], [itineraryNumber], [QueueID], [timeLoaded], [TransactionDetailID], [transactionTimeStamp])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hashKey] on [synxis].[TravelAgent]'
GO
CREATE NONCLUSTERED INDEX [IX_hashKey] ON [synxis].[TravelAgent] ([hashKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_userName] on [synxis].[UserName]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_userName] ON [synxis].[UserName] ([userName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_vipLevel] on [synxis].[VIP_Level]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_vipLevel] ON [synxis].[VIP_Level] ([vipLevel])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_HotelCode_HotelID] on [synxis].[hotel]'
GO
CREATE NONCLUSTERED INDEX [IX_HotelCode_HotelID] ON [synxis].[hotel] ([HotelCode], [HotelId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_synXis_xbeTemplateName_xbeShellName] on [synxis].[xbeTemplate]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_synXis_xbeTemplateName_xbeShellName] ON [synxis].[xbeTemplate] ([xbeTemplateName], [xbeShellName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [authority].[CRO_Code]'
GO
ALTER TABLE [authority].[CRO_Code] ADD CONSTRAINT [FK_authority_CRO_Code_CRO_GroupID] FOREIGN KEY ([CRO_Code_GroupID]) REFERENCES [authority].[CRO_Code_Group] ([CRO_Code_GroupID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [authority].[SubSourceCode]'
GO
ALTER TABLE [authority].[SubSourceCode] ADD CONSTRAINT [FK_authority_SubSourceCode_GroupID] FOREIGN KEY ([SubSourceCode_GroupID]) REFERENCES [authority].[SubSourceCode_Group] ([SubSourceCode_GroupID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [authority].[ibeSource]'
GO
ALTER TABLE [authority].[ibeSource] ADD CONSTRAINT [FK_authority_ibeSource_ibeSource_GroupID] FOREIGN KEY ([ibeSource_GroupID]) REFERENCES [authority].[ibeSource_Group] ([ibeSource_GroupID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[CRS_BookingSource]'
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD CONSTRAINT [FK_dbo_BookingSource_CROCodeID] FOREIGN KEY ([CROCodeID]) REFERENCES [dbo].[CROCode] ([CROCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Guest]'
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [FK_dbo_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [dbo].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
ALTER TABLE [dbo].[Guest] ADD CONSTRAINT [FK_dbo_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_Guest]'
GO
ALTER TABLE [map].[pegasus_Guest] ADD CONSTRAINT [FK_map_pegasus_Guest_GuestID] FOREIGN KEY ([GuestID]) REFERENCES [dbo].[Guest] ([GuestID])
GO
ALTER TABLE [map].[pegasus_Guest] ADD CONSTRAINT [FK_map_pegasus_Guest_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[Guest] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_Guest]'
GO
ALTER TABLE [map].[sabreAPI_Guest] ADD CONSTRAINT [FK_map_sabreAPI_Guest_GuestID] FOREIGN KEY ([GuestID]) REFERENCES [dbo].[Guest] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Location]'
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [dbo].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_Region] FOREIGN KEY ([RegionID]) REFERENCES [dbo].[Region] ([RegionID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK_dbo_Location_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[State] ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[PH_BookingSource]'
GO
ALTER TABLE [dbo].[PH_BookingSource] ADD CONSTRAINT [FK_dbo_PH_BookingSource_PH_ChannelID] FOREIGN KEY ([PH_ChannelID]) REFERENCES [dbo].[PH_Channel] ([PH_ChannelID])
GO
ALTER TABLE [dbo].[PH_BookingSource] ADD CONSTRAINT [FK_dbo_PH_BookingSource_PH_SecondaryChannelID] FOREIGN KEY ([PH_SecondaryChannelID]) REFERENCES [dbo].[PH_SecondaryChannel] ([PH_SecondaryChannelID])
GO
ALTER TABLE [dbo].[PH_BookingSource] ADD CONSTRAINT [FK_dbo_PH_BookingSource_PH_SubChannelID] FOREIGN KEY ([PH_SubChannelID]) REFERENCES [dbo].[PH_SubChannel] ([PH_SubChannelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TransactionDetail]'
GO
ALTER TABLE [dbo].[TransactionDetail] ADD CONSTRAINT [FK_dbo_TransactionDetail_DataSourceID] FOREIGN KEY ([DataSourceID]) REFERENCES [authority].[DataSource] ([DataSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TransactionStatus]'
GO
ALTER TABLE [dbo].[TransactionStatus] ADD CONSTRAINT [FK_dbo_TransactionStatus_DataSourceID] FOREIGN KEY ([DataSourceID]) REFERENCES [authority].[DataSource] ([DataSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_DataSourceID] FOREIGN KEY ([DataSourceID]) REFERENCES [authority].[DataSource] ([DataSourceID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [dbo].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_Hotel] FOREIGN KEY ([HotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_IATANumber] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_LastModified_ChannelID] FOREIGN KEY ([LastModified_ChannelID]) REFERENCES [sabreAPI].[Channel] ([ChannelID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_LoyaltyNumber] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [dbo].[LoyaltyProgram] ([LoyaltyProgramID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_PH_BookingSourceID] FOREIGN KEY ([PH_BookingSourceID]) REFERENCES [dbo].[PH_BookingSource] ([PH_BookingSourceID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_RateCategory] FOREIGN KEY ([RateCategoryID]) REFERENCES [dbo].[RateCategory] ([RateCategoryID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_RateCode] FOREIGN KEY ([RateCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_RoomType] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [dbo].[TransactionDetail] ([TransactionDetailID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TransactionStatus] FOREIGN KEY ([TransactionStatusID]) REFERENCES [dbo].[TransactionStatus] ([TransactionStatusID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TravelAgent] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_VoiceAgentID] FOREIGN KEY ([VoiceAgentID]) REFERENCES [dbo].[VoiceAgent] ([VoiceAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TravelAgent]'
GO
ALTER TABLE [dbo].[TravelAgent] ADD CONSTRAINT [FK_dbo_TravelAgent_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [dbo].[TravelAgent] ADD CONSTRAINT [FK_dbo_TravelAgent_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[ibeSource]'
GO
ALTER TABLE [dbo].[ibeSource] ADD CONSTRAINT [FK_dbo_ibeSource_auth_ibeSourceID] FOREIGN KEY ([auth_ibeSourceID]) REFERENCES [authority].[ibeSource] ([ibeSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_Guest]'
GO
ALTER TABLE [map].[openHosp_Guest] ADD CONSTRAINT [FK_map_openHosp_Guest_openHospID] FOREIGN KEY ([openHospID]) REFERENCES [openHosp].[Guest] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_IATANumber]'
GO
ALTER TABLE [map].[openHosp_IATANumber] ADD CONSTRAINT [FK_map_openHosp_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [map].[openHosp_IATANumber] ADD CONSTRAINT [FK_map_openHosp_IATANumber_openHospID] FOREIGN KEY ([openHospID]) REFERENCES [openHosp].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_PromoCode]'
GO
ALTER TABLE [map].[openHosp_PromoCode] ADD CONSTRAINT [FK_map_openHosp_PromoCode_openHospID] FOREIGN KEY ([openHospID]) REFERENCES [openHosp].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [map].[openHosp_PromoCode] ADD CONSTRAINT [FK_map_openHosp_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_RateCode]'
GO
ALTER TABLE [map].[openHosp_RateCode] ADD CONSTRAINT [FK_map_openHosp_RateCode_openHospID] FOREIGN KEY ([openHospID]) REFERENCES [openHosp].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [map].[openHosp_RateCode] ADD CONSTRAINT [FK_map_openHosp_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[openHosp_hotel]'
GO
ALTER TABLE [map].[openHosp_hotel] ADD CONSTRAINT [FK_map_openHosp_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
ALTER TABLE [map].[openHosp_hotel] ADD CONSTRAINT [FK_map_openHosp_hotel_openHospID] FOREIGN KEY ([openHospID]) REFERENCES [openHosp].[hotel] ([intHotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_IATANumber]'
GO
ALTER TABLE [map].[pegasus_IATANumber] ADD CONSTRAINT [FK_map_pegasus_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [map].[pegasus_IATANumber] ADD CONSTRAINT [FK_map_pegasus_IATANumber_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_PromoCode]'
GO
ALTER TABLE [map].[pegasus_PromoCode] ADD CONSTRAINT [FK_map_pegasus_PromoCode_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [map].[pegasus_PromoCode] ADD CONSTRAINT [FK_map_pegasus_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_RateCode]'
GO
ALTER TABLE [map].[pegasus_RateCode] ADD CONSTRAINT [FK_map_pegasus_RateCode_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [map].[pegasus_RateCode] ADD CONSTRAINT [FK_map_pegasus_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_hotel]'
GO
ALTER TABLE [map].[pegasus_hotel] ADD CONSTRAINT [FK_map_pegasus_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
ALTER TABLE [map].[pegasus_hotel] ADD CONSTRAINT [FK_map_pegasus_hotel_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[hotel] ([intHotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_Guest_CompanyName]'
GO
ALTER TABLE [map].[sabreAPI_Guest_CompanyName] ADD CONSTRAINT [FK_map_sabreAPI_Guest_CompanyName_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [dbo].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_IATANumber]'
GO
ALTER TABLE [map].[sabreAPI_IATANumber] ADD CONSTRAINT [FK_map_sabreAPI_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_LoyaltyNumber]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyNumber] ADD CONSTRAINT [FK_map_sabreAPI_LoyaltyNumber_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_LoyaltyProgram]'
GO
ALTER TABLE [map].[sabreAPI_LoyaltyProgram] ADD CONSTRAINT [FK_map_sabreAPI_LoyaltyProgram_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [dbo].[LoyaltyProgram] ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_PromoCode]'
GO
ALTER TABLE [map].[sabreAPI_PromoCode] ADD CONSTRAINT [FK_map_sabreAPI_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RateCategory]'
GO
ALTER TABLE [map].[sabreAPI_RateCategory] ADD CONSTRAINT [FK_map_sabreAPI_RateCategory_RateCategoryID] FOREIGN KEY ([RateCategoryID]) REFERENCES [dbo].[RateCategory] ([RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RateCode]'
GO
ALTER TABLE [map].[sabreAPI_RateCode] ADD CONSTRAINT [FK_map_sabreAPI_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RoomCategory]'
GO
ALTER TABLE [map].[sabreAPI_RoomCategory] ADD CONSTRAINT [FK_map_sabreAPI_RoomCategory_RoomCategoryID] FOREIGN KEY ([RoomCategoryID]) REFERENCES [dbo].[RoomCategory] ([RoomCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_RoomType]'
GO
ALTER TABLE [map].[sabreAPI_RoomType] ADD CONSTRAINT [FK_map_sabreAPI_RoomType_RoomTypeID] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_TravelAgent]'
GO
ALTER TABLE [map].[sabreAPI_TravelAgent] ADD CONSTRAINT [FK_map_sabreAPI_TravelAgent_TravelAgentID] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_VoiceAgent]'
GO
ALTER TABLE [map].[sabreAPI_VoiceAgent] ADD CONSTRAINT [FK_map_sabreAPI_VoiceAgent_UserNameID] FOREIGN KEY ([UserNameID]) REFERENCES [dbo].[VoiceAgent] ([VoiceAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[sabreAPI_hotel]'
GO
ALTER TABLE [map].[sabreAPI_hotel] ADD CONSTRAINT [FK_map_sabreAPI_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_Guest]'
GO
ALTER TABLE [map].[synXis_Guest] ADD CONSTRAINT [FK_map_synXis_Guest_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[Guest] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_Guest_CompanyName]'
GO
ALTER TABLE [map].[synXis_Guest_CompanyName] ADD CONSTRAINT [FK_map_synXis_Guest_CompanyName_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [dbo].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
ALTER TABLE [map].[synXis_Guest_CompanyName] ADD CONSTRAINT [FK_map_synXis_Guest_CompanyName_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_IATANumber]'
GO
ALTER TABLE [map].[synXis_IATANumber] ADD CONSTRAINT [FK_map_synXis_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [map].[synXis_IATANumber] ADD CONSTRAINT [FK_map_synXis_IATANumber_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_LoyaltyNumber]'
GO
ALTER TABLE [map].[synXis_LoyaltyNumber] ADD CONSTRAINT [FK_map_synXis_LoyaltyNumber_LoyaltyNumberID] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [dbo].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [map].[synXis_LoyaltyNumber] ADD CONSTRAINT [FK_map_synXis_LoyaltyNumber_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[LoyaltyNumber] ([LoyaltyNumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_LoyaltyProgram]'
GO
ALTER TABLE [map].[synXis_LoyaltyProgram] ADD CONSTRAINT [FK_map_synXis_LoyaltyProgram_LoyaltyProgramID] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [dbo].[LoyaltyProgram] ([LoyaltyProgramID])
GO
ALTER TABLE [map].[synXis_LoyaltyProgram] ADD CONSTRAINT [FK_map_synXis_LoyaltyProgram_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[LoyaltyProgram] ([LoyaltyProgramID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_PromoCode]'
GO
ALTER TABLE [map].[synXis_PromoCode] ADD CONSTRAINT [FK_map_synXis_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [map].[synXis_PromoCode] ADD CONSTRAINT [FK_map_synXis_PromoCode_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_RateCategory]'
GO
ALTER TABLE [map].[synXis_RateCategory] ADD CONSTRAINT [FK_map_synXis_RateCategory_RateCategoryID] FOREIGN KEY ([RateCategoryID]) REFERENCES [dbo].[RateCategory] ([RateCategoryID])
GO
ALTER TABLE [map].[synXis_RateCategory] ADD CONSTRAINT [FK_map_synXis_RateCategory_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[RateCategory] ([RateCategoryID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_RateCode]'
GO
ALTER TABLE [map].[synXis_RateCode] ADD CONSTRAINT [FK_map_synXis_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
ALTER TABLE [map].[synXis_RateCode] ADD CONSTRAINT [FK_map_synXis_RateCode_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[RateTypeCode] ([RateTypeCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_RoomType]'
GO
ALTER TABLE [map].[synXis_RoomType] ADD CONSTRAINT [FK_map_synXis_RoomType_RoomTypeID] FOREIGN KEY ([RoomTypeID]) REFERENCES [dbo].[RoomType] ([RoomTypeID])
GO
ALTER TABLE [map].[synXis_RoomType] ADD CONSTRAINT [FK_map_synXis_RoomType_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[RoomType] ([RoomTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_TravelAgent]'
GO
ALTER TABLE [map].[synXis_TravelAgent] ADD CONSTRAINT [FK_map_synXis_TravelAgent_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[TravelAgent] ([TravelAgentID])
GO
ALTER TABLE [map].[synXis_TravelAgent] ADD CONSTRAINT [FK_map_synXis_TravelAgent_TravelAgentID] FOREIGN KEY ([TravelAgentID]) REFERENCES [dbo].[TravelAgent] ([TravelAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_VoiceAgent]'
GO
ALTER TABLE [map].[synXis_VoiceAgent] ADD CONSTRAINT [FK_map_synXis_VoiceAgent_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[UserName] ([UserNameID])
GO
ALTER TABLE [map].[synXis_VoiceAgent] ADD CONSTRAINT [FK_map_synXis_VoiceAgent_UserNameID] FOREIGN KEY ([UserNameID]) REFERENCES [dbo].[VoiceAgent] ([VoiceAgentID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[synXis_hotel]'
GO
ALTER TABLE [map].[synXis_hotel] ADD CONSTRAINT [FK_map_synXis_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
ALTER TABLE [map].[synXis_hotel] ADD CONSTRAINT [FK_map_synXis_hotel_synXisID] FOREIGN KEY ([synXisID]) REFERENCES [synxis].[hotel] ([intHotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [openHosp].[Guest]'
GO
ALTER TABLE [openHosp].[Guest] ADD CONSTRAINT [FK_openHosp_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [openHosp].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
ALTER TABLE [openHosp].[Guest] ADD CONSTRAINT [FK_openHosp_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [openHosp].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [openHosp].[Location]'
GO
ALTER TABLE [openHosp].[Location] ADD CONSTRAINT [FK_openHosp_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [openHosp].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [openHosp].[Location] ADD CONSTRAINT [FK_openHosp_Location_State] FOREIGN KEY ([StateID]) REFERENCES [openHosp].[State] ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [openHosp].[MostRecentTransaction]'
GO
ALTER TABLE [openHosp].[MostRecentTransaction] ADD CONSTRAINT [FK_openHosp_MostRecentTransaction_TransactionID] FOREIGN KEY ([TransactionID]) REFERENCES [openHosp].[Transactions] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [openHosp].[Transactions]'
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_Guest] FOREIGN KEY ([GuestID]) REFERENCES [openHosp].[Guest] ([GuestID])
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_Hotel] FOREIGN KEY ([intHotelID]) REFERENCES [openHosp].[hotel] ([intHotelID])
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_IATANumber] FOREIGN KEY ([IATANumberID]) REFERENCES [openHosp].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [openHosp].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_RateTypeCode] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [openHosp].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [openHosp].[TransactionDetail] ([TransactionDetailID])
GO
ALTER TABLE [openHosp].[Transactions] ADD CONSTRAINT [FK_openHosp_Transactions_TransactionStatus] FOREIGN KEY ([TransactionStatusID]) REFERENCES [openHosp].[TransactionStatus] ([TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [pegasus].[Guest]'
GO
ALTER TABLE [pegasus].[Guest] ADD CONSTRAINT [FK_pegasus_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [pegasus].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
ALTER TABLE [pegasus].[Guest] ADD CONSTRAINT [FK_pegasus_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [pegasus].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [pegasus].[Location]'
GO
ALTER TABLE [pegasus].[Location] ADD CONSTRAINT [FK_pegasus_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [pegasus].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [pegasus].[Location] ADD CONSTRAINT [FK_pegasus_Location_State] FOREIGN KEY ([StateID]) REFERENCES [pegasus].[State] ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [pegasus].[MostRecentTransaction]'
GO
ALTER TABLE [pegasus].[MostRecentTransaction] ADD CONSTRAINT [FK_pegasus_MostRecentTransaction_TransactionID] FOREIGN KEY ([TransactionID]) REFERENCES [pegasus].[Transactions] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [pegasus].[Transactions]'
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_Guest] FOREIGN KEY ([GuestID]) REFERENCES [pegasus].[Guest] ([GuestID])
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_Hotel] FOREIGN KEY ([intHotelID]) REFERENCES [pegasus].[hotel] ([intHotelID])
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_IATANumber] FOREIGN KEY ([IATANumberID]) REFERENCES [pegasus].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [pegasus].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_RateTypeCode] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [pegasus].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [pegasus].[TransactionDetail] ([TransactionDetailID])
GO
ALTER TABLE [pegasus].[Transactions] ADD CONSTRAINT [FK_pegasus_Transactions_TransactionStatus] FOREIGN KEY ([TransactionStatusID]) REFERENCES [pegasus].[TransactionStatus] ([TransactionStatusID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[BookingSource]'
GO
ALTER TABLE [sabreAPI].[BookingSource] ADD CONSTRAINT [FK_SabreAPI_BookingSource_ChannelID] FOREIGN KEY ([ChannelID]) REFERENCES [sabreAPI].[Channel] ([ChannelID])
GO
ALTER TABLE [sabreAPI].[BookingSource] ADD CONSTRAINT [FK_SabreAPI_BookingSource_CROCodeID] FOREIGN KEY ([CROCodeID]) REFERENCES [sabreAPI].[CROCode] ([CROCodeID])
GO
ALTER TABLE [sabreAPI].[BookingSource] ADD CONSTRAINT [FK_SabreAPI_BookingSource_SecondarySourceID] FOREIGN KEY ([SecondarySourceID]) REFERENCES [sabreAPI].[SecondarySource] ([SecondarySourceID])
GO
ALTER TABLE [sabreAPI].[BookingSource] ADD CONSTRAINT [FK_SabreAPI_BookingSource_SubSourceID] FOREIGN KEY ([SubSourceID]) REFERENCES [sabreAPI].[SubSource] ([SubSourceID])
GO
ALTER TABLE [sabreAPI].[BookingSource] ADD CONSTRAINT [FK_SabreAPI_BookingSource_xbeTemplateID] FOREIGN KEY ([xbeTemplateID]) REFERENCES [sabreAPI].[xbeTemplate] ([xbeTemplateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[Exclude_ConfNum]'
GO
ALTER TABLE [sabreAPI].[Exclude_ConfNum] ADD CONSTRAINT [FK_Exclude_ConfNum_ReasonID] FOREIGN KEY ([Exclude_ConfNum_ReasonID]) REFERENCES [sabreAPI].[Exclude_ConfNum_Reason] ([Exclude_ConfNum_ReasonID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[Guest]'
GO
ALTER TABLE [sabreAPI].[Guest] ADD CONSTRAINT [FK_SabreAPI_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [sabreAPI].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
ALTER TABLE [sabreAPI].[Guest] ADD CONSTRAINT [FK_SabreAPI_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [sabreAPI].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[LeastRecentTransaction]'
GO
ALTER TABLE [sabreAPI].[LeastRecentTransaction] ADD CONSTRAINT [FK_SabreAPI_LeastRecentTransaction_TransactionID] FOREIGN KEY ([TransactionID]) REFERENCES [sabreAPI].[Transactions] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[Location]'
GO
ALTER TABLE [sabreAPI].[Location] ADD CONSTRAINT [FK_SabreAPI_Location_Area] FOREIGN KEY ([AreaID]) REFERENCES [sabreAPI].[Area] ([AreaID])
GO
ALTER TABLE [sabreAPI].[Location] ADD CONSTRAINT [FK_SabreAPI_Location_City] FOREIGN KEY ([CityID]) REFERENCES [sabreAPI].[City] ([CityID])
GO
ALTER TABLE [sabreAPI].[Location] ADD CONSTRAINT [FK_SabreAPI_Location_Country] FOREIGN KEY ([CountryID]) REFERENCES [sabreAPI].[Country] ([CountryID])
GO
ALTER TABLE [sabreAPI].[Location] ADD CONSTRAINT [FK_SabreAPI_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [sabreAPI].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [sabreAPI].[Location] ADD CONSTRAINT [FK_SabreAPI_Location_Region] FOREIGN KEY ([RegionID]) REFERENCES [sabreAPI].[Region] ([RegionID])
GO
ALTER TABLE [sabreAPI].[Location] ADD CONSTRAINT [FK_SabreAPI_Location_State] FOREIGN KEY ([StateID]) REFERENCES [sabreAPI].[State] ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[MostRecentTransaction]'
GO
ALTER TABLE [sabreAPI].[MostRecentTransaction] ADD CONSTRAINT [FK_SabreAPI_MostRecentTransaction_TransactionID] FOREIGN KEY ([TransactionID]) REFERENCES [sabreAPI].[Transactions] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[TransactionStatus]'
GO
ALTER TABLE [sabreAPI].[TransactionStatus] ADD CONSTRAINT [FK_SabreAPI_TransactionStatus_actionTypeOrder] FOREIGN KEY ([ActionTypeID]) REFERENCES [sabreAPI].[ActionType] ([ActionTypeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[Transactions]'
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_BillingDescription] FOREIGN KEY ([BillingDescriptionID]) REFERENCES [sabreAPI].[BillingDescription] ([BillingDescriptionID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_BookingSource] FOREIGN KEY ([BookingSourceID]) REFERENCES [sabreAPI].[BookingSource] ([BookingSourceID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_Chain] FOREIGN KEY ([intChainID]) REFERENCES [sabreAPI].[Chain] ([intChainID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_Consortia] FOREIGN KEY ([ConsortiaID]) REFERENCES [sabreAPI].[Consortia] ([ConsortiaID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_CorporateCode] FOREIGN KEY ([CorporateCodeID]) REFERENCES [sabreAPI].[CorporateCode] ([CorporateCodeID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_Guest] FOREIGN KEY ([GuestID]) REFERENCES [sabreAPI].[Guest] ([GuestID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [sabreAPI].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_Hotel] FOREIGN KEY ([intHotelID]) REFERENCES [sabreAPI].[hotel] ([intHotelID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_IATANumber] FOREIGN KEY ([IATANumberID]) REFERENCES [sabreAPI].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_LastModified_ChannelID] FOREIGN KEY ([LastModified_ChannelID]) REFERENCES [sabreAPI].[Channel] ([ChannelID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_LoyaltyNumber] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [sabreAPI].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_LoyaltyProgram] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [sabreAPI].[LoyaltyProgram] ([LoyaltyProgramID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_PMSRateTypeCodeID] FOREIGN KEY ([PMSRateTypeCodeID]) REFERENCES [sabreAPI].[PMSRateTypeCode] ([PMSRateTypeCodeID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [sabreAPI].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_RateCategory] FOREIGN KEY ([RateCategoryID]) REFERENCES [sabreAPI].[RateCategory] ([RateCategoryID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_RateTypeCode] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [sabreAPI].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_RoomType] FOREIGN KEY ([RoomTypeID]) REFERENCES [sabreAPI].[RoomType] ([RoomTypeID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [sabreAPI].[TransactionDetail] ([TransactionDetailID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_TransactionsExtended] FOREIGN KEY ([TransactionsExtendedID]) REFERENCES [sabreAPI].[TransactionsExtended] ([TransactionsExtendedID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_TransactionStatus] FOREIGN KEY ([TransactionStatusID]) REFERENCES [sabreAPI].[TransactionStatus] ([TransactionStatusID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_TravelAgent] FOREIGN KEY ([TravelAgentID]) REFERENCES [sabreAPI].[TravelAgent] ([TravelAgentID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_UserName] FOREIGN KEY ([UserNameID]) REFERENCES [sabreAPI].[UserName] ([UserNameID])
GO
ALTER TABLE [sabreAPI].[Transactions] ADD CONSTRAINT [FK_SabreAPI_Transactions_VIP_Level] FOREIGN KEY ([VIP_LevelID]) REFERENCES [sabreAPI].[VIP_Level] ([VIP_LevelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [sabreAPI].[TravelAgent]'
GO
ALTER TABLE [sabreAPI].[TravelAgent] ADD CONSTRAINT [FK_SabreAPI_TravelAgent_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [sabreAPI].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [sabreAPI].[TravelAgent] ADD CONSTRAINT [FK_SabreAPI_TravelAgent_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [sabreAPI].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [synxis].[BookingSource]'
GO
ALTER TABLE [synxis].[BookingSource] ADD CONSTRAINT [FK_synXis_BookingSource_ChannelID] FOREIGN KEY ([ChannelID]) REFERENCES [synxis].[Channel] ([ChannelID])
GO
ALTER TABLE [synxis].[BookingSource] ADD CONSTRAINT [FK_synXis_BookingSource_CROCodeID] FOREIGN KEY ([CROCodeID]) REFERENCES [synxis].[CROCode] ([CROCodeID])
GO
ALTER TABLE [synxis].[BookingSource] ADD CONSTRAINT [FK_synXis_BookingSource_SecondarySourceID] FOREIGN KEY ([SecondarySourceID]) REFERENCES [synxis].[SecondarySource] ([SecondarySourceID])
GO
ALTER TABLE [synxis].[BookingSource] ADD CONSTRAINT [FK_synXis_BookingSource_SubSourceID] FOREIGN KEY ([SubSourceID]) REFERENCES [synxis].[SubSource] ([SubSourceID])
GO
ALTER TABLE [synxis].[BookingSource] ADD CONSTRAINT [FK_synXis_BookingSource_xbeTemplateID] FOREIGN KEY ([xbeTemplateID]) REFERENCES [synxis].[xbeTemplate] ([xbeTemplateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [synxis].[Guest]'
GO
ALTER TABLE [synxis].[Guest] ADD CONSTRAINT [FK_synXis_Guest_Guest_EmailAddressID] FOREIGN KEY ([Guest_EmailAddressID]) REFERENCES [synxis].[Guest_EmailAddress] ([Guest_EmailAddressID])
GO
ALTER TABLE [synxis].[Guest] ADD CONSTRAINT [FK_synXis_Guest_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [synxis].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [synxis].[Location]'
GO
ALTER TABLE [synxis].[Location] ADD CONSTRAINT [FK_synxis_Location_PostalCode] FOREIGN KEY ([PostalCodeID]) REFERENCES [synxis].[PostalCode] ([PostalCodeID])
GO
ALTER TABLE [synxis].[Location] ADD CONSTRAINT [FK_synxis_Location_Region] FOREIGN KEY ([RegionID]) REFERENCES [synxis].[Region] ([RegionID])
GO
ALTER TABLE [synxis].[Location] ADD CONSTRAINT [FK_synxis_Location_State] FOREIGN KEY ([StateID]) REFERENCES [synxis].[State] ([StateID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [synxis].[MostRecentTransaction]'
GO
ALTER TABLE [synxis].[MostRecentTransaction] ADD CONSTRAINT [FK_synXis_MostRecentTransaction_TransactionID] FOREIGN KEY ([TransactionID]) REFERENCES [synxis].[Transactions] ([TransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [synxis].[Transactions]'
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_Guest] FOREIGN KEY ([GuestID]) REFERENCES [synxis].[Guest] ([GuestID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synXis_Transactions_Guest_CompanyNameID] FOREIGN KEY ([Guest_CompanyNameID]) REFERENCES [synxis].[Guest_CompanyName] ([Guest_CompanyNameID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_Hotel] FOREIGN KEY ([intHotelID]) REFERENCES [synxis].[hotel] ([intHotelID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_IATANumber] FOREIGN KEY ([IATANumberID]) REFERENCES [synxis].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_LoyaltyNumber] FOREIGN KEY ([LoyaltyNumberID]) REFERENCES [synxis].[LoyaltyNumber] ([LoyaltyNumberID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_LoyaltyProgram] FOREIGN KEY ([LoyaltyProgramID]) REFERENCES [synxis].[LoyaltyProgram] ([LoyaltyProgramID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_PMSRateTypeCodeID] FOREIGN KEY ([PMSRateTypeCodeID]) REFERENCES [synxis].[PMSRateTypeCode] ([PMSRateTypeCodeID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_PromoCode] FOREIGN KEY ([PromoCodeID]) REFERENCES [synxis].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_RateCategory] FOREIGN KEY ([RateCategoryID]) REFERENCES [synxis].[RateCategory] ([RateCategoryID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_RateTypeCode] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [synxis].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_RoomType] FOREIGN KEY ([RoomTypeID]) REFERENCES [synxis].[RoomType] ([RoomTypeID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [synxis].[TransactionDetail] ([TransactionDetailID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_TransactionsExtended] FOREIGN KEY ([TransactionsExtendedID]) REFERENCES [synxis].[TransactionsExtended] ([TransactionsExtendedID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_TransactionStatus] FOREIGN KEY ([TransactionStatusID]) REFERENCES [synxis].[TransactionStatus] ([TransactionStatusID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_TravelAgent] FOREIGN KEY ([TravelAgentID]) REFERENCES [synxis].[TravelAgent] ([TravelAgentID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_UserName] FOREIGN KEY ([UserNameID]) REFERENCES [synxis].[UserName] ([UserNameID])
GO
ALTER TABLE [synxis].[Transactions] ADD CONSTRAINT [FK_synxis_Transactions_VIP_Level] FOREIGN KEY ([VIP_LevelID]) REFERENCES [synxis].[VIP_Level] ([VIP_LevelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [synxis].[TravelAgent]'
GO
ALTER TABLE [synxis].[TravelAgent] ADD CONSTRAINT [FK_synXis_TravelAgent_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [synxis].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [synxis].[TravelAgent] ADD CONSTRAINT [FK_synXis_TravelAgent_LocationID] FOREIGN KEY ([LocationID]) REFERENCES [synxis].[Location] ([LocationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [authority].[Process_PH_BookingSource_Rules]'
GO
ALTER TABLE [authority].[Process_PH_BookingSource_Rules] ADD CONSTRAINT [DF_EventDate] DEFAULT (getdate()) FOR [EventDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [authority].[Process_PH_BookingSource_Rules] ADD CONSTRAINT [DF_Processed] DEFAULT ((0)) FOR [Processed]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[ActionType]'
GO
ALTER TABLE [dbo].[ActionType] ADD CONSTRAINT [DF_actionTypeOrder] DEFAULT ((0)) FOR [actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[AgedOut_ConfirmationNumber]'
GO
ALTER TABLE [dbo].[AgedOut_ConfirmationNumber] ADD CONSTRAINT [DF__AgedOut_C__AgedO__21A28D72] DEFAULT (getdate()) FOR [AgedOutDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [openHosp].[ActionType]'
GO
ALTER TABLE [openHosp].[ActionType] ADD CONSTRAINT [DF_actionTypeOrder] DEFAULT ((0)) FOR [actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [operations].[LOG_Populate_Hotels]'
GO
ALTER TABLE [operations].[LOG_Populate_Hotels] ADD CONSTRAINT [df_RunTime] DEFAULT (getdate()) FOR [RunTime]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [operations].[Truncate_Log]'
GO
ALTER TABLE [operations].[Truncate_Log] ADD CONSTRAINT [df_CreateDate] DEFAULT (getdate()) FOR [CreateDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [pegasus].[ActionType]'
GO
ALTER TABLE [pegasus].[ActionType] ADD CONSTRAINT [DF_actionTypeOrder] DEFAULT ((0)) FOR [actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [sabreAPI].[ActionType]'
GO
ALTER TABLE [sabreAPI].[ActionType] ADD CONSTRAINT [DF_actionTypeOrder] DEFAULT ((0)) FOR [actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [sabreAPI].[RateTypeCode]'
GO
ALTER TABLE [sabreAPI].[RateTypeCode] ADD CONSTRAINT [DF_rateTaxInclusive] DEFAULT ((0)) FOR [rateTaxInclusive]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [synxis].[ActionType]'
GO
ALTER TABLE [synxis].[ActionType] ADD CONSTRAINT [DF_actionTypeOrder] DEFAULT ((0)) FOR [actionTypeOrder]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
