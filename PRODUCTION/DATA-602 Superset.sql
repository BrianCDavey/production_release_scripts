USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/11/2019 3:33:41 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [Reporting].[BillyControlTotals]'
GO

-- =============================================
-- Author:		Kris Scott
-- Create date: 07/10/2014
-- Description:	
-- =============================================
ALTER PROCEDURE [Reporting].[BillyControlTotals] 
	-- Add the parameters for the stored procedure here
	@year int = 0, 
	@month int = 0
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @startDate DATE
	DECLARE @endDate DATE

	SET @startDate = CAST('01/01/' + CAST(@Year - 2 AS CHAR(4)) AS DATE);
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,CAST(CAST(@Month AS CHAR(2)) + '/01/' + CAST(@Year AS CHAR(4)) AS DATE)));


SELECT
			YEAR(mrt.arrivalDate) as arrivalYear,
			MONTH(mrt.arrivalDate) as arrivalMonth,
			MRT.channel,
			CD.hotelCode,
			COUNT(DISTINCT MRT.confirmationNumber) AS bookings
	,		COUNT(DISTINCT CD.confirmationNumber) AS bookingsWithCharges

	FROM Reservations.dbo.mostrecenttransactions MRT 

	LEFT JOIN Superset.dbo.calculatedDebits CD 
		ON MRT.confirmationNumber = CD.confirmationNumber 

	WHERE MRT.arrivalDate BETWEEN @startDate AND @endDate
		  AND
		  MRT.status <> 'Cancelled'
		  AND
		  MRT.channel <> 'PMS Rez Synch'
		  AND
		  MRT.CROCode <> 'HTL_Carlton'
		  AND
		  MRT.subSourceCode <> 'CCX'
		  AND
		  MRT.secondarySource <> 'Travelweb'
		  AND
		  MRT.rateTypeCode <> 'CMPMKT'
	
	GROUP BY YEAR(mrt.arrivalDate),
			MONTH(mrt.arrivalDate),
			Channel,
			CD.hotelCode

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
