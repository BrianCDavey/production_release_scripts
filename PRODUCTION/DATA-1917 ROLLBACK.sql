USE ETL
GO

/*
Run this script on:

        (local)\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 11/11/2021 4:46:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoRegistrant_FinalImport_Core_Swoogo]'
GO







-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-07-29
-- Description:	This procedure is a part of Swoogo import process loading data from ETL db to Core DB
-- Prototype: EXEC [dbo].[SwoogoRegistrant_FinalImport_Core_Swoogo] 3
-- History: 2020-08-13 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoRegistrant_FinalImport_Core_Swoogo]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--DECLARE @QueueID int = 20

	DELETE FROM Core.Swoogo.weboomAccountingRegistrations

	--max event information
	DECLARE @EventQueueID int
	SELECT @EventQueueID = MAX(QueueID) FROM dbo.Queue WHERE Application = 'SwoogoEventDetail'

	IF OBJECT_ID('tempdb..#OldInvoiceDueDate') IS NOT NULL
	DROP TABLE #OldInvoiceDueDate;

	CREATE TABLE #OldInvoiceDueDate
	(
		financeEventCode nvarchar(100) NOT NULL,
		InvoiceDueDate nvarchar(255) NULL

		PRIMARY KEY CLUSTERED(financeEventCode)
	)

	INSERT INTO #OldInvoiceDueDate(financeEventCode,InvoiceDueDate)
	SELECT DISTINCT JSON_VALUE(EventDetailJSON,'$.c_14144') AS financeEventCode
	, MAX(JSON_VALUE(EventDetailJSON,'$.c_17292')) AS invoiceDueDate
	FROM dbo.Import_SwoogoEventDetail WHERE QueueID = 37487 --hard code this batch before invoice due date column is removed
	AND JSON_VALUE(EventDetailJSON,'$.id') NOT IN (27822, 27823)
		AND JSON_VALUE(EventDetailJSON,'$.name') NOT LIKE '%template%'
	 GROUP BY  JSON_VALUE(EventDetailJSON,'$.c_14144')


	INSERT INTO Core.Swoogo.weboomAccountingRegistrations
	([QueueID], [accountCode], [billingAmount], [billingAmountCurrency], [invoiceDueDate], [serviceDate], [financeEventCode], [marketingInclusionFlag], [marketingInclusionExplanation], [registrationStatus], [confirmationNumber], [attendingGuestFullName], [eventType], [eventDate], [eventLocation], [timeLoaded], [isBad], [errorDescription],[isDeleted],[registrantType])
		SELECT DISTINCT
		@QueueID AS QueueID
		,JSON_VALUE(sd.RegistrantDetailJSON,'$.c_633673') AS accountCode
		,JSON_VALUE(sd.RegistrantDetailJSON,'$.individual_gross') AS billingAmount
		,ISNULL(NULLIF(JSON_VALUE(se.EventDetailJSON,'$.currency'),''),'USD') AS billingAmountCurrency
		,COALESCE(CASE WHEN ISDATE(JSON_VALUE(se.EventDetailJSON,'$.c_40308')) = 0 THEN NULL ELSE JSON_VALUE(se.EventDetailJSON,'$.c_40308') END, war.invoiceDueDate, old.InvoiceDueDate,CAST(getdate() AS DATE)) AS invoiceDueDate
		,JSON_VALUE(se.EventDetailJSON,'$.start_date') AS serviceDate
		,JSON_VALUE(se.EventDetailJSON,'$.c_14144') AS financeEventCode
		,CASE WHEN JSON_VALUE(sd.RegistrantDetailJSON,'$.c_669175') = '' THEN 0 ELSE 1 END AS marketingInclusionFlag
		,JSON_VALUE(sd.RegistrantDetailJSON,'$.c_669175') AS marketingInclusionExplanation
		,CASE 
			WHEN JSON_VALUE(sd.RegistrantDetailJSON,'$.c_630198.value') IN ('Hotel - Approved','Hotel (2nd Attendee) - Approved','Hotel (3rd Attendee) - Approved') 
				AND JSON_VALUE(se.EventDetailJSON,'$.type_id.value') <> 'Complimentary Event'
				THEN 'Invoiced'
			ELSE JSON_VALUE(sd.RegistrantDetailJSON,'$.registration_status') 
		END
		AS registrationStatus
		,JSON_VALUE(sd.RegistrantDetailJSON,'$.id') AS confirmationNumber
		,JSON_VALUE(sd.RegistrantDetailJSON,'$.first_name') + ' ' + JSON_VALUE(sd.RegistrantDetailJSON,'$.last_name') AS attendingGuestFullName
		,sm.Webboom AS eventType
		,JSON_VALUE(se.EventDetailJSON,'$.start_date') AS eventDate
		,CAST(JSON_VALUE(se.EventDetailJSON,'$.name') AS nvarchar(80)) AS eventLocation
		,JSON_VALUE(sd.RegistrantDetailJSON,'$.created_at') AS timeLoaded
		, NULL AS isBad
		, NULL AS errorDescription
		, 0 AS [isDeleted]
		, JSON_VALUE(sd.RegistrantDetailJSON,'$.reg_type_id.value') AS registrantType
		FROM dbo.Import_SwoogoRegistrantDetail sd
		INNER JOIN dbo.Import_SwoogoEventDetail se
		ON JSON_VALUE(sd.RegistrantDetailJSON,'$.event_id') = JSON_VALUE(se.EventDetailJSON,'$.id') AND se.QueueID = @EventQueueID
		INNER JOIN [core].[dbo].[WebboomSwoogoEventTypeMapping] sm
		ON JSON_VALUE(se.EventDetailJSON,'$.type_id.value') =sm.swoogo
		LEFT JOIN core.dbo.weboomAccountingRegistrations war ON war.confirmationNumber = JSON_VALUE(sd.RegistrantDetailJSON,'$.id')
		LEFT JOIN #OldInvoiceDueDate old ON old.financeEventCode = JSON_VALUE(se.EventDetailJSON,'$.c_14144')
		WHERE sd.QueueID = @QueueID
		AND JSON_VALUE(se.EventDetailJSON,'$.type_id.value') IS NOT NULL
		AND JSON_VALUE(sd.RegistrantDetailJSON,'$.event_id') NOT IN (27822, 27823)
		AND CAST(JSON_VALUE(sd.RegistrantDetailJSON,'$.created_at') AS DATE) >= '2021-01-25'
	

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoRegistrant_RawImport_SwoogoToETL]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-07-29
-- Description:	This procedure is a part of Swoogo import process loading data from Swoogo db to ETL DB
-- Prototype: EXEC [dbo].[SwoogoRegistrant_RawImport_SwoogoToETL] 3
-- History: 2020-08-01 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoRegistrant_RawImport_SwoogoToETL]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @QueueID int

	DECLARE @Err nvarchar(4000)

	DECLARE @count int = 0

	INSERT INTO dbo.Queue (Application, FilePath, QueueStatus, ImportStarted)
	VALUES ('SwoogoRegistrantDetail','Swoogo DB',1,getdate())

	SET @QueueID = SCOPE_IDENTITY()

	SELECT @count = COUNT(*)
	FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.SwoogoRegistrantDetail
	WHERE IsProcessed = 0

	IF(@count<> 0)
	BEGIN
		BEGIN TRY


			INSERT INTO dbo.Import_SwoogoRegistrantDetail (QueueID, RegistrantDetailJSON,SwoogoRegistrantDetailID)
			SELECT @QueueID, JSONMessage,SwoogoRegistrantDetailID
			FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.SwoogoRegistrantDetail
			WHERE IsProcessed = 0

			UPDATE SD
			SET IsProcessed = 1
			FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.SwoogoRegistrantDetail SD
			INNER JOIN dbo.Import_SwoogoRegistrantDetail ISD
			ON SD.SwoogoRegistrantDetailID = ISD.SwoogoRegistrantDetailID
			WHERE ISD.QueueID = @QueueID

			EXEC dbo.SwoogoRegistrant_FinalImport_Core @QueueID

			EXEC [dbo].[ftp_UpdateQueueStatus] @QueueID,2,'SUCCESS',@count
		END TRY
		BEGIN CATCH
			SELECT @Err = ERROR_MESSAGE()

			EXEC dbo.ftp_UpdateQueueStatus @QueueID,3,@Err,0

			RAISERROR(N'WARNING! No Records Inserted Into ETL DB.',16,1)
		END CATCH
	END
	ELSE
	BEGIN
		EXEC dbo.ftp_UpdateQueueStatus @QueueID,3,'Missing records on Swoogo table',0

		RAISERROR(N'WARNING! No Records In Swoogo.',16,1)
	END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoTransaction_FinalImport_Core_Swoogo]'
GO














-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-12-29
-- Description:	This procedure is a part of Swoogo import process loading data from ETL db to Core DB
-- Prototype: EXEC [dbo].[SwoogoTransaction_FinalImport_Core_Swoogo] 3
-- History: 2020-08-01 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoTransaction_FinalImport_Core_Swoogo]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

		INSERT INTO Core.Swoogo.weboomAccountingRegistrations
		([QueueID], [marketingInclusionFlag], [marketingInclusionExplanation], [confirmationNumber])
		SELECT DISTINCT
		@QueueID AS QueueID
		,1 AS [marketingInclusionFlag]
		,'Paid By Credit Card: Order # ' + EventID + '-' + RegistrantID AS [marketingInclusionExplanation]
		,RegistrantID AS [confirmationNumber]
		FROM dbo.Import_SwoogoTransaction
		WHERE [Type] = 'credit_card_payment' AND QueueID = @QueueID
		UNION
		SELECT DISTINCT
		@QueueID AS QueueID
		,1 AS [marketingInclusionFlag]
		,''
		,RegistrantID AS [confirmationNumber]
		FROM dbo.Import_SwoogoTransaction
		WHERE RegistrantID NOT IN (
			SELECT DISTINCT
			registrantID AS [confirmationNumber]
			FROM dbo.Import_SwoogoTransaction
			WHERE [Type] = 'credit_card_payment' AND QueueID = @QueueID
		) AND QueueID = @QueueID


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Swoogotransaction_RawImport_SwoogoToETL]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-07-29
-- Description:	This procedure is a part of Swoogo import process loading data from Swoogo db to ETL DB
-- Prototype: EXEC [dbo].[Swoogotransaction_RawImport_SwoogoToETL] 3
-- History: 2020-08-01 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[Swoogotransaction_RawImport_SwoogoToETL]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @QueueID int

	DECLARE @Err nvarchar(4000)

	DECLARE @count int = 0

	INSERT INTO dbo.Queue (Application, FilePath, QueueStatus, ImportStarted)
	VALUES ('Swoogotransaction','Swoogo DB',1,getdate())

	SET @QueueID = SCOPE_IDENTITY()

	SELECT @count = COUNT(*)
	FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.Swoogotransaction
	WHERE IsProcessed = 0

	IF(@count<> 0)
	BEGIN
		BEGIN TRY


			INSERT INTO dbo.Import_Swoogotransaction (QueueID, EventID, RegistrantID, [Type],SwoogotransactionID)
			SELECT @QueueID, EventID, RegistrantID, [Type],SwoogotransactionID
			FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.Swoogotransaction
			WHERE IsProcessed = 0

			UPDATE SD
			SET IsProcessed = 1
			FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.Swoogotransaction SD
			INNER JOIN dbo.Import_Swoogotransaction ISD
			ON SD.SwoogotransactionID = ISD.SwoogotransactionID
			WHERE ISD.QueueID = @QueueID

			EXEC dbo.Swoogotransaction_FinalImport_Core @QueueID

			EXEC [dbo].[ftp_UpdateQueueStatus] @QueueID,2,'SUCCESS',@count
		END TRY
		BEGIN CATCH
			SELECT @Err = ERROR_MESSAGE()

			EXEC dbo.ftp_UpdateQueueStatus @QueueID,3,@Err,0

			RAISERROR(N'WARNING! No Records Inserted Into ETL DB.',16,1)
		END CATCH
	END
	ELSE
	BEGIN
		EXEC dbo.ftp_UpdateQueueStatus @QueueID,3,'Missing records on Swoogo table',0

		RAISERROR(N'WARNING! No Records In Swoogo.',16,1)
	END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoEvent_FinalImport_Core_Swoogo]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-12-28
-- Description:	This procedure is a part of Swoogo import process loading data from ETL db to Core DB
-- Prototype: EXEC [dbo].[SwoogoEvent_FinalImport_Core_Swoogo] 3
-- History: 2020-08-01 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoEvent_FinalImport_Core_Swoogo]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DELETE FROM Core.Swoogo.weboomAccountingEvents

	INSERT INTO Core.Swoogo.weboomAccountingEvents 
	([QueueID], [financeType], [financeEventCode], [eventType], [eventDate], [eventLocation], [timeLoaded], [eventId], [isBad], [errorDescription],[isDeleted])
		SELECT DISTINCT
		@QueueID AS QueueID
		, sf.Webboom AS financeType
		, JSON_VALUE(sd.EventDetailJSON,'$.c_14144') AS financeEventCode
		, se.Webboom AS eventType
		, JSON_VALUE(sd.EventDetailJSON,'$.start_date') AS eventDate
		, CAST(JSON_VALUE(sd.EventDetailJSON,'$.name') AS nvarchar(80)) AS eventLocation
		, JSON_VALUE(sd.EventDetailJSON,'$.created_at') AS timeLoaded
		, JSON_VALUE(sd.EventDetailJSON,'$.id') AS eventId
		, NULL AS isBad
		, NULL AS errorDescription
		, 0 AS [isDeleted]
		FROM dbo.Import_SwoogoEventDetail sd
		LEFT JOIN [core].[dbo].[WebboomSwoogoFinanceTypeMapping] sf
		ON JSON_VALUE(sd.EventDetailJSON,'$.c_14119.value') =sf.swoogo
		LEFT JOIN [core].[dbo].[WebboomSwoogoEventTypeMapping] se
		ON JSON_VALUE(sd.EventDetailJSON,'$.type_id.value') =se.swoogo
		WHERE QueueID = @QueueID
		AND se.Webboom IS NOT NULL
		AND JSON_VALUE(sd.EventDetailJSON,'$.id') NOT IN (27822, 27823)
		AND JSON_VALUE(EventDetailJSON,'$.name') NOT LIKE 'template%'

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoEvent_RawImport_SwoogoToETL]'
GO

-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-07-29
-- Description:	This procedure is a part of Swoogo import process loading data from Swoogo db to ETL DB
-- Prototype: EXEC [dbo].[SwoogoEvent_RawImport_SwoogoToETL] 3
-- History: 2020-08-01 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoEvent_RawImport_SwoogoToETL]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @QueueID int

	DECLARE @Err nvarchar(4000)

	DECLARE @count int = 0

	INSERT INTO dbo.Queue (Application, FilePath, QueueStatus, ImportStarted)
	VALUES ('SwoogoEventDetail','Swoogo DB',1,getdate())

	SET @QueueID = SCOPE_IDENTITY()

	SELECT @count = COUNT(*)
	FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.SwoogoEventDetail
	WHERE IsProcessed = 0

	IF(@count<> 0)
	BEGIN
		BEGIN TRY


			INSERT INTO dbo.Import_SwoogoEventDetail (QueueID, EventDetailJSON,SwoogoEventDetailID)
			SELECT @QueueID, JSONMessage,SwoogoEventDetailID
			FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.SwoogoEventDetail
			WHERE IsProcessed = 0

			UPDATE SD
			SET IsProcessed = 1
			FROM [CHI-WZ-PR-01\DEPOT].Swoogo.dbo.SwoogoEventDetail SD
			INNER JOIN dbo.Import_SwoogoEventDetail ISD
			ON SD.SwoogoEventDetailID = ISD.SwoogoEventDetailID
			WHERE ISD.QueueID = @QueueID

			EXEC dbo.SwoogoEvent_FinalImport_Core @QueueID

			EXEC [dbo].[ftp_UpdateQueueStatus] @QueueID,2,'SUCCESS',@count
		END TRY
		BEGIN CATCH
			SELECT @Err = ERROR_MESSAGE()

			EXEC dbo.ftp_UpdateQueueStatus @QueueID,3,@Err,0

			RAISERROR(N'WARNING! No Records Inserted Into ETL DB.',16,1)
		END CATCH
	END
	ELSE
	BEGIN
		EXEC dbo.ftp_UpdateQueueStatus @QueueID,3,'Missing records on Swoogo table',0

		RAISERROR(N'WARNING! No Records In Swoogo.',16,1)
	END

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
