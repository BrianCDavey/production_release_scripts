USE Reservations
GO

/*
Run this script on:

        (local)\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        wus2-data-st-vm.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 4/19/2022 3:44:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_DailyEnrollmentUpdateExport]'
GO
ALTER PROCEDURE [dbo].[Epsilon_DailyEnrollmentUpdateExport]
	@QueueID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- THE ISSUE: as of 8/6/20 when a guest makes a booking online and opts in to being an I Prefer member Epsilon has no way
	-- of knowing where the booking came from so it's booking source info is a guess
	-- But Sabre gives us this information so we can figure it out and then send to Epsilon
	-- I believe this is a workaround until Epsilon and Sabre can work out their own integration.
	-- we believe the optin isn't marked as checked in these reservations
	
	-- get loyalty number, enrollment source etc from member profile - we're looking for enrollees that don't have a hotel code
		;WITH cte_LN
		AS
		(
			SELECT mp.MemberProfileID,ln.LoyaltyNumberName,mp.Enrollment_Date, es.Enrollment_SourceName, h.HotelCode
			FROM Loyalty.dbo.MemberProfile mp
				INNER JOIN Loyalty.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = mp.LoyaltyNumberID
				INNER JOIN Loyalty.dbo.Enrollment_Source es on es.Enrollment_SourceID = mp.Enrollment_SourceID
				left Join Hotels.dbo.Hotel h on h.HotelID = mp.Enrollment_HotelID
			WHERE 
			(mp.Enrollment_HotelID is null)
			and mp.Enrollment_Date > GETDATE()-3
			--between '2020-05-28' and '2020-09-13' -- this is when we turned on the new SBE and up to two days ago for historical catchup 
		),
		cte_Res -- get all reservaitons made by these null hotel enrollees on the date of their enrollment or after, number them so we find the first
		AS
		(
			SELECT 
			c.Enrollment_Date
			,c.LoyaltyNumberName as 'Loyalty_Number'
			, c.Enrollment_SourceName
			, bs.PH_Channel as Booking_Source
			, hh.HotelCode as 'Enrollment_Hotel'
			,ROW_NUMBER() OVER(PARTITION BY c.LoyaltyNumberName ORDER BY ts.confirmationDate) AS rowNum
			FROM dbo.Transactions t
				INNER JOIN dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
				INNER JOIN dbo.vw_PH_BookingSource bs ON bs.PH_BookingSourceID = t.PH_BookingSourceID
				INNER JOIN dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
				INNER JOIN dbo.hotel h ON h.HotelID = t.HotelID
				INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
				INNER JOIN cte_LN c ON c.LoyaltyNumberName = ln.loyaltyNumber AND  DATEADD(day,-3,c.Enrollment_Date)  <= ts.confirmationDate

		)
	
	-- Now we select only first reservaiton and only those made by an IBE and assign the booking source and hotel code according to
	-- business logic
	
	INSERT INTO ETL.dbo.EpsilonExportEnrollmentUpdate ([Loyalty_Number], [Enrollment_Source], [StoreCode], [QueueID])
	SELECT 
	Loyalty_Number
	, CASE WHEN 
		Booking_Source = 'IBE - Brand' THEN 'IBECHEKBOXIPREF'
		ELSE 'IBECHEKBOXINDVHTL'
		END as 'Enrollment_Source'
	, CASE WHEN
		Booking_Source ='IBE - Brand' THEN 'PHG123'
		WHEN Booking_Source is null THEN 'PHG123'
		ELSE Enrollment_Hotel
		END as 'StoreCode'
	, @QueueID AS QueueID
	FROM cte_Res 
	WHERE rowNum = 1
	--and Booking_Source  in ('IBE - Brand','IBE - Hotel')

	-- add another query to bring in enrollees who haven't booked
	UNION
	select
	LoyaltyNumberName
	,'IBECHEKBOXIPREF'
	,'PHG123'
	, @QueueID AS QueueID
	from cte_LN ln
	LEFT JOIN Reservations.dbo.MostRecentTransactions mrt on ln.LoyaltyNumberName = mrt.loyaltyNumber
	where mrt.confirmationNumber is null
	

END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
