USE Reservations
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/1/2024 2:55:11 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[IATAReservations]'
GO

CREATE PROCEDURE [rpt].[IATAReservations]
	 @startDate date
	,@endDate date
	,@IATA nvarchar(255)
AS
BEGIN
	SET NOCOUNT ON;


--These CTEs are to help prevent duplicate reservations. 

--- Hotels that part of the BHG brand
	;WITH bhg
	AS
	(
		SELECT h.HotelCode,hc.StartDate,isnull(hc.EndDate, '9999-12-31') as EndDate
		FROM Hotels.dbo.hotel h
			JOIN hotels.dbo.Hotel_Collection hc on hc.HotelID = h.HotelID
			JOIN hotels.dbo.Collection c on hc.CollectionID = c.CollectionID
		WHERE 
		-- Beyond Green Hotels only
		c.Code = 'BGH'
		-- they not only need to be in the collection code but also the collection dates need to be active
	)--Hotels that have two or more brands.
	, duals
	AS
	(
	SELECT h.HotelCode, c.Code
	FROM Hotels.dbo.hotel h
		JOIN hotels.dbo.Hotel_Collection hc on hc.HotelID = h.HotelID
		JOIN hotels.dbo.Collection c on hc.CollectionID = c.CollectionID
		JOIN bhg on h.HotelCode = bhg.HotelCode
	WHERE
		c.Code NOT IN ('BGH', 'IP')
		and GETDATE()-1 between hc.StartDate and isnull(hc.EndDate, '9999-12-31')
	)-- single branded hotels
	, solos
	AS
	(
	SELECT h.HotelCode, c.Code
	FROM Hotels.dbo.hotel h
		JOIN hotels.dbo.Hotel_Collection hc on hc.HotelID = h.HotelID
		JOIN hotels.dbo.Collection c on hc.CollectionID = c.CollectionID
		JOIN bhg on h.HotelCode = bhg.HotelCode
		left JOIN duals on duals.HotelCode = bhg.HotelCode
	WHERE duals.HotelCode is null
	)
	SELECT
		mrt.hotelCode,
		confirmationDate
		,transactiontimestamp as modificationDate
		,confirmationNumber
		,itineraryNumber
		,status as bookingStatus
		,tag.name
		,taitag.travelAgentId as iataNumber
		,CASE channel 
			WHEN 'Booking Engine' THEN 
				CASE 
					WHEN mrt.secondarySource = 'iPrefer APP' THEN 'IBE - PHG'
					WHEN tmp.xbeTemplateName IS NOT NULL THEN 'IBE - PHG'
					ELSE 'IBE - Hotel' 
				END
			WHEN 'Channel Connect' THEN 'OTA'
			WHEN 'GDS' THEN 'GDS'
			WHEN 'Google' THEN 'OTA'
			WHEN 'IDS' THEN 'OTA'
			WHEN 'Mobile Web' THEN
				CASE WHEN tmp.xbeTemplateName IS NOT NULL THEN 'IBE - PHG'
					ELSE 'IBE - Hotel'
				END		
			WHEN 'PMS Rez Synch' THEN 'PMS'
			WHEN 'Voice' THEN
				CASE WHEN ReservationBilling.dbo.CROCodes.croCode IS NULL THEN 'Voice - Hotel Agent'
					ELSE 'Voice - PHG'
				END
		END as ChannelReportLabel
		,CASE	WHEN channel = 'Booking Engine' OR channel = 'Mobile Web' THEN 
					CASE WHEN secondarySource = 'iPrefer APP' THEN 'iPrefer APP'
						ELSE COALESCE (tmp.xbeTemplateName, 'Hotel Booking Engine') 
					END
			WHEN channel = 'PMS Rez Synch' THEN 'PMS Rez Synch' 
			WHEN channel = 'Voice' THEN 
				CASE WHEN (subSourceCode = '' OR subSourceCode IS NULL) THEN 'Voice - No Subchannel' 
						ELSE subSourceCode 
				END 
			WHEN (subSourceCode = '' OR subSourceCode IS NULL) THEN 
				CASE WHEN (secondarySource = '' OR secondarySource IS NULL) THEN channel 
					ELSE secondarySource 
				END 
			WHEN (subSource = '' OR subSource IS NULL) THEN subSourceCode 
			ELSE subSource 
		END AS subSourceReportLabel,
			arrivalDate,
			departureDate,
			mrt.rooms,
			mrt.nights,
			currency,
			reservationRevenue,
			ratetypecode as RatePlan,
			roomTypeName,
			reservationRevenueUSD
	FROM Reservations.dbo.mostrecenttransactions mrt
		INNER JOIN Core.dbo.travelAgentIDs_travelAgentGroups taitag ON mrt.iatanumber = taitag.travelagentid
		INNER JOIN Core.dbo.travelAgentGroups tag ON tag.id = taitag.travelAgentGroupID
		left join solos on solos.HotelCode = mrt.HotelCode
		LEFT OUTER JOIN ReservationBilling.dbo.Templates tmp ON mrt.xbeTemplateName = tmp.xbeTemplateName
		LEFT OUTER JOIN ReservationBilling.dbo.CROCodes ON mrt.CROCode = ReservationBilling.dbo.CROCodes.croCode
														AND CROCodes.croGroupID IN (1,3)
	WHERE
		--IATA to test for meta channel reservations
		taitag.travelAgentId = @IATA
		AND
			departureDate BETWEEN @startDate and @endDate
		AND
		confirmationdate between ISNULL(taitag.startDate,'1900-01-01') AND ISNULL(taitag.endDate,'2099-09-09')
		and solos.HotelCode is null
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
