USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/24/2020 8:44:23 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[runCalculator]'
GO



ALTER PROCEDURE [work].[runCalculator]
	@runType bit = 1, --0=test, 1=production
	@startDate date = NULL,
	@endDate date = NULL,
	@hotelCode nvarchar(20) = NULL,
	@confirmationNumber nvarchar(255) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': start'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	
	-- DELETE MRT ---------------------------------------------------------------
	EXEC [work].[Delete_MRT]

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [work].[Delete_MRT] Finished'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------


	DECLARE @RunID int;
	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunType,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),@runType,0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': work.Run insert'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-2,MIN(sub.billableDate)),
				@endDate = DATEADD(DAY,2,MAX(sub.billableDate)),
				@hotelCode = MAX(sub.phgHotelCode)
		
		FROM (		
			SELECT mrt.phgHotelCode, CONVERT(date,CASE WHEN mrt.arrivalDate >= GETDATE() THEN mrt.confirmationDate ELSE mrt.arrivaldate END) as billableDate
			FROM work.mrtJoined_Reservation mrt
			WHERE mrt.confirmationNumber = @confirmationNumber
				UNION 
			SELECT tdr.phgHotelCode, CASE WHEN tdr.arrivalDate >= GETDATE() THEN tdr.transactionTimeStamp ELSE tdr.arrivalDate END as billableDate
			FROM work.tdrJoined_Reservation tdr
			WHERE tdr.confirmationNumber = @confirmationNumber
			) as sub
	END

	IF(@startDate IS NULL OR @endDate IS NULL)
	BEGIN
		IF @startDate IS NULL
			BEGIN
			IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
			BEGIN
				SET @startDate = DATEADD (mm,-1,GETDATE())
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
			ELSE
			BEGIN
				SET @startDate = GETDATE()
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
		END

		IF @endDate IS NULL OR @endDate < @startDate
		BEGIN
			SET @endDate = DATEADD(YEAR,2,@startDate)
		END
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': set up parameter'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- UPDATE RUN PARAMETERS ----------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 1,
	startDate = @startDate,
	endDate = @endDate
	WHERE runID = @RunID;
	-----------------------------------------------------------------------------
	
	-- POPULATE [MrtForCalculation] ---------------------------------------------
	SET @print = '	@startDate=' + CONVERT(varchar(100),@startDate,120) + ', @endDate=' + CONVERT(varchar(100),@endDate,120) + ', @RunID=' + CONVERT(varchar(20), @RunID) + ', @hotelCode=' + ISNULL(@hotelCode,'') + ', @confirmationNumber=' + ISNULL(@confirmationNumber,'')
	RAISERROR(@print,10,1) WITH NOWAIT

	IF @runType = 0
	BEGIN
		EXEC [work].[Populate_ExchangeRates] @runType, @startDate, @endDate
		EXEC [test].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber
	END
	ELSE
		EXEC [work].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE [MrtForCalculation] complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- POPULATE MrtForCalc_Clauses ----------------------------------------------
	IF @runType = 0
		EXEC test.Populate_MrtForCalc_Clauses @RunID
	ELSE
		EXEC work.Populate_MrtForCalc_Clauses @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE work.Populate_MrtForCalc_Clauses complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- Eliminate excluded clauses------------------------------------------------
	IF @runType = 0
		EXEC test.handleExclusions @RunID
	ELSE
		EXEC work.handleExclusions @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': handleExclusions complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- CALCULATE CHARGES --------------------------------------------------------
	IF @runType = 0
	BEGIN
		-- RUN standard calculation ---------------------------------------------
		EXEC test.Calculate_StandardCharges @RunID

		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Calculate_StandardCharges complete'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT
		-------------------------------------------------------------------------

		-- Calculate thresholds -------------------------------------------------
		EXEC test.Calculate_ThresholdCharges @RunID

		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Calculate_ThresholdCharges complete'
		RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
		RAISERROR(@print,10,1) WITH NOWAIT
		-------------------------------------------------------------------------
	END
	ELSE
	BEGIN
		DELETE c
		FROM dbo.Charges c
		WHERE billableDate BETWEEN @startDate AND @endDate
		AND hotelCode = ISNULL(@hotelCode, hotelCode)
		AND confirmationNumber = ISNULL(@confirmationNumber, confirmationNumber)
		AND c.sopNumber IS NULL
		AND transactionSourceID = 3 --delete iprefer charge for recalculating summary manual point credit
									--This is temparory solution to avoid big change on Calculate_StandardCharges proc. Will fix this bug later.

		EXEC work.Calculate_Charges @RunID
	END
	-----------------------------------------------------------------------------

	-- Subtract Booking fees from Commission fees--------------------------------
	EXEC work.handleCommissions @RunID, @RunType

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': handleCommissions complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	-- move billableDate for old historical arrival --------------------------------
	IF @runType = 1
	BEGIN
		EXEC [work].[Move_Charges_BillableDate] @RunID
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Move_Charges_BillableDate complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	-----------------------------------------------------------------------------

	
	IF @runType = 1
	BEGIN
		EXEC work.LoadBillyErrorTable @RunID
	END

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': LoadBillyErrorTable complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT

	-- SET RUN TO COMPLETED -------------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 2 
	WHERE runID = @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': execution complete'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT

	RETURN @runID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
