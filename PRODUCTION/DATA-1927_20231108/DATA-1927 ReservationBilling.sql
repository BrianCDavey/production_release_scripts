USE [ReservationBilling]
GO


SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping extended properties'
GO
BEGIN TRY
	EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N'BookingChargeCount', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N'BookingChargeCount', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N'CommissionChargeCount', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N'CommissionChargeCount', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N'iPreferChargeCount', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
BEGIN TRY
	EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N'iPreferChargeCount', NULL, NULL
END TRY
BEGIN CATCH
	DECLARE @msg nvarchar(max);
	DECLARE @severity int;
	DECLARE @state int;
	SELECT @msg = ERROR_MESSAGE(), @severity = ERROR_SEVERITY(), @state = ERROR_STATE();
	RAISERROR(@msg, @severity, @state);

	SET NOEXEC ON
END CATCH
GO
PRINT N'Dropping index [IX_clauseID_IC_startDate_endDate] from [dbo].[BillingRules]'
GO
DROP INDEX [IX_clauseID_IC_startDate_endDate] ON [dbo].[BillingRules]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_sopNumber_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_sopNumber_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] from [dbo].[Charges]'
GO
DROP INDEX [IX_arrivalDate_sopNumber_IC_transactionSourceID_transactionKey_confirmationNumber_hotelCode] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_billableDate_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_billableDate_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] from [dbo].[Charges]'
GO
DROP INDEX [IX_billableDate_hotelCode_transactionSourceID_clauseID_IC_roomNights_roomRevenueInHotelCurrency_transactionKey] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_classificationID_confirmationNumber_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_classificationID_confirmationNumber_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_classificationID_sopNumber_transactionSourceID_IC_confirmationNumber_billableDate] from [dbo].[Charges]'
GO
DROP INDEX [IX_classificationID_sopNumber_transactionSourceID_IC_confirmationNumber_billableDate] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_confirmationNumber_IC_classificationID_hotelCode_chargeValueInUSD_sopNumber_loyaltyNumber] from [dbo].[Charges]'
GO
DROP INDEX [IX_confirmationNumber_IC_classificationID_hotelCode_chargeValueInUSD_sopNumber_loyaltyNumber] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_hotelCode_billableDate_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_hotelCode_billableDate_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_runID_classificationID_sopNumber_IC_Others] from [dbo].[Charges]'
GO
DROP INDEX [IX_runID_classificationID_sopNumber_IC_Others] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_sopNumber_billableDate_IC_hotelCode] from [dbo].[Charges]'
GO
DROP INDEX [IX_sopNumber_billableDate_IC_hotelCode] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] from [dbo].[Charges]'
GO
DROP INDEX [IX_sopNumber_hotelCode_confirmationNumber_IC_transactionKey_transactionSourceID] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_transactionSourceID_sopNumber_IC_transactionKey] from [dbo].[Charges]'
GO
DROP INDEX [IX_transactionSourceID_sopNumber_IC_transactionKey] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [IX_transactionSourceID_transactionKey_sopNumber] from [dbo].[Charges]'
GO
DROP INDEX [IX_transactionSourceID_transactionKey_sopNumber] ON [dbo].[Charges]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[iPreferChargeCount]'
GO



ALTER VIEW [dbo].[iPreferChargeCount]
AS
	SELECT transactionSourceID,transactionKey,COUNT(*) AS iPreferChargesCount
	FROM dbo.Charges
	WHERE classificationID = 5
		AND clauseName NOT LIKE '%campaign%'
	GROUP BY transactionSourceID,transactionKey


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CommissionChargeCount]'
GO


ALTER VIEW [dbo].[CommissionChargeCount]
AS
	SELECT transactionSourceID,transactionKey,COUNT(*) AS commissionChargesCount
	FROM dbo.Charges
	WHERE classificationID = 2
	GROUP BY transactionSourceID,transactionKey


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[BookingChargeCount]'
GO

ALTER VIEW [dbo].[BookingChargeCount]
AS
	SELECT transactionSourceID,transactionKey,COUNT(*) AS bookingChargesCount
	FROM dbo.Charges
	WHERE classificationID = 1
	GROUP BY transactionSourceID,transactionKey

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[LoadBillyErrorTable]'
GO
-- =============================================
-- Author:		Kris Scott
-- Create date: 04/29/2016
-- Description:	

-- EXEC [work].[LoadBillyErrorTable] 19646

-- History: 2019-06-03 Ti Yao Swith to Reservations DB version
--			2020-05-12 Ti Yao Hide non billable booking 
--          2020-08-03 Ti Yao Fix Iprefer Non-Stay bug 
--          2021-07-13 Ti Yao Add BWP Error 
-- =============================================
ALTER PROCEDURE [work].[LoadBillyErrorTable]
	@RunID int,
	@maxInvoiceDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--DECLARE @RunID int = 19646
	DECLARE	@startDate date,
		@endDate date,
		@maxCalcDate date,
		@firstDayOfInvoiceMonth date;

		
	-- get current run dates
	SELECT @startDate = startdate,
		@endDate = enddate,
		@maxCalcDate = RunDate
	FROM [work].[Run]
	WHERE runID = @runID

	-- get last invoice date run dates
	SELECT @firstDayOfInvoiceMonth = DATEADD(month,DATEDIFF(month,0,@maxInvoiceDate),0)
	
	-- CREATE & POPULATE #mrtWithoutCharges -------------------------------------------------
	IF OBJECT_ID('tempdb..#mrtWithoutCharges') IS NOT NULL
		DROP TABLE #nonBillable;

	CREATE TABLE #nonBillable
	(
		transactionSourceID int,
		transactionKey int
	)

	INSERT INTO #nonBillable(transactionSourceID,transactionKey)
	SELECT c.transactionSourceID,c.transactionKey
	FROM dbo.Charges as c
	WHERE c.[classificationID] = 4
	-----------------------------------------------------------------------------------------

	-- CREATE TABLE #mrtWithoutCharges ------------------------------------------------------
	IF OBJECT_ID('tempdb..#mrtWithoutCharges') IS NOT NULL
		DROP TABLE #mrtWithoutCharges;

	CREATE TABLE #mrtWithoutCharges
	(
		[hotelCode] nvarchar(50)
		,[hotelName] nvarchar(250)
		,[synxisID] nvarchar(50)
		,[confirmationNumber] nvarchar(max)
		,classificationID int
		,clauseID int
		,[channel] nvarchar(max)
		,[secondarySource] nvarchar(max)
		,[subSourceCode] nvarchar(max)
		,[CROCode] nvarchar(max)
		,[templateAbbreviation] nvarchar(max)
		,[xbeTemplateName] nvarchar(max)
		,[rateCategoryCode] nvarchar(max)
		,[rateTypeCode] nvarchar(max)
		,[arrivalDate] date
		,[OpenHospitalityID] nvarchar(50)
		,currency nvarchar(5)
		,bookingChargesCount int
		,commissionChargesCount int
		,iPreferChargesCount int
		,phHotelCode nvarchar(50)
		,gpHotelCode nvarchar(50)
		,gpCurrency nvarchar(5)
		,resCurrency nvarchar(5)
		,bookwithpointissue bit 
	)
	-----------------------------------------------------------------------------------------

	-- INSERT INTO	#mrtWithoutCharges ------------------------------------------------------
	INSERT INTO	#mrtWithoutCharges
	SELECT DISTINCT
		COALESCE(hrs.code,hro.code,mrt.HotelCode) as [hotelCode]
		,mrt.HotelName as [hotelName]
		,mrt.hotelID as [synxisID]
		,mrt.confirmationNumber as [confirmationNumber]
		,cd.classificationID
		,brule.clauseID
		,mrt.channel as [channel]
		,mrt.secondarySource as [secondarySource]
		,mrt.subSourceCode as [subSourceCode]
		,mrt.CRO_Code as [CROCode]
		,ISNULL(pt.[siteAbbreviation],'HOTEL') as [templateAbbreviation]
		,mrt.[xbeTemplateName]
		,mrt.rateCategoryCode as [rateCategoryCode]
		,mrt.rateTypeCode as [rateTypeCode]
		,mrt.arrivalDate as [arrivalDate]
		,mrt.Hotel_OpenHospID as [OpenHospitalityID]
		,mrt.currency
		,bc.bookingChargesCount
		,cc.commissionChargesCount
		,ipc.iPreferChargesCount
		,COALESCE(hrs.code,hro.code,NULL) as phHotelCode
		,gpc.custnmbr as gpHotelCode
		,gpc.CURNCYID as gpCurrency
		,cur.CURNCYID as resCurrency
		,0 as bookwithpointissue
	FROM Reservations.[billing].[MostRecentTransactions] mrt
		LEFT OUTER JOIN [dbo].[Templates] pt ON mrt.xbeTemplateName = pt.[xbeTemplateName]
		LEFT OUTER JOIN dbo.Charges cd on mrt.TransactionID = cd.transactionKey AND cd.transactionSourceID IN (1,2)
		LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
		LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on mrt.TransactionID = bc.transactionKey AND bc.transactionSourceID IN (1,2)
		LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on mrt.TransactionID = cc.transactionKey AND cc.transactionSourceID IN (1,2)	
		LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON mrt.TransactionID = ipc.transactionKey AND ipc.transactionSourceID IN (1,2)	
		LEFT OUTER JOIN Hotels.dbo.hotelsReporting hrs ON mrt.hotelID = hrs.synxisID
		LEFT OUTER JOIN Hotels.dbo.hotelsReporting hro ON mrt.Hotel_OpenHospID = hro.openHospitalityCode
		LEFT OUTER JOIN IC.dbo.RM00101 gpc ON COALESCE(hro.code,hrs.code) = gpc.custnmbr
		LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON mrt.currency = cur.CURNCYID		AND CAST(mrt.arrivalDate AS DATE) = cur.EXCHDATE
		LEFT OUTER JOIN #nonBillable nb	on mrt.TransactionID = nb.transactionKey AND nb.transactionSourceID IN (1,2)
		LEFT OUTER JOIN dbo.Clauses cl ON COALESCE(hrs.code,hro.code,mrt.HotelCode) = cl.hotelCode AND cl.clauseName LIKE '%Any Booking%'
	WHERE
		(
			cd.hotelCode IS NULL
			OR
			(bookingChargesCount IS NULL OR bookingChargesCount <> 1)
			OR
			commissionChargesCount > 1
			OR
			cd.chargeValueInUSD IS NULL
			OR
			cd.chargeValueInHotelCurrency IS NULL
		)
		AND mrt.arrivalDate BETWEEN @firstDayOfInvoiceMonth AND @endDate
		AND mrt.timeLoaded < DATEADD(day,-2,@maxCalcDate)
		AND mrt.status <> 'Cancelled'
		AND nb.transactionKey IS NULL --only want to see bookings not handled by non-billable charges
		AND cl.clauseID IS NULL
	---------------------------------------------------------------------------------------------
 
	-- INSERT into	#mrtWithoutCharges -----------------------------------------------------------
	INSERT INTO	#mrtWithoutCharges
	SELECT DISTINCT
		tdr.Hotel_Code as [hotelCode]
		,hr.hotelName as [hotelName]
		,'' as [synxisID]
		,tdr.Booking_ID as [confirmationNumber]
		,cd.classificationID
		,brule.clauseID
		,'iPrefer Manual Entry' as [channel]
		,tdr.Transaction_Source as [secondarySource]
		,tdr.Remarks as [subSourceCode]
		,'' as [CROCode]
		,'' as [templateAbbreviation]
		,'' as [xbeTemplateName]
		,'' as [rateCategoryCode]
		,'' as [rateTypeCode]
		,tdr.Reward_Posting_Date as [arrivalDate]
		,'' as [OpenHospitalityID]
		,tdr.Currency_Code
		,bc.bookingChargesCount
		,cc.commissionChargesCount
		,ipc.iPreferChargesCount
		,hr.code as phHotelCode
		,gpc.custnmbr as gpHotelCode
		,gpc.CURNCYID as gpCurrency
		,cur.CURNCYID as resCurrency
		,0 as bookwithpointissue
	FROM Loyalty.dbo.TransactionDetailedReport tdr
		LEFT OUTER JOIN Hotels.dbo.hotelsReporting hr ON tdr.Hotel_Code = hr.code
		LEFT OUTER JOIN dbo.Charges cd on tdr.Transaction_Id = cd.transactionKey AND cd.transactionSourceID IN (3,4) --iprefer manual points + iprefer non stay
		LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
		LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on tdr.Transaction_Id = bc.transactionKey AND bc.transactionSourceID IN (3,4)
		LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on tdr.Transaction_Id = cc.transactionKey AND cc.transactionSourceID IN (3,4)
		LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON tdr.Transaction_Id = ipc.transactionKey AND ipc.transactionSourceID IN (3,4)
		LEFT OUTER JOIN IC.dbo.RM00101 gpc ON hr.code = gpc.custnmbr
		LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON tdr.Currency_Code = cur.CURNCYID AND CAST(tdr.Reward_Posting_Date AS DATE) = cur.EXCHDATE
		LEFT OUTER JOIN #nonBillable nb	on tdr.Transaction_Id = nb.transactionKey AND nb.transactionSourceID IN (3,4) --iprefer manual points
		LEFT JOIN Hotels.dbo.TestHotel th ON th.hotelcode = tdr.Hotel_Code
		LEFT OUTER JOIN dbo.Clauses cl ON tdr.Hotel_Code = cl.hotelCode AND cl.clauseName LIKE '%Any Booking%'
		LEFT OUTER JOIN dbo.Charges cd2 on tdr.Booking_ID = cd2.confirmationNumber AND cd2.transactionSourceID IN (3,4) --iprefer manual points + iprefer non stay --remove duplicate confirmationnumber
	WHERE
		(
			cd.hotelCode IS NULL
			OR
			(ipc.iPreferChargesCount IS NULL OR ipc.iPreferChargesCount  <> 1)
		)
		AND tdr.Reward_Posting_Date BETWEEN @startDate AND @endDate
		AND tdr.Reward_Posting_Date < DATEADD(day,-2,@maxCalcDate)
		AND tdr.Reservation_Revenue <> 0
		AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP','Admin Portal')
		AND tdr.Hotel_Code NOT IN ('PHG123','PHG','PHGTEST')
		AND tdr.Hotel_Code IS NOT NULL
		AND nb.transactionKey IS NULL --only want to see bookings not handled by non-billable charges;
		AND tdr.Booking_ID <> ''
		AND th.hotelcode IS NULL
		AND cl.clauseID IS NULL
		AND cd2.chargeID IS NULL --remove duplicate confirmationnumber
	---------------------------------------------------------------------------------------------

	-- INSERT into	#mrtWithoutCharges ----------------------------------------------------------
	----BWP checking----
	INSERT into	#mrtWithoutCharges
	SELECT DISTINCT
		COALESCE(hrs.code,hro.code,mrt.HotelCode) as [hotelCode]
		,mrt.HotelName as [hotelName]
		,mrt.hotelID as [synxisID]
		,mrt.confirmationNumber as [confirmationNumber]
		,cd.classificationID
		,brule.clauseID
		,mrt.channel as [channel]
		,mrt.secondarySource as [secondarySource]
		,mrt.subSourceCode as [subSourceCode]
		,mrt.CRO_Code as [CROCode]
		,ISNULL(pt.[siteAbbreviation],'HOTEL') as [templateAbbreviation]
		,mrt.xbeTemplateName as [xbeTemplateName]
		,mrt.rateCategoryCode as [rateCategoryCode]
		,mrt.rateTypeCode as [rateTypeCode]
		,mrt.arrivalDate as [arrivalDate]
		,mrt.Hotel_OpenHospID as [OpenHospitalityID]
		,mrt.currency
		,bc.bookingChargesCount
		,cc.commissionChargesCount
		,ipc.iPreferChargesCount
		,COALESCE(hrs.code,hro.code,NULL) as phHotelCode
		,gpc.custnmbr as gpHotelCode
		,gpc.CURNCYID as gpCurrency
		,cur.CURNCYID as resCurrency
		,1 as bookwithpointissue
	FROM Reservations.[billing].[MostRecentTransactions] mrt
		LEFT OUTER JOIN [dbo].[Templates] pt ON mrt.xbeTemplateName = pt.[xbeTemplateName]
		LEFT OUTER JOIN dbo.Charges cd on mrt.TransactionID = cd.transactionKey AND cd.transactionSourceID IN (1,2)
		LEFT OUTER JOIN dbo.BillingRules brule ON brule.billingRuleID = cd.[billingRuleID]
		LEFT OUTER JOIN [dbo].[BookingChargeCount] bc on mrt.TransactionID = bc.transactionKey AND bc.transactionSourceID IN (1,2)
		LEFT OUTER JOIN [dbo].[CommissionChargeCount] cc on mrt.TransactionID = cc.transactionKey AND cc.transactionSourceID IN (1,2)	
		LEFT OUTER JOIN [dbo].[iPreferChargeCount] ipc ON mrt.TransactionID = ipc.transactionKey AND ipc.transactionSourceID IN (1,2)	
		LEFT OUTER JOIN Hotels.dbo.hotelsReporting hrs ON mrt.hotelID = hrs.synxisID
		LEFT OUTER JOIN Hotels.dbo.hotelsReporting hro ON mrt.Hotel_OpenHospID = hro.openHospitalityCode
		LEFT OUTER JOIN IC.dbo.RM00101 gpc ON COALESCE(hro.code,hrs.code) = gpc.custnmbr
		LEFT OUTER JOIN DYNAMICS.dbo.MC00100 cur ON mrt.currency = cur.CURNCYID		AND CAST(mrt.arrivalDate AS DATE) = cur.EXCHDATE
		LEFT OUTER JOIN #nonBillable nb	on mrt.TransactionID = nb.transactionKey AND nb.transactionSourceID IN (1,2)
		LEFT OUTER JOIN dbo.Clauses cl ON COALESCE(hrs.code,hro.code,mrt.HotelCode) = cl.hotelCode AND cl.clauseName LIKE '%Any Booking%'
	WHERE mrt.arrivalDate BETWEEN @firstDayOfInvoiceMonth AND @endDate
		AND mrt.timeLoaded < DATEADD(day,-2,@maxCalcDate)
		AND mrt.status <> 'Cancelled'
		AND (nb.transactionKey IS NULL OR (nb.transactionKey IS NOT NULL AND cd.classificationID = 5)) --only want to see bookings not handled by non-billable charges
		AND cl.clauseID IS NULL
		AND mrt.rateTypeCode IN (SELECT rateTypeCode FROM ReservationBilling.loyalty.BookWithPointCreditRules)
	---------------------------------------------------------------------------------------------

	-- REBUILD dbo.BillyCalcErrors --------------------------------------------------------------
	--	empty the billy error table
	TRUNCATE TABLE dbo.BillyCalcErrors

	-- reinsert new error records into the table
	INSERT INTO dbo.BillyCalcErrors(errorMessage,hotelCode,hotelName,synxisID,openHospitalityID,confirmationNumber,clauseID,channel,secondarySource,subSourceCode,CROCode,templateAbbreviation,xbeTemplateName,rateCategoryCode,rateTypeCode,arrivalDate)
	SELECT 'Unable to map hotel to CRM' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NULL

	UNION ALL

	SELECT 'No hotel found in GP' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NULL

	UNION ALL

	SELECT 'No currency found on GP customer card' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NULL

	UNION ALL

	SELECT 'No currency exchange rate found in GP for ' + currency as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NOT NULL
		AND mrt.arrivalDate < GETDATE()
		AND resCurrency IS NULL 

	UNION ALL

	SELECT DISTINCT 'Multiple booking charges' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NOT NULL
		AND mrt.bookingChargesCount > 1
		AND mrt.classificationID = 1

	UNION ALL

	SELECT DISTINCT 'No booking or I Prefer charge found' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NOT NULL
		AND
		(
			(mrt.bookingChargesCount = 0 OR mrt.bookingChargesCount IS NULL)
			AND
			(mrt.ipreferChargesCount = 0 OR mrt.ipreferChargesCount IS NULL)
		)

	UNION ALL

	SELECT DISTINCT 'Multiple commission charges' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NOT NULL
		AND (mrt.commissionChargesCount > 1)
		AND mrt.classificationID = 2

	UNION ALL

	SELECT DISTINCT 'Multiple iPrefer charges' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NOT NULL
		AND (mrt.iPreferChargesCount > 1)
		AND mrt.classificationID = 5

	UNION ALL

	SELECT 'Book With Point Not Set Correctly' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE bookwithpointissue = 1

	UNION ALL

	SELECT 'Unknown error' as [errorMessage],[hotelCode],[hotelName],[synxisID],OpenHospitalityID,[confirmationNumber],[clauseID],[channel],[secondarySource],[subSourceCode],[CROCode],[templateAbbreviation],[xbeTemplateName],[rateCategoryCode],[rateTypeCode],[arrivalDate]
	FROM #mrtWithoutCharges mrt
	WHERE phHotelCode IS NOT NULL
		AND gpHotelCode IS NOT NULL
		AND gpCurrency IS NOT NULL
		AND (mrt.bookingChargesCount = 1)
		--AND (mrt.surchargeCount IS NULL OR mrt.surchargeCount <= 1)
		AND (mrt.commissionChargesCount IS NULL OR mrt.commissionChargesCount <= 1)
		AND bookwithpointissue = 0
		;
	---------------------------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Charges_Threshold_LastPeriod]'
GO
CREATE TABLE [dbo].[Charges_Threshold_LastPeriod]
(
[chargeID] [int] NOT NULL,
[runID] [int] NULL,
[clauseID] [int] NULL,
[billingRuleID] [int] NOT NULL,
[classificationID] [int] NOT NULL,
[transactionSourceID] [int] NULL,
[transactionKey] [int] NULL,
[confirmationNumber] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[hotelCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[collectionCode] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[clauseName] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[billableDate] [date] NULL,
[arrivalDate] [date] NULL,
[roomNights] [int] NULL,
[hotelCurrencyCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[exchangeDate] [date] NULL,
[chargeValueInHotelCurrency] [decimal] (38, 2) NULL,
[roomRevenueInHotelCurrency] [decimal] (38, 2) NULL,
[chargeValueInUSD] [decimal] (38, 2) NULL,
[roomRevenueInUSD] [decimal] (38, 2) NULL,
[itemCode] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[gpSiteID] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[dateCalculated] [datetime] NULL,
[sopNumber] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[invoiceDate] [date] NULL,
[loyaltyNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoyaltyNumberValidated] [bit] NULL,
[LoyaltyNumberTagged] [bit] NULL,
[deleted_sopNumber] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCrossBrand] [bit] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_Charges_Threshold_LastPeriod] on [dbo].[Charges_Threshold_LastPeriod]'
GO
ALTER TABLE [dbo].[Charges_Threshold_LastPeriod] ADD CONSTRAINT [PK_dbo_Charges_Threshold_LastPeriod] PRIMARY KEY NONCLUSTERED ([chargeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hotelCode_billableDate_IC_Others] on [dbo].[Charges_Threshold_LastPeriod]'
GO
CREATE NONCLUSTERED INDEX [IX_hotelCode_billableDate_IC_Others] ON [dbo].[Charges_Threshold_LastPeriod] ([hotelCode], [billableDate]) INCLUDE ([clauseID], [transactionSourceID], [transactionKey], [roomNights], [roomRevenueInHotelCurrency])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Calculate_ThresholdCharges]'
GO





ALTER PROCEDURE [work].[Calculate_ThresholdCharges]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	DECLARE @startDate date,@endDate date
	
	SELECT	@startDate = startDate,
			@endDate = endDate
	FROM work.Run
	WHERE RunID = @RunID

	-- #CHARGES CREATED IN PARENT CALL ---------------------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NULL
	BEGIN
		CREATE TABLE #CHARGES
		(
			[clauseID] [int] NOT NULL,
			[billingRuleID] [int] NOT NULL,
			[classificationID] [int] NOT NULL,
			[transactionSourceID] [int] NOT NULL,
			[transactionKey] [nvarchar](20) NOT NULL,
			[confirmationNumber] [nvarchar](255) NOT NULL,
			[hotelCode] [nvarchar](10) NOT NULL,
			[collectionCode] [nvarchar](6) NULL,
			[clauseName] [nvarchar](250) NULL,
			[billableDate] [date] NULL,
			[arrivalDate] [date] NULL,
			[roomNights] [int] NULL,
			[hotelCurrencyCode] [char](3) NULL,
			[exchangeDate] [date] NULL,
			[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
			[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
			[chargeValueInUSD] [decimal](38, 2) NULL,
			[roomRevenueInUSD] [decimal](38, 2) NULL,
			[itemCode] [nvarchar](50) NULL,
			[gpSiteID] [char](2) NULL,
			[dateCalculated] [datetime] NULL,
			[sopNumber] [char](21) NULL,
			[invoiceDate] [date] NULL,
			[loyaltyNumber] [nvarchar](50) NULL,
			[LoyaltyNumberValidated] [bit] NULL,
			[LoyaltyNumberTagged] [bit] NULL,
			[IsCrossBrand] [bit] NULL
		)
	END
	------------------------------------------------------------------------------------

	-- CREATE #THRESHOLD TEMP TABLE --------------------------------------------
	IF OBJECT_ID('tempdb..#THRESHOLD') IS NOT NULL
		DROP TABLE #THRESHOLD
	CREATE TABLE #THRESHOLD
	(
		billableDate date,
		transactionSourceID int,
		transactionKey nvarchar(20),
		hotelCode nvarchar(10),
		billingRuleID int,
		thresholdMinimum decimal(18,2),
		thresholdTypeID int,
		clauseID int,
		sumRoomNights int,
		countConfNum int,
		sumRoomRevenue decimal(18,2)
	)
	----------------------------------------------------------------------------

	DECLARE @billingRuleID int,
			@thresholdMinimum decimal(18,2),
			@LastPeriod date,
			@EndOfPeriod date,
			@IsGlobal bit,
			@hotelCode nvarchar(10),
			@thresholdTypeID int,
			@clauseID int
	
	-- LOOP THROUGH EACH BILLING PERIOD WITHIN DATE RANGE ----------------------
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate #THRESHOLD'
		RAISERROR(@print,10,1) WITH NOWAIT

	DECLARE curPeriod CURSOR FAST_FORWARD LOCAL FOR
		SELECT lp.LastPeriod,lp.EndOfPeriod,tr.[global],cl.hotelCode,tr.thresholdTypeID,tr.clauseID
		FROM dbo.fnc_Threshold_LastPeriod(@startDate,@endDate,@RunID) lp
			INNER JOIN dbo.ThresholdRules tr ON tr.clauseID = lp.clauseID
			INNER JOIN dbo.Clauses cl ON cl.clauseID = tr.clauseID
			INNER JOIN (SELECT DISTINCT ClauseID FROM work.MrtForCalc_Clauses WHERE runID = @RunID) mrtc ON mrtc.ClauseID = cl.clauseID

		OPEN curPeriod
		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		;WITH cte_charges
		AS
		(
			SELECT clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,[roomRevenueInHotelCurrency]
			FROM [dbo].[Charges_Threshold_LastPeriod]
			WHERE billableDate BETWEEN @LastPeriod AND @EndOfPeriod
				AND clauseID = CASE @IsGlobal WHEN 0 THEN @clauseID ELSE clauseID END
				AND hotelCode = @hotelCode
				AND (@IsGlobal = 0 OR (@IsGlobal = 1 AND transactionSourceID != 3)) --iPrefer manual transactions don't count in global counts, only if the threshold is just for manual points

			UNION ALL

			SELECT clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,[roomRevenueInHotelCurrency]
			FROM #CHARGES
			WHERE billableDate BETWEEN @LastPeriod AND @EndOfPeriod
				AND clauseID = CASE @IsGlobal WHEN 0 THEN @clauseID ELSE clauseID END
				AND hotelCode = @hotelCode
				AND (@IsGlobal = 0 OR (@IsGlobal = 1 AND transactionSourceID != 3)) --iPrefer manual transactions don't count in global counts, only if the threshold is just for manual points
		),
		cte_charges_Agg(clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,confCounter,[roomRevenueInHotelCurrency])
		AS
		(
			SELECT @clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,
				MAX(roomNights),1,MAX([roomRevenueInHotelCurrency])
			FROM cte_charges
			GROUP BY billableDate,transactionSourceID,transactionKey,hotelCode
		),
		cte_Threshold
		AS
		(
			SELECT billableDate,transactionSourceID,transactionKey,hotelCode,clauseID,
					SUM(roomNights) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomNights,
					SUM(confCounter) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS countConfNum,
					SUM([roomRevenueInHotelCurrency]) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomRevenue
			FROM cte_charges_Agg
		)
		INSERT INTO #THRESHOLD(billableDate,transactionSourceID,transactionKey,hotelCode,billingRuleID,thresholdMinimum,thresholdTypeID,clauseID,
								sumRoomNights,countConfNum,sumRoomRevenue)
		SELECT DISTINCT billableDate,transactionSourceID,transactionKey,hotelCode,br.billingRuleID,br.thresholdMinimum,@thresholdTypeID,@clauseID,
						sumRoomNights,countConfNum,sumRoomRevenue
		FROM cte_Threshold tr
			INNER JOIN dbo.BillingRules br ON br.[clauseID] = tr.[clauseID] AND tr.billableDate BETWEEN br.startDate AND br.endDate
		WHERE sumRoomNights >= CASE @thresholdTypeID WHEN 3 THEN br.thresholdMinimum ELSE sumRoomNights END
			AND countConfNum >= CASE @thresholdTypeID WHEN 2 THEN br.thresholdMinimum ELSE countConfNum END
			AND sumRoomRevenue >= CASE @thresholdTypeID WHEN 1 THEN br.thresholdMinimum ELSE sumRoomRevenue END
			AND br.startDate <= @endDate AND br.endDate >= @startDate

		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID
	END
	DEALLOCATE curPeriod
	----------------------------------------------------------------------------

	-- DELETE ALL CHARGES THAT HAVE THE SAME ClauseID BUT != BillingRuleID -----
		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate #CHARGE_ID'
		RAISERROR(@print,10,1) WITH NOWAIT

		;WITH cte_charges
		AS
		(
			SELECT DISTINCT t.transactionSourceID,t.transactionKey,t.billingRuleID,t.clauseID
			FROM dbo.Charges c
				INNER JOIN #THRESHOLD t ON t.transactionSourceID = c.transactionSourceID
					AND t.transactionKey = c.transactionKey
					AND t.clauseID = c.clauseID
				INNER JOIN (SELECT transactionSourceID,transactionKey,clauseID,MAX(thresholdMinimum) AS thresholdMinimum
							FROM #THRESHOLD
							GROUP BY transactionSourceID,transactionKey,clauseID
							) x ON x.transactionSourceID = t.transactionSourceID
					AND x.transactionKey = t.transactionKey 
					AND x.clauseID = t.clauseID 
					AND x.thresholdMinimum = t.thresholdMinimum
			WHERE c.billableDate BETWEEN @startDate AND @endDate
		)
		DELETE c
		FROM dbo.Charges c
			INNER JOIN cte_charges cte ON cte.transactionSourceID = c.transactionSourceID
				AND cte.transactionKey = c.transactionKey
				AND cte.clauseID = c.clauseID
		WHERE c.billingRuleID != cte.billingRuleID
			AND c.sopNumber IS NULL
			AND c.runID = @RunID


		;WITH cte_charges
		AS
		(
			SELECT DISTINCT t.transactionSourceID,t.transactionKey,t.billingRuleID,t.clauseID
			FROM #CHARGES c
				INNER JOIN #THRESHOLD t ON t.transactionSourceID = c.transactionSourceID
					AND t.transactionKey = c.transactionKey
					AND t.clauseID = c.clauseID
				INNER JOIN (SELECT transactionSourceID,transactionKey,clauseID,MAX(thresholdMinimum) AS thresholdMinimum
							FROM #THRESHOLD
							GROUP BY transactionSourceID,transactionKey,clauseID
							) x ON x.transactionSourceID = t.transactionSourceID
					AND x.transactionKey = t.transactionKey 
					AND x.clauseID = t.clauseID 
					AND x.thresholdMinimum = t.thresholdMinimum
			WHERE c.billableDate BETWEEN @startDate AND @endDate
		)
		DELETE c
		FROM #CHARGES c
			INNER JOIN cte_charges cte ON cte.transactionSourceID = c.transactionSourceID
				AND cte.transactionKey = c.transactionKey
				AND cte.clauseID = c.clauseID
		WHERE c.billingRuleID != cte.billingRuleID
			AND c.sopNumber IS NULL


		SET @print = CONVERT(varchar(100),GETDATE(),120) + ': DELETE dbo.Charges'
		RAISERROR(@print,10,1) WITH NOWAIT
	----------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[Calculate_ThresholdCharges]'
GO




ALTER PROCEDURE [test].[Calculate_ThresholdCharges]
	@RunID int
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @startDate date,@endDate date
	
	SELECT	@startDate = startDate,
			@endDate = endDate
	FROM work.Run
	WHERE RunID = @RunID
	
	-- CREATE #THRESHOLD TEMP TABLE --------------------------------------------
	IF OBJECT_ID('tempdb..#THRESHOLD') IS NOT NULL
		DROP TABLE #THRESHOLD
	CREATE TABLE #THRESHOLD
	(
		billableDate date,
		transactionSourceID int,
		transactionKey nvarchar(20),
		hotelCode nvarchar(10),
		billingRuleID int,
		thresholdMinimum decimal(18,2),
		thresholdTypeID int,
		clauseID int,
		sumRoomNights int,
		countConfNum int,
		sumRoomRevenue decimal(18,2)
	)
	----------------------------------------------------------------------------

	DECLARE @billingRuleID int,
			@thresholdMinimum decimal(18,2),
			@LastPeriod date,
			@EndOfPeriod date,
			@IsGlobal bit,
			@hotelCode nvarchar(10),
			@thresholdTypeID int,
			@clauseID int
	
	-- LOOP THROUGH EACH BILLING PERIOD WITHIN DATE RANGE ----------------------
	DECLARE curPeriod CURSOR FAST_FORWARD LOCAL FOR
		SELECT lp.LastPeriod,lp.EndOfPeriod,tr.[global],cl.hotelCode,tr.thresholdTypeID,tr.clauseID
		FROM test.fnc_Threshold_LastPeriod(@startDate,@endDate) lp
			INNER JOIN dbo.ThresholdRules tr ON tr.clauseID = lp.clauseID
			INNER JOIN dbo.Clauses cl ON cl.clauseID = tr.clauseID
			INNER JOIN (SELECT DISTINCT ClauseID FROM test.MrtForCalc_Clauses WHERE runID = @RunID) mrtc ON mrtc.ClauseID = cl.clauseID

		OPEN curPeriod
		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		;WITH cte_charges(clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,roomNights,confCounter,[roomRevenueInHotelCurrency])
		AS
		(
			SELECT @clauseID,billableDate,transactionSourceID,transactionKey,hotelCode,
				MAX(roomNights),1,MAX([roomRevenueInHotelCurrency])
			FROM [dbo].[Charges_Threshold_LastPeriod]
			WHERE billableDate BETWEEN @LastPeriod AND @EndOfPeriod
				AND clauseID = CASE @IsGlobal WHEN 0 THEN @clauseID ELSE clauseID END
				--AND hotelCode = CASE @IsGlobal WHEN 0 THEN hotelCode ELSE @hotelCode END
				AND hotelCode = @hotelCode
				AND (@IsGlobal = 0 OR (@IsGlobal = 1 AND transactionSourceID != 3)) --iPrefer manual transactions don't count in global counts, only if the threshold is just for manual points
			GROUP BY billableDate,transactionSourceID,transactionKey,hotelCode
		),
		cte_Threshold
		AS
		(
			SELECT billableDate,transactionSourceID,transactionKey,hotelCode,clauseID,
					SUM(roomNights) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomNights,
					SUM(confCounter) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS countConfNum,
					SUM([roomRevenueInHotelCurrency]) OVER(ORDER BY billableDate,transactionSourceID,transactionKey) AS sumRoomRevenue
			FROM cte_charges
		)
		INSERT INTO #THRESHOLD(billableDate,transactionSourceID,transactionKey,hotelCode,billingRuleID,thresholdMinimum,thresholdTypeID,clauseID,
								sumRoomNights,countConfNum,sumRoomRevenue)
		SELECT DISTINCT billableDate,transactionSourceID,transactionKey,hotelCode,br.billingRuleID,br.thresholdMinimum,@thresholdTypeID,@clauseID,
						sumRoomNights,countConfNum,sumRoomRevenue
		FROM cte_Threshold tr
			INNER JOIN dbo.BillingRules br ON br.[clauseID] = tr.[clauseID]
		WHERE sumRoomNights >= CASE @thresholdTypeID WHEN 3 THEN br.thresholdMinimum ELSE sumRoomNights END
			AND countConfNum >= CASE @thresholdTypeID WHEN 2 THEN br.thresholdMinimum ELSE countConfNum END
			AND sumRoomRevenue >= CASE @thresholdTypeID WHEN 1 THEN br.thresholdMinimum ELSE sumRoomRevenue END
			AND br.startDate <= @endDate AND br.endDate >= @startDate

		FETCH NEXT FROM curPeriod INTO @LastPeriod,@EndOfPeriod,@IsGlobal,@hotelCode,@thresholdTypeID,@clauseID
	END
	DEALLOCATE curPeriod
	----------------------------------------------------------------------------

		-- DELETE ALL CHARGES THAT HAVE THE SAME ClauseID BUT != BillingRuleID -----
		;WITH cte_charges
		AS
		(
			SELECT DISTINCT t.transactionSourceID,t.transactionKey,t.billingRuleID,t.clauseID
			FROM test.Charges c
				INNER JOIN #THRESHOLD t ON t.transactionSourceID = c.transactionSourceID
					AND t.transactionKey = c.transactionKey
					AND t.clauseID = c.clauseID
				INNER JOIN (SELECT transactionSourceID,transactionKey,clauseID,MAX(thresholdMinimum) AS thresholdMinimum
							FROM #THRESHOLD
							GROUP BY transactionSourceID,transactionKey,clauseID
						   ) x ON x.transactionSourceID = t.transactionSourceID
					AND x.transactionKey = t.transactionKey 
					AND x.clauseID = t.clauseID 
					AND x.thresholdMinimum = t.thresholdMinimum
			WHERE c.billableDate BETWEEN @startDate AND @endDate
		)
		DELETE c
		FROM test.Charges c
			INNER JOIN cte_charges cte ON cte.transactionSourceID = c.transactionSourceID
				AND cte.transactionKey = c.transactionKey
				AND cte.clauseID = c.clauseID
		WHERE c.billingRuleID != cte.billingRuleID
			AND c.sopNumber IS NULL
			AND c.runID = @RunID
		----------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Move_Charges_BillableDate]'
GO




ALTER PROCEDURE [work].[Move_Charges_BillableDate]
	@RunID int,
	@invoiceDate DATE
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	UPDATE dbo.charges
		SET billableDate = DATEADD(month, DATEDIFF(month, 0, @invoiceDate), 0)
	WHERE runID = @RunID
		AND billableDate < DATEADD(month, DATEDIFF(month, 0, @invoiceDate), 0) -- Start of latest invoice month
		AND sopNumber IS NULL --Not invoiced
		AND transactionSourceID = 3 --manual point award

	DELETE FROM dbo.charges
	WHERE runID = @RunID
		AND billableDate < DATEADD(month, DATEDIFF(month, 0, @invoiceDate), 0) -- Start of latest invoice month
		AND sopNumber IS NULL --Not invoiced
		AND transactionSourceID IN (1,2) --Synix + OpenHosp

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[handleCommissions]'
GO

ALTER PROCEDURE [work].[handleCommissions]
	@RunID int,
	@RunType bit
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	-- CREATE #FEES -------------------------------------------
	IF OBJECT_ID('tempdb..#FEES') IS NOT NULL
		DROP TABLE #FEES;
	CREATE TABLE #FEES
	(
		transactionSourceID int NOT NULL,
		transactionKey int NOT NULL,
		totalCharges decimal(38,2),
		totalChargesUSD decimal(38,2),

		PRIMARY KEY CLUSTERED(transactionSourceID,transactionKey)
	)
	-----------------------------------------------------------

	IF @RunType = 0 --test run
	BEGIN
		;WITH fees
		AS
		(
			SELECT ch.transactionSourceID, ch.transactionKey, SUM(chargeValueInHotelCurrency) AS totalCharges, SUM(chargeValueInUSD) AS totalChargesUSD
			FROM test.Charges AS ch
				INNER JOIN
				(
					SELECT DISTINCT runID,ClauseID,confirmationNumber
					FROM test.MrtForCalc_Clauses 
					WHERE blueCriteriaGroupID IS NULL
						AND orangeCriteriaID IS NULL
						AND redSourceID IS NULL
						AND runID = @RunID
				) cl ON ch.runID = cl.runID AND ch.confirmationNumber = cl.confirmationNumber AND ch.clauseID = cl.ClauseID
			WHERE ch.runid = @runid
				AND classificationID = 1 --booking charges
			GROUP BY ch.transactionSourceID,ch.transactionKey
		)
		UPDATE commissions
			SET chargeValueInHotelCurrency = 
					CASE 
						WHEN (chargeValueInHotelCurrency - fees.totalCharges) <= 0 THEN 0 
						ELSE (chargeValueInHotelCurrency - fees.totalCharges) 
					END,
				chargeValueInUSD = 
					CASE 
						WHEN (chargeValueInUSD - fees.totalChargesUSD) <= 0 THEN 0 
						ELSE (chargeValueInUSD - fees.totalChargesUSD) 
					END
		FROM test.Charges AS commissions
			INNER JOIN fees AS fees ON commissions.transactionSourceID = fees.transactionSourceID AND commissions.transactionKey = fees.transactionKey
			INNER JOIN 
			(
				SELECT DISTINCT runID, ClauseID, confirmationNumber
				FROM test.MrtForCalc_Clauses 
				WHERE  blueCriteriaGroupID IS NULL
					AND orangeCriteriaID IS NULL
					AND redSourceID IS NULL
					AND runID = @RunID
			) cl ON commissions.runID = cl.runID AND commissions.confirmationNumber = cl.confirmationNumber AND commissions.clauseID = cl.ClauseID
		WHERE commissions.runID = @RunID
			AND commissions.classificationID = 2 --commission charges
			AND commissions.sopNumber IS NULL
	END
	ELSE --production run
	BEGIN
		INSERT INTO #FEES(transactionSourceID,transactionKey,totalCharges,totalChargesUSD)
		SELECT transactionSourceID, transactionKey, SUM(chargeValueInHotelCurrency) AS totalCharges, SUM(chargeValueInUSD) AS totalChargesUSD
		FROM dbo.Charges
		WHERE runid = @runid
			AND classificationID = 1 --booking charges
			AND transactionSourceID IS NOT NULL
			AND transactionKey IS NOT NULL
		GROUP BY transactionSourceID, transactionKey

		UPDATE commissions
			SET chargeValueInHotelCurrency = 
					CASE 
						WHEN (chargeValueInHotelCurrency - fees.totalCharges) <= 0 THEN 0 
						ELSE (chargeValueInHotelCurrency - fees.totalCharges) 
					END,
				chargeValueInUSD = 
					CASE 
						WHEN (chargeValueInUSD - fees.totalChargesUSD) <= 0 THEN 0 
						ELSE (chargeValueInUSD - fees.totalChargesUSD) 
					END

		FROM dbo.Charges AS commissions
			INNER JOIN #FEES AS fees ON commissions.transactionSourceID = fees.transactionSourceID AND commissions.transactionKey = fees.transactionKey
		WHERE commissions.runID = @RunID
			AND commissions.classificationID = 2 --commission charges
			AND commissions.sopNumber IS NULL	
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_Charges_Threshold_LastPeriod]'
GO

CREATE PROCEDURE [dbo].[Populate_Charges_Threshold_LastPeriod]
	@RunID int = NULL
AS
BEGIN
	DECLARE @startDate date,
			@endDate date,
			@minLastPeriod date

	SELECT	@startDate = startDate,@endDate = endDate
	FROM work.Run
	WHERE RunID = @RunID

	SELECT @minLastPeriod = MIN(LastPeriod)
	FROM test.fnc_Threshold_LastPeriod(@startDate,@endDate) 


	TRUNCATE TABLE [dbo].[Charges_Threshold_LastPeriod]

	INSERT INTO [dbo].[Charges_Threshold_LastPeriod]([chargeID],[runID],[clauseID],[billingRuleID],[classificationID],[transactionSourceID],[transactionKey],[confirmationNumber],[hotelCode],[collectionCode],[clauseName],[billableDate],[arrivalDate],[roomNights],[hotelCurrencyCode],[exchangeDate],[chargeValueInHotelCurrency],[roomRevenueInHotelCurrency],[chargeValueInUSD],[roomRevenueInUSD],[itemCode],[gpSiteID],[dateCalculated],[sopNumber],[invoiceDate],[loyaltyNumber],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[deleted_sopNumber],[IsCrossBrand])
	SELECT [chargeID],[runID],[clauseID],[billingRuleID],[classificationID],[transactionSourceID],[transactionKey],[confirmationNumber],[hotelCode],[collectionCode],[clauseName],[billableDate],[arrivalDate],[roomNights],[hotelCurrencyCode],[exchangeDate],[chargeValueInHotelCurrency],[roomRevenueInHotelCurrency],[chargeValueInUSD],[roomRevenueInUSD],[itemCode],[gpSiteID],[dateCalculated],[sopNumber],[invoiceDate],[loyaltyNumber],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[deleted_sopNumber],[IsCrossBrand]
	FROM dbo.Charges
	WHERE billableDate >= @minLastPeriod
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[RunCalculator]'
GO




ALTER PROCEDURE [work].[RunCalculator]
	@runType bit = 1, --0=test, 1=production
	@startDate date = NULL,
	@endDate date = NULL,
	@hotelCode nvarchar(20) = NULL,
	@confirmationNumber nvarchar(255) = NULL,
	@debug bit = 0,
	@logProcess bit = 1,
	@invoicedate date = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000),
			@Process_GUID uniqueidentifier = NEWID(),
			@ProcName sysname,
			@date datetime;

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'START',@Process_GUID


	-- DELETE MRT ---------------------------------------------------------------
	EXEC [work].[Delete_MRT]

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[work].[Delete_MRT] Finished',@Process_GUID
	-----------------------------------------------------------------------------


	DECLARE @RunID int;
	-- SET RUN PARAMETERS -------------------------------------------------------
	INSERT INTO work.Run(RunDate,RunType,RunStatus,startDate,endDate,hotelCode,confirmationNumber)
	VALUES(GETDATE(),@runType,0,@startDate,@endDate,@hotelCode,@confirmationNumber)

	SET @RunID = SCOPE_IDENTITY();

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'work.Run insert',@Process_GUID
	-----------------------------------------------------------------------------

	-- GET RUN PARAMETERS -------------------------------------------------------
	IF(@confirmationNumber IS NOT NULL)
	BEGIN
		SELECT @startDate = DATEADD(DAY,-2,MIN(sub.billableDate)),
				@endDate = DATEADD(DAY,2,MAX(sub.billableDate)),
				@hotelCode = MAX(sub.phgHotelCode)
		
		FROM
		(		
			SELECT mrt.phgHotelCode, CONVERT(date,CASE WHEN mrt.arrivalDate >= GETDATE() THEN mrt.confirmationDate ELSE mrt.arrivaldate END) as billableDate
			FROM work.mrtJoined_Reservation mrt
			WHERE mrt.confirmationNumber = @confirmationNumber
				UNION 
			SELECT tdr.phgHotelCode, CASE WHEN tdr.arrivalDate >= GETDATE() THEN tdr.transactionTimeStamp ELSE tdr.arrivalDate END as billableDate
			FROM work.tdrJoined_Reservation tdr
			WHERE tdr.confirmationNumber = @confirmationNumber
		) as sub
	END

	IF(@startDate IS NULL OR @endDate IS NULL)
	BEGIN
		IF @startDate IS NULL
			BEGIN
			IF DATEPART(dd, GETDATE()) < 8 -- run for the previous month, IF we are in first week
			BEGIN
				SET @startDate = DATEADD (mm,-1,GETDATE())
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
			ELSE
			BEGIN
				SET @startDate = GETDATE()
				SET @startDate = CAST(CAST(DATEPART(mm,@startDate) AS char(2)) + '/01/' + CAST(DATEPART(yyyy,@startDate) AS char(4)) AS date)
			END
		END

		IF @endDate IS NULL OR @endDate < @startDate
		BEGIN
			SET @endDate = DATEADD(YEAR,2,@startDate)
		END
	END

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'SET UP PARAMETER',@Process_GUID
	-----------------------------------------------------------------------------

	-- UPDATE RUN PARAMETERS ----------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 1,
	startDate = @startDate,
	endDate = @endDate
	WHERE runID = @RunID;
	-----------------------------------------------------------------------------
	
	-- POPULATE [dbo].[Charges_Threshold_LastPeriod] ----------------------------
	EXEC dbo.Populate_Charges_Threshold_LastPeriod @RunID

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'POPULATE [dbo].[Charges_Threshold_LastPeriod] complete',@Process_GUID
	-----------------------------------------------------------------------------

	-- POPULATE [MrtForCalculation] ---------------------------------------------
	IF @runType = 0
	BEGIN
		EXEC [work].[Populate_ExchangeRates] @runType, @startDate, @endDate
		EXEC [test].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber
	END
	ELSE
		EXEC [work].[Populate_MrtForCalculation_Reservation] @startDate, @endDate, @RunID, @hotelCode, @confirmationNumber

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'POPULATE [MrtForCalculation] complete',@Process_GUID
	-----------------------------------------------------------------------------

	-- POPULATE BILLING VIEWS ---------------------------------------------------
	/*
	EXEC temp.Populate_BillingViews @RunID

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': POPULATE Billing Views'
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
	RAISERROR(@print,10,1) WITH NOWAIT
	*/
	-----------------------------------------------------------------------------

	-- POPULATE MrtForCalc_Clauses ----------------------------------------------
	IF @runType = 0
		EXEC test.Populate_MrtForCalc_Clauses @RunID
	ELSE
		EXEC work.Populate_MrtForCalc_Clauses @RunID

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'POPULATE work.Populate_MrtForCalc_Clauses complete',@Process_GUID
	-----------------------------------------------------------------------------

	-- Eliminate excluded clauses------------------------------------------------
	IF @runType = 0
		EXEC test.handleExclusions @RunID
	ELSE
		EXEC work.handleExclusions @RunID

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'handleExclusions complete',@Process_GUID
	-----------------------------------------------------------------------------

	-- CALCULATE CHARGES --------------------------------------------------------
	IF @runType = 0
	BEGIN
		-- RUN standard calculation ---------------------------------------------
		EXEC test.Calculate_StandardCharges @RunID

		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'Calculate_StandardCharges complete',@Process_GUID
		-------------------------------------------------------------------------

		-- Calculate thresholds -------------------------------------------------
		EXEC test.Calculate_ThresholdCharges @RunID

		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'Calculate_ThresholdCharges complete',@Process_GUID
		-------------------------------------------------------------------------
	END
	ELSE
	BEGIN
		DELETE c
		FROM dbo.Charges c
		WHERE billableDate BETWEEN @startDate AND @endDate
		AND hotelCode = ISNULL(@hotelCode, hotelCode)
		AND confirmationNumber = ISNULL(@confirmationNumber, confirmationNumber)
		AND c.sopNumber IS NULL
		AND transactionSourceID = 3 --delete iprefer charge for recalculating summary manual point credit
									--This is temparory solution to avoid big change on Calculate_StandardCharges proc. Will fix this bug later.

		EXEC work.Calculate_Charges @RunID
	END
	-----------------------------------------------------------------------------

	-- Subtract Booking fees from Commission fees--------------------------------
	EXEC work.handleCommissions @RunID, @RunType

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'handleCommissions complete',@Process_GUID
	-----------------------------------------------------------------------------

	-- move billableDate for old historical arrival --------------------------------
	IF @runType = 1
	BEGIN
		EXEC [work].[Move_Charges_BillableDate] @RunID,@invoicedate
	END

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'Move_Charges_BillableDate complete',@Process_GUID
	-----------------------------------------------------------------------------

	
	IF @runType = 1
	BEGIN
		EXEC work.LoadBillyErrorTable @RunID,@invoicedate
		EXEC work.LoadBillyPointAwardErrorsTable @RunID
	END

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'LoadBillyErrorTable complete',@Process_GUID

	-- SET RUN TO COMPLETED -------------------------------------------------------
	UPDATE work.Run
	SET RunStatus = 2 
	WHERE runID = @RunID

	SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
	EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'execution complete',@Process_GUID

	RETURN @runID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IPreferGuestNameReport_Reservation]'
GO
-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-05-30
-- Description:	This proc is a test version of IPreferGuestNameReport_2017 pointing to IC DB called by SSRS GuestNameReport_Test
-- EXEC [dbo].[IPreferGuestNameReport_Reservation] 'RES0179263           '
-- =============================================
ALTER PROCEDURE [dbo].[IPreferGuestNameReport_Reservation]
	@SOPNumber varchar(20)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'

	--DECLARE @SOPNumber varchar(20) = 'RES0115115'
	-- exec dbo.IPreferGuestNameReport_2017 @SOPNumber = 'RES0115115'
	
	-- POPULATE @currencymaster --------------------------------------------------
	DECLARE @currencymaster
		TABLE
		(
			CURNCYID char(15),
			CURRNIDX smallint,
			NOTEINDX numeric(19, 5),
			CRNCYDSC char(31),
			CRNCYSYM char(3),
			CNYSYMAR_1 smallint,
			CNYSYMAR_2 smallint,
			CNYSYMAR_3 smallint,
			CYSYMPLC smallint,
			INCLSPAC tinyint,
			NEGSYMBL smallint,
			NGSMAMPC smallint,
			NEGSMPLC smallint,
			DECSYMBL smallint,
			DECPLCUR smallint,
			THOUSSYM smallint,
			CURTEXT_1 char(25),
			CURTEXT_2 char(25),
			CURTEXT_3 char(25),
			ISOCURRC char(3),
			CURLNGID smallint,
			DEX_ROW_TS datetime,
			DEX_ROW_ID int
		);

	INSERT INTO @currencymaster(CURNCYID,CURRNIDX,NOTEINDX,CRNCYDSC,CRNCYSYM,CNYSYMAR_1,CNYSYMAR_2,CNYSYMAR_3,
								CYSYMPLC,INCLSPAC,NEGSYMBL,NGSMAMPC,NEGSMPLC,DECSYMBL,DECPLCUR,THOUSSYM,
								CURTEXT_1,CURTEXT_2,CURTEXT_3,ISOCURRC,CURLNGID,DEX_ROW_TS,DEX_ROW_ID)
	SELECT CURNCYID,CURRNIDX,NOTEINDX,CRNCYDSC,CRNCYSYM,CNYSYMAR_1,CNYSYMAR_2,CNYSYMAR_3,
		CYSYMPLC,INCLSPAC,NEGSYMBL,NGSMAMPC,NEGSMPLC,DECSYMBL,DECPLCUR,THOUSSYM,
		CURTEXT_1,CURTEXT_2,CURTEXT_3,ISOCURRC,CURLNGID,DEX_ROW_TS,DEX_ROW_ID
	FROM DYNAMICS.dbo.MC40200
	------------------------------------------------------------------------------

	-- POPULATE @GP_SOPNumber_ItemCodes ------------------------------------------
	DECLARE @GP_SOPNumber_ItemCodes
		TABLE
		(	
			ITEMNMBR varchar(50),
			ItemGrouping varchar(250)
		);

	INSERT INTO @GP_SOPNumber_ItemCodes(ITEMNMBR,ItemGrouping)
	SELECT itemCode.ITEMNMBR,
		CASE
			WHEN itemCode.ITEMDESC <> COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC) THEN COALESCE(historyLines.ITEMDESC, workLines.ITEMDESC)
			ELSE
				CASE COALESCE(RTRIM(itemClasses.ITMCLSDC), '') WHEN '' THEN ''
				ELSE RTRIM(itemClasses.ITMCLSDC) + ' '
			END
			+
			CASE COALESCE(RTRIM(itemCode.ITMSHNAM),'')
				WHEN '' THEN '' 
				ELSE RTRIM(itemCode.ITMSHNAM)
			END 
		END AS itemGrouping	
	FROM IC.dbo.IV00101 AS itemCode 
		LEFT JOIN IC.dbo.SY03900 itemNotes ON itemCode.NOTEINDX = itemNotes.NOTEINDX 
		LEFT JOIN IC.dbo.IV40600 itemClassifications ON itemClassifications.USCATNUM = 1 AND itemCode.USCATVLS_1 = itemClassifications.USCATVAL 
		LEFT JOIN IC.dbo.IV40400 itemClasses ON itemCode.ITMCLSCD = itemClasses.ITMCLSCD
		LEFT JOIN IC.dbo.SOP30300 historyLines ON historyLines.SOPNUMBE = 'abc' AND itemCode.itemCode = historyLines.ITEMNMBR
		LEFT JOIN IC.dbo.SOP10200 workLines ON  workLines.SOPNUMBE = 'ABC' AND itemCode.itemCode = workLines.ITEMNMBR

	------------------------------------------------------------------------------

	-- CREATE & POPULATE #CHARGES ------------------------------------------------
	IF OBJECT_ID('tempdb..#CHARGES') IS NOT NULL
		DROP TABLE #CHARGES;

	CREATE TABLE #CHARGES
	(
		[classificationID] [int] NOT NULL,
		[confirmationNumber] [nvarchar](255) NULL,
		[billableDate] [date] NULL,
		[arrivalDate] [date] NULL,
		[roomNights] [int] NULL,
		[hotelCurrencyCode] [char](3) NULL,
		[chargeValueInHotelCurrency] [decimal](38, 2) NULL,
		[roomRevenueInHotelCurrency] [decimal](38, 2) NULL,
		[chargeValueInUSD] [decimal](38, 2) NULL,
		[itemCode] [nvarchar](50) NULL,
		[sopNumber] [char](21) NULL,
		[loyaltyNumber] [nvarchar](50) NULL,
	)

	INSERT INTO #CHARGES([classificationID],[confirmationNumber],[billableDate],[arrivalDate],[roomNights],[hotelCurrencyCode],
						[chargeValueInHotelCurrency],[roomRevenueInHotelCurrency],[chargeValueInUSD],[itemCode],[sopNumber],[loyaltyNumber])
	SELECT [classificationID],[confirmationNumber],[billableDate],[arrivalDate],[roomNights],[hotelCurrencyCode],
			[chargeValueInHotelCurrency],[roomRevenueInHotelCurrency],[chargeValueInUSD],[itemCode],[sopNumber],[loyaltyNumber]
	FROM dbo.Charges
	WHERE sopNumber = @SOPNumber
		--AND itemCode IN(SELECT ITEMNMBR FROM @GP_SOPNumber_ItemCodes)
	------------------------------------------------------------------------------


	SELECT	main.currency AS currency
	,		main.arrivalDate AS arrivalDate
	,		main.guestLastName AS guestLastName
	,		main.guestFirstName AS guestFirstName
	,		main.confirmationNumber AS confirmationNumber
	,		main.confirmationDate AS confirmationDate
	,		main.consortia AS consortia
	,		main.IATANumber AS IATANumber
	,		main.rateTypeCode AS rateTypeCode
	,		CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END AS iPreferNumber
	,		main.averageDailyRate AS averageDailyRate
	,		main.roomNights AS roomNights
	,		main.reservationRevenue AS reservationRevenue
	,		main.iPreferCharge AS iPreferCharge
	,		main.commCharge AS commCharge
	,		main.bookCharge AS bookCharge
	,		main.surcharge AS surcharge
	,		ISNULL(main.totalCharge, 0) AS totalCharge
	,		ISNULL(REPLACE(main.itemGrouping, 'zzzzz! ', ''), 'Other Booking Sources' ) AS itemGrouping --remove hardcode value zzzzz!
	,		main.billingDate AS billingDate
	,		NULL AS CreateUser
	,		NULL AS PointsAwarded
	,		NULL AS [Description]
	,		NULL AS TransactionDate

	FROM
	(
		SELECT chargedReservations.hotelCurrencyCode AS currency, 
			chargedReservations.arrivalDate, 
			UPPER(MRT.guestLastName) AS guestLastName, 
			UPPER(MRT.guestFirstName) AS guestFirstName, 
			chargedReservations.confirmationNumber, 
			MRT.confirmationDate ,
			dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
			COALESCE (MRT.IATANumber, N'') AS IATANumber, 
			MRT.rateTypeCode,
			chargedReservations.loyaltyNumber AS iPreferNumber,
			Case When chargedReservations.roomNights <> 0
				THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
				ELSE 0 END AS averageDailyRate, 
			chargedReservations.roomNights, 
			chargedReservations.roomRevenueInHotelCurrency AS reservationRevenue, 
					SUM(case when chargedReservations.[classificationID] = 5 -- Booking
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency
						end
					else null
				end) AS iPreferCharge,
			SUM(case when chargedReservations.[classificationID] = 1 -- Booking
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency
						end
					else null
				end) AS bookCharge, 
			SUM(case when chargedReservations.[classificationID] = 2 -- commission
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency						
						end
					else null
				end) AS commCharge, 
			SUM(case when chargedReservations.classificationID = 3 -- surcharges
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency						
						end
					else null
				end) AS surcharge,
			SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
			MIN(case when chargedReservations.[classificationID] NOT IN (3,5) -- surcharges I prefer
					 then LTRIM(RTRIM(GPSopIC.itemGrouping))
					when chargedReservations.[classificationID] = 5 
					 then 'zzzzz! Hotel & Manual I Prefer Point Awards' --append zzzzz! on purpose for max logic to work
					 when chargedReservations.classificationID = 3 AND mrt.channel IN ('Channel Connect','IDS') AND itemCode = 'AMEXTMC'
						then 'zzzzz! Expedia/Amex OTA'
				END ) AS itemGrouping,
			chargedReservations.billableDate AS billingDate
		FROM @GP_SOPNumber_ItemCodes GPSopIC
			INNER JOIN #CHARGES chargedReservations ON chargedReservations.itemCode = GPSopIC.ITEMNMBR 
			LEFT OUTER JOIN Reservations.[billing].[MostRecentTransactions] MRT WITH(NOLOCK) ON chargedReservations.confirmationNumber = MRT.confirmationNumber 
		WHERE chargedReservations.classificationID <> 5
			OR
			(
				chargedReservations.classificationID = 5
				AND
				chargedReservations.chargeValueInUSD <> 0.00
				AND
				chargedReservations.itemCode NOT IN ('IPREFERMANUAL_I', 'PMS_I', 'IPREFERNONSTAY_I')
			)
		GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
				chargedReservations.arrivalDate, MRT.guestLastName, 
				MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
				MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber
	) AS main

	UNION ALL

	--Display New billy Iprefer manual point award
	SELECT	main.currency AS currency
	,		main.arrivalDate AS arrivalDate
	,		main.guestLastName AS guestLastName
	,		main.guestFirstName AS guestFirstName
	,		main.confirmationNumber AS confirmationNumber
	,		main.confirmationDate AS confirmationDate
	,		main.consortia AS consortia
	,		main.IATANumber AS IATANumber
	,		main.rateTypeCode AS rateTypeCode
	,		CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END AS iPreferNumber
	,		main.averageDailyRate AS averageDailyRate
	,		main.roomNights AS roomNights
	,		main.reservationRevenue AS reservationRevenue
	,		main.iPreferCharge AS iPreferCharge
	,		main.commCharge AS commCharge
	,		main.bookCharge AS bookCharge
	,		main.surcharge AS surcharge
	,		ISNULL(main.totalCharge, 0) AS totalCharge
	,		ISNULL(REPLACE(main.itemGrouping, 'zzzzz! ', ''), 'Other Booking Sources' ) AS itemGrouping --remove hardcode value zzzzz!
	,		main.billingDate AS billingDate
	,		NULL AS CreateUser
	,		NULL AS PointsAwarded
	,		NULL AS [Description]
	,		NULL AS TransactionDate

	FROM
	(
		SELECT chargedReservations.hotelCurrencyCode AS currency, 
			chargedReservations.arrivalDate, 
			COALESCE(UPPER(MRT.guestLastName), UPPER(cpi.Last_Name)) AS guestLastName, 
			COALESCE(UPPER(MRT.guestFirstName), UPPER(cpi.First_Name)) AS guestFirstName, 
			chargedReservations.confirmationNumber, 
			chargedReservations.billableDate AS confirmationDate, -- per MM on 5/3/23 when it's a manual point award we want to use the billable date AS the confirmationDate
			dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
			COALESCE (MRT.IATANumber, N'') AS IATANumber, 
			MRT.rateTypeCode,
			chargedReservations.loyaltyNumber AS iPreferNumber,
			Case When chargedReservations.roomNights <> 0
				THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
				ELSE 0 END AS averageDailyRate, 
			chargedReservations.roomNights, 
			chargedReservations.roomRevenueInHotelCurrency AS reservationRevenue, 
					SUM(case when chargedReservations.[classificationID] = 5 -- Booking
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency
						end
					else null
				end) AS iPreferCharge,
			SUM(case when chargedReservations.[classificationID] = 1 -- Booking
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency
						end
					else null
				end) AS bookCharge, 
			SUM(case when chargedReservations.[classificationID] = 2 -- commission
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency						
						end
					else null
				end) AS commCharge, 
			SUM(case when chargedReservations.classificationID = 3 -- surcharges
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency						
						end
					else null
				end) AS surcharge,
			SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
			MIN(case when chargedReservations.[classificationID] NOT IN (3,5) -- surcharges I prefer
					 then LTRIM(RTRIM(GPSopIC.itemGrouping))
					when chargedReservations.[classificationID] = 5 
					 then 'zzzzz! Hotel & Manual I Prefer Point Awards' --append zzzzz! on purpose for max logic to work
					 when chargedReservations.classificationID = 3 AND mrt.channel IN ('Channel Connect','IDS') AND itemCode = 'AMEXTMC'
						then 'zzzzz! Expedia/Amex OTA'
				END ) AS itemGrouping,
			chargedReservations.billableDate AS billingDate
		FROM @GP_SOPNumber_ItemCodes GPSopIC
			INNER JOIN #CHARGES chargedReservations ON chargedReservations.itemCode = GPSopIC.ITEMNMBR 
			LEFT OUTER JOIN Reservations.[billing].[MostRecentTransactions] MRT WITH(NOLOCK) ON chargedReservations.confirmationNumber = MRT.confirmationNumber 
			LEFT OUTER JOIN loyalty.dbo.Customer_Profile_Import cpi ON chargedReservations.loyaltyNumber = cpi.iPrefer_Number
		WHERE chargedReservations.classificationID = 5
			AND chargedReservations.chargeValueInUSD <> 0.00
			AND chargedReservations.itemCode IN ('IPREFERMANUAL_I', 'PMS_I') 
		GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
			chargedReservations.arrivalDate, MRT.guestLastName, 
			MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
			MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber,
			cpi.Last_Name,cpi.First_Name

	) AS main

	UNION ALL

	--Display billy Iprefer Non Stay point 
	SELECT	main.currency AS currency
	,		main.arrivalDate AS arrivalDate
	,		main.guestLastName AS guestLastName
	,		main.guestFirstName AS guestFirstName
	,		main.confirmationNumber AS confirmationNumber
	,		main.confirmationDate AS confirmationDate
	,		main.consortia AS consortia
	,		main.IATANumber AS IATANumber
	,		main.rateTypeCode AS rateTypeCode
	,		CASE WHEN main.iPreferNumber =  '' THEN NULL ELSE main.iPreferNumber END AS iPreferNumber
	,		main.averageDailyRate AS averageDailyRate
	,		main.roomNights AS roomNights
	,		main.reservationRevenue AS reservationRevenue
	,		main.iPreferCharge AS iPreferCharge
	,		main.commCharge AS commCharge
	,		main.bookCharge AS bookCharge
	,		main.surcharge AS surcharge
	,		ISNULL(main.totalCharge, 0) AS totalCharge
	,		'Non Stay Point Awards' AS itemGrouping 
	,		main.billingDate AS billingDate
	,		CreateUser
	,		PointsAwarded
	,		[Description]
	,		TransactionDate

	FROM
	(
		SELECT chargedReservations.hotelCurrencyCode AS currency, 
			chargedReservations.arrivalDate, 
			COALESCE(UPPER(MRT.guestLastName), UPPER(cpi.Last_Name)) AS guestLastName, 
			COALESCE(UPPER(MRT.guestFirstName), UPPER(cpi.First_Name)) AS guestFirstName, 
			chargedReservations.confirmationNumber, 
			ISNULL(MRT.confirmationDate, chargedReservations.billableDate) AS confirmationDate,
			dbo.getBilledConsortia_NewBilly(chargedReservations.confirmationNumber) AS consortia, 
			COALESCE (MRT.IATANumber, N'') AS IATANumber, 
			MRT.rateTypeCode,
			chargedReservations.loyaltyNumber AS iPreferNumber,
			Case When chargedReservations.roomNights <> 0
				THEN chargedReservations.roomRevenueInHotelCurrency / chargedReservations.roomNights
				ELSE 0 END AS averageDailyRate, 
			chargedReservations.roomNights, 
			chargedReservations.roomRevenueInHotelCurrency AS reservationRevenue, 
					SUM(case when chargedReservations.[classificationID] = 5 -- Booking
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency
						end
					else null
				end) AS iPreferCharge,
			SUM(case when chargedReservations.[classificationID] = 1 -- Booking
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency
						end
					else null
				end) AS bookCharge, 
			SUM(case when chargedReservations.[classificationID] = 2 -- commission
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency						
						end
					else null
				end) AS commCharge, 
			SUM(case when chargedReservations.classificationID = 3 -- surcharges
					then 
						case when chargedReservations.chargeValueInHotelCurrency = 0
							then null
							else chargedReservations.chargeValueInHotelCurrency						
						end
					else null
				end) AS surcharge,
			SUM (chargedReservations.chargeValueInHotelCurrency)  AS totalCharge,
		
			'Non Stay Point Awards' AS itemGrouping,
			chargedReservations.billableDate AS billingDate,
			uu.UserUpdatedName AS CreateUser,
			CAST(tdr.Points_Earned AS int) AS PointsAwarded,
			tdr.Remarks AS [Description],
			chargedReservations.arrivalDate AS TransactionDate
		FROM @GP_SOPNumber_ItemCodes GPSopIC
			INNER JOIN #CHARGES chargedReservations ON chargedReservations.itemCode = GPSopIC.ITEMNMBR 
			LEFT OUTER JOIN Reservations.[billing].[MostRecentTransactions] MRT WITH(NOLOCK) ON chargedReservations.confirmationNumber = MRT.confirmationNumber 
			LEFT OUTER JOIN loyalty.dbo.Customer_Profile_Import cpi ON chargedReservations.loyaltyNumber = cpi.iPrefer_Number
			LEFT OUTER JOIN loyalty.dbo.TransactionDetailedReport tdr ON chargedReservations.confirmationNumber = tdr.Booking_ID
			LEFT OUTER JOIN loyalty.dbo.PointActivity pa ON tdr.PointActivityID = pa.PointActivityID
			LEFT OUTER JOIN loyalty.dbo.UserUpdated uu ON uu.UserUpdatedID = pa.ActivityCauseUserID
		WHERE chargedReservations.classificationID = 5
			AND chargedReservations.chargeValueInUSD <> 0.00
			AND chargedReservations.itemCode = 'IPREFERNONSTAY_I' 
		GROUP BY chargedReservations.hotelCurrencyCode, --MRT.channel, 
			chargedReservations.arrivalDate, MRT.guestLastName, 
			MRT.guestFirstName, chargedReservations.confirmationNumber, MRT.confirmationDate,
			MRT.IATANumber, MRT.rateTypeCode, chargedReservations.roomNights, chargedReservations.roomRevenueInHotelCurrency, chargedReservations.billableDate, chargedReservations.loyaltyNumber,
			 cpi.Last_Name,cpi.First_Name, 		uu.UserUpdatedName, CAST(tdr.Points_Earned AS int) ,tdr.Remarks
	) AS main
	ORDER BY itemGrouping, arrivalDate, guestLastName, confirmationNumber
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [archive].[ArchiveChanges]'
GO
CREATE   PROCEDURE [archive].[ArchiveChanges]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @errorMessage nvarchar(2000) = NULL;
	
	DECLARE @FourYearsAgo date;
	SET @FourYearsAgo = CONVERT(varchar(20),YEAR(DATEADD(YEAR,-4,GETDATE()))) + '-01-01'


	BEGIN TRY
		INSERT INTO [archive].[Charges]([chargeID],[runID],[clauseID],[billingRuleID],[classificationID],[transactionSourceID],[transactionKey],[confirmationNumber],[hotelCode],[collectionCode],[clauseName],[billableDate],[arrivalDate],[roomNights],[hotelCurrencyCode],[exchangeDate],[chargeValueInHotelCurrency],[roomRevenueInHotelCurrency],[chargeValueInUSD],[roomRevenueInUSD],[itemCode],[gpSiteID],[dateCalculated],[sopNumber],[invoiceDate],[loyaltyNumber],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[deleted_sopNumber])
		SELECT [chargeID],[runID],[clauseID],[billingRuleID],[classificationID],[transactionSourceID],[transactionKey],[confirmationNumber],[hotelCode],[collectionCode],[clauseName],[billableDate],[arrivalDate],[roomNights],[hotelCurrencyCode],[exchangeDate],[chargeValueInHotelCurrency],[roomRevenueInHotelCurrency],[chargeValueInUSD],[roomRevenueInUSD],[itemCode],[gpSiteID],[dateCalculated],[sopNumber],[invoiceDate],[loyaltyNumber],[LoyaltyNumberValidated],[LoyaltyNumberTagged],[deleted_sopNumber]
		FROM dbo.Charges
		WHERE [invoiceDate] < @FourYearsAgo

		DELETE dbo.Charges WHERE [invoiceDate] < @FourYearsAgo
	END TRY
	BEGIN CATCH
		SELECT @errorMessage =  ERROR_PROCEDURE() + ': ' + ERROR_MESSAGE()

		RAISERROR(@errorMessage,16,1)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;
	END CATCH

	IF @@TRANCOUNT > 0  
		COMMIT TRANSACTION;
END


GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Billy_Nightly_Job_Master]'
GO






/* =================================================================================================
-- Author:		Ti Yao
-- Create date: 2020-04-20
-- Description:	Billy Nightly Job
Changes:

Name				Date		Description
-------------------------------------------------
-- ================================================================================================*/

ALTER PROCEDURE [dbo].[Billy_Nightly_Job_Master]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @QueueID int,
			@CreateDate datetime = GETDATE(),
			@count int,
			@QueueStepCount int,
			@QueueStepCount0 int,
			@invoicedate datetime,
			@lastbilleddate datetime,
			@clauselastbilleddate datetime

	DECLARE @QueueStepQueueID TABLE(QueueID int NOT NULL)

	SELECT @clauselastbilleddate = MAX(lastBilled) FROM dbo.Clauses 

	SELECT @invoicedate = MAX(invoiceDate) FROM dbo.Charges 

	SELECT @lastbilleddate = DATEADD(MONTH, DATEDIFF(MONTH, -1, @invoicedate)-1, -3)

	IF(@clauselastbilleddate <> @invoicedate)
	BEGIN
		UPDATE dbo.Clauses
			SET lastBilled = @lastbilleddate
	END

	SELECT @count = COUNT(*), @QueueID = MAX(QueueID)
	FROM ETL.dbo.Queue
	WHERE [Application] = 'NightlyBilly'
		AND CAST(CreateDate AS DATE) = CAST(@CreateDate AS DATE)
		AND QueueStatus = 0

	--SELECT @QueueStepCount = COUNT(*) FROM ETL.dbo.QueueStep WHERE QueueStepStatus IN (1,5) AND stepName = 'Reservations'
	SELECT @QueueStepCount = COUNT(*) FROM ETL.dbo.[Queue] WHERE QueueStatus IN(1,5) AND Application = 'Populate MRT'

	IF (@count <> 0 AND @QueueStepCount = 0 )
	BEGIN

		UPDATE ETL.dbo.Queue
			SET ImportStarted = @CreateDate,
				QueueStatus = 1
		WHERE QueueID = @QueueID

		--SELECT @QueueStepCount0 = COUNT(*) FROM ETL.dbo.QueueStep WHERE QueueStepStatus = 0 AND stepName = 'Reservations'

		--IF( @QueueStepCount0 > 0)
		--BEGIN
		--	INSERT INTO @QueueStepQueueID (QueueID)
		--	SELECT QueueID FROM ETL.dbo.QueueStep WHERE QueueStepStatus = 0 AND stepName = 'Reservations'

		--	UPDATE qs
		--	SET qs.QueueStepStatus = 5
		--	FROM ETL.dbo.QueueStep qs
		--	INNER JOIN @QueueStepQueueID qsq ON qs.QueueID = qsq.QueueID

		--END


		EXEC [work].[Populate_LocalMasterTables_Reservation]

		DECLARE @runType bit = 1,
				@startDate date = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0), --first day of previous month
				@endDate date = DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 4, -1), --last day of 3 years later
				@hotelCode nvarchar(20) = NULL,
				@confirmationNumber nvarchar(20) = NULL

		EXECUTE [work].[RunCalculator] 
		 @runType
		,@startDate
		,@endDate
		,@hotelCode
		,@confirmationNumber,
		0, --debug
		1, --log Process
		@invoicedate;


		DECLARE @emailbody nvarchar(max) 

		SET @emailbody = 'The Reservation Billing Calculation has completed successfully and the Error Report can be found at the following link:

		http://Reports/Reports/Pages/Report.aspx?ItemPath=%2fReservation+Billing%2fBilly+Calculation+Errors

		Please contact IT Development (itdevelopment@preferredhotels.com) if there are any issues with the report.'

		EXEC msdb.dbo.sp_send_dbmail
			@profile_name = 'notify',
			@recipients = 'BillingNotification@preferredhotels.com',
			@subject = 'Reservation Billing Error Report' ,
			@body = @emailbody

		DELETE dbo.Charges
		WHERE HotelCode = 'CVGSH'
			AND confirmationNumber LIKE '82235%'
			AND sopNumber IS NULL

		--IF( @QueueStepCount0 > 0)
		--BEGIN
		--	UPDATE qs
		--	SET qs.QueueStepStatus = 0
		--	FROM ETL.dbo.QueueStep qs
		--	INNER JOIN @QueueStepQueueID qsq ON qs.QueueID = qsq.QueueID
		--END
		
		EXEC [ETL].[dbo].[EpsilonExport_LoadQueue] 'EpsilonExportPointTransaction'

		EXEC ETL.[dbo].[ftp_UpdateQueueStatus] @QueueID, 2,'SUCCESS',0
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_clauseID_IC_Others] on [dbo].[BillingRules]'
GO
CREATE NONCLUSTERED INDEX [IX_clauseID_IC_Others] ON [dbo].[BillingRules] ([clauseID]) INCLUDE ([endDate], [startDate], [thresholdMinimum])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_classificationID_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_classificationID_IC_Others] ON [dbo].[Charges] ([classificationID]) INCLUDE ([confirmationNumber], [hotelCode], [chargeValueInUSD], [transactionSourceID], [transactionKey], [clauseName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_classificationID_confirmationNumber] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_classificationID_confirmationNumber] ON [dbo].[Charges] ([classificationID], [confirmationNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_classificationID_itemCode_sopNumber_chargeValueInUSD_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_classificationID_itemCode_sopNumber_chargeValueInUSD_IC_Others] ON [dbo].[Charges] ([classificationID], [itemCode], [sopNumber], [chargeValueInUSD]) INCLUDE ([confirmationNumber], [billableDate], [arrivalDate], [roomNights], [hotelCurrencyCode], [chargeValueInHotelCurrency], [roomRevenueInHotelCurrency], [loyaltyNumber])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_classificationID_transactionSourceID_billableDate_arrivalDate_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_classificationID_transactionSourceID_billableDate_arrivalDate_IC_Others] ON [dbo].[Charges] ([classificationID], [transactionSourceID], [billableDate], [arrivalDate]) INCLUDE ([billingRuleID], [transactionKey], [confirmationNumber], [hotelCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_confirmationNumber_sopNumber_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_confirmationNumber_sopNumber_IC_Others] ON [dbo].[Charges] ([confirmationNumber], [sopNumber]) INCLUDE ([transactionSourceID], [transactionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_hotelCode_sopNumber_IC_transactionSourceID_transactionKey] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_hotelCode_sopNumber_IC_transactionSourceID_transactionKey] ON [dbo].[Charges] ([hotelCode], [sopNumber]) INCLUDE ([transactionSourceID], [transactionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_runID] ON [dbo].[Charges] ([runID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID_classificationID_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_runID_classificationID_IC_Others] ON [dbo].[Charges] ([runID], [classificationID]) INCLUDE ([transactionSourceID], [transactionKey], [chargeValueInHotelCurrency], [chargeValueInUSD])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID_sopNumber_IC_clauseID_billingRuleID_transactionSourceID_transactionKey] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_runID_sopNumber_IC_clauseID_billingRuleID_transactionSourceID_transactionKey] ON [dbo].[Charges] ([runID], [sopNumber]) INCLUDE ([clauseID], [billingRuleID], [transactionSourceID], [transactionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID_transactionSourceID_sopNumber_billableDate] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_runID_transactionSourceID_sopNumber_billableDate] ON [dbo].[Charges] ([runID], [transactionSourceID], [sopNumber], [billableDate])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_sopNumber_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_sopNumber_IC_Others] ON [dbo].[Charges] ([sopNumber]) INCLUDE ([classificationID], [confirmationNumber], [billableDate], [arrivalDate], [roomNights], [hotelCurrencyCode], [chargeValueInHotelCurrency], [roomRevenueInHotelCurrency], [chargeValueInUSD], [itemCode], [loyaltyNumber], [transactionSourceID], [transactionKey])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_transactionKey_sopNumber_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_transactionKey_sopNumber_IC_Others] ON [dbo].[Charges] ([transactionKey], [sopNumber]) INCLUDE ([transactionSourceID], [billableDate], [clauseID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_transactionSourceID_IC_Others] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_transactionSourceID_IC_Others] ON [dbo].[Charges] ([transactionSourceID]) INCLUDE ([billingRuleID], [classificationID], [transactionKey], [hotelCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_transactionSourceID_sopNumber_billableDate_IC_confirmationNumber_hotelCode] on [dbo].[Charges]'
GO
CREATE NONCLUSTERED INDEX [IX_transactionSourceID_sopNumber_billableDate_IC_confirmationNumber_hotelCode] ON [dbo].[Charges] ([transactionSourceID], [sopNumber], [billableDate]) INCLUDE ([confirmationNumber], [hotelCode])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IX_runID_transactionSourceID_transactionKey_IC_Others] on [work].[MrtForCalc_Clauses]'
GO
CREATE NONCLUSTERED INDEX [IX_runID_transactionSourceID_transactionKey_IC_Others] ON [work].[MrtForCalc_Clauses] ([runID], [transactionSourceID], [transactionKey]) INCLUDE ([ClauseName], [CriteriaID], [RateCategoryCode], [RateCode], [TravelAgentGroupID], [ClauseID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
