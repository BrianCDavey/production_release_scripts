/*
Run this script on:

        wus2-data-dp-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/1/2024 3:58:52 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SwoogoEvent_Core_ErrorChecking]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-12-28
-- Description:	This procedure is a part of Swoogo import process loading data from ETL db to Core DB
-- Prototype: EXEC [dbo].[SwoogoEvent_Core_ErrorChecking] 3
-- History: 2020-08-01 Ti Yao Initial Creation
-- =============================================

ALTER PROCEDURE [dbo].[SwoogoEvent_Core_ErrorChecking]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @6monthdate date = CAST(DATEADD(month, -6, GETDATE()) AS DATE)

	--Insert Deleted Event
	INSERT INTO Core.Swoogo.weboomAccountingEvents
		([QueueID], [financeType], [financeEventCode], [eventType], [eventDate], [eventLocation], [timeLoaded], [eventId], [isBad], [errorDescription],[isDeleted],[endDate])
	SELECT 
		0 AS [QueueID], 
		cdw.[financeType], 
		cdw.[financeEventCode], 
		cdw.[eventType], 
		cdw.[eventDate], 
		cdw.[eventLocation], 
		cdw.[timeLoaded], 
		cdw.[eventId], 
		1 AS [isBad], 
		'Event deleted - event and all registrations have been removed from records. Please contact Bruce to confirm.',
		1 AS [isDeleted]
		,cdw.[endDate] AS [endDate]
	FROM Core.dbo.weboomAccountingEvents cdw
	LEFT JOIN Core.Swoogo.weboomAccountingEvents csw 
		ON cdw.financeEventCode = csw.financeEventCode AND csw.QueueID = @QueueID
	WHERE (cdw.isSwoogo = 1 OR cdw.isSwoogo IS NULL) AND csw.financeEventCode IS NULL
	AND cdw.endDate >= @6monthdate

	--Insert Deleted Registration
	INSERT INTO Core.Swoogo.weboomAccountingRegistrations
	([QueueID], [accountCode], [billingAmount], [billingAmountCurrency], [invoiceDueDate], [serviceDate], [financeEventCode], [marketingInclusionFlag], [marketingInclusionExplanation], [registrationStatus], [confirmationNumber], [attendingGuestFullName], [eventType], [eventDate], [eventLocation], [timeLoaded], [isBad], [errorDescription],[isDeleted])
	SELECT 
		0 AS [QueueID], 
		cdw.[accountCode], 
		cdw.[billingAmount], 
		cdw.[billingAmountCurrency], 
		cdw.[invoiceDueDate], 
		cdw.[serviceDate], 
		cdw.[financeEventCode], 
		cdw.[marketingInclusionFlag], 
		cdw.[marketingInclusionExplanation], 
		cdw.[registrationStatus], 
		cdw.[confirmationNumber], 
		cdw.[attendingGuestFullName], 
		cdw.[eventType], 
		cdw.[eventDate], 
		cdw.[eventLocation], 
		cdw.[timeLoaded], 
		1 AS [isBad], 
		'Event deleted - event and all registrations have been removed from records. Please contact Bruce to confirm.',
		1 AS [isDeleted]
	FROM Core.dbo.weboomAccountingRegistrations cdw
	INNER JOIN Core.Swoogo.weboomAccountingEvents csw 
		ON cdw.financeEventCode = csw.financeEventCode AND csw.isDeleted = 1

	----Deleted Registration
	--DELETE cdw
	--FROM Core.dbo.weboomAccountingRegistrations cdw
	--INNER JOIN Core.Swoogo.weboomAccountingRegistrations csw
	--ON cdw.financeEventCode = csw.financeEventCode
	--AND csw.isDeleted = 1 

	----Deleted Event
	--DELETE cdw
	--FROM Core.dbo.weboomAccountingEvents cdw
	--INNER JOIN Core.Swoogo.weboomAccountingEvents csw
	--ON cdw.financeEventCode = csw.financeEventCode
	--AND csw.isDeleted = 1 AND (cdw.isSwoogo = 1 OR cdw.isSwoogo IS NULL)


	--CHECK NOT NULL COLUMN
	UPDATE Core.Swoogo.weboomAccountingEvents 
	SET  isBad = 1
		,errorDescription = 'NOT NULL COLUMN: ' + 
			CASE WHEN [financeType] IS NULL THEN '[financeType]'
				 WHEN [financeEventCode] IS NULL THEN '[financeEventCode]'
				 WHEN [eventType] IS NULL THEN '[eventType]'
				 WHEN [eventDate] IS NULL THEN '[eventDate]'
				 WHEN [eventLocation] IS NULL THEN '[eventLocation]'
				 WHEN [timeLoaded] IS NULL THEN '[timeLoaded]'
				ELSE '' END
	WHERE QueueID = @QueueID
	AND (
	[financeType] IS NULL OR
	[financeEventCode] IS NULL OR 
	[eventType] IS NULL OR 
	[eventDate] IS NULL OR 
	[eventLocation] IS NULL OR 
	[timeLoaded] IS NULL  
	)

	--CHECK DUPLICATE COLUMN
	UPDATE Core.Swoogo.weboomAccountingEvents 
	SET  isBad = 1
		,errorDescription = ISNULL(errorDescription,'') + ' DUPLICATE COLUMN: [financeEventCode]' 
	WHERE QueueID = @QueueID
	AND financeEventCode IN (
		SELECT financeEventCode
		FROM Core.Swoogo.weboomAccountingEvents
		WHERE QueueID = @QueueID
		GROUP BY financeEventCode
		HAVING COUNT(*) > 1
	)


	--PREVENT OVERFLOW
	UPDATE Core.Swoogo.weboomAccountingEvents 
	SET  isBad = 1
		,errorDescription = ISNULL(errorDescription,'') + ' OVERFLOW COLUMN: [financeEventCode]' 
	WHERE QueueID = @QueueID
	AND LEN(financeEventCode) > 80


	--CHECK EventType FinanceType COMBINATION
	UPDATE swae
	SET  isBad = 1
		,errorDescription = ISNULL(errorDescription,'') + ' EventType & FinanceType combination not found for billing, please contact Bruce' 
	FROM Core.Swoogo.weboomAccountingEvents swae
	LEFT JOIN core.dbo.weboomItemCode wic ON swae.financeType = wic.financeType AND swae.eventType = wic.eventType
	WHERE swae.QueueID = @QueueID
	AND wic.eventType IS NULL
	AND swae.eventType <> 'Complimentary Event'


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
