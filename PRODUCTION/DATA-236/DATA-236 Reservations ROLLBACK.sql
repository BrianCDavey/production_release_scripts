/*
Run this script on:

        chi-lt-00032377.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/22/2019 10:47:45 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [FK_dbo_Transactions_TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UX_TransactionDetailID_IC_Others] from [dbo].[TransactionDetail]'
GO
DROP INDEX [UX_TransactionDetailID_IC_Others] ON [dbo].[TransactionDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransactionDetail]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TransactionDetail] DROP
COLUMN [billingDescription]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UX_TransactionDetailID_IC_Others] on [dbo].[TransactionDetail]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_TransactionDetailID_IC_Others] ON [dbo].[TransactionDetail] ([TransactionDetailID]) INCLUDE ([arrivalDate], [commisionPercent], [currency], [departureDate], [LoyaltyNumberTagged], [LoyaltyNumberValidated], [nights], [optIn], [reservationRevenue], [rooms])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionDetail]'
GO

ALTER PROCEDURE [dbo].[Populate_TransactionDetail]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'

	MERGE INTO [dbo].[TransactionDetail] AS tgt
	USING
	(
		SELECT [synXisID],[synXisTransExID],[openHospID],[confirmationNumber],CASE WHEN [nights] = 0 THEN 1 ELSE [nights] END AS [nights],
			[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
			[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
			[departureDOW],[optIn],[IsPrimaryGuest],[DataSourceID]
		FROM
		(
			SELECT td.[TransactionDetailID] AS [synXisID],x.TransactionsExtendedID AS [synXisTransExID],NULL AS [openHospID],t.[confirmationNumber],[nights],
					[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
					[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
					[departureDOW],x.[optIn],[IsPrimaryGuest],@SynXis_DS AS [DataSourceID]
			FROM synxis.TransactionDetail td
				INNER JOIN synxis.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN
					(
						SELECT DISTINCT t.TransactionID,te.TransactionsExtendedID,
									CASE te.optIn WHEN 'Y' THEN 1 ELSE 0 END AS optIn,
									te.IsPrimaryGuest
						FROM synxis.TransactionsExtended te
							INNER JOIN synxis.Transactions t ON t.TransactionsExtendedID = te.TransactionsExtendedID
					) x ON x.TransactionID = mrt.TransactionID
			WHERE t.QueueID = @QueueID

				UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [synXisTransExID],td.[TransactionDetailID] AS [openHospID],
				CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[nights],
				NULL AS [averageDailyRate],[rooms],[reservationRevenue],[currency],NULL AS [totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],NULL AS [commisionPercent],[arrivalDate],[departureDate],NULL AS [bookingLeadTime],NULL AS [arrivalDOW],
				NULL AS [departureDOW],NULL AS [optIn],NULL AS [IsPrimaryGuest],@OpenHosp_DS AS [DataSourceID]
			FROM openHosp.TransactionDetail td
				INNER JOIN openHosp.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] ELSE NULL END,
				[synXisTransExID] = src.[synXisTransExID],
				[nights] = src.[nights],
				[averageDailyRate] = src.[averageDailyRate],
				[rooms] = src.[rooms],
				[reservationRevenue] = src.[reservationRevenue],
				[currency] = src.[currency],
				[totalPackageRevenue] = src.[totalPackageRevenue],
				[totalGuestCount] = src.[totalGuestCount],
				[adultCount] = src.[adultCount],
				[childrenCount] = src.[childrenCount],
				[commisionPercent] = src.[commisionPercent],
				[arrivalDate] = src.[arrivalDate],
				[departureDate] = src.[departureDate],
				[bookingLeadTime] = src.[bookingLeadTime],
				[arrivalDOW] = src.[arrivalDOW],
				[departureDOW] = src.[departureDOW],
				[optIn] = src.[optIn],
				[IsPrimaryGuest] = src.[IsPrimaryGuest]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],
				[synXisTransExID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[IsPrimaryGuest])
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] ELSE NULL END,
				[synXisTransExID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[IsPrimaryGuest])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[Transactions]'
GO
ALTER TABLE [dbo].[Transactions] ADD CONSTRAINT [FK_dbo_Transactions_TransactionDetail] FOREIGN KEY ([TransactionDetailID]) REFERENCES [dbo].[TransactionDetail] ([TransactionDetailID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
