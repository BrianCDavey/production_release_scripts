USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/28/2020 1:28:26 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_DailyPointTransactionsExport]'
GO



ALTER PROCEDURE [dbo].[Epsilon_DailyPointTransactionsExport]
@QueueID int,
 @AsOfDay date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	--DECLARE @AsOfDay date = getdate()
	--DECLARE @QueueID int = 10056

	--populate small table
;WITH latestSent AS (
		SELECT erl.EpsilonPointTransactionLogID,QueueID, erl.ConfirmationNumber, erl.[DataSourceID] ,erl.[TransactionID] ,erl.[SourceKey] ,erl.[transactionTimeStamp],erl.[actionType],erl.[DateSent]
		, erl.HotelCode, erl.ArrivalDate, erl.DepartureDate, erl.[status]
		FROM ETL..EpsilonPointTransactionLog erl
		INNER JOIN 
		(
			SELECT MAX(EpsilonPointTransactionLogID) AS EpsilonPointTransactionLogID, DataSourceID, confirmationNumber
			FROM ETL..EpsilonPointTransactionLog 
			WHERE QueueID < @QueueID
			GROUP BY DataSourceID, confirmationNumber
		) FindMax
		ON erl.EpsilonPointTransactionLogID = FindMax.EpsilonPointTransactionLogID
		
		) 

INSERT INTO ETL.[dbo].[EpsilonPointTransactionLog]
           ([QueueID] ,[ConfirmationNumber],[DataSourceID],[TransactionID]
		   ,[SourceKey],[transactionTimeStamp],[actionType],[DateSent],[HotelCode],[ArrivalDate],[DepartureDate],[status])
SELECT DISTINCT @QueueID,t.[ConfirmationNumber],t.[DataSourceID],t.[TransactionID]
		   ,t.[SourceKey],ISNULL(t.[transactionTimeStamp],ts.confirmationDate),act.[actionType],GETDATE(),hh.hotelCode,td.arrivalDate,td.[DepartureDate], ts.[status]
FROM Reservations.dbo.Transactions t
	JOIN Reservations.dbo.TransactionDetail td
		ON t.TransactionDetailID = td.TransactionDetailID
	JOIN Reservations.dbo.TransactionStatus ts
		ON t.TransactionStatusID = ts.TransactionStatusID
	JOIN Reservations.dbo.ActionType act
		ON ts.ActionTypeID = act.ActionTypeID
	JOIN Reservations.dbo.hotel h
		ON t.HotelID = h.HotelID
	JOIN Hotels.dbo.hotel hh
		ON h.Hotel_hotelID = hh.HotelID
	JOIN Reservations.dbo.RateCode rc
		ON t.RateCodeID = rc.RateCodeID
	LEFT JOIN Reservations.dbo.PromoCode promo
		ON t.PromoCodeID = promo.PromoCodeID
	LEFT JOIN dbo.vw_CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
	LEFT JOIN ReservationBilling.dbo.Charges C ON c.confirmationNumber = t.confirmationNumber AND c.transactionSourceID = t.DataSourceID AND c.sopNumber IS NOT NULL --do not send something that is billed already.
	LEFT JOIN ReservationBilling.dbo.Charges ch ON t.confirmationNumber = ch.confirmationNumber AND ch.classificationID = 5 AND ch.sopNumber IS NULL AND ch.transactionSourceID <> 3--filter Billy calculated for iprefer charge. Remove manual point award.
	LEFT JOIN latestSent --checking for the reservation in general
		ON t.DataSourceID = latestSent.DataSourceID AND t.confirmationNumber = latestSent.confirmationNumber
	LEFT JOIN ETL.dbo.EpsilonPointTransactionLog erl
		ON t.TransactionID = erl.TransactionID AND t.sourceKey = erl.SourceKey

	WHERE td.arrivalDate <= @AsOfDay  --The arrivaldate is today or earlier 
	AND td.arrivalDate >= '2019-12-01' --previous arrival would have been billed
	AND c.chargeID IS NULL --if a SOP Number exists on a charge table. Once we have billed the point transaction, we should never send a modify or cancel to Epsilon.
	AND (ch.chargeID IS NOT NULL OR (ts.[status] = 'Cancelled' AND td.LoyaltyNumberValidated = 1)) -- Billy calculated with Iprefer + Void Reservation with Iprefer
	AND erl.transactionID IS NULL --do not send the same transaction again
	AND (( ts.[status] = 'Cancelled' AND (latestSent.[status] <> 'Cancelled' AND latestSent.[status] IS NOT NULL )) OR ts.[status] <> 'Cancelled') --remove NX and cancel only reservation
	AND t.confirmationNumber IS NOT NULL --remove invalid confirmation
	AND t.confirmationNumber <> '' --remove invalid confirmation

;WITH iopt AS (
  SELECT HotelID
      ,ISNULL([StartDate],'1900-01-01') as startDate
      ,ISNULL([EndDate],'9999-09-09') as endDate
  FROM [Hotels].[dbo].[Hotel_ProTools] hpt
  WHERE ProToolsID = 14
)
, rd AS (
  	SELECT MAX(billableDate) AS billableDate, confirmationNumber
	FROM ReservationBilling.dbo.Charges ch 
	WHERE ch.classificationID = 5 AND ch.sopNumber IS NULL AND ch.transactionSourceID <> 3--filter Billy calculated for iprefer charge. Remove manual point award.
	GROUP BY confirmationNumber
)
, jed AS (
		SELECT 
			t.confirmationNumber
			, 
			REPLACE(
			CONCAT_WS('ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA',
				(
					SELECT
						ISNULL(t2.TransactionID,'') as PhgTransactionId
						, ISNULL(t2.confirmationNumber,'') as ConfirmationNumber
						, ISNULL(hh.SynXisID, hh.OpenHospID) as CrsHotelCode
						, ISNULL(xe.toUSD,0) as UsdExchangeRate
						, ISNULL(xe.rateDate,'9999-09-09') as UsdExchangeDate
						, CASE 
							WHEN d.SourceName = 'SynXis' THEN 'Synxis' 
							WHEN d.SourceName = 'Open Hospitality' THEN 'Open Hospitality' 
							ELSE 'ERROR WITH CRS MAPPING' 
						  END as CrsSource
						, ISNULL(pc.PH_Channel,'') as BookingChannel
						, ISNULL(pbs.PH_ChannelID,0) as BookingChannelID
						, ISNULL(rc.RateCode,'') as RateCode
						, ISNULL(rc.RateName,'') as RateName
						, ISNULL(rcat.rateCategoryCode,'') as RateCategory
						, ISNULL(corp.corporationCode,'') as CorpCode
						, ISNULL(rt.roomTypeCode,'') as RoomCode
						, ISNULL(rt.roomTypeName,'') as RoomName
						, '' as RoomCategory
						, ISNULL(td.rooms,0) as RoomCount
						, ISNULL(td.totalGuestCount,0) as GuestCount
						, ISNULL(td.childrenCount,0) as ChildCount
						, ISNULL(t.itineraryNumber,0) as IteneraryNumber
						, ISNULL(i.IATANumber,'') as IataNumber
						, '' as PsuedoCityCode
						, ISNULL(ta.Name,'') as TravelAgencyName
						, '' as TravelAgentFirstName
						, '' as TravelAgentLastName
						, LOWER(ISNULL(ta.Email,'')) as TravelAgentEmailAddress
						, ISNULL(ts.creditCardType,'') as CreditCardType
						, ISNULL(ts.cancellationNumber,'') as CancellationNumber
						, ISNULL(ts.cancellationDate,'') as CancellationDate
						, ISNULL(g.FirstName,'') as GuestFirstName
						, ISNULL(g.LastName,'') as GuestLastName
						, LOWER(ISNULL(ge.emailAddress,'')) as GuestEmail
						, CASE WHEN iopt.HotelID IS NULL THEN '' ELSE td.optIn END as LoyaltyOptIn
						, CASE WHEN iopt.HotelID IS NULL THEN td.optIn ELSE '' END as MarketingOptIn
						, ISNULL(rd.billableDate,'') as ReportDate
						, td.reservationRevenue as OriginalReservationRevenue
						, td.currency as OriginalReservationCurrencyCode
					FROM Reservations.dbo.Transactions t2
						LEFT JOIN Reservations.dbo.TransactionDetail td
							ON t2.TransactionDetailID = td.TransactionDetailID
						LEFT JOIN Reservations.dbo.TransactionStatus ts
							ON t2.TransactionStatusID = ts.TransactionStatusID
						LEFT JOIN Reservations.dbo.hotel h
							ON t2.HotelID = h.HotelID
						LEFT JOIN Hotels.dbo.Hotel hh
							ON h.Hotel_hotelID = hh.HotelID
						LEFT JOIN CurrencyRates.dbo.dailyRates xe
							ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
							AND td.currency = xe.code						
						LEFT JOIN Reservations.dbo.PH_BookingSource pbs
							ON t2.PH_BookingSourceID = pbs.PH_BookingSourceID
						LEFT JOIN Reservations.dbo.PH_Channel pc
							ON pbs.PH_ChannelID = pc.PH_ChannelID
						LEFT JOIN Reservations.dbo.RateCode rc
							ON t2.RateCodeID = rc.RateCodeID						
						LEFT JOIN Reservations.dbo.RateCategory rcat
							ON t2.RateCategoryID = rcat.RateCategoryID
						LEFT JOIN Reservations.dbo.CorporateCode corp
							ON t2.CorporateCodeID = corp.CorporateCodeID
						LEFT JOIN Reservations.dbo.RoomType rt
							ON t2.RoomTypeID = rt.RoomTypeID
						LEFT JOIN Reservations.dbo.IATANumber i
							ON t2.IATANumberID = i.IATANumberID
						LEFT JOIN Reservations.dbo.TravelAgent ta
							ON t2.TravelAgentID = ta.TravelAgentID
						LEFT JOIN Reservations.dbo.Guest g
							ON t2.GuestID = g.GuestID
						LEFT JOIN Reservations.dbo.Guest_EmailAddress ge
							ON g.Guest_EmailAddressID = ge.Guest_EmailAddressID
						LEFT JOIN iopt
							ON h.Hotel_hotelID = iopt.hotelId
							AND ts.confirmationDate BETWEEN iopt.startDate AND iopt.endDate
						LEFT JOIN Reservations.authority.DataSource d
							ON d.DataSourceID = t.DataSourceID
						LEFT JOIN rd 
							ON t2.confirmationNumber = rd.confirmationNumber 
					WHERE t.confirmationNumber = t2.confirmationNumber
					FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
				)
				,(
				SELECT * FROM
					(SELECT 
						ISNULL(tag.id,0) as TravelAgencyGroupID
						, ISNULL(tag.name,'') as TravelAgencyGroupName
					FROM Reservations.dbo.Transactions t3
						JOIN Reservations.dbo.TransactionStatus ts
							ON t3.TransactionStatusID = ts.TransactionStatusID
						JOIN Reservations.dbo.IATANumber i
							ON t3.IATANumberID = i.IATANumberID
						JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag
							ON i.IATANumber = taitag.travelAgentId
							AND ts.confirmationDate BETWEEN taitag.startDate and taitag.endDate
						JOIN Core.dbo.travelAgentGroups tag
							ON taitag.travelAgentGroupID = tag.id
					WHERE t.confirmationNumber = t3.confirmationNumber
					UNION ALL
					SELECT 
						0 as TravelAgencyGroupID
						, '' as TravelAgencyGroupName
					WHERE NOT EXISTS (SELECT 1
					FROM Reservations.dbo.Transactions t3
						JOIN Reservations.dbo.TransactionStatus ts
							ON t3.TransactionStatusID = ts.TransactionStatusID
						JOIN Reservations.dbo.IATANumber i
							ON t3.IATANumberID = i.IATANumberID
						JOIN Core.dbo.travelAgentIds_travelAgentGroups taitag
							ON i.IATANumber = taitag.travelAgentId
							AND ts.confirmationDate BETWEEN taitag.startDate and taitag.endDate
						JOIN Core.dbo.travelAgentGroups tag
							ON taitag.travelAgentGroupID = tag.id
					WHERE t.confirmationNumber = t3.confirmationNumber
					)) as sub 
					FOR JSON PATH, ROOT('TravelAgencyGroups')
				)



				,(
				SELECT * FROM
					(SELECT DISTINCT ISNULL(cg.criteriaGroupID,0) as BookingSourceID
								, ISNULL(cg.criteriaGroupName,'') as BookingSourceName
						FROM Reservations.dbo.Transactions t4
						JOIN Reservations.dbo.TransactionDetail td
							ON t4.TransactionDetailID = td.TransactionDetailID
						JOIN Reservations.dbo.CRS_BookingSource cbs
							ON t4.CRS_BookingSourceID = cbs.BookingSourceID
						JOIN Reservations.dbo.CRS_Channel cc
							ON cbs.ChannelID = cc.ChannelID
						JOIN Reservations.dbo.CRS_SecondarySource c2s
							ON cbs.SecondarySourceID = c2s.SecondarySourceID
						JOIN Reservations.dbo.CRS_SubSource css
							ON cbs.SubSourceID = css.SubSourceID
						LEFT JOIN Reservations.dbo.ibeSource ibs --change left join
							ON cbs.ibeSourceNameID = ibs.ibeSourceID
						LEFT JOIN Reservations.authority.ibeSource aibs --change left join
							ON ibs.auth_ibeSourceID = aibs.ibeSourceID
						LEFT JOIN ReservationBilling.dbo.Templates rbt  --change left join
							ON aibs.ibeSourceName = rbt.xbeTemplateName
						LEFT JOIN Reservations.dbo.CROCode cro  --change left join
							ON cbs.CROCodeID = cro.CROCodeID
						LEFT JOIN Reservations.authority.CRO_Code acro --change left join
							ON cro.auth_CRO_CodeID = acro.CRO_CodeID
						LEFT JOIN ReservationBilling.dbo.CROCodes rbc --change left join
							ON acro.CRO_Code = rbc.croCode
						JOIN [ReservationBilling].[dbo].[Charges] ch
							ON t4.confirmationNumber = ch.confirmationNumber
							AND ch.classificationID = 5
						JOIN ReservationBilling.dbo.Clauses cl
							ON ch.clauseID = cl.clauseID
						JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg
							ON cl.clauseID = cicg.clauseID
						JOIN ReservationBilling.dbo.CriteriaGroups cg
							ON cicg.criteriaGroupID = cg.criteriaGroupID

					WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5
					UNION ALL
						SELECT 0 as BookingSourceID
							, '' as BookingSourceName
						WHERE NOT EXISTS ( SELECT 1
						FROM Reservations.dbo.Transactions t4
						JOIN Reservations.dbo.TransactionDetail td
							ON t4.TransactionDetailID = td.TransactionDetailID
						JOIN Reservations.dbo.CRS_BookingSource cbs
							ON t4.CRS_BookingSourceID = cbs.BookingSourceID
						JOIN Reservations.dbo.CRS_Channel cc
							ON cbs.ChannelID = cc.ChannelID
						JOIN Reservations.dbo.CRS_SecondarySource c2s
							ON cbs.SecondarySourceID = c2s.SecondarySourceID
						JOIN Reservations.dbo.CRS_SubSource css
							ON cbs.SubSourceID = css.SubSourceID
						LEFT JOIN Reservations.dbo.ibeSource ibs --change left join
							ON cbs.ibeSourceNameID = ibs.ibeSourceID
						LEFT JOIN Reservations.authority.ibeSource aibs --change left join
							ON ibs.auth_ibeSourceID = aibs.ibeSourceID
						LEFT JOIN ReservationBilling.dbo.Templates rbt  --change left join
							ON aibs.ibeSourceName = rbt.xbeTemplateName
						LEFT JOIN Reservations.dbo.CROCode cro  --change left join
							ON cbs.CROCodeID = cro.CROCodeID
						LEFT JOIN Reservations.authority.CRO_Code acro --change left join
							ON cro.auth_CRO_CodeID = acro.CRO_CodeID
						LEFT JOIN ReservationBilling.dbo.CROCodes rbc --change left join
							ON acro.CRO_Code = rbc.croCode
						JOIN [ReservationBilling].[dbo].[Charges] ch
							ON t4.confirmationNumber = ch.confirmationNumber
							AND ch.classificationID = 5
						JOIN ReservationBilling.dbo.Clauses cl
							ON ch.clauseID = cl.clauseID
						JOIN ReservationBilling.dbo.Clauses_IncludeCriteriaGroups cicg
							ON cl.clauseID = cicg.clauseID
						JOIN ReservationBilling.dbo.CriteriaGroups cg
							ON cicg.criteriaGroupID = cg.criteriaGroupID

					WHERE t.confirmationNumber = t4.confirmationNumber
					AND ch.classificationID = 5
					)) as sub 
					FOR JSON PATH, ROOT('BookingSources')
				)
			),'}ATHINGIDONOTWANTTOFINDNATURALLYINTHISDATA{',',')  as jsonExternalData
		FROM Reservations.dbo.Transactions t
			JOIN ETL.dbo.[EpsilonPointTransactionLog] eptl
				ON eptl.TransactionID = t.TransactionID
		WHERE eptl.QueueID = @QueueID
	)

INSERT INTO ETL..EpsilonExportPointTransaction
(QueueID, [01], [TRANSACTION_NUMBER], [ACT_TRANSACTION_ID], [PROFILE_ID], [CARD_NUMBER], [TRANSACTION_TYPE_CODE], [TRANS_DESCRIPTION], [TRANSACTION_DATE], [END_DATE], [TRANSACTION_NET_AMOUNT], [ELIGIBLE_REVENUE], [TAX_AMOUNT], [POST_SALES_ADJUSTMNT_AMT], [SHIPPING_HANDLING], [STATUS], [DEVICE_ID], [DEVICE_USERID], [AUTHORIZATION_CODE], [ORIGINAL_TRANSACTION_NUMBER], [SKIP_MOMENT_ENGINE_IND], [CURRENCY_CODE], [ACCT_SRC_CODE], [SRC_ACCOUNT_NUM], [ACTIVITY_DATE], [CLIENT_FILE_ID], [CLIENT_FILE_REC_NUM], [BRAND_ORG_CODE], [ASSOCIATE_NUM], [GROSS_AMOUNT], [NET_AMT], [PROG_CURR_GROSS_AMT], [TXN_SEQ_NUM], [TXN_SOURCE_CODE], [TXN_SUBTYPE_CODE], [TIER], [TRANS_LOCATION], [blank1], [PROGRAM_CODE], [EXCHANGE_RATE_ID], [GRATUITY], [SRC_CUSTOMER_ID], [DISC_AMT], [TXN_CHANNEL_CODE], [TXN_BUSINESS_DATE], [PROMO_CODE], [BUYER_TYPE_CODE], [RMB_METHOD_CODE], [RMB_VENDOR_CODE], [RMB_DATE], [blank2], [PROG_CURR_NET_AMT], [PROG_CURR_TAX_AMT], [PROG_CURR_POST_SALES_ADJ_AMT], [PROG_CURR_SHIPPING_HANDLING], [PROG_CURR_ELIG_REVENUE], [PROG_CURR_GRATUITY], [TXN_TYPE_CODE], [ORIGINAL_STORE_CODE], [ORIGINAL_TRANSACTION_DATE], [ORIGINAL_TRANSACTION_END_DATE], [SUSPEND_REASON], [SUSPEND_TRANSACTION], [JSON_EXTERNAL_DATA], [POSTING_KEY_CODE], [POSTING_KEY_VALUE])
SELECT
@QueueID,
	'01' as [01],
	t.confirmationNumber AS TRANSACTION_NUMBER,
	NULL AS ACT_TRANSACTION_ID,
	NULL AS PROFILE_ID,
	UPPER(l.loyaltyNumber) AS CARD_NUMBER,
	CASE 
		WHEN ts.[status] = 'Cancelled'  THEN 'VD' 
		ELSE 'PR'   
	END AS TRANSACTION_TYPE_CODE,
	'Transaction' AS TRANS_DESCRIPTION,
	td.arrivalDate AS TRANSACTION_DATE,
	td.departureDate AS END_DATE,
	[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS TRANSACTION_NET_AMOUNT,  --USD?
	NULL AS ELIGIBLE_REVENUE,
	NULL AS TAX_AMOUNT,
	NULL AS POST_SALES_ADJUSTMNT_AMT,
	NULL AS SHIPPING_HANDLING,
	NULL AS STATUS,
	NULL AS DEVICE_ID,
	NULL AS DEVICE_USERID,
	NULL AS AUTHORIZATION_CODE,
	CASE WHEN ts.[status] = 'Cancelled' THEN t.confirmationNumber ELSE NULL END AS ORIGINAL_TRANSACTION_NUMBER,
	'False' AS SKIP_MOMENT_ENGINE_IND,
	'USD' AS CURRENCY_CODE,
	'ALMACCT' AS ACCT_SRC_CODE,
	g.customerID AS SRC_ACCOUNT_NUM,
	CASE WHEN eptl.transactionTimeStamp IS NULL THEN ts.confirmationDate
	ELSE eptl.transactionTimeStamp END AS ACTIVITY_DATE,
	t.QueueID AS CLIENT_FILE_ID,
	NULL AS CLIENT_FILE_REC_NUM,
	'ALM_BRAND' AS BRAND_ORG_CODE,
	NULL AS ASSOCIATE_NUM,
	[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS GROSS_AMOUNT,
	[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS NET_AMT,
	NULL AS PROG_CURR_GROSS_AMT,
	NULL AS TXN_SEQ_NUM,
	'PHG File' AS TXN_SOURCE_CODE,
	CASE 
	WHEN d.SourceName = 'SynXis' THEN 'SYNXIS' 
	WHEN d.SourceName = 'Open Hospitality' THEN 'OPENH' 
		ELSE '' 
	END AS TXN_SUBTYPE_CODE,
	NULL AS TIER,
	hh.HotelCode AS TRANS_LOCATION,
	NULL AS [blank1], --intentionally left blank per spec
	'PHG' AS PROGRAM_CODE,
	NULL AS EXCHANGE_RATE_ID,
	NULL AS GRATUITY,
	NULL AS SRC_CUSTOMER_ID,
	NULL AS DISC_AMT,
	NULL AS TXN_CHANNEL_CODE,
	ts.confirmationDate AS TXN_BUSINESS_DATE,
	promo.promotionalCode AS PROMO_CODE,
	NULL AS BUYER_TYPE_CODE,
	NULL AS RMB_METHOD_CODE,
	NULL AS RMB_VENDOR_CODE,
	NULL AS RMB_DATE,
	NULL AS [blank2], --intentionally left blank per spec
	NULL AS PROG_CURR_NET_AMT,
	NULL AS PROG_CURR_TAX_AMT,
	NULL AS PROG_CURR_POST_SALES_ADJ_AMT,
	NULL AS PROG_CURR_SHIPPING_HANDLING,
	NULL AS PROG_CURR_ELIG_REVENUE,
	NULL AS PROG_CURR_GRATUITY,
	NULL AS TXN_TYPE_CODE,
	CASE WHEN ts.[status] = 'Cancelled' THEN hh.hotelCode ELSE NULL END AS ORIGINAL_STORE_CODE, 
	CASE WHEN ts.[status] = 'Cancelled' THEN td.arrivalDate ELSE NULL END AS ORIGINAL_TRANSACTION_DATE,
	CASE WHEN ts.[status] = 'Cancelled' THEN td.departureDate ELSE NULL END AS ORIGINAL_TRANSACTION_END_DATE,
	NULL AS SUSPEND_REASON,
	NULL AS SUSPEND_TRANSACTION,
	 REPLACE(REPLACE(
	--REPLACE(	
	jed.jsonExternalData
	--,'"','""')
	,CHAR(10),''),CHAR(13),'') AS JSON_EXTERNAL_DATA,
	NULL AS POSTING_KEY_CODE,
	NULL AS POSTING_KEY_VALUE

 FROM Reservations.dbo.Transactions t
	JOIN Reservations.dbo.TransactionDetail td
		ON t.TransactionDetailID = td.TransactionDetailID
	JOIN Reservations.dbo.TransactionStatus ts
		ON t.TransactionStatusID = ts.TransactionStatusID
	JOIN Reservations.dbo.ActionType act
		ON ts.ActionTypeID = act.ActionTypeID
	LEFT JOIN Reservations.dbo.LoyaltyNumber l
		ON t.LoyaltyNumberID = l.LoyaltyNumberID
	JOIN CurrencyRates.dbo.dailyRates xe
		ON CASE WHEN td.arrivalDate > GETDATE() THEN ts.confirmationDate ELSE td.arrivalDate END = xe.rateDate
		AND td.currency = xe.code
	JOIN Reservations.dbo.hotel h
		ON t.HotelID = h.HotelID
	JOIN Hotels.dbo.hotel hh
		ON h.Hotel_hotelID = hh.HotelID
	JOIN Reservations.dbo.PH_BookingSource pbs
		ON t.PH_BookingSourceID = pbs.PH_BookingSourceID
	JOIN Reservations.dbo.PH_Channel pc
		ON pbs.PH_ChannelID = pc.PH_ChannelID
	JOIN Reservations.dbo.PH_SecondaryChannel psc
		ON pbs.PH_SecondaryChannelID = psc.PH_SecondaryChannelID
	JOIN Reservations.dbo.RateCode rc
		ON t.RateCodeID = rc.RateCodeID
	LEFT JOIN Reservations.dbo.PromoCode promo
		ON t.PromoCodeID = promo.PromoCodeID
	LEFT JOIN jed
		ON t.confirmationNumber = jed.confirmationNumber
	LEFT JOIN Reservations.authority.DataSource d
	ON d.DataSourceID = t.DataSourceID
	LEFT JOIN Reservations.dbo.Guest g
		ON t.GuestID = g.GuestID
	INNER JOIN ETL.dbo.[EpsilonPointTransactionLog] eptl
		ON eptl.TransactionID = t.TransactionID
	WHERE eptl.QueueID = @QueueID AND ts.[status] <> 'Cancelled'  --Remove VD. VD will be generated later

--update transaction_number to ABC123-PR-YYYY-MM-DDTHH:MM:SS
	UPDATE ETL..EpsilonExportPointTransaction
	SET TRANSACTION_NUMBER = [TRANSACTION_NUMBER]+ '-'+[TRANSACTION_TYPE_CODE] + '-'+ CAST(CAST([ACTIVITY_DATE] AS date) AS nvarchar(10) ) + 'T' + CAST(CAST([ACTIVITY_DATE] AS time) AS nvarchar(8) ) 
	WHERE QueueID = @QueueID

	INSERT INTO ETL..EpsilonExportPointTransaction
	(QueueID, [01], [TRANSACTION_NUMBER], [ACT_TRANSACTION_ID], [PROFILE_ID], [CARD_NUMBER], [TRANSACTION_TYPE_CODE], [TRANS_DESCRIPTION], [TRANSACTION_DATE], [END_DATE], [TRANSACTION_NET_AMOUNT], [ELIGIBLE_REVENUE], [TAX_AMOUNT], [POST_SALES_ADJUSTMNT_AMT], [SHIPPING_HANDLING], [STATUS], [DEVICE_ID], [DEVICE_USERID], [AUTHORIZATION_CODE], [ORIGINAL_TRANSACTION_NUMBER], [SKIP_MOMENT_ENGINE_IND], [CURRENCY_CODE], [ACCT_SRC_CODE], [SRC_ACCOUNT_NUM], [ACTIVITY_DATE], [CLIENT_FILE_ID], [CLIENT_FILE_REC_NUM], [BRAND_ORG_CODE], [ASSOCIATE_NUM], [GROSS_AMOUNT], [NET_AMT], [PROG_CURR_GROSS_AMT], [TXN_SEQ_NUM], [TXN_SOURCE_CODE], [TXN_SUBTYPE_CODE], [TIER], [TRANS_LOCATION], [blank1], [PROGRAM_CODE], [EXCHANGE_RATE_ID], [GRATUITY], [SRC_CUSTOMER_ID], [DISC_AMT], [TXN_CHANNEL_CODE], [TXN_BUSINESS_DATE], [PROMO_CODE], [BUYER_TYPE_CODE], [RMB_METHOD_CODE], [RMB_VENDOR_CODE], [RMB_DATE], [blank2], [PROG_CURR_NET_AMT], [PROG_CURR_TAX_AMT], [PROG_CURR_POST_SALES_ADJ_AMT], [PROG_CURR_SHIPPING_HANDLING], [PROG_CURR_ELIG_REVENUE], [PROG_CURR_GRATUITY], [TXN_TYPE_CODE], [ORIGINAL_STORE_CODE], [ORIGINAL_TRANSACTION_DATE], [ORIGINAL_TRANSACTION_END_DATE], [SUSPEND_REASON], [SUSPEND_TRANSACTION], [JSON_EXTERNAL_DATA], [POSTING_KEY_CODE], [POSTING_KEY_VALUE])
	SELECT
	@QueueID,
		[01], 
		CASE WHEN LEN([TRANSACTION_NUMBER]) < 21 THEN [TRANSACTION_NUMBER]+ '-VD-'+ CAST(CAST([ACTIVITY_DATE] AS date) AS nvarchar(10) ) + 'T' + CAST(CAST([ACTIVITY_DATE] AS time) AS nvarchar(8) )
		ELSE REPLACE([TRANSACTION_NUMBER],'-PR-','-VD-') END AS [TRANSACTION_NUMBER], 
		[ACT_TRANSACTION_ID], 
		[PROFILE_ID], 
		[CARD_NUMBER], 
		'VD' AS [TRANSACTION_TYPE_CODE], 
		[TRANS_DESCRIPTION], 
		[TRANSACTION_DATE], 
		[END_DATE], 
		[TRANSACTION_NET_AMOUNT], 
		[ELIGIBLE_REVENUE], 
		[TAX_AMOUNT], 
		[POST_SALES_ADJUSTMNT_AMT], 
		[SHIPPING_HANDLING], 
		big.[STATUS], 
		[DEVICE_ID], 
		[DEVICE_USERID], 
		[AUTHORIZATION_CODE], 
		[TRANSACTION_NUMBER] AS [ORIGINAL_TRANSACTION_NUMBER], 
		[SKIP_MOMENT_ENGINE_IND], 
		[CURRENCY_CODE], 
		[ACCT_SRC_CODE], 
		[SRC_ACCOUNT_NUM], 
		[ACTIVITY_DATE], 
		[CLIENT_FILE_ID], 
		[CLIENT_FILE_REC_NUM], 
		[BRAND_ORG_CODE], 
		[ASSOCIATE_NUM], 
		[GROSS_AMOUNT], 
		[NET_AMT], 
		[PROG_CURR_GROSS_AMT], 
		[TXN_SEQ_NUM], 
		[TXN_SOURCE_CODE], 
		[TXN_SUBTYPE_CODE], 
		[TIER], 
		[TRANS_LOCATION], 
		[blank1], 
		[PROGRAM_CODE], 
		[EXCHANGE_RATE_ID], 
		[GRATUITY], 
		[SRC_CUSTOMER_ID], 
		[DISC_AMT], 
		[TXN_CHANNEL_CODE], 
		[TXN_BUSINESS_DATE], 
		[PROMO_CODE], 
		[BUYER_TYPE_CODE], 
		[RMB_METHOD_CODE], 
		[RMB_VENDOR_CODE], 
		[RMB_DATE], 
		[blank2], 
		[PROG_CURR_NET_AMT], 
		[PROG_CURR_TAX_AMT],
		[PROG_CURR_POST_SALES_ADJ_AMT], 
		[PROG_CURR_SHIPPING_HANDLING], 
		[PROG_CURR_ELIG_REVENUE], 
		[PROG_CURR_GRATUITY], 
		[TXN_TYPE_CODE], 
		TRANS_LOCATION AS [ORIGINAL_STORE_CODE], 
		[TRANSACTION_DATE] AS [ORIGINAL_TRANSACTION_DATE], 
		END_DATE AS [ORIGINAL_TRANSACTION_END_DATE], 
		[SUSPEND_REASON], 
		[SUSPEND_TRANSACTION],
		[JSON_EXTERNAL_DATA], 
		[POSTING_KEY_CODE], 
		[POSTING_KEY_VALUE]
		FROM ETL..EpsilonExportPointTransaction big
		INNER JOIN 
		(
			SELECT erl.QueueID, erl.ConfirmationNumber, erl.[DataSourceID] 
			FROM ETL..EpsilonPointTransactionLog erl
			INNER JOIN 
			(
				SELECT MAX(EpsilonPointTransactionLogID) AS EpsilonPointTransactionLogID, DataSourceID, confirmationNumber 
				FROM ETL..EpsilonPointTransactionLog 
				WHERE QueueID < @QueueID
				GROUP BY DataSourceID, confirmationNumber
			) FindMax
			ON erl.EpsilonPointTransactionLogID = FindMax.EpsilonPointTransactionLogID 
		) small
		ON CASE WHEN LEN(big.[TRANSACTION_NUMBER])<=20   THEN big.[TRANSACTION_NUMBER] ELSE LEFT(big.[TRANSACTION_NUMBER],CHARINDEX('-',big.[TRANSACTION_NUMBER])-1) END = small.ConfirmationNumber AND big.QueueID = small.QueueID AND TRANSACTION_TYPE_CODE = 'PR'
		AND 	(CASE WHEN big.TXN_SUBTYPE_CODE = 'SynXis' THEN 1 ELSE 2 END  = small.[DataSourceID])
		INNER JOIN ETL..EpsilonPointTransactionLog eptl 
			ON eptl. QueueID = @QueueID AND eptl.Confirmationnumber = CASE WHEN LEN(big.[TRANSACTION_NUMBER])<=20   THEN big.[TRANSACTION_NUMBER] ELSE LEFT(big.[TRANSACTION_NUMBER],CHARINDEX('-',big.[TRANSACTION_NUMBER])-1) END
				AND (CASE WHEN big.TXN_SUBTYPE_CODE = 'SynXis' THEN 1 ELSE 2 END  = eptl.[DataSourceID])


		UPDATE ept
		SET ept.CARD_NUMBER = UPPER(c.LoyaltyNumber)
		FROM ETL..EpsilonExportPointTransaction ept
		INNER JOIN ReservationBilling..Charges C
		ON CASE WHEN LEN(ept.[TRANSACTION_NUMBER])<=20   THEN ept.[TRANSACTION_NUMBER] ELSE LEFT(ept.[TRANSACTION_NUMBER],CHARINDEX('-',ept.[TRANSACTION_NUMBER])-1) END  = c.ConfirmationNumber AND c.classificationID = 5
		WHERE (ept.CARD_NUMBER IS NULL OR ept.CARD_NUMBER = '') AND ept.QueueID = @QueueID


END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
