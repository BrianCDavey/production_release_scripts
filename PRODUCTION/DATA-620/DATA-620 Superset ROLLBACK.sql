USE Superset
GO

/*
Run this script on:

        chi-lt-00032377.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/16/2019 8:35:06 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_PointsExport]'
GO





ALTER PROCEDURE [dbo].[Epsilon_PointsExport]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	SELECT
		CASE WHEN Transaction_Source = 'SFTP' THEN tdr.Booking_ID ELSE NULL END AS TransactionID,
		ISNULL(otn.[Membership Number],tdr.iPrefer_Number) AS CardNumber,
		COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) as NumPoints,
		CASE WHEN tdr.Arrival_Date < '1900-01-01'
			THEN tdr.Reward_Posting_Date
			ELSE tdr.Arrival_Date END as ActivityDate,
		NULL AS certificateNumber,
		CASE WHEN Transaction_Source = 'SFTP' THEN NULL ELSE tdr.Transaction_Id END as ActAdjustmentId
	  FROM [Superset].[BSI].[TransactionDetailedReport] tdr
	  LEFT JOIN Superset.BSI.IPreferMapping_OldToNew otn
		ON tdr.iPrefer_Number = otn.[Old Membership Number]
	WHERE COALESCE(tdr.Points_Earned,tdr.Points_Redemeed * -1,0) <> 0
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
