USE ReportGenerator
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ReportGenerator    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.ReportGenerator

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/1/2024 3:40:17 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[GetHotelsForSopDoc]'
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetHotelsForSopDoc]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT hotelCode,
	sopNumber as WildcardValue
	FROM F3M.dbo.FinancialDocuments fd
	LEFT JOIN [F3M].[dbo].[DocumentTypes] dt ON dt.documentTypeCode = fd.docType
	WHERE docType <> 'Payments'
	AND docType <> 'Debit Memos'
	AND dt.hasBackup = 0
	AND sopNumber not in (SELECT rl.WildCardValue FROM [dbo].ReportLog rl WHERE rl.ReportID in (100,101))
	AND sopNumber <> 'CON0089531'
	AND sopNumber NOT LIKE 'WCREDIT%' -- Remove WCREDIT type
	ORDER BY hotelCode DESC
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[GetHotelsForSopDocWithBackup]'
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetHotelsForSopDocWithBackup]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT hotelCode,
	sopNumber as WildcardValue
	FROM F3M.dbo.FinancialDocuments fd
	LEFT JOIN [F3M].[dbo].[DocumentTypes] dt ON dt.documentTypeCode = fd.docType
	WHERE docType <> 'Payments'
	AND docType <> 'Debit Memos'
	AND (dt.hasBackup = 1 OR dt.hasBackup is null)
	AND sopNumber not in (SELECT rl.WildCardValue FROM [dbo].ReportLog rl WHERE rl.ReportID in (100,101))
	AND sopNumber NOT LIKE 'WCREDIT%' -- Remove WCREDIT type
	ORDER BY hotelCode
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
