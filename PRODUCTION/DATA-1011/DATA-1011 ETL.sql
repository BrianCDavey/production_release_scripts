USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 5/13/2020 11:12:22 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_GuestAgeAnalysis]'
GO
CREATE TABLE [dbo].[Import_GuestAgeAnalysis]
(
[QueueID] [int] NOT NULL,
[guestKey] [int] NOT NULL,
[FirstName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Gender] [nchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BirthDate] [nchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_GuestAgeAnalysis_FinalImport_ToDimensionalWareHouse]'
GO




-- =============================================
-- Author:		Ti Yao
-- Create date: 2020-05-11
-- Description:	Loading data from ETL db to Dimensional Warehouse DB
-- Prototype: EXEC [dbo].[Import_GuestAgeAnalysis_FinalImport_ToDimensionalWareHouse] 1
-- History: 2020-05-11 Ti Yao Initial Creation
-- =============================================

CREATE PROCEDURE [dbo].[Import_GuestAgeAnalysis_FinalImport_ToDimensionalWareHouse]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

		--DECLARE @QueueID int = 1

		UPDATE g
		SET	g.[Birth Date] = sga.[BirthDate],
			g.[IsSelfReported_BirthDate] = 0
		FROM Dimensional_Warehouse.dim.Guest g
		INNER JOIN dbo.Import_GuestAgeAnalysis sga ON g.guestKey = sga.guestKey
		WHERE QueueID = @QueueID
			AND (g.[IsSelfReported_BirthDate] = 0 OR g.[Birth Date] = 0 )

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
