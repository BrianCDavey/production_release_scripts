USE Reservations
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations    -  This database will be modified

to synchronize it with:

        (local)\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 5/18/2022 12:13:17 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CROCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CROCode] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CROCode] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_BookingSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_BookingSource] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest_EmailAddress]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_EmailAddress] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Location]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ibeSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ibeSource] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ibeSource] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[City]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Country]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PostalCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Region]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Region] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Region] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[State]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Chain]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Chain] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Chain] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CorporateCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateCode] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateCode] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Guest_CompanyName]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_CompanyName] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Guest_CompanyName] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[hotel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[hotel] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[hotel] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IATANumber]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[IATANumber] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[IATANumber] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoyaltyNumber]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyNumber] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyNumber] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoyaltyProgram]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyProgram] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LoyaltyProgram] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PromoCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PromoCode] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PromoCode] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RateCategory]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCategory] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCategory] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RateCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCode] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RateCode] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[RoomType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RoomType] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[RoomType] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TravelAgent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[TravelAgent] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[VoiceAgent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VoiceAgent] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[VoiceAgent] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ActionType]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActionType] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ActionType] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_Chain]'
GO
CREATE TABLE [map].[pegasus_Chain]
(
[pegasusID] [int] NOT NULL,
[intChainID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_Chain] on [map].[pegasus_Chain]'
GO
ALTER TABLE [map].[pegasus_Chain] ADD CONSTRAINT [PK_map_pegasus_Chain] PRIMARY KEY CLUSTERED ([intChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_CRS_BookingSource]'
GO
CREATE TABLE [map].[pegasus_CRS_BookingSource]
(
[pegasusID] [int] NOT NULL,
[BookingSourceID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_CRS_BookingSource] on [map].[pegasus_CRS_BookingSource]'
GO
ALTER TABLE [map].[pegasus_CRS_BookingSource] ADD CONSTRAINT [PK_map_pegasus_CRS_BookingSource] PRIMARY KEY CLUSTERED ([pegasusID], [BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_Guest]'
GO
CREATE TABLE [map].[pegasus_Guest]
(
[pegasusID] [int] NOT NULL,
[GuestID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_Guest] on [map].[pegasus_Guest]'
GO
ALTER TABLE [map].[pegasus_Guest] ADD CONSTRAINT [PK_map_pegasus_Guest] PRIMARY KEY CLUSTERED ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_hotel]'
GO
CREATE TABLE [map].[pegasus_hotel]
(
[pegasusID] [int] NOT NULL,
[intHotelID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_hotel] on [map].[pegasus_hotel]'
GO
ALTER TABLE [map].[pegasus_hotel] ADD CONSTRAINT [PK_map_pegasus_hotel] PRIMARY KEY CLUSTERED ([pegasusID], [intHotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_IATANumber]'
GO
CREATE TABLE [map].[pegasus_IATANumber]
(
[pegasusID] [int] NOT NULL,
[IATANumberID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_IATANumber] on [map].[pegasus_IATANumber]'
GO
ALTER TABLE [map].[pegasus_IATANumber] ADD CONSTRAINT [PK_map_pegasus_IATANumber] PRIMARY KEY CLUSTERED ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_PromoCode]'
GO
CREATE TABLE [map].[pegasus_PromoCode]
(
[pegasusID] [int] NOT NULL,
[PromoCodeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_PromoCode] on [map].[pegasus_PromoCode]'
GO
ALTER TABLE [map].[pegasus_PromoCode] ADD CONSTRAINT [PK_map_pegasus_PromoCode] PRIMARY KEY CLUSTERED ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[pegasus_RateCode]'
GO
CREATE TABLE [map].[pegasus_RateCode]
(
[pegasusID] [int] NOT NULL,
[RateTypeCodeID] [int] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_map_pegasus_RateCode] on [map].[pegasus_RateCode]'
GO
ALTER TABLE [map].[pegasus_RateCode] ADD CONSTRAINT [PK_map_pegasus_RateCode] PRIMARY KEY CLUSTERED ([RateTypeCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RateCode]'
GO



ALTER PROCEDURE [dbo].[Populate_RateCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RateCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[RateTypeName],[RateTypeCode]
		FROM
		(
			SELECT [RateTypeCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[RateTypeName],[RateTypeCode]
			FROM synxis.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[RateTypeCodeID] AS [openHospID],NULL AS [pegasusID],[RateTypeName],[RateTypeCode]
			FROM openHosp.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[RateTypeCodeID] AS [pegasusID],[RateTypeName],[RateTypeCode]
			FROM pegasus.RateTypeCode
			WHERE [RateTypeCodeID] IN(SELECT [RateTypeCodeID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [RateTypeName],[RateTypeCode]
	) AS src ON src.[RateTypeName] = tgt.[RateName] AND src.[RateTypeCode] = tgt.[RateCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[RateName],[RateCode])
		VALUES([synXisID],[openHospID],[pegasusID],[RateTypeName],[RateTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RateCategory]'
GO



ALTER PROCEDURE [dbo].[Populate_RateCategory]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RateCategory] AS tgt
	USING
	(
		SELECT DISTINCT [RateCategoryID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[rateCategoryName],ISNULL(NULLIF([rateCategoryCode],''),'Unassigned') as [rateCategoryCode]
		FROM synxis.RateCategory
		WHERE [RateCategoryID] IN(SELECT [RateCategoryID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[rateCategoryName] = tgt.[rateCategoryName] AND src.[rateCategoryCode] = tgt.[rateCategoryCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[rateCategoryName],[rateCategoryCode])
		VALUES([synXisID],[openHospID],[pegasusID],[rateCategoryName],[rateCategoryCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PromoCode]'
GO



ALTER PROCEDURE [dbo].[Populate_PromoCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PromoCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[promotionalCode]
		FROM
		(
			SELECT [PromoCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[promotionalCode]
			FROM synxis.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[PromoCodeID] AS [openHospID],NULL AS [pegasusID],[promotionalCode]
			FROM openHosp.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[PromoCodeID] AS [pegasusID],[promotionalCode]
			FROM pegasus.PromoCode
			WHERE [PromoCodeID] IN(SELECT [PromoCodeID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [promotionalCode]
	) AS src ON src.[promotionalCode] = tgt.[promotionalCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[promotionalCode])
		VALUES([synXisID],[openHospID],[pegasusID],[promotionalCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PMSRateTypeCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PMSRateTypeCode] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PMSRateTypeCode] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PMSRateTypeCode]'
GO



ALTER PROCEDURE [dbo].[Populate_PMSRateTypeCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PMSRateTypeCode] AS tgt
	USING
	(
		SELECT DISTINCT [PMSRateTypeCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[PMSRateTypeCode]
		FROM synxis.PMSRateTypeCode
		WHERE [PMSRateTypeCodeID] IN(SELECT [PMSRateTypeCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[PMSRateTypeCode] = tgt.[PMSRateTypeCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[PMSRateTypeCode])
		VALUES([synXisID],[openHospID],[pegasusID],[PMSRateTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Region]'
GO



ALTER PROCEDURE [dbo].[Populate_Region]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Region] AS tgt
	USING
	(
		SELECT DISTINCT [RegionID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF([Region_Text],''),'unknown') AS [Region_Text]
		FROM synxis.Region
	) AS src ON src.[Region_Text] = tgt.[Region_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[Region_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[Region_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_PostalCode]'
GO




ALTER PROCEDURE [dbo].[Populate_PostalCode]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
	
	-- CREATE & POPULATE #POST ----------------------------
	IF OBJECT_ID('tempdb..#POST') IS NOT NULL
		DROP TABLE #POST;

	CREATE TABLE #POST
	(
		[synXisID] varchar(MAX),
		[openHospID] varchar(MAX),
		[pegasusID] varchar(MAX),
		[PostalCode_Text] nvarchar(255),

		PRIMARY KEY CLUSTERED([PostalCode_Text])
	)

	INSERT INTO #POST(synXisID,openHospID,pegasusID,PostalCode_Text)
	SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[PostalCode_Text]
	FROM
	(
		SELECT [PostalCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([PostalCode_Text]),''),'unknown') AS [PostalCode_Text]
		FROM synxis.PostalCode
			UNION ALL
		SELECT NULL AS [synXisID],[PostalCodeID] AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([PostalCode_Text]),''),'unknown') AS [PostalCode_Text]
		FROM openHosp.PostalCode
			UNION ALL
		SELECT NULL AS [synXisID],NULL AS [openHospID],[PostalCodeID] AS [pegasusID],ISNULL(NULLIF(TRIM([PostalCode_Text]),''),'unknown') AS [PostalCode_Text]
		FROM pegasus.PostalCode
	) x
	GROUP BY [PostalCode_Text]
	-------------------------------------------------------

	MERGE INTO [dbo].[PostalCode] AS tgt
	USING
	(
		SELECT [synXisID],[openHospID],[pegasusID],[PostalCode_Text] FROM #POST
	) AS src ON src.[PostalCode_Text] = tgt.[PostalCode_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[PostalCode_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[PostalCode_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_State]'
GO


ALTER PROCEDURE [dbo].[Populate_State]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[State] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[State_Text]
		FROM
		(
			SELECT [StateID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([State_Text]),''),'unknown') AS [State_Text]
			FROM synxis.[State]
				UNION ALL
			SELECT NULL AS [synXisID],[StateID] AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([State_Text]),''),'unknown') AS [State_Text]
			FROM openHosp.[State]
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[StateID] AS [pegasusID],ISNULL(NULLIF(TRIM([State_Text]),''),'unknown') AS [State_Text]
			FROM pegasus.[State]
		) x
		GROUP BY [State_Text]
	) AS src ON src.[State_Text] = tgt.[State_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[State_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[State_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_SubSource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SubSource] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SubSource] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_SecondarySource]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SecondarySource] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_SecondarySource] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CRS_Channel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_Channel] ADD
[pegasusID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CRS_Channel] ADD
[IsPegasus] AS (case  when [pegasusID] IS NULL then (0) else (1) end) PERSISTED NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MostRecentTransactions]'
GO







ALTER VIEW [dbo].[MostRecentTransactions]
AS

	SELECT t.TransactionID,ch.ChainName,ch.CRS_ChainID AS [ChainID],hh.HotelName,hh.SynXisID AS [HotelID],synXis.SAPID AS [SAPID],hh.HotelCode,synXis.billingDescription AS [billingDescription],t.transactionTimeStamp,synXis.faxNotificationCount AS [FaxNotificationCount],
			bs.channel,bs.secondarySource,bs.subSource,bs.subSourceCode,synXis.PMSRateTypeCode AS [PMSRateTypeCode],synXis.PMSRoomTypeCode AS [PMSRoomTypeCode],synXis.marketSourceCode AS [marketSourceCode],synXis.marketSegmentCode AS [marketSegmentCode],synXis.userName AS [userName],
			ts.[status],t.confirmationNumber,ts.confirmationDate,ts.cancellationNumber,ts.cancellationDate,ISNULL(gu.salutation,N'') AS salutation,ISNULL(gu.FirstName,N'') AS [guestFirstName],ISNULL(gu.LastName,N'') AS [guestLastName],
			ISNULL(gu.customerID,N'') AS customerID,ISNULL(gu.Address1,N'') AS [customerAddress1],ISNULL(gu.Address2,N'') AS [customerAddress2],ISNULL(NULLIF(gul.City_Text,N''),N'unknown') AS [customerCity],ISNULL(NULLIF(gul.State_Text,N''),N'unknown') AS [customerState],ISNULL(NULLIF(gul.PostalCode_Text,N''),N'unknown') AS [customerPostalCode],ISNULL(gu.phone,'') AS [customerPhone],ISNULL(NULLIF(gul.Country_Text,N''),N'unknown') AS [customerCountry],
			synXis.customerArea AS [customerArea],ISNULL(NULLIF(gul.Region_Text,N''),N'unknown') AS [customerRegion],gcn.CompanyName AS [customerCompanyName],td.arrivalDate,td.departureDate,
			td.bookingLeadTime,rc.rateCategoryName,ISNULL(NULLIF(rc.rateCategoryCode,N''),N'Unassigned') AS rateCategoryCode,COALESCE(synXis.RateTypeName,openHosp.RateTypeName,pegasus.RateTypeName) AS [rateTypeName],rac.RateCode AS [rateTypeCode],rt.roomTypeName,rt.roomTypeCode,
			td.nights,td.averageDailyRate,td.rooms,td.reservationRevenue,td.currency,ISNULL(iata.IATANumber,'') AS IATANumber,
			ISNULL(ta.[Name],N'') AS [travelAgencyName],ISNULL(ta.Address1,N'') AS [travelAgencyAddress1],ISNULL(ta.Address2,N'') AS [travelAgencyAddress2],ISNULL(NULLIF(tal.City_Text,N''),N'unknown') AS [travelAgencyCity],ISNULL(tal.State_Text,N'unknown') AS [travelAgencyState],ISNULL(tal.PostalCode_Text,N'unknown') AS [travelAgencyPostalCode],
			ISNULL(ta.Phone,N'') AS [travelAgencyPhone],ISNULL(ta.Fax,N'') AS [travelAgencyFax],ISNULL(tal.Country_Text,N'') AS [travelAgencyCountry],synXis.[travelAgencyArea] AS [travelAgencyArea],ISNULL(tal.Region_Text,N'') AS [travelAgencyRegion],ISNULL(ta.Email,N'') AS [travelAgencyEmail],
			synXis.consortiaCount AS [consortiaCount],synXis.consortiaName AS [consortiaName],td.totalPackageRevenue,td.optIn,ISNULL(gue.emailAddress,N'') AS [customerEmail],td.totalGuestCount,td.adultCount,td.childrenCount,ts.creditCardType,
			a.actionType,synXis.shareWith AS [shareWith],
			CASE td.arrivalDOW WHEN 1 THEN 'Sun' WHEN 2 THEN 'Mon' WHEN 3 THEN 'Tue' WHEN 4 THEN 'Wed' WHEN 5 THEN 'Thu' WHEN 6 THEN 'Fri' WHEN 7 THEN 'Sat' END AS ArrivalDOW,
			CASE td.departureDOW WHEN 1 THEN 'Sun' WHEN 2 THEN 'Mon' WHEN 3 THEN 'Tue' WHEN 4 THEN 'Wed' WHEN 5 THEN 'Thu' WHEN 6 THEN 'Fri' WHEN 7 THEN 'Sat' END AS DepartureDOW,
			t.itineraryNumber,synXis.secondaryCurrency AS [secondaryCurrency],synXis.secondaryCurrencyExchangeRate AS [secondaryCurrencyExchangeRate],synXis.secondaryCurrencyAverageDailyRate AS [secondaryCurrencyAverageDailyRate],synXis.secondaryCurrencyReservationRevenue AS [secondaryCurrencyReservationRevenue],synXis.secondaryCurrencyPackageRevenue AS [secondaryCurrencyPackageRevenue],
			td.commisionPercent,synXis.membershipNumber AS [membershipNumber],cc.corporationCode,pc.promotionalCode,bs.CRO_Code AS [CROCode],t.channelConnectConfirmationNumber,td.IsPrimaryGuest AS [primaryGuest],
			lp.LoyaltyProgram AS [loyaltyProgram],ln.loyaltyNumber,synXis.vipLevel AS [vipLevel],bs.ibeSourceName AS [xbeTemplateName],synXis.xbeShellName AS [xbeShellName],synXis.profileTypeSelection AS [profileTypeSelection],
			[dbo].[convertCurrencyToUSD](td.averageDailyRate,td.currency,ts.confirmationDate) AS [averageDailyRateUSD],[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS [reservationRevenueUSD],[dbo].[convertCurrencyToUSD](td.totalPackageRevenue,td.currency,ts.confirmationDate) AS [totalPackageRevenueUSD],
			t.timeLoaded,hh.OpenHospID AS [OpenHospitalityID],hh.PegasusRT4ID AS [PegasusID],ds.CRSSourceID AS [CRSSourceID],td.LoyaltyNumberValidated,td.LoyaltyNumberTagged
	FROM dbo.Transactions t WITH(NOLOCK)
		INNER JOIN authority.DataSource ds WITH(NOLOCK) ON ds.DataSourceID = t.DataSourceID
		INNER JOIN dbo.TransactionStatus ts WITH(NOLOCK) ON ts.TransactionStatusID = t.TransactionStatusID
		INNER JOIN dbo.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
		LEFT JOIN dbo.Chain ch WITH(NOLOCK) ON ch.ChainID = t.ChainID
		LEFT JOIN dbo.hotel ht WITH(NOLOCK) ON ht.HotelID = t.HotelID
		LEFT JOIN Hotels.dbo.Hotel hh WITH(NOLOCK) ON hh.HotelID = ht.Hotel_hotelID
		LEFT JOIN dbo.vw_CRS_BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.CRS_BookingSourceID
		LEFT JOIN dbo.Guest gu WITH(NOLOCK) ON gu.GuestID = t.GuestID
		LEFT JOIN dbo.vw_Location gul WITH(NOLOCK) ON gul.LocationID = gu.LocationID
		LEFT JOIN dbo.Guest_EmailAddress gue WITH(NOLOCK) ON gue.Guest_EmailAddressID = gu.Guest_EmailAddressID
		LEFT JOIN dbo.Guest_CompanyName gcn WITH(NOLOCK) ON gcn.Guest_CompanyNameID = t.Guest_CompanyNameID
		LEFT JOIN dbo.RateCategory rc WITH(NOLOCK) ON rc.RateCategoryID = t.RateCategoryID
		LEFT JOIN dbo.RoomType rt WITH(NOLOCK) ON rt.RoomTypeID = t.RoomTypeID
		LEFT JOIN dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN dbo.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
		LEFT JOIN dbo.vw_Location tal WITH(NOLOCK) ON tal.LocationID = ta.LocationID
		LEFT JOIN dbo.ActionType a WITH(NOLOCK) ON a.ActionTypeID = ts.ActionTypeID
		LEFT JOIN dbo.CorporateCode cc WITH(NOLOCK) ON cc.CorporateCodeID = t.CorporateCodeID
		LEFT JOIN dbo.PromoCode pc WITH(NOLOCK) ON pc.PromoCodeID = t.PromoCodeID
		LEFT JOIN dbo.LoyaltyNumber ln WITH(NOLOCK) ON ln.LoyaltyNumberID = t.LoyaltyNumberID
		LEFT JOIN dbo.LoyaltyProgram lp WITH(NOLOCK) ON lp.LoyaltyProgramID = t.LoyaltyProgramID
		LEFT JOIN dbo.RateCode rac WITH(NOLOCK) ON rac.RateCodeID = t.RateCodeID
		LEFT JOIN
			(
				SELECT t.TransactionID AS synXisID,bd.billingDescription,te.faxNotificationCount,pms.PMSRateTypeCode,rt.PMSRoomTypeCode,
						te.marketSourceCode,te.marketSegmentCode,un.userName,rtc.RateTypeName,te.consortiaCount,con.consortiaName,te.SAPID,
						te.shareWith,td.secondaryCurrency,td.secondaryCurrencyExchangeRate,td.secondaryCurrencyAverageDailyRate,td.secondaryCurrencyReservationRevenue,
						td.secondaryCurrencyPackageRevenue,te.membershipNumber,vip.vipLevel,te.profileTypeSelection,xbe.xbeShellName,
						ga.Area_Text AS [customerArea],taa.Area_Text AS [travelAgencyArea]
				FROM synxis.Transactions t WITH(NOLOCK)
					LEFT JOIN synxis.BillingDescription bd WITH(NOLOCK) ON bd.BillingDescriptionID = t.BillingDescriptionID
					LEFT JOIN synxis.TransactionsExtended te WITH(NOLOCK) ON te.TransactionsExtendedID = t.TransactionsExtendedID
					LEFT JOIN synxis.PMSRateTypeCode pms WITH(NOLOCK) ON pms.PMSRateTypeCodeID = t.PMSRateTypeCodeID
					LEFT JOIN synxis.RoomType rt WITH(NOLOCK) ON rt.RoomTypeID = t.RoomTypeID
					LEFT JOIN synxis.UserName un WITH(NOLOCK) ON un.UserNameID = t.UserNameID
					LEFT JOIN synxis.RateTypeCode rtc WITH(NOLOCK) ON rtc.RateTypeCodeID = t.RateTypeCodeID
					LEFT JOIN synxis.Consortia con WITH(NOLOCK) ON con.ConsortiaID = t.ConsortiaID
					LEFT JOIN synxis.TransactionDetail td WITH(NOLOCK) ON td.TransactionDetailID = t.TransactionDetailID
					LEFT JOIN synxis.VIP_Level vip WITH(NOLOCK) ON vip.VIP_LevelID = t.VIP_LevelID
					LEFT JOIN synxis.BookingSource bs WITH(NOLOCK) ON bs.BookingSourceID = t.BookingSourceID
					LEFT JOIN synxis.xbeTemplate xbe WITH(NOLOCK) ON xbe.xbeTemplateID = bs.xbeTemplateID
					LEFT JOIN synxis.Guest g WITH(NOLOCK) ON g.GuestID = t.GuestID
						LEFT JOIN synxis.[Location] gloc WITH(NOLOCK) ON gloc.LocationID = g.LocationID
						LEFT JOIN synxis.Area ga WITH(NOLOCK) ON ga.AreaID = gloc.AreaID
					LEFT JOIN synxis.TravelAgent ta WITH(NOLOCK) ON ta.TravelAgentID = t.TravelAgentID
						LEFT JOIN synxis.[Location] taloc WITH(NOLOCK) ON taloc.LocationID = ta.LocationID
						LEFT JOIN synxis.Area taa WITH(NOLOCK) ON taa.AreaID = taloc.AreaID
			) synXis ON synXis.synXisID = t.sourceKey AND ds.SourceName = 'SynXis'
		LEFT JOIN
			(
				SELECT t.TransactionID AS openHospID,rtc.RateTypeName
				FROM openHosp.Transactions t WITH(NOLOCK)
					LEFT JOIN openHosp.RateTypeCode rtc WITH(NOLOCK) ON rtc.RateTypeCodeID = t.RateTypeCodeID
			) openHosp ON openHosp.openHospID = t.sourceKey AND ds.SourceName = 'Open Hospitality'
		LEFT JOIN
			(
				SELECT t.TransactionID AS pegasusID,rtc.RateTypeName
				FROM pegasus.Transactions t WITH(NOLOCK)
					LEFT JOIN pegasus.RateTypeCode rtc WITH(NOLOCK) ON rtc.RateTypeCodeID = t.RateTypeCodeID
			) pegasus ON pegasus.pegasusID = t.sourceKey AND ds.SourceName = 'Pegasus'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[MostRecentTransactionsReporting]'
GO


ALTER VIEW [dbo].[MostRecentTransactionsReporting]
AS
SELECT TransactionID,[chainName],[chainID],COALESCE(hrx.hotelName,hro.hotelName,hrp.hotelName) AS hotelName,[hotelID],[SAPID],COALESCE(hrx.code,hro.code,hrp.code) AS hotelCode,[billingDescription],[transactionTimeStamp],[faxNotificationCount],[channel],
		[secondarySource],[subSource],[subSourceCode],[PMSRateTypeCode],[PMSRoomTypeCode],[marketSourceCode],[marketSegmentCode],[userName],[status],
		[confirmationNumber],[confirmationDate],[cancellationNumber],[cancellationDate],[salutation],[guestFirstName],[guestLastName],[customerID],[customerAddress1],
		[customerAddress2],[customerCity],[customerState],[customerPostalCode],[customerPhone],[customerCountry],[customerArea],[customerRegion],[customerCompanyName],
		[arrivalDate],[departureDate],[bookingLeadTime],[rateCategoryName],[rateCategoryCode],[rateTypeName],[rateTypeCode],[roomTypeName],[roomTypeCode],[nights],
		[averageDailyRate],[rooms],[reservationRevenue],[currency],[IATANumber],[travelAgencyName],[travelAgencyAddress1],[travelAgencyAddress2],[travelAgencyCity],
		[travelAgencyState],[travelAgencyPostalCode],[travelAgencyPhone],[travelAgencyFax],[travelAgencyCountry],[travelAgencyArea],[travelAgencyRegion],
		[travelAgencyEmail],[consortiaCount],[consortiaName],[totalPackageRevenue],[optIn],[customerEmail],[totalGuestCount],[adultCount],[childrenCount],
		[creditCardType],[actionType],[shareWith],[arrivalDOW],[departureDOW],[itineraryNumber],[secondaryCurrency],[secondaryCurrencyExchangeRate],
		[secondaryCurrencyAverageDailyRate],[secondaryCurrencyReservationRevenue],[secondaryCurrencyPackageRevenue],[commisionPercent],[membershipNumber],
		[corporationCode],[promotionalCode],CROcode,[channelConnectConfirmationNumber],[primaryGuest],[loyaltyProgram],[loyaltyNumber],[vipLevel],[xbeTemplateName],
		CASE 
			WHEN secondarySource = 'iPrefer APP' THEN 'iPrefer APP'
			WHEN (xbeTemplateName = '') THEN '' 
			WHEN ISNULL(ibe.IsStatic,0) = 0 THEN 'Hotel Website' 
			ELSE 'PHG Website' 
		END AS templateType,
		[xbeShellName],[profileTypeSelection],[averageDailyRateUSD],[reservationRevenueUSD],[totalPackageRevenueUSD],[timeLoaded],
		CASE
			WHEN channel = 'Booking Engine' OR channel = 'Mobile Web' THEN 
				CASE WHEN secondarySource = 'iPrefer APP' THEN 'iPrefer APP' ELSE COALESCE (xbeTemplateName,'Hotel Booking Engine') END
			WHEN channel = 'PMS Rez Synch' THEN 'PMS Rez Synch' 
			WHEN channel = 'Voice' THEN 
				CASE
					WHEN (subSourceCode = '' OR subSourceCode IS NULL) THEN 'Voice - No Subchannel' 
					ELSE subSourceCode 
				END 
			WHEN (subSourceCode = '' OR subSourceCode IS NULL) THEN 
				CASE WHEN (secondarySource = '' OR secondarySource IS NULL) THEN channel ELSE secondarySource END 
			WHEN (subSource = '' OR subSource IS NULL) THEN subSourceCode
			ELSE subSource 
		END AS subSourceReportLabel,
		rooms * nights AS roomNights,
		[OpenHospitalityID],[PegasusID],[CRSSourceID],
		CASE Channel 
			WHEN 'Booking Engine' THEN 
				CASE 
					WHEN secondarySource = 'iPrefer APP' THEN 'IBE - PHG'
					WHEN ISNULL(ibe.IsStatic,0) = 1 THEN 'IBE - PHG'
					ELSE 'IBE - Hotel' 
				END
			WHEN 'Channel Connect' THEN 'OTA'
			WHEN 'GDS' THEN 'GDS'
			WHEN 'Google' THEN 'OTA'
			WHEN 'IDS' THEN 'OTA'
			WHEN 'Mobile Web' THEN
				CASE WHEN ISNULL(ibe.IsStatic,0) = 1 THEN 'IBE - PHG'
					ELSE 'IBE - Hotel' 
				END		
			WHEN 'PMS Rez Synch' THEN 'PMS'
			WHEN 'Voice' THEN
				CASE WHEN croCode IS NULL THEN 'Voice - Hotel Agent'
					ELSE 'Voice - PHG'
				END
		END AS ChannelReportLabel
FROM dbo.MostRecentTransactions mrt
	LEFT JOIN authority.ibeSource ibe ON ibe.ibeSourceName = xbeTemplateName
	LEFT JOIN Hotels.dbo.hotelsReporting hrx WITH(NOLOCK) ON mrt.hotelID = hrx.synxisID
	LEFT JOIN Hotels.dbo.hotelsReporting hro WITH(NOLOCK) ON mrt.OpenHospitalityID = hro.openHospitalityCode
	LEFT JOIN Hotels.dbo.hotelsReporting hrp WITH(NOLOCK) ON mrt.PegasusID = hrp.PegasusCode

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UPDATE_CRS_BookingSourceID]'
GO



ALTER PROCEDURE [dbo].[UPDATE_CRS_BookingSourceID]
	@QueueID int = NULL
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_BS(confirmationNumber,BookingSourceID,rowNum)
	AS
	(
		SELECT t.confirmationNumber,tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM synxis.Transactions t
			INNER JOIN synxis.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[synXis_CRS_BookingSource] tbs ON tbs.synXisID = bs.BookingSourceID
			INNER JOIN synxis.Channel ch ON ch.ChannelID = bs.ChannelID
		WHERE NULLIF(ch.channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM synxis.Transactions WHERE QueueID = @QueueID)

		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM openHosp.Transactions t
			INNER JOIN openHosp.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[openHosp_CRS_BookingSource] tbs ON tbs.openHospID = bs.BookingSourceID
			INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM openHosp.Transactions WHERE QueueID = @QueueID)
		
		UNION ALL

		SELECT CASE WHEN dups.confirmationNumber IS NOT NULL THEN t.confirmationNumber + '_OH' ELSE t.confirmationNumber END AS confirmationNumber,         
		tbs.BookingSourceID,
			ROW_NUMBER() OVER(PARTITION BY t.confirmationNumber ORDER BY t.transactionTimeStamp)
		FROM pegasus.Transactions t
			INNER JOIN pegasus.BookingSource bs ON bs.BookingSourceID = t.BookingSourceID
			INNER JOIN map.[pegasus_CRS_BookingSource] tbs ON tbs.pegasusID = bs.BookingSourceID
			INNER JOIN authority.Pegasus_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
		WHERE NULLIF(obs.Channel,'') IS NOT NULL
			AND t.confirmationNumber IN(SELECT confirmationNumber FROM pegasus.Transactions WHERE QueueID = @QueueID)

	)
	UPDATE t
		SET CRS_BookingSourceID = bs.BookingSourceID
	FROM dbo.Transactions t
		INNER JOIN cte_BS bs ON bs.confirmationNumber = t.confirmationNumber
	WHERE bs.rowNum = 1
		AND t.CRS_BookingSourceID != bs.BookingSourceID
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SabreIATA_Update_MRT]'
GO


ALTER PROCEDURE [dbo].[SabreIATA_Update_MRT]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	

	-- Add Missing IATA's ------------------------------------------
	INSERT INTO dbo.IATANumber([synXisID],[openHospID],[pegasusID],[IATANumber])
	SELECT DISTINCT NULL,NULL,NULL,iata_no
	FROM ETL.dbo.Import_SabreIATA
		EXCEPT
	SELECT NULL,NULL,NULL,IATANumber
	FROM dbo.IATANumber
	----------------------------------------------------------------

	UPDATE t
		SET IATANumberID = iata.IATANumberID
	FROM dbo.Transactions t
		INNER JOIN dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		INNER JOIN ETL.dbo.Import_SabreIATA i ON i.crs_confirm_no = t.confirmationNumber AND CONVERT(date,i.arrival_date) <= td.arrivalDate
		LEFT JOIN ETL.dbo.Queue q ON q.QueueID = i.QueueID
		INNER JOIN dbo.IATANumber iata ON iata.IATANumber = i.iata_no
	WHERE t.timeLoaded <= q.ImportFinished
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_VoiceAgent]'
GO



ALTER PROCEDURE [dbo].[Populate_VoiceAgent]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[VoiceAgent] AS tgt
	USING
	(
		SELECT DISTINCT [UserNameID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[userName]
		FROM synxis.UserName
		WHERE [UserNameID] IN(SELECT [UserNameID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[userName] = tgt.[VoiceAgent]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[VoiceAgent])
		VALUES([synXisID],[openHospID],[pegasusID],[userName])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TravelAgent]'
GO



ALTER PROCEDURE [dbo].[Populate_TravelAgent]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[TravelAgent] tgt
	USING
	(
		SELECT DISTINCT STRING_AGG(CONVERT(varchar(MAX),t.TravelAgentID),',') AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],i.[IATANumberID],t.[Name],MAX(t.[Address1]) AS [Address1],
						MAX(t.[Address2]) AS [Address2],l.[LocationID],MAX(t.[Phone]) AS [Phone],MAX(t.[Fax]) AS [Fax],t.[Email]
		FROM synxis.TravelAgent t
			INNER JOIN (SELECT [LocationID],value AS synXisID FROM dbo.[Location] CROSS APPLY string_split(synXisID,',')) l ON l.synXisID = t.LocationID
			INNER JOIN (SELECT [IATANumberID],value AS synXisID FROM dbo.IATANumber CROSS APPLY string_split(synXisID,',')) i ON i.synXisID = t.IATANumberID
		WHERE t.[TravelAgentID] IN(SELECT [TravelAgentID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		GROUP BY i.[IATANumberID],t.[Name],l.[LocationID],t.[Email]
	) AS src ON ISNULL(src.[IATANumberID],'') = ISNULL(tgt.[IATANumberID],'') AND ISNULL(src.[Name],'') = ISNULL(tgt.[Name],'') AND ISNULL(src.[LocationID],'') = ISNULL(tgt.[LocationID],'') AND ISNULL(src.[Email],'') = ISNULL(tgt.[Email],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID],
				[Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[Phone] = src.[Phone],
				[Fax] = src.[Fax]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[IATANumberID],[Name],[Address1],[Address2],[LocationID],[Phone],[Fax],[Email])
		VALUES([synXisID],[openHospID],[pegasusID],[IATANumberID],[Name],[Address1],[Address2],[LocationID],[Phone],[Fax],[Email])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionStatus]'
GO



ALTER PROCEDURE [dbo].[Populate_TransactionStatus]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int,@Pegasus_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'
	SELECT @Pegasus_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus'

	MERGE INTO [dbo].[TransactionStatus] AS tgt
	USING
	(
		SELECT [synXisID],[openHospID],[pegasusID],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID],
				[DataSourceID]
		FROM
		(
			SELECT ts.[TransactionStatusID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],t.[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID],
					@SynXis_DS AS [DataSourceID]
			FROM synxis.TransactionStatus ts
				INNER JOIN synxis.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS synXisID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(synXisID,',')) x ON x.synXisID = ts.ActionTypeID
			WHERE t.QueueID = @QueueID
			
			UNION ALL
			
			SELECT NULL AS [synXisID],ts.[TransactionStatusID] AS [openHospID],NULL AS [pegasusID],
				CASE WHEN dups.[confirmationNumber] IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID],@OpenHosp_DS AS [DataSourceID]
			FROM openHosp.TransactionStatus ts
				INNER JOIN openHosp.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS openHospID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(openHospID,',')) x ON x.openHospID = ts.ActionTypeID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = ts.confirmationNumber
			WHERE t.QueueID = @QueueID
			
			UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [openHospID],ts.[TransactionStatusID] AS [pegasusID],
				CASE WHEN dups.[confirmationNumber] IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber], -----unsure if '_OH' should be replaced here
				[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],x.[ActionTypeID],@Pegasus_DS AS [DataSourceID]
			FROM pegasus.TransactionStatus ts
				INNER JOIN pegasus.Transactions t ON t.TransactionStatusID = ts.TransactionStatusID
				INNER JOIN pegasus.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN (SELECT value AS pegasusID,ActionTypeID FROM dbo.ActionType CROSS APPLY string_split(pegasusID,',')) x ON x.pegasusID = ts.ActionTypeID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = ts.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[status] = src.[status],
				[confirmationDate] = src.[confirmationDate],
				[cancellationNumber] = src.[cancellationNumber],
				[cancellationDate] = src.[cancellationDate],
				[creditCardType] = src.[creditCardType],
				[ActionTypeID] = src.[ActionTypeID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID])
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,[confirmationNumber],[status],[confirmationDate],[cancellationNumber],[cancellationDate],[creditCardType],[ActionTypeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Transactions]'
GO



ALTER PROCEDURE [dbo].[Populate_Transactions]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int, @Pegasus_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'
	SELECT @Pegasus_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus'

	MERGE INTO [dbo].[Transactions] AS tgt
	USING
	(
		SELECT [synXisID],[openHospID],[pegasusID],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],
				[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],
				[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID,
				[DataSourceID],IsMKTIPM,LoyaltyNumber_IsNewMember,LoyaltyNumber_IsImputedFromEmail
		FROM
		(
			SELECT t.[TransactionID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],t.[QueueID],t.[itineraryNumber],t.[confirmationNumber],
					t.[transactionTimeStamp],t.[channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					cc.[CorporateCodeID],rt.[RoomTypeID],rc.[RateCategoryID],u.UserNameID,
					bs.[BookingSourceID],ln.[LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],ta.[TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,gc.[Guest_CompanyNameID],lp.LoyaltyProgramID,@SynXis_DS AS [DataSourceID],
					CASE WHEN MKTIPM.RateCodeID IS NULL THEN 0 ELSE 1 END AS IsMKTIPM,
					CASE
						WHEN NULLIF(ln.[LoyaltyNumberID],624035) IS NULL THEN NULL
						ELSE
							CASE WHEN DATEDIFF(DAY,ts.confirmationDate,mp.Enrollment_Date) BETWEEN 0 AND 3 THEN 1 ELSE 0 END
					END AS LoyaltyNumber_IsNewMember,
					CASE WHEN ln.[LoyaltyNumberID] IS NULL THEN NULL ELSE 0 END AS LoyaltyNumber_IsImputedFromEmail
			FROM synxis.Transactions t
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.DataSourceID = @SynXis_DS AND ts.sourceKey = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.DataSourceID = @SynXis_DS AND td.sourceKey = t.TransactionDetailID AND td.[synXisTransExID] = t.TransactionsExtendedID
				LEFT JOIN map.[synXis_CorporateCode] cc ON cc.synXisID = t.CorporateCodeID
				LEFT JOIN map.[synXis_RoomType] rt	ON rt.synXisID = t.RoomTypeID
				LEFT JOIN map.[synXis_RateCategory] rc	ON rc.synXisID = t.RateCategoryID
				LEFT JOIN map.[synXis_VoiceAgent] u ON u.synXisID = t.UserNameID
				LEFT JOIN map.[synXis_CRS_BookingSource] bs ON bs.synXisID = t.BookingSourceID
				LEFT JOIN map.[synXis_LoyaltyNumber] ln ON ln.synXisID = t.LoyaltyNumberID
					LEFT JOIN synxis.LoyaltyNumber sln ON sln.LoyaltyNumberID = ln.LoyaltyNumberID
					LEFT JOIN Loyalty.[dbo].[LoyaltyNumber] lln ON lln.[LoyaltyNumberName] = sln.loyaltyNumber
					LEFT JOIN Loyalty.dbo.MemberProfile mp ON mp.LoyaltyNumberID = lln.LoyaltyNumberID
				LEFT JOIN map.[synXis_LoyaltyProgram] lp ON lp.synXisID = t.LoyaltyProgramID
				LEFT JOIN map.[synXis_Guest] g	ON g.synXisID = t.GuestID
				LEFT JOIN map.[synXis_RateCode] rtc ON rtc.synXisID = t.RateTypeCodeID
				LEFT JOIN map.[synXis_PromoCode] pc ON pc.synXisID = t.PromoCodeID
				LEFT JOIN map.[synXis_TravelAgent] ta ON ta.synXisID = t.TravelAgentID
				LEFT JOIN map.[synXis_IATANumber] i ON i.synXisID = t.IATANumberID
				LEFT JOIN map.[synXis_hotel] h	ON h.synXisID = t.intHotelID
				LEFT JOIN map.[synXis_Chain] ch ON ch.synXisID = t.intChainID
				LEFT JOIN map.[synXis_Guest_CompanyName] gc ON gc.synXisID = t.Guest_CompanyNameID
				LEFT JOIN (SELECT RateCodeID FROM dbo.RateCode WHERE RateCode = 'MKTIPM') MKTIPM ON MKTIPM.RateCodeID = rtc.RateTypeCodeID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID
				AND ISNULL(t.confirmationNumber,'') != ''

			UNION ALL

			SELECT NULL AS [synXisID],t.[TransactionID] AS [openHospID],NULL AS [pegasusID],t.[QueueID],NULL AS [itineraryNumber],
					CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
					t.[transactionTimeStamp],NULL AS [channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					NULL AS [CorporateCodeID],NULL AS [RoomTypeID],NULL AS [RateCategoryID],NULL AS [VoiceAgentID],
					bs.[BookingSourceID],NULL AS [LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],NULL AS [TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,NULL AS [Guest_CompanyNameID],NULL AS LoyaltyProgramID,@OpenHosp_DS AS [DataSourceID],
					CASE WHEN MKTIPM.RateCodeID IS NULL THEN 0 ELSE 1 END AS IsMKTIPM,
					NULL AS LoyaltyNumber_IsNewMember,NULL AS LoyaltyNumber_IsImputedFromEmail
			FROM openHosp.Transactions t
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.DataSourceID = @OpenHosp_DS AND ts.sourceKey = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.DataSourceID = @OpenHosp_DS AND td.sourceKey = t.TransactionDetailID
				LEFT JOIN map.[openHosp_CRS_BookingSource] bs ON bs.openHospID = t.BookingSourceID
				LEFT JOIN map.[openHosp_Guest] g ON g.openHospID = t.GuestID
				LEFT JOIN map.[openHosp_RateCode] rtc ON rtc.openHospID = t.RateTypeCodeID
				LEFT JOIN map.[openHosp_PromoCode] pc ON pc.openHospID = t.PromoCodeID
				LEFT JOIN map.[openHosp_IATANumber] i ON i.openHospID = t.IATANumberID
				LEFT JOIN map.[openHosp_hotel] h ON h.openHospID = t.intHotelID
				LEFT JOIN map.[openHosp_Chain] ch ON ch.openHospID = t.intChainID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
				LEFT JOIN (SELECT RateCodeID FROM dbo.RateCode WHERE RateCode = 'MKTIPM') MKTIPM ON MKTIPM.RateCodeID = rtc.RateTypeCodeID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID

			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],t.[TransactionID] AS [pegasusID],t.[QueueID],NULL AS [itineraryNumber],
					CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],      -----'_OH' might need to be changed
					t.[transactionTimeStamp],NULL AS [channelConnectConfirmationNumber],t.[timeLoaded],
					ts.[TransactionStatusID],td.[TransactionDetailID],
					NULL AS [CorporateCodeID],NULL AS [RoomTypeID],NULL AS [RateCategoryID],NULL AS [VoiceAgentID],
					bs.[BookingSourceID],NULL AS [LoyaltyNumberID],
					g.[GuestID],rtc.RateTypeCodeID,pc.[PromoCodeID],NULL AS [TravelAgentID],i.[IATANumberID],h.intHotelID,
					ch.intChainID,NULL AS [Guest_CompanyNameID],NULL AS LoyaltyProgramID,@Pegasus_DS AS [DataSourceID],
					CASE WHEN MKTIPM.RateCodeID IS NULL THEN 0 ELSE 1 END AS IsMKTIPM,
					NULL AS LoyaltyNumber_IsNewMember,NULL AS LoyaltyNumber_IsImputedFromEmail
			FROM pegasus.Transactions t
				INNER JOIN pegasus.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN dbo.TransactionStatus ts ON ts.DataSourceID = @Pegasus_DS AND ts.sourceKey = t.TransactionStatusID
				LEFT JOIN dbo.TransactionDetail td ON td.DataSourceID = @Pegasus_DS AND td.sourceKey = t.TransactionDetailID
				LEFT JOIN map.[pegasus_CRS_BookingSource] bs ON bs.pegasusID = t.BookingSourceID
				LEFT JOIN map.[pegasus_Guest] g ON g.pegasusID = t.GuestID
				LEFT JOIN map.[pegasus_RateCode] rtc ON rtc.pegasusID = t.RateTypeCodeID
				LEFT JOIN map.[pegasus_PromoCode] pc ON pc.pegasusID = t.PromoCodeID
				LEFT JOIN map.[pegasus_IATANumber] i ON i.pegasusID = t.IATANumberID
				LEFT JOIN map.[pegasus_hotel] h ON h.pegasusID = t.intHotelID
				LEFT JOIN map.[pegasus_Chain] ch ON ch.pegasusID = t.intChainID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
				LEFT JOIN (SELECT RateCodeID FROM dbo.RateCode WHERE RateCode = 'MKTIPM') MKTIPM ON MKTIPM.RateCodeID = rtc.RateTypeCodeID
			WHERE td.TransactionDetailID IS NOT NULL
				AND t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[QueueID] = src.[QueueID],
				[itineraryNumber] = src.[itineraryNumber],
				[transactionTimeStamp] = src.[transactionTimeStamp],
				[channelConnectConfirmationNumber] = src.[channelConnectConfirmationNumber],
				[timeLoaded] = src.[timeLoaded],
				[TransactionStatusID] = src.[TransactionStatusID],
				[TransactionDetailID] = src.[TransactionDetailID],
				[CorporateCodeID] = src.[CorporateCodeID],
				[RoomTypeID] = src.[RoomTypeID],
				[RateCategoryID] = src.[RateCategoryID],
				[VoiceAgentID] = src.UserNameID,
				[CRS_BookingSourceID] = src.[BookingSourceID],
				[LoyaltyNumberID] = ISNULL(NULLIF(src.[LoyaltyNumberID],624035),tgt.[LoyaltyNumberID]), --624035 is blank
				[GuestID] = src.[GuestID],
				[RateCodeID] = CASE WHEN IsMKTIPM = 0 THEN src.RateTypeCodeID ELSE tgt.[RateCodeID] END,
				[PromoCodeID] = src.[PromoCodeID],
				[TravelAgentID] = src.[TravelAgentID],
				[IATANumberID] = src.[IATANumberID],
				[HotelID] = src.intHotelID,
				[ChainID] = src.intChainID,
				[Guest_CompanyNameID] = src.[Guest_CompanyNameID],
				LoyaltyProgramID = src.LoyaltyProgramID,
				LoyaltyNumber_IsNewMember = src.LoyaltyNumber_IsNewMember,
				LoyaltyNumber_IsImputedFromEmail = src.LoyaltyNumber_IsImputedFromEmail
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],[VoiceAgentID],[CRS_BookingSourceID],[LoyaltyNumberID],[GuestID],[RateCodeID],[PromoCodeID],[TravelAgentID],[IATANumberID],[HotelID],[ChainID],[Guest_CompanyNameID],LoyaltyProgramID,LoyaltyNumber_IsNewMember,LoyaltyNumber_IsImputedFromEmail)
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,[QueueID],[itineraryNumber],[confirmationNumber],[transactionTimeStamp],[channelConnectConfirmationNumber],[timeLoaded],[TransactionStatusID],[TransactionDetailID],[CorporateCodeID],[RoomTypeID],[RateCategoryID],UserNameID,[BookingSourceID],[LoyaltyNumberID],[GuestID],RateTypeCodeID,[PromoCodeID],[TravelAgentID],[IATANumberID],intHotelID,intChainID,[Guest_CompanyNameID],LoyaltyProgramID,LoyaltyNumber_IsNewMember,LoyaltyNumber_IsImputedFromEmail)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_TransactionDetail]'
GO



ALTER PROCEDURE [dbo].[Populate_TransactionDetail]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int, @OpenHosp_DS int, @Pegasus_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'
	SELECT @OpenHosp_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality'
	SELECT @Pegasus_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus'

	MERGE INTO [dbo].[TransactionDetail] AS tgt
	USING
	(
		SELECT [synXisID],[synXisTransExID],[openHospID],[pegasusID],[confirmationNumber],CASE WHEN [nights] = 0 THEN 1 ELSE [nights] END AS [nights],
			[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
			[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
			[departureDOW],[optIn],[IsPrimaryGuest],[DataSourceID],[billingDescription]
		FROM
		(
			SELECT td.[TransactionDetailID] AS [synXisID],x.TransactionsExtendedID AS [synXisTransExID],NULL AS [openHospID],NULL AS [pegasusID],t.[confirmationNumber],[nights],
					[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
					[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
					[departureDOW],x.[optIn],[IsPrimaryGuest],@SynXis_DS AS [DataSourceID],bd.[billingDescription]
			FROM synxis.TransactionDetail td
				INNER JOIN synxis.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN synxis.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				INNER JOIN
					(
						SELECT DISTINCT t.TransactionID,te.TransactionsExtendedID,
									CASE te.optIn WHEN 'Y' THEN 1 ELSE 0 END AS optIn,
									te.IsPrimaryGuest
						FROM synxis.TransactionsExtended te
							INNER JOIN synxis.Transactions t ON t.TransactionsExtendedID = te.TransactionsExtendedID
					) x ON x.TransactionID = mrt.TransactionID
				LEFT JOIN synxis.BillingDescription bd ON bd.BillingDescriptionID = t.BillingDescriptionID
			WHERE t.QueueID = @QueueID

				UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [synXisTransExID],td.[TransactionDetailID] AS [openHospID], NULL AS [pegasusID],
				CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber],
				[nights],
				NULL AS [averageDailyRate],[rooms],[reservationRevenue],[currency],NULL AS [totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],NULL AS [commisionPercent],[arrivalDate],[departureDate],NULL AS [bookingLeadTime],NULL AS [arrivalDOW],
				NULL AS [departureDOW],NULL AS [optIn],NULL AS [IsPrimaryGuest],@OpenHosp_DS AS [DataSourceID],NULL AS [billingDescription]
			FROM openHosp.TransactionDetail td
				INNER JOIN openHosp.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN openHosp.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE t.QueueID = @QueueID

				UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [synXisTransExID], NULL AS [openHospID], td.[TransactionDetailID] AS [pegasusID],
				CASE WHEN dups.confirmationNumber IS NULL THEN t.[confirmationNumber] ELSE t.[confirmationNumber] + '_OH' END AS [confirmationNumber], ----should '_OH' remain the same
				[nights],
				NULL AS [averageDailyRate],[rooms],[reservationRevenue],[currency],NULL AS [totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],NULL AS [commisionPercent],[arrivalDate],[departureDate],NULL AS [bookingLeadTime],NULL AS [arrivalDOW],
				NULL AS [departureDOW],NULL AS [optIn],NULL AS [IsPrimaryGuest],@Pegasus_DS AS [DataSourceID],NULL AS [billingDescription]
			FROM pegasus.TransactionDetail td
				INNER JOIN pegasus.Transactions t ON t.TransactionDetailID = td.TransactionDetailID
				INNER JOIN pegasus.MostRecentTransaction mrt ON mrt.TransactionID = t.TransactionID
				LEFT JOIN operations.DupConfNumbers dups ON dups.confirmationNumber = t.confirmationNumber
			WHERE t.QueueID = @QueueID
		) x
	) AS src ON src.[confirmationNumber] = tgt.[confirmationNumber] AND src.[DataSourceID] = tgt.[DataSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [sourceKey] = CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[synXisTransExID] = src.[synXisTransExID],
				[nights] = src.[nights],
				[averageDailyRate] = src.[averageDailyRate],
				[rooms] = src.[rooms],
				[reservationRevenue] = src.[reservationRevenue],
				[currency] = src.[currency],
				[totalPackageRevenue] = src.[totalPackageRevenue],
				[totalGuestCount] = src.[totalGuestCount],
				[adultCount] = src.[adultCount],
				[childrenCount] = src.[childrenCount],
				[commisionPercent] = src.[commisionPercent],
				[arrivalDate] = src.[arrivalDate],
				[departureDate] = src.[departureDate],
				[bookingLeadTime] = src.[bookingLeadTime],
				[arrivalDOW] = src.[arrivalDOW],
				[departureDOW] = src.[departureDOW],
				[optIn] = src.[optIn],
				[IsPrimaryGuest] = src.[IsPrimaryGuest],
				[billingDescription] = src.[billingDescription]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([DataSourceID],[sourceKey],
				[synXisTransExID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[IsPrimaryGuest],[billingDescription])
		VALUES([DataSourceID],CASE src.[DataSourceID] WHEN @SynXis_DS THEN src.[synXisID] WHEN @OpenHosp_DS THEN src.[openHospID] WHEN @Pegasus_DS THEN src.[pegasusID] ELSE NULL END,
				[synXisTransExID],[confirmationNumber],[nights],
				[averageDailyRate],[rooms],[reservationRevenue],[currency],[totalPackageRevenue],[totalGuestCount],
				[adultCount],[childrenCount],[commisionPercent],[arrivalDate],[departureDate],[bookingLeadTime],[arrivalDOW],
				[departureDOW],[optIn],[IsPrimaryGuest],[billingDescription])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_RoomType]'
GO



ALTER PROCEDURE [dbo].[Populate_RoomType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[RoomType] AS tgt
	USING
	(
		SELECT STRING_AGG([RoomTypeID],',') AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[roomTypeName],[roomTypeCode]
		FROM synxis.RoomType
		WHERE [RoomTypeID] IN(SELECT [RoomTypeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		GROUP BY [roomTypeName],[roomTypeCode]
	) AS src ON src.[roomTypeName] = tgt.[roomTypeName] AND src.[roomTypeCode] = tgt.[roomTypeCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[roomTypeName],[roomTypeCode])
		VALUES([synXisID],[openHospID],[pegasusID],[roomTypeName],[roomTypeCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [map].[Populate_pegasus_mapTables]'
GO




CREATE PROCEDURE [map].[Populate_pegasus_mapTables]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000)

	-- map.[pegasus_CRS_BookingSource] -----------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_CRS_BookingSource]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_CRS_BookingSource] AS tgt --(pegasusID,BookingSourceID)
		USING
		(
			SELECT value,BookingSourceID
			FROM dbo.CRS_BookingSource
				CROSS APPLY string_split(pegasusID,',')
			WHERE IsPegasus = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.BookingSourceID = tgt.BookingSourceID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],BookingSourceID)
			VALUES([value],BookingSourceID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------

	-- map.[pegasus_Guest] -----------------------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_Guest]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_Guest] AS tgt --(pegasusID,GuestID)
		USING
		(
			SELECT DISTINCT og.GuestID AS [value],dg.GuestID
			FROM dbo.Guest dg
				INNER JOIN pegasus.Guest og ON og.[GuestID] = dg.[pegasusID]
			WHERE dg.IsPegasus = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.GuestID = tgt.GuestID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],GuestID)
			VALUES([value],GuestID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------

	-- map.[pegasus_RateCode] --------------------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_RateCode]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_RateCode] AS tgt --(pegasusID,RateTypeCodeID)
		USING
		(
			SELECT value,RateCodeID
			FROM dbo.RateCode
				CROSS APPLY string_split(pegasusID,',')
			WHERE IsPegasus = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.RateCodeID = tgt.[RateTypeCodeID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],[RateTypeCodeID])
			VALUES([value],RateCodeID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------

	-- map.[pegasus_PromoCode] -------------------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_PromoCode]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_PromoCode] AS tgt --(pegasusID,PromoCodeID)
		USING
		(
			SELECT value,PromoCodeID
			FROM dbo.PromoCode
				CROSS APPLY string_split(pegasusID,',')
			WHERE IsPegasus = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.PromoCodeID = tgt.PromoCodeID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],PromoCodeID)
			VALUES([value],PromoCodeID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------

	-- map.[pegasus_IATANumber] ------------------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_IATANumber]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_IATANumber] AS tgt --(pegasusID,IATANumberID)
		USING
		(
			SELECT value,IATANumberID
			FROM dbo.IATANumber
				CROSS APPLY string_split(pegasusID,',')
			WHERE IsPegasus = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.IATANumberID = tgt.IATANumberID
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],IATANumberID)
			VALUES([value],IATANumberID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------

	-- map.[pegasus_hotel] -----------------------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_hotel]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_hotel] AS tgt --(pegasusID,intHotelID)
		USING
		(
			SELECT value,HotelID
			FROM
			(
				SELECT value,h.HotelID,ROW_NUMBER() OVER(PARTITION BY value ORDER BY hh.CRM_HotelID DESC) rowNum
				FROM dbo.hotel h
					INNER JOIN Hotels.dbo.Hotel hh ON hh.HotelID = h.Hotel_hotelID
					CROSS APPLY string_split(h.pegasusID,',')
				 WHERE h.IsPegasus = 1
			) x
			WHERE rowNum = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.HotelID = tgt.[intHotelID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],[intHotelID])
			VALUES([value],HotelID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------

	-- map.[pegasus_Chain] -----------------------------------------
		-- PRINT STATUS --
		SET @print = ' ' + CONVERT(varchar(100),GETDATE(),120) + ': map.[pegasus_Chain]'
		RAISERROR(@print,10,1) WITH NOWAIT

		MERGE INTO map.[pegasus_Chain] AS tgt --(pegasusID,intChainID)
		USING
		(
			SELECT value,ChainID
			FROM dbo.Chain
				CROSS APPLY string_split(pegasusID,',')
			WHERE IsPegasus = 1
		) AS src ON src.[value] = tgt.[pegasusID] AND src.ChainID = tgt.[intChainID]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([pegasusID],[intChainID])
			VALUES([value],ChainID)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE
		;
	-----------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [map].[Populate_mapTables]'
GO


ALTER PROCEDURE [map].[Populate_mapTables]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	EXEC [map].[Populate_synxis_mapTables] @QueueID

	EXEC [map].[Populate_openhosp_mapTables] @QueueID

	EXEC [map].[Populate_pegasus_mapTables] @QueueID
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_LoyaltyProgram]'
GO



ALTER PROCEDURE [dbo].[Populate_LoyaltyProgram]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[LoyaltyProgram] AS tgt
	USING
	(
		SELECT DISTINCT [LoyaltyProgramID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[loyaltyProgram]
		FROM synxis.LoyaltyProgram
		WHERE [LoyaltyProgramID] IN(SELECT [LoyaltyProgramID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[loyaltyProgram] = tgt.[loyaltyProgram]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[loyaltyProgram])
		VALUES([synXisID],[openHospID],[pegasusID],[loyaltyProgram])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_LoyaltyNumber]'
GO



ALTER PROCEDURE [dbo].[Populate_LoyaltyNumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[LoyaltyNumber] AS tgt
	USING
	(
		SELECT DISTINCT [LoyaltyNumberID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[loyaltyNumber]
		FROM synxis.LoyaltyNumber
		WHERE [LoyaltyNumberID] IN(SELECT [LoyaltyNumberID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
			AND [loyaltyNumber] != ''
	) AS src ON src.[loyaltyNumber] = tgt.[loyaltyNumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[loyaltyNumber])
		VALUES([synXisID],[openHospID],[pegasusID],[loyaltyNumber])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Location]'
GO




ALTER PROCEDURE [dbo].[Populate_Location]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
	
	-- DEFAULT VALUES -------------------------------------------
	DECLARE @CityID int,@StateID int,@CountryID int,@PostalCodeID int,@AreaID int,@RegionID int

	SELECT @CityID = CityID FROM dbo.City WHERE City_Text = 'unknown'
	SELECT @StateID = StateID FROM dbo.[State] WHERE State_Text = 'unknown'
	SELECT @CountryID = CountryID FROM dbo.Country WHERE Country_Text = 'unknown'
	SELECT @PostalCodeID = PostalCodeID FROM dbo.PostalCode WHERE PostalCode_Text = 'unknown'
	SELECT @AreaID = AreaID FROM dbo.Area WHERE Area_Text = 'unknown'
	SELECT @RegionID = RegionID FROM dbo.Region WHERE Region_Text = 'unknown'
	-------------------------------------------------------------

	MERGE INTO [dbo].[Location] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[CityID],[StateID],[CountryID],[PostalCodeID],[AreaID],[RegionID]
		FROM
		(
			SELECT [LocationID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],
				ISNULL(c.[CityID],@CityID) AS CityID,
				ISNULL(s.[StateID],@StateID) AS StateID,
				ISNULL(cn.[CountryID],@CountryID) AS CountryID,
				ISNULL(p.[PostalCodeID],@PostalCodeID) AS PostalCodeID,
				ISNULL(a.[AreaID],@AreaID) AS AreaID,
				ISNULL(r.[RegionID],@RegionID) AS RegionID
			FROM synxis.[Location] l
				LEFT JOIN (SELECT [CityID],value AS synXisID FROM dbo.City CROSS APPLY string_split(synXisID,',')) c ON c.synXisID = l.CityID
				LEFT JOIN (SELECT [StateID],value AS synXisID FROM dbo.[State] CROSS APPLY string_split(synXisID,',')) s ON s.synXisID = l.StateID
				LEFT JOIN (SELECT [CountryID],value AS synXisID FROM dbo.Country CROSS APPLY string_split(synXisID,',')) cn ON cn.synXisID = l.CountryID
				LEFT JOIN (SELECT [PostalCodeID],value AS synXisID FROM dbo.PostalCode CROSS APPLY string_split(synXisID,',')) p ON p.synXisID = l.PostalCodeID
				LEFT JOIN (SELECT [RegionID],value AS synXisID FROM dbo.Region CROSS APPLY string_split(synXisID,',')) r ON r.synXisID = l.RegionID
				LEFT JOIN (SELECT [AreaID],value AS synXisID FROM dbo.Area CROSS APPLY string_split(synXisID,',')) a ON a.synXisID = l.[AreaID]
			WHERE [LocationID] IN
								(
								SELECT DISTINCT [LocationID] FROM [synxis].[Transactions] t INNER JOIN [synxis].[Guest] g ON g.[GuestID] = t.[GuestID] WHERE QueueID = @QueueID
									UNION
								SELECT DISTINCT [LocationID] FROM [synxis].[Transactions] t INNER JOIN [synxis].[TravelAgent] ta ON ta.[TravelAgentID] = t.[TravelAgentID] WHERE QueueID = @QueueID
								)

			UNION ALL

			SELECT NULL AS [synXisID],[LocationID] AS [openHospID],NULL AS [pegasusID],
				ISNULL(c.[CityID],@CityID) AS CityID,
				ISNULL(s.[StateID],@StateID) AS StateID,
				ISNULL(cn.[CountryID],@CountryID) AS CountryID,
				ISNULL(p.[PostalCodeID],@PostalCodeID) AS PostalCodeID,
				@AreaID AS [AreaID],
				@RegionID AS RegionID
			FROM openHosp.[Location] l
				LEFT JOIN (SELECT [CityID],value AS openHospID FROM dbo.City CROSS APPLY string_split(openHospID,',')) c ON c.openHospID = l.CityID
				LEFT JOIN (SELECT [StateID],value AS openHospID FROM dbo.[State] CROSS APPLY string_split(openHospID,',')) s ON s.openHospID = l.StateID
				LEFT JOIN (SELECT [CountryID],value AS openHospID FROM dbo.Country CROSS APPLY string_split(openHospID,',')) cn ON cn.openHospID = l.CountryID
				LEFT JOIN (SELECT [PostalCodeID],value AS openHospID FROM dbo.PostalCode CROSS APPLY string_split(openHospID,',')) p ON p.openHospID = l.PostalCodeID
			WHERE [LocationID] IN
								(
								SELECT DISTINCT [LocationID] FROM openHosp.[Transactions] t INNER JOIN openHosp.[Guest] g ON g.[GuestID] = t.[GuestID] WHERE QueueID = @QueueID
								)
			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],[LocationID] AS [pegasusID],
				ISNULL(c.[CityID],@CityID) AS CityID,
				ISNULL(s.[StateID],@StateID) AS StateID,
				ISNULL(cn.[CountryID],@CountryID) AS CountryID,
				ISNULL(p.[PostalCodeID],@PostalCodeID) AS PostalCodeID,
				@AreaID AS [AreaID],
				@RegionID AS RegionID
			FROM pegasus.[Location] l
				LEFT JOIN (SELECT [CityID],value AS pegasusID FROM dbo.City CROSS APPLY string_split(pegasusID,',')) c ON c.pegasusID = l.CityID
				LEFT JOIN (SELECT [StateID],value AS pegasusID FROM dbo.[State] CROSS APPLY string_split(pegasusID,',')) s ON s.pegasusID = l.StateID
				LEFT JOIN (SELECT [CountryID],value AS pegasusID FROM dbo.Country CROSS APPLY string_split(pegasusID,',')) cn ON cn.pegasusID = l.CountryID
				LEFT JOIN (SELECT [PostalCodeID],value AS pegasusID FROM dbo.PostalCode CROSS APPLY string_split(pegasusID,',')) p ON p.pegasusID = l.PostalCodeID
			WHERE [LocationID] IN
								(
								SELECT DISTINCT [LocationID] FROM pegasus.[Transactions] t INNER JOIN pegasus.[Guest] g ON g.[GuestID] = t.[GuestID] WHERE QueueID = @QueueID
								)
		) X
		GROUP BY [CityID],[StateID],[CountryID],[PostalCodeID],[AreaID],[RegionID]
	) AS src ON src.[CityID] = tgt.[CityID]
				AND src.[StateID] = tgt.[StateID]
				AND src.[CountryID] = tgt.[CountryID]
				AND src.[PostalCodeID] = tgt.[PostalCodeID]
				AND src.[AreaID] = tgt.[AreaID]
				AND src.[RegionID] = tgt.[RegionID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[CityID],[StateID],[CountryID],[PostalCodeID],[AreaID],[RegionID])
		VALUES([synXisID],[openHospID],[pegasusID],[CityID],[StateID],[CountryID],[PostalCodeID],[AreaID],[RegionID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_ibeSource]'
GO



ALTER PROCEDURE [dbo].[Populate_ibeSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[ibeSource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],ibe.ibeSourceID
		FROM
		(
			SELECT [xbeTemplateID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[xbeTemplateName]
			FROM synxis.xbeTemplate
			WHERE [xbeTemplateID] IN(
								SELECT DISTINCT [xbeTemplateID]
								FROM synxis.Transactions t
									INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
								WHERE t.QueueID = @QueueID
								)
				UNION ALL
			SELECT NULL AS [synXisID],bs.BookingSourceID AS [openHospID],NULL AS [pegasusID],obs.Template AS [xbeTemplateName]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
				
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],pbs.BookingSourceID AS [pegasusID],apbs.Template AS [xbeTemplateName]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
				
		) x
		INNER JOIN authority.ibeSource ibe ON ibe.ibeSourceName = x.xbeTemplateName
		GROUP BY ibe.ibeSourceID
	) AS src ON src.ibeSourceID = tgt.[auth_ibeSourceID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[auth_ibeSourceID])
		VALUES([synXisID],[openHospID],[pegasusID],ibeSourceID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_IATANumber]'
GO



ALTER PROCEDURE [dbo].[Populate_IATANumber]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[IATANumber] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[IATANumber]
		FROM
		(
			SELECT [IATANumberID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[IATANumber]
			FROM synxis.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],[IATANumberID] AS [openHospID],NULL AS [pegasusID],[IATANumber]
			FROM openHosp.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[IATANumberID] AS [pegasusID],[IATANumber]
			FROM pegasus.IATANumber
			WHERE [IATANumberID] IN(SELECT [IATANumberID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		WHERE ISNULL(IATANumber,'') != ''
		GROUP BY [IATANumber]
	) AS src ON src.[IATANumber] = tgt.[IATANumber]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[IATANumber])
		VALUES([synXisID],[openHospID],[pegasusID],[IATANumber])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_hotel]'
GO



ALTER PROCEDURE [dbo].[Populate_hotel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	-- ADD MISSING HOTELS to Hotels.dbo.Hotel -----------------
/*
	;WITH cte_Code(HotelCode)
	AS
	(
		SELECT HotelCode FROM Reservations.openHosp.hotel
			EXCEPT
		SELECT OpenHospCode FROM Hotels.dbo.Hotel
	)
	MERGE INTO Hotels.dbo.Hotel AS tgt
	USING
	(
		SELECT h.HotelCode,h.HotelName,h.HotelId AS OpenHospID,h.HotelCode AS OpenHospCode
		FROM Reservations.openHosp.hotel h
			INNER JOIN cte_Code c ON c.HotelCode = h.HotelCode
	) AS src ON src.HotelCode = tgt.HotelCode
	WHEN MATCHED THEN
		UPDATE
			SET OpenHospID = src.OpenHospID,
				OpenHospCode = src.OpenHospCode
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (HotelCode,HotelName,OpenHospID,OpenHospCode)
		VALUES(src.HotelCode,src.HotelName,src.OpenHospID,src.OpenHospCode)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;


	;WITH cte_Code(HotelId)
	AS
	(
		SELECT HotelId FROM Reservations.synXis.hotel
			EXCEPT
		SELECT SynXisID FROM Hotels.dbo.Hotel
	)
	MERGE INTO Hotels.dbo.Hotel AS tgt
	USING
	(
		SELECT h.HotelCode,h.HotelName,h.HotelId AS SynXisID,h.HotelCode AS SynXisCode
		FROM Reservations.synXis.hotel h
			INNER JOIN cte_Code c ON c.HotelId = h.HotelId
	) AS src ON src.HotelCode = tgt.HotelCode
	WHEN MATCHED THEN
		UPDATE
			SET SynXisID = src.SynXisID,
				SynXisCode = src.SynXisCode
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(HotelCode,HotelName,SynXisID,SynXisCode)
		VALUES(src.HotelCode,src.HotelName,src.SynXisID,src.SynXisCode)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
*/
	-----------------------------------------------------------

	MERGE INTO [dbo].[hotel] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[HotelId]
		FROM
		(
			SELECT [intHotelID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],MAX(shh.[HotelId]) AS [HotelId]
			FROM synxis.hotel sh
				INNER JOIN Hotels.[dbo].[Hotel] shh ON shh.[SynXisID] = sh.[HotelId]
			WHERE [intHotelID] IN(SELECT [intHotelID] FROM [synxis].[Transactions])
			GROUP BY [intHotelID]
				UNION ALL
			SELECT NULL AS [synXisID],[intHotelID] AS [openHospID],NULL AS [pegasusID],MAX(ISNULL(ohID.[HotelId],ohCode.[HotelId])) AS [HotelId]
			FROM openHosp.hotel oh
				LEFT JOIN Hotels.[dbo].[Hotel] ohID ON ohID.[OpenHospID] = oh.[HotelId]
				LEFT JOIN Hotels.[dbo].[Hotel] ohCode ON ohCode.OpenHospCode = oh.HotelCode
			WHERE ISNULL(ohID.[HotelId],ohCode.[HotelId]) IS NOT NULL
				AND [intHotelID] IN(SELECT [intHotelID] FROM openHosp.[Transactions])
			GROUP BY [intHotelID]
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[intHotelID] AS [pegasusID],MAX(ISNULL(ohID.[HotelId],ohCode.[HotelId])) AS [HotelId]
			FROM pegasus.hotel oh
				LEFT JOIN Hotels.[dbo].[Hotel] ohID ON ohID.PegasusRT4ID = oh.[HotelId]
				LEFT JOIN Hotels.[dbo].[Hotel] ohCode ON ohCode.OpenHospCode = oh.HotelCode
			WHERE ISNULL(ohID.[HotelId],ohCode.[HotelId]) IS NOT NULL
				AND [intHotelID] IN(SELECT [intHotelID] FROM pegasus.[Transactions])
			GROUP BY [intHotelID]
		) x
		GROUP BY [HotelId]
	) AS src ON src.[HotelId] = tgt.[Hotel_hotelID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[Hotel_hotelID])
		VALUES([synXisID],[openHospID],[pegasusID],[HotelId])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_EmailAddress]'
GO




ALTER PROCEDURE [dbo].[Populate_Guest_EmailAddress]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Guest_EmailAddress] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[emailAddress]
		FROM
		(
			SELECT [Guest_EmailAddressID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[emailAddress]
			FROM synxis.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM [synxis].[Transactions] t
												INNER JOIN [synxis].[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
				UNION ALL
			SELECT NULL AS [synXisID],[Guest_EmailAddressID] AS [openHospID],NULL AS [pegasusID],[emailAddress]
			FROM openHosp.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM openHosp.[Transactions] t
												INNER JOIN openHosp.[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[Guest_EmailAddressID] AS [pegasusID],[emailAddress]
			FROM pegasus.Guest_EmailAddress
			WHERE [Guest_EmailAddressID] IN
											(
											SELECT [Guest_EmailAddressID]
											FROM pegasus.[Transactions] t
												INNER JOIN pegasus.[Guest] g ON g.[GuestID] = t.[GuestID]
											WHERE QueueID = @QueueID
											)
		) x
		GROUP BY [emailAddress]
	) AS src ON src.[emailAddress] = tgt.[emailAddress]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[emailAddress])
		VALUES([synXisID],[openHospID],[pegasusID],[emailAddress])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest_CompanyName]'
GO




ALTER PROCEDURE [dbo].[Populate_Guest_CompanyName]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Guest_CompanyName] AS tgt
	USING
	(
		SELECT DISTINCT [Guest_CompanyNameID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[CompanyName]
		FROM synxis.Guest_CompanyName
		WHERE [Guest_CompanyNameID] IN(SELECT [Guest_CompanyNameID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
	) AS src ON src.[CompanyName] = tgt.[CompanyName]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[CompanyName])
		VALUES([synXisID],[openHospID],[pegasusID],[CompanyName])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest]'
GO




ALTER PROCEDURE [dbo].[Populate_Guest]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;
	
	-- #SYNXIS_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#SYNXIS_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #SYNXIS_Guest_EmailAddress;
	CREATE TABLE #SYNXIS_Guest_EmailAddress([Guest_EmailAddressID] int,synXisID int NOT NULL, PRIMARY KEY CLUSTERED(synXisID))
	
	INSERT INTO #SYNXIS_Guest_EmailAddress(Guest_EmailAddressID,synXisID)
	SELECT [Guest_EmailAddressID], value AS synXisID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(synXisID,',')
	------------------------------------------------------
	
	-- #OPEN_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#OPEN_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #OPEN_Guest_EmailAddress;
	CREATE TABLE #OPEN_Guest_EmailAddress([Guest_EmailAddressID] int,openHospID int NOT NULL, PRIMARY KEY CLUSTERED(openHospID))
	
	INSERT INTO #OPEN_Guest_EmailAddress(Guest_EmailAddressID,openHospID)
	SELECT [Guest_EmailAddressID], value AS openHospID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(openHospID,',')
	------------------------------------------------------

	-- #PEGASUS_Guest_EmailAddress ------------------------
	IF OBJECT_ID('tempdb..#PEGASUS_Guest_EmailAddress') IS NOT NULL
		DROP TABLE #PEGASUS_Guest_EmailAddress;
	CREATE TABLE #PEGASUS_Guest_EmailAddress([Guest_EmailAddressID] int,pegasusID int NOT NULL, PRIMARY KEY CLUSTERED(pegasusID))
	
	INSERT INTO #PEGASUS_Guest_EmailAddress(Guest_EmailAddressID,pegasusID)
	SELECT [Guest_EmailAddressID], value AS pegasusID FROM dbo.Guest_EmailAddress CROSS APPLY string_split(pegasusID,',')
	------------------------------------------------------
	
	-- #SYNXIS_Location ------------------------
	IF OBJECT_ID('tempdb..#SYNXIS_Location') IS NOT NULL
		DROP TABLE #SYNXIS_Location;
	CREATE TABLE #SYNXIS_Location([LocationID] int,synXisID int NOT NULL, PRIMARY KEY CLUSTERED(synXisID))
	
	INSERT INTO #SYNXIS_Location([LocationID],synXisID)
	SELECT [LocationID], value AS synXisID FROM dbo.[Location] CROSS APPLY string_split(synXisID,',')
	------------------------------------------------------ 
	
	-- #OPEN_Location ------------------------
	IF OBJECT_ID('tempdb..#OPEN_Location') IS NOT NULL
		DROP TABLE #OPEN_Location;
	CREATE TABLE #OPEN_Location([LocationID] int,openHospID int NOT NULL, PRIMARY KEY CLUSTERED(openHospID))
	
	INSERT INTO #OPEN_Location([LocationID],openHospID)
	SELECT [LocationID], value AS openHospID FROM dbo.[Location] CROSS APPLY string_split(openHospID,',')
	------------------------------------------------------

	-- #PEGASUS_Location ------------------------
	IF OBJECT_ID('tempdb..#PEGASUS_Location') IS NOT NULL
		DROP TABLE #PEGASUS_Location;
	CREATE TABLE #PEGASUS_Location([LocationID] int,pegasusID int NOT NULL, PRIMARY KEY CLUSTERED(pegasusID))
	
	INSERT INTO #PEGASUS_Location([LocationID],pegasusID)
	SELECT [LocationID], value AS pegasusID FROM dbo.[Location] CROSS APPLY string_split(pegasusID,',')
	------------------------------------------------------
	
	MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
		SELECT MAX([synXisID]) AS [synXisID],MAX([openHospID]) AS [openHospID],MAX([pegasusID]) AS [pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID],
				HASHBYTES('MD5',UPPER(ISNULL([customerID],'')) + '|' + UPPER(ISNULL([salutation],'')) + '|' + UPPER(ISNULL([FirstName],'')) + '|' + UPPER(ISNULL([FirstName],'')) + '|' + UPPER(ISNULL([LastName],'')) + '|' + UPPER(ISNULL([Address1],'')) + '|' + UPPER(ISNULL([Address2],'')) + '|' + UPPER(ISNULL(CONVERT(varchar(20),[LocationID]),'')) + '|' + UPPER(ISNULL([phone],'')) + '|' + UPPER(ISNULL(CONVERT(varchar(20),[Guest_EmailAddressID]),''))) AS hashKey
		FROM
		(
			SELECT [GuestID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM synxis.Guest g
				LEFT JOIN #SYNXIS_Guest_EmailAddress ge ON ge.synXisID = g.Guest_EmailAddressID
				LEFT JOIN #SYNXIS_Location l ON l.synXisID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
			
			UNION ALL
			
			SELECT NULL AS [synXisID],[GuestID] AS [openHospID],NULL AS [pegasusID],NULL AS [customerID],NULL AS [salutation],[FirstName],[LastName],[Address1],NULL AS [Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM openHosp.Guest g
				LEFT JOIN #OPEN_Guest_EmailAddress ge ON ge.openHospID = g.Guest_EmailAddressID
				LEFT JOIN #OPEN_Location l ON l.openHospID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)

			UNION ALL
			
			SELECT NULL AS [synXisID],NULL AS [openHospID],[GuestID] AS [pegasusID],NULL AS [customerID],NULL AS [salutation],[FirstName],[LastName],[Address1],NULL AS [Address2],l.[LocationID],[phone],ge.[Guest_EmailAddressID]
			FROM pegasus.Guest g
				LEFT JOIN #PEGASUS_Guest_EmailAddress ge ON ge.pegasusID = g.Guest_EmailAddressID
				LEFT JOIN #PEGASUS_Location l ON l.pegasusID = g.[LocationID]
			WHERE [GuestID] IN(SELECT [GuestID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID]
	) AS src ON src.hashKey = tgt.hashKey
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID])
		VALUES([synXisID],[openHospID],[pegasusID],[customerID],[salutation],[FirstName],[LastName],[Address1],[Address2],[LocationID],[phone],[Guest_EmailAddressID])
	--WHEN NOT MATCHED BY SOURCE THEN
	-- DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_SubSource]'
GO



ALTER PROCEDURE [dbo].[Populate_CRS_SubSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_SubSource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG(openHospID,',') AS openHospID,STRING_AGG(pegasusID,',') AS pegasusID,[subSource],[subSourceCode]
		FROM
		(
			SELECT [SubSourceID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[subSource],[subSourceCode]
			FROM synxis.SubSource
			WHERE [SubSourceID] IN(
										SELECT DISTINCT [SubSourceID]
										FROM synxis.Transactions t
											INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
										WHERE t.QueueID = @QueueID
										)

				UNION ALL

			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],NULL AS [pegasusID],bs.Bkg_Src_Cd AS [subSource],obs.Sub_Source AS [subSourceCode]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)

				UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],BookingSourceID AS [pegasusID],pbs.Bkg_Src_Cd AS [subSource],apbs.Sub_Source AS [subSourceCode]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [subSource],[subSourceCode]
	) AS src ON src.[subSource] = tgt.[subSource] AND src.[subSourceCode] = tgt.[subSourceCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				openHospID = src.openHospID,
				pegasusID = src.pegasusID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],openHospID,pegasusID,[subSource],[subSourceCode])
		VALUES([synXisID],openHospID,pegasusID,[subSource],[subSourceCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_SecondarySource]'
GO



ALTER PROCEDURE [dbo].[Populate_CRS_SecondarySource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_SecondarySource] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[secondarySource]
		FROM
		(
			SELECT [SecondarySourceID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[secondarySource]
			FROM synxis.SecondarySource
			WHERE [SecondarySourceID] IN(
									SELECT DISTINCT [SecondarySourceID]
									FROM synxis.Transactions t
										INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
									WHERE t.QueueID = @QueueID
									)

				UNION ALL
			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],NULL AS [pegasusID],obs.Secondary_Source AS [secondarySource]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)
			
			UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],BookingSourceID AS [pegasusID],apbs.Secondary_Source AS [secondarySource]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.Pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [secondarySource]
	) AS src ON src.[secondarySource] = tgt.[secondarySource]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
	INSERT([synXisID],[openHospID],[pegasusID],[secondarySource])
	VALUES([synXisID],[openHospID],[pegasusID],[secondarySource])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_Channel]'
GO




ALTER PROCEDURE [dbo].[Populate_CRS_Channel]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_Channel] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[channel]
		FROM
		(
			SELECT [ChannelID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[channel]
			FROM synxis.Channel
			WHERE [ChannelID] IN(
									SELECT DISTINCT bs.[ChannelID]
									FROM synxis.Transactions t
										INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
									WHERE t.QueueID = @QueueID
								)

				UNION ALL

			SELECT NULL AS [synXisID],BookingSourceID AS [openHospID],NULL AS [pegasusID],obs.Channel
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obs ON obs.Bkg_Src_Cd = bs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM openHosp.Transactions t WHERE t.QueueID = @QueueID)

				UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],BookingSourceID AS [pegasusID],apbs.Channel
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.Pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
			WHERE [BookingSourceID] IN(SELECT DISTINCT [BookingSourceID] FROM pegasus.Transactions t WHERE t.QueueID = @QueueID)
		) x
		GROUP BY [channel]
	) AS src ON src.[channel] = tgt.[channel]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[channel])
		VALUES([synXisID],[openHospID],[pegasusID],[channel])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CRS_BookingSource]'
GO



ALTER PROCEDURE [dbo].[Populate_CRS_BookingSource]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CRS_BookingSource] tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID]
		FROM
		(
			SELECT sbs.[BookingSourceID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.[SubSourceID],dcro.[CROCodeID],dx.[ibeSourceID] AS [xbeTemplateID]
			FROM synxis.BookingSource sbs
				LEFT JOIN (SELECT [ChannelID], value AS synXisID FROM dbo.CRS_Channel CROSS APPLY string_split(synXisID,','))  dc ON dc.synXisID = sbs.ChannelID
				LEFT JOIN (SELECT [SecondarySourceID], value AS synXisID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(synXisID,',')) dss ON dss.synXisID = sbs.SecondarySourceID
				LEFT JOIN (SELECT [SubSourceID], value AS synXisID FROM dbo.CRS_SubSource CROSS APPLY string_split(synXisID,',')) ds ON ds.synXisID = sbs.SubSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS synXisID FROM dbo.CROCode CROSS APPLY string_split(synXisID,',')) dcro ON dcro.synXisID = sbs.CROCodeID
				LEFT JOIN (SELECT [ibeSourceID], value AS synXisID FROM dbo.ibeSource CROSS APPLY string_split(synXisID,',')) dx ON dx.synXisID = sbs.xbeTemplateID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				
			UNION ALL

			SELECT NULL AS [synXisID],obs.[BookingSourceID] AS [openHospID],NULL AS [pegasusID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.SubSourceID,dcro.CROCodeID AS [CROCodeID],dx.ibeSourceID AS [xbeTemplateID]
			FROM openHosp.BookingSource obs
				LEFT JOIN (SELECT [ChannelID], value AS openHospID FROM dbo.CRS_Channel CROSS APPLY string_split(openHospID,',')) dc ON dc.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [SecondarySourceID], value AS openHospID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(openHospID,',')) dss ON dss.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [SubSourceID], value AS openHospID FROM dbo.CRS_SubSource CROSS APPLY string_split(openHospID,',')) ds ON ds.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS openHospID FROM dbo.CROCode CROSS APPLY string_split(openHospID,',')) dcro ON dcro.openHospID = obs.BookingSourceID
				LEFT JOIN (SELECT [ibeSourceID], value AS openHospID FROM dbo.ibeSource CROSS APPLY string_split(openHospID,',')) dx ON dx.openHospID = obs.BookingSourceID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM openHosp.[Transactions] WHERE QueueID = @QueueID)

			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],pbs.[BookingSourceID] AS [pegasusID],dc.[ChannelID],dss.[SecondarySourceID],
					ds.SubSourceID,dcro.CROCodeID AS [CROCodeID],dx.ibeSourceID AS [xbeTemplateID]
			FROM pegasus.BookingSource pbs
				LEFT JOIN (SELECT [ChannelID], value AS pegasusID FROM dbo.CRS_Channel CROSS APPLY string_split(pegasusID,',')) dc ON dc.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [SecondarySourceID], value AS pegasusID FROM dbo.CRS_SecondarySource CROSS APPLY string_split(pegasusID,',')) dss ON dss.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [SubSourceID], value AS pegasusID FROM dbo.CRS_SubSource CROSS APPLY string_split(pegasusID,',')) ds ON ds.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [CROCodeID], value AS pegasusID FROM dbo.CROCode CROSS APPLY string_split(pegasusID,',')) dcro ON dcro.pegasusID = pbs.BookingSourceID
				LEFT JOIN (SELECT [ibeSourceID], value AS pegasusID FROM dbo.ibeSource CROSS APPLY string_split(pegasusID,',')) dx ON dx.pegasusID = pbs.BookingSourceID
			WHERE [BookingSourceID] IN(SELECT [BookingSourceID] FROM pegasus.[Transactions] WHERE QueueID = @QueueID)
		) X
		GROUP BY [ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID]
	) AS src ON ISNULL(src.[ChannelID],'') = ISNULL(tgt.[ChannelID],'') AND ISNULL(src.[SecondarySourceID],'') = ISNULL(tgt.[SecondarySourceID],'') AND ISNULL(src.[SubSourceID],'') = ISNULL(tgt.[SubSourceID],'')
				AND ISNULL(src.[CROCodeID],'') = ISNULL(tgt.[CROCodeID],'') AND ISNULL(src.[xbeTemplateID],'') = ISNULL(tgt.[ibeSourceNameID],'')
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[ibeSourceNameID])
		VALUES([synXisID],[openHospID],[pegasusID],[ChannelID],[SecondarySourceID],[SubSourceID],[CROCodeID],[xbeTemplateID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CROCode]'
GO



ALTER PROCEDURE [dbo].[Populate_CROCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CROCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[auth_CRO_CodeID]
		FROM
		(
			SELECT sCRO.[CROCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM synxis.CROCode sCRO
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = sCRO.CROCode
			WHERE [CROCodeID] IN(
								SELECT DISTINCT [CROCodeID]
								FROM synxis.Transactions t
									INNER JOIN [synxis].[BookingSource] bs ON bs.[BookingSourceID] = t.[BookingSourceID]
								WHERE t.QueueID = @QueueID
								)

			UNION ALL

			SELECT NULL AS [synXisID],bs.BookingSourceID AS [openHospID],NULL AS [pegasusID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM openHosp.BookingSource bs
				INNER JOIN authority.OpenHosp_BookingSource obh ON obh.Bkg_Src_Cd = bs.Bkg_Src_Cd
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = obh.CRO_Code
			WHERE obh.CRO_Code != ''

			UNION ALL

			SELECT NULL AS [synXisID],NULL AS [openHospID],pbs.BookingSourceID AS [pegasusID],aCRO.CRO_CodeID AS [auth_CRO_CodeID]
			FROM pegasus.BookingSource pbs
				INNER JOIN authority.Pegasus_BookingSource apbs ON apbs.Bkg_Src_Cd = pbs.Bkg_Src_Cd
				INNER JOIN authority.CRO_Code aCRO ON aCRO.CRO_Code = apbs.CRO_Code
			WHERE apbs.CRO_Code != ''
		) X
		GROUP BY [auth_CRO_CodeID]
	) AS src ON src.[auth_CRO_CodeID] = tgt.[auth_CRO_CodeID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[auth_CRO_CodeID])
		VALUES([synXisID],[openHospID],[pegasusID],[auth_CRO_CodeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Country]'
GO



ALTER PROCEDURE [dbo].[Populate_Country]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Country] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],ISNULL(NULLIF([Country_Text],''),'unknown') AS [Country_Text]
		FROM
		(
			SELECT [CountryID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[Country_Text]
			FROM synxis.Country
				UNION ALL
			SELECT NULL AS [synXisID],[CountryID] AS [openHospID],NULL AS [pegasusID],[Country_Text]
			FROM openHosp.Country
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[CountryID] AS [pegasusID],[Country_Text]
			FROM pegasus.Country
		) x
		GROUP BY [Country_Text]
	) AS src ON src.[Country_Text] = tgt.[Country_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[Country_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[Country_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CorporateCode]'
GO



ALTER PROCEDURE [dbo].[Populate_CorporateCode]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[CorporateCode] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[corporationCode]
		FROM
		(
			SELECT [CorporateCodeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[corporationCode]
			FROM synxis.CorporateCode
			WHERE [CorporateCodeID] IN(SELECT [CorporateCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],CorpInfoCodeID AS [openHospID],NULL AS [pegasusID],CorpInfoCode AS [corporationCode]
			FROM openHosp.CorpInfoCode
			WHERE [CorpInfoCodeID] IN(SELECT [CorpInfoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],CorpInfoCodeID AS [pegasusID],CorpInfoCode AS [corporationCode]
			FROM pegasus.CorpInfoCode
			WHERE [CorpInfoCodeID] IN(SELECT [CorpInfoCodeID] FROM [synxis].[Transactions] WHERE QueueID = @QueueID)
		) x
		GROUP BY [corporationCode]
	) AS src ON src.[corporationCode] = tgt.[corporationCode]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[corporationCode])
		VALUES([synXisID],[openHospID],[pegasusID],[corporationCode])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_City]'
GO



ALTER PROCEDURE [dbo].[Populate_City]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[City] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[City_Text]
		FROM
		(
			SELECT [CityID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([City_Text]),''),'unknown') AS [City_Text]
			FROM synxis.City
				UNION ALL
			SELECT NULL AS [synXisID],[CityID] AS [openHospID],NULL AS [pegasusID],ISNULL(NULLIF(TRIM([City_Text]),''),'unknown') AS [City_Text]
			FROM openHosp.City
				UNION ALL
			SELECT NULL AS [synXisID],NULL AS [openHospID],[CityID] AS [pegasusID],ISNULL(NULLIF(TRIM([City_Text]),''),'unknown') AS [City_Text]
			FROM pegasus.City
		) x
		GROUP BY [City_Text]
	) AS src ON src.[City_Text] = tgt.[City_Text]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[City_Text])
		VALUES([synXisID],[openHospID],[pegasusID],[City_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Chain]'
GO



ALTER PROCEDURE [dbo].[Populate_Chain]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Chain] AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID],[ChainName],[ChainID]
		FROM
			(
				SELECT [intChainID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[ChainName],[ChainID]
				FROM synxis.Chain
					UNION ALL
				SELECT NULL AS [synXisID],[intChainID] AS [openHospID],NULL AS [pegasusID],[ChainName],[ChainID]
				FROM openHosp.Chain
					UNION ALL
				SELECT NULL AS [synXisID],NULL AS [openHospID],[intChainID] AS [pegasusID],[ChainName],[ChainID]
				FROM pegasus.Chain
			) x
		GROUP BY [ChainName],[ChainID]
	) AS src ON src.[ChainID] = tgt.[CRS_ChainID]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID],
				[ChainName] = src.[ChainName]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[ChainName],[CRS_ChainID])
		VALUES(src.[synXisID],src.[openHospID],src.[pegasusID],src.[ChainName],src.[ChainID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_ActionType]'
GO


ALTER PROCEDURE [dbo].[Populate_ActionType]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	MERGE INTO dbo.ActionType AS tgt
	USING
	(
		SELECT STRING_AGG([synXisID],',') AS [synXisID],STRING_AGG([openHospID],',') AS [openHospID],STRING_AGG([pegasusID],',') AS [pegasusID]
				,[actionType],[actionTypeOrder]
		FROM
			(
				SELECT [ActionTypeID] AS [synXisID],NULL AS [openHospID],NULL AS [pegasusID],[actionType],[actionTypeOrder]
				FROM synxis.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM synxis.Transactions t
											INNER JOIN synxis.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
					UNION ALL
				SELECT NULL AS [synXisID],[ActionTypeID] AS [openHospID],NULL AS [pegasusID],[actionType],[actionTypeOrder]
				FROM openHosp.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM openHosp.Transactions t
											INNER JOIN openHosp.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
					UNION ALL
				SELECT NULL AS [synXisID],NULL AS [openHospID],[ActionTypeID] AS [pegasusID],[actionType],[actionTypeOrder]
				FROM pegasus.ActionType
				WHERE [ActionTypeID] IN(
										SELECT DISTINCT [ActionTypeID]
										FROM pegasus.Transactions t
											INNER JOIN pegasus.TransactionStatus ts ON ts.[TransactionStatusID] = t.[TransactionStatusID]
										WHERE t.QueueID = @QueueID
										)
			) x
		GROUP BY [actionType],[actionTypeOrder]
	) AS src ON src.[actionType] = tgt.[actionType]
	WHEN MATCHED THEN
		UPDATE
			SET [synXisID] = src.[synXisID],
				[openHospID] = src.[openHospID],
				[pegasusID] = src.[pegasusID],
				[actionTypeOrder] = src.[actionTypeOrder]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([synXisID],[openHospID],[pegasusID],[actionType],[actionTypeOrder])
		VALUES(src.[synXisID],src.[openHospID],src.[pegasusID],src.[actionType],src.[actionTypeOrder])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END

--ALTER TABLE [dbo].[ActionType]
--		ADD pegasusID varchar(MAX) NULL,
--			IsPegasus AS (CASE WHEN pegasusID IS NULL THEN 0 ELSE 1 END) Persisted
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_Voice_Brand]'
GO


ALTER PROCEDURE [dbo].[PH_BookingSource_Voice_Brand]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND ISNULL(sub.subSourceCode,'') != 'VCG'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND ISNULL(acro.CRO_Code_GroupID,0) NOT IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			
		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND ISNULL(acro.CRO_Code_GroupID,0) NOT IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',acro.CRO_Code,ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Voice')
			AND ISNULL(acro.CRO_Code_GroupID,0) IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND ISNULL(sub.subSourceCode,'') != 'VCG'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND ISNULL(acro.CRO_Code_GroupID,0) NOT IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'Voice - Brand',ISNULL(acro.CRO_Code,''),ISNULL(NULLIF(TRIM(sub.subSource),''),NULLIF(TRIM(sub.subSourceCode),'')),'#5AC8FA'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,CROCodeID,SubSourceID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			LEFT JOIN dbo.CROCode cro ON cro.CROCodeID = bs.CROCodeID
			LEFT JOIN authority.CRO_Code acro ON acro.CRO_CodeID = cro.auth_CRO_CodeID
			LEFT JOIN authority.CRO_Code_Group acrog ON acrog.CRO_Code_GroupID = acro.CRO_Code_GroupID
			LEFT JOIN dbo.CRS_SubSource sub ON sub.SubSourceID = bs.SubSourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND ISNULL(acro.CRO_Code_GroupID,0) NOT IN(SELECT CRO_Code_GroupID FROM authority.CRO_Code_Group WHERE CRO_Code_Group IN('PHG Call Center','HE Call Center'))
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PH_BookingSource_IBE_Brand]'
GO



ALTER PROCEDURE [dbo].[PH_BookingSource_IBE_Brand]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- CREATE #BOOKING ----------------------------------------------------
	IF OBJECT_ID('tempdb..#BOOKING') IS NULL
	BEGIN
		CREATE TABLE #BOOKING
		(
			[confirmationNumber] nvarchar(20) NOT NULL,
			[PH_Channel] nvarchar(255),
			[PH_SecondaryChannel] nvarchar(255),
			[PH_SubChannel] nvarchar(255),
			[Color] char(7),

			PRIMARY KEY CLUSTERED([confirmationNumber])
		)
	END
	-----------------------------------------------------------------------

	DELETE #BOOKING

	IF @QueueID IS NULL
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',autx.ibeSourceName,ss.secondarySource,'#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID IN(	SELECT ibeSource_GroupID
											FROM [authority].[ibeSource_Group]
											WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
										  )
			AND ss.secondarySource != 'IPrefer App'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','I Prefer Mobile App','IPrefer App','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine')
			AND ss.secondarySource = 'IPrefer App'

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			INNER JOIN authority.Pegasus_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
	END
	ELSE
	BEGIN
		INSERT INTO #BOOKING(confirmationNumber,PH_Channel,PH_SecondaryChannel,PH_SubChannel,[Color])
		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',autx.ibeSourceName,ss.secondarySource,'#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine','Mobile Web')
			AND autxo.ibeSource_GroupID IN(	SELECT ibeSource_GroupID
											FROM [authority].[ibeSource_Group]
											WHERE ibeSourceName IN('PHG Websites','IPrefer Websites','Preferred Golf Websites','HHA Websites','HHW Websites','Preferred Residences')
										  )
			AND ss.secondarySource != 'IPrefer App'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand','I Prefer Mobile App','IPrefer App','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN dbo.CRS_BookingSource bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN dbo.CRS_Channel c ON c.ChannelID = bs.ChannelID
			INNER JOIN dbo.ibeSource x ON x.ibeSourceID = bs.ibeSourceNameID
			INNER JOIN authority.ibeSource autx ON autx.ibeSourceID = x.auth_ibeSourceID
			INNER JOIN authority.ibeSource_Group autxo ON autxo.ibeSource_GroupID = autx.ibeSource_GroupID
			LEFT JOIN dbo.CRS_SecondarySource ss ON ss.SecondarySourceID = bs.SecondarySourceID
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis') AND t.sourceKey IS NOT NULL
			AND c.channel IN('Booking Engine')
			AND ss.secondarySource = 'IPrefer App'
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,value AS openHospID FROM dbo.CRS_BookingSource CROSS APPLY string_split(openHospID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN openHosp.BookingSource obs ON obs.BookingSourceID = bs.openHospID
			INNER JOIN authority.OpenHosp_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Open Hospitality') AND t.sourceKey IS NOT NULL
			AND t.QueueID = @QueueID

		UNION ALL

		SELECT DISTINCT t.confirmationNumber,'IBE - Brand',aobs.Template,'Open Hospitality','#FFCC00'
		FROM dbo.Transactions t
			INNER JOIN (SELECT BookingSourceID,value AS pegasusID FROM dbo.CRS_BookingSource CROSS APPLY string_split(pegasusID,',')) bs ON bs.BookingSourceID = t.CRS_BookingSourceID
			INNER JOIN pegasus.BookingSource obs ON obs.BookingSourceID = bs.pegasusID
			INNER JOIN authority.Pegasus_BookingSource aobs ON aobs.Bkg_Src_Cd = obs.Bkg_Src_Cd
		WHERE t.DataSourceID IN(SELECT DataSourceID FROM authority.DataSource WHERE SourceName = 'Pegasus') AND t.sourceKey IS NOT NULL
			AND t.QueueID = @QueueID
	END

	EXEC dbo.PH_BookingSource_Populate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Fix_Duplicate_Guest]'
GO


ALTER PROCEDURE [dbo].[Fix_Duplicate_Guest]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT OFF;

	IF OBJECT_ID('tempdb..#DUPS') IS NOT NULL
		DROP TABLE #DUPS;
	
	CREATE TABLE #DUPS
	(
		GoodID int NOT NULL,
		BadID int NOT NULL,

		PRIMARY KEY CLUSTERED(GoodID,BadID)
	)
	
	-- SYNXIS ------------------------------------------------------------------
	;WITH cte_Dups
	AS
	(
		SELECT g.synXisID,g.GuestID,
			ROW_NUMBER() OVER(PARTITION BY g.synXisID ORDER BY g.GuestID) AS rowNum
		FROM dbo.Guest g
			INNER JOIN dbo.Location l ON l.LocationID = g.LocationID
		WHERE g.synxisID IN(SELECT synxisID FROM dbo.Guest GROUP BY synxisID HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d1.GuestID AS GoodID, d2.GuestID AS BadID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.synXisID = d1.synXisID AND d1.GuestID != d2.GuestID
	WHERE d1.rowNum = 1
		AND d2.rowNum > 1
	

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.synXis_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)


	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------

	-- OPEN HOSPITALITY --------------------------------------------------------
	DELETE #DUPS

	;WITH cte_Dups
	AS
	(
		SELECT g.openHospID,g.GuestID,
			ROW_NUMBER() OVER(PARTITION BY g.openHospID ORDER BY g.GuestID) AS rowNum
		FROM dbo.Guest g
			INNER JOIN dbo.Location l ON l.LocationID = g.LocationID
		WHERE g.openHospID IN(SELECT openHospID FROM dbo.Guest GROUP BY openHospID HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d1.GuestID AS GoodID, d2.GuestID AS BadID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.openHospID = d1.openHospID AND d1.GuestID != d2.GuestID
	WHERE d1.rowNum = 1
		AND d2.rowNum > 1
	

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.openHosp_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)


	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------
	
	-- PEGASUS --------------------------------------------------------
	DELETE #DUPS

	;WITH cte_Dups
	AS
	(
		SELECT g.pegasusID,g.GuestID,
			ROW_NUMBER() OVER(PARTITION BY g.pegasusID ORDER BY g.GuestID) AS rowNum
		FROM dbo.Guest g
			INNER JOIN dbo.Location l ON l.LocationID = g.LocationID
		WHERE g.pegasusID IN(SELECT pegasusID FROM dbo.Guest GROUP BY pegasusID HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d1.GuestID AS GoodID, d2.GuestID AS BadID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.pegasusID = d1.pegasusID AND d1.GuestID != d2.GuestID
	WHERE d1.rowNum = 1
		AND d2.rowNum > 1
	

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.pegasus_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)


	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------
	-- hashKey -----------------------------------------------------------------
	DELETE #DUPS

	;WITH cte_Dups
	AS
	(
		SELECT hashKey,GuestID, ROW_NUMBER() OVER(PARTITION BY hashKey ORDER BY GuestID) AS rowNum
		FROM dbo.Guest
		WHERE hashKey IN(SELECT hashKey FROM dbo.Guest GROUP BY hashKey HAVING COUNT(*) > 1)
	)
	INSERT INTO #DUPS(GoodID,BadID)
	SELECT d2.GuestID,d1.GuestID
	FROM cte_Dups d1
		INNER JOIN cte_Dups d2 ON d2.hashKey = d1.hashKey AND d2.rowNum = 2
	WHERE d1.rowNum = 1

	UPDATE t
		SET GuestID = f.GoodID
	FROM dbo.Transactions t
		INNER JOIN #DUPS f ON f.BadID = t.GuestID


	DELETE map.synXis_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)

	DELETE map.openHosp_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)

	DELETE map.pegasus_Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)

	DELETE dbo.Guest
	WHERE GuestID IN(SELECT BadID FROM #DUPS)
	----------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Delete_UnmappedGuests]'
GO


ALTER PROCEDURE [dbo].[Delete_UnmappedGuests]
AS
BEGIN
	IF OBJECT_ID('tempdb..#GUEST_ID') IS NOT NULL
		DROP TABLE #GUEST_ID;
	CREATE TABLE #GUEST_ID(GuestID int NOT NULL, PRIMARY KEY CLUSTERED(GuestID))

	INSERT INTO #GUEST_ID(GuestID)
	SELECT DISTINCT g.GuestID
	FROM dbo.Guest g
		LEFT JOIN dbo.Transactions t ON t.GuestID = g.GuestID
	WHERE t.GuestID IS NULL


	DELETE map.synXis_Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)

	DELETE map.openHosp_Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)

	DELETE map.pegasus_Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)

	DELETE dbo.Guest WHERE GuestID IN(SELECT GuestID FROM #GUEST_ID)
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_CRS_BookingSource]'
GO
ALTER TABLE [map].[pegasus_CRS_BookingSource] ADD CONSTRAINT [FK_map_pegasus_CRS_BookingSource_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[BookingSource] ([BookingSourceID])
GO
ALTER TABLE [map].[pegasus_CRS_BookingSource] ADD CONSTRAINT [FK_map_pegasus_CRS_BookingSource_BookingSourceID] FOREIGN KEY ([BookingSourceID]) REFERENCES [dbo].[CRS_BookingSource] ([BookingSourceID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_Chain]'
GO
ALTER TABLE [map].[pegasus_Chain] ADD CONSTRAINT [FK_map_pegasus_Chain_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[Chain] ([intChainID])
GO
ALTER TABLE [map].[pegasus_Chain] ADD CONSTRAINT [FK_map_pegasus_Chain_intChainID] FOREIGN KEY ([intChainID]) REFERENCES [dbo].[Chain] ([ChainID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_Guest]'
GO
ALTER TABLE [map].[pegasus_Guest] ADD CONSTRAINT [FK_map_pegasus_Guest_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[Guest] ([GuestID])
GO
ALTER TABLE [map].[pegasus_Guest] ADD CONSTRAINT [FK_map_pegasus_Guest_GuestID] FOREIGN KEY ([GuestID]) REFERENCES [dbo].[Guest] ([GuestID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_IATANumber]'
GO
ALTER TABLE [map].[pegasus_IATANumber] ADD CONSTRAINT [FK_map_pegasus_IATANumber_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[IATANumber] ([IATANumberID])
GO
ALTER TABLE [map].[pegasus_IATANumber] ADD CONSTRAINT [FK_map_pegasus_IATANumber_IATANumberID] FOREIGN KEY ([IATANumberID]) REFERENCES [dbo].[IATANumber] ([IATANumberID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_PromoCode]'
GO
ALTER TABLE [map].[pegasus_PromoCode] ADD CONSTRAINT [FK_map_pegasus_PromoCode_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[PromoCode] ([PromoCodeID])
GO
ALTER TABLE [map].[pegasus_PromoCode] ADD CONSTRAINT [FK_map_pegasus_PromoCode_PromoCodeID] FOREIGN KEY ([PromoCodeID]) REFERENCES [dbo].[PromoCode] ([PromoCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_RateCode]'
GO
ALTER TABLE [map].[pegasus_RateCode] ADD CONSTRAINT [FK_map_pegasus_RateCode_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[RateTypeCode] ([RateTypeCodeID])
GO
ALTER TABLE [map].[pegasus_RateCode] ADD CONSTRAINT [FK_map_pegasus_RateCode_RateTypeCodeID] FOREIGN KEY ([RateTypeCodeID]) REFERENCES [dbo].[RateCode] ([RateCodeID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [map].[pegasus_hotel]'
GO
ALTER TABLE [map].[pegasus_hotel] ADD CONSTRAINT [FK_map_pegasus_hotel_pegasusID] FOREIGN KEY ([pegasusID]) REFERENCES [pegasus].[hotel] ([intHotelID])
GO
ALTER TABLE [map].[pegasus_hotel] ADD CONSTRAINT [FK_map_pegasus_hotel_intHotelID] FOREIGN KEY ([intHotelID]) REFERENCES [dbo].[hotel] ([HotelID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
