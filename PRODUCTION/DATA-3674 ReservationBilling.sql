USE ReservationBilling
GO

/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ReservationBilling    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/17/2024 4:19:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [loyalty].[Calculate_BookWithPointFlatRate_CreditMemo]'
GO


ALTER PROCEDURE [loyalty].[Calculate_BookWithPointFlatRate_CreditMemo]
AS
BEGIN
	DECLARE	@startDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --first day of previous month
			,@endDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), -1) --last day of previous month

IF OBJECT_ID('tempdb..#BWPFlatRateCM') IS NOT NULL
    DROP TABLE #BWPFlatRateCM;

	SELECT 
	confirmationNumber
	,arrivalDate
	,status
	,LoyaltyNumberValidated
	,channel
	,mrt.HotelCode
	,mrt.hotelName
	,mrt.rateTypeCode
	,nights
	,loyaltyNumber
	,h.HotelID
	,BW.BookWithPointCreditRulesID
	,BW.FlatFee*mrt.nights AS Voucher_Value
	,BW.FlatFee_Currency AS Voucher_Currency
	,CAST(Reservations.dbo.convertCurrencyToUSD(BW.FlatFee*mrt.nights, BW.FlatFee_Currency, mrt.arrivalDate) AS numeric(8,2)) AS Payable_Value
	,CM.SOPNumber AS CM_SopNumber
	,CM.Voucher_Number AS CM_Voucher_Number
	,CM.Redemption_Date AS CM_Redemption_Date
	,CM.Hotel_Code AS CM_Hotel_Code
	,CM.Membership_Number AS CM_Membership_Number
	,CM.Voucher_Value AS CM_Voucher_Value
	,CM.Voucher_Currency AS CM_Voucher_Currency
	,G.FirstName + ' ' + g.LastName  AS Member_Name
	INTO #BWPFlatRateCM
	FROM Reservations_Reporting.[billing].[MostRecentTransactions] mrt
		INNER JOIN Hotels.dbo.Hotel h 
			ON h.HotelCode = mrt.HotelCode
		LEFT JOIN loyalty.BookWithPointCreditRules BW 
			ON BW.hotelID = h.HotelID
			AND BW.RateTypeCode = mrt.rateTypeCode
			AND mrt.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
		LEFT JOIN loyalty.Credit_Memos CM 
			ON mrt.confirmationNumber = cm.Voucher_Number
		LEFT JOIN loyalty.dbo.LoyaltyNumber ln 
			ON ln.LoyaltyNumberName = mrt.loyaltyNumber
		LEFT JOIN Guests.dbo.Guest g 
			ON g.LoyaltyNumberID = ln.loyaltyNumberID  
	WHERE arrivalDate >= @startDate
		AND arrivalDate <= @enddate
		AND status <> 'Cancelled'


-- 1. If the voucher number does not previously exist in our data, add it
 --INSERT/UPDATE loyalty.Credit_Memos ------------------------------------
	INSERT INTO loyalty.Credit_Memos(Redemption_Date, Hotel_Code, Membership_Number, Voucher_Number,
									Voucher_Value, Voucher_Currency, Payable_Value,Create_Date,SOPNumber,
									Invoice_Date, Currency_Conversion_Date, IsBookWithPoint,rateTypeCode,Credit_Type)
	SELECT	CAST(mrt.arrivalDate AS DATE) AS Redemption_Date, 
			mrt.HotelCode AS Hotel_Code, 
			mrt.loyaltyNumber AS Membership_Number, 
			mrt.confirmationNumber AS Voucher_Number, 
			mrt.Voucher_Value AS Voucher_Value, 
			mrt.Voucher_Currency AS Voucher_Currency, 
			mrt.Payable_Value AS Payable_Value,
			GETDATE() as Create_Date, 
			null as SOPNumber, 
			null as Invoice_Date, 
			CAST(mrt.arrivalDate AS DATE),
			1,
			mrt.rateTypeCode,
			1
	FROM	#BWPFlatRateCM mrt
	WHERE mrt.CM_Voucher_Number IS NULL
		AND mrt.channel <> 'PMS Rez Synch'
		and mrt.LoyaltyNumberValidated = 1 
		AND mrt.BookWithPointCreditRulesID IS NOT NULL

-- 2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
	
	UPDATE CM
		set	CM.Redemption_Date = CAST(mrt.arrivalDate AS DATE), 
			CM.Hotel_Code = mrt.HotelCode , 
			CM.Membership_Number = mrt.loyaltyNumber , 
			CM.Voucher_Value = mrt.Voucher_Value , 
			CM.Voucher_Currency = mrt.Voucher_Currency ,
			CM.Payable_Value = mrt.Payable_Value ,
			CM.Currency_Conversion_Date = CAST(mrt.arrivalDate AS DATE), 
			CM.Create_Date = GetDate(),
			CM.rateTypeCode = mrt.rateTypeCode
		FROM  Loyalty.Credit_Memos CM 
		JOIN #BWPFlatRateCM mrt
				ON cm.Voucher_Number = mrt.confirmationNumber
				AND mrt.CM_SOPNumber IS NULL
				AND mrt.BookWithPointCreditRulesID IS NOT NULL

--Handling Errors
-- 3. If the voucher number DOES exist in our data and DOES have a SOP Number
	--    check all fields of the new record against the existing.
	-- 3b. If the new record has a change in any field FROM the old record
	--    it needs to be reported as an error to the Finance team somehow
-- 4. If the voucher number DOES exist in our data and Does not have [BookWithPointCreditRules] setup.

INSERT INTO Loyalty.[Credit_Memo_Errors]([Redemption_Date],[Hotel_Code],[Hotel_Name],[Membership_Number],
											[Member_Name],[Voucher_Type],[Voucher_Number],[Voucher_Name],
											[Voucher_Value],[Voucher_Currency],[Payable_Value_USD],[filename],
											[importdate],[Error_Message],IsBookWithPoint,[Credit_Type])
SELECT CAST(mrt.arrivalDate AS DATE)
		, mrt.HotelCode
		, mrt.HotelName
		, mrt.loyaltyNumber
		, mrt.Member_Name
		, mrt.rateTypeCode
		, mrt.confirmationNumber
		, mrt.rateTypeCode
		, mrt.Voucher_Value
		, mrt.Voucher_Currency
		, mrt.Payable_Value
		, NULL
		, NULL
		,'Certificate_Number already exists with an SOPNumber, but with different data.' as ErrorMessage
		,1
		,1
	FROM #BWPFlatRateCM mrt
  
	WHERE mrt.CM_SOPNumber is not null
	AND mrt.CM_Voucher_Number IS NOT NULL
	AND mrt.BookWithPointCreditRulesID IS NOT NULL
	AND (
			mrt.CM_Redemption_Date <> mrt.arrivalDate
			OR
			mrt.CM_Hotel_Code <> mrt.hotelcode
			OR
			mrt.CM_Membership_Number <> mrt.loyaltyNumber
			OR
			mrt.CM_Voucher_Value <> mrt.Voucher_Value
			OR
			mrt.CM_Voucher_Currency <> mrt.Voucher_Currency
		)

	UNION ALL

	SELECT CAST(mrt.arrivalDate AS DATE)
		, mrt.HotelCode
		, mrt.HotelName
		, mrt.loyaltyNumber
		, mrt.Member_Name
		, mrt.rateTypeCode
		, mrt.confirmationNumber
		, mrt.rateTypeCode
		, mrt.Voucher_Value
		, mrt.Voucher_Currency
		, mrt.Payable_Value
		, NULL
		, NULL
		, 'Certificate_Number already exists but BookWithPoint CreditRules was not set up.' as ErrorMessage
		, 1
		,1
	FROM #BWPFlatRateCM mrt 
	WHERE mrt.rateTypeCode IN (SELECT DISTINCT [RateTypeCode] FROM [loyalty].[BookWithPointCreditRules])
	AND mrt.BookWithPointCreditRulesID IS NULL

END 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
