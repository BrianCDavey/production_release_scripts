USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 10:14:06 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityStandard]'
GO

ALTER procedure [rpt].[ProfitabilityStandard]

	@billableStart date 
	,@billableEnd date
	,@hotelCodes nvarchar(max) = ''
	,@channels nvarchar(max) = ''
AS

BEGIN
	-- #hotelCodes -----------------------------------------------
	IF OBJECT_ID('tempdb..#hotelCodes') IS NOT NULL
		DROP TABLE #hotelCodes
	CREATE TABLE #hotelCodes(value nvarchar(2048))

	INSERT INTO #hotelCodes(value)
	SELECT value FROM dbo.SplitString(@hotelCodes, ',') 
	--------------------------------------------------------------

	-- #channels -----------------------------------------------
	IF OBJECT_ID('tempdb..#channels') IS NOT NULL
		DROP TABLE #channels
	CREATE TABLE #channels(value nvarchar(2048))

	INSERT INTO #channels(value)
	SELECT value FROM dbo.SplitString(@channels, ',') 
	--------------------------------------------------------------

SELECT bookingStatus
	,[BillableYear]
		,[BillableMonth]
		,[ArrivalYear]
      ,[ArrivalMonth]
	  ,[InvoicedDate]
	  ,[SopNumber]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[hotelCurrency]

      ,SUM([ConfirmedSynxisBookingCount]) AS [ConfirmedSynxisBookingCount]
      ,SUM([ConfirmedRevenueUSD]) AS [ConfirmedRevenueUSD]
      ,SUM([ConfirmedRevenueHotelCurrency]) AS [ConfirmedRevenueHotelCurrency]
      ,SUM([ConfirmedRoomNights]) AS [ConfirmedRoomNights]
	  ,CASE 
		WHEN ISNULL(SUM([ConfirmedRoomNights]),0) = 0 THEN 0
		ELSE SUM([ConfirmedRevenueUSD]) / SUM([ConfirmedRoomNights]) 
		END AS [avgNightlyRateUSD]
	  ,CASE 
		WHEN ISNULL(SUM([ConfirmedSynxisBookingCount]),0) = 0 THEN 0
		ELSE SUM([ConfirmedRoomNights]) / SUM([ConfirmedSynxisBookingCount]) 
		END AS [avgLengthOfStay]
      ,SUM([TotalCost]) AS [TotalCost]
      ,AVG([costperbooking]) as [costperbooking]
      
      ,SUM([BillyBookingCount]) AS [BillyBookingCount]
      ,SUM([PHGSiteBillyBookingCount]) AS [PHGSiteBillyBookingCount]
      ,SUM([HHASiteBillyBookingCount]) AS [HHASiteBillyBookingCount]
      ,SUM([BillyBookingCharges]) AS [BillyBookingCharges]
      ,SUM([PHGSiteBillyBookingCharges]) AS [PHGSiteBillyBookingCharges]
      ,SUM([HHASiteBillyBookingCharges]) AS [HHASiteBillyBookingCharges]

      ,SUM([commissions]) AS [commissions]
      ,SUM([PHGSiteCommissions]) AS [PHGSiteCommissions]
      ,SUM([HHASiteCommissions]) AS [HHASiteCommissions]
      ,SUM([surcharges]) AS [surcharges]
      ,SUM([PHGSiteSurcharges]) AS [PHGSiteSurcharges]
      ,SUM([HHASiteSurcharges]) AS [HHASiteSurcharges]
      ,SUM([BookAndComm]) AS [BookAndComm]
      ,SUM([BookAndCommHotelCurrency]) AS [BookAndCommHotelCurrency]
      ,SUM([PHGSiteBookAndComm]) AS [PHGSiteBookAndComm]
      ,SUM([HHASiteBookAndComm]) AS [HHASiteBookAndComm]

      ,SUM([IPreferBookingCount]) AS [IPreferBookingCount]
      ,SUM([IPreferCharges]) AS [IPreferCharges]
      ,SUM([IPreferCost]) AS [IPreferCost]
      ,SUM([BillyBookingWithZeroBookAndComm]) AS [BillyBookingWithZeroBookAndComm]

  FROM  rpt.[ProfitabilityReport]

  WHERE DATEFROMPARTS(ISNULL(billableYear,arrivalYear), ISNULL(billableMonth,ArrivalMonth), 1) BETWEEN @billableStart AND @billableEnd
	AND (@hotelCodes = '' OR hotelCode IN (SELECT value FROM #hotelCodes))
	AND (@channels = '' OR channel IN (SELECT value FROM #channels))
	AND ((ISNULL(bookAndComm,0) + ISNULL(Surcharges,0) + ISNULL(iprefercharges,0)) <> 0)

  GROUP BY bookingStatus
  ,[BillableYear]
		,[BillableMonth]
		,[ArrivalYear]
      ,[ArrivalMonth]
	  ,[InvoicedDate]
	  ,[SopNumber]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[hotelCurrency]
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
