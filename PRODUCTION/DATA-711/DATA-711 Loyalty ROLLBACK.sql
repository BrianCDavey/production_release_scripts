USE Loyalty
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Loyalty    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Loyalty

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 12/19/2019 2:45:07 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[TransactionDetailedReport]'
GO

ALTER VIEW [dbo].[TransactionDetailedReport]
AS

SELECT 
p.QueueID AS [QueueID]	
, p.[External_SourceKey] AS [Transaction_Id]
, acs.ActivityCauseSystemName AS [Transaction_Source]
, p.[Notes] AS [Remarks]	
, l.loyaltyNumberName AS [iPrefer_Number]
, CASE WHEN g.GuestID IS NULL THEN 'D' ELSE 'E' END AS [Member_Status] 
, t.confirmationNumber AS [Booking_ID]	 
, t.HotelCode AS [Hotel_Code]	
, p.[ActivityCauseDate] AS [Arrival_Date]	
, t.departureDate AS [Departure_Date]	
, pc.PH_Channel AS [Booking_Source]	
, actT.[ActivityTypeName] AS [Campaign] 
, CASE WHEN p.Points > 0 THEN p.Points ELSE 0 END AS [Points_Earned] 
, CASE WHEN p.Points < 0 THEN p.Points * -1 ELSE 0 END AS [Points_Redemeed]
, p.ActivityCauseAmount AS [Reservation_Revenue] 
, p.[ActivityCauseCurrency] AS [Currency_Code] 
, p.[ActivityDate] AS [Transaction_Date] 
, p.CurrencyExchangeDate AS [Reward_Posting_Date]	
, h.MainBrandCode AS [Hotel_Brand] 
, h.HotelName AS [Hotel_Name]	
, [PointLiabilityUSD] AS [Value_of_Redemption_USD]	
, Reservations.[dbo].[convertCurrencyToUSD](ActivityCauseAmount, p.[ActivityCauseCurrency], [ActivityCauseDate]) AS [Amount_Spent_USD]	
, NULL AS IP_TDR_ID  
  FROM [Loyalty].[dbo].[PointActivity] p
  LEFT JOIN loyalty.dbo.ActivityCauseSystem acs ON acs.ActivityCauseSystemID = p.ActivityCauseSystemID
  INNER JOIN loyalty.[dbo].[LoyaltyNumber] l ON p.loyaltyNumberID = l.loyaltyNumberID
  LEFT JOIN loyalty.[dbo].[ActivityType] actT ON actT.ActivityTypeID = p.ActivityTypeID
  LEFT JOIN Reservations.dbo.MostRecentTransactions t ON t.confirmationNumber = p.TransactionNumber
  LEFT JOIN Reservations.dbo.Transactions tr ON tr.TransactionID = t.TransactionID
  LEFT JOIN Reservations.dbo.PH_BookingSource b ON tr.PH_BookingSourceID = b.PH_BookingSourceID
  LEFT JOIN Reservations.dbo.PH_Channel pc ON pc.PH_ChannelID = b.PH_ChannelID
  LEFT JOIN Hotels.dbo.hotelsReporting h ON h.code = t.HotelCode
  LEFT JOIN Guests.dbo.guest g ON g.LoyaltyNumberID = l.LoyaltyNumberID

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [BSI].[Populate_Transactions]'
GO
ALTER   PROCEDURE [BSI].[Populate_Transactions]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH cte_TDR
	AS
	(
		SELECT [Transaction_Id],[Remarks],[Points_Earned],[Points_Redemeed],[Currency_Code],
				[Transaction_Date],[Reward_Posting_Date],[Value_of_Redemption_USD],
				[Transaction_Source],[Campaign],[iPrefer_Number],
				CASE [Member_Status] WHEN 'D' THEN 0 WHEN 'E' THEN 1 WHEN 'C' THEN 2 WHEN 'W' THEN 3 WHEN 'B' THEN 4 END AS MemberStatus,
				[IP_TDR_ID]
		FROM Superset.[BSI].[TransactionDetailedReport]
	)
	MERGE INTO BSI.Transactions AS tgt
	USING
	(
		SELECT tdr.[Transaction_Id],tdr.[Remarks],tdr.[Points_Earned],tdr.[Points_Redemeed],tdr.[Currency_Code],
				tdr.[Transaction_Date],tdr.[Reward_Posting_Date],tdr.[Value_of_Redemption_USD],[iPrefer_Number],
				ts.TransactionSourceID,ln.LoyaltyNumberID,tdr.MemberStatus,cam.CampaignID,tdr.[IP_TDR_ID]
		FROM cte_TDR tdr
			INNER JOIN BSI.LoyaltyNumber ln ON ln.LoyaltyNumberName = tdr.[iPrefer_Number]
			LEFT JOIN BSI.TransactionSource ts ON ts.TransactionSourceName = tdr.[Transaction_Source]
			LEFT JOIN BSI.Campaign cam ON cam.CampaignName = tdr.[Campaign]
	) AS src ON src.[IP_TDR_ID] = tgt.[IP_TDR_ID]
	WHEN MATCHED THEN
		UPDATE
			SET	TDR_Transaction_ID = src.[Transaction_Id],
				[Remarks] = src.[Remarks],
				[Points_Earned] = src.[Points_Earned],
				[Points_Redemeed] = src.[Points_Redemeed],
				[Currency_Code] = src.[Currency_Code],
				[Transaction_Date] = src.[Transaction_Date],
				[Reward_Posting_Date] = src.[Reward_Posting_Date],
				[Value_of_Redemption_USD] = src.[Value_of_Redemption_USD],
				TransactionSourceID = src.TransactionSourceID,
				LoyaltyNumberID = src.LoyaltyNumberID,
				MemberStatus = src.MemberStatus,
				CampaignID = src.CampaignID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([IP_TDR_ID],TDR_Transaction_ID,[Remarks],[Points_Earned],[Points_Redemeed],[Currency_Code],[Transaction_Date],
				[Reward_Posting_Date],[Value_of_Redemption_USD],TransactionSourceID,LoyaltyNumberID,MemberStatus,CampaignID)
		VALUES([IP_TDR_ID],[Transaction_Id],[Remarks],[Points_Earned],[Points_Redemeed],[Currency_Code],[Transaction_Date],
				[Reward_Posting_Date],[Value_of_Redemption_USD],TransactionSourceID,LoyaltyNumberID,MemberStatus,CampaignID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Epsilon_Populate_PointActivity]'
GO

















-- =============================================
-- Author:		Ti Yao
-- Create date: 2019-09-13
-- Description:	This procedure is a part of Epsilon import process loading data from ETL db to Loyalty DB
-- Prototype: EXEC [dbo].[Epsilon_Populate_PointActivity] 5435
-- History: 2019-09-13 Ti Yao Initial Creation
-- =============================================
ALTER PROCEDURE [dbo].[Epsilon_Populate_PointActivity]
	@QueueID int = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ds int
	SELECT @ds = DataSourceID FROM dbo.DataSource WHERE SourceName = 'epsilon'

	MERGE INTO dbo.PointActivity AS tgt
	USING
	(
		SELECT DISTINCT  
		systemPointActivityID AS [External_SourceKey]
		, [PointActivityID] AS [Internal_SourceKey]
		, CAST(REPLACE(LEFT(NULLIF([ActivityDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityDate],''),2)   AS datetime ) AS [ActivityDate]
		, ac.[ActivityTypeID]
		, pt.[PointTypeID]
		, acs.[ActivityCauseSystemID]
		, [TransactionNumber]
		, uu.UserUpdatedID AS [ActivityCauseUserID]
		, CAST(NULLIF([ActivityCauseAmount],'') AS decimal(20,5)) AS [ActivityCauseAmount]
		, [ActivityCauseCurrency]
		, CAST(REPLACE(LEFT(NULLIF([ActivityCauseDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([ActivityCauseDate],''),2)   AS datetime ) AS [ActivityCauseDate]
		, CAST(REPLACE(LEFT(NULLIF([CurrencyExchangeDate],''),18),'.',':') + ' ' + RIGHT(NULLIF([CurrencyExchangeDate],''),2)   AS datetime ) AS [CurrencyExchangeDate]
		, CAST(NULLIF([CurrencyExchangeRate],'') AS decimal(10,9) ) AS [CurrencyExchangeRate]
		, CAST(NULLIF([Points],'') AS decimal(15,5) ) AS [Points]
		, CAST(NULLIF([PointLiabilityUSD],'') AS decimal(10,2) ) AS [PointLiabilityUSD]
		, [Notes]
		, ln.[LoyaltyNumberID]
		, @ds AS [DataSourceID]
		,  [QueueID]
		, ActivityCauseHotelID
		FROM epsilon.PointActivity pa
		INNER JOIN dbo.LoyaltyNumber ln ON ln.Epsilon_ID = pa.LoyaltyNumberID
		LEFT JOIN dbo.ActivityType ac ON pa.ActivityTypeID  = ac.Epsilon_ID
		LEFT JOIN dbo.PointType pt ON pt.Epsilon_ID  = pa.PointTypeID
		LEFT JOIN dbo.ActivityCauseSystem acs ON acs.Epsilon_ID  = pa.ActivityCauseSystemID
		LEFT JOIN dbo.UserUpdated uu ON uu.Epsilon_ID = pa.ActivityCauseUserID
		--WHERE pa.QueueID = @QueueID
	) AS src ON 
	ISNULL(tgt.[External_SourceKey],'') = ISNULL(src.[External_SourceKey],'')
	AND tgt.Internal_SourceKey = src.Internal_SourceKey
	AND ISNULL(tgt.[ActivityDate],'1900-01-01')  = ISNULL(src.[ActivityDate],'1900-01-01')
	AND ISNULL(tgt.[ActivityTypeID],'') = ISNULL(src.[ActivityTypeID],'')
	AND ISNULL(tgt.[PointTypeID],'') = ISNULL(src.[PointTypeID],'')
	AND ISNULL(tgt.[ActivityCauseSystemID],'') = ISNULL(src.[ActivityCauseSystemID],'')
	AND ISNULL(tgt.[TransactionNumber],'') = ISNULL(src.[TransactionNumber],'')
	AND ISNULL(tgt.[ActivityCauseUserID],'') = ISNULL(src.[ActivityCauseUserID],'')
	AND ISNULL(tgt.[ActivityCauseAmount],0) = ISNULL(src.[ActivityCauseAmount],0)
	AND ISNULL(tgt.[ActivityCauseCurrency],'') = ISNULL(src.[ActivityCauseCurrency],'')
	AND ISNULL(tgt.[ActivityCauseDate],'1900-01-01') = ISNULL(src.[ActivityCauseDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeDate],'1900-01-01') = ISNULL(src.[CurrencyExchangeDate],'1900-01-01')
	AND ISNULL(tgt.[CurrencyExchangeRate],0) = ISNULL(src.[CurrencyExchangeRate],0)
	AND ISNULL(tgt.[Points],0) = ISNULL(src.[Points],0)
	AND ISNULL(tgt.[PointLiabilityUSD],0) = ISNULL(src.[PointLiabilityUSD],0)
	AND ISNULL(tgt.[Notes],'') = ISNULL(src.[Notes],'')
	AND ISNULL(tgt.[LoyaltyNumberID],'') = ISNULL(src.[LoyaltyNumberID],'')
	AND ISNULL(tgt.[DataSourceID],'') = ISNULL(src.[DataSourceID],'')
	AND ISNULL(tgt.ActivityCauseHotelID,'') = ISNULL(src.ActivityCauseHotelID,'')
	WHEN NOT MATCHED BY TARGET THEN
		INSERT( [ActivityDate], [ActivityTypeID], [PointTypeID], [ActivityCauseSystemID], [TransactionNumber], [ActivityCauseUserID], [ActivityCauseAmount], [ActivityCauseCurrency], [ActivityCauseDate], [CurrencyExchangeDate], [CurrencyExchangeRate], [Points], [PointLiabilityUSD], [Notes], [LoyaltyNumberID], [DataSourceID], [Internal_SourceKey],  [QueueID],[External_SourceKey]	,ActivityCauseHotelID)
		VALUES( src.[ActivityDate], src.[ActivityTypeID], src.[PointTypeID], src.[ActivityCauseSystemID], src.[TransactionNumber], src.[ActivityCauseUserID], src.[ActivityCauseAmount], src.[ActivityCauseCurrency], src.[ActivityCauseDate], src.[CurrencyExchangeDate], src.[CurrencyExchangeRate], src.[Points], src.[PointLiabilityUSD], src.[Notes], src.[LoyaltyNumberID], src.[DataSourceID], src.[Internal_SourceKey],  src.[QueueID],src.[External_SourceKey],src.ActivityCauseHotelID)
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
