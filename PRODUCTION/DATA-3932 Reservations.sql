/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Reservations    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/1/2024 2:20:56 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [rpt].[AARPItemCode]'
GO



-- =============================================
-- Author:		Benjamin Truong
-- Create date: 2024-09-25
-- Description:	Proc for AARPItemCode
-- =============================================
CREATE PROCEDURE [rpt].[AARPItemCode]
	 @startDate date
	,@endDate date
AS
BEGIN
	SET NOCOUNT ON;

	SELECT mrt.confirmationNumber
		,SUM(c.chargeValueInHotelCurrency) AS chargeValueInHotelCurrency
		,SUM(F3M.dbo.convertCurrency(c.chargeValueInHotelCurrency, c.hotelCurrencyCode,'USD',c.invoiceDate)) AS chargeValueInUSD
		,SUM(F3M.dbo.convertCurrency(c.roomRevenueInHotelCurrency, c.hotelCurrencyCode,'USD',c.invoiceDate)) as roomRevenueUSD
		,mrt.hotelCode
		,mrt.HotelName
		,mrt.confirmationDate
		,mrt.arrivalDate
		,mrt.departureDate
		,mrt.IATANumber
		,mrt.rateTypeCode 
	FROM ReservationBilling.dbo.charges c
		LEFT JOIN Superset.dbo.mostrecenttransactions mrt ON c.confirmationNumber = mrt.confirmationNumber
	WHERE c.itemCode = 'AARP'
		AND c.billableDate BETWEEN @startDate and @endDate
	GROUP BY mrt.confirmationNumber,c.roomRevenueInUSD,mrt.hotelCode,mrt.HotelName, mrt.confirmationDate
		, mrt.arrivalDate, mrt.departureDate, mrt.IATANumber, mrt.rateTypeCode
	ORDER BY mrt.IATANumber desc, mrt.rateTypeCode
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
