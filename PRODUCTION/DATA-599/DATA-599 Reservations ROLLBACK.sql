USE Reservations
GO

/*
Run this script on:

        chi-lt-00032377.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/8/2019 3:32:51 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityExtended]'
GO


ALTER procedure [rpt].[ProfitabilityExtended]
	@billableStart date
	,@billableEnd date
	,@hotelCodes nvarchar(max) = ''
	,@channels nvarchar(max) = ''
	,@rateCodes nvarchar(max) = ''
	,@iatas nvarchar(max) = ''
	,@iataGroups nvarchar(max) = ''

AS

BEGIN

	-- #iataGroups -----------------------------------------------
	IF OBJECT_ID('tempdb..#iataGroups') IS NOT NULL
		DROP TABLE #iataGroups
	CREATE TABLE #iataGroups(value nvarchar(2048))

	INSERT INTO #iataGroups(value)
	SELECT value FROM dbo.SplitString(@iataGroups, ',') 
	--------------------------------------------------------------

	-- #hotelCodes -----------------------------------------------
	IF OBJECT_ID('tempdb..#hotelCodes') IS NOT NULL
		DROP TABLE #hotelCodes
	CREATE TABLE #hotelCodes(value nvarchar(2048))

	INSERT INTO #hotelCodes(value)
	SELECT value FROM dbo.SplitString(@hotelCodes, ',') 
	--------------------------------------------------------------

	-- #channels -----------------------------------------------
	IF OBJECT_ID('tempdb..#channels') IS NOT NULL
		DROP TABLE #channels
	CREATE TABLE #channels(value nvarchar(2048))

	INSERT INTO #channels(value)
	SELECT value FROM dbo.SplitString(@channels, ',') 
	--------------------------------------------------------------

	-- #rateCodes -----------------------------------------------
	IF OBJECT_ID('tempdb..#rateCodes') IS NOT NULL
		DROP TABLE #rateCodes
	CREATE TABLE #rateCodes(value nvarchar(2048))

	INSERT INTO #rateCodes(value)
	SELECT value FROM dbo.SplitString(@rateCodes, ',') 
	--------------------------------------------------------------

	-- #iatas -----------------------------------------------
	IF OBJECT_ID('tempdb..#iatas') IS NOT NULL
		DROP TABLE #iatas
	CREATE TABLE #iatas(value nvarchar(2048))

	INSERT INTO #iatas(value)
	SELECT value FROM dbo.SplitString(@iatas, ',') 
	--------------------------------------------------------------

;WITH iataGroupMatches AS
	(
		--get all distinct profitability record ids where one of the iata groups is present
		SELECT DISTINCT pr.id
		FROM rpt.[ProfitabilityReport] pr
			INNER JOIN #iataGroups i
				ON pr.IATAGroups LIKE '%' + i.value + '%'
		WHERE DATEFROMPARTS(ISNULL(billableYear,arrivalYear), ISNULL(billableMonth,ArrivalMonth), 1) BETWEEN @billableStart AND @billableEnd
			AND (@hotelCodes = '' OR hotelCode IN (SELECT value FROM #hotelCodes))
			AND (@channels = '' OR channel IN (SELECT value FROM #channels))
			AND (@rateCodes = '' OR rateTypeCode IN (SELECT value FROM #rateCodes))
			AND (@iatas = '' OR IATANumber IN (SELECT value FROM #iatas))
	)
SELECT bookingStatus
	,[ConfirmationYear]
      ,[ConfirmationMonth]
	  ,[BillableYear]
		,[BillableMonth]
		,[ArrivalYear]
      ,[ArrivalMonth]
	  ,[InvoicedDate]
	  ,[SopNumber]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[IataGam]
      ,[IATAGroups]
      ,[IATANumber]
      ,[travelAgencyName]
      ,[travelAgencyCity]
      ,[travelAgencyState]
      ,[travelAgencyCountry]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[ratename]
      ,[rateCategory_PHG]
	  ,[hotelCurrency]

      ,SUM([ConfirmedSynxisBookingCount]) AS [ConfirmedSynxisBookingCount]
      ,SUM([ConfirmedRevenueUSD]) AS [ConfirmedRevenueUSD]
      ,SUM([ConfirmedRevenueHotelCurrency]) AS [ConfirmedRevenueHotelCurrency]
      ,SUM([ConfirmedRoomNights]) AS [ConfirmedRoomNights]
	  ,CASE 
		WHEN ISNULL(SUM([ConfirmedRoomNights]),0) = 0 THEN 0
		ELSE SUM([ConfirmedRevenueUSD]) / SUM([ConfirmedRoomNights]) 
		END AS [avgNightlyRateUSD]
	  ,CASE 
		WHEN ISNULL(SUM([ConfirmedSynxisBookingCount]),0) = 0 THEN 0
		ELSE SUM([ConfirmedRoomNights]) / SUM([ConfirmedSynxisBookingCount]) 
		END AS [avgLengthOfStay]
      ,SUM([TotalCost]) AS [TotalCost]
      ,CASE 
		WHEN ISNULL(SUM(ConfirmedSynxisBookingCount),0) = 0 THEN 0
		ELSE SUM(TotalCost) / SUM(ConfirmedSynxisBookingCount) 
		END AS [costperbooking]      
      
      ,SUM([BillyBookingCount]) AS [BillyBookingCount]
      ,SUM([PHGSiteBillyBookingCount]) AS [PHGSiteBillyBookingCount]
      ,SUM([HHASiteBillyBookingCount]) AS [HHASiteBillyBookingCount]
      ,SUM([BillyBookingCharges]) AS [BillyBookingCharges]
      ,SUM([PHGSiteBillyBookingCharges]) AS [PHGSiteBillyBookingCharges]
      ,SUM([HHASiteBillyBookingCharges]) AS [HHASiteBillyBookingCharges]

      ,SUM([commissions]) AS [commissions]
      ,SUM([PHGSiteCommissions]) AS [PHGSiteCommissions]
      ,SUM([HHASiteCommissions]) AS [HHASiteCommissions]
      ,SUM([surcharges]) AS [surcharges]
      ,SUM([PHGSiteSurcharges]) AS [PHGSiteSurcharges]
      ,SUM([HHASiteSurcharges]) AS [HHASiteSurcharges]
      ,SUM([BookAndComm]) AS [BookAndComm]
      ,SUM([BookAndCommHotelCurrency]) AS [BookAndCommHotelCurrency]
      ,SUM([PHGSiteBookAndComm]) AS [PHGSiteBookAndComm]
      ,SUM([HHASiteBookAndComm]) AS [HHASiteBookAndComm]

      ,SUM([IPreferBookingCount]) AS [IPreferBookingCount]
      ,SUM([IPreferCharges]) AS [IPreferCharges]
      ,SUM([IPreferCost]) AS [IPreferCost]
      ,SUM([BillyBookingWithZeroBookAndComm]) AS [BillyBookingWithZeroBookAndComm]

  FROM rpt.[ProfitabilityReport] pr
  LEFT OUTER JOIN iataGroupMatches igm
	ON pr.id = igm.id

  WHERE DATEFROMPARTS(ISNULL(billableYear,arrivalYear), ISNULL(billableMonth,ArrivalMonth), 1) BETWEEN @billableStart AND @billableEnd
	AND (@hotelCodes = '' OR hotelCode IN (SELECT value FROM #hotelCodes))
	AND (@channels = '' OR channel IN (SELECT value FROM #channels))
	AND (@rateCodes = '' OR rateTypeCode IN (SELECT value FROM #rateCodes))
	AND (@iatas = '' OR IATANumber IN (SELECT value FROM #iatas))
	AND (@iataGroups = '' OR igm.id IS NOT NULL)
	AND ((ISNULL(bookAndComm,0) + ISNULL(Surcharges,0) + ISNULL(iprefercharges,0)) <> 0)



  GROUP BY  bookingStatus
	,[ConfirmationYear]
      ,[ConfirmationMonth]
	  ,[BillableYear]
		,[BillableMonth]
		,[ArrivalYear]
      ,[ArrivalMonth]
	  ,[InvoicedDate]
	  ,[SopNumber]
	  ,[crsHotelID]
      ,[hotelStatus]
	  ,[hotelCode]
      ,[hotelName]
      ,[hotelRooms]
      ,[primaryBrand]
      ,[CRMClassification]
      ,[gpSalesTerritory]
      ,[AMD]
      ,[RD]
      ,[RAM]
      ,[AccountManager]
      ,[geographicRegion]
      ,[country]
      ,[state]
      ,[city]
      ,[billingDescription]
      ,[channel]
      ,[secondarySource]
      ,[subSource]      
      ,[CROcode]
	  ,[reportingChannel]
	  ,[reportingSecondarySource]
	  ,[reportingSubSource]
	  ,[itemCodeFamily]
	  ,[surchargeItemCode]
	  ,[IataGam]
      ,[IATAGroups]
      ,[IATANumber]
      ,[travelAgencyName]
      ,[travelAgencyCity]
      ,[travelAgencyState]
      ,[travelAgencyCountry]
      ,[rateCategoryCode]
      ,[rateTypeCode]
      ,[ratename]
      ,[rateCategory_PHG]
	  ,[hotelCurrency]

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[ProfitabilityReport_FINAL]'
GO


ALTER PROCEDURE [rpt].[ProfitabilityReport_FINAL]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @SynXis_DS int
	SELECT @SynXis_DS = DataSourceID FROM authority.DataSource WHERE SourceName = 'SynXis'

	--build IATA table
	-----------------------------------------------------------------------
		DROP TABLE IF EXISTS #iataCRM

		CREATE TABLE #iataCRM
		(
			iataNumber nvarchar(20) NOT NULL,
			iatamanager nvarchar(200) NOT NULL,

			PRIMARY KEY CLUSTERED(iataNumber,iatamanager)
		)

		INSERT INTO #iataCRM(iataNumber,iatamanager)
		SELECT accountnumber as iataNumber,MIN(phg_iataglobalaccountmanageridName) iatamanager
		FROM LocalCRM.dbo.account
		WHERE phg_iataglobalaccountmanageridName IS NOT NULL
			AND accountnumber IS NOT NULL
		GROUP BY accountnumber
	----------------------------------------------------------------------

	TRUNCATE TABLE rpt.[ProfitabilityReport]

	INSERT INTO [rpt].[ProfitabilityReport]
           ([crsHotelID]
           ,[hotelCode]
           ,[hotelName]
           ,[primaryBrand]
           ,[gpSalesTerritory]
           ,[AMD]
           ,[RD]
           ,[RAM]
           ,[AccountManager]
           ,[geographicRegion]
           ,[country]
           ,[city]
           ,[state]
           ,[hotelRooms]
           ,[hotelStatus]
           ,[CRMClassification]
           ,[hotelcurrency]
           ,[billingDescription]
           ,[channel]
           ,[secondarySource]
           ,[subSource]
           ,[CROcode]
           ,[reportingChannel]
           ,[reportingSecondarySource]
           ,[reportingSubSource]
           ,[itemCodeFamily]
           ,[surchargeItemCode]
           ,[IataGam]
           ,[IATAGroups]
           ,[IATANumber]
           ,[travelAgencyName]
           ,[travelAgencyCity]
           ,[travelAgencyState]
           ,[travelAgencyCountry]
           ,[rateCategoryCode]
           ,[rateTypeCode]
           ,[ratename]
           ,[rateCategory_PHG]
           ,[ArrivalYear]
           ,[ArrivalMonth]
           ,[ConfirmationYear]
           ,[ConfirmationMonth]
           ,[CancellationYear]
           ,[CancellationMonth]
           ,[BillableYear]
           ,[BillableMonth]
           ,[InvoicedYear]
           ,[InvoicedMonth]
           ,[InvoicedDate]
           ,[SopNumber]
           ,[BookingStatus]
           ,[ConfirmedSynxisBookingCount]
           ,[ConfirmedRevenueUSD]
           ,[ConfirmedRoomNights]
           ,[TotalCost]
           ,[avgNightlyRateUSD]
           ,[avgLengthOfStay]
           ,[BillyBookingCount]
           ,[PHGSiteBillyBookingCount]
           ,[HHASiteBillyBookingCount]
           ,[BillyBookingCharges]
           ,[PHGSiteBillyBookingCharges]
           ,[HHASiteBillyBookingCharges]
           ,[commissions]
           ,[PHGSiteCommissions]
           ,[HHASiteCommissions]
           ,[surcharges]
           ,[PHGSiteSurcharges]
           ,[HHASiteSurcharges]
           ,[BookAndComm]
           ,[PHGSiteBookAndComm]
           ,[HHASiteBookAndComm]
           ,[costperbooking]
           ,[IPreferBookingCount]
           ,[IPreferCharges]
           ,[IPreferCost]
           ,[BillyBookingWithZeroBookAndComm]
           ,[ConfirmedRevenueHotelCurrency]
           ,[BillyBookingChargesHotelCurrency]
           ,[PHGSiteBillyBookingChargesHotelCurrency]
           ,[HHASiteBillyBookingChargesHotelCurrency]
           ,[CommissionsHotelCurrency]
           ,[PHGSiteCommissionsHotelCurrency]
           ,[HHASiteCommissionsHotelCurrency]
           ,[SurchargesHotelCurrency]
           ,[PHGSiteSurchargesHotelCurrency]
           ,[HHASiteSurchargesHotelCurrency]
           ,[BookAndCommHotelCurrency]
           ,[PHGSiteBookAndCommHotelCurrency]
           ,[HHASiteBookAndCommHotelCurrency]
           ,[IPreferChargesHotelCurrency])
	SELECT
		CASE 
			WHEN t.DataSourceID != @SynXis_DS THEN ISNULL(h.synxisID,h.OpenHospID)
			WHEN t.DataSourceID = @SynXis_DS THEN h.SynXisID 
			ELSE h.OpenHospID 
		END as [crsHotelID]
		, f.[hotelCode]
		, h.[hotelName]
		, hr.MainBrandCode AS [primaryBrand]
		, hr.PHG_FinanceRegion as [gpSalesTerritory]
		, hr.PHG_AreaManagerIdName as [AMD]
		, hr.PHG_RegionalManagerIdName as [RD]
		, hr.PHG_RevenueAccountManagerIdName as [RAM]
		, hr.PHG_AccountManagerName as [AccountManager]
		, hr.GeographicRegionName as [geographicRegion]
		, hr.ShortName as [country]
		, hr.PhysicalCity as [city]
		, hr.State as [state]
		, hr.TotalRooms as [hotelRooms]
		, hr.StatusCodeName as [hotelStatus]
		, ha.accountclassificationcodename as [CRMClassification]
		, hr.currencycode as [hotelcurrency]
		, sbd.billingDescription as [billingDescription]
		, ISNULL(cc.channel,'charge for non-reservation') as [channel]
		, c2s.secondarySource as [secondarySource]
		, css.subSource as [subSource]
		, cro.CRO_Code as [CROcode]
		, pc.PH_Channel as reportChannel
		, p2s.PH_SecondaryChannel as reportSecondaryChannel
		, pss.PH_SubChannel as reportSubChannel
		, f.[itemCodeFamily]
		, f.[surchargeItemCode]
		, taa.iataManager as [IataGam]
		, Core.dbo.GetIATAGroups(iata.IATANumber,ts.confirmationDate) as [IATAGroups]
		, iata.IATANumber as [IATANumber]
		, ta.Name as [travelAgencyName]
		, taLcy.City_Text as [travelAgencyCity]
		, taLs.State_Text as [travelAgencyState]
		, taLc.Country_Text as [travelAgencyCountry]
		, rcat.rateCategoryCode as [rateCategoryCode]
		, rc.RateCode as [rateTypeCode]
		, rc.RateName as [ratename]
		,	CASE 
				WHEN LEFT(rc.RateCode,3) = 'NEG' OR LEFT(rcat.rateCategoryCode,3) = 'NEG' THEN 'NEG' 
				WHEN LEFT(rc.RateCode,3) = 'CON' OR LEFT(rcat.rateCategoryCode,3) = 'CON' THEN 'CON' 
				WHEN LEFT(rc.RateCode,3) = 'MKT' OR LEFT(rcat.rateCategoryCode,3) = 'MKT' THEN 'MKT' 
				WHEN LEFT(rc.RateCode,3) = 'PRO' OR LEFT(rcat.rateCategoryCode,3) = 'PRO' THEN 'PRO'
				WHEN LEFT(rc.RateCode,3) = 'PKG' OR LEFT(rcat.rateCategoryCode,3) = 'PKG' THEN 'PKG'  
				WHEN LEFT(rc.RateCode,3) = 'GOV' OR LEFT(rcat.rateCategoryCode,3) = 'GOV' THEN 'GOV' 
				WHEN LEFT(rc.RateCode,3) = 'BAR' OR LEFT(rcat.rateCategoryCode,3) = 'BAR' THEN 'BAR' 
				WHEN LEFT(rc.RateCode,3) = 'DIS' OR LEFT(rcat.rateCategoryCode,3) = 'DIS' THEN 'DIS' 
				ELSE LEFT(rcat.rateCategoryCode,3)
			END as [rateCategory_PHG]
		, YEAR(td.arrivalDate) as [ArrivalYear]
		, MONTH(td.arrivalDate) as [ArrivalMonth]
		, YEAR(ts.confirmationDate) as [ConfirmationYear]
		, MONTH(ts.confirmationDate) as [ConfirmationMonth]
		, YEAR(ts.cancellationDate) as CancellationYear
		, MONTH(ts.cancellationDate) as CancellationMonth
		, f.[BillableYear]
		, f.[BillableMonth]
		, f.[InvoiceYear]
		, f.[InvoiceMonth]
		, f.[InvoiceDate]
		, f.[SopNumber]
		, ts.status as bookingStatus

		, COUNT(DISTINCT f.ConfirmedSynxisConfirmationNumber) AS ConfirmedSynxisBookingCount
		, SUM(td.reservationRevenue * ISNULL(xeArri.toUSD,xeConf.toUSD)) as [ConfirmedRevenueUSD]
		, SUM(td.rooms * td.nights) as [ConfirmedRoomNights]
		,SUM
        (
			CASE pc.PH_Channel
                WHEN  'OTA Bundle' THEN 0
            ELSE
                CASE sbd.billingDescription WHEN '' THEN pegsBookingCost.cost ELSE synxisBookingCost.cost END
                +
                CASE cc.channel WHEN 'GDS' THEN gdsBookingCost.cost ELSE 0 END
            END
        ) as [TotalCost]	
		, CASE WHEN ISNULL(SUM(td.rooms * td.nights),0) = 0 THEN 0 ELSE SUM(td.reservationRevenue * ISNULL(xeArri.toUSD,xeConf.toUSD)) / SUM(td.rooms * td.nights) END as [avgNightlyRateUSD]
		, CASE WHEN ISNULL(COUNT(DISTINCT f.ConfirmedSynxisConfirmationNumber),0) = 0 THEN 0 ELSE SUM(td.nights) / COUNT(DISTINCT f.ConfirmedSynxisConfirmationNumber) END as [avgLengthOfStay]
		, COUNT(DISTINCT (CASE WHEN f.BookAndCommUSD > 0 THEN f.BillyConfirmationNumber END)) AS BillyBookingCount
		, COUNT(DISTINCT f.PHGSiteBillyConfirmationNumber) AS PHGSiteBillyBookingCount
		, COUNT(DISTINCT f.HHASiteBillyConfirmationNumber) AS HHASiteBillyBookingCount
		, SUM(f.BillyBookingChargesUSD) AS BillyBookingCharges
		, SUM(f.PHGSiteBillyBookingChargesUSD) AS PHGSiteBillyBookingCharges
		, SUM(f.HHASiteBillyBookingChargesUSD) AS HHASiteBillyBookingCharges
		, SUM(f.commissionsUSD) AS commissions
		, SUM(f.PHGSiteCommissionsUSD) AS PHGSiteCommissions
		, SUM(f.HHASiteCommissionsUSD) AS HHASiteCommissions
		, SUM(f.surchargesUSD) AS surcharges
		, SUM(f.PHGSiteSurchargesUSD) AS PHGSiteSurcharges
		, SUM(f.HHASiteSurchargesUSD) AS HHASiteSurcharges
		, SUM(f.BookAndCommUSD) BookAndComm
		, SUM(f.PHGSiteBookAndCommUSD) AS PHGSiteBookAndComm
		, SUM(f.HHASiteBookAndCommUSD) AS HHASiteBookAndComm
		,SUM
        (
			CASE pc.PH_Channel
                WHEN  'OTA Bundle' THEN 0
            ELSE
                CASE sbd.billingDescription WHEN '' THEN pegsBookingCost.cost ELSE synxisBookingCost.cost END
                +
                CASE cc.channel WHEN 'GDS' THEN gdsBookingCost.cost ELSE 0 END
            END
        ) / COUNT(DISTINCT f.ConfirmationNumber)  as costperbooking
		, COUNT(DISTINCT f.IPreferConfirmationNumber) AS IPreferBookingCount
		, SUM(f.IPreferChargesUSD) AS IPreferCharges
		, SUM(f.IPreferCost ) AS IPreferCost
		, COUNT(DISTINCT f.BillyWithZeroBookAndCommConfirmationNumber) AS BillyBookingWithZeroBookAndComm

		, SUM((td.reservationRevenue  * ISNULL(xeArri.toUSD,xeConf.toUSD)) / (ISNULL(xeArriGP.fromUSD,xeConfGP.fromUSD))) as [ConfirmedRevenueHotelCurrency]
		, SUM(f.BillyBookingChargesHotelCurrency) AS BillyBookingChargesHotelCurrency
		, SUM(f.PHGSiteBillyBookingChargesHotelCurrency) AS PHGSiteBillyBookingChargesHotelCurrency
		, SUM(f.HHASiteBillyBookingChargesHotelCurrency) AS HHASiteBillyBookingChargesHotelCurrency
		, SUM(f.commissionsHotelCurrency) AS CommissionsHotelCurrency
		, SUM(f.PHGSiteCommissionsHotelCurrency) AS PHGSiteCommissionsHotelCurrency
		, SUM(f.HHASiteCommissionsHotelCurrency) AS HHASiteCommissionsHotelCurrency
		, SUM(f.surchargesHotelCurrency) AS SurchargesHotelCurrency
		, SUM(f.PHGSiteSurchargesHotelCurrency) AS PHGSiteSurchargesHotelCurrency
		, SUM(f.HHASiteSurchargesHotelCurrency) AS HHASiteSurchargesHotelCurrency
		, SUM(f.BookAndCommHotelCurrency) AS BookAndCommHotelCurrency
		, SUM(f.PHGSiteBookAndCommHotelCurrency) AS PHGSiteBookAndCommHotelCurrency
		, SUM(f.HHASiteBookAndCommHotelCurrency) AS HHASiteBookAndCommHotelCurrency
		, SUM(f.IPreferChargesHotelCurrency) AS IPreferChargesHotelCurrency 
	FROM Prof_Rpt.FinalResults as f
	LEFT JOIN Reservations.dbo.Transactions t ON f.confirmationNumber = t.confirmationNumber
	LEFT JOIN Reservations.dbo.TransactionDetail td ON t.transactionDetailID = td.transactionDetailID
	LEFT JOIN Reservations.dbo.TransactionStatus ts ON t.TransactionStatusID = ts.TransactionStatusID
	LEFT JOIN Hotels.dbo.Hotel h ON f.hotelCode = h.HotelCode
	LEFT JOIN Hotels.dbo.hotelsReporting hr ON h.HotelCode = hr.code
	LEFT JOIN LocalCRM.dbo.Account ha ON hr.crmGUID = ha.accountID
	LEFT JOIN IC.dbo.RM00101 as gpCust ON h.HotelCode = gpCust.CUSTNMBR
	LEFT JOIN CurrencyRates.dbo.dailyRates xeConfGP ON ts.confirmationDate = xeConfGP.rateDate AND LTRIM(RTRIM(gpCust.CURNCYID)) = xeConfGP.code
	LEFT JOIN CurrencyRates.dbo.dailyRates xeArriGP ON td.arrivalDate = xeArriGP.rateDate AND LTRIM(RTRIM(gpCust.CURNCYID)) = xeArriGP.code
	LEFT JOIN Reservations.synxis.Transactions st ON t.sourceKey = st.TransactionID AND t.DataSourceID = @SynXis_DS
	LEFT JOIN Reservations.synxis.BillingDescription sbd ON st.BillingDescriptionID = sbd.BillingDescriptionID
	LEFT JOIN Reservations.dbo.CRS_BookingSource crsBS ON t.CRS_BookingSourceID = crsBS.BookingSourceID
	LEFT JOIN Reservations.dbo.CRS_Channel cc ON crsBS.ChannelID = cc.ChannelID
	LEFT JOIN Reservations.dbo.CRS_SecondarySource c2s ON crsBS.SecondarySourceID = c2s.SecondarySourceID
	LEFT JOIN Reservations.dbo.CRS_SubSource css ON crsBS.SubSourceID = css.SubSourceID
	LEFT JOIN Reservations.dbo.PH_BookingSource phBS ON t.PH_BookingSourceID = phBS.PH_BookingSourceID
	LEFT JOIN Reservations.dbo.PH_Channel pc ON phBS.PH_ChannelID = pc.PH_ChannelID
	LEFT JOIN Reservations.dbo.PH_SecondaryChannel p2s ON phBS.PH_SecondaryChannelID = p2s.PH_SecondaryChannelID
	LEFT JOIN Reservations.dbo.PH_SubChannel pss ON phBS.PH_SubChannelID = pss.PH_SubChannelID
	LEFT JOIN Reservations.dbo.CROCode ccro ON crsBS.CROCodeID = ccro.CROCodeID
	LEFT JOIN Reservations.authority.CRO_Code cro ON ccro.auth_CRO_CodeID = cro.CRO_CodeID
	LEFT JOIN Reservations.dbo.IATANumber iata ON t.IATANumberID = iata.IATANumberID
	LEFT JOIN Reservations.dbo.TravelAgent ta ON t.TravelAgentID = ta.TravelAgentID
	LEFT JOIN Reservations.dbo.Location taL ON ta.LocationID = taL.LocationID
	LEFT JOIN Reservations.dbo.City taLcy ON taL.CityID = taLcy.CityID
	LEFT JOIN Reservations.dbo.State taLs ON taL.StateID = taLs.StateID
	LEFT JOIN Reservations.dbo.Country taLc ON taL.CountryID = taLc.CountryID
	LEFT JOIN #iataCrm taa ON iata.IATANumber = taa.iataNumber
	LEFT JOIN Reservations.dbo.RateCode rc ON t.RateCodeID = rc.RateCodeID
	LEFT JOIN Reservations.dbo.RateCategory rcat ON t.RateCategoryID = rcat.RateCategoryID
	LEFT JOIN CurrencyRates.dbo.dailyRates xeConf ON ts.confirmationDate = xeConf.rateDate AND td.currency = xeConf.code
	LEFT JOIN CurrencyRates.dbo.dailyRates xeArri ON td.arrivalDate = xeArri.rateDate AND td.currency = xeArri.code

	LEFT OUTER JOIN Superset.dbo.gdsBookingCost ON c2s.secondarySource = gdsBookingCost.gdsType 
												AND td.arrivalDate BETWEEN gdsBookingCost.startDate
												AND ISNULL(gdsBookingCost.endDate,'12/31/2999')
	LEFT OUTER JOIN Superset.dbo.synxisBookingCost ON sbd.billingDescription = synxisBookingCost.billingDescription 
												AND td.arrivalDate BETWEEN synxisBookingCost.startDate
												AND ISNULL(synxisBookingCost.endDate,'12/31/2999')
	LEFT OUTER JOIN Superset.dbo.pegsBookingCost ON cc.channel = pegsBookingCost.channel 
												AND td.arrivalDate BETWEEN pegsBookingCost.startDate
												AND ISNULL(pegsBookingCost.endDate,'12/31/2999')

	GROUP BY 		
		CASE 
			WHEN t.DataSourceID != @SynXis_DS THEN ISNULL(h.synxisID,h.OpenHospID)
			WHEN t.DataSourceID = @SynXis_DS THEN h.SynXisID 
			ELSE h.OpenHospID 
		END
		, f.[hotelCode]
		, h.[hotelName]
		, hr.MainBrandCode
		, hr.PHG_FinanceRegion
		, hr.PHG_AreaManagerIdName
		, hr.PHG_RegionalManagerIdName
		, hr.PHG_RevenueAccountManagerIdName
		, hr.PHG_AccountManagerName
		, hr.GeographicRegionName
		, hr.ShortName
		, hr.PhysicalCity
		, hr.State
		, hr.TotalRooms
		, hr.StatusCodeName
		, ha.accountclassificationcodename
		, hr.currencycode
		, sbd.billingDescription
		, ISNULL(cc.channel,'charge for non-reservation')
		, c2s.secondarySource
		, css.subSource
		, cro.CRO_Code
		, pc.PH_Channel
		, p2s.PH_SecondaryChannel
		, pss.PH_SubChannel
		, f.[itemCodeFamily]
		, f.[surchargeItemCode]
		, taa.iataManager
		, Core.dbo.GetIATAGroups(iata.IATANumber,ts.confirmationDate)
		, iata.IATANumber
		, ta.Name
		, taLcy.City_Text
		, taLs.State_Text
		, taLc.Country_Text
		, rcat.rateCategoryCode
		, rc.RateCode
		, rc.RateName
		, CASE 
				WHEN LEFT(rc.RateCode,3) = 'NEG' OR LEFT(rcat.rateCategoryCode,3) = 'NEG' THEN 'NEG' 
				WHEN LEFT(rc.RateCode,3) = 'CON' OR LEFT(rcat.rateCategoryCode,3) = 'CON' THEN 'CON' 
				WHEN LEFT(rc.RateCode,3) = 'MKT' OR LEFT(rcat.rateCategoryCode,3) = 'MKT' THEN 'MKT' 
				WHEN LEFT(rc.RateCode,3) = 'PRO' OR LEFT(rcat.rateCategoryCode,3) = 'PRO' THEN 'PRO'
				WHEN LEFT(rc.RateCode,3) = 'PKG' OR LEFT(rcat.rateCategoryCode,3) = 'PKG' THEN 'PKG'  
				WHEN LEFT(rc.RateCode,3) = 'GOV' OR LEFT(rcat.rateCategoryCode,3) = 'GOV' THEN 'GOV' 
				WHEN LEFT(rc.RateCode,3) = 'BAR' OR LEFT(rcat.rateCategoryCode,3) = 'BAR' THEN 'BAR' 
				WHEN LEFT(rc.RateCode,3) = 'DIS' OR LEFT(rcat.rateCategoryCode,3) = 'DIS' THEN 'DIS' 
				ELSE LEFT(rcat.rateCategoryCode,3)
		END 
		, YEAR(td.arrivalDate)
		, MONTH(td.arrivalDate)
		, YEAR(ts.confirmationDate)
		, MONTH(ts.confirmationDate)
		, YEAR(ts.cancellationDate)
		, MONTH(ts.cancellationDate)
		, f.[BillableYear]
		, f.[BillableMonth]
		, f.[InvoiceYear]
		, f.[InvoiceMonth]
		, f.[InvoiceDate]
		, f.[SopNumber]
		, ts.status
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
