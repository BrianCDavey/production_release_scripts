/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.F3M    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.F3M

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/1/2024 4:48:59 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[sopDocsForUpdate]'
GO



ALTER VIEW [dbo].[sopDocsForUpdate]
AS
	select distinct
		LTRIM(RTRIM(rt.[Document Number])) as sopNumber
		,LTRIM(RTRIM(rt.[Customer Number])) as hotelCode
		,LTRIM(RTRIM(COALESCE(sopHeader.docid, rt.[Document Type]))) as docType
	,CASE
		WHEN [Document Number] = [Document Description]
			THEN CASE 
					WHEN LEN([Customer PO Number]) = 0
						THEN LTRIM(RTRIM([Document Type]))
					ELSE LTRIM(RTRIM([Customer PO Number]))
				END
		ELSE LTRIM(RTRIM([Document Type])) + ' ' + LTRIM(RTRIM([Document Description]))
	END	AS [docDescription]		
		,custMaster.CURNCYID as currencyCode
		,rt.[Document Date] as docDate
		,CASE WHEN rt.[Due Date] IS NULL OR rt.[Due Date] <= '1900-01-10'
			THEN rt.[Document Date]
			ELSE rt.[Due Date]
			END as dueDate
		,CASE WHEN rt.[Document Number] LIKE 'WCREDIT%'
		THEN dbo.convertCurrency(COALESCE(rt.[Originating Write Off Amount],rt.[Write Off Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		ELSE dbo.convertCurrency(COALESCE(rt.[Originating Original Trx Amount],rt.[Original Trx Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		END as totalAmount		
		,CASE WHEN  rt.[Document Number] LIKE 'WCREDIT%'
		THEN dbo.convertCurrency(COALESCE(COALESCE(rt.[Originating Write Off Amount],rt.[Write Off Amount]) - COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],0), 0),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE()))
		ELSE dbo.convertCurrency(COALESCE(COALESCE(rt.[Originating Original Trx Amount],rt.[Original Trx Amount]) - COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],0), 0),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		END as amountPaid
		,
		CASE WHEN  rt.[Document Number] LIKE 'WCREDIT%'
		THEN dbo.convertCurrency(COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],rt.[Originating Write Off Amount],rt.[Write Off Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		ELSE dbo.convertCurrency(COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],rt.[Originating Original Trx Amount],rt.[Original Trx Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		END as balanceDue	

	from IC.dbo.ReceivablesTransactions rt
	LEFT OUTER JOIN IC.dbo.SOP30200 as sopHeader
		ON rt.[Document Number] = sopHeader.sopnumbe	
	INNER JOIN [dbo].financialDocuments fd
		on rt.[Document Number] = fd.sopNumber
	LEFT OUTER JOIN IC.dbo.RM00101 custMaster
		ON rt.[Customer Number]= custMaster.CUSTNMBR
	LEFT OUTER JOIN [dbo].HideDocuments hd
		ON rt.[Document Number] = hd.sopNumber
	WHERE 
	COALESCE(fd.balanceDue,99999999999999.99999) <> dbo.convertCurrency(COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],rt.[Originating Original Trx Amount],rt.[Original Trx Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
	AND (sopHeader.docID IS NULL OR sopHeader.docid NOT IN ('DNSINV','DNSCM')) 
	AND hd.sopNumber IS NULL --don't bring in docs we are intentionally hiding

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[sopDocsForInsert]'
GO


ALTER VIEW [dbo].[sopDocsForInsert]
AS
select distinct
		LTRIM(RTRIM(rt.[Document Number])) as sopNumber
		,LTRIM(RTRIM(rt.[Customer Number])) as hotelCode
		,LTRIM(RTRIM(rt.[Batch Number])) as batchName
		,LTRIM(RTRIM(COALESCE(sopHeader.docid, rt.[Document Type]))) as docType
	,CASE
		WHEN rt.[Document Number] = rt.[Document Description]
			THEN CASE 
					WHEN LEN([Customer PO Number]) > 0
						THEN LTRIM(RTRIM([Customer PO Number]))
				END
		ELSE LTRIM(RTRIM([Document Type])) + ' ' + LTRIM(RTRIM([Document Description]))
	END	AS [docDescription]
		,custMaster.CURNCYID as currencyCode
		,rt.[Document Date] as docDate
		,CASE WHEN rt.[Due Date] IS NULL OR rt.[Due Date] <= '1900-01-10'
			THEN rt.[Document Date]
			ELSE rt.[Due Date]
			END as dueDate
		,CASE WHEN rt.[Document Number] LIKE 'WCREDIT%'
		THEN dbo.convertCurrency(COALESCE(rt.[Originating Write Off Amount],rt.[Write Off Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		ELSE dbo.convertCurrency(COALESCE(rt.[Originating Original Trx Amount],rt.[Original Trx Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		END as totalAmount		
		,CASE WHEN  rt.[Document Number] LIKE 'WCREDIT%'
		THEN dbo.convertCurrency(COALESCE(COALESCE(rt.[Originating Write Off Amount],rt.[Write Off Amount]) - COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],0), 0),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE()))
		ELSE dbo.convertCurrency(COALESCE(COALESCE(rt.[Originating Original Trx Amount],rt.[Original Trx Amount]) - COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],0), 0),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		END as amountPaid
		,
		CASE WHEN  rt.[Document Number] LIKE 'WCREDIT%'
		THEN dbo.convertCurrency(COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],rt.[Originating Write Off Amount],rt.[Write Off Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		ELSE dbo.convertCurrency(COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],rt.[Originating Original Trx Amount],rt.[Original Trx Amount]),rt.[Currency ID],custMaster.CURNCYID,DATEADD(DAY,-1,GETDATE())) 
		END as balanceDue				

	from IC.dbo.ReceivablesTransactions as rt
	LEFT OUTER JOIN IC.dbo.SOP30200 as sopHeader
		ON rt.[Document Number] = sopHeader.sopnumbe
	LEFT OUTER JOIN IC.dbo.RM00101 custMaster
		ON rt.[Customer Number] = custMaster.CUSTNMBR
	LEFT OUTER JOIN [dbo].financialDocuments fd
		on rt.[Document Number] = fd.sopNumber
	LEFT OUTER JOIN [dbo].HideDocuments hd
		ON rt.[Document Number] = hd.sopNumber
	WHERE 
	(rt.[Document Date] >= DATEADD(YEAR,-1,GETDATE())
	OR COALESCE(rt.[Originating Current Trx Amount],rt.[Current Trx Amount],rt.[Originating Original Trx Amount],rt.[Original Trx Amount]) <> 0)
	AND fd.sopNumber IS NULL --don't bring in docs that are already there
	AND (sopHeader.docID IS NULL OR sopHeader.docid NOT IN ('DNSINV','DNSCM'))
	AND hd.sopNumber IS NULL --don't bring in docs we are intentionally hiding
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LoadFinancialData]'
GO





-- =============================================
-- Author:		Kris Scott
-- Create date: 01/06/2016
-- Description:	Gets financial data from GP and loads it into the financial tables in F3M
-- =============================================
ALTER PROCEDURE [dbo].[LoadFinancialData] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--insert all posted sop docs into the Preferred Net metadata table
	INSERT INTO [dbo].financialDocuments ([sopNumber]
      ,[hotelCode]
      ,[batchName]
      ,[docType]
      ,[docDescription]
      ,[currencyCode]
      ,[totalAmount]
      ,[docDate]
      ,[dueDate]
      ,[amountPaid]
      ,[balanceDue])
	SELECT [sopNumber]
      ,[hotelCode]
      ,[batchName]      
      ,[docType]
      ,[docDescription]
      ,[currencyCode]
      ,[totalAmount]
      ,[docDate]
      ,[dueDate]
      ,[amountPaid]
      ,[balanceDue]
	FROM [dbo].[sopDocsForInsert];
	
	--update all posted sop docs with the most accurate payment info
	UPDATE fd
	SET fd.[totalAmount] = sopUpdate.totalAmount
      ,fd.[amountPaid] = sopUpdate.amountPaid
      ,fd.[balanceDue] = sopUpdate.balanceDue
	FROM [dbo].financialDocuments fd
	INNER JOIN [dbo].[sopDocsForUpdate] sopUpdate
		ON fd.sopNumber = sopUpdate.sopNumber;

	--delete any sop docs that we now want to hide
	DELETE fd
	FROM [dbo].financialDocuments fd
	INNER JOIN [dbo].HideDocuments hd
		ON fd.sopNumber = hd.sopNumber;

	--DELETE all existing financial accounts data
	TRUNCATE TABLE [dbo].[FinancialAccounts];
		
	--insert new accounts data
	INSERT INTO [dbo].[FinancialAccounts] ([hotelCode]
	  ,[hotelName]
	  ,[custClass]
      ,[currencyCode]
      ,[totalDue]
      ,[currentBalance]
      ,[pastDue]
      ,[pastDueDays]
      ,[serviceLevel]
	  ,[onPortal])
     SELECT gp.[hotelCode]
	  ,gp.[hotelName]
	  ,gp.[customerClass]
      ,gp.[currencyCode]
      ,gp.[totalDue]
      ,gp.[currentBalance]
      ,gp.[pastDue]
      ,gp.[pastDueDays]
      ,gp.[serviceLevel]
	  ,0
     FROM dbo.[FinancialAccountsSource] gp
     LEFT OUTER JOIN [dbo].[FinancialAccounts] fa
		on gp.hotelCode = fa.hotelCode
		and gp.currencyCode = fa.currencyCode
	WHERE fa.hotelCode IS NULL;
  
	--mark hotels on portal that are supposed to be on the portal
	UPDATE fa
	SET onPortal = 1
	FROM [dbo].[FinancialAccounts] fa
	INNER JOIN Core.dbo.hotelsReporting
		ON fa.hotelCode = hotelsReporting.code
	INNER JOIN LocalCRM.dbo.Account
		ON hotelsReporting.crmGuid = Account.accountID
		AND	Account.statecode = 0
		AND	(
			Account.PHG_MemberPortalImportOverride = 0 --0 means Yes in CRM
			OR (
				Account.statuscode IN (100000001, 100000002) --Member or Renewal
				AND
				hotelsReporting.mainBrandCode IN ('LEG','LVX','LIF','CON','PSR','PRR')
				)
			);
	
	--Make credits negative
	UPDATE dbo.FinancialDocuments
	SET   [totalAmount] = totalAmount * -1
      ,[balanceDue] = balanceDue * -1
    WHERE (docType IN ('CM','CMIA','CMIG','CMIP','CMIR','Payments') OR sopNumber LIKE 'WCREDIT%')
    AND totalAmount > 0

	--Make amount paid negative
	UPDATE dbo.FinancialDocuments
	SET   [amountPaid] = amountPaid * -1
    WHERE (docType NOT IN ('CM','CMIA','CMIG','CMIP','CMIR','Payments') AND sopNumber NOT LIKE 'WCREDIT%')
    AND amountPaid > 0	
	
END




GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
