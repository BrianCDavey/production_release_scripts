/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ETL    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 9/24/2024 5:39:35 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[ETL_CurrencyRateCheck]'
GO

CREATE PROCEDURE [dbo].[ETL_CurrencyRateCheck]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;WITH _cteRates
	AS
	(
		SELECT code,MAX(RateDate) as LastRateDate
		FROM [PTG-SQL-PUBLIC.CRESTWOODCLOUD.COM].CurrencyRates.dbo.dailyRates
		GROUP BY code
	),
	NecessaryRates
	AS
	(
		SELECT DISTINCT CURNCYID
		FROM IC.dbo.RM00101
		WHERE CURNCYID <> ''
	)
	SELECT r.CURNCYID AS Currency,
		c.LastRateDate,
		CASE
			WHEN c.LastRateDate < CONVERT(VARCHAR(10), getdate(), 23) THEN 'Rates did not load today'
			ELSE  'Rates loaded'
		END AS Message
	FROM NecessaryRates r
		LEFT JOIN _cteRates c ON r.CURNCYID = c.code
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
