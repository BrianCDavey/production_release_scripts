/*
Run this script on:

        CHI-SQ-DP-01\CHISQZ01.CmsImport    -  This database will be modified

to synchronize it with:

        CHISQZ01.CmsImport

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.1.7.14336 from Red Gate Software Ltd at 4/6/2020 2:15:18 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[ConsortiaAccounts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ConsortiaAccounts] ALTER COLUMN [consortiaAccountIataGlobalAccountManager] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[ConsortiaAccounts] ALTER COLUMN [accountIndustry] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_ConsortiaSales]'
GO

ALTER PROCEDURE [dbo].[mpid_ConsortiaSales]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.ConsortiaAccounts;

		DECLARE @ConsortiaData TABLE(TableName nvarchar(6),
										nodeName nvarchar(100),
										crmAccountName nvarchar(100),
										consortiaAccountCrmGUID uniqueidentifier,
										crmAccountStreetAddressLine1 nvarchar(100),
										crmAccountStreetAddressLine2 nvarchar(100),
										crmAccountCity nvarchar(100),
										crmAccountState nvarchar(100),
										crmAccountCountry nvarchar(100),
										crmAccountPostalCode nvarchar(100),
										crmAccountPhoneNumber nvarchar(100),
										crmAccountEmailAddress nvarchar(100),
										crmAccountWebSite nvarchar(100),
										crmAccountManagedAccount bit,
										consortiaAccountIataGlobalAccountManager int,
										accountIndustry int,
										isDisabled bit);

		INSERT INTO @ConsortiaData(TableName,nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
									crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
									consortiaAccountIataGlobalAccountManager,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,LTRIM(RTRIM(account.Name)) AS nodeName,LTRIM(RTRIM(account.Name)) AS crmAccountName,UPPER(account.AccountID) AS consortiaAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,ISNULL(Address1_City, '') AS crmAccountCity,
					ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,
					ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,ISNULL(WebSiteURL, '') AS crmAccountWebSite,
					CASE account.phg_consortiasalesstatus WHEN 100000002 THEN 1 ELSE 0 END AS crmAccountManagedAccount,
					umbracoIataGlobalAccountManager.nodeId AS consortiaAccountIataGlobalAccountManager,umbracoIndustry.nodeId AS accountIndustry,0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.phg_consortiasalesstatus,ISNULL(phg_showinmemberportalconsortia, 0) AS PHG_ShowinMemberPortalConsortia,
								phg_iataglobalaccountmanagerid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_consortiasalesstatus IN (100000000,100000002)
							AND Account.phg_consortiasales = 1
							AND Account.phg_showinmemberportalconsortia = 1
					  ) account
					LEFT JOIN MemberPortal.dbo.cmsContentXml as umbracoIataGlobalAccountManager ON CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																										AND CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_iataglobalaccountmanagerid
					LEFT JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																						AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
					UNION
				SELECT 'TARGET' AS TableName,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/consortiaAccountCrmGuid)[1]','uniqueidentifier') AS consortiaAccountCrmGuid,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
					ISNULL(CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/crmAccountManagedAccount)[1]','BIT') AS crmAccountManagedAccount,
					caigam.id AS consortiaAccountIataGlobalAccountManager,
					cai.id AS accountIndustry,
					CAST(cmsContentXml.xml as xml).value('(/ConsortiaAccount/disabled)[1]','bit') AS isDisabled
				FROM (
						SELECT xml, CAST(cx.xml as xml).value('(/ConsortiaAccount/consortiaAccountIataGlobalAccountManager)[1]','nvarchar(100)') accountManager, CAST(cx.xml as xml).value('(/ConsortiaAccount/crmAccountIndustry)[1]','nvarchar(100)') accountIndustry
						FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
						WHERE ct.alias = 'ConsortiaAccount'
					) cmsContentXml
					LEFT JOIN MemberPortal.dbo.umbracoNode caigam ON caigam.uniqueID = CAST(SUBSTRING(accountManager, 16, 8) + '-' + SUBSTRING(accountManager, 24, 4) + '-' + SUBSTRING(accountManager, 28, 4) + '-' + SUBSTRING(accountManager, 32, 4) + '-' + SUBSTRING(accountManager, 36, 12) AS uniqueidentifier)
					LEFT JOIN MemberPortal.dbo.umbracoNode cai ON cai.uniqueID = CAST(SUBSTRING(accountIndustry, 16, 8) + '-' + SUBSTRING(accountIndustry, 24, 4) + '-' + SUBSTRING(accountIndustry, 28, 4) + '-' + SUBSTRING(accountIndustry, 32, 4) + '-' + SUBSTRING(accountIndustry, 36, 12) AS uniqueidentifier)
			  ) unioned
		GROUP BY nodeName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,consortiaAccountIataGlobalAccountManager,
				accountIndustry,isDisabled
		HAVING COUNT(*) = 1;

		--SELECT * FROM @ConsortiaData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.ConsortiaAccounts(Name,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
											crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
											consortiaAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,0,accountIndustry
		FROM @ConsortiaData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.ConsortiaAccounts(Name,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
											crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
											consortiaAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,consortiaAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				consortiaAccountIataGlobalAccountManager,1,accountIndustry
		FROM @ConsortiaData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @ConsortiaData S WHERE S.TableName = 'SOURCE' AND T.consortiaAccountCrmGUID = S.consortiaAccountCrmGUID );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[CorporateAccounts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateAccounts] ALTER COLUMN [corporateAccountGlobalTeamLead] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateAccounts] ALTER COLUMN [corporateAccountRegionalTeamLeadAmericas] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateAccounts] ALTER COLUMN [corporateAccountRegionalTeamLeadASPAC] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[CorporateAccounts] ALTER COLUMN [corporateAccountRegionalTeamLeadEMEA] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_CorporateAccounts]'
GO

ALTER PROCEDURE [dbo].[mpid_CorporateAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.CorporateAccounts;

		DECLARE @CorporateAccountData TABLE(TableName nvarchar(6),
											nodeName nvarchar(100),
											crmAccountName nvarchar(100),
											corporateAccountCrmGUID uniqueidentifier,
											crmAccountStreetAddressLine1 nvarchar(100),
											crmAccountStreetAddressLine2 nvarchar(100),
											crmAccountCity nvarchar(100),
											crmAccountState nvarchar(100),
											crmAccountCountry nvarchar(100),
											crmAccountPostalCode nvarchar(100),
											crmAccountPhoneNumber nvarchar(100),
											crmAccountEmailAddress nvarchar(100),
											crmAccountWebSite nvarchar(100),
											crmAccountSalesStatus nvarchar(25),
											corporateAccountGlobalTeamLead int,
											corporateAccountRegionalTeamLeadAmericas int,
											corporateAccountRegionalTeamLeadASPAC int,
											corporateAccountRegionalTeamLeadEMEA int,
											accountIndustry int,
											isDisabled bit);

		INSERT INTO @CorporateAccountData(TableName,nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,
											crmAccountCity,crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
											crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,
											corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,
				crmAccountCity,crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,
				corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,account.Name AS nodeName,account.Name AS crmAccountName,UPPER(account.AccountID) AS corporateAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,
					ISNULL(Address1_City, '') AS crmAccountCity,ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,
					ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,
					ISNULL(WebSiteURL, '') AS crmAccountWebSite,ISNULL(COALESCE(phg_corporatesalesstatusname, 'UNKNOWN'), '') AS crmAccountSalesStatus,
					umbracoLeadGlobal.nodeId AS corporateAccountGlobalTeamLead,umbracoLeadAmericas.nodeId AS corporateAccountRegionalTeamLeadAmericas,
					umbracoLeadASPAC.nodeId AS corporateAccountRegionalTeamLeadASPAC,umbracoLeadEMEA.nodeId AS corporateAccountRegionalTeamLeadEMEA,
					umbracoIndustry.nodeId AS accountIndustry,0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.PHG_CorporateSalesStatus,account.PHG_CorporateSalesStatusName,phg_corporateglobalaccountmanagerid,
								phg_corporatermamericasid,phg_corporatermaspacid,phg_corporatermemeaid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND account.phg_corporateaccount = 1
							AND account.PHG_ShowinMemberPortalCorporateSales = 1
					  ) account
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadGlobal ON CAST(umbracoLeadGlobal.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																							AND CAST(umbracoLeadGlobal.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporateglobalaccountmanagerid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadAmericas ON CAST(umbracoLeadAmericas.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																							AND CAST(umbracoLeadAmericas.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporatermamericasid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadASPAC ON CAST(umbracoLeadASPAC.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145 
																							AND CAST(umbracoLeadASPAC.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporatermaspacid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoLeadEMEA ON CAST(umbracoLeadEMEA.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																							AND CAST(umbracoLeadEMEA.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_corporatermemeaid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																							AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
						UNION

				 SELECT 'TARGET' AS TableName,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/corporateAccountCrmGuid)[1]','uniqueidentifier') AS corporateAccountCrmGuid,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/corporateAccount/crmAccountSalesStatus)[1]','nvarchar(MAX)'), '') AS crmAccountSalesStatus,
						cagtl.id AS corporateAccountGlobalTeamLead,
						cartla.id AS corporateAccountRegionalTeamLeadAmericas,
						cartlp.id AS corporateAccountRegionalTeamLeadASPAC,
						cartle.id AS corporateAccountRegionalTeamLeadEMEA,
						ai.id AS accountIndustry,
						CAST(cmsContentXml.xml as xml).value('(/corporateAccount/disabled)[1]','bit') AS isDisabled
				 FROM (
						SELECT xml,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountGlobalTeamLead)[1]','nvarchar(100)') globalTeamLead,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountRegionalTeamLeadAmericas)[1]','nvarchar(100)') regionalTeamLeadAmericas,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountRegionalTeamLeadASPAC)[1]','nvarchar(100)') regionalTeamLeadASPAC,
							CAST(cx.xml as xml).value('(/corporateAccount/corporateAccountRegionalTeamLeadEMEA)[1]','nvarchar(100)') regionalTeamLeadEMEA,
							CAST(cx.xml as xml).value('(/corporateAccount/crmAccountIndustry)[1]','nvarchar(100)') accountIndustry
						FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
						WHERE ct.alias = 'corporateAccount'
					) cmsContentXml
					LEFT JOIN MemberPortal.dbo.umbracoNode cagtl ON cagtl.uniqueID = CAST(SUBSTRING(globalTeamLead, 16, 8) + '-' + SUBSTRING(globalTeamLead, 24, 4) + '-' + SUBSTRING(globalTeamLead, 28, 4) + '-' + SUBSTRING(globalTeamLead, 32, 4) + '-' + SUBSTRING(globalTeamLead, 36, 12) AS uniqueidentifier)
					LEFT JOIN MemberPortal.dbo.umbracoNode cartla ON cartla.uniqueID = CAST(SUBSTRING(regionalTeamLeadAmericas, 16, 8) + '-' + SUBSTRING(regionalTeamLeadAmericas, 24, 4) + '-' + SUBSTRING(regionalTeamLeadAmericas, 28, 4) + '-' + SUBSTRING(regionalTeamLeadAmericas, 32, 4) + '-' + SUBSTRING(regionalTeamLeadAmericas, 36, 12) AS uniqueidentifier)
					LEFT JOIN MemberPortal.dbo.umbracoNode cartlp ON cartlp.uniqueID = CAST(SUBSTRING(regionalTeamLeadASPAC, 16, 8) + '-' + SUBSTRING(regionalTeamLeadASPAC, 24, 4) + '-' + SUBSTRING(regionalTeamLeadASPAC, 28, 4) + '-' + SUBSTRING(regionalTeamLeadASPAC, 32, 4) + '-' + SUBSTRING(regionalTeamLeadASPAC, 36, 12) AS uniqueidentifier)
					LEFT JOIN MemberPortal.dbo.umbracoNode cartle ON cartle.uniqueID = CAST(SUBSTRING(regionalTeamLeadEMEA, 16, 8) + '-' + SUBSTRING(regionalTeamLeadEMEA, 24, 4) + '-' + SUBSTRING(regionalTeamLeadEMEA, 28, 4) + '-' + SUBSTRING(regionalTeamLeadEMEA, 32, 4) + '-' + SUBSTRING(regionalTeamLeadEMEA, 36, 12) AS uniqueidentifier)
					LEFT JOIN MemberPortal.dbo.umbracoNode ai ON ai.uniqueID = CAST(SUBSTRING(accountIndustry, 16, 8) + '-' + SUBSTRING(accountIndustry, 24, 4) + '-' + SUBSTRING(accountIndustry, 28, 4) + '-' + SUBSTRING(accountIndustry, 32, 4) + '-' + SUBSTRING(accountIndustry, 36, 12) AS uniqueidentifier)
				) unioned
		GROUP BY nodeName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountSalesStatus,corporateAccountGlobalTeamLead,
				corporateAccountRegionalTeamLeadAmericas,corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @CorporateAccountData ORDER BY nodeName,TableName


		INSERT INTO CmsImport.dbo.CorporateAccounts(Name,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
													crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
													crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
													corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
				corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,0,accountIndustry
		FROM @CorporateAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.CorporateAccounts(Name,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
													crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
													crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
													corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,corporateAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountSalesStatus,corporateAccountGlobalTeamLead,corporateAccountRegionalTeamLeadAmericas,
				corporateAccountRegionalTeamLeadASPAC,corporateAccountRegionalTeamLeadEMEA,1,accountIndustry
		FROM @CorporateAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @CorporateAccountData S WHERE S.TableName = 'SOURCE' AND T.corporateAccountCrmGUID = S.corporateAccountCrmGUID );
	END
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[GroupAccounts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[GroupAccounts] ALTER COLUMN [groupAccountSalesDirector] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[GroupAccounts] ALTER COLUMN [accountIndustry] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_GroupAccounts]'
GO

ALTER PROCEDURE [dbo].[mpid_GroupAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;


	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN

		TRUNCATE TABLE CmsImport.dbo.GroupAccounts;

		DECLARE @GroupAccountData TABLE(TableName nvarchar(6),
										nodeName nvarchar(100),
										crmAccountName nvarchar(100),
										groupAccountCrmGUID uniqueidentifier,
										crmAccountStreetAddressLine1 nvarchar(100),
										crmAccountStreetAddressLine2 nvarchar(100),
										crmAccountCity nvarchar(100),
										crmAccountState nvarchar(100),
										crmAccountCountry nvarchar(100),
										crmAccountPostalCode nvarchar(100),
										crmAccountPhoneNumber nvarchar(100),
										crmAccountEmailAddress nvarchar(100),
										crmAccountWebSite nvarchar(100),
										crmAccountManagedAccount bit,
										groupAccountSalesDirector int,
										accountIndustry int,
										isDisabled bit);

		INSERT INTO @GroupAccountData(TableName,nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
										crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
										crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,account.Name AS nodeName,account.Name AS crmAccountName,UPPER(account.AccountID) AS groupAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,
					ISNULL(Address1_City, '') AS crmAccountCity,ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,
					ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,
					ISNULL(WebSiteURL, '') AS crmAccountWebSite,CASE account.phg_groupsalesstatus WHEN 100000002 THEN 'True' ELSE 'False' END AS crmAccountManagedAccount,
					umbracoGroupSales.nodeId AS groupAccountSalesDirector,umbracoIndustry.nodeId AS accountIndustry,0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,
							Telephone1,EMailAddress1,WebSiteURL,account.phg_groupsalesstatus,phg_groupsalesdirectorid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_groupsalesstatus IN (100000001,100000002)
							AND account.phg_groupaccount = 1
							AND Account.phg_showinmemberportalgroupsales = 1
					  ) account
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoGroupSales ON CAST(umbracoGroupSales.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																						AND CAST(umbracoGroupSales.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_groupsalesdirectorid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																						AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
					UNION

					SELECT 'TARGET' AS TableName,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/groupAccountCrmGuid)[1]','uniqueidentifier') AS groupAccountCrmGUID,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
							ISNULL(CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountManagedAccount)[1]','BIT') AS crmAccountManagedAccount,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/groupAccountSalesDirector)[1]','nvarchar(MAX)') AS groupAccountSalesDirector,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/crmAccountIndustry)[1]','nvarchar(MAX)') AS accountIndustry,
							CAST(cmsContentXml.xml as xml).value('(/GroupAccount/disabled)[1]','bit') AS isDisabled
					FROM MemberPortal.dbo.cmsContentXml
					WHERE CAST(cmsContentXml.xml as xml).value('(/GroupAccount/@nodeType)[1]','nvarchar(MAX)') = 17836
			 ) unioned
		GROUP BY nodeName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,groupAccountSalesDirector,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @GroupAccountData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.GroupAccounts(Name,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
										crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
										groupAccountSalesDirector,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				groupAccountSalesDirector,0,accountIndustry
		FROM @GroupAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.GroupAccounts(Name,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
										crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
										groupAccountSalesDirector,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,groupAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,
				crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,
				groupAccountSalesDirector,1,accountIndustry
		FROM @GroupAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @GroupAccountData S WHERE S.TableName = 'SOURCE' AND T.groupAccountCrmGUID = S.groupAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[LeisureAccounts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LeisureAccounts] ALTER COLUMN [leisureAccountIataGlobalAccountManager] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[LeisureAccounts] ALTER COLUMN [accountIndustry] [int] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mpid_LeisureAccounts]'
GO

ALTER PROCEDURE [dbo].[mpid_LeisureAccounts]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM LocalCRM.dbo.Account)
	BEGIN
		TRUNCATE TABLE CmsImport.dbo.LeisureAccounts;

		DECLARE @LeisureAccountData TABLE(TableName nvarchar(6),
											nodeName nvarchar(100),
											crmAccountName nvarchar(100),
											leisureAccountCrmGUID uniqueidentifier,
											crmAccountStreetAddressLine1 nvarchar(100),
											crmAccountStreetAddressLine2 nvarchar(100),
											crmAccountCity nvarchar(100),
											crmAccountState nvarchar(100),
											crmAccountCountry nvarchar(100),
											crmAccountPostalCode nvarchar(100),
											crmAccountPhoneNumber nvarchar(100),
											crmAccountEmailAddress nvarchar(100),
											crmAccountWebSite nvarchar(100),
											crmAccountManagedAccount bit,
											leisureAccountIataGlobalAccountManager int,
											accountIndustry int,
											isDisabled bit);

		INSERT INTO @LeisureAccountData(TableName,nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
										crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
										crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,accountIndustry,isDisabled)
		SELECT MIN(TableName) AS TableName,nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,
				crmAccountState,crmAccountCountry,crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,
				crmAccountManagedAccount,leisureIataGlobalAccountManager,accountIndustry,isDisabled
		FROM (SELECT 'SOURCE' AS TableName,account.Name AS nodeName,account.Name AS crmAccountName,UPPER(account.AccountID) AS leisureAccountCrmGUID,
					ISNULL(Address1_Line1, '') AS crmAccountStreetAddressLine1,ISNULL(Address1_Line2, '') AS crmAccountStreetAddressLine2,
					ISNULL(Address1_City, '') AS crmAccountCity,ISNULL(Address1_StateOrProvince, '') AS crmAccountState,ISNULL(Address1_Country, '') AS crmAccountCountry,
					ISNULL(Address1_PostalCode, '') AS crmAccountPostalCode,ISNULL(Telephone1, '') AS crmAccountPhoneNumber,ISNULL(EMailAddress1, '') AS crmAccountEmailAddress,
					ISNULL(WebSiteURL, '') AS crmAccountWebSite,CASE account.phg_leisuresalesstatus WHEN 100000002 THEN 1 ELSE 0 END AS crmAccountManagedAccount,
					umbracoIataGlobalAccountManager.nodeId AS leisureIataGlobalAccountManager,umbracoIndustry.nodeId AS accountIndustry,0 AS isDisabled
				FROM (SELECT DISTINCT Name,account.AccountID,Address1_Line1,Address1_Line2,Address1_City,Address1_StateOrProvince,Address1_Country,Address1_PostalCode,Telephone1,
								EMailAddress1,WebSiteURL,account.phg_leisuresalesstatus,phg_iataglobalaccountmanagerid,Account.IndustryCode
						FROM LocalCRM.dbo.Account AS account
						WHERE Account.statecode = 0
							AND Account.phg_leisuresalesstatus IN (100000001,100000002)
							AND account.phg_leisuresales = 1
							AND account.phg_showinmemberportalleisure = 1
					 ) account
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIataGlobalAccountManager ON CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/@nodeType)[1]','nvarchar(MAX)') = 2145
																										AND CAST(umbracoIataGlobalAccountManager.xml as xml).value('(/PhgEmployee/crmContactGuid)[1]','uniqueidentifier') = account.phg_iataglobalaccountmanagerid
					LEFT OUTER JOIN MemberPortal.dbo.cmsContentXml as umbracoIndustry ON CAST(umbracoIndustry.xml as xml).value('(/Industry/@nodeType)[1]','nvarchar(MAX)') = 12055
																						AND CAST(umbracoIndustry.xml as xml).value('(/Industry/industryId)[1]','int') = account.IndustryCode
					UNION

					SELECT 'TARGET' AS TableName,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/@nodeName)[1]','nvarchar(MAX)') AS nodeName,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountName)[1]','nvarchar(MAX)') AS crmAccountName,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/leisureAccountCrmGuid)[1]','uniqueidentifier') AS leisureAccountCrmGuid,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountStreetAddressLine1)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine1,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountStreetAddressLine2)[1]','nvarchar(MAX)'), '') AS crmAccountStreetAddressLine2,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountCity)[1]','nvarchar(MAX)'), '') AS crmAccountCity,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountState)[1]','nvarchar(MAX)'), '') AS crmAccountState,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountCountry)[1]','nvarchar(MAX)'), '') AS crmAccountCountry,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountPostalCode)[1]','nvarchar(MAX)'), '') AS crmAccountPostalCode,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountPhoneNumber)[1]','nvarchar(MAX)'), '') AS crmAccountPhoneNumber,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountEmailAddress)[1]','nvarchar(MAX)'), '') AS crmAccountEmailAddress,
						ISNULL(CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountWebSite)[1]','nvarchar(MAX)'), '') AS crmAccountWebSite,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/crmAccountManagedAccount)[1]','BIT') AS crmAccountManagedAccount,
						ligam.id AS leisureIataGlobalAccountManager,
						ai.id AS accountIndustry,
						CAST(cmsContentXml.xml as xml).value('(/LeisureAccount/disabled)[1]','bit') AS isDisabled
					FROM (
						SELECT xml,
							CAST(cx.xml as xml).value('(/LeisureAccount/leisureAccountIataGlobalAccountManager)[1]','nvarchar(100)') globalAccountManager,
							CAST(cx.xml as xml).value('(/LeisureAccount/crmAccountIndustry)[1]','nvarchar(100)') accountIndustry
						FROM MemberPortal.dbo.cmsContentXml cx JOIN MemberPortal.dbo.cmsContent c ON cx.nodeId = c.nodeId JOIN MemberPortal.dbo.cmsContentType ct ON c.contentType = ct.nodeId
						WHERE ct.alias = 'LeisureAccount'
					) cmsContentXml
						LEFT JOIN MemberPortal.dbo.umbracoNode ligam ON ligam.uniqueID = CAST(SUBSTRING(globalAccountManager, 16, 8) + '-' + SUBSTRING(globalAccountManager, 24, 4) + '-' + SUBSTRING(globalAccountManager, 28, 4) + '-' + SUBSTRING(globalAccountManager, 32, 4) + '-' + SUBSTRING(globalAccountManager, 36, 12) AS uniqueidentifier)
						LEFT JOIN MemberPortal.dbo.umbracoNode ai ON ai.uniqueID = CAST(SUBSTRING(accountIndustry, 16, 8) + '-' + SUBSTRING(accountIndustry, 24, 4) + '-' + SUBSTRING(accountIndustry, 28, 4) + '-' + SUBSTRING(accountIndustry, 32, 4) + '-' + SUBSTRING(accountIndustry, 36, 12) AS uniqueidentifier)
			  ) unioned
		GROUP BY nodeName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureIataGlobalAccountManager,accountIndustry,isDisabled
		HAVING COUNT(*) = 1;


		--SELECT * FROM @LeisureAccountData ORDER BY nodeName, TableName


		INSERT INTO CmsImport.dbo.LeisureAccounts(Name,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState, crmAccountCountry, crmAccountPostalCode, crmAccountPhoneNumber,
										crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,0,accountIndustry
		FROM @LeisureAccountData
		WHERE TableName = 'SOURCE';


		INSERT INTO CmsImport.dbo.LeisureAccounts(Name,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState, crmAccountCountry, crmAccountPostalCode, crmAccountPhoneNumber,
										crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,crmAccountDisabled,accountIndustry)
		SELECT crmAccountName,crmAccountName,leisureAccountCrmGUID,crmAccountStreetAddressLine1,crmAccountStreetAddressLine2,crmAccountCity,crmAccountState,crmAccountCountry,
				crmAccountPostalCode,crmAccountPhoneNumber,crmAccountEmailAddress,crmAccountWebSite,crmAccountManagedAccount,leisureAccountIataGlobalAccountManager,1,accountIndustry
		FROM @LeisureAccountData T
		WHERE T.TableName = 'TARGET'
			AND T.isDisabled = 0
			AND NOT EXISTS ( SELECT 1 FROM @LeisureAccountData S WHERE S.TableName = 'SOURCE' AND T.leisureAccountCrmGUID = S.leisureAccountCrmGUID );
	END
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
