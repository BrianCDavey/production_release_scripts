USE Reservations
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 12/17/2019 10:51:09 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[HistoricHotelsReservations_Monthly]'
GO

ALTER PROCEDURE [rpt].[HistoricHotelsReservations_Monthly]
	@Month int,
	@Year int,
	@YTD bit
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	/*
	3.	Guest Name Report
	a.	This report will contain guest information for reservations made during the prior month (or year to date - based on the param).
	b.	This report will be automatically generated every Monday for reservations made the prior Sunday through Saturday.
	c.	The fields in the report will be AS follows
	*/

	DECLARE @startDate date,@endDate date

	---------------------------------------------
	IF @YTD = 0
	BEGIN
		SET @startDate = CONVERT(varchar(20),@Year) + '-' + CONVERT(varchar(20),@Month) + '-01'
		SET @endDate = EOMONTH(@startDate)
	END
	ELSE
	BEGIN
		SET @startDate = CONVERT(varchar(20),@Year) + '-01-01'
		SET @endDate = CONVERT(varchar(20),@Year) + '-12-31'
	END
	---------------------------------------------

	---------------------------------------------
	IF OBJECT_ID('tempdb..#HOTELS') IS NOT NULL
		DROP TABLE #HOTELS;
	CREATE TABLE #HOTELS(synxisID int,hotelCode nvarchar(20),PRIMARY KEY CLUSTERED(synxisID,hotelCode))

	INSERT INTO #HOTELS(synxisID,hotelCode)
	SELECT DISTINCT synxisID,hotelCode
	FROM Core.dbo.hotels_Synxis
	---------------------------------------------

	---------------------------------------------
	SELECT mrt.ConfirmationNumber,mrt.GuestFirstName,mrt.GuestLastName,mrt.CustomerEmail,mrt.CustomerAddress1,mrt.CustomerAddress2,
			mrt.CustomerCity,mrt.CustomerState,mrt.CustomerCountry,mrt.CustomerPostalCode,
			COALESCE(HRS.hotelName, HRO.hotelName) AS HotelName,
			COALESCE(HRS.code, HRO.code) AS HotelCode,
			mrt.hotelID,mrt.openhospitalityId,mrt.arrivaldate, mrt.departuredate,mrt.ratetypecode,mrt.reservationrevenueUSD,mrt.Rooms,
			mrt.Nights,ConfirmationDate,mrt.IATANumber,mrt.SubsourceReportLabel,mrt.Channel,mrt.Status -- (cancelled  vs  confirmed)
	FROM Reservations.dbo.MostRecentTransactionsReporting mrt
		LEFT JOIN Core.dbo.hotels_OpenHospitality HOH ON mrt.OpenHospitalityID = HOH.openHospitalityCode
		LEFT JOIN #HOTELS h ON MRT.hotelID = h.synxisID
		LEFT JOIN Hotels.dbo.hotelsReporting HRS ON h.hotelCode = HRS.code
		LEFT JOIN Hotels.dbo.hotelsReporting HRO ON HOH.hotelCode = HRO.code
	WHERE (
			OpenHospitalityID IS  NOT NULL OR OpenHospitalityID <> '' -- we want all OpenHospitality bookings
			OR subSourceReportLabel LIKE '%historic%' --and we also want anything with a subsourcereportlabel with historic in it
			OR subSourceReportLabel LIKE '%HHA%' --and we also want anything with a subsourcereportlabel with historic in it
			OR (ChannelReportLabel = 'GDS' AND (HRO.mainBrandCode IN('HE','HW') OR HRS.mainBrandCode IN('HE','HW'))) -- need to include GDS bookings for  HE/HW _only_ hotels
		   )
		AND mrt.confirmationDate BETWEEN @startDate AND @endDate
	ORDER BY COALESCE(HRS.code, HRO.code);
	---------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
