USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 12/17/2019 10:52:06 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [rpt].[HistoricHotelsReservations_Monthly]'
GO



ALTER PROCEDURE [rpt].[HistoricHotelsReservations_Monthly]

	@Month int ,
	@Year int ,
	@YTD bit
	AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	/*
	3.	Guest Name Report
	a.	This report will contain guest information for reservations made during the prior month (or year to date - based on the param).
	b.	This report will be automatically generated every Monday for reservations made the prior Sunday through Saturday.
	c.	The fields in the report will be AS follows
	*/

	DECLARE @startDate date,@endDate date

	---------------------------------------------
	IF @YTD = 0
	BEGIN
		SET @startDate = CONVERT(varchar(20),@Year) + '-' + CONVERT(varchar(20),@Month) + '-01'
		SET @endDate = EOMONTH(@startDate)
	END
	ELSE
	BEGIN
		SET @startDate = CONVERT(varchar(20),@Year) + '-01-01'
		SET @endDate = CONVERT(varchar(20),@Year) + '-12-31'
	END
	---------------------------------------------

	---------------------------------------------
	IF OBJECT_ID('tempdb..#CRS') IS NOT NULL
		DROP TABLE #CRS;

	CREATE TABLE #CRS
	(
		BookingSourceID int NOT NULL,
		channel nvarchar(50),
		[xbeTemplateName] nvarchar(250),
		subSourceReportLabel nvarchar(250),
		ChannelReportLabel varchar(20),
		IsHistoric bit NOT NULL,

		PRIMARY KEY CLUSTERED(BookingSourceID)
	)

	INSERT INTO #CRS(BookingSourceID,channel,xbeTemplateName,subSourceReportLabel,ChannelReportLabel,IsHistoric)
	SELECT BookingSourceID,channel,[xbeTemplateName],subSourceReportLabel,ChannelReportLabel,
		CASE
			WHEN subSourceReportLabel LIKE '%historic%' THEN CONVERT(bit,1) --and we also want anything with a subsourcereportlabel with historic in it
			WHEN subSourceReportLabel LIKE '%HHA%' THEN CONVERT(bit,1) --and we also want anything with a subsourcereportlabel with historic in it
			ELSE CONVERT(bit,0)
		END AS IsHistoric
	FROM
	(
		SELECT bs.BookingSourceID,bs.channel,bs.ibeSourceName AS [xbeTemplateName],
			CASE
				WHEN bs.channel = 'Booking Engine' OR bs.channel = 'Mobile Web' THEN 
					CASE WHEN bs.secondarySource = 'iPrefer APP' THEN 'iPrefer APP' ELSE COALESCE (bs.ibeSourceName,'Hotel Booking Engine') END
				WHEN bs.channel = 'PMS Rez Synch' THEN 'PMS Rez Synch' 
				WHEN bs.channel = 'Voice' THEN 
					CASE
						WHEN (bs.subSourceCode = '' OR bs.subSourceCode IS NULL) THEN 'Voice - No Subchannel' 
						ELSE bs.subSourceCode
					END 
				WHEN (bs.subSourceCode = '' OR bs.subSourceCode IS NULL) THEN 
					CASE WHEN (bs.secondarySource = '' OR bs.secondarySource IS NULL) THEN bs.channel ELSE bs.secondarySource END 
				WHEN (bs.subSource = '' OR bs.subSource IS NULL) THEN bs.subSourceCode
				ELSE bs.subSource
			END AS subSourceReportLabel,
			CASE bs.Channel 
				WHEN 'Booking Engine' THEN 
					CASE 
						WHEN bs.secondarySource = 'iPrefer APP' THEN 'IBE - PHG'
						WHEN ISNULL(bs.IsStatic,0) = 1 THEN 'IBE - PHG'
						ELSE 'IBE - Hotel' 
					END
				WHEN 'Channel Connect' THEN 'OTA'
				WHEN 'GDS' THEN 'GDS'
				WHEN 'Google' THEN 'OTA'
				WHEN 'IDS' THEN 'OTA'
				WHEN 'Mobile Web' THEN
					CASE WHEN ISNULL(bs.IsStatic,0) = 1 THEN 'IBE - PHG'
						ELSE 'IBE - Hotel' 
					END		
				WHEN 'PMS Rez Synch' THEN 'PMS'
				WHEN 'Voice' THEN
					CASE WHEN bs.CRO_Code IS NULL THEN 'Voice - Hotel Agent'
						ELSE 'Voice - PHG'
					END
			END AS ChannelReportLabel
		FROM Reservations.dbo.vw_CRS_BookingSource bs
	) x
	---------------------------------------------

	---------------------------------------------
	IF OBJECT_ID('tempdb..#HOTEL') IS NOT NULL
		DROP TABLE #HOTEL;

	CREATE TABLE #HOTEL
	(
		HotelID int NOT NULL,
		SynXisID bigint,
		OpenHospID bigint,
		hotelName nvarchar(250),
		hotelCode varchar(20),
		IsHistoric bit NOT NULL,

		PRIMARY KEY CLUSTERED(HotelID,hotelCode)
	)

	INSERT INTO #HOTEL(HotelID,SynXisID,OpenHospID,hotelName,hotelCode,IsHistoric)
	SELECT HotelID,SynXisID,OpenHospID,ISNULL(hotelName,N''),ISNULL(hotelCode,''),
		CASE
			WHEN hrx_MainBrandCode IN('HE','HW') THEN CONVERT(bit,1)
			WHEN hro_MainBrandCode IN('HE','HW') THEN CONVERT(bit,1)
			ELSE CONVERT(bit,0)
		END AS IsHistoric
	FROM
	(
		SELECT hh.HotelID,hh.SynXisID,hh.OpenHospID,ISNULL(hrx.hotelName,hro.hotelName) AS hotelName,ISNULL(hrx.code,hro.code) AS hotelCode,
				hrx.MainBrandCode AS hrx_MainBrandCode,hro.MainBrandCode AS hro_MainBrandCode
		FROM Hotels.dbo.Hotel hh
			LEFT JOIN Hotels.dbo.hotelsReporting hrx WITH(NOLOCK) ON hh.SynXisID = hrx.synxisID
			LEFT JOIN Hotels.dbo.hotelsReporting hro WITH(NOLOCK) ON hh.OpenHospID = hro.openHospitalityCode
	) x
	WHERE SynXisID IS NOT NULL
		OR OpenHospID IS NOT NULL
		OR hotelName IS NOT NULL
		OR hotelCode IS NOT NULL
		OR hrx_MainBrandCode IS NOT NULL
		OR hro_MainBrandCode IS NOT NULL
	order by hotelCode
	---------------------------------------------

	---------------------------------------------
	;WITH cte_Location
	AS
	(
		SELECT loc.LocationID,ISNULL(NULLIF(cty.City_Text,N''),N'unknown') AS City_Text,ISNULL(NULLIF(ste.State_Text,N''),N'unknown') AS State_Text,
			ISNULL(NULLIF(con.Country_Text,N''),N'unknown') AS Country_Text,ISNULL(NULLIF(pc.PostalCode_Text,N''),N'unknown') AS PostalCode_Text
		FROM Reservations.dbo.[Location] loc
			LEFT JOIN Reservations.dbo.City cty ON cty.CityID = loc.CityID
			LEFT JOIN Reservations.dbo.[State] ste ON ste.StateID = loc.StateID
			LEFT JOIN Reservations.dbo.Country con ON con.CountryID = loc.CountryID
			LEFT JOIN Reservations.dbo.PostalCode pc ON pc.PostalCodeID = loc.PostalCodeID
	),
	cte_Guest
	AS
	(
		SELECT g.GuestID,ISNULL(g.FirstName,N'') AS GuestFirstName,ISNULL(g.LastName,N'') AS GuestLastName,
				ISNULL(ge.emailAddress,N'') AS CustomerEmail,ISNULL(g.Address1,N'') AS CustomerAddress1,ISNULL(g.Address2,N'') AS CustomerAddress2,
				loc.City_Text AS CustomerCity,loc.State_Text AS CustomerState,loc.Country_Text AS CustomerCountry,loc.PostalCode_Text AS CustomerPostalCode
		FROM Reservations.dbo.Guest g
			LEFT JOIN Reservations.dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
			LEFT JOIN cte_Location loc ON loc.LocationID = g.LocationID
	)
	SELECT t.confirmationNumber,g.GuestFirstName,g.GuestLastName,g.CustomerEmail,g.CustomerAddress1,g.CustomerAddress2,
			g.CustomerCity,g.CustomerState,g.CustomerCountry,g.CustomerPostalCode,hh.hotelName,hh.hotelCode,
			hh.SynXisID AS hotelID,hh.OpenHospID AS openhospitalityId,td.arrivalDate,td.departureDate,rac.RateCode AS rateTypeCode,
			Reservations.[dbo].[convertCurrencyToUSD](td.reservationRevenue,td.currency,ts.confirmationDate) AS [reservationRevenueUSD],td.rooms,
			td.nights,ts.confirmationDate,iata.IATANumber,crs.subSourceReportLabel,crs.channel,ts.[status]
	FROM Reservations.dbo.Transactions t
		INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
		INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
		LEFT JOIN cte_Guest g ON g.GuestID = t.GuestID
		LEFT JOIN Reservations.dbo.hotel h ON h.HotelID = t.HotelID
		LEFT JOIN #HOTEL hh ON hh.HotelID = h.Hotel_hotelID
		LEFT JOIN Reservations.dbo.RateCode rac ON rac.RateCodeID = t.RateCodeID
		LEFT JOIN Reservations.dbo.IATANumber iata WITH(NOLOCK) ON iata.IATANumberID = t.IATANumberID
		LEFT JOIN #CRS crs ON crs.BookingSourceID = t.CRS_BookingSourceID
	WHERE ts.confirmationDate BETWEEN @startDate AND @endDate
		AND
		(
			hh.OpenHospID IS NOT NULL
			OR crs.IsHistoric = 1
			OR (crs.ChannelReportLabel = 'GDS' AND hh.IsHistoric = 1) -- need to include GDS bookings for  HE/HW _only_ hotels
		)

		END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
