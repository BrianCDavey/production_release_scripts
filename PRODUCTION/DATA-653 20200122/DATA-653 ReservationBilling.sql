USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/22/2020 9:58:46 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [test].[tdrJoined_Reservation]'
GO












ALTER VIEW [test].[tdrJoined_Reservation]
AS
     SELECT 
			3 as transactionSourceID, --3 = I Prefer Manual Transactions
			CAST(tdr.Transaction_Id as nvarchar(20)) as transactionKey,
            tdr.Booking_ID as confirmationNumber, 
			hotels.HotelCode AS phgHotelCode,
            NULL AS crsHotelID,
            hotels.hotelName AS hotelName,
            COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
            COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
            null as chainID,
            'IPM' as chainName,
            'IPM' as bookingStatus,
            'iPrefer Manual Entry' as synxisBillingDescription,
            'iPrefer Manual Entry' AS bookingChannel,
            'iPrefer Manual Entry' AS bookingSecondarySource,
            'iPrefer Manual Entry' AS bookingSubSourceCode,
            null as bookingTemplateGroupId,
            'IPM' as bookingTemplateAbbreviation,
            'IPM' as xbeTemplateName,
            'iPrefer Manual Entry' as CROcode,
            null as bookingCroGroupID,
            'iPrefer Manual Entry' as bookingRateCategoryCode,
            'iPrefer Manual' as bookingRateCode,
            'iPrefer Manual' as bookingIATA,
            tdr.Reward_Posting_Date as transactionTimeStamp,
            (SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date), (tdr.Departure_Date), (tdr.Reward_Posting_Date)) AS Fields(d)) as confirmationDate,
            tdr.Arrival_Date as arrivalDate,
            tdr.Departure_Date as departureDate,
            CAST(null as date) as cancellationDate,
            CAST(null as varchar) as cancellationNumber,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as nights,
            1 as rooms,
            DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) as roomNights,
            tdr.Reservation_Revenue as roomRevenueInBookingCurrency,
            tdr.Currency_Code as bookingCurrencyCode,
            null as timeLoaded,
			'IPREFERMANUAL' AS [ItemCode],
			CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END as exchangeDate,
			gpCustomer.CURNCYID as hotelCurrencyCode,
			hotelCM.DECPLCUR as hotelCurrencyDecimalPlaces,
			hotelCE.XCHGRATE as hotelCurrencyExchangeRate,
			bookingCE.XCHGRATE as bookingCurrencyExchangeRate,
            null as CRSSourceID,
			'I Prefer' as loyaltyProgram,
			tdr.iPrefer_Number as loyaltyNumber,
			'iPrefer Manual Entry' as travelAgencyName,
			1 as LoyaltyNumberValidated,
			--CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END as LoyaltyNumberTagged, --Comment out in case we need in the future
			0 AS LoyaltyNumberTagged,
			tdr.Transaction_Source, 
			tdr.Booking_Source  

     FROM Loyalty.dbo.[TransactionDetailedReport] tdr
        LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
        LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
        LEFT JOIN Hotels..Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
        LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
        LEFT JOIN Hotels..Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.vw_local_exchange_rates hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.vw_local_exchange_rates bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
			AND CASE WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date ELSE tdr.Arrival_Date END = bookingCE.EXCHDATE
		--LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
		--	AND tdr.Hotel_Code = tc.Hotel_Code
		--	AND tc.DateTagged <= tdr.Reward_Posting_Date --Comment out in case we need in the future


  WHERE tdr.Reservation_Revenue <> 0
	AND tdr.Points_Earned <> 0
	AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') --remove epsilon SFTP and all BSI old point activity
	AND tdr.Hotel_Code <> 'PHG123'
	AND tdr.IsForMigration = 0
	AND tdr.Hotel_Code IS NOT NULL

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[Populate_tdrJoined_Reservation]'
GO




ALTER   PROCEDURE [work].[Populate_tdrJoined_Reservation]
	@startDate date = NULL,
	@endDate date = NULL
AS
BEGIN
	TRUNCATE TABLE [temp].[tdrJoined_Reservation]

	;WITH cte_TDR
	AS
	(
		SELECT
			ISNULL(td.arrivalDate,p.[ActivityCauseDate]) AS [Arrival_Date],
			hh.HotelCode AS [Hotel_Code],
			td.departureDate AS [Departure_Date],
			CASE
				WHEN p.CurrencyExchangeDate = '2001-01-01 00:00:00.000' OR p.CurrencyExchangeDate IS NULL THEN p.ActivityDate
				ELSE p.CurrencyExchangeDate
			END AS [Reward_Posting_Date],
			COALESCE(NULLIF(p.[ActivityCauseCurrency],''),'USD') AS [Currency_Code],l.loyaltyNumberName AS [iPrefer_Number],
			CASE
				WHEN ISNULL(NULLIF(p.[ActivityCauseCurrency],''),'USD') = 'USD' THEN CAST(p.Points AS decimal(20,5))/10
				ELSE CurrencyRates.dbo.convertCurrency
						(
							CAST(p.Points AS decimal(20,5))/10,
							'USD',
							p.[ActivityCauseCurrency],
							CASE
								WHEN p.CurrencyExchangeDate = '2001-01-01 00:00:00.000' OR p.CurrencyExchangeDate IS NULL THEN p.ActivityDate
								ELSE p.CurrencyExchangeDate 
							END
						)
			END AS [Reservation_Revenue],
			CASE WHEN p.Points > 0 THEN p.Points ELSE 0 END AS [Points_Earned],
			acs.ActivityCauseSystemName AS [Transaction_Source],
			p.IsForMigration,
			p.PointActivityID AS [Transaction_Id],
			p.TransactionNumber AS [Booking_ID],
			p.[ActivityDate] AS [Transaction_Date]
		FROM Loyalty.dbo.PointActivity p
			INNER JOIN loyalty.[dbo].[LoyaltyNumber] l ON p.loyaltyNumberID = l.loyaltyNumberID
			LEFT JOIN Reservations.dbo.Transactions t ON t.confirmationNumber = p.TransactionNumber
			LEFT JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			LEFT JOIN loyalty.dbo.ActivityCauseSystem acs ON acs.ActivityCauseSystemID = p.ActivityCauseSystemID
			LEFT JOIN Hotels.dbo.Hotel hh ON hh.HotelID = p.ActivityCauseHotelID
		WHERE p.PointTypeID = 3 --filter for point credits only
	)
	INSERT INTO [temp].[tdrJoined_Reservation]([transactionSourceID],[transactionKey],[confirmationNumber],[phgHotelCode],[crsHotelID],
												[hotelName],[mainBrandCode],[gpSiteID],[chainID],[chainName],[bookingStatus],
												[synxisBillingDescription],[bookingChannel],[bookingSecondarySource],[bookingSubSourceCode],
												[bookingTemplateGroupId],[bookingTemplateAbbreviation],[xbeTemplateName],[CROcode],
												[bookingCroGroupID],[bookingRateCategoryCode],[bookingRateCode],[bookingIATA],
												[transactionTimeStamp],[confirmationDate],[arrivalDate],[departureDate],[cancellationDate],
												[cancellationNumber],[nights],[rooms],[roomNights],[roomRevenueInBookingCurrency],
												[bookingCurrencyCode],[timeLoaded],[ItemCode],[exchangeDate],[hotelCurrencyCode],
												[hotelCurrencyDecimalPlaces],[hotelCurrencyExchangeRate],[bookingCurrencyExchangeRate],
												[CRSSourceID],[loyaltyProgram],[loyaltyNumber],[travelAgencyName],[LoyaltyNumberValidated],
												[LoyaltyNumberTagged],[Transaction_Source],ConfMonth,ConfYear)
	SELECT 3 AS transactionSourceID,--3 = I Prefer Manual Transactions
		tdr.Transaction_Id AS transactionKey,
		tdr.Booking_ID AS confirmationNumber,
		hotels.HotelCode AS phgHotelCode,
		NULL AS crsHotelID,
		hotels.hotelName AS hotelName,
		COALESCE(activeBrands.code,inactiveBrands.code) AS mainBrandCode,
		COALESCE(activeBrands.gpSiteID,inactiveBrands.gpSiteID) AS gpSiteID,
		NULL AS chainID,'IPM' AS chainName,
		'IPM' AS bookingStatus,
		'iPrefer Manual Entry' AS synxisBillingDescription,
		'iPrefer Manual Entry' AS bookingChannel,
		'iPrefer Manual Entry' AS bookingSecondarySource,
		'iPrefer Manual Entry' AS bookingSubSourceCode,
		NULL AS bookingTemplateGroupId,
		'IPM' AS bookingTemplateAbbreviation,
		'IPM' AS xbeTemplateName,
		'iPrefer Manual Entry' AS CROcode,
		NULL AS bookingCroGroupID,
		'iPrefer Manual Entry' AS bookingRateCategoryCode,
		'iPrefer Manual' AS bookingRateCode,
		'iPrefer Manual' AS bookingIATA,
		tdr.Reward_Posting_Date AS transactionTimeStamp,
		(SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date),(tdr.Departure_Date),(tdr.Reward_Posting_Date)) AS Fields(d)) AS confirmationDate,
		tdr.Arrival_Date AS arrivalDate,
		tdr.Departure_Date AS departureDate,
		CAST(NULL AS date) AS cancellationDate,
		CAST(NULL AS varchar) AS cancellationNumber,
		ISNULL(DATEDIFF(DAY,tdr.Arrival_Date,tdr.Departure_Date),1) AS nights,
		1 AS rooms,
		ISNULL(DATEDIFF(DAY,tdr.Arrival_Date,tdr.Departure_Date), 1) AS roomNights,
		tdr.Reservation_Revenue AS roomRevenueInBookingCurrency,
		tdr.Currency_Code AS bookingCurrencyCode,
		NULL AS timeLoaded,
		'IPREFERMANUAL' AS [ItemCode],
		CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END AS exchangeDate,
		gpCustomer.CURNCYID AS hotelCurrencyCode,
		hotelCM.DECPLCUR AS hotelCurrencyDecimalPlaces,
		hotelCE.XCHGRATE AS hotelCurrencyExchangeRate,
		bookingCE.XCHGRATE AS bookingCurrencyExchangeRate,
		NULL AS CRSSourceID,
		'I Prefer' AS loyaltyProgram,
		tdr.iPrefer_Number AS loyaltyNumber,
		'iPrefer Manual Entry' AS travelAgencyName,
		1 AS LoyaltyNumberValidated,
		--CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END AS LoyaltyNumberTagged, --comment out in case we need this in the future
		0 AS LoyaltyNumberTagged,
		tdr.Transaction_Source,
		MONTH((SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date),(tdr.Departure_Date),(tdr.Reward_Posting_Date)) AS Fields(d))) AS ConfMonth,
		YEAR((SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date),(tdr.Departure_Date),(tdr.Reward_Posting_Date)) AS Fields(d))) AS ConfYear
	FROM cte_TDR tdr --Loyalty.dbo.[TransactionDetailedReport] tdr
		LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
		LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
		LEFT JOIN Hotels.dbo.Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
		LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
		LEFT JOIN Hotels.dbo.Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
													AND CASE
															WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date
															ELSE tdr.Arrival_Date
														END = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
													AND CASE
															WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date
															ELSE tdr.Arrival_Date
														END = bookingCE.EXCHDATE
		--LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
		--											AND tdr.Hotel_Code = tc.Hotel_Code
		--											AND tc.DateTagged <= tdr.Reward_Posting_Date --comment out in case we need to bring this back later
	WHERE tdr.Reservation_Revenue <> 0
		AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP','Admin Portal') --remove epsilon SFTP and all BSI old point activity
		AND tdr.Hotel_Code <> 'PHG123'
		AND tdr.IsForMigration = 0
		AND tdr.Hotel_Code IS NOT NULL
		AND tdr.Reward_Posting_Date BETWEEN @startDate AND @endDate
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[tdrJoined_Reservation]'
GO




ALTER VIEW [work].[tdrJoined_Reservation]
AS
	SELECT 3 AS transactionSourceID, --3 = I Prefer Manual Transactions
		tdr.Transaction_Id AS transactionKey,
		tdr.Booking_ID AS confirmationNumber,
		hotels.HotelCode AS phgHotelCode,
		NULL AS crsHotelID,
		hotels.hotelName AS hotelName,
		COALESCE(activeBrands.code, inactiveBrands.code) AS mainBrandCode,
		COALESCE(activeBrands.gpSiteID, inactiveBrands.gpSiteID) AS gpSiteID,
		NULL AS chainID,'IPM' AS chainName,
		'IPM' AS bookingStatus,
		'iPrefer Manual Entry' AS synxisBillingDescription,
		'iPrefer Manual Entry' AS bookingChannel,
		'iPrefer Manual Entry' AS bookingSecondarySource,
		'iPrefer Manual Entry' AS bookingSubSourceCode,
		NULL AS bookingTemplateGroupId,
		'IPM' AS bookingTemplateAbbreviation,
		'IPM' AS xbeTemplateName,
		'iPrefer Manual Entry' AS CROcode,
		NULL AS bookingCroGroupID,
		'iPrefer Manual Entry' AS bookingRateCategoryCode,
		'iPrefer Manual' AS bookingRateCode,
		'iPrefer Manual' AS bookingIATA,
		tdr.Reward_Posting_Date AS transactionTimeStamp,
		(SELECT MIN(d) FROM (VALUES (tdr.Arrival_Date), (tdr.Departure_Date), (tdr.Reward_Posting_Date)) AS Fields(d)) AS confirmationDate,
		tdr.Arrival_Date AS arrivalDate,
		tdr.Departure_Date AS departureDate,
		CAST(NULL AS date) AS cancellationDate,
		CAST(NULL AS varchar) AS cancellationNumber,
		DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) AS nights,
		1 AS rooms,
		DATEDIFF(DAY, tdr.Arrival_Date, tdr.Departure_Date) AS roomNights,
		tdr.Reservation_Revenue AS roomRevenueInBookingCurrency,
		tdr.Currency_Code AS bookingCurrencyCode,
		NULL AS timeLoaded,
		'IPREFERMANUAL' AS [ItemCode],
		CASE WHEN tdr.Reward_Posting_Date >= GETDATE() THEN tdr.Transaction_Date ELSE tdr.Reward_Posting_Date END AS exchangeDate,
		gpCustomer.CURNCYID AS hotelCurrencyCode,
		hotelCM.DECPLCUR AS hotelCurrencyDecimalPlaces,
		hotelCE.XCHGRATE AS hotelCurrencyExchangeRate,
		bookingCE.XCHGRATE AS bookingCurrencyExchangeRate,
		NULL AS CRSSourceID,
		'I Prefer' AS loyaltyProgram,
		tdr.iPrefer_Number AS loyaltyNumber,
		'iPrefer Manual Entry' AS travelAgencyName,
		1 AS LoyaltyNumberValidated,
		--CASE WHEN tc.[iPrefer Number] IS NULL THEN 0 ELSE 1 END AS LoyaltyNumberTagged,--Comment out in case we need in the future
		0 AS LoyaltyNumberTagged,
		tdr.Transaction_Source,
		tdr.Booking_Source  
	FROM Loyalty.dbo.[TransactionDetailedReport] tdr
		LEFT JOIN Hotels.dbo.Hotel hotels ON hotels.HotelCode = tdr.Hotel_Code
		LEFT JOIN work.hotelActiveBrands ON hotels.HotelCode = hotelActiveBrands.hotelCode 
		LEFT JOIN Hotels.dbo.Collection activeBrands ON hotelActiveBrands.mainHeirarchy = activeBrands.Hierarchy
		LEFT JOIN work.hotelInactiveBrands ON hotels.HotelCode = hotelInactiveBrands.hotelCode
		LEFT JOIN Hotels.dbo.Collection inactiveBrands ON hotelInactiveBrands.mainHeirarchy = inactiveBrands.Hierarchy
		LEFT JOIN work.GPCustomerTable gpCustomer ON hotels.HotelCode = gpCustomer.CUSTNMBR
		LEFT JOIN work.[local_exchange_rates] hotelCE ON gpCustomer.CURNCYID = hotelCE.CURNCYID 
													AND CASE
															WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date
															ELSE tdr.Arrival_Date
														END = hotelCE.EXCHDATE
		LEFT JOIN work.GPCurrencyMaster hotelCM ON gpCustomer.CURNCYID = hotelCM.CURNCYID			
		LEFT JOIN work.[local_exchange_rates] bookingCE ON tdr.Currency_Code = bookingCE.CURNCYID 
													AND CASE
															WHEN tdr.Arrival_Date >= GETDATE() THEN tdr.Reward_Posting_Date
															ELSE tdr.Arrival_Date
														END = bookingCE.EXCHDATE
		--LEFT JOIN Superset.BSI.TaggedCustomers tc ON tdr.iPrefer_Number = tc.[iPrefer Number]
		--											AND tdr.Hotel_Code = tc.Hotel_Code
		--											AND tc.DateTagged <= tdr.Reward_Posting_Date --Comment out in case we need in the future
	WHERE tdr.Reservation_Revenue <> 0
		AND tdr.Points_Earned <> 0
		AND tdr.Transaction_Source NOT IN ('PHG File','Hotel Portal','Admin','SFTP', 'Admin Portal') --remove epsilon SFTP and all BSI old point activity
		AND tdr.Hotel_Code <> 'PHG123'
		AND tdr.IsForMigration = 0
		AND tdr.Hotel_Code IS NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[postCreditMemoIntegration]'
GO

-- =============================================
-- Author:		Tory Greco
-- Create date: Sept 5, 2012
-- Description:	REwrite of original form Kris Scott
--				connect sop numbers to credit memos after the smart connect integration
-- History: 2020-01-17 move from superset to ReservationBillings
-- =============================================
CREATE procedure [dbo].[postCreditMemoIntegration] 
	@startDate date, 
	@endDate date,
	@invoiceDate date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	SET @endDate = DATEADD(DAY,-1,DATEADD(MONTH,1,@startdate)) --the end of the start month

	select	SOPNUMBE, CUSTNMBR, DOCID, DOCDATE, BACHNUMB 
	into	#SQP03SOP10100C
	from	chisqp01.IC.dbo.SOP10100
	where	DOCID = 'CMIP'
	and		BACHNUMB like 'IPREFERCM%'
	--select * from #SQP03SOP10100C

	UPDATE	CM
	SET		CM.sopNumber = gp.SOPNUMBE,
			CM.Invoice_Date = gp.DOCDATE
	FROM	#SQP03SOP10100C	as gp
	join	ReservationBilling.loyalty.Credit_Memos CM with (nolock)
		on	CM.Hotel_Code = gp.CUSTNMBR
	WHERE	CM.Create_Date >= @startDate
	AND		CM.Redemption_Date <= @enddate
	AND		CM.sopNumber is null
	
	drop table #SQP03SOP10100C
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
