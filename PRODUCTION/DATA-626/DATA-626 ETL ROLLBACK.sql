/*
Run this script on:

        chi-lt-00032377.ETL    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/17/2019 3:44:13 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonExportPointTransaction]'
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] DROP CONSTRAINT [PK_dbo_EpsilonExportPointTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonExportReservation]'
GO
ALTER TABLE [dbo].[EpsilonExportReservation] DROP CONSTRAINT [PK_dbo_EpsilonExportReservation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonExport_LOG]'
GO
ALTER TABLE [dbo].[EpsilonExport_LOG] DROP CONSTRAINT [PK_EpsilonExport_LOG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonPointTransactionLog]'
GO
ALTER TABLE [dbo].[EpsilonPointTransactionLog] DROP CONSTRAINT [PK_dbo_EpsilonPointTransactionLog]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonReservationLog]'
GO
ALTER TABLE [dbo].[EpsilonReservationLog] DROP CONSTRAINT [PK_dbo_EpsilonReservationLog]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[EpsilonExport_LOG]'
GO
ALTER TABLE [dbo].[EpsilonExport_LOG] DROP CONSTRAINT [DF_EpsilonExport_LOG_LogStatus]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonReward]'
GO
DROP TABLE [dbo].[Import_EpsilonReward]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonRedemption]'
GO
DROP TABLE [dbo].[Import_EpsilonRedemption]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonPoint]'
GO
DROP TABLE [dbo].[Import_EpsilonPoint]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Import_EpsilonMember]'
GO
DROP TABLE [dbo].[Import_EpsilonMember]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonReservationLog]'
GO
DROP TABLE [dbo].[EpsilonReservationLog]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonPointTransactionLog]'
GO
DROP TABLE [dbo].[EpsilonPointTransactionLog]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonExport_LOG]'
GO
DROP TABLE [dbo].[EpsilonExport_LOG]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonExportReservation]'
GO
DROP TABLE [dbo].[EpsilonExportReservation]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EpsilonExportPointTransaction]'
GO
DROP TABLE [dbo].[EpsilonExportPointTransaction]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
