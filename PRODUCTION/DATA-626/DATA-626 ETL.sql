USE ETL
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.ETL    -  This database will be modified

to synchronize it with:

        chi-lt-00032377.ETL

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.19.12066 from Red Gate Software Ltd at 10/17/2019 3:43:00 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonExportPointTransaction]'
GO
CREATE TABLE [dbo].[EpsilonExportPointTransaction]
(
[EpsilonExportPointTransactionID] [int] NOT NULL IDENTITY(1, 1),
[EpsilonExport_LOG_ID] [int] NULL,
[01] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TRANSACTION_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACT_TRANSACTION_ID] [int] NULL,
[PROFILE_ID] [int] NULL,
[CARD_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_TYPE_CODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TRANS_DESCRIPTION] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TRANSACTION_DATE] [date] NOT NULL,
[END_DATE] [date] NULL,
[TRANSACTION_NET_AMOUNT] [decimal] (18, 2) NULL,
[ELIGIBLE_REVENUE] [int] NULL,
[TAX_AMOUNT] [int] NULL,
[POST_SALES_ADJUSTMNT_AMT] [int] NULL,
[SHIPPING_HANDLING] [int] NULL,
[STATUS] [int] NULL,
[DEVICE_ID] [int] NULL,
[DEVICE_USERID] [int] NULL,
[AUTHORIZATION_CODE] [int] NULL,
[ORIGINAL_TRANSACTION_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SKIP_MOMENT_ENGINE_IND] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CURRENCY_CODE] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACCT_SRC_CODE] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SRC_ACCOUNT_NUM] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIVITY_DATE] [datetime] NULL,
[CLIENT_FILE_ID] [int] NOT NULL,
[CLIENT_FILE_REC_NUM] [int] NULL,
[BRAND_ORG_CODE] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ASSOCIATE_NUM] [int] NULL,
[GROSS_AMOUNT] [decimal] (18, 2) NULL,
[NET_AMT] [decimal] (18, 2) NULL,
[PROG_CURR_GROSS_AMT] [int] NULL,
[TXN_SEQ_NUM] [int] NULL,
[TXN_SOURCE_CODE] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TXN_SUBTYPE_CODE] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TIER] [int] NULL,
[TRANS_LOCATION] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[blank1] [int] NULL,
[PROGRAM_CODE] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EXCHANGE_RATE_ID] [int] NULL,
[GRATUITY] [int] NULL,
[SRC_CUSTOMER_ID] [int] NULL,
[DISC_AMT] [int] NULL,
[TXN_CHANNEL_CODE] [int] NULL,
[TXN_BUSINESS_DATE] [date] NULL,
[PROMO_CODE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUYER_TYPE_CODE] [int] NULL,
[RMB_METHOD_CODE] [int] NULL,
[RMB_VENDOR_CODE] [int] NULL,
[RMB_DATE] [int] NULL,
[blank2] [int] NULL,
[PROG_CURR_NET_AMT] [int] NULL,
[PROG_CURR_TAX_AMT] [int] NULL,
[PROG_CURR_POST_SALES_ADJ_AMT] [int] NULL,
[PROG_CURR_SHIPPING_HANDLING] [int] NULL,
[PROG_CURR_ELIG_REVENUE] [int] NULL,
[PROG_CURR_GRATUITY] [int] NULL,
[TXN_TYPE_CODE] [int] NULL,
[ORIGINAL_STORE_CODE] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_TRANSACTION_DATE] [date] NULL,
[ORIGINAL_TRANSACTION_END_DATE] [date] NULL,
[SUSPEND_REASON] [int] NULL,
[SUSPEND_TRANSACTION] [int] NULL,
[JSON_EXTERNAL_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTING_KEY_CODE] [int] NULL,
[POSTING_KEY_VALUE] [int] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_EpsilonExportPointTransaction] on [dbo].[EpsilonExportPointTransaction]'
GO
ALTER TABLE [dbo].[EpsilonExportPointTransaction] ADD CONSTRAINT [PK_dbo_EpsilonExportPointTransaction] PRIMARY KEY CLUSTERED  ([EpsilonExportPointTransactionID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonExportReservation]'
GO
CREATE TABLE [dbo].[EpsilonExportReservation]
(
[EpsilonExportReservationID] [int] NOT NULL IDENTITY(1, 1),
[EpsilonExport_LOG_ID] [int] NULL,
[01] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RESERVATION_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACT_RESERVATION_ID] [int] NULL,
[PROFILE_ID] [int] NULL,
[CARD_NUMBER] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_TYPE_CODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RESERVATION_DESCRIPTION] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ARRIVAL_DATE] [date] NOT NULL,
[DEPARTURE_DATE] [date] NULL,
[RESERVATION_REVENUE] [decimal] (18, 2) NULL,
[ELIGIBLE_REVENUE] [int] NULL,
[TAX_AMOUNT] [int] NULL,
[POST_SALES_ADJUSTMNT_AMT] [int] NULL,
[SHIPPING_HANDLING] [int] NULL,
[STATUS] [int] NULL,
[DEVICE_ID] [int] NULL,
[DEVICE_USERID] [int] NULL,
[AUTHORIZATION_CODE] [int] NULL,
[ORIGINAL_RESERVATION_NUMBER] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SKIP_MOMENT_ENGINE_IND] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CURRENCY_CODE] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ACCT_SRC_CODE] [varchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SRC_ACCOUNT_NUM] [nvarchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACTIVITY_DATE] [datetime] NULL,
[CLIENT_FILE_ID] [int] NOT NULL,
[CLIENT_FILE_REC_NUM] [int] NULL,
[BRAND_ORG_CODE] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ASSOCIATE_NUM] [int] NULL,
[GROSS_AMOUNT] [decimal] (18, 2) NULL,
[NET_AMT] [decimal] (18, 2) NULL,
[PROG_CURR_GROSS_AMT] [int] NULL,
[TXN_SEQ_NUM] [int] NULL,
[TXN_SOURCE_CODE] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TXN_SUBTYPE_CODE] [int] NULL,
[TIER] [int] NULL,
[TRANS_LOCATION] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[blank1] [int] NULL,
[PROGRAM_CODE] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EXCHANGE_RATE_ID] [int] NULL,
[GRATUITY] [int] NULL,
[SRC_CUSTOMER_ID] [int] NULL,
[DISC_AMT] [int] NULL,
[RESERVATION_CHANNEL_CODE] [int] NULL,
[BOOKING_DATE] [date] NULL,
[PROMO_CODE] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BUYER_TYPE_CODE] [int] NULL,
[RMB_METHOD_CODE] [int] NULL,
[RMB_VENDOR_CODE] [int] NULL,
[RMB_DATE] [int] NULL,
[blank2] [int] NULL,
[PROG_CURR_RES_TOTAL] [int] NULL,
[PROG_CURR_TAX_AMT] [int] NULL,
[PROG_CURR_POST_SALES_ADJ_AMT] [int] NULL,
[PROG_CURR_SHIPPING_HANDLING] [int] NULL,
[PROG_CURR_ELIG_REVENUE] [int] NULL,
[PROG_CURR_GRATUITY] [int] NULL,
[TXN_TYPE_CODE] [int] NULL,
[ORIGINAL_STORE_CODE] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORIGINAL_RESERVATION_ARRIVAL] [date] NULL,
[ORIGINAL_RESERVATION_DEPARTURE] [date] NULL,
[SUSPEND_REASON] [int] NULL,
[SUSPEND_RESERVATION] [int] NULL,
[POSTING_KEY_CODE] [int] NULL,
[POSTING_KEY_VALUE] [int] NULL,
[JSON_EXTERNAL_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CRS_SOURCE] [varchar] (22) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CARD_NUMBER_SRC] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GUEST_FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUEST_LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUEST_EMAIL_ADDRESS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_EpsilonExportReservation] on [dbo].[EpsilonExportReservation]'
GO
ALTER TABLE [dbo].[EpsilonExportReservation] ADD CONSTRAINT [PK_dbo_EpsilonExportReservation] PRIMARY KEY CLUSTERED  ([EpsilonExportReservationID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonExport_LOG]'
GO
CREATE TABLE [dbo].[EpsilonExport_LOG]
(
[EpsilonExport_LOG_ID] [int] NOT NULL IDENTITY(1, 1),
[EpsilonApplication] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LogDate] [datetime] NOT NULL,
[LogStatus] [tinyint] NOT NULL CONSTRAINT [DF_EpsilonExport_LOG_LogStatus] DEFAULT ((0)),
[LogStatus_Desc] AS (case [LogStatus] when (0) then 'Not Started' when (1) then 'In Progress' when (2) then 'Finished' when (3) then 'Error' when (4) then 'Resolved' else 'Error' end),
[FileLocation] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_EpsilonExport_LOG] on [dbo].[EpsilonExport_LOG]'
GO
ALTER TABLE [dbo].[EpsilonExport_LOG] ADD CONSTRAINT [PK_EpsilonExport_LOG] PRIMARY KEY CLUSTERED  ([EpsilonExport_LOG_ID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonPointTransactionLog]'
GO
CREATE TABLE [dbo].[EpsilonPointTransactionLog]
(
[EpsilonPointTransactionLogID] [int] NOT NULL IDENTITY(1, 1),
[EpsilonExport_LOG_ID] [int] NULL,
[ConfirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataSourceID] [int] NOT NULL,
[TransactionID] [int] NOT NULL,
[SourceKey] [int] NULL,
[transactionTimeStamp] [datetime] NULL,
[actionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateSent] [datetime] NOT NULL,
[HotelCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArrivalDate] [date] NOT NULL,
[DepartureDate] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_EpsilonPointTransactionLog] on [dbo].[EpsilonPointTransactionLog]'
GO
ALTER TABLE [dbo].[EpsilonPointTransactionLog] ADD CONSTRAINT [PK_dbo_EpsilonPointTransactionLog] PRIMARY KEY CLUSTERED  ([EpsilonPointTransactionLogID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EpsilonReservationLog]'
GO
CREATE TABLE [dbo].[EpsilonReservationLog]
(
[EpsilonReservationLogID] [int] NOT NULL IDENTITY(1, 1),
[EpsilonExport_LOG_ID] [int] NULL,
[ConfirmationNumber] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DataSourceID] [int] NOT NULL,
[TransactionID] [int] NOT NULL,
[SourceKey] [int] NULL,
[transactionTimeStamp] [datetime] NULL,
[actionType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateSent] [datetime] NOT NULL,
[HotelCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ArrivalDate] [date] NOT NULL,
[DepartureDate] [date] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_dbo_EpsilonReservationLog] on [dbo].[EpsilonReservationLog]'
GO
ALTER TABLE [dbo].[EpsilonReservationLog] ADD CONSTRAINT [PK_dbo_EpsilonReservationLog] PRIMARY KEY CLUSTERED  ([EpsilonReservationLogID])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonMember]'
GO
CREATE TABLE [dbo].[Import_EpsilonMember]
(
[QueueID] [int] NOT NULL,
[PROFILE_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARD_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FIRST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MIDDLE_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LAST_NAME] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EMAIL_ADDR] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ADDRESS_LINE_2] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COUNTRY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[POSTAL_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIER_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CLIENT_JOIN_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SOURCE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PROMO_CODE] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ENROLLMENT_STORE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GLOBAL_OPT_OUT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[json_external_data] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BIRTH_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GENDER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MEMBERTYPE] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IS_EMAIL_VALIDATED] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IATA_ID] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRAVEL_AGENCY_NAME] [nvarchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonPoint]'
GO
CREATE TABLE [dbo].[Import_EpsilonPoint]
(
[QueueID] [int] NOT NULL,
[ACT_POINT_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_TRANSACTION_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_ADJUSTMENT_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_ACTIVITY_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_ORDER_REWARD_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARD_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DISPLAY_POINT_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SHORT_DESC] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_USER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TXN_SOURCE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_USER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_NET_AMOUNT] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANSACTION_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_EXCHANGE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_EXCHANGE_RATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUM_POINTS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USD_PTS_LIABILITY] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANS_DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PointCategory] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonRedemption]'
GO
CREATE TABLE [dbo].[Import_EpsilonRedemption]
(
[QueueID] [int] NOT NULL,
[ACT_REWARD_CERT_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CERT_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARD_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_USER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REWARD_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[STORE_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TITLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RETAIL_COST] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Import_EpsilonReward]'
GO
CREATE TABLE [dbo].[Import_EpsilonReward]
(
[QueueID] [int] NOT NULL,
[ACT_ORDER_REWARD_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_ORDER_DETAIL_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ACT_REWARD_CERT_ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CERT_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ORDER_DETAIL_STATUS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CARD_NUMBER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UPDATE_USER] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[REWARD_TYPE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MANUFACTURER] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TITLE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RETAIL_COST] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[USD_REWARD_VALUE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_CODE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_EXCHANGE_DATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CURRENCY_EXCHANGE_RATE] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NUM_POINTS] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TRANS_DESCRIPTION] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
