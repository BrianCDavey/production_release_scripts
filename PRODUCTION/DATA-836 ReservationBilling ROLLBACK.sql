USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/20/2020 8:16:37 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[sourceCodes_SupersetMigration]'
GO
ALTER TABLE [dbo].[sourceCodes_SupersetMigration] DROP CONSTRAINT [PK_SourceCodes_SupersetMigration]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [work].[Move_Charges_BillableDate]'
GO
DROP PROCEDURE [work].[Move_Charges_BillableDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[sourceCodes_SupersetMigration]'
GO
DROP TABLE [dbo].[sourceCodes_SupersetMigration]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[SourceCodes]'
GO



ALTER VIEW [dbo].[SourceCodes]
AS
SELECT ISNULL(CAST((row_number() OVER (ORDER BY channel )) AS int), 0) 
AS EDMXID, channel AS primaryChannelDescription, channel AS primaryChannelCode, secondarySource AS secondaryChannelDescription, secondarySource AS secondaryChannelCode, subSource AS subChannelDescription , 
                         subSourceCode  AS subChannelCode
FROM Reservations..vw_CRS_BookingSource AS sourcecodes_1
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
