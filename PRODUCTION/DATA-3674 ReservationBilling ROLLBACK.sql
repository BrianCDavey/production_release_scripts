/*
Run this script on:

        wus2-data-dp-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 10/17/2024 4:20:01 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [loyalty].[Calculate_BookWithPointFlatRate_CreditMemo]'
GO

ALTER PROCEDURE [loyalty].[Calculate_BookWithPointFlatRate_CreditMemo]
AS
BEGIN
	DECLARE	@startDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --first day of previous month
			,@endDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), -1) --last day of previous month


	/*	It is possible for the same Voucher Number to appear in multiple Epsilon Redemption files. 
		We want to end up with only one record per voucher number. 
		Add handling to achieve the following:

			1. If the voucher number does not previously exist in our data, add it
			2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
			3. If the voucher number DOES exist in our data and DOES have a SOP Number, check all fields of the new record against the existing.
				3a. If the new record matches the old record, ignore the new record
				3b. If the new record has a change in any field FROM the old record, it needs to be reported as an error to the Finance team somehow
	*/

	
	-- 1. If the voucher number does not previously exist in our data, add it
	IF OBJECT_ID('tempdb..#insert_new') IS NOT NULL
		DROP TABLE #insert_new;
	CREATE TABLE #insert_new(confirmationNumber nvarchar(20) NOT NULL PRIMARY KEY CLUSTERED(confirmationNumber))

	INSERT INTO #insert_new(confirmationNumber)
	SELECT i.confirmationNumber
	FROM Reservations_Reporting.[billing].[MostRecentTransactions] i
		INNER JOIN Hotels.dbo.hotel ON i.HotelCode = hotel.HotelCode
		INNER JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = hotel.HotelID AND bw.RateTypeCode = i.rateTypeCode 
														AND i.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
		LEFT OUTER JOIN loyalty.Credit_Memos cm ON i.confirmationNumber = cm.Voucher_Number
	WHERE i.arrivalDate >= @startDate
		and i.arrivalDate <= @enddate
		and cm.Voucher_Number IS NULL
		and i.status <> 'Cancelled'
	-------------------------------------------------------------------------

	-- 2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
	IF OBJECT_ID('tempdb..#update_exists') IS NOT NULL
		DROP TABLE #update_exists;
	CREATE TABLE #update_exists(confirmationNumber nvarchar(20) NOT NULL PRIMARY KEY CLUSTERED(confirmationNumber))

	INSERT INTO #update_exists(confirmationNumber)
	SELECT i.confirmationNumber
	FROM Reservations_Reporting.[billing].[MostRecentTransactions] i
		INNER JOIN Hotels.dbo.hotel ON i.HotelCode = hotel.HotelCode
		INNER JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = hotel.HotelID AND bw.RateTypeCode = i.rateTypeCode
														AND i.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
		LEFT OUTER JOIN loyalty.Credit_Memos cm ON i.confirmationNumber = cm.Voucher_Number
	WHERE i.arrivalDate >= @startDate
		and i.arrivalDate <= @enddate
		and cm.SOPNumber IS NULL
		and i.status <> 'Cancelled'
	-------------------------------------------------------------------------

	-- 3. If the voucher number DOES exist in our data and DOES have a SOP Number
	--    check all fields of the new record against the existing.
	-- 3a. If the new record matches the old record, ignore the new record
	IF OBJECT_ID('tempdb..#ignore') IS NOT NULL
		DROP TABLE #ignore;
	CREATE TABLE #ignore(confirmationNumber nvarchar(20) NOT NULL PRIMARY KEY CLUSTERED(confirmationNumber))

	INSERT INTO #ignore(confirmationNumber)
	SELECT Imported.confirmationNumber
	FROM loyalty.Credit_Memos CM
		INNER JOIN Reservations_Reporting.[billing].[MostRecentTransactions] Imported ON CM.Voucher_Number = Imported.confirmationNumber
		INNER JOIN Hotels.dbo.hotel ON Imported.HotelCode = hotel.HotelCode
		INNER JOIN loyalty.BookWithPointCreditRules BW ON BW.FlatFee*Imported.nights = CM.Voucher_Value AND BW.hotelID = hotel.HotelID AND BW.RateTypeCode = Imported.rateTypeCode
														AND Imported.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
	WHERE Imported.arrivalDate >= @startDate
		AND Imported.arrivalDate <= @enddate
		AND CM.Redemption_Date = Imported.arrivalDate
		AND CM.Hotel_Code = hotel.hotelcode
		AND CM.Membership_Number = Imported.loyaltyNumber
		AND CM.Voucher_Value = BW.FlatFee*Imported.nights 
		AND CM.Voucher_Currency = BW.FlatFee_Currency 
		AND CM.SOPNumber is not null
		AND Imported.status <> 'Cancelled'
	-------------------------------------------------------------------------

	-- 3. If the voucher number DOES exist in our data and DOES have a SOP Number
	--    check all fields of the new record against the existing.
	-- 3b. If the new record has a change in any field FROM the old record
	--    it needs to be reported as an error to the Finance team somehow
	IF OBJECT_ID('tempdb..#error') IS NOT NULL
		DROP TABLE #error;
	CREATE TABLE #error(confirmationNumber nvarchar(20) NOT NULL,ErrorMessage nvarchar(100) PRIMARY KEY CLUSTERED(confirmationNumber))

	INSERT INTO #error(confirmationNumber,ErrorMessage)
	SELECT Imported.confirmationNumber,
		'Certificate_Number already exists with an SOPNumer, but with different data.' as ErrorMessage
	FROM loyalty.Credit_Memos CM
		INNER JOIN Reservations_Reporting.[billing].[MostRecentTransactions] Imported ON CM.Voucher_Number = Imported.confirmationNumber
		INNER JOIN Hotels.dbo.hotel ON Imported.HotelCode = hotel.HotelCode
		INNER JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = hotel.HotelID AND BW.RateTypeCode = Imported.rateTypeCode
														AND Imported.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
	WHERE Imported.arrivalDate >= @startDate
		AND Imported.arrivalDate <= @enddate
		AND CM.SOPNumber is not null
		AND
		(
			CM.Redemption_Date <> Imported.arrivalDate
			OR
			CM.Hotel_Code <> hotel.hotelcode
			OR
			CM.Membership_Number <> Imported.loyaltyNumber
			OR
			CM.Voucher_Value <> BW.FlatFee*Imported.nights 
			OR
			CM.Voucher_Currency <> BW.FlatFee_Currency 
		)
		AND Imported.status <> 'Cancelled'
	-------------------------------------------------------------------------

	-- 4. If the voucher number DOES exist in our data and Does not have [BookWithPointCreditRules] setup.
	IF OBJECT_ID('tempdb..#error_BookWithPointCreditRules') IS NOT NULL
		DROP TABLE #error_BookWithPointCreditRules;
	CREATE TABLE #error_BookWithPointCreditRules(confirmationNumber nvarchar(20) NOT NULL,ErrorMessage nvarchar(100) PRIMARY KEY CLUSTERED(confirmationNumber))

	INSERT INTO #error_BookWithPointCreditRules(confirmationNumber,ErrorMessage)
	SELECT Imported.confirmationNumber,
		'Certificate_Number already exists but BookWithPoint CreditRules was not set up.' as ErrorMessage
	FROM Reservations_Reporting.[billing].[MostRecentTransactions] Imported
		LEFT JOIN Hotels.dbo.hotel ON Imported.HotelCode = hotel.HotelCode
		LEFT JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = hotel.HotelID AND bw.RateTypeCode = Imported.rateTypeCode
													AND Imported.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
	WHERE Imported.arrivalDate >= @startDate
		AND Imported.arrivalDate <= @enddate
		AND Imported.rateTypeCode IN (SELECT DISTINCT [RateTypeCode] FROM [loyalty].[BookWithPointCreditRules])
		AND BW.BookWithPointCreditRulesID IS NULL
		AND Imported.status <> 'Cancelled'
	-------------------------------------------------------------------------

	-- INSERT/UPDATE loyalty.Credit_Memos ------------------------------------
	INSERT INTO loyalty.Credit_Memos(Redemption_Date, Hotel_Code, Membership_Number, Voucher_Number,
									Voucher_Value, Voucher_Currency, Payable_Value,Create_Date,SOPNumber,
									Invoice_Date, Currency_Conversion_Date, IsBookWithPoint,rateTypeCode,Credit_Type)
	SELECT	CAST(BSI.arrivalDate AS DATE) AS Redemption_Date, 
			BSI.HotelCode AS Hotel_Code, 
			BSI.loyaltyNumber AS Membership_Number, 
			BSI.confirmationNumber AS Voucher_Number, 
			BW.FlatFee*BSI.nights AS Voucher_Value, 
			BW.FlatFee_Currency AS Voucher_Currency, 
			CAST(Reservations.dbo.convertCurrencyToUSD(BW.FlatFee*BSI.nights, BW.FlatFee_Currency, BSI.arrivalDate) AS numeric(8,2)) AS Payable_Value,
			GETDATE() as Create_Date, 
			null as SOPNumber, 
			null as Invoice_Date, 
			CAST(BSI.arrivalDate AS DATE),
			1,
			BSI.rateTypeCode,
			1
	FROM	Reservations_Reporting.[billing].[MostRecentTransactions] BSI
		INNER JOIN #insert_new i ON	BSI.confirmationNumber = i.confirmationNumber
		INNER JOIN Hotels.dbo.Hotel h ON h.HotelCode = BSI.HotelCode
		INNER JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = h.HotelID AND BW.RateTypeCode = BSI.rateTypeCode
														AND BSI.arrivalDate BETWEEN BW.StartDate AND BW.EndDate

	UPDATE CM
		set	CM.Redemption_Date = CAST(Imported.arrivalDate AS DATE), 
			CM.Hotel_Code = Imported.HotelCode , 
			CM.Membership_Number = Imported.loyaltyNumber , 
			CM.Voucher_Value = BW.FlatFee*Imported.nights , 
			CM.Voucher_Currency = BW.FlatFee_Currency ,
			CM.Payable_Value = CAST(Reservations.dbo.convertCurrencyToUSD(BW.FlatFee*Imported.nights, BW.FlatFee_Currency, Imported.arrivalDate) AS numeric(8,2)), 
			CM.Currency_Conversion_Date = CAST(Imported.arrivalDate AS DATE), 
			CM.Create_Date = GetDate(),
			CM.rateTypeCode = Imported.rateTypeCode
		FROM Loyalty.Credit_Memos CM
			INNER JOIN Reservations_Reporting.[billing].[MostRecentTransactions] Imported ON CM.Voucher_Number = Imported.confirmationNumber
			INNER JOIN #update_exists u ON CM.Voucher_Number = u.confirmationNumber
			INNER JOIN	Hotels.dbo.Hotel h ON h.HotelCode = Imported.HotelCode
			INNER JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = h.HotelID AND BW.RateTypeCode = Imported.rateTypeCode
															AND Imported.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
		-------------------------------------------------------------------------

	-- empty table for the current run
	--truncate table loyalty.[Credit_Memo_Errors]

	-- INSERT INTO Loyalty.Credit_Memo_Errors ------------------------------------
	INSERT INTO Loyalty.[Credit_Memo_Errors]([Redemption_Date],[Hotel_Code],[Hotel_Name],[Membership_Number],
											[Member_Name],[Voucher_Type],[Voucher_Number],[Voucher_Name],
											[Voucher_Value],[Voucher_Currency],[Payable_Value_USD],[filename],
											[importdate],[Error_Message],IsBookWithPoint,[Credit_Type])
	SELECT CAST(imported.arrivalDate AS DATE)
		, imported.HotelCode
		, imported.HotelName
		, imported.loyaltyNumber
		, G.FirstName + ' ' + g.LastName 
		, imported.rateTypeCode
		, imported.confirmationNumber
		, imported.rateTypeCode
		, BW.FlatFee*Imported.nights
		, BW.FlatFee_Currency 
		, CAST(Reservations.dbo.convertCurrencyToUSD(BW.FlatFee*Imported.nights, BW.FlatFee_Currency, imported.arrivalDate) AS numeric(8,2))
		, NULL, NULL, e.ErrorMessage
		, 1
		,1
	FROM #error e
		INNER JOIN Reservations_Reporting.[billing].[MostRecentTransactions] imported ON e.confirmationNumber = imported.confirmationNumber
		INNER JOIN	Hotels.dbo.Hotel h ON h.HotelCode = imported.HotelCode
		LEFT JOIN loyalty.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberName = Imported.loyaltyNumber
		LEFT JOIN Guests.dbo.Guest g ON g.LoyaltyNumberID = ln.loyaltyNumberID    
		LEFT JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = h.HotelID  AND BW.RateTypeCode = Imported.rateTypeCode
														AND Imported.arrivalDate BETWEEN BW.StartDate AND BW.EndDate

	UNION ALL

	SELECT CAST(imported.arrivalDate AS DATE)
		, imported.HotelCode
		, imported.HotelName
		, imported.loyaltyNumber
		, G.FirstName + ' ' + g.LastName 
		, imported.rateTypeCode
		, imported.confirmationNumber
		, imported.rateTypeCode
		, BW.FlatFee*Imported.nights
		, BW.FlatFee_Currency 
		, CAST(Reservations.dbo.convertCurrencyToUSD(BW.FlatFee*Imported.nights, BW.FlatFee_Currency, imported.arrivalDate) AS numeric(8,2))
		, NULL, NULL, e.ErrorMessage
		, 1
		,1
	FROM #error_BookWithPointCreditRules e
		INNER JOIN Reservations_Reporting.[billing].[MostRecentTransactions] imported ON e.confirmationNumber = imported.confirmationNumber
		INNER JOIN	Hotels.dbo.Hotel h ON h.HotelCode = imported.HotelCode
		LEFT JOIN loyalty.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberName = Imported.loyaltyNumber
		LEFT JOIN Guests.dbo.Guest g ON g.LoyaltyNumberID = ln.loyaltyNumberID    
		LEFT JOIN loyalty.BookWithPointCreditRules BW ON BW.hotelID = h.HotelID  AND BW.RateTypeCode = Imported.rateTypeCode
														AND Imported.arrivalDate BETWEEN BW.StartDate AND BW.EndDate
	-------------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
