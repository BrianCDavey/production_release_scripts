USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 2/13/2020 9:59:19 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [loyalty].[Calculate_Credit_Memos]'
GO


ALTER procedure [loyalty].[Calculate_Credit_Memos]
AS
BEGIN


declare	@startDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) --first day of previous month
		,@endDate DATE = DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) --last day of previous month


/*	It is possible for the same Voucher Number to appear in multiple Epsilon Redemption files. 
	We want to end up with only one record per voucher number. 
	Add handling to achieve the following:

		1. If the voucher number does not previously exist in our data, add it
		2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
		3. If the voucher number DOES exist in our data and DOES have a SOP Number, check all fields of the new record against the existing.
			3a. If the new record matches the old record, ignore the new record
			3b. If the new record has a change in any field from the old record, it needs to be reported as an error to the Finance team somehow
*/


--		1. If the voucher number does not previously exist in our data, add it
select	i.MemberRewardNumber, MAX(i.RedemptionActivityID) mostrecent_id
into	#insert_new
from	Loyalty.dbo.RedemptionActivity i
	left outer join loyalty.Credit_Memos cm
		on i.MemberRewardNumber = cm.Voucher_Number
where	i.LastUpdated >= @startDate
	and i.LastUpdated <= @enddate
	and cm.Voucher_Number IS NULL
group by i.MemberRewardNumber


--		2. If the voucher number DOES exist in our data, but does not have a SOP Number, update it
select	i.MemberRewardNumber, MAX(i.RedemptionActivityID) mostrecent_id
into	#update_exists -- select * 
from	Loyalty.dbo.RedemptionActivity i
	left outer join loyalty.Credit_Memos cm
		on i.MemberRewardNumber = cm.Voucher_Number
where	i.LastUpdated >= @startDate
	and i.LastUpdated <= @enddate
	and cm.SOPNumber IS NULL
group by i.MemberRewardNumber


--		3. If the voucher number DOES exist in our data and DOES have a SOP Number
--			check all fields of the new record against the existing.
--		3a. If the new record matches the old record, ignore the new record
select	Imported.MemberRewardNumber
into	#ignore
from	loyalty.Credit_Memos CM
join	Loyalty.dbo.RedemptionActivity Imported
 on		CM.Voucher_Number = Imported.MemberRewardNumber
LEFT JOIN Hotels.dbo.hotel ON Imported.RedeemedHotelID = hotel.HotelID
LEFT JOIN Loyalty.dbo.loyaltyNumber ln ON ln.loyaltynumberID = imported.loyaltyNumberID
where	Imported.LastUpdated >= @startDate
and		Imported.LastUpdated <= @enddate
and		CM.Redemption_Date = Imported.LastUpdated
and		CM.Hotel_Code = hotel.hotelcode
and		CM.Membership_Number = ln.LoyaltyNumberName
and		CM.Voucher_Value = Imported.RewardCost 
and		CM.Voucher_Currency = Imported.RewardCurrency 
and		CM.SOPNumber is not null

--		3. If the voucher number DOES exist in our data and DOES have a SOP Number
--			check all fields of the new record against the existing.
--		3b. If the new record has a change in any field from the old record
--			it needs to be reported as an error to the Finance team somehow
select	Imported.MemberRewardNumber
, 'Voucher_Number already exists with an SOPNumer, but with different data.' as ErrorMessage
into	#error
from	Loyalty.Credit_Memos CM
join	Loyalty.dbo.RedemptionActivity Imported
 on		CM.Voucher_Number = Imported.MemberRewardNumber
LEFT JOIN Hotels.dbo.hotel ON Imported.RedeemedHotelID = hotel.HotelID
LEFT JOIN Loyalty.dbo.loyaltyNumber ln ON ln.loyaltynumberID = imported.loyaltyNumberID
where	Imported.LastUpdated >= @startDate
and		Imported.LastUpdated <= @enddate
and		CM.SOPNumber is not null
and (	CM.Redemption_Date <> Imported.LastUpdated
or		CM.Hotel_Code <> hotel.hotelcode
or		CM.Membership_Number <> ln.LoyaltyNumberName
or		CM.Voucher_Value <> Imported.RewardCost 
or		CM.Voucher_Currency <> Imported.RewardCurrency 

	)


insert	into	loyalty.Credit_Memos
	(	Redemption_Date, Hotel_Code, Membership_Number, Voucher_Number, 
		Voucher_Value, Voucher_Currency, Payable_Value, 
		Create_Date, SOPNumber, Invoice_Date, Currency_Conversion_Date
	)
select	CAST(LastUpdated AS DATE), hotel.HotelCode, ln.LoyaltyNumberName , i.MemberRewardNumber, 
		bsi.RewardCost, bsi.RewardCurrency, CAST(ISNULL(hru.ReimbursementPercentage, 0.850) * Reservations.dbo.convertCurrencyToUSD(bsi.RewardCost, bsi.RewardCurrency, bsi.LastUpdated) AS numeric(8,2)), 
		GETDATE() as Create_Date, null as SOPNumber, null as Invoice_Date, CAST(LastUpdated AS DATE)
from	Loyalty.dbo.RedemptionActivity BSI
LEFT JOIN Hotels.dbo.hotel ON bsi.RedeemedHotelID = hotel.HotelID
LEFT JOIN loyalty.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = BSI.LoyaltyNumberID
join	#insert_new i
	on	BSI.MemberRewardNumber = i.MemberRewardNumber
	and	BSI.RedemptionActivityID = i.mostrecent_id
INNER JOIN CurrencyRates.dbo.dailyRates xe
	ON CAST(CreateDate AS DATE) = xe.rateDate
	AND RewardCurrency = xe.code
LEFT JOIN ReservationBilling.loyalty.HotelRedemptionRules hru
	ON BSI.RedeemedHotelID = hru.HotelID


update	CM
set		CM.Redemption_Date = CAST(Imported.LastUpdated AS DATE), 
		CM.Hotel_Code = hotel.HotelCode , 
		CM.Membership_Number = ln.LoyaltyNumberName , 
		CM.Voucher_Value = Imported.RewardCost , 
		CM.Voucher_Currency = Imported.RewardCurrency , 
		CM.Payable_Value = CAST(ISNULL(hru.ReimbursementPercentage, 0.850) * Reservations.dbo.convertCurrencyToUSD(Imported.RewardCost, Imported.RewardCurrency, Imported.LastUpdated) AS numeric(8,2)) , 
		CM.Currency_Conversion_Date = CAST(Imported.LastUpdated AS DATE), 
		CM.Create_Date = GetDate()
from	Loyalty.Credit_Memos CM
join	Loyalty.dbo.RedemptionActivity Imported
	on	CM.Voucher_Number = Imported.MemberRewardNumber
join	#update_exists u
	on	CM.Voucher_Number = u.Voucher_Number
	and	Imported.id = u.mostrecent_id
LEFT JOIN Hotels.dbo.hotel ON Imported.RedeemedHotelID = hotel.HotelID
LEFT JOIN loyalty.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = Imported.LoyaltyNumberID
LEFT JOIN CurrencyRates.dbo.dailyRates xe
	ON CAST(CreateDate AS DATE) = xe.rateDate
	AND RewardCurrency = xe.code
LEFT JOIN ReservationBilling.loyalty.HotelRedemptionRules hru
	ON Imported.RedeemedHotelID = hru.HotelID


-- empty table for the current run
truncate table loyalty.[Credit_Memo_Errors]

INSERT INTO Loyalty.[Credit_Memo_Errors]
	(	[Redemption_Date],[Hotel_Code],[Hotel_Name],[Membership_Number],[Member_Name],[Voucher_Type]
		,[Voucher_Number],[Voucher_Name],[Voucher_Value],[Voucher_Currency],[Payable_Value_USD]
		,[filename],[importdate],[Error_Message]
	)
select	CAST(LastUpdated AS DATE), hotel.HotelCode, Hotel.HotelName, ln.LoyaltyNumberName, G.FirstName + ' ' + g.LastName , rt.rewardtypename, i.MemberRewardNumber, r.rewardName,
		imported.RewardCost, imported.RewardCurrency, CAST(ISNULL(hru.ReimbursementPercentage, 0.850) * Reservations.dbo.convertCurrencyToUSD(imported.RewardCost, imported.RewardCurrency, imported.LastUpdated) AS numeric(8,2)), 
		q.filename, q.createdate, e.ErrorMessage
from 	#error e
join	Loyalty.dbo.RedemptionActivity imported
	on	e.Voucher_Number = imported.MemberRewardNumber
LEFT JOIN Hotels.dbo.hotel ON Imported.RedeemedHotelID = hotel.HotelID
LEFT JOIN loyalty.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = Imported.LoyaltyNumberID
LEFT JOIN CurrencyRates.dbo.dailyRates xe
	ON CAST(CreateDate AS DATE) = xe.rateDate
	AND RewardCurrency = xe.code
LEFT JOIN ReservationBilling.loyalty.HotelRedemptionRules hru
	ON Imported.RedeemedHotelID = hru.HotelID
LEFT JOIN Guests.dbo.Guest g ON g.LoyaltyNumberID = imported.loyaltyNumberID    
LEFT JOIN Loyalty.dbo.rewardtype rt ON imported.rewardtypeID = rt.rewardtypeid
LEFT JOIN loyalty.dbo.reward r ON r.rewardid = imported.rewardid
LEFT JOIN ETL.dbo.Queue q ON q.queueid = imported.QueueID

drop table #insert_new
drop table #update_exists
drop table #error
drop table #ignore

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
