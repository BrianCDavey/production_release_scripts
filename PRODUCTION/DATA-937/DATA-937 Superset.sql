USE Superset
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Superset    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Superset

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 4/27/2020 10:11:44 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[mostrecenttransactions]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[mostrecenttransactions] ADD
[Location_LocationHashKeyOriginalCase] AS (hashbytes('MD5',((((TRIM(isnull([customerAddress1],''))+TRIM(isnull([customerAddress2],'')))+TRIM(isnull([customerCity],'')))+TRIM(isnull([customerState],'')))+TRIM(isnull([customerCountry],'')))+TRIM(isnull([customerPostalCode],'')))) PERSISTED,
[Location_LocationHashKeyCaseInsensitive] AS (hashbytes('MD5',((((upper(TRIM(isnull([customerAddress1],'')))+upper(TRIM(isnull([customerAddress2],''))))+upper(TRIM(isnull([customerCity],''))))+upper(TRIM(isnull([customerState],''))))+upper(TRIM(isnull([customerCountry],''))))+upper(TRIM(isnull([customerPostalCode],''))))) PERSISTED
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping users'
GO
DROP USER [INDECORP\RSerei]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DROP USER [INDECORP\reportgenerator]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
