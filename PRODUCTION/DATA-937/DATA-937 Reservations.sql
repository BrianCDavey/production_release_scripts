USE Reservations
GO

/*
Run this script on:

        CHI-SQ-PR-01\WAREHOUSE.Reservations    -  This database will be modified

to synchronize it with:

        CHI-SQ-DP-01\WAREHOUSE.Reservations

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.2.9.15508 from Red Gate Software Ltd at 4/27/2020 9:58:12 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Location]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Location] ALTER COLUMN [Locations_LocationID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[City]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[City] ALTER COLUMN [Locations_CityID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Country]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[Country] ALTER COLUMN [Locations_CountryID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PostalCode]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PostalCode] ALTER COLUMN [Locations_PostalCodeID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[State]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[State] ALTER COLUMN [Locations_StateID] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_Guest_FromLocation]'
GO




CREATE PROCEDURE [dbo].[Populate_Guest_FromLocation]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
		
	-- #SYNXIS_Location ------------------------
	IF OBJECT_ID('tempdb..#Locations_Location') IS NOT NULL
		DROP TABLE #Locations_Location;
	CREATE TABLE #Locations_Location([LocationID] int,Locations_LocationID int NOT NULL, SynxisID varchar(max) NULL, OpenHospID varchar(max))
	
	INSERT INTO #Locations_Location([LocationID],Locations_LocationID, SynxisID, OpenHospID)
	SELECT [LocationID], value AS Locations_LocationID, SynxisID, OpenHospID FROM dbo.[Location] CROSS APPLY string_split(Locations_LocationID,',')
	------------------------------------------------------ 
		
	MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
			SELECT a.[Address1],a.[Address2] + ' ' + a.address3 AS [Address2],l.[LocationID], a.AddressID, g.guestID, lhkm.LocationHashKey
			FROM synxis.Guest g
			INNER JOIN Locations.dbo.LocationHashKeyMapping lhkm ON g.Location_LocationHashKey = lhkm.LocationHashKey
			INNER JOIN Locations.dbo.Address a ON lhkm.AddressID = a.AddressID
			LEFT JOIN #Locations_Location l ON l.Locations_LocationID = a.[LocationID] AND (SynxisID IS NOT NULL OR (OpenHospID IS NULL AND SynxisID IS NULL) )


	) AS src ON src.guestID = tgt.synxisID
	WHEN MATCHED THEN
		UPDATE
			SET [Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Location_AddressID] = src.[AddressID],
				[Location_LocationHashKey] = src.LocationHashKey
	--WHEN NOT MATCHED BY SOURCE THEN
	-- DELETE
	;

		MERGE INTO [dbo].[Guest] AS tgt
	USING
	(
			SELECT a.[Address1],a.[Address2] + ' ' + a.address3 AS [Address2],l.[LocationID], a.AddressID, g.guestID,lhkm.LocationHashKey
			FROM OpenHosp.Guest g
			INNER JOIN Locations.dbo.LocationHashKeyMapping lhkm ON g.Location_LocationHashKey = lhkm.LocationHashKey
			INNER JOIN Locations.dbo.Address a ON lhkm.AddressID = a.AddressID
			LEFT JOIN #Locations_Location l ON l.Locations_LocationID = a.[LocationID] AND (OpenHospID IS NOT NULL OR (OpenHospID IS NULL AND SynxisID IS NULL) )

	) AS src ON src.guestID = tgt.openhospid
	WHEN MATCHED THEN
		UPDATE
			SET [Address1] = src.[Address1],
				[Address2] = src.[Address2],
				[LocationID] = src.[LocationID],
				[Location_AddressID] = src.[AddressID],
				[Location_LocationHashKey] = src.LocationHashKey
	--WHEN NOT MATCHED BY SOURCE THEN
	-- DELETE
	;

						
			

END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_Location_FromLocations]'
GO




CREATE PROCEDURE [dbo].[Populate_Location_FromLocations]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Location] AS tgt
	USING
	(

		SELECT STRING_AGG(Locations_LocationID,',') AS Locations_LocationID, [CityID],[StateID],[CountryID],[PostalCodeID]
		FROM
		(
			SELECT LocationID AS Locations_LocationID,c.[CityID],s.[StateID],cn.[CountryID],p.[PostalCodeID]
			FROM Locations.dbo.[Location] l
				LEFT JOIN (SELECT CityID, value AS Locations_CityID FROM dbo.City CROSS APPLY string_split(Locations_CityID,',')) c ON c.Locations_CityID = l.CityID
				LEFT JOIN (SELECT StateID, value AS Locations_StateID FROM dbo.[State] CROSS APPLY string_split(Locations_StateID,',')) s ON s.Locations_StateID = l.StateID
				LEFT JOIN (SELECT CountryID, value AS Locations_CountryID FROM dbo.Country CROSS APPLY string_split(Locations_CountryID,',')) cn ON cn.Locations_CountryID = l.CountryID
				LEFT JOIN (SELECT PostalCodeID, value AS Locations_PostalCodeID FROM dbo.PostalCode CROSS APPLY string_split(Locations_PostalCodeID,',')) p ON p.Locations_PostalCodeID = l.PostalCodeID
		) X
		GROUP BY [CityID],[StateID],[CountryID],[PostalCodeID]
	) AS src ON ISNULL(src.[CityID],'') = ISNULL(tgt.[CityID],'')
				AND ISNULL(src.[StateID],'') = ISNULL(tgt.[StateID],'')
				AND ISNULL(src.[CountryID],'') = ISNULL(tgt.[CountryID],'')
				AND ISNULL(src.[PostalCodeID],'') = ISNULL(tgt.[PostalCodeID],'')
	WHEN MATCHED THEN
		UPDATE
			SET Locations_LocationID = src.Locations_LocationID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT(Locations_LocationID,[CityID],[StateID],[CountryID],[PostalCodeID])
		VALUES(Locations_LocationID,[CityID],[StateID],[CountryID],[PostalCodeID])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_PostalCode_FromLocations]'
GO






CREATE PROCEDURE [dbo].[Populate_PostalCode_FromLocations]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[PostalCode] AS tgt
	USING
	(
		SELECT STRING_AGG(Locations_PostalCodeID,',') AS Locations_PostalCodeID, [PostalCode_Text]
		FROM
		(
			SELECT [PostalCodeID] AS Locations_PostalCodeID,ISNULL(NULLIF([PostalCode_Text],''),'unknown') AS [PostalCode_Text]
			FROM Locations.dbo.PostalCode
		) x
		GROUP BY [PostalCode_Text]
	) AS src ON src.[PostalCode_Text] = tgt.[PostalCode_Text]
	WHEN MATCHED THEN
		UPDATE
			SET Locations_PostalCodeID = src.Locations_PostalCodeID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Locations_PostalCodeID],[PostalCode_Text])
		VALUES([Locations_PostalCodeID],[PostalCode_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_Country_FromLocations]'
GO




CREATE PROCEDURE [dbo].[Populate_Country_FromLocations]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[Country] AS tgt
	USING
	(
		SELECT STRING_AGG(Locations_CountryID,',') AS Locations_CountryID, [Country_Text]
		FROM
		(
			SELECT [CountryID] AS Locations_CountryID,ISNULL(NULLIF([Country_Text],''),'unknown') AS [Country_Text]
			FROM Locations.dbo.Country
		) x
		GROUP BY [Country_Text]
	) AS src ON src.[Country_Text] = tgt.[Country_Text]
	WHEN MATCHED THEN
		UPDATE
			SET Locations_CountryID = src.Locations_CountryID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Locations_CountryID],[Country_Text])
		VALUES([Locations_CountryID],[Country_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_State_FromLocations]'
GO



CREATE PROCEDURE [dbo].[Populate_State_FromLocations]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[State] AS tgt
	USING
	(
		SELECT STRING_AGG(Locations_StateID,',') AS Locations_StateID, [State_Text]
		FROM
		(
			SELECT [StateID] AS Locations_StateID,ISNULL(NULLIF([State_Text],''),'unknown') AS [State_Text]
			FROM Locations.dbo.State
		) x
		GROUP BY [State_Text]
	) AS src ON src.[State_Text] = tgt.[State_Text]
	WHEN MATCHED THEN
		UPDATE
			SET Locations_StateID = src.Locations_StateID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Locations_StateID],[State_Text])
		VALUES([Locations_StateID],[State_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Populate_City_FromLocations]'
GO



CREATE PROCEDURE [dbo].[Populate_City_FromLocations]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	MERGE INTO [dbo].[City] AS tgt
	USING
	(
		SELECT STRING_AGG(Locations_CityID,',') AS Locations_CityID, [City_Text]
		FROM
		(
			SELECT [CityID] AS Locations_CityID,ISNULL(NULLIF([City_Text],''),'unknown') AS [City_Text]
			FROM Locations.dbo.City
		) x
		GROUP BY [City_Text]
	) AS src ON src.[City_Text] = tgt.[City_Text]
	WHEN MATCHED THEN
		UPDATE
			SET Locations_CityID = src.Locations_CityID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Locations_CityID],[City_Text])
		VALUES([Locations_CityID],[City_Text])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[LoadGuestLocationHashKeyOneTime]'
GO




CREATE PROCEDURE [dbo].[LoadGuestLocationHashKeyOneTime]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @print varchar(2000);

	----populate openhosp--------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': #LOCATION'
	RAISERROR(@print,10,1) WITH NOWAIT
	
	IF OBJECT_ID('tempdb..#LOCATION') IS NOT NULL
	DROP TABLE #LOCATION;

	CREATE TABLE #LOCATION
	(
		LocationID int NOT NULL,
		City_Text nvarchar(255) NOT NULL,
		State_Text nvarchar(255) NOT NULL,
		Country_Text nvarchar(255) NOT NULL,
		PostalCode_Text nvarchar(255) NOT NULL,
	)
	INSERT INTO #LOCATION(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text)
	SELECT DISTINCT l.LocationID,ISNULL(ci.City_Text,'NULL'),ISNULL(st.State_Text,'NULL'),ISNULL(co.Country_Text,'NULL'),ISNULL(p.PostalCode_Text,'NULL')
	FROM  [openHosp].[Location] l
		LEFT JOIN [openHosp].[City] ci ON ci.CityID = l.CityID
		LEFT JOIN [openHosp].[State] st ON st.StateID = l.StateID
		LEFT JOIN [openHosp].[Country] co ON co.CountryID = l.CountryID
		LEFT JOIN [openHosp].[PostalCode] p ON p.PostalCodeID = l.PostalCodeID

		------------------------------------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Update [openHosp].[Guest]'
	RAISERROR(@print,10,1) WITH NOWAIT
	
	UPDATE g
	SET Location_LocationHashKey = HASHBYTES('MD5', UPPER(TRIM(ISNULL(g.Address1,''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.City_Text,'NULL',''),''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.State_Text,'NULL',''),''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.Country_Text,'NULL',''),''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.PostalCode_Text,'NULL',''),''))))
	FROM [openHosp].[Guest] g
		LEFT JOIN #LOCATION gLoc ON gloc.LocationID = g.LocationID
	-----------------------------------------------------------------------------------

	----populate synxis--------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': #LOCATION_synxis'
	RAISERROR(@print,10,1) WITH NOWAIT
	
	IF OBJECT_ID('tempdb..#LOCATION_synxis') IS NOT NULL
	DROP TABLE #LOCATION_synxis;


	CREATE TABLE #LOCATION_synxis
	(
		LocationID int NOT NULL,
		City_Text nvarchar(255) NOT NULL,
		State_Text nvarchar(255) NOT NULL,
		Country_Text nvarchar(255) NOT NULL,
		PostalCode_Text nvarchar(255) NOT NULL,
	)
	INSERT INTO #LOCATION(LocationID,City_Text,State_Text,Country_Text,PostalCode_Text)
	SELECT DISTINCT l.LocationID,ISNULL(ci.City_Text,'NULL'),ISNULL(st.State_Text,'NULL'),ISNULL(co.Country_Text,'NULL'),ISNULL(p.PostalCode_Text,'NULL')
	FROM  [synxis].[Location] l
		LEFT JOIN [synxis].[City] ci ON ci.CityID = l.CityID
		LEFT JOIN [synxis].[State] st ON st.StateID = l.StateID
		LEFT JOIN [synxis].[Country] co ON co.CountryID = l.CountryID
		LEFT JOIN [synxis].[PostalCode] p ON p.PostalCodeID = l.PostalCodeID


	---------------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': [synxis].[Guest]'
	RAISERROR(@print,10,1) WITH NOWAIT


	UPDATE g
	SET Location_LocationHashKey = HASHBYTES('MD5', UPPER(TRIM(ISNULL(g.Address1,'')))+UPPER(TRIM(ISNULL(g.Address2,''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.City_Text,'NULL',''),''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.State_Text,'NULL',''),''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.Country_Text,'NULL',''),''))) + UPPER(TRIM(ISNULL(REPLACE(gLoc.PostalCode_Text,'NULL',''),''))))
	FROM [synxis].[Guest] g
		LEFT JOIN #LOCATION gLoc ON gloc.LocationID = g.LocationID
	-----------------------------------------------------------------------------------

	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate_City_FromLocations'
	RAISERROR(@print,10,1) WITH NOWAIT
	EXEC dbo.Populate_City_FromLocations


	----------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate_State_FromLocations'
	RAISERROR(@print,10,1) WITH NOWAIT
	EXEC dbo.Populate_State_FromLocations

	------------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate_Country_FromLocations'
	RAISERROR(@print,10,1) WITH NOWAIT
	EXEC dbo.Populate_Country_FromLocations

	--------------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate_PostalCode_FromLocations'
	RAISERROR(@print,10,1) WITH NOWAIT
	EXEC dbo.Populate_PostalCode_FromLocations

	--------------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate_Location_FromLocations'
	RAISERROR(@print,10,1) WITH NOWAIT
	EXEC dbo.Populate_Location_FromLocations

	------------------------------------------------------
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': Populate_Guest_FromLocation'
	RAISERROR(@print,10,1) WITH NOWAIT
	EXEC dbo.Populate_Guest_FromLocation

	-- PRINT STATUS --
	SET @print = CONVERT(varchar(100),GETDATE(),120) + ': FINISHED'
	RAISERROR(@print,10,1) WITH NOWAIT
	RAISERROR('--------------------------------------------------------',10,1) WITH NOWAIT
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
