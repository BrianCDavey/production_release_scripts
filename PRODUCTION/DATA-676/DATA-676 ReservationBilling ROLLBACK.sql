USE ReservationBilling
GO

/*
Run this script on:

        CHI-SQ-DP-01\WAREHOUSE.ReservationBilling    -  This database will be modified

to synchronize it with:

        CHI-SQ-PR-01\WAREHOUSE.ReservationBilling

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.0.0.12866 from Red Gate Software Ltd at 1/22/2020 10:15:58 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [work].[billyItemCode]'
GO



ALTER FUNCTION [work].[billyItemCode]
(
	-- Add the parameters for the function here
	@billingDescription nvarchar(50),
	@bookingChannel nvarchar(50),
	@bookingSecondarySource nvarchar(50),
	@bookingSubSourceCode nvarchar(50),
	@bookingTemplateAbbreviation nvarchar(50),
	@bookingCroOptionID int,
	@chainID int
)
RETURNS nvarchar(50)
AS
BEGIN
	DECLARE @Result nvarchar(50)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result =  
        CASE @billingDescription
            WHEN 'Channel Connect - Booking.com' THEN 'DRC-BKC'
            WHEN 'Channel Connect - Expedia' THEN 'DRC-EXP'
            WHEN 'Channel Connect - Expedia Quick Connect' THEN 'DRC-EXQ'
            WHEN 'Channel Connect - Londontown.com' THEN 'DRC-LT'
            WHEN 'Channel Connect - Travelocity' THEN 'DRC-TLC'
            WHEN 'Channel Connect Reservations' THEN 'DRC-DRC'
            WHEN 'Channel Connect Express Reservations' THEN 'DRC-CCX'
            WHEN 'GDS Reservations' THEN
                CASE @bookingSecondarySource
                    WHEN 'Amadeus' THEN 'GDS-1A'
                    WHEN 'Galileo' THEN 'GDS-UA'
                    WHEN 'Sabre' THEN 'GDS-AA'
                    WHEN 'Worldspan' THEN 'GDS-1P'
					WHEN 'GDS' THEN 'GDS-AA'
                    ELSE null
                END
            WHEN 'Amadeus Reservations' THEN 'GDS-1A'
            WHEN 'Galileo Reservations' THEN 'GDS-UA'
            WHEN 'Sabre Reservations' THEN 'GDS-AA'
            WHEN 'Worldspan Reservations' THEN 'GDS-1P'
            WHEN 'Group Rooming List Import' THEN 'IBE-GC-ROOM'
            WHEN 'Guest Connect Booking Engine Reservations' THEN 
				CASE 
					WHEN @bookingSubSourceCode = '18009515/' THEN 'VOC-HE' 
					WHEN @bookingTemplateAbbreviation = 'HHAVOICE' THEN 'VOC-HE' 
					ELSE'IBE-GC-' + @bookingTemplateAbbreviation
				END
            WHEN 'Guest Connect Mobile Booking Engine Reservations' THEN 'IBE-MOB-' + @bookingTemplateAbbreviation
            WHEN 'HTML Booking Engine Reservations' THEN 'IBE-HTM-' + @bookingTemplateAbbreviation
            WHEN 'IDS Reservations' THEN 'IDS-ADS'
            WHEN 'Travel Web Reservations' THEN 'IDS-TWB'
            WHEN 'Travelocity Merchant Model Reservations' THEN 'GDS-AA-TLM'
            WHEN 'SynXis Call Center' THEN
                CASE 
					WHEN @bookingCroOptionID = 1 THEN --if the cro is one of PHGs
						CASE @bookingSubSourceCode 
							WHEN 'VCG' THEN 'VOC-VCG'
							WHEN 'VAKA' THEN 'VOC-VCG'
		                    ELSE 'VOC-PHG'
						END
	                WHEN @bookingCroOptionID = 3 THEN --hha CROs
						CASE @bookingSubSourceCode 
							WHEN 'VCG' THEN 'VOC-VCG-HE'
							WHEN 'VAKA' THEN 'VOC-VCG-HE'
		                    ELSE 'VOC-HE'
						END
					ELSE 'VOC-HOT'
				END
            WHEN 'Voice Agent Reservations' THEN
				CASE
					WHEN @bookingCroOptionID = 1 THEN --if the cro is one of PHGs
						CASE @bookingSubSourceCode 
							WHEN 'VCG' THEN 'VOC-VCG'
							WHEN 'VAKA' THEN 'VOC-VCG'
		                    ELSE 'VOC-PHG'
						END
					WHEN @bookingCroOptionID = 3 THEN --hha CROs
						CASE @bookingSubSourceCode 
							WHEN 'VCG' THEN 'VOC-VCG-HE'
							WHEN 'VAKA' THEN 'VOC-VCG-HE'
		                    ELSE 'VOC-HE'
						END
					ELSE 'VOC-HOT'
                END
            WHEN 'Open Hospitality' THEN 
				CASE 
					WHEN @bookingChannel = 'Voice' THEN 'VOC-HE'
					ELSE 'IBE-OH-' + @bookingTemplateAbbreviation
				END
            WHEN '' THEN --these are pegasus migrated bookings, we make our best educated guess
                CASE @bookingChannel
                    WHEN 'Voice' THEN 'VOC-VOC'
                    WHEN 'IDS' THEN 'IDS-ADS'
                    WHEN 'GDS' THEN
						CASE @bookingSecondarySource
                            WHEN 'Amadeus' THEN 'GDS-1A'
                            WHEN 'Galileo' THEN 'GDS-UA'
                            WHEN 'Sabre' THEN 'GDS-AA'
                            WHEN 'Worldspan' THEN 'GDS-1P'
							ELSE NULL
						END
                    WHEN 'CRS' THEN 'VOC-VOC'
                    WHEN 'Booking Engine' THEN 'IBE-GC-' + @bookingTemplateAbbreviation
                    ELSE NULL
				END
			WHEN 'UNKNOWN' THEN 
				CASE @bookingChannel
					WHEN 'PMS Rez Synch' THEN 'PMS'
					ELSE NULL
			END
			ELSE NULL
		END;
		
	RETURN @Result
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
