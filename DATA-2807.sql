/*
Run this script on:

        phg-hub-data-wus2-mi.e823ebc3d618.database.windows.net.Dimensional_Warehouse    -  This database will be modified

to synchronize it with:

        wus2-data-dp-01\WAREHOUSE.Dimensional_Warehouse

You are recommended to back up your database before running this script

Script created by SQL Compare version 14.5.1.18536 from Red Gate Software Ltd at 5/15/2023 4:10:41 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [fact].[Reservation]'
GO
ALTER TABLE [fact].[Reservation] DROP CONSTRAINT [FK_fact_Reservation_corporateAccountKey]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[fix_Reservation_LoyaltyNumber]'
GO
DROP PROCEDURE [dbo].[fix_Reservation_LoyaltyNumber]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dim].[Hotel]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dim].[Hotel] DROP
COLUMN [PH EVP]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[AddZeroKeyNullRecords]'
GO



ALTER PROCEDURE [dbo].[AddZeroKeyNullRecords]
	@debug bit = 0,
	@logProcess bit = 1,
	@Process_GUID uniqueidentifier
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ProcName sysname,@date datetime,@message varchar(2000)


	-- dim.DimDate --------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[DimDate]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[DimDate] WHERE [DateKey] = 0)
		BEGIN
			INSERT INTO [dim].[DimDate]([dateKey],[Full Date],[Date Name],[Day of Week],[Day Name Of Week],[Day of Month],[Day of Year],[Weekday Weekend],[Week of Year],[Month Name],[Month of Year],[Is Last Day Of Month],[Calendar Quarter],[Calendar Year],[Calendar Year Month],[Calendar Year Quarter])
			SELECT 0 AS [DateKey],CONVERT(date,'1753-01-01') AS [FullDate],'Unknown' AS [DateName],0 AS [DayOfWeek],'Unknown' AS [DayNameOfWeek],0 AS [DayOfMonth],0 AS [DayOfYear],'Unknown' AS [WeekdayWeekend],0 AS [WeekOfYear],'Unknown' AS [MonthName],0 AS [MonthOfYear],CONVERT(bit,0) AS [IsLastDayOfMonth],0 AS [CalendarQuarter],0 AS [CalendarYear],'Unknown' AS [CalendarYearMonth],'Unknown' AS [CalendarYearQtr]
		END
	-----------------------------------------------------------

	-- dim.BookingStatus --------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[BookingStatus]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID


		IF NOT EXISTS(SELECT * FROM [dim].BookingStatus WHERE [bookingStatusKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].BookingStatus ON

			INSERT INTO [dim].BookingStatus([bookingStatusKey],[Booking Status Code],[Booking Status Name])
			SELECT 0,'','Unknown'

			SET IDENTITY_INSERT [dim].BookingStatus OFF
		END

	-----------------------------------------------------------

	-- dim.Hotel ----------------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Hotel]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Hotel] WHERE [hotelKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Hotel] ON

			INSERT INTO [dim].[Hotel]([hotelKey],[Hotel Code],[Hotel Name],[Hotel Address],[Hotel City],[Hotel Sub State],[Hotel State],[Hotel Country],[Hotel Postal Code],[Hotel GeoCode],[Hotel Type],[Room Count],[Seasonal Hotel],[Hotel Average Nightly Rate],[PH Geographic Region Code],[PH Geographic Region Name],[PH AMD User Name],[PH AMD Name],[PH RD User Name],[PH RD Name],[PH RAM User Name],[PH RAM],[PH RD Supervisor User Name],[PH RD Supervisor],[PH RAM Supervisor User Name],[PH RAM Supervisor],[PH Primary Collection Code],[PH Primary Collection Name],[Historic Hotel Primary Collection Code],[Historic Hotel Primary Collection Name],[Residence Hotel Primary Collection Code],[Residence Hotel Primary Collection Name],[Primary 3rd Party],[Primary 3rd Party Collection],[I Prefer Participant],[I Prefer Opt-In Participant],[sourceKey],[Hotel Classification],[Primary Owner],[Hotel Management Company])
			SELECT 0 AS [hotelKey],'Unknown' AS [hotelCode],'Unknown' AS [hotelName],'Unknown' AS [hotelAddress],
				'Unknown' AS [hotelCity],'Unknown' AS [hotelSubState],'Unknown' AS [hotelState],'Unknown' AS [hotelCountry],
				'Unknown' AS [hotelPostalCode],geography::Point(0,0,4326) AS [hotelLatLong],'Unknown' AS [hotelType],
				0 AS [roomCount],CONVERT(bit,0) AS [seasonalHotel],0.0 AS [hotelAverageNightlyRate],
				'Unknown' AS [phGeographicRegionCode],'Unknown' AS [phGeographicRegionName],
				'Unknown' AS [phAreaManagerUsername],'Unknown' AS [phAreaManagerName],
				'Unknown' AS [phRegionalDirectorUsername],'Unknown' AS [phRegionalDirectorName],
				'Unknown' AS [phRevenueAccountManagerUsername],'Unknown' AS [phRevenueAccountManager],
				'Unknown' AS [phRdSupervisorUsername],'Unknown' AS [phRdSupervisor],'Unknown' AS [phRamSupervisorUsername],
				'Unknown' AS [phRamSupervisor],'Unknown' AS [phPrimaryCollectionCode],'Unknown' AS [phPrimaryCollectionName],
				'Unknown' AS [hePrimaryCollectionCode],'Unknown' AS [hePrimaryCollectionName],
				'Unknown' AS [resPrimaryCollectionCode],'Unknown' AS [resPrimaryCollectionName],
				'Unknown' AS [primary3rdParty],'Unknown' AS [primary3rdPartyCollection],
				CONVERT(bit,0) AS [loyaltyParticipant],CONVERT(bit,0) AS [loyaltyOptInParticipant],0 AS sourceKey,
				'Unknown' AS [Hotel Classification],'Unknown' AS [Primary Owner],'Unknown' AS [Hotel Management Company]

			SET IDENTITY_INSERT [dim].[Hotel] OFF
		END
	-----------------------------------------------------------
	
	-- [dim].[PHG Member] -------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[PHG Member]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[PHG Member] WHERE [phgMemberKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[PHG Member] ON

			INSERT INTO [dim].[PHG Member]([phgMemberKey],[CRM_UserID],[Member Name],[Member Email],[Member Address],[Member City],[Member State],[Member Country],[Member Postal Code],[Member Region],[Member GeoCode],[Member Territory])
			VALUES(0,'00000000-0000-0000-0000-000000000000','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown',geography::Point(0,0,4326),'Unknown')

			SET IDENTITY_INSERT [dim].[PHG Member] OFF
		END
	-----------------------------------------------------------

	-- [dim].[Contact] ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Contact]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Contact] WHERE [contactKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Contact] ON

			INSERT INTO [dim].[Contact]([contactKey],[CRM_ContactID],[Contact Name],[Contact Email],[Contact Address],[Contact City],[Contact State],[Contact Country],[Contact Postal Code],[Contact Region],[Contact GeoCode])
			VALUES(0,'00000000-0000-0000-0000-000000000000','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown',geography::Point(0,0,4326))

			SET IDENTITY_INSERT [dim].[Contact] OFF
		END
	-----------------------------------------------------------

	-- [dim].[Account] ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Account]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Account] WHERE [accountKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Account] ON

			INSERT INTO [dim].[Account]([accountKey],[CRM_AccountID],[Account Name],[Account Email],[Account Address],[Account City],[Account State],[Account Country],[Account Postal Code],[Account Region],[Account GeoCode],[Account Industry],[Account Competitive Set],[IBE Company])
			VALUES(0,'00000000-0000-0000-0000-000000000000','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown','Unknown',geography::Point(0,0,4326),'Unknown','Unknown','Unknown')

			SET IDENTITY_INSERT [dim].[Account] OFF
		END
	-----------------------------------------------------------

	-- [fact].[Group Sales RFP] -------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [fact].[Group Sales RFP]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [fact].[Group Sales RFP] WHERE [groupSalesRfpKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [fact].[Group Sales RFP] ON

			INSERT INTO [fact].[Group Sales RFP]([groupSalesRfpKey],[CRM_OpportunityID],[RFP Name],[Create Date],[Close Date],
												[Start Date],[End Date],[Proposal Due Date],[Contract Start Date],[Contract End Date],
												[Decision Date],[Estimated Value],[Actual Value],[Close Probability],[Expected Total Attendance],
												[Total Room Nights],[Peak Room Nights],[ROI Guaranteed],[Is New Business],[Catering Only],[Opportunity Rating],
												[RFP State],[RFP Status],[RFP Lead Source],[Winning Hotel],[Event Host Account],[Managing Account],
												[Event Planner Contact],[Sales Contact],[Creator Member],[Owner Member],[currencykey],
												[SplitCommission],[CommissionType],[Managing Event Host])
			VALUES(0,'00000000-0000-0000-0000-000000000000','Unknown',0,0,
					0,0,0,0,0,
					0,0,0,0,0,
					0,0,'','','','Unknown',
					'Unknown','Unknown','Unknown',0,0,0,
					0,0,0,0,0,
					'Unknown','Unknown','Unknown')

			SET IDENTITY_INSERT [fact].[Group Sales RFP] OFF
		END
	-----------------------------------------------------------

	-- [fact].[Group Sales Lead] ------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [fact].[Group Sales Lead]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [fact].[Group Sales Lead] WHERE [groupSalesLeadKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [fact].[Group Sales Lead] ON

			INSERT INTO [fact].[Group Sales Lead]([groupSalesLeadKey],[CRM_RequestForProposalID],[Lead Name],[Create Date],[Original Sourced Date],[Start Date],[End Date],
												[Flexible Dates],[Average Daily Rate Double Occupancy],[Average Daily Rate Single Occupancy],[Expected Total Attendance],[Room Nights],
												[Total Room Nights],[Total Room Revenue],[Event Dates Are Firm],[Lead Is Active],[Lead Status],[Creator Member],
												[Owner Member],[hotelKey],[currencykey],[groupSalesRfpKey],[Hotel Contact])
			VALUES(0,'00000000-0000-0000-0000-000000000000','Unknown',0,0,0,0,
					'Unknown',0,0,0,0,
					0,0,'Unknown','','Unknown',0,
					0,0,0,0,0)

			SET IDENTITY_INSERT [fact].[Group Sales Lead] OFF
		END
	-----------------------------------------------------------

	-- [fact].[Group Sales Actual] ------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [fact].[Group Sales Actual]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [fact].[Group Sales Actual] WHERE [groupSalesActualKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [fact].[Group Sales Actual] ON

			INSERT INTO [fact].[Group Sales Actual]([groupSalesActualKey],[CRM_GroupSalesActualsID],[Actual Name],[Create Date],[Start Date],[End Date],[Exchange Rate],
													[Actual Revenue],[Actual Room Nights],[Food And Beverage Spend],[Actual Is Active],[Actual Status Reason],
													[Winning Hotel],[currencykey],[groupSalesRfpKey],[Close Date],[Actual Revenue USD])
			VALUES(0,'00000000-0000-0000-0000-000000000000','Unknown',0,0,0,0,
					0,0,0,'','Unknown',
					0,0,0,0,0.0)

			SET IDENTITY_INSERT [fact].[Group Sales Actual] OFF
		END
	-----------------------------------------------------------

	-- dim.BookingSource --------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[BookingSource]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[BookingSource] WHERE [bookingSourceKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[BookingSource] ON
	
			INSERT INTO [dim].[BookingSource]([bookingSourceKey],[PH Channel],[phSecondarySource],[PH Sub Source],[Booking System],[CRS Channel],[CRS Secondary Source],[CRS Sub Source],[CRO Code],[CRO Name],[IBE Source],CRS_sourceKey,PH_sourceKey)
			SELECT 0 AS [bookingSourceKey],'Unknown' AS [phChannel],'Unknown' AS [phSecondarySource],'Unknown' AS [phSubSource],'Unknown' AS [bookingSystem],'Unknown' AS [crsChannel],'Unknown' AS [crsSecondarySource],'Unknown' AS [crsSubSource],'Unknown' AS [croCode],'Unknown' AS [croName],'Unknown' AS [ibeTemplate],0 AS CRS_sourceKey,0 AS PH_sourceKey
		
			SET IDENTITY_INSERT [dim].[BookingSource] OFF
		END
	-----------------------------------------------------------

	-- dim.BookingStatus --------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- dim.CorporateAccount -----------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.CRS_Chain ------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[CRS_Chain]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[CRS_Chain] WHERE [crsChainKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[CRS_Chain] ON

			INSERT INTO [dim].[CRS_Chain]([crsChainKey],[CRS Chain ID],[CRS Chain Name])
			SELECT 0 AS [crsChainKey],'Unknown' AS [crsChainID],'Unknown' AS [crsChainName]

			SET IDENTITY_INSERT [dim].[CRS_Chain] OFF
		END
	-----------------------------------------------------------

	-- dim.Currency -------------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- fact.CurrencyExchange ----------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [fact].[CurrencyExchange]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [fact].[CurrencyExchange] WHERE [CurrencyExchangeKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [fact].[CurrencyExchange] ON

			INSERT INTO [fact].[CurrencyExchange]([CurrencyExchangeKey],[currencyKey],[DateKey],[USD Exchange Rate])
			SELECT 0 AS [CurrencyExchangeKey],0 AS [currencyKey],NULL AS [DateKey],0.0 AS [USD_ExchangeRate]

			SET IDENTITY_INSERT [fact].[CurrencyExchange] OFF
		END
	-----------------------------------------------------------

	-- dim.FormOfPayment --------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[FormOfPayment]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[FormOfPayment] WHERE [formOfPaymentKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[FormOfPayment] ON

			INSERT INTO [dim].[FormOfPayment]([formOfPaymentKey],[Form of Payment Code],[Form of Payment Name])
			SELECT 0 AS [formOfPaymentKey],'Unknown' AS [formOfPaymentCode],'Unknown' AS [formOfPaymentName]

			SET IDENTITY_INSERT [dim].[FormOfPayment] OFF
		END
	-----------------------------------------------------------

	-- dim.Guest ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Guest]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Guest] WHERE [guestKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Guest] ON

			INSERT INTO [dim].[Guest]([guestKey],[First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],[sourceKey],memberKey)
			SELECT 0 AS [guestKey],'Unknown' AS [firstName],'Unknown' AS [lastName],'Unknown' AS [fullName],'Unknown' AS [emailAddress],'Unknown' AS [emailDomain],'Unknown' AS [guestAddress],'Unknown' AS [guestCity],'Unknown' AS [guestSubState],'Unknown' AS [guestState],'Unknown' AS [guestCountry],'Unknown' AS [guestPostalCode],geography::Point(0,0,4326) AS [guestLatLong],'Unknown' AS [phLoyaltyNumber],'Unknown' AS [phLoyaltyStatus],'Unknown' AS [phLoyaltyTier],'Unknown' AS [gender],'0' AS sourceKey,0 AS memberKey

			SET IDENTITY_INSERT [dim].[Guest] OFF
		END
	-----------------------------------------------------------

	-- dim.Hotel_PH_Collection --------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Hotel_PH_Collection]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Hotel_PH_Collection] WHERE [hotel_PH_CollectionKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Hotel_PH_Collection] ON

			INSERT INTO [dim].[Hotel_PH_Collection]([hotel_PH_CollectionKey],[hotelKey],[Start Date],[End Date],[Collection Code],[Collection Name],[priorityOrder])
			SELECT 0 AS [hotel_PH_CollectionKey],0 AS [hotelKey],NULL AS [Start Date],NULL AS [End Date],'Unknown' AS [collectionCode],'Unknown' AS [collectionName],0 AS [priorityOrder]

			SET IDENTITY_INSERT [dim].[Hotel_PH_Collection] OFF
		END
	-----------------------------------------------------------

	-- dim.Hotel_PH_Program -----------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Hotel_PH_Program]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Hotel_PH_Program] WHERE [hotel_PH_ProgramKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Hotel_PH_Program] ON

			INSERT INTO [dim].[Hotel_PH_Program]([hotel_PH_ProgramKey],[hotelKey],[Start Date],[End Date],[Program Name])
			SELECT 0 AS [hotel_PH_ProgramKey],0 AS [hotelKey],NULL AS [Start Date],NULL AS [End Date],'Unknown' AS [programName]

			SET IDENTITY_INSERT [dim].[Hotel_PH_Program] OFF
		END
	-----------------------------------------------------------

	-- dim.HotelExpereince ------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[HotelExpereince]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[HotelExpereince] WHERE [hotelExpereinceKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[HotelExpereince] ON

			INSERT INTO [dim].[HotelExpereince]([hotelExpereinceKey],[hotelKey],[Experience])
			SELECT 0 AS [hotelExpereinceKey],0 AS [hotelKey],'Unknown' AS [experience]

			SET IDENTITY_INSERT [dim].[HotelExpereince] OFF
		END
	-----------------------------------------------------------

	-- dim.HotelParentCompany ---------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[HotelParentCompany]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[HotelParentCompany] WHERE [hotelParentCompanyKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[HotelParentCompany] ON

			INSERT INTO [dim].[HotelParentCompany]([hotelParentCompanyKey],[hotelKey],[Start Date],[End Date],[CRM ID],[Parent Name])
			SELECT 0 AS [hotelParentCompanyKey],0 AS [hotelKey],NULL AS [Start Date],NULL AS [End Date],CONVERT(uniqueidentifier,'00000000-0000-0000-0000-000000000000') AS [parentAccountCrmID],'Unknown' AS [parentAccountName]

			SET IDENTITY_INSERT [dim].[HotelParentCompany] OFF
		END
	-----------------------------------------------------------

	-- dim.LeisureAccount -------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.LoyaltyDetail --------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- dim.PartyDetail ----------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.PseudoCity -----------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.Rate -----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Rate]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Rate] WHERE [rateKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Rate] ON
		
			INSERT INTO [dim].[Rate]([rateKey],[Rate Code],[Rate Name],[PH Rate Name],[Rate Category],[PH Rate Category],[GDS Rate Access Code],[GDS Booking Code],[Commissionable],[Corporate Code],[Promo Code],[Coupon Code])
			SELECT 0 AS [rateKey],'Unknown' AS [rateCode],'Unknown' AS [rateName],'Unknown' AS [phRateName],'Unknown' AS [rateCategory],'Unknown' AS [phRateCategory],'Unknown' AS [gdsRateAccessCode],'Unknown' AS [gdsBookingCode],CONVERT(bit,0) AS [commissionable],'Unknown' AS [corporateCode],'Unknown' AS [promoCode],'Unknown' AS [couponCode]

			SET IDENTITY_INSERT [dim].[Rate] OFF
		END
	-----------------------------------------------------------

	-- dim.Room -----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[Room]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[Room] WHERE [roomKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[Room] ON

			INSERT INTO [dim].[Room]([roomKey],[Room Code],[Room Name],[Room Category],[PH Room Type])
			SELECT 0 AS [roomKey],'Unknown' AS [roomCode],'Unknown' AS [roomName],'Unknown' AS [crsRoomCategory],'Unknown' AS [phRoomType]

			SET IDENTITY_INSERT [dim].[Room] OFF
		END
	-----------------------------------------------------------

	-- dim.TravelAgency ---------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[TravelAgency]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[TravelAgency] WHERE [travelAgencyKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[TravelAgency] ON
		
			INSERT INTO [dim].[TravelAgency]([travelAgencyKey],[IATA Number],[Travel Agency CRM Guid],[Travel Agency Name],[Travel Agency Address],[Travel Agency City],[Travel Agency Sub State],[Travel Agency State],[Travel Agency Country],[Travel Agency Postal Code],[Travel Agency GeoCode],[PH Account Manager User Name],[PH Account Manager Name],[PH Account Manager Supervisor User Name],[PH Account Manager Supervisor],[Primary Travel Agent Group],[Start Date],[End Date])
			SELECT 0 AS [travelAgencyKey],'0' AS [iataNumber],CONVERT(uniqueidentifier,'00000000-0000-0000-0000-000000000000') AS [travelAgencyCrmIGuid],'Unknown' AS [travelAgencyName],'Unknown' AS [travelAgencyAddress],'Unknown' AS [travelAgencyCity],'Unknown' AS [travelAgencySubstate],'Unknown' AS [travelAgencyState],'Unknown' AS [travelAgencyCountry],'Unknown' AS [travelAgencyPostalCode],geography::Point(0,0,4326) AS [travelAgencyLatLong],'Unknown' AS [phAccountManagerUsername],'Unknown' AS [phAccountManagerName],'Unknown' AS [phAmSupervisorUsername],'Unknown' AS [phAmSupervisor],'Unknown' AS [primaryTravelAgentGroup],NULL AS [rowStartDate],NULL AS [rowEndDate]

			SET IDENTITY_INSERT [dim].[TravelAgency] OFF
		END
	-----------------------------------------------------------

	-- dim.TravelAgencyGroup ----------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.VoiceAgent -----------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE(),@message = 'AddZeroKeyNullRecords TO [dim].[VoiceAgent]'
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,@message,@Process_GUID

		IF NOT EXISTS(SELECT * FROM [dim].[VoiceAgent] WHERE [voiceAgentKey] = 0)
		BEGIN
			SET IDENTITY_INSERT [dim].[VoiceAgent] ON

			INSERT INTO [dim].[VoiceAgent]([voiceAgentKey],[Voice Agent User Name])
			SELECT 0 AS [voiceAgentKey],'Unknown' AS [voiceAgentUserName]

			SET IDENTITY_INSERT [dim].[VoiceAgent] OFF
		END
	-----------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_temp_Guest_Reservations]'
GO

ALTER   PROCEDURE [dbo].[Populate_temp_Guest_Reservations]
	@OldestDate date
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	-- TRUNCATE TABLE
	TRUNCATE TABLE [temp].[Guest_Reservations];

	-- MERGE INTO TABLE
	;WITH cte_Oldest
	AS
	(
		SELECT DISTINCT GuestID
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
		WHERE ts.confirmationDate > @OldestDate
	),
	cte_LN
	AS
	(
		-- In order to uniqueify GuestID some Loyalty Numbers will be lost
		-- a single guest has multiple Valid Loyalty Numbers
		SELECT t.GuestID,ln.loyaltyNumber,
				ROW_NUMBER() OVER(PARTITION BY t.GuestID ORDER BY t.transactionTimeStamp DESC) AS rowNum
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.LoyaltyNumber ln ON ln.LoyaltyNumberID = t.LoyaltyNumberID
			INNER JOIN Loyalty.dbo.LoyaltyNumber lln ON lln.LoyaltyNumberName = ln.loyaltyNumber
			INNER JOIN Loyalty.dbo.MemberProfile mp ON mp.LoyaltyNumberID = lln.LoyaltyNumberID
		WHERE mp.MemberStatus = 1
	),
	cte_Guest
	AS
	(
		SELECT DISTINCT g.FirstName,g.LastName,g.FirstName + ' ' + g.LastName AS FullName,ge.emailAddress,
				SUBSTRING(ge.emailAddress,CHARINDEX('@',ge.emailAddress) + 1,LEN(ge.emailAddress)) AS emailDomain,
				COALESCE(g.Address1,'') + ' ' + COALESCE(g.Address2,'') AS guestAddress,
				NULLIF(loc.City_Text,'') AS guestCity,
				NULLIF(loc.State_Text,'') AS guestState,
				NULLIF(loc.Country_Text,'') AS guestCountry,
				NULLIF(loc.PostalCode_Text,'') AS guestPostalCode,
				g.GuestID AS sourceKey,
				ln.LoyaltyNumber
		FROM Reservations.dbo.Guest g
			LEFT JOIN Reservations.dbo.Guest_EmailAddress ge ON ge.Guest_EmailAddressID = g.Guest_EmailAddressID
			LEFT JOIN Reservations.dbo.vw_Location loc ON loc.LocationID = g.LocationID
			LEFT JOIN cte_LN ln ON ln.GuestID = g.GuestID AND ln.rowNum = 1
		WHERE g.GuestID IN(SELECT GuestID FROM cte_Oldest)
	)
	INSERT INTO [temp].[Guest_Reservations]([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],
											[Guest City],[Guest State],[Guest Country],[Guest Postal Code],
											[PH Loyalty Number],[sourceKey])
	SELECT DISTINCT FirstName,LastName,FullName,emailAddress,emailDomain,guestAddress,
					guestCity,guestState,guestCountry,guestPostalCode,
					loyaltyNumber AS [phLoyaltyNumber],sourceKey
	FROM cte_Guest g
END
 
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Guest]'
GO


ALTER PROCEDURE [dbo].[Populate_Guest]
	@OldestDate date,
	@debug bit = 0,
	@logProcess bit = 1,
	@Process_GUID uniqueidentifier
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	DECLARE @ProcName sysname,@date datetime

	-- POPULATE [temp].[Guest_Reservations] -------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	POPULATE [temp].[Guest_Reservations]',@Process_GUID

		EXEC [dbo].[Populate_temp_Guest_Reservations] @OldestDate;
	-----------------------------------------------------------------------

	-- POPULATE [temp].[Guest_Loyalty] ------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	POPULATE [temp].[Guest_Loyalty]',@Process_GUID

		EXEC [dbo].[Populate_temp_Guest_Loyalty];
	-----------------------------------------------------------------------

	-- POPULATE temp.Guest for Reservations -------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	POPULATE temp.Guest for Reservations',@Process_GUID

		EXEC [dbo].[Populate_temp_Guest];
	-----------------------------------------------------------------------

	-- TRUNCATE dim.Guest TABLE -------------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	TRUNCATE dim.Guest',@Process_GUID

		EXEC [dbo].[Reset_dim_Guest]
	-----------------------------------------------------------------------

	-- DISABLE INDEXES BEFORE LOAD ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	DISABLE INDEXES BEFORE LOAD',@Process_GUID

		EXEC dbo.disable_NonClusteredIndexes 'Guest','dim'
	-----------------------------------------------------------------------

	-- MERGE INTO dim.Guest for Reservations ------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	MERGE INTO dim.Guest for Reservations',@Process_GUID
		
		-- CREATE & POPULATE #HASH --------------------------
		IF OBJECT_ID('tempdb..#HASH') IS NOT NULL
			DROP TABLE #HASH;

		CREATE TABLE #HASH
		(
			hashKey varbinary(250) NOT NULL,
			sourceKey varchar(MAX),
			[max_GuestID] int NOT NULL,
			sourceKeyLoyalty varchar(MAX),

			PRIMARY KEY CLUSTERED([max_GuestID])
		)

		INSERT INTO #HASH(hashKey,sourceKey,[max_GuestID],sourceKeyLoyalty)
		SELECT hashKey,
			STRING_AGG(CONVERT(varchar(MAX),[sourceKey]),',') AS [sourceKey],
			MAX(temp_GuestID) AS [max_GuestID],
			STRING_AGG(CONVERT(varchar(MAX),[sourceKeyLoyalty]),',') AS [sourceKeyLoyalty]
		FROM temp.Guest
		WHERE [PH Loyalty Number] = 'Unknown'
		GROUP BY hashKey
		-----------------------------------------------------

		MERGE INTO [dim].[Guest] AS tgt
		USING
		(
			SELECT [First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],
					COALESCE(sa.subAreaName,g.[Guest State]) AS [Guest State],
					COALESCE(c2.shortName,c3.shortName,[Guest Country]) AS [Guest Country],
					[Guest Postal Code],
					[Guest GeoCode],
					[PH Loyalty Number],
					[PH Loyalty Status],
					[PHLoyalty Tier],
					[Gender],
					[dbo].[Uniquify_CSV](h.sourceKeyLoyalty) AS sourceKeyLoyalty,
					[dbo].[Uniquify_CSV](h.[sourceKey]) AS sourceKey,
					[IsSelfReported_Gender],
					0 AS memberKey,
					[Marital Status],
					'False' AS [Is Frequent I Prefer],
					g.hashKey
			FROM temp.Guest g
				INNER JOIN #HASH h ON h.[max_GuestID] = g.temp_GuestID
				LEFT JOIN ISO.dbo.countries c2 ON c2.code2 = g.[Guest Country]
				LEFT JOIN ISO.dbo.countries c3 ON c3.code3 = g.[Guest Country]
				LEFT JOIN ISO.dbo.subAreas sa ON sa.subAreaCode = g.[Guest State]
												AND sa.countryCode2 = 'US'
												AND g.[Guest Country] IN('US','USA','unknown','United States')
		) AS src ON src.hashKey = tgt.hashKey
		WHEN MATCHED THEN
			UPDATE
				SET [First Name] = src.[First Name],
					[Last Name] = src.[Last Name],
					[Full Name] = src.[Full Name],
					[Email Address] = src.[Email Address],
					[Email Domain] = src.[Email Domain],
					[Guest Address] = src.[Guest Address],
					[Guest City] = src.[Guest City],
					[Guest Sub State] = src.[Guest Sub State],
					[Guest State] = src.[Guest State],
					[Guest Country] = src.[Guest Country],
					[Guest Postal Code] = src.[Guest Postal Code],
					[Guest GeoCode] = src.[Guest GeoCode],
					[PH Loyalty Number] = src.[PH Loyalty Number],
					[PH Loyalty Status] = src.[PH Loyalty Status],
					[PHLoyalty Tier] = src.[PHLoyalty Tier],
					[Gender] = CASE src.[Gender] WHEN 'Unknown' THEN tgt.[Gender] ELSE src.[Gender] END,
					[IsSelfReported_Gender] = CASE src.[Gender] WHEN 'Unknown' THEN tgt.[IsSelfReported_Gender] ELSE src.[IsSelfReported_Gender] END,
					sourceKeyLoyalty = src.sourceKeyLoyalty,
					memberKey = src.memberKey,
					[Marital Status] = src.[Marital Status],
					[Is Frequent I Prefer] = src.[Is Frequent I Prefer]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],sourceKeyLoyalty,[sourceKey],[IsSelfReported_Gender],memberKey,[Marital Status],[Is Frequent I Prefer])
			VALUES([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],sourceKeyLoyalty,[sourceKey],[IsSelfReported_Gender],memberKey,[Marital Status],[Is Frequent I Prefer])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	-----------------------------------------------------------------------

	-- MERGE INTO dim.Guest for Loyalty -----------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	MERGE INTO dim.Guest for Loyalty',@Process_GUID

		-- CREATE & POPULATE #LN ----------------------------
		IF OBJECT_ID('tempdb..#LN') IS NOT NULL
			DROP TABLE #LN;

		CREATE TABLE #LN
		(
			[PH Loyalty Number] varchar(250) NOT NULL,
			[sourceKey] varchar(MAX),
			[max_GuestID] int NOT NULL,
			[sourceKeyLoyalty] varchar(MAX),
			[First Name] nvarchar(MAX) NOT NULL,
			[Last Name] nvarchar(MAX) NOT NULL,
			[Full Name] nvarchar(MAX) NOT NULL,
			[Email Address] nvarchar(MAX) NOT NULL,
			[Email Domain] nvarchar(MAX) NOT NULL,
			[Guest Address] nvarchar(MAX) NOT NULL,
			[Guest City] nvarchar(MAX) NOT NULL,
			[Guest Sub State] nvarchar(MAX) NOT NULL,
			[Guest State] nvarchar(MAX) NOT NULL,
			[Guest Country] nvarchar(MAX) NOT NULL,
			[Guest Postal Code] nvarchar(MAX) NOT NULL

			PRIMARY KEY CLUSTERED([max_GuestID])
		)

		INSERT INTO #LN([PH Loyalty Number],[sourceKey],[max_GuestID],[sourceKeyLoyalty],[First Name],[Last Name],
						[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],
						[Guest State],[Guest Country],[Guest Postal Code])
		SELECT [PH Loyalty Number],
			STRING_AGG(CONVERT(varchar(MAX),[sourceKey]),',') AS [sourceKey],
			MAX(temp_GuestID) AS [max_GuestID],
			STRING_AGG(CONVERT(varchar(MAX),[sourceKeyLoyalty]),',') AS [sourceKeyLoyalty],
			ISNULL(MAX(NULLIF(NULLIF([First Name],'Unknown'),'')),'Unknown') AS [First Name],
			ISNULL(MAX(NULLIF(NULLIF([Last Name],'Unknown'),'')),'Unknown') AS [Last Name],
			ISNULL(MAX(NULLIF(NULLIF([Full Name],'Unknown'),'')),'Unknown') AS [Full Name],
			ISNULL(MAX(NULLIF(NULLIF([Email Address],'Unknown'),'')),'Unknown') AS [Email Address],
			ISNULL(MAX(NULLIF(NULLIF([Email Domain],'Unknown'),'')),'Unknown') AS [Email Domain],
			ISNULL(MAX(NULLIF(NULLIF([Guest Address],'Unknown'),'')),'Unknown') AS [Guest Address],
			ISNULL(MAX(NULLIF(NULLIF([Guest City],'Unknown'),'')),'Unknown') AS [Guest City],
			ISNULL(MAX(NULLIF(NULLIF([Guest Sub State],'Unknown'),'')),'Unknown') AS [Guest Sub State],
			ISNULL(MAX(NULLIF(NULLIF([Guest State],'Unknown'),'')),'Unknown') AS [Guest State],
			ISNULL(MAX(NULLIF(NULLIF([Guest Country],'Unknown'),'')),'Unknown') AS [Guest Country],
			ISNULL(MAX(NULLIF(NULLIF([Guest Postal Code],'Unknown'),'')),'Unknown') AS [Guest Postal Code]
		FROM temp.Guest
		WHERE [PH Loyalty Number] != 'Unknown'
		GROUP BY [PH Loyalty Number]
		-----------------------------------------------------

		MERGE INTO [dim].[Guest] AS tgt
		USING
		(
			SELECT ln.[First Name],
					ln.[Last Name],
					ln.[Full Name],
					ln.[Email Address],
					ln.[Email Domain],
					ln.[Guest Address],
					ln.[Guest City],
					ln.[Guest Sub State],
					ln.[Guest State],
					COALESCE(c2.shortName,c3.shortName,ln.[Guest Country]) AS [Guest Country],
					ln.[Guest Postal Code],
					[Guest GeoCode],
					g.[PH Loyalty Number],
					[PH Loyalty Status],
					[PHLoyalty Tier],
					[Gender],
					[dbo].[Uniquify_CSV](ln.sourceKeyLoyalty) AS sourceKeyLoyalty,
					[dbo].[Uniquify_CSV](ln.[sourceKey]) AS sourceKey,
					[IsSelfReported_Gender],
					0 AS memberKey,
					[Marital Status],
					'False' AS [Is Frequent I Prefer]
			FROM temp.Guest g
				INNER JOIN #LN ln ON ln.max_GuestID = g.temp_GuestID
				LEFT JOIN ISO.dbo.countries c2 ON c2.code2 = ln.[Guest Country]
				LEFT JOIN ISO.dbo.countries c3 ON c3.code3 = ln.[Guest Country]
		) AS src ON src.[PH Loyalty Number] = tgt.[PH Loyalty Number]
		WHEN MATCHED THEN
			UPDATE
				SET [First Name] = src.[First Name],
					[Last Name] = src.[Last Name],
					[Full Name] = src.[Full Name],
					[Email Address] = src.[Email Address],
					[Email Domain] = src.[Email Domain],
					[Guest Address] = src.[Guest Address],
					[Guest City] = src.[Guest City],
					[Guest Sub State] = src.[Guest Sub State],
					[Guest State] = src.[Guest State],
					[Guest Country] = src.[Guest Country],
					[Guest Postal Code] = src.[Guest Postal Code],
					[Guest GeoCode] = src.[Guest GeoCode],
					[PH Loyalty Number] = src.[PH Loyalty Number],
					[PH Loyalty Status] = src.[PH Loyalty Status],
					[PHLoyalty Tier] = src.[PHLoyalty Tier],
					[Gender] = CASE src.[Gender] WHEN 'Unknown' THEN tgt.[Gender] ELSE src.[Gender] END,
					[IsSelfReported_Gender] = CASE src.[Gender] WHEN 'Unknown' THEN tgt.[IsSelfReported_Gender] ELSE src.[IsSelfReported_Gender] END,
					sourceKey = src.sourceKey,
					memberKey = src.memberKey,
					[Marital Status] = src.[Marital Status],
					[Is Frequent I Prefer] = src.[Is Frequent I Prefer]
		WHEN NOT MATCHED BY TARGET THEN
			INSERT([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],sourceKeyLoyalty,[sourceKey],[IsSelfReported_Gender],memberKey,[Is Frequent I Prefer])
			VALUES([First Name],[Last Name],[Full Name],[Email Address],[Email Domain],[Guest Address],[Guest City],[Guest Sub State],[Guest State],[Guest Country],[Guest Postal Code],[Guest GeoCode],[PH Loyalty Number],[PH Loyalty Status],[PHLoyalty Tier],[Gender],sourceKeyLoyalty,[sourceKey],[IsSelfReported_Gender],memberKey,[Is Frequent I Prefer])
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE
		;
	-----------------------------------------------------------------------

	-- ENABLE INDEXES AFTER LOAD ------------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	ENABLE INDEXES AFTER LOAD',@Process_GUID

		EXEC dbo.enable_NonClusteredIndexes 'Guest','dim'	
	-----------------------------------------------------------------------

	-- POPULATE MAPPING TABLE ---------------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	POPULATE MAPPING TABLE',@Process_GUID

		EXEC map.Populate_guestKey_GuestID
	-----------------------------------------------------------------------

	-- POPULATE dim.Member ------------------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'	POPULATE dim.Member',@Process_GUID

		EXEC [dbo].Populate_Member
	-----------------------------------------------------------------------
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Hotel]'
GO
ALTER PROCEDURE [dbo].[Populate_Hotel]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;
	
	DECLARE @maxInt int = 2147483647;

	;WITH cte_ResRev(HotelID,AvgNightlyRate)
	AS
	(
		SELECT h.Hotel_hotelID,SUM(td.reservationRevenue)/CONVERT(decimal(38,2),SUM(td.nights))/CONVERT(decimal(38,2),SUM(td.rooms)) AS AvgNightlyRate
		FROM Reservations.dbo.Transactions t
			INNER JOIN Reservations.dbo.TransactionDetail td ON td.TransactionDetailID = t.TransactionDetailID
			INNER JOIN Reservations.dbo.TransactionStatus ts ON ts.TransactionStatusID = t.TransactionStatusID
			INNER JOIN Reservations.dbo.hotel h ON h.HotelID = t.HotelID
		WHERE ts.confirmationDate BETWEEN DATEADD(year,-1,GETDATE()) AND GETDATE()
		GROUP BY h.Hotel_hotelID
	),
	cte_Col(HotelID,Code,CollectionName,rowNum)
	AS
	(
		SELECT hcol.HotelID,col.Code,col.CollectionName,ROW_NUMBER() OVER(PARTITION BY hcol.HotelID ORDER BY ISNULL(col.Hierarchy,@maxInt))
		FROM Hotels.dbo.[Collection] col
			INNER JOIN Hotels.dbo.Hotel_Collection hcol ON hcol.CollectionID = col.CollectionID
	),
	cte_LOC(LocationID,CityName,StateCode,CountryName,PostalCodeName,RegionName)
	AS
	(
		SELECT l.LocationID,cty.CityName,st.StateCode,cnty.CountryName,post.PostalCodeName,reg.RegionName
		FROM Hotels.dbo.[Location] l
		LEFT JOIN Hotels.dbo.City cty ON cty.CityID = l.CityID
		LEFT JOIN Hotels.dbo.[State] st ON st.StateID = l.StateID
		LEFT JOIN Hotels.dbo.Country cnty ON cnty.CountryID = l.CountryID
		LEFT JOIN Hotels.dbo.PostalCode post ON post.PostalCodeID = l.PostalCodeID
		LEFT JOIN Hotels.dbo.Region reg ON reg.RegionID = l.RegionID
	),
	cte_User(HotelID,WindowsAccount,FirstName,LastName,PHG_RoleName)
	AS
	(
		SELECT pur.HotelID,pu.WindowsAccount,p.FirstName,p.LastName,pr.PHG_RoleName
		FROM Hotels.dbo.Hotel_PHG_User_Role pur
			INNER JOIN Hotels.dbo.PHG_User pu ON pu.PHG_UserID = pur.PHG_UserID
			INNER JOIN Hotels.dbo.PHG_Role pr ON pr.PHG_RoleID = pur.PHG_RoleID
			INNER JOIN Hotels.dbo.Person p ON p.PersonID = pu.PersonID
	),
	cte_HotelParent
	AS
	(
		SELECT hpc.HotelID,ISNULL(pc.ParentCompanyName,'Unknown') AS ParentCompanyName,pr.ParentRelationshipName,hpc.StartDate
		FROM Hotels.dbo.Hotel_ParentCompany hpc
			INNER JOIN Hotels.dbo.ParentRelationship pr ON pr.ParentRelationshipID = hpc.ParentRelationshipID
			INNER JOIN Hotels.dbo.ParentCompany pc ON pc.ParentCompanyID = hpc.ParentCompanyID
		WHERE ISNULL(hpc.EndDate,'9999-01-01') >= GETDATE()
	),
	cte_Owner
	AS
	(
		SELECT HotelID,ParentCompanyName,ROW_NUMBER() OVER(PARTITION BY HotelID ORDER BY StartDate DESC) AS rowNum
		FROM cte_HotelParent
		WHERE ParentRelationshipName IN('Ownership','Hotel Owners and Developers')
	),
	cte_Management
	AS
	(
		SELECT HotelID,ParentCompanyName,ROW_NUMBER() OVER(PARTITION BY HotelID ORDER BY StartDate DESC) AS rowNum
		FROM cte_HotelParent
		WHERE ParentRelationshipName IN('Management','Hotel Management Company')
	),
	cte_FinanceCurrency
	AS
	(
		SELECT fin.custnmbr AS hotelCode,cur.currencyKey
		FROM IC.dbo.RM00101 fin
			INNER JOIN dim.Currency cur ON cur.[Currency Code] = fin.curncyID
	),
	cte_GDS
	AS
	(
		SELECT DISTINCT h.HotelID,g.GDS_Name,c.ChainCodeName
		FROM Hotels.dbo.Hotel h
			INNER JOIN Hotels.[dbo].[Hotel_GDS] hg ON hg.HotelID = h.HotelID
			LEFT JOIN Hotels.dbo.GDS g ON g.GDS_ID = hg.GDS_ID
			LEFT JOIN Hotels.[dbo].[ChainCode] c ON c.ChainCodeID = hg.ChainCodeID
	),
	cte_DC
	AS
	(
		SELECT HotelID,STRING_AGG(OTA,' \ ') AS ChainCodeName
		FROM
		(
			SELECT DISTINCT h.HotelID,
				CASE WHEN pt.ProToolsName = 'OTA Bundle' THEN 'OTA Bundle' ELSE ptd.OTA END AS OTA
			FROM Hotels.dbo.Hotel h
				INNER JOIN Hotels.[dbo].[Hotel_ProTools_Detail] hptd ON hptd.HotelID = h.HotelID
				INNER JOIN Hotels.[dbo].[ProTools_Detail] ptd ON ptd.ProTools_DetailID = hptd.ProTools_DetailID
				INNER JOIN Hotels.[dbo].[ProTools] pt ON pt.ProToolsID = ptd.ProToolsID
			WHERE pt.ProToolsName IN('Direct Connect','OTA Bundle')
		) x
		GROUP BY HotelID
	),
	cte_hotel
	AS
	(
		SELECT DISTINCT
			CASE WHEN h.SynXisCode IS NULL THEN ISNULL(hop.HotelCode,h.HotelCode) ELSE h.HotelCode END AS [hotelCode],
			h.HotelName,
			ISNULL(h.Address1,'Unknown') + ISNULL(' ' + h.Address2,'') AS [hotelAddress],
			ISNULL(loc.CityName,'Unknown') AS [hotelCity],
			'Unknown' AS [hotelSubState],
			ISNULL(loc.StateCode,'Unknown') AS [hotelState],
			ISNULL(loc.CountryName,'Unknown') AS [hotelCountry],
			ISNULL(loc.PostalCodeName,'Unknown') AS [hotelPostalCode],
			ISNULL(h.Latitude,0) AS Latitude,
			ISNULL(h.Longitude,0) AS Longitude,
			'Unknown' AS [hotelType],
			ISNULL(h.Rooms,0) AS [roomCount],
			0 AS [seasonalHotel],
			ISNULL(rev.AvgNightlyRate,0) AS [hotelAverageNightlyRate],
			ISNULL(loc.RegionName,'Unknown') AS [phGeographicRegionCode],
			ISNULL(loc.RegionName,'Unknown') AS [phGeographicRegionName],
			ISNULL(usrArea.WindowsAccount,'Unknown') AS [phAreaManagerUsername],
			ISNULL(usrArea.FirstName,'Unknown') + ' ' + ISNULL(usrArea.LastName,'') AS [phAreaManagerName],
			ISNULL(usrReg.WindowsAccount,'Unknown') AS [phRegionalDirectorUsername],
			ISNULL(usrReg.FirstName,'Unknown') + ' ' + ISNULL(usrReg.LastName,'') AS [phRegionalDirectorName],
			ISNULL(usrRev.WindowsAccount,'Unknown') AS [phRevenueAccountManagerUsername],
			ISNULL(usrRev.FirstName,'Unknown') + ' ' + ISNULL(usrRev.LastName,'') AS [phRevenueAccountManager],
			ISNULL(usrAD.WindowsAccount,'Unknown') AS [phRdSupervisorUsername],
			ISNULL(usrAD.FirstName,'Unknown') + ' ' + ISNULL(usrAD.LastName,'') AS [phRdSupervisor],
			ISNULL(usrRAM.WindowsAccount,'Unknown') AS [phRamSupervisorUsername],
			ISNULL(usrRAM.FirstName,'Unknown') + ' ' + ISNULL(usrRAM.LastName,'') AS [phRamSupervisor],
			ISNULL(ph.Code,'Unknown') AS [phPrimaryCollectionCode],
			ISNULL(ph.CollectionName,'Unknown') AS [phPrimaryCollectionName],
			ISNULL(he.Code,'Unknown') AS [hePrimaryCollectionCode],
			ISNULL(he.CollectionName,'Unknown') AS [hePrimaryCollectionName],
			ISNULL(res.Code,'Unknown') AS [resPrimaryCollectionCode],
			ISNULL(res.CollectionName,'Unknown') AS [resPrimaryCollectionName],
			'Unknown' AS [primary3rdParty],
			'Unknown' AS [primary3rdPartyCollection],
			0 AS [loyaltyParticipant],
			0 AS [loyaltyOptInParticipant],
			h.HotelID AS sourceKey,
			sc.StatusCodeName,
			ISNULL(h.[SynXisCode],'Unknown') AS [SynXisCode],
			ISNULL(CONVERT(nvarchar(50),h.[SynXisID]),'Unknown') AS [SynXisID],
			ISNULL(h.[OpenHospCode],'Unknown') AS [OpenHospCode],
			ISNULL(CONVERT(nvarchar(50),h.[OpenHospID]),'Unknown') AS [OpenHospID],
			ISNULL(h.ReportingCurrency,'Unknown') AS ReportingCurrency,
			ISNULL(h.LoyaltyEnrollmentGoals,0) AS LoyaltyEnrollmentGoals,
			ISNULL(h.phg_IPreferExecutiveAdminidName,'') AS phg_IPreferExecutiveAdminidName,
			h.LoyaltyCheckboxProgram,
			ISNULL(cl.ClassificationName,N'Unknown') AS [Hotel Classification],
			fin.currencyKey AS [Hotel Finance Currency],
			ISNULL(sabre.ChainCodeName,N'Unknown') AS [Sabre Chain Code],
			ISNULL(amadeus.ChainCodeName,N'Unknown') AS [Amadues Chain Code],
			ISNULL(galileo.ChainCodeName,N'Unknown') AS [Galileo Chain Code],
			ISNULL(worldspan.ChainCodeName,N'Unknown') AS [Worldspan Chain Code],
			ISNULL(pegasus.ChainCodeName,N'Unknown') AS [Pegasus Chain Code],
			ISNULL(dc.ChainCodeName,N'Unknown') AS [Direct Connect],
			ISNULL(o.ParentCompanyName,N'Unknown') AS [Primary Owner],
			ISNULL(m.ParentCompanyName,N'Unknown') AS [Hotel Management Company]
		FROM Hotels.[dbo].[Hotel] h
			LEFT JOIN Hotels.authority.Hotels_OpenHosp hop ON hop.openHospID = h.OpenHospID
			LEFT JOIN Hotels.[dbo].[StatusCode] sc ON sc.[StatusCodeID] = h.[StatusCodeID]
			LEFT JOIN Hotels.dbo.Classification cl ON cl.ClassificationID = h.ClassificationID
			LEFT JOIN cte_ResRev rev ON rev.HotelID = h.HotelID
			LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 1) ph ON ph.HotelID = h.HotelID
			LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 2 AND Code IN('HE','HW')) he ON he.HotelID = h.HotelID
			LEFT JOIN (SELECT HotelID,Code,CollectionName FROM cte_Col WHERE rowNum = 2 AND Code IN('RES')) res ON res.HotelID = h.HotelID
			LEFT JOIN cte_LOC loc ON loc.LocationID = h.LocationID
			LEFT JOIN cte_User usrArea ON usrArea.HotelID = h.HotelID AND usrArea.PHG_RoleName = 'Area Managing Director'
			LEFT JOIN cte_User usrReg ON usrReg.HotelID = h.HotelID AND usrReg.PHG_RoleName = 'Regional Director'
			LEFT JOIN cte_User usrRev ON usrRev.HotelID = h.HotelID AND usrRev.PHG_RoleName = 'Revenue Account Manager'
			LEFT JOIN cte_User usrAD ON usrAD.HotelID = h.HotelID AND usrAD.PHG_RoleName = 'Account Director'
			LEFT JOIN cte_User usrRAM ON usrRAM.HotelID = h.HotelID AND usrRAM.PHG_RoleName = 'Regional Admin'
			LEFT JOIN cte_FinanceCurrency fin ON fin.hotelCode = h.HotelCode

			LEFT JOIN cte_GDS sabre ON sabre.HotelID = h.HotelID AND sabre.GDS_Name = 'Sabre'
			LEFT JOIN cte_GDS amadeus ON amadeus.HotelID = h.HotelID AND amadeus.GDS_Name = 'Amadeus'
			LEFT JOIN cte_GDS galileo ON galileo.HotelID = h.HotelID AND galileo.GDS_Name = 'Galileo'
			LEFT JOIN cte_GDS worldspan ON worldspan.HotelID = h.HotelID AND worldspan.GDS_Name = 'Worldspan'
			LEFT JOIN cte_GDS pegasus ON pegasus.HotelID = h.HotelID AND pegasus.GDS_Name = 'Pegasus'
			LEFT JOIN cte_DC dc ON dc.HotelID = h.HotelID

			LEFT JOIN cte_Owner o ON o.HotelID = h.HotelID AND o.rowNum = 1
			LEFT JOIN cte_Management m ON m.HotelID = h.HotelID AND m.rowNum = 1
		WHERE h.HotelCode IS NOT NULL
			AND h.CRM_HotelID IS NOT NULL
	)
	MERGE INTO [dim].[Hotel] AS tgt
	USING
	(
		SELECT [hotelCode],[hotelName],[hotelAddress],[hotelCity],[hotelSubState],
				COALESCE(sa.subAreaName,c.[hotelState]) AS [hotelState],
				[hotelCountry],
				[hotelPostalCode],geography::Point(Latitude,Longitude,4326) AS [Hotel GeoCode],[hotelType],[roomCount],[seasonalHotel],[hotelAverageNightlyRate],
				[phGeographicRegionCode],[phGeographicRegionName],[phAreaManagerUsername],[phAreaManagerName],
				[phRegionalDirectorUsername],[phRegionalDirectorName],[phRevenueAccountManagerUsername],
				[phRevenueAccountManager],[phRdSupervisorUsername],[phRdSupervisor],[phRamSupervisorUsername],
				[phRamSupervisor],[phPrimaryCollectionCode],[phPrimaryCollectionName],[hePrimaryCollectionCode],
				[hePrimaryCollectionName],[resPrimaryCollectionCode],[resPrimaryCollectionName],
				[primary3rdParty],[primary3rdPartyCollection],[loyaltyParticipant],[loyaltyOptInParticipant],sourceKey,StatusCodeName,
				[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID],ReportingCurrency,
				LoyaltyEnrollmentGoals,phg_IPreferExecutiveAdminidName,LoyaltyCheckboxProgram,[Hotel Classification],[Hotel Finance Currency],
				[Sabre Chain Code],[Amadues Chain Code],[Galileo Chain Code],[Worldspan Chain Code],[Pegasus Chain Code],[Direct Connect],
				[Primary Owner],[Hotel Management Company]
		FROM cte_hotel c
			LEFT JOIN ISO.dbo.subAreas sa ON sa.subAreaCode = c.[hotelState]
											AND sa.countryCode2 = 'US'
											AND c.[hotelCountry] IN('US','USA','unknown','United States')

	) AS src ON src.sourceKey = tgt.[sourceKey]
	WHEN MATCHED THEN
		UPDATE
			SET [Hotel Code] = src.[hotelCode],
				[Hotel Name] = src.[hotelName],
				[Hotel Address] = src.[hotelAddress],
				[Hotel City] = src.[hotelCity],
				[Hotel Sub State] = src.[hotelSubState],
				[Hotel State] = src.[hotelState],
				[Hotel Country] = src.[hotelCountry],
				[Hotel Postal Code] = src.[hotelPostalCode],
				[Hotel GeoCode] = src.[Hotel GeoCode],
				[Hotel Type] = src.[hotelType],
				[Room Count] = src.[roomCount],
				[Seasonal Hotel] = src.[seasonalHotel],
				[Hotel Average Nightly Rate] = src.[hotelAverageNightlyRate],
				[PH Geographic Region Code] = src.[phGeographicRegionCode],
				[PH Geographic Region Name] = src.[phGeographicRegionName],
				[PH AMD User Name] = src.[phAreaManagerUsername],
				[PH AMD Name] = src.[phAreaManagerName],
				[PH RD User Name] = src.[phRegionalDirectorUsername],
				[PH RD Name] = src.[phRegionalDirectorName],
				[PH RAM User Name] = src.[phRevenueAccountManagerUsername],
				[PH RAM] = src.[phRevenueAccountManager],
				[PH RD Supervisor User Name] = src.[phRdSupervisorUsername],
				[PH RD Supervisor] = src.[phRdSupervisor],
				[PH RAM Supervisor User Name] = src.[phRamSupervisorUsername],
				[PH RAM Supervisor] = src.[phRamSupervisor],
				[PH Primary Collection Code] = src.[phPrimaryCollectionCode],
				[PH Primary Collection Name] = src.[phPrimaryCollectionName],
				[Historic Hotel Primary Collection Code] = src.[hePrimaryCollectionCode],
				[Historic Hotel Primary Collection Name] = src.[hePrimaryCollectionName],
				[Residence Hotel Primary Collection Code] = src.[resPrimaryCollectionCode],
				[Residence Hotel Primary Collection Name] = src.[resPrimaryCollectionName],
				[Primary 3rd Party] = src.[primary3rdParty],
				[Primary 3rd Party Collection] = src.[primary3rdPartyCollection],
				[I Prefer Participant] = src.[loyaltyParticipant],
				[I Prefer Opt-In Participant] = src.[loyaltyOptInParticipant],
				[sourceKey] = src.sourceKey,
				[Status Code] = src.StatusCodeName,
				[SynXisCode] = src.[SynXisCode],
				[SynXisID] = src.[SynXisID],
				[OpenHospCode] = src.[OpenHospCode],
				[OpenHospID] = src.[OpenHospID],
				[Hotel Reporting Currency] = src.ReportingCurrency,
				[Loyalty Engagement Specialist] = src.phg_IPreferExecutiveAdminidName,
				[Loyalty Enrollment Goals] = src.LoyaltyEnrollmentGoals,
				[Loyalty Checkbox Program] = src.LoyaltyCheckboxProgram,
				[Hotel Classification] = src.[Hotel Classification],
				[Hotel Finance Currency] = src.[Hotel Finance Currency],
				[Sabre Chain Code] = src.[Sabre Chain Code],
				[Amadues Chain Code] = src.[Amadues Chain Code],
				[Galileo Chain Code] = src.[Galileo Chain Code],
				[Worldspan Chain Code] = src.[Worldspan Chain Code],
				[Pegasus Chain Code] = src.[Pegasus Chain Code],
				[Direct Connect] = src.[Direct Connect],
				[Primary Owner] = src.[Primary Owner],
				[Hotel Management Company] = src.[Hotel Management Company]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([Hotel Code],[Hotel Name],[Hotel Address],[Hotel City],[Hotel Sub State],[Hotel State],[Hotel Country],[Hotel Postal Code],[Hotel GeoCode],[Hotel Type],[Room Count],[Seasonal Hotel],[Hotel Average Nightly Rate],[PH Geographic Region Code],[PH Geographic Region Name],[PH AMD User Name],[PH AMD Name],[PH RD User Name],[PH RD Name],[PH RAM User Name],[PH RAM],[PH RD Supervisor User Name],[PH RD Supervisor],[PH RAM Supervisor User Name],[PH RAM Supervisor],[PH Primary Collection Code],[PH Primary Collection Name],[Historic Hotel Primary Collection Code],[Historic Hotel Primary Collection Name],[Residence Hotel Primary Collection Code],[Residence Hotel Primary Collection Name],[Primary 3rd Party],[Primary 3rd Party Collection],[I Prefer Participant],[I Prefer Opt-In Participant],[sourceKey],[Status Code],[SynXisCode],[SynXisID],[OpenHospCode],[OpenHospID],[Hotel Reporting Currency],[Loyalty Engagement Specialist],[Loyalty Enrollment Goals],[Loyalty Checkbox Program],[Hotel Classification],[Hotel Finance Currency],[Sabre Chain Code],[Amadues Chain Code],[Galileo Chain Code],[Worldspan Chain Code],[Pegasus Chain Code],[Direct Connect],[Primary Owner],[Hotel Management Company])
		VALUES([hotelCode],src.[hotelName],src.[hotelAddress],src.[hotelCity],src.[hotelSubState],src.[hotelState],src.[hotelCountry],src.[hotelPostalCode],src.[Hotel GeoCode],src.[hotelType],src.[roomCount],src.[seasonalHotel],src.[hotelAverageNightlyRate],src.[phGeographicRegionCode],src.[phGeographicRegionName],src.[phAreaManagerUsername],src.[phAreaManagerName],src.[phRegionalDirectorUsername],src.[phRegionalDirectorName],src.[phRevenueAccountManagerUsername],src.[phRevenueAccountManager],src.[phRdSupervisorUsername],src.[phRdSupervisor],src.[phRamSupervisorUsername],src.[phRamSupervisor],src.[phPrimaryCollectionCode],src.[phPrimaryCollectionName],src.[hePrimaryCollectionCode],src.[hePrimaryCollectionName],src.[resPrimaryCollectionCode],src.[resPrimaryCollectionName],src.[primary3rdParty],src.[primary3rdPartyCollection],src.[loyaltyParticipant],src.[loyaltyOptInParticipant],src.sourceKey,src.StatusCodeName,src.[SynXisCode],src.[SynXisID],src.[OpenHospCode],src.[OpenHospID],src.ReportingCurrency,src.phg_IPreferExecutiveAdminidName,src.LoyaltyEnrollmentGoals,src.LoyaltyCheckboxProgram,src.[Hotel Classification],src.[Hotel Finance Currency],[Sabre Chain Code],[Amadues Chain Code],[Galileo Chain Code],[Worldspan Chain Code],[Pegasus Chain Code],[Direct Connect],[Primary Owner],[Hotel Management Company])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Update_Reservations_CrossBrand]'
GO
ALTER PROCEDURE [dbo].[Update_Reservations_CrossBrand]
AS
BEGIN
	SET NOCOUNT ON;
	-- CREATE & POPULATE #GUEST --------------------------
	IF OBJECT_ID('tempdb..#GUEST') IS NOT NULL
		DROP TABLE #GUEST;
	CREATE TABLE #GUEST(guestKey int NOT NULL, enrollmentHotelKey int NOT NULL, PRIMARY KEY CLUSTERED(guestKey))
	-- SELECT ALL MEMBERS THAT HAVE AN ENROLLMENT HOTEL AND HAVE A GUESTKEY
	-- THIS SHOULD BE ALL MEMBERS IN THEORY BUT THIS ENSURES WE DO NOT ACCIDENTLY SELECT ORPHANS
	-- WE NEED THE ENROLLMENT HOTEL KEY ALSO TO CHECK IF RESERVATION IS CROSS BRAND OR SAME BRAND
	INSERT INTO #GUEST(guestKey, enrollmentHotelKey)
	SELECT DISTINCT guestKey, enrollmentHotelKey
	FROM [Dimensional_Warehouse].[dim].[Member]
	WHERE enrollmentHotelKey > 0
		AND guestKey > 0
	------------------------------------------------------
	-- CREATE & POPULATE #RES ----------------------------
	IF OBJECT_ID('tempdb..#RES') IS NOT NULL
		DROP TABLE #RES;
	CREATE TABLE #RES(reservationKey int NOT NULL, PRIMARY KEY CLUSTERED(reservationKey))
	INSERT INTO #RES(reservationKey)
	SELECT reservationKey
	FROM [Dimensional_Warehouse].[fact].[Reservation] R
	-- GET ONLY RESERVATIONS THAT HAVE GUEST KEYS THAT HAVE NON-ZERO ENROLLMENT HOTEL KEYS
	-- AND ALSO WHERE THE GUEST MADE A RESERVATION AT A DIFFERENT HOTEL THAN THEIR ENROLLMENT HOTEL
	INNER JOIN  #GUEST g on r.guestKey = g.guestKey AND r.hotelKey <> g.enrollmentHotelKey
	WHERE r.hotelKey > 0
	------------------------------------------------------
	UPDATE fr
		SET [Cross Brand] = CASE --WHEN r.reservationKey IS NULL AND g.guestKey is null THEN 'Not I Prefer Reservation'
								WHEN r.reservationKey IS NOT NULL AND g.guestKey is not null  THEN 'Yes'
								WHEN r.reservationKey IS NULL AND g.guestKey is not null THEN 'No'
								ELSE 'Not I Prefer Reservation'
								END
	FROM [Dimensional_Warehouse].[fact].[Reservation] fr
		LEFT JOIN #RES r ON r.reservationKey = fr.reservationKey -- if no match in RES table then it's not cross brand
		left join #GUEST g on fr.guestKey = g.guestKey -- but we need to make sure it's a 
		-- 3 possible scenarios: 1. reservation is not I Prefer, nothing to match in RES or Guest table then cross brand should be 'NA'
		--						2. reservation is I prefer and present in RES table then cross brand should be 'Yes'
		--						3. reservations is I prefer and not present in Res table then cross brand should be 'No'
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_Warehouse_Reservations]'
GO
ALTER PROCEDURE [dbo].[Populate_Warehouse_Reservations]
	@debug bit = 0,
	@logProcess bit = 1,
	@Process_GUID uniqueidentifier
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
	SET NOCOUNT ON;

	DECLARE @OldestDate date = '2017-12-31' --DATEADD(YEAR,-4,GETDATE())

	DECLARE @ProcName sysname,@date datetime
	
	-- dim.BookingSource --------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[BookingSource]',@Process_GUID

		EXEC dbo.Populate_BookingSource
	-----------------------------------------------------------

	-- temp.BookingSource -------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[temp].[BookingSource]',@Process_GUID
		
		TRUNCATE TABLE temp.BookingSource

		INSERT INTO temp.BookingSource(bookingSourceKey,[Booking System],CRS_sourceKey,PH_sourceKey)
		SELECT DISTINCT bookingSourceKey,[Booking System],crs.value AS CRS_sourceKey,ph.value AS PH_sourceKey
		FROM dim.BookingSource
			CROSS APPLY string_split(ISNULL(CRS_sourceKey,0),',') crs
			CROSS APPLY string_split(ISNULL(PH_sourceKey,0),',') ph
	-----------------------------------------------------------

	-- dim.BookingStatus --------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- dim.CorporateAccount -----------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.CRS_Chain ------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[CRS_Chain]',@Process_GUID

		EXEC dbo.Populate_CRS_Chain
	-----------------------------------------------------------

	-- dim.Currency -------------------------------------------
		-- STATIC TABLE
	-----------------------------------------------------------

	-- POPULATE DimDate ---------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'POPULATE DimDate',@Process_GUID

		DECLARE @minDate date,@maxDate date
		SELECT @minDate = MIN(minDate),@maxDate = MAX(maxDate)
		FROM dbo.vw_GetMinMaxDate

		EXEC [dim].[Populate_DimDate_MinMax] @minDate,@maxDate;
	-----------------------------------------------------------

	-- UPDATE Year To Date Columns ----------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'UPDATE Year To Date Columns',@Process_GUID

		EXEC dbo.Update_YTD_Columns;
	-----------------------------------------------------------

	
	-- fact.CurrencyExchange ----------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[fact].[CurrencyExchange]',@Process_GUID

		EXEC dbo.Populate_CurrencyExchange
	-----------------------------------------------------------

	-- dim.DimDate --------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[DimDate]',@Process_GUID


		DECLARE @dimDate date = DATEADD(month,6,GETDATE())
		EXEC [dim].[Populate_DimDate] @dimDate
	-----------------------------------------------------------

	-- dim.FormOfPayment --------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[FormOfPayment]',@Process_GUID

		EXEC dbo.Populate_FormOfPayment
	-----------------------------------------------------------

	-- dim.Guest ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[Guest]',@Process_GUID

		EXEC dbo.Populate_Guest @OldestDate,@debug,@logProcess,@Process_GUID
	-----------------------------------------------------------

	-- dim.Hotel ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[Hotel]',@Process_GUID

		EXEC dbo.Populate_Hotel
	-----------------------------------------------------------

	-- dim.Hotel_PH_Collection --------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[Hotel_PH_Collection]',@Process_GUID

		EXEC dbo.Populate_Hotel_PH_Collection
	-----------------------------------------------------------

	-- UPDATE dim.Hotel for Collections -----------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'UPDATE [dim].[Hotel]',@Process_GUID

		EXEC dbo.Update_HotelPrimaryCollection
	-----------------------------------------------------------


	-- dim.Hotel_PH_Program -----------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[Hotel_PH_Program]',@Process_GUID

		EXEC dbo.Populate_Hotel_PH_Program
	-----------------------------------------------------------

	-- dim.HotelExpereince ------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[HotelExpereince]',@Process_GUID

		EXEC dbo.Populate_HotelExpereince
	-----------------------------------------------------------

	-- dim.HotelParentCompany ---------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[HotelParentCompany]',@Process_GUID

		EXEC dbo.Populate_HotelParentCompany
	-----------------------------------------------------------

	-- dim.LeisureAccount -------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.LoyaltyDetail --------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[LoyaltyDetail]',@Process_GUID

		EXEC dbo.Populate_LoyaltyDetail
	-----------------------------------------------------------

	-- dim.PartyDetail ----------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.PseudoCity -----------------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.Rate -----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[Rate]',@Process_GUID

		EXEC dbo.Populate_Rate
	-----------------------------------------------------------

	-- dim.Room -----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[Room]',@Process_GUID

		EXEC dbo.Populate_Room
	-----------------------------------------------------------

	-- dim.TravelAgency ---------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[TravelAgency]',@Process_GUID

		EXEC dbo.Populate_TravelAgency
	-----------------------------------------------------------

	-- dim.TravelAgencyGroup ----------------------------
		-- NOT IN PHASE I
	-----------------------------------------------------------

	-- dim.VoiceAgent -----------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dim].[VoiceAgent]',@Process_GUID

		EXEC dbo.Populate_VoiceAgent
	-----------------------------------------------------------

	-- fact.Reservation ---------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[fact].[Reservation]',@Process_GUID
		
		EXEC dbo.Populate_Reservations @minDate,@maxDate,@OldestDate
	-----------------------------------------------------------

	-- UPDATE guestKey ----------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'UPDATE guestKey',@Process_GUID
		
		EXEC dbo.Update_guestKey
	-----------------------------------------------------------

	-- UPDATE Cross Brand --------------------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'UPDATE Cross Brand',@Process_GUID
		
		EXEC dbo.Update_Reservations_CrossBrand
	-----------------------------------------------------------

	-- UPDATE [Is Frequent I Prefer] --------------------------
		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'UPDATE [Is Frequent I Prefer]',@Process_GUID

		IF OBJECT_ID('tempdb..#FREQ') IS NOT NULL
			DROP TABLE #FREQ;

		CREATE TABLE #FREQ
		(
			guestKey int NOT NULL,
			PRIMARY KEY CLUSTERED(guestKey)
		)

		INSERT INTO #FREQ(guestKey)
		SELECT r.guestKey
		FROM fact.Reservation r
		WHERE r.bookingStatusKey IN(SELECT bookingStatusKey FROM dim.BookingStatus WHERE [Booking Status Name] = 'Confirmed')
			AND r.[Arrival Date] IN(SELECT dateKey FROM dim.DimDate WHERE [Full Date] < GETDATE())
		GROUP BY r.guestKey
		HAVING COUNT(r.[Confirmation Number]) > 1

		UPDATE dim.Guest
			SET [Is Frequent I Prefer] = 'True'
		WHERE guestKey IN(SELECT guestKey FROM #FREQ)
	-----------------------------------------------------------
	
	-- dbo.Delete_OldestData ---------------------------------------
		--SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		--EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'[dbo].[Delete_OldestData]',@Process_GUID
		
		--EXEC dbo.Delete_OldestData @OldestDate
	-----------------------------------------------------------

		SELECT @ProcName = OBJECT_NAME(@@PROCID),@date = GETDATE()
		EXEC operations.logProcess @debug,@logProcess,@ProcName,@date,'FINISHED',@Process_GUID
END

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dim].[CorporateAccount]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dim].[CorporateAccount] DROP
COLUMN [Corporate Address],
COLUMN [Corporate City],
COLUMN [Corporate State],
COLUMN [Corporate Country],
COLUMN [Corporate Postal Code]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Populate_CorporateAccount]'
GO

ALTER PROCEDURE [dbo].[Populate_CorporateAccount]
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON;

	;MERGE INTO dim.CorporateAccount AS tgt
	USING
	(
		SELECT a.[name] AS [Corporate Account],
			a2.phg_ratecode AS [Rate Code],
			a2.phg_corporatecode AS [Corporate Code],
			a.phg_corporateglobalaccountmanageridname AS [PH GAM]
		FROM LocalCRM.dbo.Account2 a2
			INNER JOIN LocalCRM.dbo.Account a ON a2.accountid = a.accountid
		WHERE a.phg_corporateaccount = 1
			AND a.phg_corporateglobalaccountmanageridname IS NOT NULL
	) AS src ON src.[Rate Code] = tgt.[Rate Code] AND src.[Corporate Code] = tgt.[Corporate Code]
	WHEN MATCHED THEN
		UPDATE
			SET [Corporate Account] = src.[Corporate Account],
				[PH GAM] = src.[PH GAM]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT ([Corporate Account],[Rate Code],[Corporate Code],[PH GAM])
		VALUES ([Corporate Account],[Rate Code],[Corporate Code],[PH GAM])
	--WHEN NOT MATCHED BY SOURCE THEN
	--	DELETE
	;
END
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
